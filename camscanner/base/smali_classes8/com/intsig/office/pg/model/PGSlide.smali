.class public Lcom/intsig/office/pg/model/PGSlide;
.super Ljava/lang/Object;
.source "PGSlide.java"


# static fields
.field public static final Slide_Layout:B = 0x1t

.field public static final Slide_Master:B = 0x0t

.field public static final Slide_Normal:B = 0x2t


# instance fields
.field private bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private geometryType:I

.field private grpShapeLst:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private hasTable:Z

.field private hasTransition:Z

.field private masterIndexs:[I

.field private notes:Lcom/intsig/office/pg/model/PGNotes;

.field private shapeAnimLst:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/pg/animate/ShapeAnimation;",
            ">;"
        }
    .end annotation
.end field

.field private shapeCountForFind:I

.field private shapes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/shape/IShape;",
            ">;"
        }
    .end annotation
.end field

.field private shapesForFind:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/shape/IShape;",
            ">;"
        }
    .end annotation
.end field

.field private showMasterHeadersFooters:Z

.field private slideNo:I

.field private slideType:I

.field private smartArtList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/common/shape/SmartArt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeCountForFind:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 3
    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->masterIndexs:[I

    .line 4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    const/4 v1, 0x1

    .line 5
    iput-boolean v1, p0, Lcom/intsig/office/pg/model/PGSlide;->showMasterHeadersFooters:Z

    .line 6
    iput v0, p0, Lcom/intsig/office/pg/model/PGSlide;->geometryType:I

    return-void

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(ILcom/intsig/office/pg/model/PGNotes;)V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeCountForFind:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 9
    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->masterIndexs:[I

    .line 10
    iput p1, p0, Lcom/intsig/office/pg/model/PGSlide;->slideNo:I

    .line 11
    iput-object p2, p0, Lcom/intsig/office/pg/model/PGSlide;->notes:Lcom/intsig/office/pg/model/PGNotes;

    .line 12
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    const/4 p1, 0x1

    .line 13
    iput-boolean p1, p0, Lcom/intsig/office/pg/model/PGSlide;->showMasterHeadersFooters:Z

    .line 14
    iput v0, p0, Lcom/intsig/office/pg/model/PGSlide;->geometryType:I

    return-void

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method


# virtual methods
.method public addGroupShape(ILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 11
    .line 12
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    new-array v1, v0, [Ljava/lang/Integer;

    .line 17
    .line 18
    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    :goto_0
    if-ge v2, v0, :cond_2

    .line 23
    .line 24
    aget-object v3, v1, v2

    .line 25
    .line 26
    iget-object v4, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 27
    .line 28
    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-eqz v4, :cond_1

    .line 33
    .line 34
    iget-object v4, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 35
    .line 36
    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    check-cast v4, Ljava/util/List;

    .line 41
    .line 42
    invoke-interface {p2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    invoke-interface {p2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    .line 47
    .line 48
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 52
    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public addShapeAnimation(Lcom/intsig/office/pg/animate/ShapeAnimation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    if-eqz p1, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    :cond_1
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public addSmartArt(Ljava/lang/String;Lcom/intsig/office/common/shape/SmartArt;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->smartArtList:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->smartArtList:Ljava/util/Map;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->smartArtList:Ljava/util/Map;

    .line 13
    .line 14
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public appendShapes(Lcom/intsig/office/common/shape/IShape;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTable:Z

    .line 5
    .line 6
    if-nez v0, :cond_2

    .line 7
    .line 8
    invoke-interface {p1}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x6

    .line 13
    if-ne v0, v1, :cond_1

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const/4 v0, 0x0

    .line 18
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTable:Z

    .line 19
    .line 20
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->notes:Lcom/intsig/office/pg/model/PGNotes;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGNotes;->dispose()V

    .line 7
    .line 8
    .line 9
    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->notes:Lcom/intsig/office/pg/model/PGNotes;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 16
    .line 17
    .line 18
    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 19
    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 21
    .line 22
    if-eqz v0, :cond_3

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_2

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    check-cast v2, Lcom/intsig/office/common/shape/IShape;

    .line 39
    .line 40
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->dispose()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 45
    .line 46
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 47
    .line 48
    .line 49
    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 50
    .line 51
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 52
    .line 53
    if-eqz v0, :cond_4

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->dispose()V

    .line 56
    .line 57
    .line 58
    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 59
    .line 60
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 61
    .line 62
    if-eqz v0, :cond_5

    .line 63
    .line 64
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 65
    .line 66
    .line 67
    iput-object v1, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 68
    .line 69
    :cond_5
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGeometryType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/model/PGSlide;->geometryType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGroupShape()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->grpShapeLst:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMasterIndexs()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->masterIndexs:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNotes()Lcom/intsig/office/pg/model/PGNotes;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->notes:Lcom/intsig/office/pg/model/PGNotes;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShape(FF)Lcom/intsig/office/common/shape/IShape;
    .locals 7

    .line 3
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/common/shape/IShape;

    .line 4
    invoke-interface {v1}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v2

    .line 5
    invoke-interface {v1}, Lcom/intsig/office/common/shape/IShape;->getType()S

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    .line 6
    check-cast v1, Lcom/intsig/office/common/shape/TableShape;

    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/TableShape;->getCellCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 8
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/shape/TableShape;->getCell(I)Lcom/intsig/office/common/shape/TableCell;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 9
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/TableCell;->getBounds()Lcom/intsig/office/java/awt/Rectanglef;

    move-result-object v5

    .line 10
    invoke-virtual {v5, p1, p2}, Lcom/intsig/office/java/awt/Rectanglef;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 11
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/TableCell;->getText()Lcom/intsig/office/common/shape/TextBox;

    move-result-object p1

    return-object p1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    float-to-double v3, p1

    float-to-double v5, p2

    .line 12
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->contains(DD)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public getShape(I)Lcom/intsig/office/common/shape/IShape;
    .locals 1

    if-ltz p1, :cond_1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/common/shape/IShape;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getShape(II)Lcom/intsig/office/common/shape/IShape;
    .locals 8

    .line 13
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/common/shape/IShape;

    .line 14
    invoke-interface {v1}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v2

    .line 15
    invoke-interface {v1}, Lcom/intsig/office/common/shape/IShape;->getType()S

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    .line 16
    check-cast v1, Lcom/intsig/office/common/shape/TableShape;

    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/TableShape;->getCellCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 18
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/shape/TableShape;->getCell(I)Lcom/intsig/office/common/shape/TableCell;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 19
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/TableCell;->getBounds()Lcom/intsig/office/java/awt/Rectanglef;

    move-result-object v5

    int-to-float v6, p1

    int-to-float v7, p2

    .line 20
    invoke-virtual {v5, v6, v7}, Lcom/intsig/office/java/awt/Rectanglef;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 21
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/TableCell;->getText()Lcom/intsig/office/common/shape/TextBox;

    move-result-object p1

    return-object p1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 22
    :cond_2
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public getShapeCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShapeCountForFind()I
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTable:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    iget v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeCountForFind:I

    .line 11
    .line 12
    if-lez v0, :cond_1

    .line 13
    .line 14
    return v0

    .line 15
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const/4 v1, 0x0

    .line 29
    const/4 v2, 0x0

    .line 30
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-eqz v3, :cond_5

    .line 35
    .line 36
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    check-cast v3, Lcom/intsig/office/common/shape/IShape;

    .line 41
    .line 42
    invoke-interface {v3}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    const/4 v5, 0x6

    .line 47
    if-ne v4, v5, :cond_4

    .line 48
    .line 49
    const/4 v4, 0x0

    .line 50
    :goto_1
    move-object v5, v3

    .line 51
    check-cast v5, Lcom/intsig/office/common/shape/TableShape;

    .line 52
    .line 53
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/TableShape;->getCellCount()I

    .line 54
    .line 55
    .line 56
    move-result v6

    .line 57
    if-ge v4, v6, :cond_2

    .line 58
    .line 59
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/shape/TableShape;->getCell(I)Lcom/intsig/office/common/shape/TableCell;

    .line 60
    .line 61
    .line 62
    move-result-object v5

    .line 63
    if-eqz v5, :cond_3

    .line 64
    .line 65
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/TableCell;->getText()Lcom/intsig/office/common/shape/TextBox;

    .line 66
    .line 67
    .line 68
    move-result-object v6

    .line 69
    if-eqz v6, :cond_3

    .line 70
    .line 71
    iget-object v6, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 72
    .line 73
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/TableCell;->getText()Lcom/intsig/office/common/shape/TextBox;

    .line 74
    .line 75
    .line 76
    move-result-object v5

    .line 77
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    add-int/lit8 v2, v2, 0x1

    .line 81
    .line 82
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_4
    iget-object v4, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 86
    .line 87
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    add-int/lit8 v2, v2, 0x1

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_5
    iput v2, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeCountForFind:I

    .line 94
    .line 95
    return v2
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getShapeForFind(I)Lcom/intsig/office/common/shape/IShape;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTable:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    if-ltz p1, :cond_2

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-lt p1, v0, :cond_1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapesForFind:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    check-cast p1, Lcom/intsig/office/common/shape/IShape;

    .line 28
    .line 29
    return-object p1

    .line 30
    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 31
    return-object p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getShapes()[Lcom/intsig/office/common/shape/IShape;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/common/shape/IShape;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/common/shape/IShape;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSlideNo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/model/PGSlide;->slideNo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSlideShowAnimation()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/pg/animate/ShapeAnimation;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapeAnimLst:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSlideType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/model/PGSlide;->slideType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSmartArt(Ljava/lang/String;)Lcom/intsig/office/common/shape/SmartArt;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->smartArtList:Ljava/util/Map;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/office/common/shape/SmartArt;

    .line 12
    .line 13
    return-object p1

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_1

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    check-cast v2, Lcom/intsig/office/common/shape/IShape;

    .line 17
    .line 18
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    const/4 v4, 0x1

    .line 23
    if-ne v3, v4, :cond_0

    .line 24
    .line 25
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getPlaceHolderID()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-ne v3, p1, :cond_0

    .line 30
    .line 31
    return-object v2

    .line 32
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 p1, 0x0

    .line 36
    return-object p1
    .line 37
    .line 38
.end method

.method public getTextboxShape(II)Lcom/intsig/office/common/shape/IShape;
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    sub-int/2addr v0, v1

    .line 9
    :goto_0
    if-ltz v0, :cond_3

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/pg/model/PGSlide;->shapes:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Lcom/intsig/office/common/shape/IShape;

    .line 18
    .line 19
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    const/4 v5, 0x6

    .line 28
    if-ne v4, v5, :cond_1

    .line 29
    .line 30
    check-cast v2, Lcom/intsig/office/common/shape/TableShape;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/TableShape;->getCellCount()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    const/4 v4, 0x0

    .line 37
    :goto_1
    if-ge v4, v3, :cond_2

    .line 38
    .line 39
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/TableShape;->getCell(I)Lcom/intsig/office/common/shape/TableCell;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    if-eqz v5, :cond_0

    .line 44
    .line 45
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/TableCell;->getBounds()Lcom/intsig/office/java/awt/Rectanglef;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    int-to-float v7, p1

    .line 50
    int-to-float v8, p2

    .line 51
    invoke-virtual {v6, v7, v8}, Lcom/intsig/office/java/awt/Rectanglef;->contains(FF)Z

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    if-eqz v6, :cond_0

    .line 56
    .line 57
    invoke-virtual {v5}, Lcom/intsig/office/common/shape/TableCell;->getText()Lcom/intsig/office/common/shape/TextBox;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    return-object p1

    .line 62
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_1
    invoke-virtual {v3, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->contains(II)Z

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    if-eqz v3, :cond_2

    .line 70
    .line 71
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-ne v3, v1, :cond_2

    .line 76
    .line 77
    return-object v2

    .line 78
    :cond_2
    add-int/lit8 v0, v0, -0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_3
    const/4 p1, 0x0

    .line 82
    return-object p1
    .line 83
    .line 84
    .line 85
.end method

.method public hasTransition()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTransition:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowMasterHeadersFooter()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/model/PGSlide;->showMasterHeadersFooters:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/model/PGSlide;->bgFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGeometryType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/pg/model/PGSlide;->geometryType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLayoutSlideIndex(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->masterIndexs:[I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    aput p1, v0, v1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setMasterSlideIndex(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/PGSlide;->masterIndexs:[I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aput p1, v0, v1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNotes(Lcom/intsig/office/pg/model/PGNotes;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/model/PGSlide;->notes:Lcom/intsig/office/pg/model/PGNotes;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShowMasterHeadersFooters(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/pg/model/PGSlide;->showMasterHeadersFooters:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSlideNo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/pg/model/PGSlide;->slideNo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSlideType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/pg/model/PGSlide;->slideType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTransition(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/pg/model/PGSlide;->hasTransition:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

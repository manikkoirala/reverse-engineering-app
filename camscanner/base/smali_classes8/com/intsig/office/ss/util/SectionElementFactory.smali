.class public Lcom/intsig/office/ss/util/SectionElementFactory;
.super Ljava/lang/Object;
.source "SectionElementFactory.java"


# static fields
.field private static attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

.field private static book:Lcom/intsig/office/ss/model/baseModel/Workbook;

.field private static leaf:Lcom/intsig/office/simpletext/model/LeafElement;

.field private static offset:I

.field private static paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 3
    .line 4
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    sput v1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static getSectionElement(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/ss/model/baseModel/Cell;)Lcom/intsig/office/simpletext/model/SectionElement;
    .locals 5

    .line 1
    sput-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    move-result-object p0

    .line 3
    new-instance v0, Lcom/intsig/office/simpletext/model/SectionElement;

    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    const-wide/16 v1, 0x0

    .line 4
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v1

    .line 6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v2, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 7
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 9
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    move-result v2

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_1

    const/4 v4, 0x3

    if-eq v2, v4, :cond_1

    :cond_0
    const/4 v4, 0x0

    .line 11
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual {v2, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->getFontIndex()S

    .line 13
    sput v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 14
    invoke-static {v0, p1, p0, p2}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/ss/model/baseModel/Cell;)I

    move-result p0

    if-eqz p0, :cond_2

    int-to-long p0, p0

    .line 15
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    goto :goto_0

    .line 16
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/SectionElement;->dispose()V

    const/4 v0, 0x0

    .line 17
    :goto_0
    invoke-static {}, Lcom/intsig/office/ss/util/SectionElementFactory;->dispose()V

    return-object v0
.end method

.method public static getSectionElement(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/simpletext/model/SectionElement;
    .locals 5

    .line 18
    sput-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 19
    new-instance p0, Lcom/intsig/office/simpletext/model/SectionElement;

    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    const-wide/16 v0, 0x0

    .line 20
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v0

    .line 22
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    iget v2, p2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x41700000    # 15.0f

    mul-float v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 23
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    iget p2, p2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-float p2, p2

    mul-float p2, p2, v3

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    invoke-virtual {v1, v0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 24
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginLeft()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 25
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginTop()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 26
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginRight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 27
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getMarginBottom()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getVerticalAlignment()S

    move-result p2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p2, v1, :cond_0

    const/4 v3, 0x2

    if-eq p2, v3, :cond_2

    const/4 v4, 0x3

    if-eq p2, v4, :cond_1

    const/4 v3, 0x4

    if-eq p2, v3, :cond_2

    const/4 v3, 0x7

    if-eq p2, v3, :cond_2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    .line 29
    :cond_2
    :goto_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 30
    invoke-static {p0, p1}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;)I

    move-result p1

    int-to-long p1, p1

    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 32
    invoke-static {}, Lcom/intsig/office/ss/util/SectionElementFactory;->dispose()V

    return-object p0
.end method

.method private static processBreakLine(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;IBLjava/lang/String;Z)I
    .locals 15

    .line 1
    move-object v0, p0

    .line 2
    move-object/from16 v1, p1

    .line 3
    .line 4
    move/from16 v2, p3

    .line 5
    .line 6
    move-object/from16 v3, p4

    .line 7
    .line 8
    const-wide/16 v4, 0x0

    .line 9
    .line 10
    const-string v6, "\n"

    .line 11
    .line 12
    const/4 v7, 0x0

    .line 13
    if-eqz v3, :cond_1

    .line 14
    .line 15
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    .line 16
    .line 17
    .line 18
    move-result v8

    .line 19
    if-nez v8, :cond_0

    .line 20
    .line 21
    goto/16 :goto_0

    .line 22
    .line 23
    :cond_0
    new-instance v8, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 24
    .line 25
    invoke-direct {v8, v3}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    sput-object v8, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 31
    .line 32
    .line 33
    move-result-object v9

    .line 34
    sget-object v10, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 35
    .line 36
    const/4 v12, 0x0

    .line 37
    sget-object v8, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 38
    .line 39
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 40
    .line 41
    .line 42
    move-result-object v13

    .line 43
    sget-object v14, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 44
    .line 45
    move/from16 v11, p2

    .line 46
    .line 47
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 48
    .line 49
    .line 50
    sget-object v8, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 51
    .line 52
    sget v9, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 53
    .line 54
    int-to-long v9, v9

    .line 55
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 56
    .line 57
    .line 58
    sget v8, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 59
    .line 60
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    add-int/2addr v8, v3

    .line 65
    sput v8, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 66
    .line 67
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 68
    .line 69
    int-to-long v8, v8

    .line 70
    invoke-virtual {v3, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 71
    .line 72
    .line 73
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 74
    .line 75
    sget-object v8, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 76
    .line 77
    invoke-virtual {v3, v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 78
    .line 79
    .line 80
    if-eqz p5, :cond_3

    .line 81
    .line 82
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 83
    .line 84
    new-instance v8, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    sget-object v9, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 90
    .line 91
    invoke-virtual {v9, v7}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v9

    .line 95
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v6

    .line 105
    invoke-virtual {v3, v6}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    sget v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 109
    .line 110
    add-int/lit8 v3, v3, 0x1

    .line 111
    .line 112
    sput v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 113
    .line 114
    sget-object v6, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 115
    .line 116
    int-to-long v8, v3

    .line 117
    invoke-virtual {v6, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 118
    .line 119
    .line 120
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 121
    .line 122
    sget v6, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 123
    .line 124
    int-to-long v8, v6

    .line 125
    invoke-virtual {v3, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 126
    .line 127
    .line 128
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 129
    .line 130
    invoke-virtual {p0, v3, v4, v5}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 131
    .line 132
    .line 133
    new-instance v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 134
    .line 135
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 136
    .line 137
    .line 138
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 139
    .line 140
    sget v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 141
    .line 142
    int-to-long v3, v3

    .line 143
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 144
    .line 145
    .line 146
    new-instance v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 147
    .line 148
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 149
    .line 150
    .line 151
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 158
    .line 159
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    sget-object v4, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 164
    .line 165
    invoke-virtual {v0, v1, v3, v4}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 166
    .line 167
    .line 168
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    sget-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 173
    .line 174
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 179
    .line 180
    .line 181
    sput-object v7, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 182
    .line 183
    goto/16 :goto_2

    .line 184
    .line 185
    :cond_1
    :goto_0
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 186
    .line 187
    if-eqz v3, :cond_2

    .line 188
    .line 189
    new-instance v8, Ljava/lang/StringBuilder;

    .line 190
    .line 191
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .line 193
    .line 194
    sget-object v9, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 195
    .line 196
    invoke-virtual {v9, v7}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v9

    .line 200
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v6

    .line 210
    invoke-virtual {v3, v6}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    sget v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 214
    .line 215
    add-int/lit8 v3, v3, 0x1

    .line 216
    .line 217
    sput v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 218
    .line 219
    sget-object v6, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 220
    .line 221
    int-to-long v8, v3

    .line 222
    invoke-virtual {v6, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 223
    .line 224
    .line 225
    goto :goto_1

    .line 226
    :cond_2
    new-instance v3, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 227
    .line 228
    invoke-direct {v3, v6}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 229
    .line 230
    .line 231
    sput-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 232
    .line 233
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 234
    .line 235
    .line 236
    move-result-object v8

    .line 237
    sget-object v9, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 238
    .line 239
    const/4 v11, 0x0

    .line 240
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 241
    .line 242
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 243
    .line 244
    .line 245
    move-result-object v12

    .line 246
    sget-object v13, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 247
    .line 248
    move/from16 v10, p2

    .line 249
    .line 250
    invoke-virtual/range {v8 .. v13}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 251
    .line 252
    .line 253
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 254
    .line 255
    sget v6, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 256
    .line 257
    int-to-long v8, v6

    .line 258
    invoke-virtual {v3, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 259
    .line 260
    .line 261
    sget v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 262
    .line 263
    add-int/lit8 v3, v3, 0x1

    .line 264
    .line 265
    sput v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 266
    .line 267
    sget-object v6, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 268
    .line 269
    int-to-long v8, v3

    .line 270
    invoke-virtual {v6, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 271
    .line 272
    .line 273
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 274
    .line 275
    sget-object v6, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 276
    .line 277
    invoke-virtual {v3, v6}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 278
    .line 279
    .line 280
    :goto_1
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 281
    .line 282
    sget v6, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 283
    .line 284
    int-to-long v8, v6

    .line 285
    invoke-virtual {v3, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 286
    .line 287
    .line 288
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 289
    .line 290
    invoke-virtual {p0, v3, v4, v5}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 291
    .line 292
    .line 293
    new-instance v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 294
    .line 295
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 296
    .line 297
    .line 298
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 299
    .line 300
    sget v3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 301
    .line 302
    int-to-long v3, v3

    .line 303
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 304
    .line 305
    .line 306
    new-instance v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 307
    .line 308
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 309
    .line 310
    .line 311
    sput-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 312
    .line 313
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 314
    .line 315
    .line 316
    move-result-object v0

    .line 317
    sget-object v3, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 318
    .line 319
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 320
    .line 321
    .line 322
    move-result-object v3

    .line 323
    sget-object v4, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 324
    .line 325
    invoke-virtual {v0, v1, v3, v4}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 326
    .line 327
    .line 328
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 329
    .line 330
    .line 331
    move-result-object v0

    .line 332
    sget-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 333
    .line 334
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 335
    .line 336
    .line 337
    move-result-object v1

    .line 338
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 339
    .line 340
    .line 341
    sput-object v7, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 342
    .line 343
    :cond_3
    :goto_2
    sget v0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 344
    .line 345
    return v0
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method private static processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/ss/model/baseModel/Cell;)I
    .locals 8

    const/4 p3, 0x0

    .line 1
    sput p3, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getString()Ljava/lang/String;

    move-result-object v0

    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 v4, 0x3

    if-eq v1, v4, :cond_2

    const/4 v3, 0x5

    if-eq v1, v3, :cond_1

    const/4 v3, 0x6

    if-eq v1, v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    .line 4
    :cond_2
    :goto_0
    new-instance v1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    sput-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 5
    sget v4, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 6
    new-instance v1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    sput-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 7
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    move-result-object v1

    sget-object v4, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v4

    sget-object v5, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    invoke-virtual {v1, p2, v4, v5}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    sget-object v4, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->getFormatRunCount()I

    move-result v1

    if-nez v1, :cond_3

    .line 10
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getFontIndex()S

    move-result p1

    .line 11
    invoke-static {p0, p2, v0, p1, v3}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    goto/16 :goto_3

    .line 12
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->formatIterator()Ljava/util/Iterator;

    move-result-object p1

    .line 13
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;

    .line 14
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v4

    invoke-virtual {v0, p3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    .line 15
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    move-result v4

    const-string v5, ""

    const-string v6, "\n"

    if-nez v4, :cond_4

    .line 16
    invoke-virtual {p3, v6, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p3

    .line 17
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getFontIndex()S

    move-result v4

    .line 18
    invoke-static {p0, p2, p3, v4, v3}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    .line 19
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_7

    .line 20
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;

    .line 21
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-le v4, v7, :cond_5

    goto :goto_2

    .line 22
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v4

    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v7

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 23
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    move-result v7

    if-nez v7, :cond_6

    .line 24
    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 25
    :cond_6
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result v1

    .line 26
    invoke-static {p0, p2, v4, v1, v3}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    move-object v1, p3

    goto :goto_1

    .line 27
    :cond_7
    :goto_2
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 28
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    move-result p3

    if-nez p3, :cond_8

    .line 29
    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 30
    :cond_8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result p3

    .line 31
    invoke-static {p0, p2, p1, p3, v3}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    .line 32
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    if-eqz p1, :cond_9

    .line 33
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p3, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 34
    sget p1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    add-int/2addr p1, v2

    sput p1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 35
    :cond_9
    :goto_3
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    if-eqz p1, :cond_a

    sget-object p2, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    if-nez p1, :cond_a

    .line 36
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    sget p2, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    int-to-long p2, p2

    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 37
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    sget-object p2, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 38
    :cond_a
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    move-result-wide p1

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/SectionElement;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    if-nez p1, :cond_b

    .line 39
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    sget p2, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    int-to-long p2, p2

    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 40
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    const-wide/16 p2, 0x0

    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 41
    :cond_b
    sget p0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    return p0
.end method

.method private static processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;)I
    .locals 8

    const/4 v0, 0x0

    .line 42
    sput v0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 43
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getString()Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getHorizontalAlignment()S

    move-result p1

    const/4 v3, 0x1

    if-eq p1, v3, :cond_2

    const/4 v4, 0x2

    if-eq p1, v4, :cond_1

    const/4 v5, 0x3

    if-eq p1, v5, :cond_0

    const/4 v4, 0x4

    if-eq p1, v4, :cond_1

    const/4 v4, 0x7

    if-eq p1, v4, :cond_1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 46
    :cond_2
    :goto_0
    new-instance p1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-direct {p1}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    sput-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 47
    sget v4, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    int-to-long v4, v4

    invoke-virtual {p1, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 48
    new-instance p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    invoke-direct {p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    sput-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 49
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p1

    sget-object v4, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;->getUnicodeString()Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;->formatIterator()Ljava/util/Iterator;

    move-result-object p1

    .line 51
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;

    .line 52
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_4

    .line 53
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;

    .line 54
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-le v6, v7, :cond_3

    goto :goto_2

    .line 55
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v6

    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result v1

    .line 57
    invoke-static {p0, v5, v6, v1, v0}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    move-object v1, v4

    goto :goto_1

    .line 58
    :cond_4
    :goto_2
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getCharacterPos()S

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/common/UnicodeString$FormatRun;->getFontIndex()S

    move-result v1

    invoke-static {p0, v5, p1, v1, v0}, Lcom/intsig/office/ss/util/SectionElementFactory;->processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V

    .line 59
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    if-eqz p1, :cond_5

    sget-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/ParagraphElement;->getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    if-nez p1, :cond_5

    .line 60
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-virtual {v1, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 61
    sget p1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    add-int/2addr p1, v3

    sput p1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 62
    sget-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 63
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    sget-object v0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 64
    :cond_5
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    if-nez p1, :cond_6

    .line 65
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    sget v0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 66
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 67
    :cond_6
    sget p0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    return p0
.end method

.method private static processParagraph_SubString(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;Ljava/lang/String;IB)V
    .locals 9

    .line 1
    const-string v0, "\n"

    .line 2
    .line 3
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance p0, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 10
    .line 11
    invoke-direct {p0, p2}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    sput-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    sget-object v1, Lcom/intsig/office/ss/util/SectionElementFactory;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    sget-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    sget-object v5, Lcom/intsig/office/ss/util/SectionElementFactory;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 30
    .line 31
    move v2, p3

    .line 32
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 33
    .line 34
    .line 35
    sget-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 36
    .line 37
    sget p1, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 38
    .line 39
    int-to-long p3, p1

    .line 40
    invoke-virtual {p0, p3, p4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 41
    .line 42
    .line 43
    sget p0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 44
    .line 45
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    add-int/2addr p0, p1

    .line 50
    sput p0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 51
    .line 52
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 53
    .line 54
    int-to-long p2, p0

    .line 55
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 56
    .line 57
    .line 58
    sget-object p0, Lcom/intsig/office/ss/util/SectionElementFactory;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 59
    .line 60
    sget-object p1, Lcom/intsig/office/ss/util/SectionElementFactory;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 61
    .line 62
    invoke-virtual {p0, p1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_0
    const/16 v0, 0xa

    .line 67
    .line 68
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    :goto_0
    if-ltz v1, :cond_2

    .line 73
    .line 74
    const/4 v2, 0x0

    .line 75
    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v7

    .line 79
    const/4 v8, 0x1

    .line 80
    move-object v3, p0

    .line 81
    move-object v4, p1

    .line 82
    move v5, p3

    .line 83
    move v6, p4

    .line 84
    invoke-static/range {v3 .. v8}, Lcom/intsig/office/ss/util/SectionElementFactory;->processBreakLine(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;IBLjava/lang/String;Z)I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    sput v2, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 89
    .line 90
    add-int/lit8 v1, v1, 0x1

    .line 91
    .line 92
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    if-ge v1, v2, :cond_1

    .line 97
    .line 98
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object p2

    .line 106
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    goto :goto_0

    .line 111
    :cond_1
    const/4 p2, 0x0

    .line 112
    :cond_2
    move-object v4, p2

    .line 113
    if-eqz v4, :cond_3

    .line 114
    .line 115
    const/4 v5, 0x1

    .line 116
    move-object v0, p0

    .line 117
    move-object v1, p1

    .line 118
    move v2, p3

    .line 119
    move v3, p4

    .line 120
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/ss/util/SectionElementFactory;->processBreakLine(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/style/CellStyle;IBLjava/lang/String;Z)I

    .line 121
    .line 122
    .line 123
    move-result p0

    .line 124
    sput p0, Lcom/intsig/office/ss/util/SectionElementFactory;->offset:I

    .line 125
    .line 126
    :cond_3
    :goto_1
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

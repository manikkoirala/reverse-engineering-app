.class public Lcom/intsig/office/ss/util/HeaderUtil;
.super Ljava/lang/Object;
.source "HeaderUtil.java"


# static fields
.field private static util:Lcom/intsig/office/ss/util/HeaderUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/ss/util/HeaderUtil;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/ss/util/HeaderUtil;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/ss/util/HeaderUtil;->util:Lcom/intsig/office/ss/util/HeaderUtil;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static instance()Lcom/intsig/office/ss/util/HeaderUtil;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/ss/util/HeaderUtil;->util:Lcom/intsig/office/ss/util/HeaderUtil;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getColumnHeaderIndexByText(Ljava/lang/String;)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v2

    .line 7
    if-ge v0, v2, :cond_0

    .line 8
    .line 9
    mul-int/lit8 v1, v1, 0x1a

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    add-int/lit8 v2, v2, -0x41

    .line 16
    .line 17
    add-int/2addr v1, v2

    .line 18
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    add-int/lit8 v0, v0, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 24
    .line 25
    return v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getColumnHeaderTextByIndex(I)Ljava/lang/String;
    .locals 3

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    :goto_0
    if-ltz p1, :cond_0

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    rem-int/lit8 v2, p1, 0x1a

    .line 11
    .line 12
    int-to-char v2, v2

    .line 13
    add-int/lit8 v2, v2, 0x41

    .line 14
    .line 15
    int-to-char v2, v2

    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    div-int/lit8 p1, p1, 0x1a

    .line 27
    .line 28
    add-int/lit8 p1, p1, -0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isActiveColumn(Lcom/intsig/office/ss/model/baseModel/Sheet;I)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v2, 0x2

    .line 14
    const/4 v3, 0x0

    .line 15
    if-ne v0, v2, :cond_2

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-ne p1, p2, :cond_1

    .line 22
    .line 23
    return v1

    .line 24
    :cond_1
    return v3

    .line 25
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ltz v2, :cond_3

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-gt v0, p2, :cond_4

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    if-lt p1, p2, :cond_4

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-ne p1, p2, :cond_4

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_4
    const/4 v1, 0x0

    .line 66
    :goto_0
    return v1
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public isActiveRow(Lcom/intsig/office/ss/model/baseModel/Sheet;I)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x1

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    return v2

    .line 10
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-ne v0, v2, :cond_2

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-ne p1, p2, :cond_1

    .line 22
    .line 23
    return v2

    .line 24
    :cond_1
    return v1

    .line 25
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    if-ltz v3, :cond_3

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-gt v0, p2, :cond_4

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    if-lt p1, p2, :cond_4

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-ne p1, p2, :cond_4

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_4
    const/4 v2, 0x0

    .line 66
    :goto_0
    return v2
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public Lcom/intsig/office/ss/util/format/DateTimeFormat;
.super Ljava/lang/Object;
.source "DateTimeFormat.java"


# static fields
.field public static final DATE_FIELD:I = 0x3

.field public static final DAY_OF_WEEK_FIELD:I = 0xe

.field public static final DAY_OF_WEEK_IN_MONTH_FIELD:I = 0xb

.field public static final DAY_OF_YEAR_FIELD:I = 0xa

.field public static final ERA_FIELD:I = 0x0

.field public static final FULL:I = 0x0

.field public static final HOUR0_FIELD:I = 0x10

.field public static final HOUR1_FIELD:I = 0xf

.field public static final HOUR_OF_DAY0_FIELD:I = 0x5

.field public static final HOUR_OF_DAY1_FIELD:I = 0x4

.field public static final LONG:I = 0x1

.field public static final MEDIUM:I = 0x2

.field public static final MILLISECOND_FIELD:I = 0x8

.field public static final MINUTE_FIELD:I = 0x6

.field public static final MONTH_FIELD:I = 0x2

.field public static final SECOND_FIELD:I = 0x7

.field public static final SHORT:I = 0x3

.field public static final TIMEZONE_FIELD:I = 0x11

.field public static final WEEK_OF_MONTH_FIELD:I = 0xd

.field public static final WEEK_OF_YEAR_FIELD:I = 0xc

.field public static final YEAR_FIELD:I = 0x1

.field private static final datePatternChars:Ljava/lang/String; = "GyMdEDFwWazZ"

.field static final patternChars:Ljava/lang/String; = "GyMdkHmsSEDFwWahKzZ"

.field private static final timePatternChars:Ljava/lang/String; = "HhsSkK"


# instance fields
.field private ampm:Z

.field protected calendar:Ljava/util/Calendar;

.field private dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

.field protected numberFormat:Ljava/text/NumberFormat;

.field private pattern:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/ss/util/format/DateTimeFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0

    .line 9
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;-><init>(Ljava/util/Locale;)V

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->adjust(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->validatePattern(Ljava/lang/String;)V

    .line 12
    iput-object p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->pattern:Ljava/lang/String;

    .line 13
    new-instance p1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    invoke-direct {p1, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    return-void
.end method

.method private constructor <init>(Ljava/util/Locale;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 4
    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    const/4 v2, 0x1

    .line 5
    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setParseIntegerOnly(Z)V

    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 7
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    const/16 p1, -0x50

    .line 8
    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->add(II)V

    return-void
.end method

.method private adjust(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 1
    const-string v0, "AM/PM"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const-string v2, "\u4e0a\u5348/\u4e0b\u5348"

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    :cond_0
    const-string v1, ""

    .line 18
    .line 19
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 29
    .line 30
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->isDate(Ljava/lang/String;)Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->isTime(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const/16 v2, 0x4d

    .line 39
    .line 40
    const/16 v3, 0x6d

    .line 41
    .line 42
    if-eqz v1, :cond_5

    .line 43
    .line 44
    if-eqz v0, :cond_5

    .line 45
    .line 46
    const-string v0, "mmm"

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    :goto_0
    const/4 v4, -0x1

    .line 53
    if-le v1, v4, :cond_4

    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    add-int/lit8 v5, v1, 0x3

    .line 60
    .line 61
    :goto_1
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    .line 62
    .line 63
    .line 64
    move-result v6

    .line 65
    if-ne v6, v3, :cond_2

    .line 66
    .line 67
    add-int/lit8 v5, v5, 0x1

    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_2
    :goto_2
    if-ge v1, v5, :cond_3

    .line 71
    .line 72
    aput-char v2, v4, v1

    .line 73
    .line 74
    add-int/lit8 v1, v1, 0x1

    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_3
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    goto :goto_0

    .line 86
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 87
    .line 88
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    .line 90
    .line 91
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    .line 95
    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_5
    if-eqz v0, :cond_6

    .line 99
    .line 100
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    goto :goto_3

    .line 105
    :cond_6
    iget-boolean v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 106
    .line 107
    if-nez v0, :cond_7

    .line 108
    .line 109
    const/16 v0, 0x68

    .line 110
    .line 111
    const/16 v1, 0x6b

    .line 112
    .line 113
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    :cond_7
    :goto_3
    return-object p1
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private append(Ljava/lang/StringBuffer;CI)V
    .locals 11

    .line 1
    const-string v0, "GyMdkHmsSEDFwWahKzZ"

    .line 2
    .line 3
    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/4 v0, -0x1

    .line 8
    if-eq p2, v0, :cond_11

    .line 9
    .line 10
    const/4 v1, 0x4

    .line 11
    const/4 v2, 0x7

    .line 12
    const/4 v3, 0x0

    .line 13
    const/16 v4, 0xc

    .line 14
    .line 15
    const/4 v5, 0x5

    .line 16
    const/16 v6, 0xa

    .line 17
    .line 18
    const/16 v7, 0xb

    .line 19
    .line 20
    const/4 v8, 0x1

    .line 21
    const/4 v9, 0x3

    .line 22
    const/4 v10, 0x2

    .line 23
    packed-switch p2, :pswitch_data_0

    .line 24
    .line 25
    .line 26
    :pswitch_0
    goto/16 :goto_3

    .line 27
    .line 28
    :pswitch_1
    invoke-direct {p0, p1, v3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumericTimeZone(Ljava/lang/StringBuffer;Z)V

    .line 29
    .line 30
    .line 31
    goto/16 :goto_3

    .line 32
    .line 33
    :pswitch_2
    invoke-direct {p0, p1, p3, v8}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendTimeZone(Ljava/lang/StringBuffer;IZ)V

    .line 34
    .line 35
    .line 36
    goto/16 :goto_3

    .line 37
    .line 38
    :pswitch_3
    const/16 v1, 0xa

    .line 39
    .line 40
    goto/16 :goto_4

    .line 41
    .line 42
    :pswitch_4
    iget-boolean p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 43
    .line 44
    if-eqz p2, :cond_1

    .line 45
    .line 46
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 47
    .line 48
    invoke-virtual {p2, v6}, Ljava/util/Calendar;->get(I)I

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    if-nez p2, :cond_0

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    move v4, p2

    .line 56
    :goto_0
    invoke-direct {p0, p1, p3, v4}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 57
    .line 58
    .line 59
    goto/16 :goto_3

    .line 60
    .line 61
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 62
    .line 63
    invoke-virtual {p2, v7}, Ljava/util/Calendar;->get(I)I

    .line 64
    .line 65
    .line 66
    move-result p2

    .line 67
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_3

    .line 71
    .line 72
    :pswitch_5
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 73
    .line 74
    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    if-ne p3, v9, :cond_2

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 81
    .line 82
    iget-object v1, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    aget-object p2, v1, p2

    .line 89
    .line 90
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    goto/16 :goto_3

    .line 94
    .line 95
    :cond_2
    if-le p3, v9, :cond_f

    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 98
    .line 99
    iget-object v1, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 100
    .line 101
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    aget-object p2, v1, p2

    .line 106
    .line 107
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    .line 109
    .line 110
    goto/16 :goto_3

    .line 111
    .line 112
    :pswitch_6
    const/4 v1, 0x3

    .line 113
    goto/16 :goto_4

    .line 114
    .line 115
    :pswitch_7
    const/16 v1, 0x8

    .line 116
    .line 117
    goto/16 :goto_4

    .line 118
    .line 119
    :pswitch_8
    const/4 v1, 0x6

    .line 120
    goto/16 :goto_4

    .line 121
    .line 122
    :pswitch_9
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 123
    .line 124
    const/16 v1, 0xe

    .line 125
    .line 126
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->get(I)I

    .line 127
    .line 128
    .line 129
    move-result p2

    .line 130
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 131
    .line 132
    .line 133
    goto/16 :goto_3

    .line 134
    .line 135
    :pswitch_a
    const/16 v1, 0xd

    .line 136
    .line 137
    goto/16 :goto_4

    .line 138
    .line 139
    :pswitch_b
    if-eq p3, v9, :cond_6

    .line 140
    .line 141
    if-le p3, v5, :cond_3

    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_3
    if-ne p3, v1, :cond_4

    .line 145
    .line 146
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 147
    .line 148
    iget-object p2, p2, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->stdMonths:[Ljava/lang/String;

    .line 149
    .line 150
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 151
    .line 152
    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    aget-object p2, p2, v1

    .line 157
    .line 158
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    .line 160
    .line 161
    goto/16 :goto_3

    .line 162
    .line 163
    :cond_4
    if-ne p3, v5, :cond_5

    .line 164
    .line 165
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 166
    .line 167
    iget-object p2, p2, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->stdShortestMonths:[Ljava/lang/String;

    .line 168
    .line 169
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 170
    .line 171
    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    aget-object p2, p2, v1

    .line 176
    .line 177
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    .line 179
    .line 180
    goto/16 :goto_3

    .line 181
    .line 182
    :cond_5
    const/16 v1, 0xc

    .line 183
    .line 184
    goto/16 :goto_4

    .line 185
    .line 186
    :cond_6
    :goto_1
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 187
    .line 188
    iget-object p2, p2, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->stdShortMonths:[Ljava/lang/String;

    .line 189
    .line 190
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 191
    .line 192
    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    aget-object p2, p2, v1

    .line 197
    .line 198
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 199
    .line 200
    .line 201
    goto/16 :goto_3

    .line 202
    .line 203
    :pswitch_c
    iget-boolean p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 204
    .line 205
    if-eqz p2, :cond_8

    .line 206
    .line 207
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 208
    .line 209
    invoke-virtual {p2, v6}, Ljava/util/Calendar;->get(I)I

    .line 210
    .line 211
    .line 212
    move-result p2

    .line 213
    if-nez p2, :cond_7

    .line 214
    .line 215
    goto :goto_2

    .line 216
    :cond_7
    move v4, p2

    .line 217
    :goto_2
    invoke-direct {p0, p1, p3, v4}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 218
    .line 219
    .line 220
    goto/16 :goto_3

    .line 221
    .line 222
    :cond_8
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 223
    .line 224
    invoke-virtual {p2, v7}, Ljava/util/Calendar;->get(I)I

    .line 225
    .line 226
    .line 227
    move-result p2

    .line 228
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 229
    .line 230
    .line 231
    goto/16 :goto_3

    .line 232
    .line 233
    :pswitch_d
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 234
    .line 235
    invoke-virtual {p2, v7}, Ljava/util/Calendar;->get(I)I

    .line 236
    .line 237
    .line 238
    move-result p2

    .line 239
    if-nez p2, :cond_9

    .line 240
    .line 241
    const/16 p2, 0x18

    .line 242
    .line 243
    :cond_9
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 244
    .line 245
    .line 246
    goto/16 :goto_3

    .line 247
    .line 248
    :pswitch_e
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 249
    .line 250
    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    .line 251
    .line 252
    .line 253
    move-result p2

    .line 254
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 255
    .line 256
    iget-object v2, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->stdShortWeekdays:[Ljava/lang/String;

    .line 257
    .line 258
    array-length v3, v2

    .line 259
    if-ge p2, v3, :cond_f

    .line 260
    .line 261
    if-ne p3, v9, :cond_a

    .line 262
    .line 263
    aget-object p2, v2, p2

    .line 264
    .line 265
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 266
    .line 267
    .line 268
    goto :goto_3

    .line 269
    :cond_a
    if-le p3, v9, :cond_b

    .line 270
    .line 271
    iget-object v1, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->stdWeekdays:[Ljava/lang/String;

    .line 272
    .line 273
    aget-object p2, v1, p2

    .line 274
    .line 275
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    .line 277
    .line 278
    goto :goto_3

    .line 279
    :cond_b
    const/4 v1, 0x5

    .line 280
    goto :goto_4

    .line 281
    :pswitch_f
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 282
    .line 283
    invoke-virtual {p2, v10}, Ljava/util/Calendar;->get(I)I

    .line 284
    .line 285
    .line 286
    move-result p2

    .line 287
    if-gt p3, v10, :cond_c

    .line 288
    .line 289
    add-int/2addr p2, v8

    .line 290
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 291
    .line 292
    .line 293
    goto :goto_3

    .line 294
    :cond_c
    if-ne p3, v9, :cond_d

    .line 295
    .line 296
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 297
    .line 298
    iget-object v1, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 299
    .line 300
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    .line 301
    .line 302
    .line 303
    move-result-object v1

    .line 304
    aget-object p2, v1, p2

    .line 305
    .line 306
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 307
    .line 308
    .line 309
    goto :goto_3

    .line 310
    :cond_d
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 311
    .line 312
    iget-object v1, v1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 313
    .line 314
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v1

    .line 318
    aget-object p2, v1, p2

    .line 319
    .line 320
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    .line 322
    .line 323
    goto :goto_3

    .line 324
    :pswitch_10
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 325
    .line 326
    invoke-virtual {p2, v8}, Ljava/util/Calendar;->get(I)I

    .line 327
    .line 328
    .line 329
    move-result p2

    .line 330
    if-ne p3, v10, :cond_e

    .line 331
    .line 332
    rem-int/lit8 p2, p2, 0x64

    .line 333
    .line 334
    invoke-direct {p0, p1, v10, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 335
    .line 336
    .line 337
    goto :goto_3

    .line 338
    :cond_e
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 339
    .line 340
    .line 341
    goto :goto_3

    .line 342
    :pswitch_11
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 343
    .line 344
    iget-object p2, p2, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 345
    .line 346
    invoke-virtual {p2}, Ljava/text/DateFormatSymbols;->getEras()[Ljava/lang/String;

    .line 347
    .line 348
    .line 349
    move-result-object p2

    .line 350
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 351
    .line 352
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    .line 353
    .line 354
    .line 355
    move-result v1

    .line 356
    aget-object p2, p2, v1

    .line 357
    .line 358
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 359
    .line 360
    .line 361
    :cond_f
    :goto_3
    const/4 v1, -0x1

    .line 362
    :goto_4
    :pswitch_12
    if-eq v1, v0, :cond_10

    .line 363
    .line 364
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 365
    .line 366
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->get(I)I

    .line 367
    .line 368
    .line 369
    move-result p2

    .line 370
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 371
    .line 372
    .line 373
    :cond_10
    return-void

    .line 374
    :cond_11
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 375
    .line 376
    const-string p2, "invalidate char"

    .line 377
    .line 378
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 379
    .line 380
    .line 381
    throw p1

    .line 382
    nop

    .line 383
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_12
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private appendNumber(Ljava/lang/StringBuffer;II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/text/NumberFormat;->getMinimumIntegerDigits()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 8
    .line 9
    invoke-virtual {v1, p2}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 10
    .line 11
    .line 12
    iget-object p2, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 13
    .line 14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 15
    .line 16
    .line 17
    move-result-object p3

    .line 18
    new-instance v1, Ljava/text/FieldPosition;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p2, p3, p1, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private appendNumericTimeZone(Ljava/lang/StringBuffer;Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 2
    .line 3
    const/16 v1, 0xf

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 10
    .line 11
    const/16 v2, 0x10

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    add-int/2addr v0, v1

    .line 18
    if-gez v0, :cond_0

    .line 19
    .line 20
    neg-int v0, v0

    .line 21
    const/16 v1, 0x2d

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/16 v1, 0x2b

    .line 25
    .line 26
    :goto_0
    if-eqz p2, :cond_1

    .line 27
    .line 28
    const-string v2, "GMT"

    .line 29
    .line 30
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    .line 32
    .line 33
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 34
    .line 35
    .line 36
    const v1, 0x36ee80

    .line 37
    .line 38
    .line 39
    div-int v2, v0, v1

    .line 40
    .line 41
    const/4 v3, 0x2

    .line 42
    invoke-direct {p0, p1, v3, v2}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 43
    .line 44
    .line 45
    if-eqz p2, :cond_2

    .line 46
    .line 47
    const/16 p2, 0x3a

    .line 48
    .line 49
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 50
    .line 51
    .line 52
    :cond_2
    rem-int/2addr v0, v1

    .line 53
    const p2, 0xea60

    .line 54
    .line 55
    .line 56
    div-int/2addr v0, p2

    .line 57
    invoke-direct {p0, p1, v3, v0}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private appendTimeZone(Ljava/lang/StringBuffer;IZ)V
    .locals 4

    .line 1
    if-eqz p3, :cond_2

    .line 2
    .line 3
    iget-object p3, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 4
    .line 5
    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    .line 6
    .line 7
    .line 8
    move-result-object p3

    .line 9
    iget-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 10
    .line 11
    const/16 v1, 0x10

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x1

    .line 18
    const/4 v2, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    const/4 v3, 0x4

    .line 25
    if-ge p2, v3, :cond_1

    .line 26
    .line 27
    const/4 v1, 0x0

    .line 28
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    invoke-virtual {p3, v0, v1, p2}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->appendNumericTimeZone(Ljava/lang/StringBuffer;Z)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private formatImpl(Ljava/util/Date;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->pattern:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const/4 v0, 0x0

    .line 13
    const/4 v1, -0x1

    .line 14
    const/4 v2, 0x0

    .line 15
    const/4 v3, 0x0

    .line 16
    const/4 v4, -0x1

    .line 17
    const/4 v5, 0x0

    .line 18
    :goto_0
    if-ge v2, p1, :cond_9

    .line 19
    .line 20
    iget-object v6, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->pattern:Ljava/lang/String;

    .line 21
    .line 22
    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    const/16 v7, 0x27

    .line 27
    .line 28
    if-ne v6, v7, :cond_2

    .line 29
    .line 30
    if-lez v3, :cond_0

    .line 31
    .line 32
    int-to-char v8, v4

    .line 33
    invoke-direct {p0, p2, v8, v3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->append(Ljava/lang/StringBuffer;CI)V

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    :cond_0
    if-ne v4, v6, :cond_1

    .line 38
    .line 39
    invoke-virtual {p2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 40
    .line 41
    .line 42
    const/4 v4, -0x1

    .line 43
    goto :goto_1

    .line 44
    :cond_1
    move v4, v6

    .line 45
    :goto_1
    xor-int/lit8 v5, v5, 0x1

    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_2
    if-nez v5, :cond_7

    .line 49
    .line 50
    if-eq v4, v6, :cond_4

    .line 51
    .line 52
    const/16 v7, 0x61

    .line 53
    .line 54
    if-lt v6, v7, :cond_3

    .line 55
    .line 56
    const/16 v7, 0x7a

    .line 57
    .line 58
    if-le v6, v7, :cond_4

    .line 59
    .line 60
    :cond_3
    const/16 v7, 0x41

    .line 61
    .line 62
    if-lt v6, v7, :cond_7

    .line 63
    .line 64
    const/16 v7, 0x5a

    .line 65
    .line 66
    if-gt v6, v7, :cond_7

    .line 67
    .line 68
    :cond_4
    if-ne v4, v6, :cond_5

    .line 69
    .line 70
    add-int/lit8 v3, v3, 0x1

    .line 71
    .line 72
    goto :goto_2

    .line 73
    :cond_5
    if-lez v3, :cond_6

    .line 74
    .line 75
    int-to-char v4, v4

    .line 76
    invoke-direct {p0, p2, v4, v3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->append(Ljava/lang/StringBuffer;CI)V

    .line 77
    .line 78
    .line 79
    :cond_6
    const/4 v3, 0x1

    .line 80
    move v4, v6

    .line 81
    goto :goto_2

    .line 82
    :cond_7
    if-lez v3, :cond_8

    .line 83
    .line 84
    int-to-char v4, v4

    .line 85
    invoke-direct {p0, p2, v4, v3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->append(Ljava/lang/StringBuffer;CI)V

    .line 86
    .line 87
    .line 88
    const/4 v3, 0x0

    .line 89
    :cond_8
    int-to-char v4, v6

    .line 90
    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    const/4 v4, -0x1

    .line 94
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_9
    if-lez v3, :cond_a

    .line 98
    .line 99
    int-to-char p1, v4

    .line 100
    invoke-direct {p0, p2, p1, v3}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->append(Ljava/lang/StringBuffer;CI)V

    .line 101
    .line 102
    .line 103
    :cond_a
    iget-boolean p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->ampm:Z

    .line 104
    .line 105
    if-eqz p1, :cond_b

    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 108
    .line 109
    iget-object p1, p1, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->formatData:Ljava/text/DateFormatSymbols;

    .line 110
    .line 111
    invoke-virtual {p1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    iget-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 116
    .line 117
    const/16 v1, 0x9

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    aget-object p1, p1, v0

    .line 124
    .line 125
    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    .line 127
    .line 128
    :cond_b
    return-object p2
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private isDate(Ljava/lang/String;)Z
    .locals 4

    .line 1
    const-string v0, "AM"

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    const-string v0, "PM"

    .line 9
    .line 10
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const/4 v0, 0x0

    .line 15
    const/4 v1, 0x0

    .line 16
    :goto_0
    const/16 v2, 0xc

    .line 17
    .line 18
    if-ge v1, v2, :cond_1

    .line 19
    .line 20
    const-string v2, "GyMdEDFwWazZ"

    .line 21
    .line 22
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    const/4 v3, -0x1

    .line 31
    if-le v2, v3, :cond_0

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    goto :goto_1

    .line 35
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    :goto_1
    return v0
.end method

.method public static isDateTimeFormat(Ljava/lang/String;)Z
    .locals 4

    .line 1
    const-string v0, "E+"

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    const/4 v0, 0x0

    .line 10
    const/4 v1, 0x0

    .line 11
    :goto_0
    const/16 v2, 0x13

    .line 12
    .line 13
    if-ge v1, v2, :cond_1

    .line 14
    .line 15
    const-string v2, "GyMdkHmsSEDFwWahKzZ"

    .line 16
    .line 17
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    const/4 v3, -0x1

    .line 26
    if-le v2, v3, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    goto :goto_1

    .line 30
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    :goto_1
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isTime(Ljava/lang/String;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    const/4 v2, 0x6

    .line 4
    if-ge v1, v2, :cond_1

    .line 5
    .line 6
    const-string v2, "HhsSkK"

    .line 7
    .line 8
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const/4 v3, -0x1

    .line 17
    if-le v2, v3, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    :goto_1
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private validateFormat(C)V
    .locals 1

    .line 1
    const-string v0, "GyMdkHmsSEDFwWahKzZ"

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/4 v0, -0x1

    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 12
    .line 13
    const-string v0, "invalidate char"

    .line 14
    .line 15
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    throw p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private validatePattern(Ljava/lang/String;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, -0x1

    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, -0x1

    .line 10
    const/4 v6, 0x0

    .line 11
    :goto_0
    if-ge v3, v0, :cond_9

    .line 12
    .line 13
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    .line 14
    .line 15
    .line 16
    move-result v7

    .line 17
    const/16 v8, 0x27

    .line 18
    .line 19
    if-ne v7, v8, :cond_2

    .line 20
    .line 21
    if-lez v4, :cond_0

    .line 22
    .line 23
    int-to-char v4, v5

    .line 24
    invoke-direct {p0, v4}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->validateFormat(C)V

    .line 25
    .line 26
    .line 27
    const/4 v4, 0x0

    .line 28
    :cond_0
    if-ne v5, v7, :cond_1

    .line 29
    .line 30
    const/4 v5, -0x1

    .line 31
    goto :goto_1

    .line 32
    :cond_1
    move v5, v7

    .line 33
    :goto_1
    xor-int/lit8 v6, v6, 0x1

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_2
    if-nez v6, :cond_7

    .line 37
    .line 38
    if-eq v5, v7, :cond_4

    .line 39
    .line 40
    const/16 v8, 0x61

    .line 41
    .line 42
    if-lt v7, v8, :cond_3

    .line 43
    .line 44
    const/16 v8, 0x7a

    .line 45
    .line 46
    if-le v7, v8, :cond_4

    .line 47
    .line 48
    :cond_3
    const/16 v8, 0x41

    .line 49
    .line 50
    if-lt v7, v8, :cond_7

    .line 51
    .line 52
    const/16 v8, 0x5a

    .line 53
    .line 54
    if-gt v7, v8, :cond_7

    .line 55
    .line 56
    :cond_4
    if-ne v5, v7, :cond_5

    .line 57
    .line 58
    add-int/lit8 v4, v4, 0x1

    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_5
    if-lez v4, :cond_6

    .line 62
    .line 63
    int-to-char v4, v5

    .line 64
    invoke-direct {p0, v4}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->validateFormat(C)V

    .line 65
    .line 66
    .line 67
    :cond_6
    const/4 v4, 0x1

    .line 68
    move v5, v7

    .line 69
    goto :goto_2

    .line 70
    :cond_7
    if-lez v4, :cond_8

    .line 71
    .line 72
    int-to-char v4, v5

    .line 73
    invoke-direct {p0, v4}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->validateFormat(C)V

    .line 74
    .line 75
    .line 76
    const/4 v4, 0x0

    .line 77
    :cond_8
    const/4 v5, -0x1

    .line 78
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_9
    if-lez v4, :cond_a

    .line 82
    .line 83
    int-to-char p1, v5

    .line 84
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->validateFormat(C)V

    .line 85
    .line 86
    .line 87
    :cond_a
    if-nez v6, :cond_b

    .line 88
    .line 89
    return-void

    .line 90
    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 91
    .line 92
    const-string v0, "invalidate pattern"

    .line 93
    .line 94
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    throw p1
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->calendar:Ljava/util/Calendar;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/ss/util/format/DateTimeFormat;->dateTimeFormatData:Lcom/intsig/office/ss/util/format/DateTimeFormatSymbols;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public format(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/ss/util/format/DateTimeFormat;->formatImpl(Ljava/util/Date;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/office/ss/util/format/FractionalFormat;
.super Ljava/text/Format;
.source "FractionalFormat.java"


# instance fields
.field private ONE_DIGIT:S

.field private THREE_DIGIT:S

.field private TWO_DIGIT:S

.field private UNITS:S

.field private mode:S

.field private units:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/text/Format;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-short v0, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->ONE_DIGIT:S

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    iput-short v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->TWO_DIGIT:S

    .line 9
    .line 10
    const/4 v2, 0x3

    .line 11
    iput-short v2, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->THREE_DIGIT:S

    .line 12
    .line 13
    const/4 v2, 0x4

    .line 14
    iput-short v2, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 15
    .line 16
    iput v0, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 17
    .line 18
    const/4 v0, -0x1

    .line 19
    iput-short v0, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 20
    .line 21
    const-string v0, "# ?/?"

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->ONE_DIGIT:S

    .line 30
    .line 31
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 32
    .line 33
    goto/16 :goto_0

    .line 34
    .line 35
    :cond_0
    const-string v0, "# ??/??"

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->TWO_DIGIT:S

    .line 44
    .line 45
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 46
    .line 47
    goto/16 :goto_0

    .line 48
    .line 49
    :cond_1
    const-string v0, "# ???/???"

    .line 50
    .line 51
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_2

    .line 56
    .line 57
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->THREE_DIGIT:S

    .line 58
    .line 59
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    const-string v0, "# ?/2"

    .line 63
    .line 64
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    if-eqz v0, :cond_3

    .line 69
    .line 70
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 71
    .line 72
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 73
    .line 74
    iput v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_3
    const-string v0, "# ?/4"

    .line 78
    .line 79
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-eqz v0, :cond_4

    .line 84
    .line 85
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 86
    .line 87
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 88
    .line 89
    iput v2, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_4
    const-string v0, "# ?/8"

    .line 93
    .line 94
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-eqz v0, :cond_5

    .line 99
    .line 100
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 101
    .line 102
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 103
    .line 104
    const/16 p1, 0x8

    .line 105
    .line 106
    iput p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_5
    const-string v0, "# ??/16"

    .line 110
    .line 111
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    if-eqz v0, :cond_6

    .line 116
    .line 117
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 118
    .line 119
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 120
    .line 121
    const/16 p1, 0x10

    .line 122
    .line 123
    iput p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_6
    const-string v0, "# ?/10"

    .line 127
    .line 128
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result v0

    .line 132
    if-eqz v0, :cond_7

    .line 133
    .line 134
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 135
    .line 136
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 137
    .line 138
    const/16 p1, 0xa

    .line 139
    .line 140
    iput p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 141
    .line 142
    goto :goto_0

    .line 143
    :cond_7
    const-string v0, "# ??/100"

    .line 144
    .line 145
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    if-eqz p1, :cond_8

    .line 150
    .line 151
    iget-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    .line 152
    .line 153
    iput-short p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    .line 154
    .line 155
    const/16 p1, 0x64

    .line 156
    .line 157
    iput p1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    .line 158
    .line 159
    :cond_8
    :goto_0
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private format(DI)Ljava/lang/String;
    .locals 27

    move-wide/from16 v0, p1

    double-to-long v2, v0

    const-wide/16 v4, 0x0

    cmpg-double v6, v0, v4

    if-gez v6, :cond_0

    const/4 v4, -0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x1

    .line 1
    :goto_0
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    long-to-double v5, v2

    sub-double/2addr v0, v5

    const-wide/16 v7, 0x0

    const-wide v9, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpl-double v11, v0, v9

    if-lez v11, :cond_7

    move-wide v11, v0

    move-wide/from16 v17, v7

    move-wide v13, v9

    const-wide/16 v15, 0x1

    move-wide/from16 v9, v17

    :goto_1
    const-wide/high16 v19, 0x3ff0000000000000L    # 1.0

    div-double v19, v19, v11

    add-double v11, v19, v13

    double-to-long v11, v11

    long-to-double v5, v11

    sub-double v5, v19, v5

    cmp-long v19, v9, v7

    if-lez v19, :cond_1

    mul-long v11, v11, v15

    add-long v15, v11, v17

    :cond_1
    move-wide v11, v15

    long-to-double v7, v11

    div-double v17, v7, v0

    const-wide/high16 v21, 0x3fe0000000000000L    # 0.5

    move-wide/from16 v23, v11

    add-double v11, v17, v21

    double-to-long v11, v11

    move-wide/from16 v17, v2

    long-to-double v2, v11

    div-double/2addr v7, v2

    sub-double/2addr v7, v0

    .line 2
    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    move/from16 v7, p3

    move v8, v4

    move-wide/from16 v25, v5

    int-to-long v4, v7

    cmp-long v6, v11, v4

    if-lez v6, :cond_4

    if-lez v19, :cond_2

    long-to-double v2, v9

    div-double v4, v2, v0

    add-double v4, v4, v21

    double-to-long v4, v4

    long-to-double v6, v4

    div-double/2addr v2, v6

    sub-double/2addr v2, v0

    .line 3
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-wide v11, v4

    goto :goto_3

    :cond_2
    const-wide/16 v2, 0x1

    long-to-double v6, v2

    long-to-double v2, v4

    div-double/2addr v6, v2

    sub-double/2addr v6, v0

    .line 4
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v6, v2, v0

    if-lez v6, :cond_3

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x1

    goto :goto_3

    :cond_3
    move-wide v11, v4

    const-wide/16 v9, 0x1

    goto :goto_3

    :cond_4
    const-wide v4, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v6, v2, v4

    if-lez v6, :cond_6

    cmpg-double v6, v25, v13

    if-gez v6, :cond_5

    goto :goto_2

    :cond_5
    div-double v13, v4, v2

    move v4, v8

    move-wide/from16 v2, v17

    move-wide/from16 v15, v23

    move-wide/from16 v11, v25

    const-wide/16 v7, 0x0

    move-wide/from16 v17, v9

    move-wide v9, v15

    goto :goto_1

    :cond_6
    :goto_2
    move-wide/from16 v9, v23

    goto :goto_3

    :cond_7
    move-wide/from16 v17, v2

    move v8, v4

    const-wide/16 v9, 0x1

    const-wide/16 v11, 0x0

    :goto_3
    cmp-long v0, v9, v11

    if-nez v0, :cond_8

    const-wide/16 v0, 0x1

    add-long v2, v17, v0

    const-wide/16 v0, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    goto :goto_4

    :cond_8
    const-wide/16 v0, 0x0

    cmp-long v2, v11, v0

    if-nez v2, :cond_9

    move-wide v9, v0

    :cond_9
    move-wide/from16 v2, v17

    :goto_4
    if-gez v8, :cond_b

    cmp-long v4, v2, v0

    if-nez v4, :cond_a

    neg-long v9, v9

    goto :goto_5

    :cond_a
    neg-long v2, v2

    :cond_b
    :goto_5
    const-string v4, ""

    cmp-long v5, v2, v0

    if-eqz v5, :cond_c

    .line 5
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_c
    cmp-long v2, v9, v0

    if-eqz v2, :cond_d

    cmp-long v2, v11, v0

    if-eqz v2, :cond_d

    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_d
    return-object v4
.end method

.method private formatUnit(DI)Ljava/lang/String;
    .locals 6

    .line 1
    double-to-long v0, p1

    .line 2
    long-to-double v2, v0

    .line 3
    sub-double/2addr p1, v2

    .line 4
    int-to-double v2, p3

    .line 5
    mul-double p1, p1, v2

    .line 6
    .line 7
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    .line 8
    .line 9
    .line 10
    move-result-wide p1

    .line 11
    const-string v2, ""

    .line 12
    .line 13
    const-wide/16 v3, 0x0

    .line 14
    .line 15
    cmp-long v5, v0, v3

    .line 16
    .line 17
    if-eqz v5, :cond_0

    .line 18
    .line 19
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    :cond_0
    cmp-long v0, p1, v3

    .line 28
    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, " "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string p1, "/"

    .line 45
    .line 46
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    :cond_1
    return-object v2
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final format(D)Ljava/lang/String;
    .locals 2

    .line 7
    iget-short v0, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->mode:S

    iget-short v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->ONE_DIGIT:S

    if-ne v0, v1, :cond_0

    const/16 v0, 0x9

    .line 8
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/ss/util/format/FractionalFormat;->format(DI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 9
    :cond_0
    iget-short v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->TWO_DIGIT:S

    if-ne v0, v1, :cond_1

    const/16 v0, 0x63

    .line 10
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/ss/util/format/FractionalFormat;->format(DI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 11
    :cond_1
    iget-short v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->THREE_DIGIT:S

    if-ne v0, v1, :cond_2

    const/16 v0, 0x3e7

    .line 12
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/ss/util/format/FractionalFormat;->format(DI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 13
    :cond_2
    iget-short v1, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->UNITS:S

    if-ne v0, v1, :cond_3

    .line 14
    iget v0, p0, Lcom/intsig/office/ss/util/format/FractionalFormat;->units:I

    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/ss/util/format/FractionalFormat;->formatUnit(DI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 15
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Unexpected Case"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2

    .line 16
    instance-of p3, p1, Ljava/lang/Number;

    if-eqz p3, :cond_0

    .line 17
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/ss/util/format/FractionalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-object p2

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Can only handle Numbers"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public parseObject(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

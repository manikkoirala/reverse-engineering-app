.class public Lcom/intsig/office/ss/other/FocusCell;
.super Ljava/lang/Object;
.source "FocusCell.java"


# static fields
.field public static final CELL:S = 0x3s

.field public static final COLUMNHEADER:S = 0x2s

.field public static final ROWHEADER:S = 0x1s

.field public static final UNKNOWN:S


# instance fields
.field private area:Landroid/graphics/Rect;

.field private col:I

.field private headerType:S

.field private row:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    return-void
.end method

.method public constructor <init>(SLandroid/graphics/Rect;II)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-short p1, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 5
    iput-object p2, p0, Lcom/intsig/office/ss/other/FocusCell;->area:Landroid/graphics/Rect;

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 6
    iput p3, p0, Lcom/intsig/office/ss/other/FocusCell;->row:I

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    if-ne p1, p2, :cond_1

    .line 7
    iput p4, p0, Lcom/intsig/office/ss/other/FocusCell;->col:I

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public clone()Lcom/intsig/office/ss/other/FocusCell;
    .locals 5

    .line 2
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/intsig/office/ss/other/FocusCell;->area:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 3
    new-instance v1, Lcom/intsig/office/ss/other/FocusCell;

    iget-short v2, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    iget v3, p0, Lcom/intsig/office/ss/other/FocusCell;->row:I

    iget v4, p0, Lcom/intsig/office/ss/other/FocusCell;->col:I

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/intsig/office/ss/other/FocusCell;-><init>(SLandroid/graphics/Rect;II)V

    return-object v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/other/FocusCell;->clone()Lcom/intsig/office/ss/other/FocusCell;

    move-result-object v0

    return-object v0
.end method

.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColumn()I
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v1, 0x3

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, -0x1

    .line 11
    return v0

    .line 12
    :cond_1
    :goto_0
    iget v0, p0, Lcom/intsig/office/ss/other/FocusCell;->col:I

    .line 13
    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/other/FocusCell;->area:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRow()I
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v1, 0x3

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, -0x1

    .line 11
    return v0

    .line 12
    :cond_1
    :goto_0
    iget v0, p0, Lcom/intsig/office/ss/other/FocusCell;->row:I

    .line 13
    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()I
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setColumn(I)V
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v1, 0x3

    .line 7
    if-ne v0, v1, :cond_1

    .line 8
    .line 9
    :cond_0
    iput p1, p0, Lcom/intsig/office/ss/other/FocusCell;->col:I

    .line 10
    .line 11
    :cond_1
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/other/FocusCell;->area:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRow(I)V
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v1, 0x3

    .line 7
    if-ne v0, v1, :cond_1

    .line 8
    .line 9
    :cond_0
    iput p1, p0, Lcom/intsig/office/ss/other/FocusCell;->row:I

    .line 10
    .line 11
    :cond_1
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setType(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/other/FocusCell;->headerType:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

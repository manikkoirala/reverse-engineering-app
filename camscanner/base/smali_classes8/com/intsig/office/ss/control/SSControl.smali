.class public Lcom/intsig/office/ss/control/SSControl;
.super Lcom/intsig/office/system/AbstractControl;
.source "SSControl.java"


# instance fields
.field private excelView:Lcom/intsig/office/ss/control/ExcelView;

.field private isDispose:Z

.field private mainControl:Lcom/intsig/office/system/IControl;

.field private spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Lcom/intsig/office/ss/model/baseModel/Workbook;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractControl;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/ss/control/ExcelView;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-direct {p1, v0, p3, p2, p0}, Lcom/intsig/office/ss/control/ExcelView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/system/IControl;)V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/ExcelView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method static bridge synthetic O8(Lcom/intsig/office/ss/control/SSControl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSControl;->updateStatus()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private exportImage()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/ss/control/SSControl$6;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/office/ss/control/SSControl$6;-><init>(Lcom/intsig/office/ss/control/SSControl;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private updateStatus()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/ss/control/SSControl;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/ss/control/SSControl;->isDispose:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/ss/control/SSControl;)Lcom/intsig/office/system/IControl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/ss/control/SSControl;)Lcom/intsig/office/ss/control/Spreadsheet;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public actionEvent(ILjava/lang/Object;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    sparse-switch p1, :sswitch_data_0

    .line 5
    .line 6
    .line 7
    goto/16 :goto_2

    .line 8
    .line 9
    :sswitch_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/ExcelView;->removeSheetBar()V

    .line 12
    .line 13
    .line 14
    goto/16 :goto_2

    .line 15
    .line 16
    :sswitch_1
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 17
    .line 18
    check-cast p2, Ljava/lang/Integer;

    .line 19
    .line 20
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/control/ExcelView;->showSheet(I)V

    .line 25
    .line 26
    .line 27
    goto/16 :goto_2

    .line 28
    .line 29
    :sswitch_2
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->initCalloutView()V

    .line 32
    .line 33
    .line 34
    goto/16 :goto_2

    .line 35
    .line 36
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getEventManage()Lcom/intsig/office/system/beans/AEventManage;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    if-eqz p1, :cond_6

    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getEventManage()Lcom/intsig/office/system/beans/AEventManage;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 51
    .line 52
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    add-int/lit8 p2, p2, -0xa

    .line 57
    .line 58
    int-to-float p2, p2

    .line 59
    invoke-virtual {p1, v2, v2, v0, p2}, Lcom/intsig/office/system/beans/AEventManage;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSControl;->exportImage()V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 66
    .line 67
    new-instance p2, Lcom/intsig/office/ss/control/SSControl$5;

    .line 68
    .line 69
    invoke-direct {p2, p0}, Lcom/intsig/office/ss/control/SSControl$5;-><init>(Lcom/intsig/office/ss/control/SSControl;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 73
    .line 74
    .line 75
    goto/16 :goto_2

    .line 76
    .line 77
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getEventManage()Lcom/intsig/office/system/beans/AEventManage;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    if-eqz p1, :cond_6

    .line 84
    .line 85
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getEventManage()Lcom/intsig/office/system/beans/AEventManage;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 92
    .line 93
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 94
    .line 95
    .line 96
    move-result p2

    .line 97
    neg-int p2, p2

    .line 98
    add-int/lit8 p2, p2, 0xa

    .line 99
    .line 100
    int-to-float p2, p2

    .line 101
    invoke-virtual {p1, v2, v2, v0, p2}, Lcom/intsig/office/system/beans/AEventManage;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 102
    .line 103
    .line 104
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSControl;->exportImage()V

    .line 105
    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 108
    .line 109
    new-instance p2, Lcom/intsig/office/ss/control/SSControl$4;

    .line 110
    .line 111
    invoke-direct {p2, p0}, Lcom/intsig/office/ss/control/SSControl$4;-><init>(Lcom/intsig/office/ss/control/SSControl;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p1, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 115
    .line 116
    .line 117
    goto/16 :goto_2

    .line 118
    .line 119
    :sswitch_5
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSControl;->exportImage()V

    .line 120
    .line 121
    .line 122
    goto/16 :goto_2

    .line 123
    .line 124
    :sswitch_6
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 125
    .line 126
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getReader()Lcom/intsig/office/system/IReader;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    if-eqz p1, :cond_6

    .line 131
    .line 132
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 133
    .line 134
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getReader()Lcom/intsig/office/system/IReader;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-interface {p1}, Lcom/intsig/office/system/IReader;->abortReader()V

    .line 139
    .line 140
    .line 141
    goto/16 :goto_2

    .line 142
    .line 143
    :sswitch_7
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 144
    .line 145
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getActiveCellHyperlink()Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 146
    .line 147
    .line 148
    move-result-object p1

    .line 149
    if-eqz p1, :cond_6

    .line 150
    .line 151
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getLinkType()I

    .line 152
    .line 153
    .line 154
    move-result p2

    .line 155
    const/4 v0, 0x2

    .line 156
    const/4 v3, 0x1

    .line 157
    if-ne p2, v0, :cond_2

    .line 158
    .line 159
    invoke-virtual {p1}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getAddress()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object p1

    .line 163
    const-string p2, "!"

    .line 164
    .line 165
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 166
    .line 167
    .line 168
    move-result p2

    .line 169
    invoke-virtual {p1, v1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    const-string v4, "\'"

    .line 174
    .line 175
    const-string v5, ""

    .line 176
    .line 177
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    add-int/2addr p2, v3

    .line 182
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 183
    .line 184
    .line 185
    move-result v3

    .line 186
    invoke-virtual {p1, p2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object p1

    .line 190
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 191
    .line 192
    .line 193
    move-result-object p2

    .line 194
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/util/ReferenceUtil;->getRowIndex(Ljava/lang/String;)I

    .line 195
    .line 196
    .line 197
    move-result p2

    .line 198
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 199
    .line 200
    .line 201
    move-result-object v3

    .line 202
    invoke-virtual {v3, p1}, Lcom/intsig/office/ss/util/ReferenceUtil;->getColumnIndex(Ljava/lang/String;)I

    .line 203
    .line 204
    .line 205
    move-result p1

    .line 206
    iget-object v3, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 207
    .line 208
    invoke-virtual {v3}, Lcom/intsig/office/ss/control/Spreadsheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 209
    .line 210
    .line 211
    move-result-object v3

    .line 212
    invoke-virtual {v3, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 213
    .line 214
    .line 215
    move-result-object v3

    .line 216
    invoke-virtual {v3, p2, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellRowCol(II)V

    .line 217
    .line 218
    .line 219
    iget-object v3, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 220
    .line 221
    invoke-virtual {v3, v0}, Lcom/intsig/office/ss/control/ExcelView;->showSheet(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    add-int/lit8 p2, p2, -0x1

    .line 225
    .line 226
    add-int/lit8 p1, p1, -0x1

    .line 227
    .line 228
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 229
    .line 230
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 231
    .line 232
    .line 233
    move-result-object v0

    .line 234
    if-ltz p2, :cond_0

    .line 235
    .line 236
    goto :goto_0

    .line 237
    :cond_0
    const/4 p2, 0x0

    .line 238
    :goto_0
    if-ltz p1, :cond_1

    .line 239
    .line 240
    move v1, p1

    .line 241
    :cond_1
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/ss/view/SheetView;->goToCell(II)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    const/16 p2, 0x14

    .line 249
    .line 250
    invoke-interface {p1, p2, v2}, Lcom/intsig/office/system/IMainFrame;->doActionEvent(ILjava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 254
    .line 255
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 256
    .line 257
    .line 258
    goto/16 :goto_2

    .line 259
    .line 260
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getLinkType()I

    .line 261
    .line 262
    .line 263
    move-result p2

    .line 264
    const/4 v0, 0x3

    .line 265
    if-eq p2, v0, :cond_4

    .line 266
    .line 267
    invoke-virtual {p1}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getLinkType()I

    .line 268
    .line 269
    .line 270
    move-result p2

    .line 271
    if-ne p2, v3, :cond_3

    .line 272
    .line 273
    goto :goto_1

    .line 274
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 275
    .line 276
    const-string p2, "not supported hyperlink!"

    .line 277
    .line 278
    const/16 v0, 0x11

    .line 279
    .line 280
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 281
    .line 282
    .line 283
    goto/16 :goto_2

    .line 284
    .line 285
    :cond_4
    :goto_1
    new-instance p2, Landroid/content/Intent;

    .line 286
    .line 287
    const-string v0, "android.intent.action.VIEW"

    .line 288
    .line 289
    invoke-virtual {p1}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getAddress()Ljava/lang/String;

    .line 290
    .line 291
    .line 292
    move-result-object p1

    .line 293
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 294
    .line 295
    .line 296
    move-result-object p1

    .line 297
    invoke-direct {p2, v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 301
    .line 302
    .line 303
    move-result-object p1

    .line 304
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 305
    .line 306
    .line 307
    move-result-object p1

    .line 308
    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    .line 310
    .line 311
    goto/16 :goto_2

    .line 312
    .line 313
    :sswitch_8
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSControl;->updateStatus()V

    .line 314
    .line 315
    .line 316
    goto/16 :goto_2

    .line 317
    .line 318
    :sswitch_9
    check-cast p2, [I

    .line 319
    .line 320
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 321
    .line 322
    aget p2, p2, v1

    .line 323
    .line 324
    int-to-float p2, p2

    .line 325
    const v0, 0x461c4000    # 10000.0f

    .line 326
    .line 327
    .line 328
    div-float/2addr p2, v0

    .line 329
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/control/Spreadsheet;->setZoom(F)V

    .line 330
    .line 331
    .line 332
    goto/16 :goto_2

    .line 333
    .line 334
    :sswitch_a
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 335
    .line 336
    .line 337
    move-result-object p1

    .line 338
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 339
    .line 340
    invoke-virtual {p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getActiveCellContent()Ljava/lang/String;

    .line 341
    .line 342
    .line 343
    move-result-object p2

    .line 344
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 345
    .line 346
    .line 347
    move-result-object v0

    .line 348
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 349
    .line 350
    .line 351
    move-result-object v0

    .line 352
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/system/SysKit;->internetSearch(Ljava/lang/String;Landroid/app/Activity;)V

    .line 353
    .line 354
    .line 355
    goto :goto_2

    .line 356
    :sswitch_b
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 357
    .line 358
    .line 359
    move-result-object p1

    .line 360
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 361
    .line 362
    .line 363
    move-result-object p1

    .line 364
    const-string p2, "clipboard"

    .line 365
    .line 366
    invoke-virtual {p1, p2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 367
    .line 368
    .line 369
    move-result-object p1

    .line 370
    check-cast p1, Landroid/text/ClipboardManager;

    .line 371
    .line 372
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 373
    .line 374
    invoke-virtual {p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getActiveCellContent()Ljava/lang/String;

    .line 375
    .line 376
    .line 377
    move-result-object p2

    .line 378
    invoke-virtual {p1, p2}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 379
    .line 380
    .line 381
    goto :goto_2

    .line 382
    :sswitch_c
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 383
    .line 384
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 385
    .line 386
    .line 387
    move-result-object p1

    .line 388
    if-eqz p1, :cond_5

    .line 389
    .line 390
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 391
    .line 392
    new-instance v0, Lcom/intsig/office/ss/control/SSControl$2;

    .line 393
    .line 394
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/ss/control/SSControl$2;-><init>(Lcom/intsig/office/ss/control/SSControl;Ljava/lang/Object;)V

    .line 395
    .line 396
    .line 397
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 398
    .line 399
    .line 400
    goto :goto_2

    .line 401
    :cond_5
    new-instance p1, Lcom/intsig/office/ss/control/SSControl$3;

    .line 402
    .line 403
    invoke-direct {p1, p0, p2}, Lcom/intsig/office/ss/control/SSControl$3;-><init>(Lcom/intsig/office/ss/control/SSControl;Ljava/lang/Object;)V

    .line 404
    .line 405
    .line 406
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 407
    .line 408
    .line 409
    goto :goto_2

    .line 410
    :sswitch_d
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 411
    .line 412
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 413
    .line 414
    .line 415
    move-result-object p1

    .line 416
    if-eqz p1, :cond_6

    .line 417
    .line 418
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 419
    .line 420
    new-instance v0, Lcom/intsig/office/ss/control/SSControl$1;

    .line 421
    .line 422
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/ss/control/SSControl$1;-><init>(Lcom/intsig/office/ss/control/SSControl;Ljava/lang/Object;)V

    .line 423
    .line 424
    .line 425
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 426
    .line 427
    .line 428
    goto :goto_2

    .line 429
    :sswitch_e
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 430
    .line 431
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->isAutoTest()Z

    .line 432
    .line 433
    .line 434
    move-result p1

    .line 435
    if-eqz p1, :cond_6

    .line 436
    .line 437
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/SSControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 438
    .line 439
    .line 440
    move-result-object p1

    .line 441
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 442
    .line 443
    .line 444
    move-result-object p1

    .line 445
    invoke-virtual {p1}, Landroid/app/Activity;->onBackPressed()V

    .line 446
    .line 447
    .line 448
    goto :goto_2

    .line 449
    :sswitch_f
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 450
    .line 451
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/ExcelView;->init()V

    .line 452
    .line 453
    .line 454
    goto :goto_2

    .line 455
    :sswitch_10
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 456
    .line 457
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 458
    .line 459
    .line 460
    :catch_0
    :cond_6
    :goto_2
    return-void

    .line 461
    :sswitch_data_0
    .sparse-switch
        -0x10000000 -> :sswitch_10
        0x13 -> :sswitch_f
        0x16 -> :sswitch_e
        0x1a -> :sswitch_d
        0x1b -> :sswitch_c
        0x10000002 -> :sswitch_b
        0x20000002 -> :sswitch_a
        0x20000005 -> :sswitch_9
        0x20000007 -> :sswitch_8
        0x20000008 -> :sswitch_7
        0x20000009 -> :sswitch_6
        0x2000000a -> :sswitch_5
        0x2000000d -> :sswitch_4
        0x2000000e -> :sswitch_3
        0x2000001e -> :sswitch_2
        0x40000001 -> :sswitch_1
        0x40000005 -> :sswitch_0
    .end sparse-switch
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public canBackLayout()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSControl;->isDispose:Z

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 10
    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/ExcelView;->dispose()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActionValue(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    const/4 v2, 0x0

    .line 4
    sparse-switch p1, :sswitch_data_0

    .line 5
    .line 6
    .line 7
    goto/16 :goto_1

    .line 8
    .line 9
    :sswitch_0
    check-cast p2, Ljava/lang/Integer;

    .line 10
    .line 11
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    const/4 p2, -0x1

    .line 16
    if-ne p1, p2, :cond_0

    .line 17
    .line 18
    return-object v2

    .line 19
    :cond_0
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    sub-int/2addr p1, v1

    .line 26
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    return-object p1

    .line 37
    :cond_1
    return-object v2

    .line 38
    :sswitch_1
    new-instance p1, Ljava/util/Vector;

    .line 39
    .line 40
    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    .line 41
    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 44
    .line 45
    invoke-virtual {p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheetCount()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    :goto_0
    if-ge v0, v1, :cond_2

    .line 54
    .line 55
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    add-int/lit8 v0, v0, 0x1

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_2
    return-object p1

    .line 70
    :sswitch_2
    instance-of p1, p2, [I

    .line 71
    .line 72
    if-eqz p1, :cond_3

    .line 73
    .line 74
    move-object p1, p2

    .line 75
    check-cast p1, [I

    .line 76
    .line 77
    if-eqz p1, :cond_3

    .line 78
    .line 79
    array-length v3, p1

    .line 80
    const/4 v4, 0x3

    .line 81
    if-ne v3, v4, :cond_3

    .line 82
    .line 83
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 84
    .line 85
    aget v0, p1, v0

    .line 86
    .line 87
    aget v1, p1, v1

    .line 88
    .line 89
    const/4 v2, 0x2

    .line 90
    aget p1, p1, v2

    .line 91
    .line 92
    int-to-float p1, p1

    .line 93
    const v2, 0x461c4000    # 10000.0f

    .line 94
    .line 95
    .line 96
    div-float/2addr p1, v2

    .line 97
    invoke-virtual {p2, v0, v1, p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getThumbnail(IIF)Landroid/graphics/Bitmap;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    return-object p1

    .line 102
    :cond_3
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 103
    .line 104
    if-eqz p1, :cond_4

    .line 105
    .line 106
    check-cast p2, Landroid/graphics/Bitmap;

    .line 107
    .line 108
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getSnapshot(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    return-object p1

    .line 113
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 114
    .line 115
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getCurrentSheetNumber()I

    .line 116
    .line 117
    .line 118
    move-result p1

    .line 119
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    return-object p1

    .line 124
    :sswitch_5
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetCount()I

    .line 127
    .line 128
    .line 129
    move-result p1

    .line 130
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    return-object p1

    .line 135
    :sswitch_6
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 136
    .line 137
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getFitZoom()F

    .line 138
    .line 139
    .line 140
    move-result p1

    .line 141
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    return-object p1

    .line 146
    :sswitch_7
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 147
    .line 148
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getZoom()F

    .line 149
    .line 150
    .line 151
    move-result p1

    .line 152
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    return-object p1

    .line 157
    :cond_4
    :goto_1
    return-object v2

    .line 158
    nop

    .line 159
    :sswitch_data_0
    .sparse-switch
        0x20000005 -> :sswitch_7
        0x20000006 -> :sswitch_6
        0x2000000b -> :sswitch_5
        0x2000000c -> :sswitch_4
        0x20000010 -> :sswitch_2
        0x20000018 -> :sswitch_3
        0x40000002 -> :sswitch_1
        0x40000003 -> :sswitch_0
    .end sparse-switch
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getApplicationType()B
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentViewIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/ExcelView;->getCurrentViewIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCustomDialog()Lcom/intsig/office/common/ICustomDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getCustomDialog()Lcom/intsig/office/common/ICustomDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFind()Lcom/intsig/office/system/IFind;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->spreadSheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainFrame()Lcom/intsig/office/system/IMainFrame;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSysKit()Lcom/intsig/office/system/SysKit;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->excelView:Lcom/intsig/office/ss/control/ExcelView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isAutoTest()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->isAutoTest()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public layoutView(IIII)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public setLayoutThreadDied(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStopDraw(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

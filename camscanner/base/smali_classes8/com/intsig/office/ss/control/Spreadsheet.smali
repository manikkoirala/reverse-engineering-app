.class public Lcom/intsig/office/ss/control/Spreadsheet;
.super Landroid/widget/LinearLayout;
.source "Spreadsheet.java"

# interfaces
.implements Lcom/intsig/office/system/IFind;
.implements Lcom/intsig/office/ss/model/interfacePart/IReaderListener;
.implements Lcom/intsig/office/system/beans/CalloutView/IExportListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Spreadsheet"


# instance fields
.field private abortDrawing:Z

.field private callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

.field private control:Lcom/intsig/office/system/IControl;

.field private currentSheetIndex:I

.field private currentSheetName:Ljava/lang/String;

.field private editor:Lcom/intsig/office/ss/control/SSEditor;

.field private eventManage:Lcom/intsig/office/ss/control/SSEventManage;

.field private fileName:Ljava/lang/String;

.field private initFinish:Z

.field private isConfigurationChanged:Z

.field private isDefaultSheetBar:Z

.field private mIsNeedReDraw:Z

.field private parent:Lcom/intsig/office/ss/control/ExcelView;

.field private preShowSheetIndex:I

.field private sheetbarHeight:I

.field private sheetview:Lcom/intsig/office/ss/view/SheetView;

.field private workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/system/IControl;Lcom/intsig/office/ss/control/ExcelView;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->isDefaultSheetBar:Z

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->preShowSheetIndex:I

    .line 9
    .line 10
    iput-object p5, p0, Lcom/intsig/office/ss/control/Spreadsheet;->parent:Lcom/intsig/office/ss/control/ExcelView;

    .line 11
    .line 12
    iput-object p2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 13
    .line 14
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 15
    .line 16
    .line 17
    iput-object p3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 18
    .line 19
    iput-object p4, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 20
    .line 21
    new-instance p2, Lcom/intsig/office/ss/control/SSEventManage;

    .line 22
    .line 23
    invoke-direct {p2, p0, p4}, Lcom/intsig/office/ss/control/SSEventManage;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/system/IControl;)V

    .line 24
    .line 25
    .line 26
    iput-object p2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 27
    .line 28
    new-instance p2, Lcom/intsig/office/ss/control/SSEditor;

    .line 29
    .line 30
    invoke-direct {p2, p0}, Lcom/intsig/office/ss/control/SSEditor;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;)V

    .line 31
    .line 32
    .line 33
    iput-object p2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->editor:Lcom/intsig/office/ss/control/SSEditor;

    .line 34
    .line 35
    iget-object p2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 36
    .line 37
    invoke-virtual {p0, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, p1}, Landroid/view/View;->setLongClickable(Z)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method static bridge synthetic O8(Lcom/intsig/office/ss/control/Spreadsheet;)Lcom/intsig/office/ss/model/baseModel/Workbook;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private initSheetbar()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private showSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V
    .locals 4

    .line 15
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    invoke-virtual {v0}, Lcom/intsig/office/system/beans/AEventManage;->stopFling()V

    .line 16
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/intsig/office/system/IMainFrame;->setFindBackForwardState(Z)V

    .line 17
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    .line 19
    invoke-interface {v0, v3, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 20
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/SheetView;->changeSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    move-result v0

    const/4 v2, 0x2

    const/16 v3, 0x1a

    if-eq v0, v2, :cond_0

    .line 23
    invoke-virtual {p1, p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setReaderListener(Lcom/intsig/office/ss/model/interfacePart/IReaderListener;)V

    .line 24
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v3, v0}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 25
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    const v0, 0x20000009

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {p1, v3, v0}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 27
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getReaderHandler()Lcom/intsig/office/system/ReaderHandler;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 28
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 29
    iput v1, v0, Landroid/os/Message;->what:I

    .line 30
    iget v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/office/system/ReaderHandler;->handleMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 32
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-void
.end method

.method private toPicture(Lcom/intsig/office/common/IOfficeToPicture;)V
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/picture/PictureKit;->isDrawPictrue()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x1

    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/picture/PictureKit;->setDrawPictrue(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    invoke-interface {p1, v1, v3}, Lcom/intsig/office/common/IOfficeToPicture;->getBitmap(II)Landroid/graphics/Bitmap;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    if-nez v1, :cond_0

    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    new-instance v3, Landroid/graphics/Canvas;

    .line 33
    .line 34
    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 35
    .line 36
    .line 37
    iget-object v4, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 38
    .line 39
    invoke-virtual {v4}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v5

    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 48
    .line 49
    .line 50
    move-result v6

    .line 51
    if-ne v5, v6, :cond_1

    .line 52
    .line 53
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 54
    .line 55
    .line 56
    move-result v5

    .line 57
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 58
    .line 59
    .line 60
    move-result v6

    .line 61
    if-eq v5, v6, :cond_2

    .line 62
    .line 63
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 64
    .line 65
    .line 66
    move-result v5

    .line 67
    int-to-float v5, v5

    .line 68
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    int-to-float v6, v6

    .line 73
    div-float/2addr v5, v6

    .line 74
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    int-to-float v6, v6

    .line 79
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v7

    .line 83
    int-to-float v7, v7

    .line 84
    div-float/2addr v6, v7

    .line 85
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    .line 86
    .line 87
    .line 88
    move-result v5

    .line 89
    mul-float v5, v5, v4

    .line 90
    .line 91
    iget-object v6, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 92
    .line 93
    invoke-virtual {v6, v5, v2}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 94
    .line 95
    .line 96
    :cond_2
    const/4 v5, -0x1

    .line 97
    invoke-virtual {v3, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 98
    .line 99
    .line 100
    iget-object v5, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 101
    .line 102
    invoke-virtual {v5, v3}, Lcom/intsig/office/ss/view/SheetView;->drawSheet(Landroid/graphics/Canvas;)V

    .line 103
    .line 104
    .line 105
    iget-object v5, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 106
    .line 107
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 108
    .line 109
    .line 110
    move-result-object v5

    .line 111
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getCalloutManager()Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    iget v6, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 116
    .line 117
    invoke-virtual {v5, v3, v6, v4}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->drawPath(Landroid/graphics/Canvas;IF)V

    .line 118
    .line 119
    .line 120
    invoke-interface {p1, v1}, Lcom/intsig/office/common/IOfficeToPicture;->callBack(Landroid/graphics/Bitmap;)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 124
    .line 125
    invoke-virtual {p1, v4, v2}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 126
    .line 127
    .line 128
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/picture/PictureKit;->setDrawPictrue(Z)V

    .line 133
    .line 134
    .line 135
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/ss/control/Spreadsheet;)Lcom/intsig/office/system/IControl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/ss/control/Spreadsheet;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/ss/control/Spreadsheet;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public OnReadingFinished()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/office/ss/control/Spreadsheet$3;

    .line 16
    .line 17
    invoke-direct {v0, p0}, Lcom/intsig/office/ss/control/Spreadsheet$3;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abortDrawing()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public computeScroll()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/SSEventManage;->computeScroll()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public createPicture()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/common/IOfficeToPicture;->getModeType()B

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    :try_start_0
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/control/Spreadsheet;->toPicture(Lcom/intsig/office/common/IOfficeToPicture;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :catch_0
    move-exception v0

    .line 21
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 22
    .line 23
    .line 24
    :cond_0
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->parent:Lcom/intsig/office/ss/control/ExcelView;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->dispose()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 18
    .line 19
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 20
    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/SSEventManage;->dispose()V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 27
    .line 28
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->editor:Lcom/intsig/office/ss/control/SSEditor;

    .line 29
    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/SSEditor;->dispose()V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->editor:Lcom/intsig/office/ss/control/SSEditor;

    .line 36
    .line 37
    :cond_2
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public exportImage()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    const v1, 0x2000000a

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public find(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/SheetView;->find(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public findBackward()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->findBackward()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public findForward()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->findForward()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActiveCellContent()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    return-object v0

    .line 34
    :cond_0
    const-string v0, ""

    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getActiveCellHyperlink()Lcom/intsig/office/common/hyperlink/Hyperlink;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getHyperLink()Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getHyperLink()Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    return-object v0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBottomBarHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->parent:Lcom/intsig/office/ss/control/ExcelView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/ExcelView;->getBottomBarHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getControl()Lcom/intsig/office/system/IControl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentSheetNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEditor()Lcom/intsig/office/simpletext/control/IWord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->editor:Lcom/intsig/office/ss/control/SSEditor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEventManage()Lcom/intsig/office/system/beans/AEventManage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->eventManage:Lcom/intsig/office/ss/control/SSEventManage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFitZoom()F
    .locals 1

    .line 1
    const/high16 v0, 0x3f000000    # 0.5f

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheetCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheetCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheetView()Lcom/intsig/office/ss/view/SheetView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSnapshot(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    monitor-enter v0

    .line 8
    :try_start_0
    new-instance v1, Landroid/graphics/Canvas;

    .line 9
    .line 10
    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    const/4 v5, 0x1

    .line 28
    if-ne v3, v4, :cond_1

    .line 29
    .line 30
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    if-eq v3, v4, :cond_2

    .line 39
    .line 40
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    int-to-float v3, v3

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    int-to-float v4, v4

    .line 50
    div-float/2addr v3, v4

    .line 51
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    int-to-float v4, v4

    .line 56
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 57
    .line 58
    .line 59
    move-result v6

    .line 60
    int-to-float v6, v6

    .line 61
    div-float/2addr v4, v6

    .line 62
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    mul-float v3, v3, v2

    .line 67
    .line 68
    iget-object v4, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 69
    .line 70
    invoke-virtual {v4, v3, v5}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 71
    .line 72
    .line 73
    :cond_2
    const/4 v3, -0x1

    .line 74
    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 75
    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 78
    .line 79
    invoke-virtual {v3, v1}, Lcom/intsig/office/ss/view/SheetView;->drawSheet(Landroid/graphics/Canvas;)V

    .line 80
    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 83
    .line 84
    invoke-virtual {v1, v2, v5}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 85
    .line 86
    .line 87
    monitor-exit v0

    .line 88
    return-object p1

    .line 89
    :catchall_0
    move-exception p1

    .line 90
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    throw p1
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getThumbnail(IIF)Landroid/graphics/Bitmap;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    const/4 v3, 0x2

    .line 15
    if-eq v2, v3, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 19
    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    new-instance v2, Lcom/intsig/office/ss/view/SheetView;

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 25
    .line 26
    invoke-virtual {v3, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-direct {v2, p0, v1}, Lcom/intsig/office/ss/view/SheetView;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 31
    .line 32
    .line 33
    iput-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 34
    .line 35
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 36
    .line 37
    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/intsig/office/ss/view/SheetView;->getThumbnail(Lcom/intsig/office/ss/model/baseModel/Sheet;IIF)Landroid/graphics/Bitmap;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    return-object p1

    .line 42
    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 43
    return-object p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoom()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/ss/view/SheetView;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public init()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 2
    .line 3
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-lez v0, :cond_0

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 13
    .line 14
    add-int/2addr v0, v1

    .line 15
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 22
    .line 23
    new-instance v2, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->fileName:Ljava/lang/String;

    .line 29
    .line 30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v3, " : "

    .line 34
    .line 35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget-object v3, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 39
    .line 40
    const/4 v4, 0x0

    .line 41
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    const/high16 v3, 0x40000000    # 2.0f

    .line 57
    .line 58
    invoke-interface {v0, v3, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 62
    .line 63
    if-nez v0, :cond_1

    .line 64
    .line 65
    new-instance v0, Lcom/intsig/office/ss/view/SheetView;

    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 68
    .line 69
    invoke-virtual {v2, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-direct {v0, p0, v2}, Lcom/intsig/office/ss/view/SheetView;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 74
    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 77
    .line 78
    :cond_1
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->initFinish:Z

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 81
    .line 82
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    const/4 v1, 0x2

    .line 91
    if-eq v0, v1, :cond_2

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 94
    .line 95
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v0, p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setReaderListener(Lcom/intsig/office/ss/model/interfacePart/IReaderListener;)V

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 103
    .line 104
    const/16 v1, 0x1a

    .line 105
    .line 106
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 107
    .line 108
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 109
    .line 110
    .line 111
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->mIsNeedReDraw:Z

    .line 112
    .line 113
    if-eqz v0, :cond_3

    .line 114
    .line 115
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 116
    .line 117
    .line 118
    :cond_3
    new-instance v0, Lcom/intsig/office/ss/control/Spreadsheet$1;

    .line 119
    .line 120
    invoke-direct {v0, p0}, Lcom/intsig/office/ss/control/Spreadsheet$1;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 124
    .line 125
    .line 126
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public initCalloutView()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 12
    .line 13
    invoke-direct {v0, v1, v2, p0}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;-><init>(Landroid/content/Context;Lcom/intsig/office/system/IControl;Lcom/intsig/office/system/beans/CalloutView/IExportListener;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->setIndex(I)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 24
    .line 25
    const/4 v1, -0x1

    .line 26
    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 27
    .line 28
    .line 29
    const/16 v1, 0x32

    .line 30
    .line 31
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 32
    .line 33
    const/16 v1, 0x1e

    .line 34
    .line 35
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 38
    .line 39
    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public isAbortDrawing()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->isConfigurationChanged:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->initFinish:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->mIsNeedReDraw:Z

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->mIsNeedReDraw:Z

    .line 11
    .line 12
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/SheetView;->drawSheet(Landroid/graphics/Canvas;)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 18
    .line 19
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->isAutoTest()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    const/4 v0, 0x2

    .line 24
    if-eqz p1, :cond_3

    .line 25
    .line 26
    iget p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheetCount()I

    .line 31
    .line 32
    .line 33
    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 34
    sub-int/2addr v2, v1

    .line 35
    if-ge p1, v2, :cond_2

    .line 36
    .line 37
    :goto_0
    :try_start_1
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-eq p1, v0, :cond_1

    .line 48
    .line 49
    const-wide/16 v2, 0x32

    .line 50
    .line 51
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :catch_0
    :cond_1
    :try_start_2
    iget p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 56
    .line 57
    add-int/2addr p1, v1

    .line 58
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/control/Spreadsheet;->showSheet(I)V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 63
    .line 64
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 65
    .line 66
    const/16 v2, 0x16

    .line 67
    .line 68
    invoke-interface {p1, v2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 73
    .line 74
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    if-eqz p1, :cond_4

    .line 79
    .line 80
    invoke-interface {p1}, Lcom/intsig/office/common/IOfficeToPicture;->getModeType()B

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-nez v1, :cond_4

    .line 85
    .line 86
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/Spreadsheet;->toPicture(Lcom/intsig/office/common/IOfficeToPicture;)V

    .line 87
    .line 88
    .line 89
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 90
    .line 91
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    if-eq p1, v0, :cond_5

    .line 100
    .line 101
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 102
    .line 103
    .line 104
    :cond_5
    iget p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->preShowSheetIndex:I

    .line 105
    .line 106
    iget v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 107
    .line 108
    if-eq p1, v0, :cond_6

    .line 109
    .line 110
    iput v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->preShowSheetIndex:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 111
    .line 112
    goto :goto_2

    .line 113
    :catch_1
    move-exception p1

    .line 114
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    .line 115
    .line 116
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {v0, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 125
    .line 126
    .line 127
    :cond_6
    :goto_2
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    iget-boolean p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->isConfigurationChanged:Z

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->isConfigurationChanged:Z

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/office/ss/control/Spreadsheet$2;

    .line 12
    .line 13
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/control/Spreadsheet$2;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public removeSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->removeSearch()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeSheetBar()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->isDefaultSheetBar:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public resetSearchResult()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setZoom(F)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/ss/view/SheetView;-><init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 20
    .line 21
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/SheetView;->setZoom(F)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public showSheet(I)V
    .locals 3

    .line 6
    iget v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    if-eq v0, p1, :cond_2

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object v0

    .line 9
    iput p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetName:Ljava/lang/String;

    .line 11
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->control:Lcom/intsig/office/system/IControl;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 12
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->callouts:Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    if-eqz p1, :cond_1

    .line 13
    iget v1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    invoke-virtual {p1, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->setIndex(I)V

    .line 14
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/control/Spreadsheet;->showSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public showSheet(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 3
    :cond_1
    iput-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetName:Ljava/lang/String;

    .line 4
    iget-object p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->workbook:Lcom/intsig/office/ss/model/baseModel/Workbook;

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheetIndex(Lcom/intsig/office/ss/model/baseModel/Sheet;)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/ss/control/Spreadsheet;->currentSheetIndex:I

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/control/Spreadsheet;->showSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    return-void
.end method

.method public startDrawing()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/ss/control/SSEventManage;
.super Lcom/intsig/office/system/beans/AEventManage;
.source "SSEventManage.java"

# interfaces
.implements Lcom/intsig/office/system/ITimerListener;


# static fields
.field private static final MINDISTANCE:I = 0xa


# instance fields
.field private actionDown:Z

.field private longPress:Z

.field private newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

.field private oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

.field private oldX:I

.field private oldY:I

.field private scrolling:Z

.field private spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

.field private timer:Lcom/intsig/office/system/beans/ATimer;


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/system/IControl;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/system/beans/AEventManage;-><init>(Landroid/content/Context;Lcom/intsig/office/system/IControl;)V

    .line 6
    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 9
    .line 10
    new-instance p1, Lcom/intsig/office/system/beans/ATimer;

    .line 11
    .line 12
    const/16 p2, 0x3e8

    .line 13
    .line 14
    invoke-direct {p1, p2, p0}, Lcom/intsig/office/system/beans/ATimer;-><init>(ILcom/intsig/office/system/ITimerListener;)V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private actionUp(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->actionDown:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->actionDown:Z

    .line 8
    .line 9
    iget-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 10
    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/ss/control/SSEventManage;->changeHeaderEnd()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    goto :goto_0

    .line 18
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->longPress:Z

    .line 19
    .line 20
    if-nez v1, :cond_2

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->checkClickedCell(Landroid/view/MotionEvent;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->checkClickedHeader(Landroid/view/MotionEvent;)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->longPress:Z

    .line 35
    .line 36
    if-eqz p1, :cond_4

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/ATimer;->isRunning()Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-nez p1, :cond_3

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/ATimer;->start()V

    .line 49
    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/ATimer;->restart()V

    .line 55
    .line 56
    .line 57
    :cond_4
    :goto_1
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private changeHeaderEnd()Z
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 5
    .line 6
    if-eqz v1, :cond_f

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FocusCell;->getType()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/4 v3, 0x1

    .line 25
    if-eq v2, v3, :cond_6

    .line 26
    .line 27
    const/4 v4, 0x2

    .line 28
    if-eq v2, v4, :cond_0

    .line 29
    .line 30
    const/4 v3, 0x0

    .line 31
    goto/16 :goto_9

    .line 32
    .line 33
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 40
    .line 41
    iget-object v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 42
    .line 43
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    iget v4, v4, Landroid/graphics/Rect;->left:I

    .line 48
    .line 49
    sub-int/2addr v2, v4

    .line 50
    iget-object v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 51
    .line 52
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    iget v4, v4, Landroid/graphics/Rect;->right:I

    .line 57
    .line 58
    iget-object v5, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 59
    .line 60
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 61
    .line 62
    .line 63
    move-result-object v5

    .line 64
    iget v5, v5, Landroid/graphics/Rect;->left:I

    .line 65
    .line 66
    sub-int/2addr v4, v5

    .line 67
    sub-int/2addr v2, v4

    .line 68
    int-to-float v2, v2

    .line 69
    iget-object v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 70
    .line 71
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/FocusCell;->getColumn()I

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    :goto_0
    invoke-virtual {v1, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isColumnHidden(I)Z

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    if-eqz v5, :cond_1

    .line 80
    .line 81
    add-int/lit8 v4, v4, -0x1

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {v1, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 85
    .line 86
    .line 87
    move-result v5

    .line 88
    iget-object v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 89
    .line 90
    invoke-virtual {v6}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 91
    .line 92
    .line 93
    move-result-object v6

    .line 94
    invoke-virtual {v6}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 95
    .line 96
    .line 97
    move-result v6

    .line 98
    div-float/2addr v2, v6

    .line 99
    add-float/2addr v5, v2

    .line 100
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    invoke-virtual {v1, v4, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setColumnPixelWidth(II)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getFirstRowNum()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    :goto_1
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getLastRowNum()I

    .line 112
    .line 113
    .line 114
    move-result v4

    .line 115
    if-gt v2, v4, :cond_e

    .line 116
    .line 117
    add-int/lit8 v4, v2, 0x1

    .line 118
    .line 119
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    if-nez v2, :cond_2

    .line 124
    .line 125
    goto :goto_3

    .line 126
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getFirstCol()I

    .line 127
    .line 128
    .line 129
    move-result v5

    .line 130
    iget-object v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 131
    .line 132
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/FocusCell;->getColumn()I

    .line 133
    .line 134
    .line 135
    move-result v6

    .line 136
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    .line 137
    .line 138
    .line 139
    move-result v5

    .line 140
    :goto_2
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getLastCol()I

    .line 141
    .line 142
    .line 143
    move-result v6

    .line 144
    if-gt v5, v6, :cond_5

    .line 145
    .line 146
    invoke-virtual {v2, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 147
    .line 148
    .line 149
    move-result-object v6

    .line 150
    if-eqz v6, :cond_4

    .line 151
    .line 152
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 153
    .line 154
    .line 155
    move-result v7

    .line 156
    if-ltz v7, :cond_3

    .line 157
    .line 158
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 159
    .line 160
    .line 161
    move-result v6

    .line 162
    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 163
    .line 164
    .line 165
    move-result-object v6

    .line 166
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 167
    .line 168
    .line 169
    move-result v7

    .line 170
    invoke-virtual {v1, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 171
    .line 172
    .line 173
    move-result-object v7

    .line 174
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 175
    .line 176
    .line 177
    move-result v6

    .line 178
    invoke-virtual {v7, v6}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 179
    .line 180
    .line 181
    move-result-object v6

    .line 182
    :cond_3
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->removeSTRoot()V

    .line 183
    .line 184
    .line 185
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 186
    .line 187
    goto :goto_2

    .line 188
    :cond_5
    invoke-virtual {v2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->setInitExpandedRangeAddress(Z)V

    .line 189
    .line 190
    .line 191
    :goto_3
    move v2, v4

    .line 192
    goto :goto_1

    .line 193
    :cond_6
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 194
    .line 195
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FocusCell;->getRow()I

    .line 196
    .line 197
    .line 198
    move-result v2

    .line 199
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 200
    .line 201
    .line 202
    move-result-object v4

    .line 203
    if-nez v4, :cond_7

    .line 204
    .line 205
    new-instance v4, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 206
    .line 207
    invoke-direct {v4, v0}, Lcom/intsig/office/ss/model/baseModel/Row;-><init>(I)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {v4, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowNumber(I)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v4, v1}, Lcom/intsig/office/ss/model/baseModel/Row;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {v1, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 217
    .line 218
    .line 219
    goto :goto_5

    .line 220
    :cond_7
    :goto_4
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 221
    .line 222
    .line 223
    move-result-object v4

    .line 224
    if-eqz v4, :cond_8

    .line 225
    .line 226
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 227
    .line 228
    .line 229
    move-result-object v4

    .line 230
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 231
    .line 232
    .line 233
    move-result v4

    .line 234
    if-eqz v4, :cond_8

    .line 235
    .line 236
    add-int/lit8 v2, v2, -0x1

    .line 237
    .line 238
    goto :goto_4

    .line 239
    :cond_8
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 240
    .line 241
    .line 242
    move-result-object v4

    .line 243
    if-nez v4, :cond_9

    .line 244
    .line 245
    new-instance v4, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 246
    .line 247
    invoke-direct {v4, v0}, Lcom/intsig/office/ss/model/baseModel/Row;-><init>(I)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v4, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowNumber(I)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {v4, v1}, Lcom/intsig/office/ss/model/baseModel/Row;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v1, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 257
    .line 258
    .line 259
    :cond_9
    :goto_5
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 260
    .line 261
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 262
    .line 263
    .line 264
    move-result-object v2

    .line 265
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 266
    .line 267
    iget-object v5, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 268
    .line 269
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 270
    .line 271
    .line 272
    move-result-object v5

    .line 273
    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 274
    .line 275
    sub-int/2addr v2, v5

    .line 276
    int-to-float v2, v2

    .line 277
    iget-object v5, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 278
    .line 279
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 280
    .line 281
    .line 282
    move-result-object v5

    .line 283
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 284
    .line 285
    iget-object v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 286
    .line 287
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 288
    .line 289
    .line 290
    move-result-object v6

    .line 291
    iget v6, v6, Landroid/graphics/Rect;->top:I

    .line 292
    .line 293
    sub-int/2addr v5, v6

    .line 294
    int-to-float v5, v5

    .line 295
    sub-float/2addr v2, v5

    .line 296
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 297
    .line 298
    .line 299
    move-result v5

    .line 300
    iget-object v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 301
    .line 302
    invoke-virtual {v6}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 303
    .line 304
    .line 305
    move-result-object v6

    .line 306
    invoke-virtual {v6}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 307
    .line 308
    .line 309
    move-result v6

    .line 310
    div-float/2addr v2, v6

    .line 311
    add-float/2addr v5, v2

    .line 312
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 313
    .line 314
    .line 315
    move-result v2

    .line 316
    int-to-float v2, v2

    .line 317
    invoke-virtual {v4, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowPixelHeight(F)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 321
    .line 322
    .line 323
    move-result v2

    .line 324
    :goto_6
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getLastRowNum()I

    .line 325
    .line 326
    .line 327
    move-result v4

    .line 328
    if-gt v2, v4, :cond_e

    .line 329
    .line 330
    add-int/lit8 v4, v2, 0x1

    .line 331
    .line 332
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 333
    .line 334
    .line 335
    move-result-object v2

    .line 336
    if-nez v2, :cond_a

    .line 337
    .line 338
    goto :goto_8

    .line 339
    :cond_a
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getFirstCol()I

    .line 340
    .line 341
    .line 342
    move-result v5

    .line 343
    :goto_7
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getLastCol()I

    .line 344
    .line 345
    .line 346
    move-result v6

    .line 347
    if-gt v5, v6, :cond_d

    .line 348
    .line 349
    invoke-virtual {v2, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 350
    .line 351
    .line 352
    move-result-object v6

    .line 353
    if-eqz v6, :cond_c

    .line 354
    .line 355
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 356
    .line 357
    .line 358
    move-result v7

    .line 359
    if-ltz v7, :cond_b

    .line 360
    .line 361
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 362
    .line 363
    .line 364
    move-result v6

    .line 365
    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 366
    .line 367
    .line 368
    move-result-object v6

    .line 369
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 370
    .line 371
    .line 372
    move-result v7

    .line 373
    invoke-virtual {v1, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 374
    .line 375
    .line 376
    move-result-object v7

    .line 377
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 378
    .line 379
    .line 380
    move-result v6

    .line 381
    invoke-virtual {v7, v6}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 382
    .line 383
    .line 384
    move-result-object v6

    .line 385
    :cond_b
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->removeSTRoot()V

    .line 386
    .line 387
    .line 388
    :cond_c
    add-int/lit8 v5, v5, 0x1

    .line 389
    .line 390
    goto :goto_7

    .line 391
    :cond_d
    invoke-virtual {v2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->setInitExpandedRangeAddress(Z)V

    .line 392
    .line 393
    .line 394
    :goto_8
    move v2, v4

    .line 395
    goto :goto_6

    .line 396
    :cond_e
    :goto_9
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 397
    .line 398
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 399
    .line 400
    .line 401
    move-result-object v1

    .line 402
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->updateMinRowAndColumnInfo()V

    .line 403
    .line 404
    .line 405
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 406
    .line 407
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 408
    .line 409
    .line 410
    move-result-object v1

    .line 411
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/view/SheetView;->setDrawMovingHeaderLine(Z)V

    .line 412
    .line 413
    .line 414
    const/4 v0, 0x0

    .line 415
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 416
    .line 417
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 418
    .line 419
    move v0, v3

    .line 420
    :cond_f
    return v0
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private changingHeader(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FocusCell;->getType()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eq v2, v0, :cond_2

    .line 24
    .line 25
    const/4 p1, 0x2

    .line 26
    if-eq v2, p1, :cond_1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iget v0, v0, Landroid/graphics/Rect;->right:I

    .line 42
    .line 43
    int-to-float v0, v0

    .line 44
    iget v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 45
    .line 46
    int-to-float v2, v2

    .line 47
    sub-float/2addr v1, v2

    .line 48
    add-float/2addr v0, v1

    .line 49
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 54
    .line 55
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 56
    .line 57
    add-int/lit8 v2, v1, 0xa

    .line 58
    .line 59
    if-gt v0, v2, :cond_3

    .line 60
    .line 61
    add-int/lit8 v1, v1, 0xa

    .line 62
    .line 63
    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 73
    .line 74
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 79
    .line 80
    int-to-float v1, v1

    .line 81
    iget v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 82
    .line 83
    int-to-float v2, v2

    .line 84
    sub-float/2addr p1, v2

    .line 85
    add-float/2addr v1, p1

    .line 86
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 91
    .line 92
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 93
    .line 94
    add-int/lit8 v2, v1, 0xa

    .line 95
    .line 96
    if-gt p1, v2, :cond_3

    .line 97
    .line 98
    add-int/lit8 v1, v1, 0xa

    .line 99
    .line 100
    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 101
    .line 102
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 103
    .line 104
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 109
    .line 110
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/view/SheetView;->changeHeaderArea(Lcom/intsig/office/ss/other/FocusCell;)V

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private checkClickedCell(Landroid/view/MotionEvent;)Z
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    int-to-float v1, v1

    .line 20
    const/4 v2, 0x0

    .line 21
    cmpl-float v1, v1, p1

    .line 22
    .line 23
    if-gtz v1, :cond_a

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    int-to-float v1, v1

    .line 36
    cmpl-float v1, v1, v0

    .line 37
    .line 38
    if-lez v1, :cond_0

    .line 39
    .line 40
    goto/16 :goto_5

    .line 41
    .line 42
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    new-instance v3, Lcom/intsig/office/ss/other/DrawingCell;

    .line 49
    .line 50
    invoke-direct {v3}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    int-to-float v4, v4

    .line 58
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    int-to-float v4, v4

    .line 66
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 96
    .line 97
    .line 98
    move-result-object v4

    .line 99
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    if-eqz v4, :cond_1

    .line 104
    .line 105
    const/high16 v4, 0x10000

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_1
    const/high16 v4, 0x100000

    .line 109
    .line 110
    :goto_0
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    const/4 v6, 0x1

    .line 115
    cmpg-float v5, v5, p1

    .line 116
    .line 117
    if-gtz v5, :cond_5

    .line 118
    .line 119
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 120
    .line 121
    .line 122
    move-result v5

    .line 123
    if-gt v5, v4, :cond_5

    .line 124
    .line 125
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 126
    .line 127
    .line 128
    move-result-object v5

    .line 129
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 130
    .line 131
    .line 132
    move-result v7

    .line 133
    invoke-virtual {v5, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 134
    .line 135
    .line 136
    move-result-object v5

    .line 137
    if-eqz v5, :cond_2

    .line 138
    .line 139
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 140
    .line 141
    .line 142
    move-result v7

    .line 143
    if-eqz v7, :cond_2

    .line 144
    .line 145
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 146
    .line 147
    .line 148
    move-result v5

    .line 149
    add-int/2addr v5, v6

    .line 150
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 151
    .line 152
    .line 153
    goto :goto_0

    .line 154
    :cond_2
    if-nez v5, :cond_3

    .line 155
    .line 156
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 157
    .line 158
    .line 159
    move-result-object v5

    .line 160
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 161
    .line 162
    .line 163
    move-result v5

    .line 164
    int-to-float v5, v5

    .line 165
    goto :goto_1

    .line 166
    :cond_3
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 167
    .line 168
    .line 169
    move-result v5

    .line 170
    :goto_1
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 174
    .line 175
    .line 176
    move-result v5

    .line 177
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 178
    .line 179
    .line 180
    move-result v7

    .line 181
    mul-float v5, v5, v7

    .line 182
    .line 183
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 184
    .line 185
    .line 186
    move-result v5

    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 189
    .line 190
    .line 191
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 192
    .line 193
    .line 194
    move-result v5

    .line 195
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 196
    .line 197
    .line 198
    move-result-object v7

    .line 199
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 200
    .line 201
    .line 202
    move-result v7

    .line 203
    if-ne v5, v7, :cond_4

    .line 204
    .line 205
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 206
    .line 207
    .line 208
    move-result-object v5

    .line 209
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 210
    .line 211
    .line 212
    move-result v5

    .line 213
    if-nez v5, :cond_4

    .line 214
    .line 215
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 216
    .line 217
    .line 218
    move-result-object v5

    .line 219
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 220
    .line 221
    .line 222
    move-result-wide v7

    .line 223
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 224
    .line 225
    .line 226
    move-result v5

    .line 227
    float-to-double v9, v5

    .line 228
    mul-double v7, v7, v9

    .line 229
    .line 230
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    .line 231
    .line 232
    .line 233
    move-result-wide v7

    .line 234
    long-to-float v5, v7

    .line 235
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 236
    .line 237
    .line 238
    goto :goto_2

    .line 239
    :cond_4
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 240
    .line 241
    .line 242
    move-result v5

    .line 243
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 244
    .line 245
    .line 246
    :goto_2
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 247
    .line 248
    .line 249
    move-result v5

    .line 250
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 251
    .line 252
    .line 253
    move-result v7

    .line 254
    add-float/2addr v5, v7

    .line 255
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 259
    .line 260
    .line 261
    move-result v5

    .line 262
    add-int/2addr v5, v6

    .line 263
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 264
    .line 265
    .line 266
    goto/16 :goto_0

    .line 267
    .line 268
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 273
    .line 274
    .line 275
    move-result-object p1

    .line 276
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 277
    .line 278
    .line 279
    move-result p1

    .line 280
    if-eqz p1, :cond_6

    .line 281
    .line 282
    const/16 p1, 0x100

    .line 283
    .line 284
    goto :goto_3

    .line 285
    :cond_6
    const/16 p1, 0x4000

    .line 286
    .line 287
    :goto_3
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 288
    .line 289
    .line 290
    move-result v4

    .line 291
    cmpg-float v4, v4, v0

    .line 292
    .line 293
    if-gtz v4, :cond_9

    .line 294
    .line 295
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 296
    .line 297
    .line 298
    move-result v4

    .line 299
    if-gt v4, p1, :cond_9

    .line 300
    .line 301
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 302
    .line 303
    .line 304
    move-result-object v4

    .line 305
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 306
    .line 307
    .line 308
    move-result v5

    .line 309
    invoke-virtual {v4, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isColumnHidden(I)Z

    .line 310
    .line 311
    .line 312
    move-result v4

    .line 313
    if-eqz v4, :cond_7

    .line 314
    .line 315
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 316
    .line 317
    .line 318
    move-result v4

    .line 319
    add-int/2addr v4, v6

    .line 320
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 321
    .line 322
    .line 323
    goto :goto_3

    .line 324
    :cond_7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 325
    .line 326
    .line 327
    move-result-object v4

    .line 328
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 329
    .line 330
    .line 331
    move-result v5

    .line 332
    invoke-virtual {v4, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 333
    .line 334
    .line 335
    move-result v4

    .line 336
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 337
    .line 338
    .line 339
    move-result v5

    .line 340
    mul-float v4, v4, v5

    .line 341
    .line 342
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 343
    .line 344
    .line 345
    move-result v4

    .line 346
    int-to-float v4, v4

    .line 347
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setWidth(F)V

    .line 348
    .line 349
    .line 350
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 351
    .line 352
    .line 353
    move-result v4

    .line 354
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 355
    .line 356
    .line 357
    move-result-object v5

    .line 358
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 359
    .line 360
    .line 361
    move-result v5

    .line 362
    if-ne v4, v5, :cond_8

    .line 363
    .line 364
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 365
    .line 366
    .line 367
    move-result-object v4

    .line 368
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->isColumnAllVisible()Z

    .line 369
    .line 370
    .line 371
    move-result v4

    .line 372
    if-nez v4, :cond_8

    .line 373
    .line 374
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 375
    .line 376
    .line 377
    move-result-object v4

    .line 378
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    .line 379
    .line 380
    .line 381
    move-result-wide v4

    .line 382
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 383
    .line 384
    .line 385
    move-result v7

    .line 386
    float-to-double v7, v7

    .line 387
    mul-double v4, v4, v7

    .line 388
    .line 389
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    .line 390
    .line 391
    .line 392
    move-result-wide v4

    .line 393
    long-to-float v4, v4

    .line 394
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 395
    .line 396
    .line 397
    goto :goto_4

    .line 398
    :cond_8
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 399
    .line 400
    .line 401
    move-result v4

    .line 402
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 403
    .line 404
    .line 405
    :goto_4
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 406
    .line 407
    .line 408
    move-result v4

    .line 409
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 410
    .line 411
    .line 412
    move-result v5

    .line 413
    add-float/2addr v4, v5

    .line 414
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 415
    .line 416
    .line 417
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 418
    .line 419
    .line 420
    move-result v4

    .line 421
    add-int/2addr v4, v6

    .line 422
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 423
    .line 424
    .line 425
    goto/16 :goto_3

    .line 426
    .line 427
    :cond_9
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 428
    .line 429
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 430
    .line 431
    .line 432
    move-result-object p1

    .line 433
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 434
    .line 435
    .line 436
    move-result-object p1

    .line 437
    invoke-virtual {p1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellType(S)V

    .line 438
    .line 439
    .line 440
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 441
    .line 442
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 443
    .line 444
    .line 445
    move-result-object p1

    .line 446
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 447
    .line 448
    .line 449
    move-result v0

    .line 450
    sub-int/2addr v0, v6

    .line 451
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 452
    .line 453
    .line 454
    move-result v1

    .line 455
    sub-int/2addr v1, v6

    .line 456
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/ss/view/SheetView;->selectedCell(II)V

    .line 457
    .line 458
    .line 459
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 460
    .line 461
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 462
    .line 463
    .line 464
    move-result-object p1

    .line 465
    const v0, 0x20000007

    .line 466
    .line 467
    .line 468
    const/4 v1, 0x0

    .line 469
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 470
    .line 471
    .line 472
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 473
    .line 474
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 475
    .line 476
    .line 477
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 478
    .line 479
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 480
    .line 481
    .line 482
    return v6

    .line 483
    :cond_a
    :goto_5
    return v2
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private checkClickedHeader(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    int-to-float v3, v3

    .line 20
    const/4 v4, 0x1

    .line 21
    cmpl-float v3, v3, v0

    .line 22
    .line 23
    if-lez v3, :cond_0

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    int-to-float v3, v3

    .line 30
    cmpg-float v3, v3, v1

    .line 31
    .line 32
    if-gez v3, :cond_0

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellType(S)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->findClickedRowHeader(Landroid/view/MotionEvent;)I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellRow(I)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    int-to-float v3, v3

    .line 58
    cmpg-float v0, v3, v0

    .line 59
    .line 60
    if-gez v0, :cond_1

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    int-to-float v0, v0

    .line 67
    cmpl-float v0, v0, v1

    .line 68
    .line 69
    if-lez v0, :cond_1

    .line 70
    .line 71
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const/4 v1, 0x2

    .line 76
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellType(S)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->findClickedColumnHeader(Landroid/view/MotionEvent;)I

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellColumn(I)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_1
    const/4 v4, 0x0

    .line 92
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    const v0, 0x20000007

    .line 99
    .line 100
    .line 101
    const/4 v1, 0x0

    .line 102
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 103
    .line 104
    .line 105
    return v4
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private findChangingColumnHeader(Landroid/view/MotionEvent;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/office/ss/other/DrawingCell;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    new-instance v3, Landroid/graphics/Rect;

    .line 44
    .line 45
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 57
    .line 58
    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    if-eqz v4, :cond_0

    .line 73
    .line 74
    const/16 v4, 0x100

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_0
    const/16 v4, 0x4000

    .line 78
    .line 79
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 80
    .line 81
    .line 82
    move-result v5

    .line 83
    cmpg-float v5, v5, p1

    .line 84
    .line 85
    if-gtz v5, :cond_3

    .line 86
    .line 87
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 88
    .line 89
    .line 90
    move-result v5

    .line 91
    if-gt v5, v4, :cond_3

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    invoke-virtual {v5, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isColumnHidden(I)Z

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    if-eqz v5, :cond_1

    .line 106
    .line 107
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 108
    .line 109
    .line 110
    move-result v5

    .line 111
    add-int/lit8 v5, v5, 0x1

    .line 112
    .line 113
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 122
    .line 123
    .line 124
    move-result v5

    .line 125
    invoke-virtual {v2, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 130
    .line 131
    .line 132
    move-result v5

    .line 133
    mul-float v2, v2, v5

    .line 134
    .line 135
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 136
    .line 137
    .line 138
    move-result v2

    .line 139
    int-to-float v2, v2

    .line 140
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setWidth(F)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 144
    .line 145
    .line 146
    move-result v2

    .line 147
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 148
    .line 149
    .line 150
    move-result-object v5

    .line 151
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 152
    .line 153
    .line 154
    move-result v5

    .line 155
    if-ne v2, v5, :cond_2

    .line 156
    .line 157
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->isColumnAllVisible()Z

    .line 162
    .line 163
    .line 164
    move-result v2

    .line 165
    if-nez v2, :cond_2

    .line 166
    .line 167
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    .line 172
    .line 173
    .line 174
    move-result-wide v5

    .line 175
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 176
    .line 177
    .line 178
    move-result v2

    .line 179
    float-to-double v7, v2

    .line 180
    mul-double v5, v5, v7

    .line 181
    .line 182
    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    .line 183
    .line 184
    .line 185
    move-result-wide v5

    .line 186
    long-to-float v2, v5

    .line 187
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 188
    .line 189
    .line 190
    goto :goto_1

    .line 191
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 192
    .line 193
    .line 194
    move-result v2

    .line 195
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 196
    .line 197
    .line 198
    :goto_1
    iget v2, v3, Landroid/graphics/Rect;->right:I

    .line 199
    .line 200
    iput v2, v3, Landroid/graphics/Rect;->left:I

    .line 201
    .line 202
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 203
    .line 204
    .line 205
    move-result v2

    .line 206
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 207
    .line 208
    .line 209
    move-result v2

    .line 210
    iput v2, v3, Landroid/graphics/Rect;->right:I

    .line 211
    .line 212
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 213
    .line 214
    .line 215
    move-result v2

    .line 216
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 217
    .line 218
    .line 219
    move-result v2

    .line 220
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 221
    .line 222
    .line 223
    move-result v5

    .line 224
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 225
    .line 226
    .line 227
    move-result v6

    .line 228
    add-float/2addr v5, v6

    .line 229
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 233
    .line 234
    .line 235
    move-result v5

    .line 236
    add-int/lit8 v5, v5, 0x1

    .line 237
    .line 238
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 239
    .line 240
    .line 241
    goto/16 :goto_0

    .line 242
    .line 243
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 244
    .line 245
    if-nez v0, :cond_4

    .line 246
    .line 247
    new-instance v0, Lcom/intsig/office/ss/other/FocusCell;

    .line 248
    .line 249
    invoke-direct {v0}, Lcom/intsig/office/ss/other/FocusCell;-><init>()V

    .line 250
    .line 251
    .line 252
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 253
    .line 254
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 255
    .line 256
    const/4 v4, 0x2

    .line 257
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/other/FocusCell;->setType(S)V

    .line 258
    .line 259
    .line 260
    int-to-float v0, v2

    .line 261
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 262
    .line 263
    .line 264
    move-result v2

    .line 265
    add-float/2addr v0, v2

    .line 266
    const/high16 v2, 0x40000000    # 2.0f

    .line 267
    .line 268
    div-float/2addr v0, v2

    .line 269
    cmpl-float p1, p1, v0

    .line 270
    .line 271
    if-lez p1, :cond_5

    .line 272
    .line 273
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 274
    .line 275
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 276
    .line 277
    .line 278
    move-result v0

    .line 279
    add-int/lit8 v0, v0, -0x1

    .line 280
    .line 281
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/other/FocusCell;->setColumn(I)V

    .line 282
    .line 283
    .line 284
    iget p1, v3, Landroid/graphics/Rect;->right:I

    .line 285
    .line 286
    iput p1, v3, Landroid/graphics/Rect;->left:I

    .line 287
    .line 288
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 289
    .line 290
    .line 291
    move-result p1

    .line 292
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 293
    .line 294
    .line 295
    move-result p1

    .line 296
    iput p1, v3, Landroid/graphics/Rect;->right:I

    .line 297
    .line 298
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 299
    .line 300
    invoke-virtual {p1, v3}, Lcom/intsig/office/ss/other/FocusCell;->setRect(Landroid/graphics/Rect;)V

    .line 301
    .line 302
    .line 303
    goto :goto_3

    .line 304
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 305
    .line 306
    .line 307
    move-result p1

    .line 308
    sub-int/2addr p1, v4

    .line 309
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 310
    .line 311
    if-ltz p1, :cond_6

    .line 312
    .line 313
    goto :goto_2

    .line 314
    :cond_6
    const/4 p1, 0x0

    .line 315
    :goto_2
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/other/FocusCell;->setColumn(I)V

    .line 316
    .line 317
    .line 318
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 319
    .line 320
    invoke-virtual {p1, v3}, Lcom/intsig/office/ss/other/FocusCell;->setRect(Landroid/graphics/Rect;)V

    .line 321
    .line 322
    .line 323
    :goto_3
    return-void
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private findChangingRowHeader(Landroid/view/MotionEvent;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/office/ss/other/DrawingCell;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    new-instance v3, Landroid/graphics/Rect;

    .line 44
    .line 45
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 46
    .line 47
    .line 48
    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 49
    .line 50
    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 61
    .line 62
    .line 63
    move-result v4

    .line 64
    if-eqz v4, :cond_0

    .line 65
    .line 66
    const/high16 v4, 0x10000

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    const/high16 v4, 0x100000

    .line 70
    .line 71
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    const/4 v6, 0x1

    .line 76
    cmpg-float v5, v5, p1

    .line 77
    .line 78
    if-gtz v5, :cond_4

    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    if-gt v5, v4, :cond_4

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 87
    .line 88
    .line 89
    move-result-object v5

    .line 90
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 91
    .line 92
    .line 93
    move-result v7

    .line 94
    invoke-virtual {v5, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 95
    .line 96
    .line 97
    move-result-object v5

    .line 98
    if-eqz v5, :cond_1

    .line 99
    .line 100
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 101
    .line 102
    .line 103
    move-result v7

    .line 104
    if-eqz v7, :cond_1

    .line 105
    .line 106
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 107
    .line 108
    .line 109
    move-result v5

    .line 110
    add-int/2addr v5, v6

    .line 111
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 112
    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_1
    if-nez v5, :cond_2

    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 122
    .line 123
    .line 124
    move-result v2

    .line 125
    int-to-float v2, v2

    .line 126
    goto :goto_1

    .line 127
    :cond_2
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 128
    .line 129
    .line 130
    move-result v2

    .line 131
    :goto_1
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 139
    .line 140
    .line 141
    move-result v5

    .line 142
    mul-float v2, v2, v5

    .line 143
    .line 144
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    int-to-float v2, v2

    .line 149
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 153
    .line 154
    .line 155
    move-result v2

    .line 156
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 157
    .line 158
    .line 159
    move-result-object v5

    .line 160
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 161
    .line 162
    .line 163
    move-result v5

    .line 164
    if-ne v2, v5, :cond_3

    .line 165
    .line 166
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 167
    .line 168
    .line 169
    move-result-object v2

    .line 170
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    if-nez v2, :cond_3

    .line 175
    .line 176
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 177
    .line 178
    .line 179
    move-result-object v2

    .line 180
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 181
    .line 182
    .line 183
    move-result-wide v7

    .line 184
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 185
    .line 186
    .line 187
    move-result v2

    .line 188
    float-to-double v9, v2

    .line 189
    mul-double v7, v7, v9

    .line 190
    .line 191
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    .line 192
    .line 193
    .line 194
    move-result-wide v7

    .line 195
    long-to-float v2, v7

    .line 196
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 197
    .line 198
    .line 199
    goto :goto_2

    .line 200
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 201
    .line 202
    .line 203
    move-result v2

    .line 204
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 205
    .line 206
    .line 207
    :goto_2
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 208
    .line 209
    iput v2, v3, Landroid/graphics/Rect;->top:I

    .line 210
    .line 211
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 212
    .line 213
    .line 214
    move-result v2

    .line 215
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 216
    .line 217
    .line 218
    move-result v2

    .line 219
    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 220
    .line 221
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 222
    .line 223
    .line 224
    move-result v2

    .line 225
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 226
    .line 227
    .line 228
    move-result v2

    .line 229
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 230
    .line 231
    .line 232
    move-result v5

    .line 233
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 234
    .line 235
    .line 236
    move-result v7

    .line 237
    add-float/2addr v5, v7

    .line 238
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 242
    .line 243
    .line 244
    move-result v5

    .line 245
    add-int/2addr v5, v6

    .line 246
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 247
    .line 248
    .line 249
    goto/16 :goto_0

    .line 250
    .line 251
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 252
    .line 253
    if-nez v0, :cond_5

    .line 254
    .line 255
    new-instance v0, Lcom/intsig/office/ss/other/FocusCell;

    .line 256
    .line 257
    invoke-direct {v0}, Lcom/intsig/office/ss/other/FocusCell;-><init>()V

    .line 258
    .line 259
    .line 260
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 261
    .line 262
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 263
    .line 264
    invoke-virtual {v0, v6}, Lcom/intsig/office/ss/other/FocusCell;->setType(S)V

    .line 265
    .line 266
    .line 267
    int-to-float v0, v2

    .line 268
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 269
    .line 270
    .line 271
    move-result v2

    .line 272
    add-float/2addr v0, v2

    .line 273
    const/high16 v2, 0x40000000    # 2.0f

    .line 274
    .line 275
    div-float/2addr v0, v2

    .line 276
    cmpl-float p1, p1, v0

    .line 277
    .line 278
    if-lez p1, :cond_6

    .line 279
    .line 280
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 281
    .line 282
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 283
    .line 284
    .line 285
    move-result v0

    .line 286
    sub-int/2addr v0, v6

    .line 287
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/other/FocusCell;->setRow(I)V

    .line 288
    .line 289
    .line 290
    iget p1, v3, Landroid/graphics/Rect;->bottom:I

    .line 291
    .line 292
    iput p1, v3, Landroid/graphics/Rect;->top:I

    .line 293
    .line 294
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 295
    .line 296
    .line 297
    move-result p1

    .line 298
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 299
    .line 300
    .line 301
    move-result p1

    .line 302
    iput p1, v3, Landroid/graphics/Rect;->bottom:I

    .line 303
    .line 304
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 305
    .line 306
    invoke-virtual {p1, v3}, Lcom/intsig/office/ss/other/FocusCell;->setRect(Landroid/graphics/Rect;)V

    .line 307
    .line 308
    .line 309
    goto :goto_4

    .line 310
    :cond_6
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 311
    .line 312
    .line 313
    move-result p1

    .line 314
    add-int/lit8 p1, p1, -0x2

    .line 315
    .line 316
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 317
    .line 318
    if-ltz p1, :cond_7

    .line 319
    .line 320
    goto :goto_3

    .line 321
    :cond_7
    const/4 p1, 0x0

    .line 322
    :goto_3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/other/FocusCell;->setRow(I)V

    .line 323
    .line 324
    .line 325
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 326
    .line 327
    invoke-virtual {p1, v3}, Lcom/intsig/office/ss/other/FocusCell;->setRect(Landroid/graphics/Rect;)V

    .line 328
    .line 329
    .line 330
    :goto_4
    return-void
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private findClickedColumnHeader(Landroid/view/MotionEvent;)I
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/office/ss/other/DrawingCell;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_0

    .line 48
    .line 49
    const/16 v2, 0x100

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/16 v2, 0x4000

    .line 53
    .line 54
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    cmpg-float v3, v3, p1

    .line 59
    .line 60
    if-gtz v3, :cond_3

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-gt v3, v2, :cond_3

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 73
    .line 74
    .line 75
    move-result v4

    .line 76
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isColumnHidden(I)Z

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    if-eqz v3, :cond_1

    .line 81
    .line 82
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 83
    .line 84
    .line 85
    move-result v3

    .line 86
    add-int/lit8 v3, v3, 0x1

    .line 87
    .line 88
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 101
    .line 102
    .line 103
    move-result v3

    .line 104
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    mul-float v3, v3, v4

    .line 109
    .line 110
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    int-to-float v3, v3

    .line 115
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setWidth(F)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 119
    .line 120
    .line 121
    move-result v3

    .line 122
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 123
    .line 124
    .line 125
    move-result-object v4

    .line 126
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 127
    .line 128
    .line 129
    move-result v4

    .line 130
    if-ne v3, v4, :cond_2

    .line 131
    .line 132
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 133
    .line 134
    .line 135
    move-result-object v3

    .line 136
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->isColumnAllVisible()Z

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    if-nez v3, :cond_2

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    .line 147
    .line 148
    .line 149
    move-result-wide v3

    .line 150
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 151
    .line 152
    .line 153
    move-result v5

    .line 154
    float-to-double v5, v5

    .line 155
    mul-double v3, v3, v5

    .line 156
    .line 157
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    .line 158
    .line 159
    .line 160
    move-result-wide v3

    .line 161
    long-to-float v3, v3

    .line 162
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 163
    .line 164
    .line 165
    goto :goto_1

    .line 166
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 167
    .line 168
    .line 169
    move-result v3

    .line 170
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 171
    .line 172
    .line 173
    :goto_1
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 174
    .line 175
    .line 176
    move-result v3

    .line 177
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 178
    .line 179
    .line 180
    move-result v4

    .line 181
    add-float/2addr v3, v4

    .line 182
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 186
    .line 187
    .line 188
    move-result v3

    .line 189
    add-int/lit8 v3, v3, 0x1

    .line 190
    .line 191
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 192
    .line 193
    .line 194
    goto/16 :goto_0

    .line 195
    .line 196
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 197
    .line 198
    .line 199
    move-result p1

    .line 200
    add-int/lit8 p1, p1, -0x1

    .line 201
    .line 202
    return p1
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private findClickedRowHeader(Landroid/view/MotionEvent;)I
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/office/ss/other/DrawingCell;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_0

    .line 48
    .line 49
    const/high16 v2, 0x10000

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/high16 v2, 0x100000

    .line 53
    .line 54
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    cmpg-float v3, v3, p1

    .line 59
    .line 60
    if-gtz v3, :cond_4

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-gt v3, v2, :cond_4

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 73
    .line 74
    .line 75
    move-result v4

    .line 76
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 77
    .line 78
    .line 79
    move-result-object v3

    .line 80
    if-eqz v3, :cond_1

    .line 81
    .line 82
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 83
    .line 84
    .line 85
    move-result v4

    .line 86
    if-eqz v4, :cond_1

    .line 87
    .line 88
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    add-int/lit8 v3, v3, 0x1

    .line 93
    .line 94
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_1
    if-nez v3, :cond_2

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 101
    .line 102
    .line 103
    move-result-object v3

    .line 104
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    int-to-float v3, v3

    .line 109
    goto :goto_1

    .line 110
    :cond_2
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    :goto_1
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 122
    .line 123
    .line 124
    move-result v4

    .line 125
    mul-float v3, v3, v4

    .line 126
    .line 127
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 128
    .line 129
    .line 130
    move-result v3

    .line 131
    int-to-float v3, v3

    .line 132
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 140
    .line 141
    .line 142
    move-result-object v4

    .line 143
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 144
    .line 145
    .line 146
    move-result v4

    .line 147
    if-ne v3, v4, :cond_3

    .line 148
    .line 149
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 150
    .line 151
    .line 152
    move-result-object v3

    .line 153
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 154
    .line 155
    .line 156
    move-result v3

    .line 157
    if-nez v3, :cond_3

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 164
    .line 165
    .line 166
    move-result-wide v3

    .line 167
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 168
    .line 169
    .line 170
    move-result v5

    .line 171
    float-to-double v5, v5

    .line 172
    mul-double v3, v3, v5

    .line 173
    .line 174
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    .line 175
    .line 176
    .line 177
    move-result-wide v3

    .line 178
    long-to-float v3, v3

    .line 179
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 180
    .line 181
    .line 182
    goto :goto_2

    .line 183
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 184
    .line 185
    .line 186
    move-result v3

    .line 187
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 188
    .line 189
    .line 190
    :goto_2
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 191
    .line 192
    .line 193
    move-result v3

    .line 194
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 195
    .line 196
    .line 197
    move-result v4

    .line 198
    add-float/2addr v3, v4

    .line 199
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 203
    .line 204
    .line 205
    move-result v3

    .line 206
    add-int/lit8 v3, v3, 0x1

    .line 207
    .line 208
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 209
    .line 210
    .line 211
    goto/16 :goto_0

    .line 212
    .line 213
    :cond_4
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    add-int/lit8 p1, p1, -0x1

    .line 218
    .line 219
    return p1
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method


# virtual methods
.method public actionPerformed()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->stop()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 7
    .line 8
    const v1, 0x2000000a

    .line 9
    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public computeScroll()V
    .locals 8

    .line 1
    invoke-super {p0}, Lcom/intsig/office/system/beans/AEventManage;->computeScroll()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_6

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isFling:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 16
    .line 17
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iget-object v2, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 22
    .line 23
    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    iget v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 28
    .line 29
    if-ne v1, v3, :cond_0

    .line 30
    .line 31
    iget v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 32
    .line 33
    if-ne v3, v2, :cond_0

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 36
    .line 37
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 52
    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    iget v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 58
    .line 59
    const/4 v5, 0x2

    .line 60
    if-eq v1, v4, :cond_2

    .line 61
    .line 62
    iget v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 63
    .line 64
    if-nez v6, :cond_2

    .line 65
    .line 66
    sub-int v4, v1, v4

    .line 67
    .line 68
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    if-le v4, v5, :cond_1

    .line 73
    .line 74
    const/4 v4, 0x1

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    iput v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 77
    .line 78
    :cond_2
    const/4 v4, 0x0

    .line 79
    :goto_0
    iget v6, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 80
    .line 81
    if-eq v2, v6, :cond_4

    .line 82
    .line 83
    iget v7, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 84
    .line 85
    if-nez v7, :cond_4

    .line 86
    .line 87
    sub-int/2addr v6, v2

    .line 88
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    .line 89
    .line 90
    .line 91
    move-result v6

    .line 92
    if-le v6, v5, :cond_3

    .line 93
    .line 94
    const/4 v4, 0x1

    .line 95
    goto :goto_1

    .line 96
    :cond_3
    iput v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 97
    .line 98
    :cond_4
    :goto_1
    if-eqz v4, :cond_5

    .line 99
    .line 100
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 101
    .line 102
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getRowHeader()Lcom/intsig/office/ss/view/RowHeader;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 107
    .line 108
    .line 109
    move-result v4

    .line 110
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/view/RowHeader;->calculateRowHeaderWidth(F)V

    .line 111
    .line 112
    .line 113
    iget v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 114
    .line 115
    sub-int v0, v1, v0

    .line 116
    .line 117
    int-to-float v0, v0

    .line 118
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    int-to-float v0, v0

    .line 123
    iget v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 124
    .line 125
    sub-int v4, v2, v4

    .line 126
    .line 127
    int-to-float v4, v4

    .line 128
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    int-to-float v4, v4

    .line 133
    invoke-virtual {v3, v0, v4}, Lcom/intsig/office/ss/view/SheetView;->scrollBy(FF)V

    .line 134
    .line 135
    .line 136
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 137
    .line 138
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 139
    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 142
    .line 143
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 144
    .line 145
    .line 146
    iput v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 147
    .line 148
    iput v2, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 149
    .line 150
    :cond_6
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/system/beans/AEventManage;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/FocusCell;->dispose()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 15
    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/FocusCell;->dispose()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 24
    .line 25
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 26
    .line 27
    if-eqz v1, :cond_2

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/ATimer;->dispose()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 33
    .line 34
    :cond_2
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public fling(II)V
    .locals 11

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/office/system/beans/AEventManage;->fling(II)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getScrollX()F

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    mul-float v1, v1, v0

    .line 25
    .line 26
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getScrollY()F

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    mul-float v1, v1, v0

    .line 41
    .line 42
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    const/4 v0, 0x0

    .line 47
    iput v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 48
    .line 49
    iput v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 50
    .line 51
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-le v0, v1, :cond_0

    .line 60
    .line 61
    iput v4, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 64
    .line 65
    const/4 v5, 0x0

    .line 66
    const/4 v7, 0x0

    .line 67
    const/4 v8, 0x0

    .line 68
    const/4 v9, 0x0

    .line 69
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getMaxScrollY()I

    .line 76
    .line 77
    .line 78
    move-result v10

    .line 79
    move v6, p2

    .line 80
    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_0
    iput v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 85
    .line 86
    iget-object v2, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 87
    .line 88
    const/4 v6, 0x0

    .line 89
    const/4 v7, 0x0

    .line 90
    iget-object p2, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 91
    .line 92
    invoke-virtual {p2}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    invoke-virtual {p2}, Lcom/intsig/office/ss/view/SheetView;->getMaxScrollX()I

    .line 97
    .line 98
    .line 99
    move-result v8

    .line 100
    const/4 v9, 0x0

    .line 101
    const/4 v10, 0x0

    .line 102
    move v5, p1

    .line 103
    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 104
    .line 105
    .line 106
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 109
    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 114
    .line 115
    .line 116
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/system/beans/AEventManage;->onLongPress(Landroid/view/MotionEvent;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->longPress:Z

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    iput v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldY:I

    .line 20
    .line 21
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    iput v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldX:I

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 28
    .line 29
    invoke-virtual {v3}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    int-to-float v4, v4

    .line 38
    cmpl-float v4, v4, v1

    .line 39
    .line 40
    if-lez v4, :cond_0

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    int-to-float v4, v4

    .line 47
    cmpg-float v4, v4, v2

    .line 48
    .line 49
    if-gez v4, :cond_0

    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->findChangingRowHeader(Landroid/view/MotionEvent;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    int-to-float v4, v4

    .line 60
    cmpg-float v1, v4, v1

    .line 61
    .line 62
    if-gez v1, :cond_1

    .line 63
    .line 64
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    int-to-float v1, v1

    .line 69
    cmpl-float v1, v1, v2

    .line 70
    .line 71
    if-lez v1, :cond_1

    .line 72
    .line 73
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/control/SSEventManage;->findChangingColumnHeader(Landroid/view/MotionEvent;)V

    .line 74
    .line 75
    .line 76
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 77
    .line 78
    if-eqz p1, :cond_2

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/office/ss/other/FocusCell;->clone()Lcom/intsig/office/ss/other/FocusCell;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 87
    .line 88
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    iget-object v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 93
    .line 94
    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/view/SheetView;->changeHeaderArea(Lcom/intsig/office/ss/other/FocusCell;)V

    .line 95
    .line 96
    .line 97
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/view/SheetView;->setDrawMovingHeaderLine(Z)V

    .line 104
    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 109
    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 112
    .line 113
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 114
    .line 115
    .line 116
    :cond_2
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/intsig/office/system/beans/AEventManage;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    const/4 v0, 0x0

    .line 15
    const/4 v1, 0x1

    .line 16
    const/high16 v2, 0x40000000    # 2.0f

    .line 17
    .line 18
    cmpl-float p2, p2, v2

    .line 19
    .line 20
    if-lez p2, :cond_0

    .line 21
    .line 22
    const/4 p2, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p2, 0x0

    .line 25
    const/4 p3, 0x0

    .line 26
    :goto_0
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    cmpl-float v2, v3, v2

    .line 31
    .line 32
    if-lez v2, :cond_1

    .line 33
    .line 34
    const/4 p2, 0x1

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 p4, 0x0

    .line 37
    :goto_1
    if-eqz p2, :cond_2

    .line 38
    .line 39
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 40
    .line 41
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getRowHeader()Lcom/intsig/office/ss/view/RowHeader;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/view/RowHeader;->calculateRowHeaderWidth(F)V

    .line 52
    .line 53
    .line 54
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 55
    .line 56
    .line 57
    move-result p2

    .line 58
    int-to-float p2, p2

    .line 59
    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result p3

    .line 63
    int-to-float p3, p3

    .line 64
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/ss/view/SheetView;->scrollBy(FF)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 75
    .line 76
    .line 77
    :cond_2
    return v1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/office/system/beans/AEventManage;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 8
    .line 9
    .line 10
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v2, 0x2

    .line 19
    const/4 v3, 0x1

    .line 20
    if-ne v0, v2, :cond_2

    .line 21
    .line 22
    iput-boolean v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 23
    .line 24
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->actionDown:Z

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 27
    .line 28
    if-eqz p1, :cond_1

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getSheetView()Lcom/intsig/office/ss/view/SheetView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/view/SheetView;->setDrawMovingHeaderLine(Z)V

    .line 37
    .line 38
    .line 39
    const/4 p1, 0x0

    .line 40
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->oldHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->newHeaderArea:Lcom/intsig/office/ss/other/FocusCell;

    .line 43
    .line 44
    :cond_1
    return v3

    .line 45
    :cond_2
    if-eqz p1, :cond_5

    .line 46
    .line 47
    if-eq p1, v3, :cond_4

    .line 48
    .line 49
    if-eq p1, v2, :cond_3

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_3
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/control/SSEventManage;->changingHeader(Landroid/view/MotionEvent;)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->abortDrawing()V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 61
    .line 62
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/control/SSEventManage;->actionUp(Landroid/view/MotionEvent;)V

    .line 67
    .line 68
    .line 69
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->scrolling:Z

    .line 70
    .line 71
    iput-boolean v1, p0, Lcom/intsig/office/ss/control/SSEventManage;->actionDown:Z

    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/office/ss/control/SSEventManage;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_5
    iput-boolean v3, p0, Lcom/intsig/office/ss/control/SSEventManage;->actionDown:Z

    .line 80
    .line 81
    :goto_0
    return v1
    .line 82
    .line 83
    .line 84
    .line 85
.end method

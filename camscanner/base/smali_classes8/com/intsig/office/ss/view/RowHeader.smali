.class public Lcom/intsig/office/ss/view/RowHeader;
.super Ljava/lang/Object;
.source "RowHeader.java"


# static fields
.field private static final EXTEDES_WIDTH:I = 0xa


# instance fields
.field private rect:Landroid/graphics/Rect;

.field private rowHeaderWidth:I

.field private sheetview:Lcom/intsig/office/ss/view/SheetView;

.field private y:F


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/view/SheetView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x32

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private drawFirstVisibleHeader(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V
    .locals 15

    .line 1
    move-object v0, p0

    .line 2
    move-object/from16 v7, p1

    .line 3
    .line 4
    move/from16 v1, p3

    .line 5
    .line 6
    move-object/from16 v8, p4

    .line 7
    .line 8
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 9
    .line 10
    .line 11
    move-result-object v9

    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 13
    .line 14
    .line 15
    iget-object v2, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 18
    .line 19
    .line 20
    move-result-object v10

    .line 21
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/SheetScroller;->getRowHeight()F

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    mul-float v11, v2, v1

    .line 26
    .line 27
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 28
    .line 29
    .line 30
    move-result-wide v2

    .line 31
    float-to-double v4, v1

    .line 32
    mul-double v2, v2, v4

    .line 33
    .line 34
    double-to-float v12, v2

    .line 35
    invoke-static {}, Lcom/intsig/office/ss/util/HeaderUtil;->instance()Lcom/intsig/office/ss/util/HeaderUtil;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    iget-object v2, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/ss/util/HeaderUtil;->isActiveRow(Lcom/intsig/office/ss/model/baseModel/Sheet;I)Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    sget v1, Lcom/intsig/office/constant/SSConstant;->ACTIVE_COLOR:I

    .line 56
    .line 57
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    sget v1, Lcom/intsig/office/constant/SSConstant;->HEADER_FILL_COLOR:I

    .line 62
    .line 63
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    .line 65
    .line 66
    :goto_0
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 67
    .line 68
    iget v2, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 69
    .line 70
    float-to-int v3, v2

    .line 71
    iget v4, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 72
    .line 73
    add-float/2addr v2, v12

    .line 74
    float-to-int v2, v2

    .line 75
    const/4 v5, 0x0

    .line 76
    invoke-virtual {v1, v5, v3, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 77
    .line 78
    .line 79
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 80
    .line 81
    invoke-virtual {v7, v1, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 82
    .line 83
    .line 84
    const v13, -0x382e27

    .line 85
    .line 86
    .line 87
    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    .line 89
    .line 90
    const/4 v2, 0x0

    .line 91
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 92
    .line 93
    const/high16 v14, 0x3f800000    # 1.0f

    .line 94
    .line 95
    add-float v5, v3, v14

    .line 96
    .line 97
    move-object/from16 v1, p1

    .line 98
    .line 99
    move/from16 v4, p2

    .line 100
    .line 101
    move-object/from16 v6, p4

    .line 102
    .line 103
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    .line 108
    .line 109
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 110
    .line 111
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 112
    .line 113
    int-to-float v4, v1

    .line 114
    add-float v5, v3, v14

    .line 115
    .line 116
    move-object/from16 v1, p1

    .line 117
    .line 118
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 122
    .line 123
    .line 124
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 125
    .line 126
    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 127
    .line 128
    .line 129
    const/high16 v1, -0x1000000

    .line 130
    .line 131
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    add-int/lit8 v1, v1, 0x1

    .line 139
    .line 140
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    float-to-int v2, v2

    .line 149
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 150
    .line 151
    sub-int/2addr v3, v2

    .line 152
    div-int/lit8 v3, v3, 0x2

    .line 153
    .line 154
    float-to-double v4, v11

    .line 155
    iget v2, v9, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 156
    .line 157
    iget v6, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 158
    .line 159
    sub-float/2addr v2, v6

    .line 160
    float-to-double v13, v2

    .line 161
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    .line 162
    .line 163
    .line 164
    move-result-wide v13

    .line 165
    sub-double/2addr v4, v13

    .line 166
    double-to-int v2, v4

    .line 167
    int-to-float v3, v3

    .line 168
    iget v4, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 169
    .line 170
    int-to-float v2, v2

    .line 171
    add-float/2addr v4, v2

    .line 172
    iget v2, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 173
    .line 174
    sub-float/2addr v4, v2

    .line 175
    sub-float/2addr v11, v12

    .line 176
    sub-float/2addr v4, v11

    .line 177
    invoke-virtual {v7, v1, v3, v4, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 181
    .line 182
    .line 183
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawRowLine(Landroid/graphics/Canvas;IIFLandroid/graphics/Paint;)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    move/from16 v8, p2

    .line 6
    .line 7
    move/from16 v9, p4

    .line 8
    .line 9
    move-object/from16 v10, p5

    .line 10
    .line 11
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 12
    .line 13
    .line 14
    move-result-object v11

    .line 15
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 16
    .line 17
    .line 18
    move-result-object v12

    .line 19
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 22
    .line 23
    .line 24
    move-result-object v13

    .line 25
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    move/from16 v3, p3

    .line 36
    .line 37
    if-le v2, v3, :cond_0

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    move v2, v3

    .line 45
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-nez v3, :cond_1

    .line 50
    .line 51
    int-to-float v3, v8

    .line 52
    invoke-direct {v0, v7, v3, v9, v10}, Lcom/intsig/office/ss/view/RowHeader;->drawFirstVisibleHeader(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V

    .line 53
    .line 54
    .line 55
    add-int/lit8 v2, v2, 0x1

    .line 56
    .line 57
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 58
    .line 59
    float-to-double v3, v3

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 61
    .line 62
    .line 63
    move-result-wide v5

    .line 64
    float-to-double v14, v9

    .line 65
    mul-double v5, v5, v14

    .line 66
    .line 67
    add-double/2addr v3, v5

    .line 68
    double-to-float v1, v3

    .line 69
    iput v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 70
    .line 71
    :cond_1
    invoke-virtual {v13}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    if-eqz v1, :cond_2

    .line 80
    .line 81
    const/high16 v1, 0x10000

    .line 82
    .line 83
    const/high16 v14, 0x10000

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_2
    const/high16 v1, 0x100000

    .line 87
    .line 88
    const/high16 v14, 0x100000

    .line 89
    .line 90
    :goto_1
    move v15, v2

    .line 91
    :goto_2
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 92
    .line 93
    iget v2, v12, Landroid/graphics/Rect;->bottom:I

    .line 94
    .line 95
    int-to-float v2, v2

    .line 96
    const/4 v6, 0x0

    .line 97
    const v5, -0x382e27

    .line 98
    .line 99
    .line 100
    const/high16 v16, 0x3f800000    # 1.0f

    .line 101
    .line 102
    cmpg-float v1, v1, v2

    .line 103
    .line 104
    if-gtz v1, :cond_6

    .line 105
    .line 106
    if-ge v15, v14, :cond_6

    .line 107
    .line 108
    invoke-virtual {v13, v15}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    if-eqz v1, :cond_3

    .line 113
    .line 114
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    if-eqz v2, :cond_3

    .line 119
    .line 120
    invoke-virtual {v10, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    .line 122
    .line 123
    const/4 v2, 0x0

    .line 124
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 125
    .line 126
    sub-float v3, v1, v16

    .line 127
    .line 128
    iget v4, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 129
    .line 130
    int-to-float v4, v4

    .line 131
    add-float v5, v1, v16

    .line 132
    .line 133
    move-object/from16 v1, p1

    .line 134
    .line 135
    move-object/from16 v6, p5

    .line 136
    .line 137
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 138
    .line 139
    .line 140
    add-int/lit8 v15, v15, 0x1

    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_3
    if-nez v1, :cond_4

    .line 144
    .line 145
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 146
    .line 147
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    int-to-float v1, v1

    .line 156
    goto :goto_3

    .line 157
    :cond_4
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 158
    .line 159
    .line 160
    move-result v1

    .line 161
    :goto_3
    mul-float v4, v1, v9

    .line 162
    .line 163
    invoke-static {}, Lcom/intsig/office/ss/util/HeaderUtil;->instance()Lcom/intsig/office/ss/util/HeaderUtil;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    iget-object v2, v0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 168
    .line 169
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    invoke-virtual {v1, v2, v15}, Lcom/intsig/office/ss/util/HeaderUtil;->isActiveRow(Lcom/intsig/office/ss/model/baseModel/Sheet;I)Z

    .line 174
    .line 175
    .line 176
    move-result v1

    .line 177
    if-eqz v1, :cond_5

    .line 178
    .line 179
    sget v1, Lcom/intsig/office/constant/SSConstant;->ACTIVE_COLOR:I

    .line 180
    .line 181
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    .line 183
    .line 184
    goto :goto_4

    .line 185
    :cond_5
    sget v1, Lcom/intsig/office/constant/SSConstant;->HEADER_FILL_COLOR:I

    .line 186
    .line 187
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 188
    .line 189
    .line 190
    :goto_4
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 191
    .line 192
    iget v2, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 193
    .line 194
    float-to-int v3, v2

    .line 195
    iget v5, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 196
    .line 197
    add-float/2addr v2, v4

    .line 198
    float-to-int v2, v2

    .line 199
    invoke-virtual {v1, v6, v3, v5, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 200
    .line 201
    .line 202
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 203
    .line 204
    invoke-virtual {v7, v1, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 205
    .line 206
    .line 207
    const v5, -0x382e27

    .line 208
    .line 209
    .line 210
    invoke-virtual {v10, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 211
    .line 212
    .line 213
    const/4 v2, 0x0

    .line 214
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 215
    .line 216
    int-to-float v6, v8

    .line 217
    add-float v17, v3, v16

    .line 218
    .line 219
    move-object/from16 v1, p1

    .line 220
    .line 221
    move/from16 v18, v4

    .line 222
    .line 223
    move v4, v6

    .line 224
    const v6, -0x382e27

    .line 225
    .line 226
    .line 227
    move/from16 v5, v17

    .line 228
    .line 229
    const v9, -0x382e27

    .line 230
    .line 231
    .line 232
    move-object/from16 v6, p5

    .line 233
    .line 234
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 235
    .line 236
    .line 237
    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 238
    .line 239
    .line 240
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 241
    .line 242
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 243
    .line 244
    int-to-float v4, v1

    .line 245
    add-float v5, v3, v16

    .line 246
    .line 247
    move-object/from16 v1, p1

    .line 248
    .line 249
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 250
    .line 251
    .line 252
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 253
    .line 254
    .line 255
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 256
    .line 257
    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 258
    .line 259
    .line 260
    const/high16 v1, -0x1000000

    .line 261
    .line 262
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 263
    .line 264
    .line 265
    add-int/lit8 v15, v15, 0x1

    .line 266
    .line 267
    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    float-to-int v2, v2

    .line 276
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 277
    .line 278
    sub-int/2addr v3, v2

    .line 279
    div-int/lit8 v3, v3, 0x2

    .line 280
    .line 281
    move/from16 v2, v18

    .line 282
    .line 283
    float-to-double v4, v2

    .line 284
    iget v6, v11, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 285
    .line 286
    iget v9, v11, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 287
    .line 288
    sub-float/2addr v6, v9

    .line 289
    move-object/from16 v17, v13

    .line 290
    .line 291
    move/from16 v18, v14

    .line 292
    .line 293
    float-to-double v13, v6

    .line 294
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    .line 295
    .line 296
    .line 297
    move-result-wide v13

    .line 298
    sub-double/2addr v4, v13

    .line 299
    double-to-int v4, v4

    .line 300
    int-to-float v3, v3

    .line 301
    iget v5, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 302
    .line 303
    int-to-float v4, v4

    .line 304
    add-float/2addr v5, v4

    .line 305
    iget v4, v11, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 306
    .line 307
    sub-float/2addr v5, v4

    .line 308
    invoke-virtual {v7, v1, v3, v5, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 309
    .line 310
    .line 311
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 312
    .line 313
    .line 314
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 315
    .line 316
    add-float/2addr v1, v2

    .line 317
    iput v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 318
    .line 319
    move/from16 v9, p4

    .line 320
    .line 321
    move-object/from16 v13, v17

    .line 322
    .line 323
    move/from16 v14, v18

    .line 324
    .line 325
    goto/16 :goto_2

    .line 326
    .line 327
    :cond_6
    const v9, -0x382e27

    .line 328
    .line 329
    .line 330
    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 331
    .line 332
    .line 333
    const/4 v2, 0x0

    .line 334
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 335
    .line 336
    int-to-float v4, v8

    .line 337
    add-float v5, v3, v16

    .line 338
    .line 339
    move-object/from16 v1, p1

    .line 340
    .line 341
    const/4 v8, 0x0

    .line 342
    move-object/from16 v6, p5

    .line 343
    .line 344
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 348
    .line 349
    .line 350
    iget v3, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 351
    .line 352
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 353
    .line 354
    int-to-float v4, v1

    .line 355
    add-float v5, v3, v16

    .line 356
    .line 357
    move-object/from16 v1, p1

    .line 358
    .line 359
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 360
    .line 361
    .line 362
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 363
    .line 364
    iget v2, v12, Landroid/graphics/Rect;->bottom:I

    .line 365
    .line 366
    int-to-float v2, v2

    .line 367
    cmpg-float v1, v1, v2

    .line 368
    .line 369
    if-gez v1, :cond_7

    .line 370
    .line 371
    sget v1, Lcom/intsig/office/constant/SSConstant;->HEADER_FILL_COLOR:I

    .line 372
    .line 373
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 374
    .line 375
    .line 376
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 377
    .line 378
    iget v2, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 379
    .line 380
    add-float v2, v2, v16

    .line 381
    .line 382
    float-to-int v2, v2

    .line 383
    iget v3, v12, Landroid/graphics/Rect;->right:I

    .line 384
    .line 385
    iget v4, v12, Landroid/graphics/Rect;->bottom:I

    .line 386
    .line 387
    invoke-virtual {v1, v8, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 388
    .line 389
    .line 390
    iget-object v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 391
    .line 392
    invoke-virtual {v7, v1, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 393
    .line 394
    .line 395
    :cond_7
    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 396
    .line 397
    .line 398
    iget v1, v0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 399
    .line 400
    int-to-float v2, v1

    .line 401
    const/4 v3, 0x0

    .line 402
    add-int/lit8 v1, v1, 0x1

    .line 403
    .line 404
    int-to-float v4, v1

    .line 405
    iget v5, v0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 406
    .line 407
    move-object/from16 v1, p1

    .line 408
    .line 409
    move-object/from16 v6, p5

    .line 410
    .line 411
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 412
    .line 413
    .line 414
    return-void
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private layoutRowLine(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object p4, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {p4}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object p4

    .line 11
    iget-object v0, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-le v1, p2, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-nez v1, :cond_1

    .line 32
    .line 33
    add-int/lit8 p2, p2, 0x1

    .line 34
    .line 35
    iget v1, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 36
    .line 37
    float-to-double v1, v1

    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 39
    .line 40
    .line 41
    move-result-wide v3

    .line 42
    float-to-double v5, p3

    .line 43
    mul-double v3, v3, v5

    .line 44
    .line 45
    add-double/2addr v1, v3

    .line 46
    double-to-float v0, v1

    .line 47
    iput v0, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 48
    .line 49
    :cond_1
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_2

    .line 58
    .line 59
    const/high16 v0, 0x10000

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    const/high16 v0, 0x100000

    .line 63
    .line 64
    :goto_0
    iget v1, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 65
    .line 66
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 67
    .line 68
    int-to-float v2, v2

    .line 69
    cmpg-float v1, v1, v2

    .line 70
    .line 71
    if-gtz v1, :cond_5

    .line 72
    .line 73
    if-ge p2, v0, :cond_5

    .line 74
    .line 75
    invoke-virtual {p4, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    if-eqz v1, :cond_3

    .line 80
    .line 81
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    if-eqz v2, :cond_3

    .line 86
    .line 87
    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_3
    if-nez v1, :cond_4

    .line 91
    .line 92
    iget-object v1, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 93
    .line 94
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    int-to-float v1, v1

    .line 103
    goto :goto_2

    .line 104
    :cond_4
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    :goto_2
    mul-float v1, v1, p3

    .line 109
    .line 110
    iget v2, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 111
    .line 112
    add-float/2addr v2, v1

    .line 113
    iput v2, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 114
    .line 115
    goto :goto_1

    .line 116
    :cond_5
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method public calculateRowHeaderWidth(F)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/high16 v1, 0x41800000    # 16.0f

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentMinRow()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    add-int/lit8 v0, v0, 0xa

    .line 33
    .line 34
    iput v0, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 35
    .line 36
    const/16 v1, 0x32

    .line 37
    .line 38
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v0, v0

    .line 43
    mul-float v0, v0, p1

    .line 44
    .line 45
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    iput p1, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/view/RowHeader;->sheetview:Lcom/intsig/office/ss/view/SheetView;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public draw(Landroid/graphics/Canvas;IF)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 13
    .line 14
    .line 15
    move-result v7

    .line 16
    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    .line 17
    .line 18
    .line 19
    move-result v8

    .line 20
    const/high16 v1, 0x41800000    # 16.0f

    .line 21
    .line 22
    mul-float v1, v1, p3

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 25
    .line 26
    .line 27
    const/high16 v1, 0x41f00000    # 30.0f

    .line 28
    .line 29
    mul-float v1, v1, p3

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    iput-object v1, p0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 38
    .line 39
    iget v2, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 40
    .line 41
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 42
    .line 43
    const/4 v4, 0x0

    .line 44
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 45
    .line 46
    .line 47
    sget v1, Lcom/intsig/office/constant/SSConstant;->HEADER_FILL_COLOR:I

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/office/ss/view/RowHeader;->rect:Landroid/graphics/Rect;

    .line 53
    .line 54
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 55
    .line 56
    .line 57
    move-object v1, p0

    .line 58
    move-object v2, p1

    .line 59
    move v3, p2

    .line 60
    move v5, p3

    .line 61
    move-object v6, v0

    .line 62
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/view/RowHeader;->drawRowLine(Landroid/graphics/Canvas;IIFLandroid/graphics/Paint;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public getRowBottomBound(Landroid/graphics/Canvas;F)I
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v1}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/high16 v2, 0x41800000    # 16.0f

    .line 17
    .line 18
    mul-float v2, v2, p2

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 21
    .line 22
    .line 23
    const/high16 v2, 0x41f00000    # 30.0f

    .line 24
    .line 25
    mul-float v2, v2, p2

    .line 26
    .line 27
    iput v2, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 28
    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-direct {p0, p1, v2, p2, v1}, Lcom/intsig/office/ss/view/RowHeader;->layoutRowLine(Landroid/graphics/Canvas;IFLandroid/graphics/Paint;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 34
    .line 35
    .line 36
    iget p1, p0, Lcom/intsig/office/ss/view/RowHeader;->y:F

    .line 37
    .line 38
    float-to-int p1, p1

    .line 39
    iget p2, v0, Landroid/graphics/Rect;->bottom:I

    .line 40
    .line 41
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    return p1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getRowHeaderWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setRowHeaderWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/view/RowHeader;->rowHeaderWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

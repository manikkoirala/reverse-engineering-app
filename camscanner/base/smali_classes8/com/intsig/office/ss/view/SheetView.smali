.class public Lcom/intsig/office/ss/view/SheetView;
.super Ljava/lang/Object;
.source "SheetView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/ss/view/SheetView$ExtendCell;
    }
.end annotation


# static fields
.field public static final MAXCOLUMN_03:I = 0x100

.field public static final MAXCOLUMN_07:I = 0x4000

.field public static final MAXROW_03:I = 0x10000

.field public static final MAXROW_07:I = 0x100000


# instance fields
.field private cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

.field private cellView:Lcom/intsig/office/ss/view/CellView;

.field private clipRect:Landroid/graphics/Rect;

.field private columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

.field private effects:Landroid/graphics/PathEffect;

.field private extendCell:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/ss/view/SheetView$ExtendCell;",
            ">;"
        }
    .end annotation
.end field

.field private findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

.field private isDrawMovingHeaderLine:Z

.field private rowHeader:Lcom/intsig/office/ss/view/RowHeader;

.field private scrollX:F

.field private scrollY:F

.field private selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

.field private selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

.field private shapeView:Lcom/intsig/office/ss/view/ShapeView;

.field private sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

.field private sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

.field private spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

.field private tableFormatView:Lcom/intsig/office/ss/view/TableFormatView;

.field private zoom:F


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/control/Spreadsheet;Lcom/intsig/office/ss/model/baseModel/Sheet;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, 0x40000000    # 2.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/ss/other/SheetScroller;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/office/ss/other/SheetScroller;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 17
    .line 18
    new-instance v0, Landroid/graphics/DashPathEffect;

    .line 19
    .line 20
    const/4 v1, 0x4

    .line 21
    new-array v1, v1, [F

    .line 22
    .line 23
    fill-array-data v1, :array_0

    .line 24
    .line 25
    .line 26
    const/high16 v2, 0x3f800000    # 1.0f

    .line 27
    .line 28
    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->effects:Landroid/graphics/PathEffect;

    .line 32
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 39
    .line 40
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 41
    .line 42
    iput-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 43
    .line 44
    new-instance p1, Lcom/intsig/office/ss/view/RowHeader;

    .line 45
    .line 46
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/view/RowHeader;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 50
    .line 51
    new-instance p1, Lcom/intsig/office/ss/view/ColumnHeader;

    .line 52
    .line 53
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/view/ColumnHeader;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 54
    .line 55
    .line 56
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 57
    .line 58
    new-instance p1, Lcom/intsig/office/ss/view/ShapeView;

    .line 59
    .line 60
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/view/ShapeView;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 61
    .line 62
    .line 63
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->shapeView:Lcom/intsig/office/ss/view/ShapeView;

    .line 64
    .line 65
    new-instance p1, Lcom/intsig/office/ss/view/TableFormatView;

    .line 66
    .line 67
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/view/TableFormatView;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 68
    .line 69
    .line 70
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->tableFormatView:Lcom/intsig/office/ss/view/TableFormatView;

    .line 71
    .line 72
    new-instance p1, Lcom/intsig/office/ss/view/CellView;

    .line 73
    .line 74
    invoke-direct {p1, p0}, Lcom/intsig/office/ss/view/CellView;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 75
    .line 76
    .line 77
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 78
    .line 79
    new-instance p1, Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 80
    .line 81
    const/4 p2, 0x0

    .line 82
    invoke-direct {p1, p2, p2, p2, p2}, Lcom/intsig/office/ss/model/CellRangeAddress;-><init>(IIII)V

    .line 83
    .line 84
    .line 85
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 86
    .line 87
    new-instance p1, Lcom/intsig/office/ss/other/DrawingCell;

    .line 88
    .line 89
    invoke-direct {p1}, Lcom/intsig/office/ss/other/DrawingCell;-><init>()V

    .line 90
    .line 91
    .line 92
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/office/ss/view/SheetView;->initForDrawing()V

    .line 95
    .line 96
    .line 97
    return-void

    .line 98
    nop

    .line 99
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private drawActiveCellBorder(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 12
    .line 13
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v0, p0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/view/SheetView;II)Landroid/graphics/RectF;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, p1, v0, v2}, Lcom/intsig/office/ss/view/CellView;->drawActiveCellBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;S)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private drawCells(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Row;)V
    .locals 8

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 16
    .line 17
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 18
    .line 19
    mul-float v0, v0, v2

    .line 20
    .line 21
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/other/DrawingCell;->setHeight(F)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-ne v0, v1, :cond_2

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->isRowAllVisible()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    .line 52
    .line 53
    .line 54
    move-result-wide v1

    .line 55
    double-to-float v1, v1

    .line 56
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 57
    .line 58
    mul-float v1, v1, v2

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 61
    .line 62
    .line 63
    goto :goto_2

    .line 64
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleHeight(F)V

    .line 71
    .line 72
    .line 73
    :goto_2
    if-nez p2, :cond_3

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isAccomplished()Z

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    :cond_3
    if-eqz p2, :cond_f

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isAccomplished()Z

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    if-nez v0, :cond_4

    .line 104
    .line 105
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Row;->isCompleted()Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-nez v0, :cond_4

    .line 110
    .line 111
    goto/16 :goto_b

    .line 112
    .line 113
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 114
    .line 115
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    int-to-float v1, v1

    .line 122
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/DrawingCell;->setLeft(F)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 128
    .line 129
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/DrawingCell;->setColumnIndex(I)V

    .line 134
    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 137
    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 143
    .line 144
    .line 145
    move-result v1

    .line 146
    if-eqz v1, :cond_5

    .line 147
    .line 148
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    check-cast v1, Lcom/intsig/office/ss/view/SheetView$ExtendCell;

    .line 153
    .line 154
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->dispose()V

    .line 155
    .line 156
    .line 157
    goto :goto_3

    .line 158
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 159
    .line 160
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 161
    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 164
    .line 165
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->isAccomplished()Z

    .line 166
    .line 167
    .line 168
    move-result v0

    .line 169
    if-eqz v0, :cond_6

    .line 170
    .line 171
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Row;->isInitExpandedRangeAddress()Z

    .line 172
    .line 173
    .line 174
    move-result v0

    .line 175
    if-nez v0, :cond_6

    .line 176
    .line 177
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/view/SheetView;->initRowExtendedRangeAddress(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 178
    .line 179
    .line 180
    const/4 v0, 0x1

    .line 181
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->setInitExpandedRangeAddress(Z)V

    .line 182
    .line 183
    .line 184
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 189
    .line 190
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getMaxColumn()I

    .line 195
    .line 196
    .line 197
    move-result v1

    .line 198
    :goto_4
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 199
    .line 200
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 201
    .line 202
    .line 203
    move-result v2

    .line 204
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 205
    .line 206
    int-to-float v3, v3

    .line 207
    const/4 v4, 0x0

    .line 208
    cmpg-float v2, v2, v3

    .line 209
    .line 210
    if-gtz v2, :cond_c

    .line 211
    .line 212
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 213
    .line 214
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 215
    .line 216
    .line 217
    move-result v2

    .line 218
    if-ge v2, v1, :cond_c

    .line 219
    .line 220
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 221
    .line 222
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 223
    .line 224
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 225
    .line 226
    .line 227
    move-result v3

    .line 228
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnInfo(I)Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 229
    .line 230
    .line 231
    move-result-object v2

    .line 232
    if-eqz v2, :cond_7

    .line 233
    .line 234
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 235
    .line 236
    .line 237
    move-result v3

    .line 238
    if-eqz v3, :cond_7

    .line 239
    .line 240
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 241
    .line 242
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseColumn()V

    .line 243
    .line 244
    .line 245
    goto :goto_4

    .line 246
    :cond_7
    if-eqz v2, :cond_8

    .line 247
    .line 248
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 249
    .line 250
    .line 251
    move-result v2

    .line 252
    goto :goto_5

    .line 253
    :cond_8
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 254
    .line 255
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultColWidth()I

    .line 256
    .line 257
    .line 258
    move-result v2

    .line 259
    int-to-float v2, v2

    .line 260
    :goto_5
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 261
    .line 262
    iget v5, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 263
    .line 264
    mul-float v2, v2, v5

    .line 265
    .line 266
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setWidth(F)V

    .line 267
    .line 268
    .line 269
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 270
    .line 271
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 276
    .line 277
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 278
    .line 279
    .line 280
    move-result v3

    .line 281
    if-ne v2, v3, :cond_a

    .line 282
    .line 283
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 284
    .line 285
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->isColumnAllVisible()Z

    .line 286
    .line 287
    .line 288
    move-result v2

    .line 289
    if-eqz v2, :cond_9

    .line 290
    .line 291
    goto :goto_6

    .line 292
    :cond_9
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 293
    .line 294
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 295
    .line 296
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    .line 297
    .line 298
    .line 299
    move-result-wide v5

    .line 300
    double-to-float v3, v5

    .line 301
    iget v5, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 302
    .line 303
    mul-float v3, v3, v5

    .line 304
    .line 305
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 306
    .line 307
    .line 308
    goto :goto_7

    .line 309
    :cond_a
    :goto_6
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 310
    .line 311
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 312
    .line 313
    .line 314
    move-result v3

    .line 315
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/other/DrawingCell;->setVisibleWidth(F)V

    .line 316
    .line 317
    .line 318
    :goto_7
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 319
    .line 320
    if-eqz v2, :cond_b

    .line 321
    .line 322
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/FindingMgr;->getValue()Ljava/lang/String;

    .line 323
    .line 324
    .line 325
    move-result-object v4

    .line 326
    :cond_b
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 327
    .line 328
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 329
    .line 330
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 331
    .line 332
    .line 333
    move-result v3

    .line 334
    invoke-virtual {p2, v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 335
    .line 336
    .line 337
    move-result-object v3

    .line 338
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 339
    .line 340
    invoke-virtual {v2, p1, v3, v5, v4}, Lcom/intsig/office/ss/view/CellView;->draw(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/other/DrawingCell;Ljava/lang/String;)V

    .line 341
    .line 342
    .line 343
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 344
    .line 345
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseLeftWithVisibleWidth()V

    .line 346
    .line 347
    .line 348
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 349
    .line 350
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseColumn()V

    .line 351
    .line 352
    .line 353
    goto/16 :goto_4

    .line 354
    .line 355
    :cond_c
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 356
    .line 357
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 358
    .line 359
    .line 360
    move-result-object p2

    .line 361
    :goto_8
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 362
    .line 363
    .line 364
    move-result v0

    .line 365
    if-eqz v0, :cond_f

    .line 366
    .line 367
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 368
    .line 369
    .line 370
    move-result-object v0

    .line 371
    check-cast v0, Lcom/intsig/office/ss/view/SheetView$ExtendCell;

    .line 372
    .line 373
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 374
    .line 375
    .line 376
    move-result-object v1

    .line 377
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getTableInfo()Lcom/intsig/office/ss/model/table/SSTable;

    .line 378
    .line 379
    .line 380
    move-result-object v2

    .line 381
    if-eqz v2, :cond_d

    .line 382
    .line 383
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 384
    .line 385
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 386
    .line 387
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 388
    .line 389
    .line 390
    move-result-object v5

    .line 391
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 392
    .line 393
    .line 394
    move-result v6

    .line 395
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 396
    .line 397
    .line 398
    move-result v7

    .line 399
    invoke-virtual {v3, v2, v5, v6, v7}, Lcom/intsig/office/ss/view/CellView;->getTableCellStyle(Lcom/intsig/office/ss/model/table/SSTable;Lcom/intsig/office/ss/model/baseModel/Workbook;II)Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 400
    .line 401
    .line 402
    move-result-object v2

    .line 403
    goto :goto_9

    .line 404
    :cond_d
    move-object v2, v4

    .line 405
    :goto_9
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontKit;->instance()Lcom/intsig/office/simpletext/font/FontKit;

    .line 406
    .line 407
    .line 408
    move-result-object v3

    .line 409
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 410
    .line 411
    .line 412
    move-result-object v5

    .line 413
    invoke-virtual {v5}, Lcom/intsig/office/ss/control/Spreadsheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 414
    .line 415
    .line 416
    move-result-object v5

    .line 417
    invoke-virtual {v3, v1, v5, v2}, Lcom/intsig/office/simpletext/font/FontKit;->getCellPaint(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)Landroid/graphics/Paint;

    .line 418
    .line 419
    .line 420
    move-result-object v1

    .line 421
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 422
    .line 423
    .line 424
    invoke-static {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->〇080(Lcom/intsig/office/ss/view/SheetView$ExtendCell;)Landroid/graphics/RectF;

    .line 425
    .line 426
    .line 427
    move-result-object v2

    .line 428
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 429
    .line 430
    .line 431
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getContent()Ljava/lang/Object;

    .line 432
    .line 433
    .line 434
    move-result-object v2

    .line 435
    instance-of v3, v2, Ljava/lang/String;

    .line 436
    .line 437
    if-eqz v3, :cond_e

    .line 438
    .line 439
    invoke-virtual {v1}, Landroid/graphics/Paint;->getTextSize()F

    .line 440
    .line 441
    .line 442
    move-result v3

    .line 443
    iget v5, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 444
    .line 445
    mul-float v5, v5, v3

    .line 446
    .line 447
    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 448
    .line 449
    .line 450
    check-cast v2, Ljava/lang/String;

    .line 451
    .line 452
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getX()F

    .line 453
    .line 454
    .line 455
    move-result v5

    .line 456
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getY()F

    .line 457
    .line 458
    .line 459
    move-result v0

    .line 460
    invoke-virtual {p1, v2, v5, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 461
    .line 462
    .line 463
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 464
    .line 465
    .line 466
    goto :goto_a

    .line 467
    :cond_e
    check-cast v2, Lcom/intsig/office/simpletext/view/STRoot;

    .line 468
    .line 469
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getX()F

    .line 470
    .line 471
    .line 472
    move-result v1

    .line 473
    float-to-int v1, v1

    .line 474
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;->getY()F

    .line 475
    .line 476
    .line 477
    move-result v0

    .line 478
    float-to-int v0, v0

    .line 479
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 480
    .line 481
    invoke-virtual {v2, p1, v1, v0, v3}, Lcom/intsig/office/simpletext/view/STRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 482
    .line 483
    .line 484
    :goto_a
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 485
    .line 486
    .line 487
    goto :goto_8

    .line 488
    :cond_f
    :goto_b
    return-void
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private drawMovingHeaderLine(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/view/SheetView;->isDrawMovingHeaderLine:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {v0}, Landroid/graphics/Paint;->getPathEffect()Landroid/graphics/PathEffect;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    const/high16 v4, -0x1000000

    .line 30
    .line 31
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    .line 33
    .line 34
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 35
    .line 36
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    .line 38
    .line 39
    new-instance v4, Landroid/graphics/Path;

    .line 40
    .line 41
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 42
    .line 43
    .line 44
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 45
    .line 46
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getType()I

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    const/4 v6, 0x1

    .line 51
    const/4 v7, 0x0

    .line 52
    if-ne v5, v6, :cond_0

    .line 53
    .line 54
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 55
    .line 56
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 61
    .line 62
    int-to-float v5, v5

    .line 63
    invoke-virtual {v4, v7, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 64
    .line 65
    .line 66
    iget v3, v3, Landroid/graphics/Rect;->right:I

    .line 67
    .line 68
    int-to-float v3, v3

    .line 69
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 70
    .line 71
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 72
    .line 73
    .line 74
    move-result-object v5

    .line 75
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 76
    .line 77
    int-to-float v5, v5

    .line 78
    invoke-virtual {v4, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_0
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 83
    .line 84
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getType()I

    .line 85
    .line 86
    .line 87
    move-result v5

    .line 88
    const/4 v6, 0x2

    .line 89
    if-ne v5, v6, :cond_1

    .line 90
    .line 91
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 92
    .line 93
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    iget v5, v5, Landroid/graphics/Rect;->right:I

    .line 98
    .line 99
    int-to-float v5, v5

    .line 100
    invoke-virtual {v4, v5, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 101
    .line 102
    .line 103
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 104
    .line 105
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/FocusCell;->getRect()Landroid/graphics/Rect;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    iget v5, v5, Landroid/graphics/Rect;->right:I

    .line 110
    .line 111
    int-to-float v5, v5

    .line 112
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    .line 113
    .line 114
    int-to-float v3, v3

    .line 115
    invoke-virtual {v4, v5, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 116
    .line 117
    .line 118
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->effects:Landroid/graphics/PathEffect;

    .line 119
    .line 120
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 121
    .line 122
    .line 123
    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 127
    .line 128
    .line 129
    sget-object p1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 130
    .line 131
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    .line 136
    .line 137
    :cond_2
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private drawRows(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 8
    .line 9
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    int-to-float v2, v2

    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setTop(F)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/DrawingCell;->setRowIndex(I)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getMaxRow()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->isAbortDrawing()Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-nez v2, :cond_1

    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 53
    .line 54
    int-to-float v3, v3

    .line 55
    cmpg-float v2, v2, v3

    .line 56
    .line 57
    if-gtz v2, :cond_1

    .line 58
    .line 59
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 60
    .line 61
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-ge v2, v1, :cond_1

    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 68
    .line 69
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 70
    .line 71
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    if-eqz v2, :cond_0

    .line 80
    .line 81
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    if-eqz v3, :cond_0

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseRow()V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/ss/view/SheetView;->drawCells(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 94
    .line 95
    .line 96
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 97
    .line 98
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseTopWithVisibleHeight()V

    .line 99
    .line 100
    .line 101
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 102
    .line 103
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->increaseRow()V

    .line 104
    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_1
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private drawThumbnail(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->startDrawing()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 15
    .line 16
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnRightBound(Landroid/graphics/Canvas;F)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 21
    .line 22
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 23
    .line 24
    invoke-virtual {v1, p1, v2}, Lcom/intsig/office/ss/view/RowHeader;->getRowBottomBound(Landroid/graphics/Canvas;F)I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    .line 29
    .line 30
    iget v3, v2, Landroid/graphics/Rect;->right:I

    .line 31
    .line 32
    add-int/lit8 v4, v3, 0xa

    .line 33
    .line 34
    if-ge v0, v3, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    move v0, v4

    .line 38
    :goto_0
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 39
    .line 40
    add-int/lit8 v3, v2, 0x32

    .line 41
    .line 42
    if-ge v1, v2, :cond_1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    move v1, v3

    .line 46
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 47
    .line 48
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 49
    .line 50
    invoke-virtual {v2, p1, v0, v3}, Lcom/intsig/office/ss/view/RowHeader;->draw(Landroid/graphics/Canvas;IF)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 54
    .line 55
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 56
    .line 57
    invoke-virtual {v2, p1, v1, v3}, Lcom/intsig/office/ss/view/ColumnHeader;->draw(Landroid/graphics/Canvas;IF)V

    .line 58
    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    int-to-float v2, v2

    .line 67
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 68
    .line 69
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    int-to-float v3, v3

    .line 74
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 75
    .line 76
    .line 77
    int-to-float v0, v0

    .line 78
    int-to-float v1, v1

    .line 79
    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 80
    .line 81
    .line 82
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/SheetView;->drawRows(Landroid/graphics/Canvas;)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->tableFormatView:Lcom/intsig/office/ss/view/TableFormatView;

    .line 86
    .line 87
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/TableFormatView;->draw(Landroid/graphics/Canvas;)V

    .line 88
    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->shapeView:Lcom/intsig/office/ss/view/ShapeView;

    .line 91
    .line 92
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/ShapeView;->draw(Landroid/graphics/Canvas;)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 96
    .line 97
    .line 98
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getExtendTextLeftBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I
    .locals 3

    .line 1
    add-int/lit8 p2, p2, -0x1

    .line 2
    .line 3
    :goto_0
    if-ltz p2, :cond_2

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    cmpl-float v0, p3, v0

    .line 7
    .line 8
    if-lez v0, :cond_2

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(IZ)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-gez v1, :cond_0

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v1, v2, v0}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_0

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 47
    .line 48
    return p2

    .line 49
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 50
    .line 51
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 56
    .line 57
    mul-float v0, v0, v1

    .line 58
    .line 59
    sub-float/2addr p3, v0

    .line 60
    add-int/lit8 p2, p2, -0x1

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    add-int/lit8 p2, p2, 0x1

    .line 64
    .line 65
    return p2
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getExtendTextRightBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I
    .locals 3

    .line 1
    :goto_0
    add-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    cmpl-float v0, p3, v0

    .line 5
    .line 6
    if-lez v0, :cond_2

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(IZ)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-gez v1, :cond_0

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v1, v2, v0}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 45
    .line 46
    return p2

    .line 47
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 48
    .line 49
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 54
    .line 55
    mul-float v0, v0, v1

    .line 56
    .line 57
    sub-float/2addr p3, v0

    .line 58
    goto :goto_0

    .line 59
    :cond_2
    add-int/lit8 p2, p2, -0x1

    .line 60
    .line 61
    return p2
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private initForDrawing()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollX()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    iput v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollY()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    int-to-float v0, v0

    .line 17
    iput v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 22
    .line 23
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 24
    .line 25
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 30
    .line 31
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getZoom()F

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const/4 v1, 0x1

    .line 45
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/ss/view/SheetView;->selectedCell(II)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const v1, 0x20000007

    .line 70
    .line 71
    .line 72
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 73
    .line 74
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private initRowExtendedRangeAddress(Lcom/intsig/office/ss/model/baseModel/Row;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->cellCollection()Ljava/util/Collection;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    const/4 v4, 0x0

    .line 18
    if-eqz v3, :cond_b

    .line 19
    .line 20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    move-object v6, v3

    .line 25
    check-cast v6, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 26
    .line 27
    iget-object v3, v0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 28
    .line 29
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    iget v5, v0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 38
    .line 39
    mul-float v3, v3, v5

    .line 40
    .line 41
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 42
    .line 43
    .line 44
    move-result-object v5

    .line 45
    if-eqz v5, :cond_1

    .line 46
    .line 47
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    if-nez v5, :cond_0

    .line 56
    .line 57
    :cond_1
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    const/4 v7, 0x4

    .line 62
    if-eq v5, v7, :cond_0

    .line 63
    .line 64
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    const/16 v7, 0xa

    .line 69
    .line 70
    if-eq v5, v7, :cond_0

    .line 71
    .line 72
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    if-nez v5, :cond_2

    .line 77
    .line 78
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    const/16 v7, 0xb

    .line 83
    .line 84
    if-eq v5, v7, :cond_2

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 88
    .line 89
    .line 90
    move-result-object v5

    .line 91
    if-eqz v5, :cond_3

    .line 92
    .line 93
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 94
    .line 95
    .line 96
    move-result v8

    .line 97
    invoke-virtual {v0, v8}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidth(I)I

    .line 98
    .line 99
    .line 100
    move-result v8

    .line 101
    int-to-float v8, v8

    .line 102
    goto :goto_1

    .line 103
    :cond_3
    const/4 v8, 0x0

    .line 104
    :goto_1
    invoke-static {v6}, Lcom/intsig/office/ss/view/CellView;->isComplexText(Lcom/intsig/office/ss/model/baseModel/Cell;)Z

    .line 105
    .line 106
    .line 107
    move-result v9

    .line 108
    const/high16 v10, 0x40000000    # 2.0f

    .line 109
    .line 110
    if-eqz v9, :cond_7

    .line 111
    .line 112
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 113
    .line 114
    .line 115
    move-result-object v9

    .line 116
    iget-object v11, v0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 117
    .line 118
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 119
    .line 120
    .line 121
    move-result v12

    .line 122
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 123
    .line 124
    .line 125
    move-result v13

    .line 126
    invoke-virtual {v9, v11, v12, v13}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;II)Landroid/graphics/Rect;

    .line 127
    .line 128
    .line 129
    move-result-object v9

    .line 130
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 131
    .line 132
    .line 133
    move-result-object v11

    .line 134
    invoke-virtual {v11}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 135
    .line 136
    .line 137
    move-result-object v11

    .line 138
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 139
    .line 140
    .line 141
    move-result v12

    .line 142
    invoke-virtual {v11, v12}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 143
    .line 144
    .line 145
    move-result-object v11

    .line 146
    check-cast v11, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 147
    .line 148
    if-eqz v11, :cond_0

    .line 149
    .line 150
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 151
    .line 152
    .line 153
    move-result-wide v12

    .line 154
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 155
    .line 156
    .line 157
    move-result-wide v14

    .line 158
    sub-long/2addr v12, v14

    .line 159
    const-wide/16 v14, 0x0

    .line 160
    .line 161
    cmp-long v16, v12, v14

    .line 162
    .line 163
    if-nez v16, :cond_4

    .line 164
    .line 165
    goto/16 :goto_0

    .line 166
    .line 167
    :cond_4
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/model/SectionElement;->getParaCollection()Lcom/intsig/office/simpletext/model/IElementCollection;

    .line 168
    .line 169
    .line 170
    move-result-object v12

    .line 171
    new-instance v13, Ljava/util/ArrayList;

    .line 172
    .line 173
    invoke-interface {v12}, Lcom/intsig/office/simpletext/model/IElementCollection;->size()I

    .line 174
    .line 175
    .line 176
    move-result v14

    .line 177
    invoke-direct {v13, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 178
    .line 179
    .line 180
    const/4 v14, 0x0

    .line 181
    :goto_2
    invoke-interface {v12}, Lcom/intsig/office/simpletext/model/IElementCollection;->size()I

    .line 182
    .line 183
    .line 184
    move-result v15

    .line 185
    if-ge v14, v15, :cond_5

    .line 186
    .line 187
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 188
    .line 189
    .line 190
    move-result-object v15

    .line 191
    invoke-interface {v12, v14}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 192
    .line 193
    .line 194
    move-result-object v16

    .line 195
    invoke-interface/range {v16 .. v16}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 196
    .line 197
    .line 198
    move-result-object v7

    .line 199
    invoke-virtual {v15, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 200
    .line 201
    .line 202
    move-result v7

    .line 203
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 204
    .line 205
    .line 206
    move-result-object v7

    .line 207
    invoke-interface {v13, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 211
    .line 212
    .line 213
    move-result-object v7

    .line 214
    invoke-interface {v12, v14}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 215
    .line 216
    .line 217
    move-result-object v15

    .line 218
    invoke-interface {v15}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 219
    .line 220
    .line 221
    move-result-object v15

    .line 222
    invoke-virtual {v7, v15, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 223
    .line 224
    .line 225
    add-int/lit8 v14, v14, 0x1

    .line 226
    .line 227
    goto :goto_2

    .line 228
    :cond_5
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 229
    .line 230
    .line 231
    move-result-object v7

    .line 232
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 233
    .line 234
    .line 235
    move-result-object v14

    .line 236
    const/high16 v15, 0x50f00000

    .line 237
    .line 238
    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    .line 239
    .line 240
    .line 241
    move-result v15

    .line 242
    invoke-virtual {v14, v7, v15}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 243
    .line 244
    .line 245
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 246
    .line 247
    .line 248
    move-result-object v14

    .line 249
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    .line 250
    .line 251
    .line 252
    move-result v9

    .line 253
    int-to-float v9, v9

    .line 254
    const/high16 v15, 0x41700000    # 15.0f

    .line 255
    .line 256
    mul-float v9, v9, v15

    .line 257
    .line 258
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 259
    .line 260
    .line 261
    move-result v9

    .line 262
    invoke-virtual {v14, v7, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 263
    .line 264
    .line 265
    new-instance v9, Lcom/intsig/office/simpletext/model/STDocument;

    .line 266
    .line 267
    invoke-direct {v9}, Lcom/intsig/office/simpletext/model/STDocument;-><init>()V

    .line 268
    .line 269
    .line 270
    invoke-interface {v9, v11}, Lcom/intsig/office/simpletext/model/IDocument;->appendSection(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 271
    .line 272
    .line 273
    new-instance v11, Lcom/intsig/office/simpletext/view/STRoot;

    .line 274
    .line 275
    iget-object v14, v0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 276
    .line 277
    invoke-virtual {v14}, Lcom/intsig/office/ss/control/Spreadsheet;->getEditor()Lcom/intsig/office/simpletext/control/IWord;

    .line 278
    .line 279
    .line 280
    move-result-object v14

    .line 281
    invoke-direct {v11, v14, v9}, Lcom/intsig/office/simpletext/view/STRoot;-><init>(Lcom/intsig/office/simpletext/control/IWord;Lcom/intsig/office/simpletext/model/IDocument;)V

    .line 282
    .line 283
    .line 284
    invoke-virtual {v11, v4}, Lcom/intsig/office/simpletext/view/STRoot;->setWrapLine(Z)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/view/STRoot;->doLayout()V

    .line 288
    .line 289
    .line 290
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 291
    .line 292
    .line 293
    move-result-object v14

    .line 294
    invoke-interface {v14, v4}, Lcom/intsig/office/simpletext/view/IView;->getLayoutSpan(B)I

    .line 295
    .line 296
    .line 297
    move-result v14

    .line 298
    invoke-virtual {v11}, Lcom/intsig/office/simpletext/view/STRoot;->dispose()V

    .line 299
    .line 300
    .line 301
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 302
    .line 303
    .line 304
    move-result-object v11

    .line 305
    int-to-float v14, v14

    .line 306
    add-float/2addr v14, v8

    .line 307
    const/high16 v8, 0x40800000    # 4.0f

    .line 308
    .line 309
    add-float/2addr v14, v8

    .line 310
    mul-float v15, v15, v14

    .line 311
    .line 312
    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    .line 313
    .line 314
    .line 315
    move-result v8

    .line 316
    invoke-virtual {v11, v7, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 317
    .line 318
    .line 319
    new-instance v7, Lcom/intsig/office/simpletext/view/STRoot;

    .line 320
    .line 321
    iget-object v8, v0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 322
    .line 323
    invoke-virtual {v8}, Lcom/intsig/office/ss/control/Spreadsheet;->getEditor()Lcom/intsig/office/simpletext/control/IWord;

    .line 324
    .line 325
    .line 326
    move-result-object v8

    .line 327
    invoke-direct {v7, v8, v9}, Lcom/intsig/office/simpletext/view/STRoot;-><init>(Lcom/intsig/office/simpletext/control/IWord;Lcom/intsig/office/simpletext/model/IDocument;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/view/STRoot;->doLayout()V

    .line 331
    .line 332
    .line 333
    :goto_3
    invoke-interface {v12}, Lcom/intsig/office/simpletext/model/IElementCollection;->size()I

    .line 334
    .line 335
    .line 336
    move-result v8

    .line 337
    if-ge v4, v8, :cond_6

    .line 338
    .line 339
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 340
    .line 341
    .line 342
    move-result-object v8

    .line 343
    invoke-interface {v12, v4}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 344
    .line 345
    .line 346
    move-result-object v9

    .line 347
    invoke-interface {v9}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 348
    .line 349
    .line 350
    move-result-object v9

    .line 351
    invoke-interface {v13, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 352
    .line 353
    .line 354
    move-result-object v11

    .line 355
    check-cast v11, Ljava/lang/Integer;

    .line 356
    .line 357
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    .line 358
    .line 359
    .line 360
    move-result v11

    .line 361
    invoke-virtual {v8, v9, v11}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 362
    .line 363
    .line 364
    add-int/lit8 v4, v4, 0x1

    .line 365
    .line 366
    goto :goto_3

    .line 367
    :cond_6
    iget v4, v0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 368
    .line 369
    mul-float v14, v14, v4

    .line 370
    .line 371
    float-to-int v4, v14

    .line 372
    int-to-float v4, v4

    .line 373
    sub-float/2addr v4, v3

    .line 374
    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;->setSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)V

    .line 375
    .line 376
    .line 377
    goto :goto_4

    .line 378
    :cond_7
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 379
    .line 380
    .line 381
    move-result v4

    .line 382
    if-gez v4, :cond_9

    .line 383
    .line 384
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 385
    .line 386
    .line 387
    move-result-object v4

    .line 388
    iget-object v7, v0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 389
    .line 390
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 391
    .line 392
    .line 393
    move-result-object v7

    .line 394
    invoke-virtual {v4, v7, v6}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 395
    .line 396
    .line 397
    move-result-object v4

    .line 398
    if-eqz v4, :cond_0

    .line 399
    .line 400
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 401
    .line 402
    .line 403
    move-result v7

    .line 404
    if-nez v7, :cond_8

    .line 405
    .line 406
    goto/16 :goto_0

    .line 407
    .line 408
    :cond_8
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontKit;->instance()Lcom/intsig/office/simpletext/font/FontKit;

    .line 409
    .line 410
    .line 411
    move-result-object v7

    .line 412
    iget-object v9, v0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 413
    .line 414
    invoke-virtual {v9}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 415
    .line 416
    .line 417
    move-result-object v9

    .line 418
    const/4 v11, 0x0

    .line 419
    invoke-virtual {v7, v6, v9, v11}, Lcom/intsig/office/simpletext/font/FontKit;->getCellPaint(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)Landroid/graphics/Paint;

    .line 420
    .line 421
    .line 422
    move-result-object v7

    .line 423
    invoke-virtual {v7}, Landroid/graphics/Paint;->getTextSize()F

    .line 424
    .line 425
    .line 426
    move-result v9

    .line 427
    iget v11, v0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 428
    .line 429
    mul-float v11, v11, v9

    .line 430
    .line 431
    invoke-virtual {v7, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 432
    .line 433
    .line 434
    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 435
    .line 436
    .line 437
    move-result v4

    .line 438
    add-float/2addr v4, v8

    .line 439
    add-float/2addr v4, v10

    .line 440
    sub-float/2addr v4, v3

    .line 441
    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 442
    .line 443
    .line 444
    :goto_4
    const/4 v3, 0x0

    .line 445
    goto :goto_5

    .line 446
    :cond_9
    const/4 v3, 0x0

    .line 447
    const/4 v4, 0x0

    .line 448
    :goto_5
    cmpl-float v3, v4, v3

    .line 449
    .line 450
    if-lez v3, :cond_0

    .line 451
    .line 452
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 453
    .line 454
    .line 455
    move-result v3

    .line 456
    if-gez v3, :cond_0

    .line 457
    .line 458
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 459
    .line 460
    .line 461
    move-result v3

    .line 462
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 463
    .line 464
    .line 465
    move-result v7

    .line 466
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 467
    .line 468
    .line 469
    move-result v5

    .line 470
    packed-switch v5, :pswitch_data_0

    .line 471
    .line 472
    .line 473
    :goto_6
    move v10, v3

    .line 474
    move v8, v7

    .line 475
    goto :goto_7

    .line 476
    :pswitch_0
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 477
    .line 478
    .line 479
    move-result v5

    .line 480
    invoke-direct {v0, v1, v5, v4}, Lcom/intsig/office/ss/view/SheetView;->getExtendTextLeftBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I

    .line 481
    .line 482
    .line 483
    move-result v4

    .line 484
    move v10, v3

    .line 485
    move v8, v4

    .line 486
    goto :goto_7

    .line 487
    :pswitch_1
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 488
    .line 489
    .line 490
    move-result v3

    .line 491
    div-float/2addr v4, v10

    .line 492
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/office/ss/view/SheetView;->getExtendTextLeftBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I

    .line 493
    .line 494
    .line 495
    move-result v3

    .line 496
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 497
    .line 498
    .line 499
    move-result v5

    .line 500
    invoke-direct {v0, v1, v5, v4}, Lcom/intsig/office/ss/view/SheetView;->getExtendTextRightBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I

    .line 501
    .line 502
    .line 503
    move-result v4

    .line 504
    move v8, v3

    .line 505
    move v10, v4

    .line 506
    goto :goto_7

    .line 507
    :pswitch_2
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 508
    .line 509
    .line 510
    move-result v3

    .line 511
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/office/ss/view/SheetView;->getExtendTextRightBound(Lcom/intsig/office/ss/model/baseModel/Row;IF)I

    .line 512
    .line 513
    .line 514
    move-result v3

    .line 515
    goto :goto_6

    .line 516
    :goto_7
    if-ne v8, v10, :cond_a

    .line 517
    .line 518
    goto/16 :goto_0

    .line 519
    .line 520
    :cond_a
    new-instance v3, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 521
    .line 522
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 523
    .line 524
    .line 525
    move-result v7

    .line 526
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 527
    .line 528
    .line 529
    move-result v9

    .line 530
    move-object v5, v3

    .line 531
    invoke-direct/range {v5 .. v10}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;-><init>(Lcom/intsig/office/ss/model/baseModel/Cell;IIII)V

    .line 532
    .line 533
    .line 534
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedCellCount()I

    .line 535
    .line 536
    .line 537
    move-result v4

    .line 538
    invoke-virtual {v1, v4, v3}, Lcom/intsig/office/ss/model/baseModel/Row;->addExpandedRangeAddress(ILcom/intsig/office/ss/other/ExpandedCellRangeAddress;)V

    .line 539
    .line 540
    .line 541
    goto/16 :goto_0

    .line 542
    .line 543
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedCellCount()I

    .line 544
    .line 545
    .line 546
    move-result v2

    .line 547
    :goto_8
    if-ge v4, v2, :cond_e

    .line 548
    .line 549
    invoke-virtual {v1, v4}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedRangeAddress(I)Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 550
    .line 551
    .line 552
    move-result-object v3

    .line 553
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getRangedAddress()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 554
    .line 555
    .line 556
    move-result-object v5

    .line 557
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 558
    .line 559
    .line 560
    move-result v5

    .line 561
    :goto_9
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getRangedAddress()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 562
    .line 563
    .line 564
    move-result-object v6

    .line 565
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 566
    .line 567
    .line 568
    move-result v6

    .line 569
    if-gt v5, v6, :cond_d

    .line 570
    .line 571
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 572
    .line 573
    .line 574
    move-result-object v6

    .line 575
    if-nez v6, :cond_c

    .line 576
    .line 577
    new-instance v6, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 578
    .line 579
    const/4 v7, 0x3

    .line 580
    invoke-direct {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 581
    .line 582
    .line 583
    invoke-virtual {v6, v5}, Lcom/intsig/office/ss/model/baseModel/Cell;->setColNumber(I)V

    .line 584
    .line 585
    .line 586
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 587
    .line 588
    .line 589
    move-result v7

    .line 590
    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;->setRowNumber(I)V

    .line 591
    .line 592
    .line 593
    iget-object v7, v0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 594
    .line 595
    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 596
    .line 597
    .line 598
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowStyle()I

    .line 599
    .line 600
    .line 601
    move-result v7

    .line 602
    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellStyle(I)V

    .line 603
    .line 604
    .line 605
    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Row;->addCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 606
    .line 607
    .line 608
    :cond_c
    invoke-virtual {v6, v4}, Lcom/intsig/office/ss/model/baseModel/Cell;->setExpandedRangeAddressIndex(I)V

    .line 609
    .line 610
    .line 611
    add-int/lit8 v5, v5, 0x1

    .line 612
    .line 613
    goto :goto_9

    .line 614
    :cond_d
    add-int/lit8 v4, v4, 0x1

    .line 615
    .line 616
    goto :goto_8

    .line 617
    :cond_e
    return-void

    .line 618
    nop

    .line 619
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private resizeCalloutView()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->setZoom(F)V

    .line 18
    .line 19
    .line 20
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 23
    .line 24
    mul-float v0, v0, v1

    .line 25
    .line 26
    float-to-int v0, v0

    .line 27
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 28
    .line 29
    mul-float v2, v2, v1

    .line 30
    .line 31
    float-to-int v1, v2

    .line 32
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    sub-int/2addr v3, v0

    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    sub-int/2addr v4, v1

    .line 48
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 49
    .line 50
    invoke-virtual {v5}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 51
    .line 52
    .line 53
    move-result-object v5

    .line 54
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    .line 55
    .line 56
    .line 57
    move-result v5

    .line 58
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 59
    .line 60
    invoke-virtual {v6}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    .line 65
    .line 66
    .line 67
    move-result v6

    .line 68
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 69
    .line 70
    .line 71
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 72
    .line 73
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->getCalloutView()Lcom/intsig/office/system/beans/CalloutView/CalloutView;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {v2, v0, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->setClip(II)V

    .line 78
    .line 79
    .line 80
    :cond_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/ss/view/SheetView;)Lcom/intsig/office/ss/control/Spreadsheet;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public addExtendCell(Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;FFLjava/lang/Object;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 2
    .line 3
    new-instance v8, Lcom/intsig/office/ss/view/SheetView$ExtendCell;

    .line 4
    .line 5
    move-object v1, v8

    .line 6
    move-object v2, p0

    .line 7
    move-object v3, p1

    .line 8
    move-object v4, p2

    .line 9
    move v5, p3

    .line 10
    move v6, p4

    .line 11
    move-object v7, p5

    .line 12
    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/ss/view/SheetView$ExtendCell;-><init>(Lcom/intsig/office/ss/view/SheetView;Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;FFLjava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public changeHeaderArea(Lcom/intsig/office/ss/other/FocusCell;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public changeSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 3
    .line 4
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->removeSTRoot()V

    .line 5
    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/office/ss/view/SheetView;->initForDrawing()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/office/ss/view/SheetView;->resizeCalloutView()V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/ss/view/SheetView$1;

    .line 18
    .line 19
    invoke-direct {v0, p0}, Lcom/intsig/office/ss/view/SheetView$1;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 23
    .line 24
    .line 25
    monitor-exit p0

    .line 26
    return-void

    .line 27
    :catchall_0
    move-exception p1

    .line 28
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    throw p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/RowHeader;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 14
    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/ColumnHeader;->dispose()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 23
    .line 24
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 25
    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/CellView;->dispose()V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellView:Lcom/intsig/office/ss/view/CellView;

    .line 32
    .line 33
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->shapeView:Lcom/intsig/office/ss/view/ShapeView;

    .line 34
    .line 35
    if-eqz v1, :cond_3

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/ShapeView;->dispose()V

    .line 38
    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->shapeView:Lcom/intsig/office/ss/view/ShapeView;

    .line 41
    .line 42
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 43
    .line 44
    if-eqz v1, :cond_4

    .line 45
    .line 46
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->dispose()V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 50
    .line 51
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 52
    .line 53
    if-eqz v1, :cond_5

    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->dispose()V

    .line 56
    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 59
    .line 60
    :cond_5
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 61
    .line 62
    if-eqz v1, :cond_6

    .line 63
    .line 64
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/FindingMgr;->dispose()V

    .line 65
    .line 66
    .line 67
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 68
    .line 69
    :cond_6
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 70
    .line 71
    if-eqz v1, :cond_7

    .line 72
    .line 73
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 74
    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->extendCell:Ljava/util/List;

    .line 77
    .line 78
    :cond_7
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->selectedHeaderInfor:Lcom/intsig/office/ss/other/FocusCell;

    .line 79
    .line 80
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->effects:Landroid/graphics/PathEffect;

    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public drawSheet(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 3
    .line 4
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->startDrawing()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 16
    .line 17
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnRightBound(Landroid/graphics/Canvas;F)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 22
    .line 23
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 24
    .line 25
    invoke-virtual {v1, p1, v2}, Lcom/intsig/office/ss/view/RowHeader;->getRowBottomBound(Landroid/graphics/Canvas;F)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    .line 30
    .line 31
    iget v3, v2, Landroid/graphics/Rect;->right:I

    .line 32
    .line 33
    add-int/lit8 v4, v3, 0xa

    .line 34
    .line 35
    if-ge v0, v3, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    move v0, v4

    .line 39
    :goto_0
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 40
    .line 41
    add-int/lit8 v3, v2, 0x32

    .line 42
    .line 43
    if-ge v1, v2, :cond_1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    move v1, v3

    .line 47
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 48
    .line 49
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 50
    .line 51
    invoke-virtual {v2, p1, v0, v3}, Lcom/intsig/office/ss/view/RowHeader;->draw(Landroid/graphics/Canvas;IF)V

    .line 52
    .line 53
    .line 54
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 55
    .line 56
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 57
    .line 58
    invoke-virtual {v2, p1, v1, v3}, Lcom/intsig/office/ss/view/ColumnHeader;->draw(Landroid/graphics/Canvas;IF)V

    .line 59
    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 62
    .line 63
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    int-to-float v2, v2

    .line 68
    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 69
    .line 70
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    .line 71
    .line 72
    .line 73
    move-result v3

    .line 74
    int-to-float v3, v3

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 76
    .line 77
    .line 78
    int-to-float v0, v0

    .line 79
    int-to-float v1, v1

    .line 80
    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 81
    .line 82
    .line 83
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/SheetView;->drawRows(Landroid/graphics/Canvas;)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->tableFormatView:Lcom/intsig/office/ss/view/TableFormatView;

    .line 87
    .line 88
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/TableFormatView;->draw(Landroid/graphics/Canvas;)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/SheetView;->drawActiveCellBorder(Landroid/graphics/Canvas;)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->shapeView:Lcom/intsig/office/ss/view/ShapeView;

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/view/ShapeView;->draw(Landroid/graphics/Canvas;)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/SheetView;->drawMovingHeaderLine(Landroid/graphics/Canvas;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 103
    .line 104
    .line 105
    monitor-exit p0

    .line 106
    return-void

    .line 107
    :catchall_0
    move-exception p1

    .line 108
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    throw p1
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public find(Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/other/FindingMgr;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/ss/other/FindingMgr;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 15
    .line 16
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/other/FindingMgr;->findCell(Lcom/intsig/office/ss/model/baseModel/Sheet;Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/view/SheetView;->goToFindedCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 23
    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    return p1

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    return p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public findBackward()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/FindingMgr;->findBackward(Z)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/view/SheetView;->goToFindedCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    return v0

    .line 18
    :cond_1
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public findForward()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/FindingMgr;->findForward(Z)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/view/SheetView;->goToFindedCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    return v0

    .line 18
    :cond_1
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColumnHeaderHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentMinColumn()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentMinRow()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndentWidth(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getFont(I)Lcom/intsig/office/simpletext/font/Font;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/font/Font;->getFontSize()D

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 17
    .line 18
    mul-double v0, v0, v2

    .line 19
    .line 20
    int-to-double v2, p1

    .line 21
    mul-double v0, v0, v2

    .line 22
    .line 23
    const-wide v2, 0x3ff5555560000000L    # 1.3333333730697632

    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    mul-double v0, v0, v2

    .line 29
    .line 30
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    .line 31
    .line 32
    .line 33
    move-result-wide v0

    .line 34
    long-to-int p1, v0

    .line 35
    return p1
    .line 36
    .line 37
    .line 38
.end method

.method public getIndentWidthWithZoom(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getFont(I)Lcom/intsig/office/simpletext/font/Font;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/font/Font;->getFontSize()D

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 17
    .line 18
    mul-double v0, v0, v2

    .line 19
    .line 20
    int-to-double v2, p1

    .line 21
    mul-double v0, v0, v2

    .line 22
    .line 23
    const-wide v2, 0x3ff5555560000000L    # 1.3333333730697632

    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    mul-double v0, v0, v2

    .line 29
    .line 30
    iget p1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 31
    .line 32
    float-to-double v2, p1

    .line 33
    mul-double v0, v0, v2

    .line 34
    .line 35
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    .line 36
    .line 37
    .line 38
    move-result-wide v0

    .line 39
    long-to-int p1, v0

    .line 40
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getMaxScrollX()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollX()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMaxScrollY()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollY()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRowHeader()Lcom/intsig/office/ss/view/RowHeader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRowHeaderWidth()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getRowHeader()Lcom/intsig/office/ss/view/RowHeader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScrollX()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScrollY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheetIndex()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheetIndex(Lcom/intsig/office/ss/model/baseModel/Sheet;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    add-int/lit8 v0, v0, 0x1

    .line 14
    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getThumbnail(Lcom/intsig/office/ss/model/baseModel/Sheet;IIF)Landroid/graphics/Bitmap;
    .locals 8

    .line 1
    monitor-enter p0

    .line 2
    int-to-float p2, p2

    .line 3
    mul-float p2, p2, p4

    .line 4
    .line 5
    float-to-int p2, p2

    .line 6
    int-to-float p3, p3

    .line 7
    mul-float p3, p3, p4

    .line 8
    .line 9
    float-to-int p3, p3

    .line 10
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 11
    .line 12
    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    if-nez p2, :cond_0

    .line 17
    .line 18
    monitor-exit p0

    .line 19
    const/4 p1, 0x0

    .line 20
    return-object p1

    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-virtual {p3}, Lcom/intsig/office/common/picture/PictureKit;->isDrawPictrue()Z

    .line 26
    .line 27
    .line 28
    move-result p3

    .line 29
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const/4 v1, 0x1

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/picture/PictureKit;->setDrawPictrue(Z)V

    .line 35
    .line 36
    .line 37
    new-instance v0, Landroid/graphics/Canvas;

    .line 38
    .line 39
    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 40
    .line 41
    .line 42
    const/4 v2, -0x1

    .line 43
    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollX()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollY()I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getZoom()F

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 59
    .line 60
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 61
    .line 62
    const/4 v6, 0x0

    .line 63
    iput v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 64
    .line 65
    iput v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 66
    .line 67
    const/4 v6, 0x0

    .line 68
    invoke-virtual {p1, v6, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setScroll(II)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0, p4, v1}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 72
    .line 73
    .line 74
    iget-object p4, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 75
    .line 76
    iget v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 77
    .line 78
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v6

    .line 82
    iget v7, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 83
    .line 84
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v7

    .line 88
    invoke-virtual {p4, p1, v6, v7}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/view/SheetView;->drawThumbnail(Landroid/graphics/Canvas;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v2, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setScroll(II)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setZoom(F)V

    .line 98
    .line 99
    .line 100
    iput-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 101
    .line 102
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollX()I

    .line 103
    .line 104
    .line 105
    move-result p4

    .line 106
    int-to-float p4, p4

    .line 107
    iput p4, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 108
    .line 109
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollY()I

    .line 110
    .line 111
    .line 112
    move-result p4

    .line 113
    int-to-float p4, p4

    .line 114
    iput p4, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 115
    .line 116
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getZoom()F

    .line 117
    .line 118
    .line 119
    move-result p4

    .line 120
    invoke-virtual {p0, p4, v1}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 121
    .line 122
    .line 123
    iget-object p4, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 124
    .line 125
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 126
    .line 127
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 132
    .line 133
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 134
    .line 135
    .line 136
    move-result v1

    .line 137
    invoke-virtual {p4, p1, v0, v1}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 138
    .line 139
    .line 140
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    invoke-virtual {p1, p3}, Lcom/intsig/office/common/picture/PictureKit;->setDrawPictrue(Z)V

    .line 145
    .line 146
    .line 147
    monitor-exit p0

    .line 148
    return-object p2

    .line 149
    :catchall_0
    move-exception p1

    .line 150
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    throw p1
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public goToCell(II)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;IIZ)Landroid/graphics/Rect;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iget p2, p1, Landroid/graphics/Rect;->left:I

    .line 13
    .line 14
    int-to-float p2, p2

    .line 15
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 16
    .line 17
    int-to-float p1, p1

    .line 18
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/ss/view/SheetView;->scrollTo(FF)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public goToFindedCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-lez v2, :cond_1

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    add-int/lit8 v0, v0, -0x1

    .line 23
    .line 24
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-lez v2, :cond_2

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    add-int/lit8 v1, v1, -0x1

    .line 35
    .line 36
    :cond_2
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellRowCol(II)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    invoke-virtual {p0, v2, p1}, Lcom/intsig/office/ss/view/SheetView;->selectedCell(II)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/ss/view/SheetView;->goToCell(II)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 64
    .line 65
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    const/16 v0, 0x14

    .line 75
    .line 76
    const/4 v1, 0x0

    .line 77
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 81
    .line 82
    invoke-virtual {p1}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    const v0, 0x2000000a

    .line 87
    .line 88
    .line 89
    invoke-interface {p1, v0, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public removeSearch()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->findingMgr:Lcom/intsig/office/ss/other/FindingMgr;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/FindingMgr;->setValue(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public scrollBy(FF)V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 3
    .line 4
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 5
    .line 6
    div-float/2addr p1, v1

    .line 7
    add-float/2addr v0, p1

    .line 8
    iput v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollX()F

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 28
    .line 29
    iget p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 32
    .line 33
    div-float/2addr p2, v0

    .line 34
    add-float/2addr p1, p2

    .line 35
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollY()F

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    iget p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 44
    .line 45
    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 56
    .line 57
    iget p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 58
    .line 59
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 60
    .line 61
    .line 62
    move-result p2

    .line 63
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 64
    .line 65
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setScroll(II)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 73
    .line 74
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 75
    .line 76
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 77
    .line 78
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 83
    .line 84
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/office/ss/view/SheetView;->resizeCalloutView()V

    .line 92
    .line 93
    .line 94
    monitor-exit p0

    .line 95
    return-void

    .line 96
    :catchall_0
    move-exception p1

    .line 97
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    throw p1
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public scrollTo(FF)V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollX()F

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 22
    .line 23
    iput p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollY()F

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    iget p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 32
    .line 33
    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    .line 34
    .line 35
    .line 36
    move-result p2

    .line 37
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 44
    .line 45
    iget p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 46
    .line 47
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 48
    .line 49
    .line 50
    move-result p2

    .line 51
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 52
    .line 53
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setScroll(II)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 61
    .line 62
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 63
    .line 64
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 65
    .line 66
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 71
    .line 72
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 77
    .line 78
    .line 79
    monitor-exit p0

    .line 80
    return-void

    .line 81
    :catchall_0
    move-exception p1

    .line 82
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    throw p1
    .line 84
    .line 85
.end method

.method public selectedCell(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-ltz v1, :cond_0

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 26
    .line 27
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->setFirstRow(I)V

    .line 46
    .line 47
    .line 48
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->setLastRow(I)V

    .line 55
    .line 56
    .line 57
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->setFirstColumn(I)V

    .line 64
    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->setLastColumn(I)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 77
    .line 78
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->setFirstRow(I)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 82
    .line 83
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->setLastRow(I)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 87
    .line 88
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->setFirstColumn(I)V

    .line 89
    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 92
    .line 93
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->setLastColumn(I)V

    .line 94
    .line 95
    .line 96
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 103
    .line 104
    .line 105
    move-result p2

    .line 106
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->selecetedCellsRange:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 107
    .line 108
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setActiveCellRowCol(II)V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public setDrawMovingHeaderLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/view/SheetView;->isDrawMovingHeaderLine:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpreadsheet(Lcom/intsig/office/ss/control/Spreadsheet;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoom(F)V
    .locals 1

    .line 1
    monitor-enter p0

    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/ss/view/SheetView;->setZoom(FZ)V

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/ss/view/SheetView;->resizeCalloutView()V

    .line 4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setZoom(FFF)V
    .locals 4

    .line 76
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 78
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v2}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr p2, v2

    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    div-float/2addr p2, v2

    .line 79
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v2}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr p3, v2

    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    div-float/2addr p3, v2

    .line 80
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollX()F

    move-result v2

    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr p2, v3

    invoke-static {v2, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    .line 81
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMaxScrollY()F

    move-result v2

    iget-object v3, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr p3, v3

    invoke-static {v2, p3}, Ljava/lang/Math;->min(FF)F

    move-result p3

    .line 82
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 83
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v2, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setZoom(F)V

    .line 84
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v2, p1}, Lcom/intsig/office/ss/view/RowHeader;->calculateRowHeaderWidth(F)V

    .line 85
    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v2, p1}, Lcom/intsig/office/ss/view/ColumnHeader;->calculateColumnHeaderHeight(F)V

    mul-float p2, p2, p1

    .line 86
    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr p2, v0

    div-float/2addr p2, p1

    mul-float p3, p3, p1

    .line 87
    div-int/lit8 v1, v1, 0x2

    int-to-float v0, v1

    sub-float/2addr p3, v0

    div-float/2addr p3, p1

    float-to-int p1, p2

    int-to-float p1, p1

    float-to-int p2, p3

    int-to-float p2, p2

    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/ss/view/SheetView;->scrollTo(FF)V

    return-void
.end method

.method public declared-synchronized setZoom(FZ)V
    .locals 9

    monitor-enter p0

    .line 5
    :try_start_0
    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    if-nez p2, :cond_3

    .line 6
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->spreadsheet:Lcom/intsig/office/ss/control/Spreadsheet;

    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->getBottomBarHeight()I

    move-result v0

    sub-int/2addr p2, v0

    .line 7
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_1

    if-eq v0, v1, :cond_0

    goto/16 :goto_1

    .line 8
    :cond_0
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v0

    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 9
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v5

    add-int/2addr v5, v4

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 10
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    move-result-wide v6

    double-to-float v6, v6

    .line 11
    invoke-virtual {v0, p0, v5, v6}, Lcom/intsig/office/ss/util/ModelUtil;->getValueX(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result v0

    .line 12
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    goto :goto_0

    .line 13
    :cond_1
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v0

    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 14
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v5

    add-int/2addr v5, v4

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 15
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    move-result-wide v6

    double-to-float v6, v6

    .line 16
    invoke-virtual {v0, p0, v5, v6}, Lcom/intsig/office/ss/util/ModelUtil;->getValueY(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result v0

    int-to-float v5, p2

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    :goto_0
    const/4 v0, 0x1

    goto :goto_2

    .line 17
    :cond_2
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v0

    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 18
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v5

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 19
    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v6

    .line 20
    invoke-virtual {v0, p0, v5, v6}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/view/SheetView;II)Landroid/graphics/RectF;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v5

    cmpl-float v5, v5, v3

    if-lez v5, :cond_4

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v5, v5, v3

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    iget v7, v5, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    int-to-float v8, p2

    .line 22
    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/graphics/RectF;->intersect(FFFF)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :cond_4
    :goto_1
    const/4 v0, 0x0

    .line 23
    :goto_2
    iput p1, p0, Lcom/intsig/office/ss/view/SheetView;->zoom:F

    .line 24
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v5, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setZoom(F)V

    .line 25
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v5, p1}, Lcom/intsig/office/ss/view/RowHeader;->calculateRowHeaderWidth(F)V

    .line 26
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v5, p1}, Lcom/intsig/office/ss/view/ColumnHeader;->calculateColumnHeaderHeight(F)V

    if-eqz v0, :cond_1c

    .line 27
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1c

    .line 28
    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v5}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v5

    sub-int/2addr v0, v5

    int-to-float v0, v0

    .line 29
    iget-object v5, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v5}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v5

    sub-int v5, p2, v5

    int-to-float v5, v5

    .line 30
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellType()S

    move-result v6

    if-eqz v6, :cond_a

    if-eq v6, v4, :cond_7

    if-eq v6, v1, :cond_5

    goto/16 :goto_9

    .line 31
    :cond_5
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object p2

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v1

    add-int/2addr v1, v4

    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-float v2, v5

    invoke-virtual {p2, p0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getValueX(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result p2

    .line 32
    :goto_3
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v1

    cmpl-float v2, p2, v2

    if-lez v2, :cond_15

    int-to-float v1, v1

    sub-float/2addr p2, v1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    cmpl-float p2, p2, v3

    if-lez p2, :cond_15

    .line 33
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    move-result p2

    mul-float v1, p2, p1

    cmpl-float v1, v1, v0

    if-lez v1, :cond_6

    goto/16 :goto_9

    .line 34
    :cond_6
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    add-float/2addr v1, p2

    iput v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 35
    iget-object p2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {p2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {p2, v1}, Lcom/intsig/office/ss/other/SheetScroller;->setMinColumnIndex(I)V

    .line 36
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object p2

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v1

    add-int/2addr v1, v4

    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleColumnWidth()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-float v2, v5

    invoke-virtual {p2, p0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getValueX(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result p2

    goto :goto_3

    .line 37
    :cond_7
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v1

    add-int/2addr v1, v4

    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v2, v6

    invoke-virtual {v0, p0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getValueY(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result v0

    :goto_4
    int-to-float v1, p2

    cmpl-float v2, v0, v1

    if-lez v2, :cond_15

    sub-float/2addr v0, v1

    .line 38
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_15

    .line 39
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    move-result-object v0

    if-nez v0, :cond_8

    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_5

    :cond_8
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    move-result v0

    :goto_5
    mul-float v1, v0, p1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_9

    goto/16 :goto_9

    .line 41
    :cond_9
    iget v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 42
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v0}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/other/SheetScroller;->setMinRowIndex(I)V

    .line 43
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v1

    add-int/2addr v1, v4

    iget-object v2, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getVisibleRowHeight()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v2, v6

    invoke-virtual {v0, p0, v1, v2}, Lcom/intsig/office/ss/util/ModelUtil;->getValueY(Lcom/intsig/office/ss/view/SheetView;IF)F

    move-result v0

    goto :goto_4

    .line 44
    :cond_a
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v1

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v6

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v7

    invoke-virtual {v1, p0, v6, v7}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/view/SheetView;II)Landroid/graphics/RectF;

    move-result-object v1

    .line 45
    :cond_b
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v7}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-ltz v6, :cond_c

    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_c

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    .line 46
    invoke-virtual {v7}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-ltz v6, :cond_c

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, p2

    cmpl-float v6, v6, v7

    if-lez v6, :cond_15

    .line 47
    :cond_c
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v7}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gez v6, :cond_e

    .line 48
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v7}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    move-result v6

    mul-float v7, v6, p1

    cmpl-float v7, v7, v0

    if-lez v7, :cond_d

    goto/16 :goto_9

    .line 49
    :cond_d
    iget v7, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    sub-float/2addr v7, v6

    iput v7, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 50
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/other/SheetScroller;->setMinColumnIndex(I)V

    goto :goto_6

    .line 51
    :cond_e
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_10

    .line 52
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v7}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    move-result v6

    mul-float v7, v6, p1

    cmpl-float v7, v7, v0

    if-lez v7, :cond_f

    goto/16 :goto_9

    .line 53
    :cond_f
    iget v7, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    add-float/2addr v7, v6

    iput v7, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 54
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/other/SheetScroller;->setMinColumnIndex(I)V

    .line 55
    :cond_10
    :goto_6
    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v7}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gez v6, :cond_13

    .line 56
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    move-result-object v1

    if-nez v1, :cond_11

    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    move-result v1

    int-to-float v1, v1

    goto :goto_7

    :cond_11
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    move-result v1

    :goto_7
    mul-float v6, v1, p1

    cmpl-float v6, v6, v5

    if-lez v6, :cond_12

    goto :goto_9

    .line 58
    :cond_12
    iget v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    sub-float/2addr v6, v1

    iput v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 59
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v6

    sub-int/2addr v6, v4

    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/other/SheetScroller;->setMinRowIndex(I)V

    goto :goto_a

    .line 60
    :cond_13
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    int-to-float v6, p2

    cmpl-float v1, v1, v6

    if-lez v1, :cond_17

    .line 61
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    move-result-object v1

    if-nez v1, :cond_14

    .line 62
    invoke-virtual {p0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    move-result v1

    int-to-float v1, v1

    goto :goto_8

    :cond_14
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    move-result v1

    :goto_8
    mul-float v6, v1, p1

    cmpl-float v6, v6, v5

    if-lez v6, :cond_16

    .line 63
    :cond_15
    :goto_9
    iget-object p1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    iget p2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    float-to-int p2, p2

    iget v0, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    float-to-int v0, v0

    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setScroll(II)V

    goto/16 :goto_f

    .line 64
    :cond_16
    iget v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    add-float/2addr v6, v1

    iput v6, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 65
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/other/SheetScroller;->setMinRowIndex(I)V

    .line 66
    :cond_17
    :goto_a
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    move-result-object v1

    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellRow()I

    move-result v6

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v7}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getActiveCellColumn()I

    move-result v7

    invoke-virtual {v1, p0, v6, v7}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/view/SheetView;II)Landroid/graphics/RectF;

    move-result-object v1

    .line 67
    iget v6, v1, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v7}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gez v6, :cond_18

    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_18

    :goto_b
    const/4 v6, 0x1

    goto :goto_c

    .line 68
    :cond_18
    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/intsig/office/ss/view/SheetView;->rowHeader:Lcom/intsig/office/ss/view/RowHeader;

    invoke-virtual {v7}, Lcom/intsig/office/ss/view/RowHeader;->getRowHeaderWidth()I

    move-result v7

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_19

    iget v6, v1, Landroid/graphics/RectF;->right:F

    iget v7, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    .line 69
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gez v6, :cond_19

    .line 70
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/other/SheetScroller;->setMinColumnIndex(I)V

    goto :goto_b

    :cond_19
    const/4 v6, 0x0

    .line 71
    :goto_c
    iget v7, v1, Landroid/graphics/RectF;->top:F

    iget-object v8, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v8}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpg-float v7, v7, v3

    if-gez v7, :cond_1a

    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p0, Lcom/intsig/office/ss/view/SheetView;->clipRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-ltz v7, :cond_1a

    :goto_d
    const/4 v6, 0x1

    goto :goto_e

    .line 72
    :cond_1a
    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p0, Lcom/intsig/office/ss/view/SheetView;->columnHeader:Lcom/intsig/office/ss/view/ColumnHeader;

    invoke-virtual {v8}, Lcom/intsig/office/ss/view/ColumnHeader;->getColumnHeaderHeight()I

    move-result v8

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_1b

    iget v7, v1, Landroid/graphics/RectF;->bottom:F

    iget v8, v1, Landroid/graphics/RectF;->top:F

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1b

    .line 73
    iget-object v6, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    invoke-virtual {v6}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/other/SheetScroller;->setMinRowIndex(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_d

    :cond_1b
    :goto_e
    if-eqz v6, :cond_b

    .line 74
    monitor-exit p0

    return-void

    .line 75
    :cond_1c
    :goto_f
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public updateMinRowAndColumnInfo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/SheetView;->sheetScroller:Lcom/intsig/office/ss/other/SheetScroller;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/ss/view/SheetView;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/ss/view/SheetView;->scrollX:F

    .line 6
    .line 7
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    iget v3, p0, Lcom/intsig/office/ss/view/SheetView;->scrollY:F

    .line 12
    .line 13
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/ss/other/SheetScroller;->update(Lcom/intsig/office/ss/model/baseModel/Sheet;II)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/ss/view/CellBorderView;
.super Ljava/lang/Object;
.source "CellBorderView.java"


# instance fields
.field private sheetView:Lcom/intsig/office/ss/view/SheetView;


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/view/SheetView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private BottomBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-ltz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    move-object p1, v2

    .line 48
    :cond_0
    if-eqz v0, :cond_1

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderBottom()S

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-lez v2, :cond_1

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderBottomColorIdx()S

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    return p1

    .line 61
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    add-int/lit8 v0, v0, 0x1

    .line 66
    .line 67
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_3

    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    if-eqz p1, :cond_3

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-ltz v2, :cond_2

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    if-eqz p1, :cond_2

    .line 118
    .line 119
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    :cond_2
    if-eqz v0, :cond_3

    .line 124
    .line 125
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderTop()S

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    if-lez p1, :cond_3

    .line 130
    .line 131
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderTopColorIdx()S

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    return p1

    .line 136
    :cond_3
    const/4 p1, -0x1

    .line 137
    return p1
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private LeftBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-ltz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    move-object p1, v2

    .line 48
    :cond_0
    const/4 v2, -0x1

    .line 49
    const/4 v3, 0x0

    .line 50
    const/4 v4, 0x1

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderLeft()S

    .line 54
    .line 55
    .line 56
    move-result v5

    .line 57
    if-lez v5, :cond_1

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderLeftColorIdx()S

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    sub-int/2addr v5, v4

    .line 77
    invoke-virtual {v0, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    if-ltz v6, :cond_2

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 102
    .line 103
    .line 104
    move-result v6

    .line 105
    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 106
    .line 107
    .line 108
    move-result-object v6

    .line 109
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    invoke-virtual {v6, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    if-eqz v0, :cond_2

    .line 118
    .line 119
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 120
    .line 121
    .line 122
    move-result-object v5

    .line 123
    :cond_2
    if-eqz v5, :cond_3

    .line 124
    .line 125
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderRight()S

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    if-lez v0, :cond_3

    .line 130
    .line 131
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderRightColorIdx()S

    .line 132
    .line 133
    .line 134
    move-result v0

    .line 135
    goto :goto_0

    .line 136
    :cond_3
    const/4 v0, -0x1

    .line 137
    const/4 v4, 0x0

    .line 138
    :goto_0
    if-eqz v4, :cond_4

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 141
    .line 142
    .line 143
    move-result v5

    .line 144
    if-ltz v5, :cond_4

    .line 145
    .line 146
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 147
    .line 148
    .line 149
    move-result v5

    .line 150
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 155
    .line 156
    .line 157
    move-result v5

    .line 158
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedRangeAddress(I)Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getRangedAddress()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    if-eq p1, v1, :cond_4

    .line 175
    .line 176
    goto :goto_1

    .line 177
    :cond_4
    move v3, v4

    .line 178
    :goto_1
    if-eqz v3, :cond_5

    .line 179
    .line 180
    return v0

    .line 181
    :cond_5
    return v2
    .line 182
.end method

.method private RightBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-ltz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    move-object p1, v2

    .line 48
    :cond_0
    const/4 v2, -0x1

    .line 49
    const/4 v3, 0x0

    .line 50
    const/4 v4, 0x1

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderRight()S

    .line 54
    .line 55
    .line 56
    move-result v5

    .line 57
    if-lez v5, :cond_1

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderRightColorIdx()S

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    add-int/2addr v5, v4

    .line 77
    invoke-virtual {v0, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    if-ltz v6, :cond_2

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 102
    .line 103
    .line 104
    move-result v6

    .line 105
    invoke-virtual {v1, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 106
    .line 107
    .line 108
    move-result-object v6

    .line 109
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    invoke-virtual {v6, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    if-eqz v0, :cond_2

    .line 118
    .line 119
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 120
    .line 121
    .line 122
    move-result-object v5

    .line 123
    :cond_2
    if-eqz v5, :cond_3

    .line 124
    .line 125
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderLeft()S

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    if-lez v0, :cond_3

    .line 130
    .line 131
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderLeftColorIdx()S

    .line 132
    .line 133
    .line 134
    move-result v0

    .line 135
    goto :goto_0

    .line 136
    :cond_3
    const/4 v0, -0x1

    .line 137
    const/4 v4, 0x0

    .line 138
    :goto_0
    if-eqz v4, :cond_4

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 141
    .line 142
    .line 143
    move-result v5

    .line 144
    if-ltz v5, :cond_4

    .line 145
    .line 146
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 147
    .line 148
    .line 149
    move-result v5

    .line 150
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 155
    .line 156
    .line 157
    move-result v5

    .line 158
    invoke-virtual {v1, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedRangeAddress(I)Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getRangedAddress()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    if-eq p1, v1, :cond_4

    .line 175
    .line 176
    goto :goto_1

    .line 177
    :cond_4
    move v3, v4

    .line 178
    :goto_1
    if-eqz v3, :cond_5

    .line 179
    .line 180
    return v0

    .line 181
    :cond_5
    return v2
    .line 182
.end method

.method private TopBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-ltz v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    move-object p1, v2

    .line 48
    :cond_0
    if-eqz v0, :cond_1

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderTop()S

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-lez v2, :cond_1

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderTopColorIdx()S

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    return p1

    .line 61
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    add-int/lit8 v0, v0, -0x1

    .line 66
    .line 67
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_3

    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    if-eqz p1, :cond_3

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-ltz v2, :cond_2

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    if-eqz p1, :cond_2

    .line 118
    .line 119
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    :cond_2
    if-eqz v0, :cond_3

    .line 124
    .line 125
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderBottom()S

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    if-lez p1, :cond_3

    .line 130
    .line 131
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderBottomColorIdx()S

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    return p1

    .line 136
    :cond_3
    const/4 p1, -0x1

    .line 137
    return p1
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 11

    .line 1
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 10
    .line 11
    .line 12
    move-result v7

    .line 13
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 20
    .line 21
    .line 22
    move-result-object v8

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 24
    .line 25
    .line 26
    iget v1, p3, Landroid/graphics/RectF;->left:F

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    int-to-float v2, v2

    .line 35
    const/4 v9, -0x1

    .line 36
    const/high16 v10, 0x3f800000    # 1.0f

    .line 37
    .line 38
    cmpl-float v1, v1, v2

    .line 39
    .line 40
    if-lez v1, :cond_1

    .line 41
    .line 42
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/view/CellBorderView;->LeftBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-le v1, v9, :cond_0

    .line 47
    .line 48
    invoke-virtual {v8, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    .line 54
    .line 55
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 56
    .line 57
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 58
    .line 59
    add-float v4, v2, v10

    .line 60
    .line 61
    iget v5, p3, Landroid/graphics/RectF;->bottom:F

    .line 62
    .line 63
    move-object v1, p1

    .line 64
    move-object v6, v0

    .line 65
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    if-eqz p4, :cond_1

    .line 70
    .line 71
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    if-eqz v1, :cond_1

    .line 76
    .line 77
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    .line 87
    .line 88
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 89
    .line 90
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 91
    .line 92
    add-float v4, v2, v10

    .line 93
    .line 94
    iget v5, p3, Landroid/graphics/RectF;->bottom:F

    .line 95
    .line 96
    move-object v1, p1

    .line 97
    move-object v6, v0

    .line 98
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 99
    .line 100
    .line 101
    :cond_1
    :goto_0
    iget v1, p3, Landroid/graphics/RectF;->top:F

    .line 102
    .line 103
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 104
    .line 105
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    int-to-float v2, v2

    .line 110
    cmpl-float v1, v1, v2

    .line 111
    .line 112
    if-lez v1, :cond_3

    .line 113
    .line 114
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/view/CellBorderView;->TopBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    if-le v1, v9, :cond_2

    .line 119
    .line 120
    invoke-virtual {v8, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    .line 126
    .line 127
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 128
    .line 129
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 130
    .line 131
    iget v4, p3, Landroid/graphics/RectF;->right:F

    .line 132
    .line 133
    add-float v5, v3, v10

    .line 134
    .line 135
    move-object v1, p1

    .line 136
    move-object v6, v0

    .line 137
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 138
    .line 139
    .line 140
    goto :goto_1

    .line 141
    :cond_2
    if-eqz p4, :cond_3

    .line 142
    .line 143
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    if-eqz v1, :cond_3

    .line 148
    .line 149
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    .line 159
    .line 160
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 161
    .line 162
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 163
    .line 164
    iget v4, p3, Landroid/graphics/RectF;->right:F

    .line 165
    .line 166
    add-float v5, v3, v10

    .line 167
    .line 168
    move-object v1, p1

    .line 169
    move-object v6, v0

    .line 170
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 171
    .line 172
    .line 173
    :cond_3
    :goto_1
    iget v1, p3, Landroid/graphics/RectF;->right:F

    .line 174
    .line 175
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 176
    .line 177
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 178
    .line 179
    .line 180
    move-result v2

    .line 181
    int-to-float v2, v2

    .line 182
    cmpl-float v1, v1, v2

    .line 183
    .line 184
    if-lez v1, :cond_5

    .line 185
    .line 186
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/view/CellBorderView;->RightBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I

    .line 187
    .line 188
    .line 189
    move-result v1

    .line 190
    if-le v1, v9, :cond_4

    .line 191
    .line 192
    invoke-virtual {v8, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 197
    .line 198
    .line 199
    iget v2, p3, Landroid/graphics/RectF;->right:F

    .line 200
    .line 201
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 202
    .line 203
    add-float v4, v2, v10

    .line 204
    .line 205
    iget v5, p3, Landroid/graphics/RectF;->bottom:F

    .line 206
    .line 207
    move-object v1, p1

    .line 208
    move-object v6, v0

    .line 209
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 210
    .line 211
    .line 212
    goto :goto_2

    .line 213
    :cond_4
    if-eqz p4, :cond_5

    .line 214
    .line 215
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 216
    .line 217
    .line 218
    move-result-object v1

    .line 219
    if-eqz v1, :cond_5

    .line 220
    .line 221
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 222
    .line 223
    .line 224
    move-result-object v1

    .line 225
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 226
    .line 227
    .line 228
    move-result v1

    .line 229
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 230
    .line 231
    .line 232
    iget v2, p3, Landroid/graphics/RectF;->right:F

    .line 233
    .line 234
    iget v3, p3, Landroid/graphics/RectF;->top:F

    .line 235
    .line 236
    add-float v4, v2, v10

    .line 237
    .line 238
    iget v5, p3, Landroid/graphics/RectF;->bottom:F

    .line 239
    .line 240
    move-object v1, p1

    .line 241
    move-object v6, v0

    .line 242
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 243
    .line 244
    .line 245
    :cond_5
    :goto_2
    iget v1, p3, Landroid/graphics/RectF;->bottom:F

    .line 246
    .line 247
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 248
    .line 249
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 250
    .line 251
    .line 252
    move-result v2

    .line 253
    int-to-float v2, v2

    .line 254
    cmpl-float v1, v1, v2

    .line 255
    .line 256
    if-lez v1, :cond_7

    .line 257
    .line 258
    invoke-direct {p0, p2}, Lcom/intsig/office/ss/view/CellBorderView;->BottomBorder(Lcom/intsig/office/ss/model/baseModel/Cell;)I

    .line 259
    .line 260
    .line 261
    move-result p2

    .line 262
    if-le p2, v9, :cond_6

    .line 263
    .line 264
    invoke-virtual {v8, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 265
    .line 266
    .line 267
    move-result p2

    .line 268
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 269
    .line 270
    .line 271
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 272
    .line 273
    iget v3, p3, Landroid/graphics/RectF;->bottom:F

    .line 274
    .line 275
    iget v4, p3, Landroid/graphics/RectF;->right:F

    .line 276
    .line 277
    add-float v5, v3, v10

    .line 278
    .line 279
    move-object v1, p1

    .line 280
    move-object v6, v0

    .line 281
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 282
    .line 283
    .line 284
    goto :goto_3

    .line 285
    :cond_6
    if-eqz p4, :cond_7

    .line 286
    .line 287
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 288
    .line 289
    .line 290
    move-result-object p2

    .line 291
    if-eqz p2, :cond_7

    .line 292
    .line 293
    invoke-virtual {p4}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getBorderColor()Ljava/lang/Integer;

    .line 294
    .line 295
    .line 296
    move-result-object p2

    .line 297
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 298
    .line 299
    .line 300
    move-result p2

    .line 301
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 302
    .line 303
    .line 304
    iget v2, p3, Landroid/graphics/RectF;->left:F

    .line 305
    .line 306
    iget v3, p3, Landroid/graphics/RectF;->bottom:F

    .line 307
    .line 308
    iget v4, p3, Landroid/graphics/RectF;->right:F

    .line 309
    .line 310
    add-float v5, v3, v10

    .line 311
    .line 312
    move-object v1, p1

    .line 313
    move-object v6, v0

    .line 314
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 315
    .line 316
    .line 317
    :cond_7
    :goto_3
    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 321
    .line 322
    .line 323
    return-void
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public drawActiveCellBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;S)V
    .locals 12

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellBorderView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getColumnHeaderHeight()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    .line 36
    .line 37
    .line 38
    move-result v8

    .line 39
    new-instance v9, Landroid/graphics/Paint;

    .line 40
    .line 41
    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 42
    .line 43
    .line 44
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 45
    .line 46
    invoke-virtual {v9, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 47
    .line 48
    .line 49
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 50
    .line 51
    sget v3, Lcom/intsig/office_preview/R$color;->cs_color_brand:I

    .line 52
    .line 53
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    invoke-virtual {v9, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    .line 59
    .line 60
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 61
    .line 62
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    .line 68
    .line 69
    const/high16 v10, 0x3f800000    # 1.0f

    .line 70
    .line 71
    const/high16 v11, 0x40000000    # 2.0f

    .line 72
    .line 73
    if-nez p3, :cond_0

    .line 74
    .line 75
    iget v2, p2, Landroid/graphics/RectF;->left:F

    .line 76
    .line 77
    iget v3, p2, Landroid/graphics/RectF;->right:F

    .line 78
    .line 79
    cmpl-float v3, v2, v3

    .line 80
    .line 81
    if-eqz v3, :cond_0

    .line 82
    .line 83
    iget v3, p2, Landroid/graphics/RectF;->top:F

    .line 84
    .line 85
    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    .line 86
    .line 87
    cmpl-float v5, v3, v4

    .line 88
    .line 89
    if-eqz v5, :cond_0

    .line 90
    .line 91
    sub-float p3, v2, v11

    .line 92
    .line 93
    sub-float v0, v3, v11

    .line 94
    .line 95
    add-float v5, v2, v10

    .line 96
    .line 97
    add-float v6, v4, v11

    .line 98
    .line 99
    move-object v2, p1

    .line 100
    move v3, p3

    .line 101
    move v4, v0

    .line 102
    move-object v7, v1

    .line 103
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 104
    .line 105
    .line 106
    iget p3, p2, Landroid/graphics/RectF;->left:F

    .line 107
    .line 108
    sub-float v3, p3, v11

    .line 109
    .line 110
    iget p3, p2, Landroid/graphics/RectF;->top:F

    .line 111
    .line 112
    sub-float v4, p3, v11

    .line 113
    .line 114
    iget v0, p2, Landroid/graphics/RectF;->right:F

    .line 115
    .line 116
    add-float v5, v0, v11

    .line 117
    .line 118
    add-float v6, p3, v10

    .line 119
    .line 120
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 121
    .line 122
    .line 123
    iget p3, p2, Landroid/graphics/RectF;->right:F

    .line 124
    .line 125
    sub-float v3, p3, v10

    .line 126
    .line 127
    iget v0, p2, Landroid/graphics/RectF;->top:F

    .line 128
    .line 129
    sub-float v4, v0, v11

    .line 130
    .line 131
    add-float v5, p3, v11

    .line 132
    .line 133
    iget p3, p2, Landroid/graphics/RectF;->bottom:F

    .line 134
    .line 135
    add-float v6, p3, v11

    .line 136
    .line 137
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 138
    .line 139
    .line 140
    iget p3, p2, Landroid/graphics/RectF;->left:F

    .line 141
    .line 142
    sub-float v3, p3, v11

    .line 143
    .line 144
    iget p3, p2, Landroid/graphics/RectF;->bottom:F

    .line 145
    .line 146
    sub-float v4, p3, v10

    .line 147
    .line 148
    iget v0, p2, Landroid/graphics/RectF;->right:F

    .line 149
    .line 150
    add-float v5, v0, v11

    .line 151
    .line 152
    add-float v6, p3, v11

    .line 153
    .line 154
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 155
    .line 156
    .line 157
    invoke-static {v11}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 158
    .line 159
    .line 160
    move-result p3

    .line 161
    int-to-float p3, p3

    .line 162
    iget v0, p2, Landroid/graphics/RectF;->left:F

    .line 163
    .line 164
    sub-float v3, v0, p3

    .line 165
    .line 166
    iget v2, p2, Landroid/graphics/RectF;->top:F

    .line 167
    .line 168
    sub-float v4, v2, p3

    .line 169
    .line 170
    add-float v5, v0, p3

    .line 171
    .line 172
    add-float v6, v2, p3

    .line 173
    .line 174
    move-object v2, p1

    .line 175
    move-object v7, v9

    .line 176
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 177
    .line 178
    .line 179
    iget v0, p2, Landroid/graphics/RectF;->right:F

    .line 180
    .line 181
    sub-float v3, v0, p3

    .line 182
    .line 183
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 184
    .line 185
    sub-float v4, p2, p3

    .line 186
    .line 187
    add-float v5, v0, p3

    .line 188
    .line 189
    add-float v6, p2, p3

    .line 190
    .line 191
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 192
    .line 193
    .line 194
    goto :goto_0

    .line 195
    :cond_0
    const/4 v2, 0x1

    .line 196
    const/4 v9, 0x2

    .line 197
    if-ne p3, v2, :cond_1

    .line 198
    .line 199
    iget v2, p2, Landroid/graphics/RectF;->top:F

    .line 200
    .line 201
    iget v3, p2, Landroid/graphics/RectF;->bottom:F

    .line 202
    .line 203
    cmpl-float v3, v2, v3

    .line 204
    .line 205
    if-eqz v3, :cond_1

    .line 206
    .line 207
    iget p3, v0, Landroid/graphics/Rect;->left:I

    .line 208
    .line 209
    sub-int/2addr p3, v9

    .line 210
    int-to-float v3, p3

    .line 211
    sub-float v4, v2, v11

    .line 212
    .line 213
    iget p3, v0, Landroid/graphics/Rect;->right:I

    .line 214
    .line 215
    add-int/lit8 p3, p3, 0xa

    .line 216
    .line 217
    int-to-float v5, p3

    .line 218
    add-float v6, v2, v10

    .line 219
    .line 220
    move-object v2, p1

    .line 221
    move-object v7, v1

    .line 222
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 223
    .line 224
    .line 225
    iget p3, v0, Landroid/graphics/Rect;->left:I

    .line 226
    .line 227
    sub-int/2addr p3, v9

    .line 228
    int-to-float v3, p3

    .line 229
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 230
    .line 231
    sub-float v4, p2, v10

    .line 232
    .line 233
    iget p3, v0, Landroid/graphics/Rect;->right:I

    .line 234
    .line 235
    add-int/lit8 p3, p3, 0xa

    .line 236
    .line 237
    int-to-float v5, p3

    .line 238
    add-float v6, p2, v11

    .line 239
    .line 240
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 241
    .line 242
    .line 243
    goto :goto_0

    .line 244
    :cond_1
    if-ne p3, v9, :cond_2

    .line 245
    .line 246
    iget p3, p2, Landroid/graphics/RectF;->left:F

    .line 247
    .line 248
    iget v2, p2, Landroid/graphics/RectF;->right:F

    .line 249
    .line 250
    cmpl-float v2, p3, v2

    .line 251
    .line 252
    if-eqz v2, :cond_2

    .line 253
    .line 254
    sub-float v3, p3, v11

    .line 255
    .line 256
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 257
    .line 258
    sub-int/2addr v2, v9

    .line 259
    int-to-float v4, v2

    .line 260
    add-float v5, p3, v10

    .line 261
    .line 262
    iget p3, v0, Landroid/graphics/Rect;->bottom:I

    .line 263
    .line 264
    add-int/2addr p3, v9

    .line 265
    int-to-float v6, p3

    .line 266
    move-object v2, p1

    .line 267
    move-object v7, v1

    .line 268
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 269
    .line 270
    .line 271
    iget p2, p2, Landroid/graphics/RectF;->right:F

    .line 272
    .line 273
    sub-float v3, p2, v10

    .line 274
    .line 275
    iget p3, v0, Landroid/graphics/Rect;->top:I

    .line 276
    .line 277
    sub-int/2addr p3, v9

    .line 278
    int-to-float v4, p3

    .line 279
    add-float v5, p2, v11

    .line 280
    .line 281
    iget p2, v0, Landroid/graphics/Rect;->bottom:I

    .line 282
    .line 283
    add-int/2addr p2, v9

    .line 284
    int-to-float v6, p2

    .line 285
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 286
    .line 287
    .line 288
    :cond_2
    :goto_0
    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 289
    .line 290
    .line 291
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 292
    .line 293
    .line 294
    return-void
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

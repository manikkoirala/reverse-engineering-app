.class public Lcom/intsig/office/ss/view/CellView;
.super Ljava/lang/Object;
.source "CellView.java"


# instance fields
.field private cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

.field private cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

.field private cellRect:Landroid/graphics/RectF;

.field private left:F

.field private mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

.field private mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

.field private numericCellAlignRight:Z

.field private sheetView:Lcom/intsig/office/ss/view/SheetView;

.field private strBuilder:Ljava/lang/StringBuilder;

.field private tableStyleKit:Lcom/intsig/office/ss/model/table/TableStyleKit;

.field private top:F


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/view/SheetView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/office/ss/other/MergeCell;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/intsig/office/ss/other/MergeCell;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 15
    .line 16
    invoke-direct {v0}, Lcom/intsig/office/ss/other/MergeCellMgr;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 20
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/ss/model/table/TableStyleKit;

    .line 29
    .line 30
    invoke-direct {v0}, Lcom/intsig/office/ss/model/table/TableStyleKit;-><init>()V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->tableStyleKit:Lcom/intsig/office/ss/model/table/TableStyleKit;

    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/office/ss/view/CellBorderView;

    .line 38
    .line 39
    invoke-direct {v0, p1}, Lcom/intsig/office/ss/view/CellBorderView;-><init>(Lcom/intsig/office/ss/view/SheetView;)V

    .line 40
    .line 41
    .line 42
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

    .line 43
    .line 44
    new-instance p1, Landroid/graphics/RectF;

    .line 45
    .line 46
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private adjustGeneralCellContent(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/CellView;->scientificToNormal(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/16 v0, 0x45

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, -0x1

    .line 12
    if-le v0, v1, :cond_0

    .line 13
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->getScientificGeneralContents(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    return-object p1

    .line 19
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->getNonScientificGeneralContents(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    return-object p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private ceilNumeric(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1
    const/16 v0, 0x2e

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-lez v0, :cond_2

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    array-length v3, v2

    .line 15
    sub-int/2addr v3, v1

    .line 16
    :goto_0
    if-le v3, v0, :cond_0

    .line 17
    .line 18
    aget-char v4, v2, v3

    .line 19
    .line 20
    const/16 v5, 0x39

    .line 21
    .line 22
    if-ne v4, v5, :cond_0

    .line 23
    .line 24
    add-int/lit8 v3, v3, -0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    if-le v3, v0, :cond_1

    .line 28
    .line 29
    aget-char p1, v2, v3

    .line 30
    .line 31
    add-int/2addr p1, v1

    .line 32
    int-to-char p1, p1

    .line 33
    aput-char p1, v2, v3

    .line 34
    .line 35
    const/4 p1, 0x0

    .line 36
    add-int/2addr v3, v1

    .line 37
    invoke-static {v2, p1, v3}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    goto :goto_1

    .line 42
    :cond_1
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    double-to-int p1, v2

    .line 47
    add-int/2addr p1, v1

    .line 48
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    goto :goto_1

    .line 53
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-ne v0, v1, :cond_3

    .line 58
    .line 59
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 60
    .line 61
    .line 62
    move-result-wide v2

    .line 63
    double-to-int p1, v2

    .line 64
    add-int/2addr p1, v1

    .line 65
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    :cond_3
    :goto_1
    return-object p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private convertToSectionElement(Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;II)Lcom/intsig/office/simpletext/model/SectionElement;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 4
    .line 5
    .line 6
    const-wide/16 v1, 0x0

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-direct {p0, v1, v2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->initPageProp(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/ss/model/style/CellStyle;II)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    int-to-long p1, p1

    .line 27
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 28
    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawAccountCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move-object/from16 v3, p4

    .line 8
    .line 9
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 14
    .line 15
    .line 16
    move-result v5

    .line 17
    float-to-int v5, v5

    .line 18
    iget v6, v4, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 19
    .line 20
    iget v7, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 21
    .line 22
    sub-float/2addr v6, v7

    .line 23
    float-to-double v6, v6

    .line 24
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 25
    .line 26
    .line 27
    move-result-wide v6

    .line 28
    double-to-int v6, v6

    .line 29
    iget v7, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 30
    .line 31
    const/high16 v8, 0x40000000    # 2.0f

    .line 32
    .line 33
    add-float/2addr v7, v8

    .line 34
    iput v7, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 35
    .line 36
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 37
    .line 38
    .line 39
    move-result-object v7

    .line 40
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 41
    .line 42
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 43
    .line 44
    .line 45
    move-result v10

    .line 46
    invoke-virtual {v9, v10}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidthWithZoom(I)I

    .line 47
    .line 48
    .line 49
    move-result v9

    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 51
    .line 52
    .line 53
    iget-object v10, v0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 54
    .line 55
    invoke-virtual {v1, v10}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 56
    .line 57
    .line 58
    add-int/2addr v5, v9

    .line 59
    const/4 v10, 0x2

    .line 60
    add-int/2addr v5, v10

    .line 61
    int-to-float v5, v5

    .line 62
    iget-object v11, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 63
    .line 64
    invoke-virtual {v11}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 65
    .line 66
    .line 67
    move-result v11

    .line 68
    const/high16 v12, 0x40800000    # 4.0f

    .line 69
    .line 70
    const/4 v13, 0x3

    .line 71
    const/4 v14, 0x0

    .line 72
    const/4 v15, 0x1

    .line 73
    cmpl-float v5, v5, v11

    .line 74
    .line 75
    if-lez v5, :cond_1

    .line 76
    .line 77
    iget-object v2, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    sub-float/2addr v2, v12

    .line 84
    const-string v5, "#"

    .line 85
    .line 86
    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    div-float/2addr v2, v5

    .line 91
    float-to-int v2, v2

    .line 92
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    .line 95
    .line 96
    .line 97
    move-result v7

    .line 98
    invoke-virtual {v5, v14, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    :goto_0
    if-ge v14, v2, :cond_0

    .line 102
    .line 103
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 104
    .line 105
    const/16 v7, 0x23

    .line 106
    .line 107
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    add-int/lit8 v14, v14, 0x1

    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_0
    iget-object v2, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 120
    .line 121
    .line 122
    goto/16 :goto_3

    .line 123
    .line 124
    :cond_1
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 125
    .line 126
    .line 127
    move-result v5

    .line 128
    if-eqz v5, :cond_3

    .line 129
    .line 130
    if-eq v5, v15, :cond_3

    .line 131
    .line 132
    if-eq v5, v13, :cond_2

    .line 133
    .line 134
    goto :goto_1

    .line 135
    :cond_2
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 136
    .line 137
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 138
    .line 139
    .line 140
    move-result v7

    .line 141
    int-to-float v9, v9

    .line 142
    sub-float/2addr v7, v9

    .line 143
    invoke-virtual {v5, v7}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 144
    .line 145
    .line 146
    goto :goto_1

    .line 147
    :cond_3
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 148
    .line 149
    int-to-float v7, v9

    .line 150
    add-float/2addr v5, v7

    .line 151
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 152
    .line 153
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 154
    .line 155
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 156
    .line 157
    .line 158
    move-result v9

    .line 159
    sub-float/2addr v9, v7

    .line 160
    invoke-virtual {v5, v9}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 161
    .line 162
    .line 163
    :goto_1
    const-string v5, "*"

    .line 164
    .line 165
    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 166
    .line 167
    .line 168
    move-result v5

    .line 169
    const/4 v7, -0x1

    .line 170
    if-le v5, v7, :cond_5

    .line 171
    .line 172
    invoke-virtual {v2, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v7

    .line 176
    add-int/2addr v5, v15

    .line 177
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    .line 178
    .line 179
    .line 180
    move-result v9

    .line 181
    invoke-virtual {v2, v5, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v2

    .line 185
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 186
    .line 187
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 188
    .line 189
    .line 190
    move-result v5

    .line 191
    new-instance v9, Ljava/lang/StringBuilder;

    .line 192
    .line 193
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v9

    .line 206
    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 207
    .line 208
    .line 209
    move-result v9

    .line 210
    sub-float/2addr v5, v9

    .line 211
    sub-float/2addr v5, v12

    .line 212
    const-string v9, " "

    .line 213
    .line 214
    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 215
    .line 216
    .line 217
    move-result v9

    .line 218
    div-float/2addr v5, v9

    .line 219
    float-to-int v5, v5

    .line 220
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 221
    .line 222
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    .line 223
    .line 224
    .line 225
    move-result v11

    .line 226
    invoke-virtual {v9, v14, v11}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 230
    .line 231
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    :goto_2
    if-ge v14, v5, :cond_4

    .line 235
    .line 236
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 237
    .line 238
    const/16 v9, 0x20

    .line 239
    .line 240
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    add-int/lit8 v14, v14, 0x1

    .line 244
    .line 245
    goto :goto_2

    .line 246
    :cond_4
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 247
    .line 248
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    iget-object v2, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 252
    .line 253
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v2

    .line 257
    :cond_5
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 258
    .line 259
    .line 260
    move-result-object v5

    .line 261
    invoke-virtual {v5}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 262
    .line 263
    .line 264
    move-result v5

    .line 265
    if-eqz v5, :cond_8

    .line 266
    .line 267
    if-eq v5, v15, :cond_7

    .line 268
    .line 269
    if-eq v5, v10, :cond_6

    .line 270
    .line 271
    if-eq v5, v13, :cond_7

    .line 272
    .line 273
    goto :goto_4

    .line 274
    :cond_6
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 275
    .line 276
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 277
    .line 278
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 279
    .line 280
    .line 281
    move-result v7

    .line 282
    int-to-float v6, v6

    .line 283
    sub-float/2addr v7, v6

    .line 284
    add-float/2addr v5, v7

    .line 285
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 286
    .line 287
    goto :goto_4

    .line 288
    :cond_7
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 289
    .line 290
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 291
    .line 292
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 293
    .line 294
    .line 295
    move-result v7

    .line 296
    int-to-float v6, v6

    .line 297
    sub-float/2addr v7, v6

    .line 298
    div-float/2addr v7, v8

    .line 299
    add-float/2addr v5, v7

    .line 300
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 301
    .line 302
    goto :goto_4

    .line 303
    :cond_8
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 304
    .line 305
    add-float/2addr v5, v8

    .line 306
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 307
    .line 308
    :goto_4
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 309
    .line 310
    iget-object v6, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 311
    .line 312
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 313
    .line 314
    .line 315
    move-result v6

    .line 316
    sub-float/2addr v5, v6

    .line 317
    iget-object v6, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 318
    .line 319
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 320
    .line 321
    .line 322
    move-result v6

    .line 323
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 324
    .line 325
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 326
    .line 327
    .line 328
    move-result v7

    .line 329
    sub-float/2addr v6, v7

    .line 330
    sub-float/2addr v5, v6

    .line 331
    iget v6, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 332
    .line 333
    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 334
    .line 335
    sub-float/2addr v6, v4

    .line 336
    iget-object v4, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 337
    .line 338
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 339
    .line 340
    .line 341
    move-result v4

    .line 342
    sub-float/2addr v6, v4

    .line 343
    iget-object v4, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 344
    .line 345
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 346
    .line 347
    .line 348
    move-result v4

    .line 349
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 350
    .line 351
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 352
    .line 353
    .line 354
    move-result v7

    .line 355
    sub-float/2addr v4, v7

    .line 356
    sub-float/2addr v6, v4

    .line 357
    invoke-virtual {v1, v2, v5, v6, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 358
    .line 359
    .line 360
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 361
    .line 362
    .line 363
    return-void
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawCellContents(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/other/DrawingCell;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-ltz v1, :cond_1

    .line 12
    .line 13
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedRangeAddress(I)Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getRangedAddress()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 34
    .line 35
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 36
    .line 37
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 42
    .line 43
    .line 44
    move-result v6

    .line 45
    invoke-virtual {v3, v4, v2, v5, v6}, Lcom/intsig/office/ss/other/MergeCellMgr;->isDrawMergeCell(Lcom/intsig/office/ss/view/SheetView;Lcom/intsig/office/ss/model/CellRangeAddress;II)Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-eqz v3, :cond_0

    .line 50
    .line 51
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 52
    .line 53
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 54
    .line 55
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    invoke-virtual {v3, v4, v2, v5, v6}, Lcom/intsig/office/ss/other/MergeCellMgr;->getMergedCellSize(Lcom/intsig/office/ss/view/SheetView;Lcom/intsig/office/ss/model/CellRangeAddress;II)Lcom/intsig/office/ss/other/MergeCell;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    iput-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 68
    .line 69
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 70
    .line 71
    .line 72
    move-result p2

    .line 73
    invoke-virtual {v1, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getExpandedRangeAddress(I)Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/ExpandedCellRangeAddress;->getExpandedCell()Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 82
    .line 83
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 84
    .line 85
    iget v3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 86
    .line 87
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 88
    .line 89
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    add-float/2addr v4, v2

    .line 94
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 95
    .line 96
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    sub-float/2addr v4, v5

    .line 101
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 106
    .line 107
    .line 108
    move-result v6

    .line 109
    sub-float/2addr v5, v6

    .line 110
    sub-float/2addr v4, v5

    .line 111
    iget v5, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 112
    .line 113
    iget-object v6, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 114
    .line 115
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 116
    .line 117
    .line 118
    move-result v6

    .line 119
    add-float/2addr v5, v6

    .line 120
    iget-object v6, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 121
    .line 122
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 123
    .line 124
    .line 125
    move-result v6

    .line 126
    sub-float/2addr v5, v6

    .line 127
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 128
    .line 129
    .line 130
    move-result v6

    .line 131
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 132
    .line 133
    .line 134
    move-result p3

    .line 135
    sub-float/2addr v6, p3

    .line 136
    sub-float/2addr v5, v6

    .line 137
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_0
    return-void

    .line 142
    :cond_1
    :goto_0
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 143
    .line 144
    .line 145
    move-result-object p3

    .line 146
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {p3, v1, p2}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object p3

    .line 154
    if-eqz p3, :cond_7

    .line 155
    .line 156
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 157
    .line 158
    .line 159
    move-result v1

    .line 160
    if-nez v1, :cond_2

    .line 161
    .line 162
    goto :goto_2

    .line 163
    :cond_2
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontKit;->instance()Lcom/intsig/office/simpletext/font/FontKit;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    invoke-virtual {v1, p2, v0, p4}, Lcom/intsig/office/simpletext/font/FontKit;->getCellPaint(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)Landroid/graphics/Paint;

    .line 172
    .line 173
    .line 174
    move-result-object p4

    .line 175
    invoke-virtual {p4}, Landroid/graphics/Paint;->getTextSize()F

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 180
    .line 181
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 182
    .line 183
    .line 184
    move-result v1

    .line 185
    mul-float v1, v1, v0

    .line 186
    .line 187
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 188
    .line 189
    .line 190
    const/4 v1, 0x0

    .line 191
    iput-boolean v1, p0, Lcom/intsig/office/ss/view/CellView;->numericCellAlignRight:Z

    .line 192
    .line 193
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 194
    .line 195
    .line 196
    move-result v1

    .line 197
    const/4 v2, 0x4

    .line 198
    if-eq v1, v2, :cond_3

    .line 199
    .line 200
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 201
    .line 202
    .line 203
    move-result v1

    .line 204
    if-nez v1, :cond_4

    .line 205
    .line 206
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 207
    .line 208
    .line 209
    move-result v1

    .line 210
    const/16 v2, 0xb

    .line 211
    .line 212
    if-eq v1, v2, :cond_4

    .line 213
    .line 214
    :cond_3
    const/4 v1, 0x1

    .line 215
    iput-boolean v1, p0, Lcom/intsig/office/ss/view/CellView;->numericCellAlignRight:Z

    .line 216
    .line 217
    :cond_4
    invoke-virtual {p4}, Landroid/graphics/Paint;->getColor()I

    .line 218
    .line 219
    .line 220
    move-result v1

    .line 221
    const/4 v2, -0x1

    .line 222
    if-ne v1, v2, :cond_5

    .line 223
    .line 224
    const-string v1, "#212121"

    .line 225
    .line 226
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 227
    .line 228
    .line 229
    move-result v1

    .line 230
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 231
    .line 232
    .line 233
    :cond_5
    iget-boolean v1, p0, Lcom/intsig/office/ss/view/CellView;->numericCellAlignRight:Z

    .line 234
    .line 235
    if-eqz v1, :cond_6

    .line 236
    .line 237
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->drawNumericCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V

    .line 238
    .line 239
    .line 240
    goto :goto_1

    .line 241
    :cond_6
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->drawNonNumericCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V

    .line 242
    .line 243
    .line 244
    :goto_1
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 245
    .line 246
    .line 247
    :cond_7
    :goto_2
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->drawWrapComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->drawNotWrapComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private drawGeneralCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 6

    .line 1
    invoke-direct {p0, p3, p4}, Lcom/intsig/office/ss/view/CellView;->adjustGeneralCellContent(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p3

    .line 5
    invoke-virtual {p4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 10
    .line 11
    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 12
    .line 13
    sub-float/2addr v1, v2

    .line 14
    float-to-double v1, v1

    .line 15
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide v1

    .line 19
    double-to-int v1, v1

    .line 20
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 21
    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 24
    .line 25
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 26
    .line 27
    .line 28
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    const/high16 v3, 0x40000000    # 2.0f

    .line 37
    .line 38
    if-eqz v2, :cond_2

    .line 39
    .line 40
    const/4 v4, 0x1

    .line 41
    if-eq v2, v4, :cond_1

    .line 42
    .line 43
    const/4 v4, 0x2

    .line 44
    if-eq v2, v4, :cond_0

    .line 45
    .line 46
    const/4 v4, 0x3

    .line 47
    if-eq v2, v4, :cond_1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 51
    .line 52
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 53
    .line 54
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    int-to-float v1, v1

    .line 59
    sub-float/2addr v4, v1

    .line 60
    add-float/2addr v2, v4

    .line 61
    iput v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 65
    .line 66
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 67
    .line 68
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    int-to-float v1, v1

    .line 73
    sub-float/2addr v4, v1

    .line 74
    div-float/2addr v4, v3

    .line 75
    add-float/2addr v2, v4

    .line 76
    iput v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_2
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 80
    .line 81
    add-float/2addr v1, v3

    .line 82
    iput v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 83
    .line 84
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 85
    .line 86
    .line 87
    move-result-object p2

    .line 88
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 89
    .line 90
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidthWithZoom(I)I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    add-int/lit8 v2, v1, 0x4

    .line 99
    .line 100
    int-to-float v2, v2

    .line 101
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 102
    .line 103
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 104
    .line 105
    .line 106
    move-result v4

    .line 107
    cmpl-float v2, v2, v4

    .line 108
    .line 109
    if-ltz v2, :cond_3

    .line 110
    .line 111
    invoke-direct {p0, p3}, Lcom/intsig/office/ss/view/CellView;->isNumeric(Ljava/lang/String;)Z

    .line 112
    .line 113
    .line 114
    move-result v2

    .line 115
    if-nez v2, :cond_3

    .line 116
    .line 117
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 118
    .line 119
    .line 120
    return-void

    .line 121
    :cond_3
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 122
    .line 123
    .line 124
    move-result v2

    .line 125
    float-to-int v2, v2

    .line 126
    add-int v4, v2, v1

    .line 127
    .line 128
    add-int/lit8 v4, v4, 0x4

    .line 129
    .line 130
    int-to-float v4, v4

    .line 131
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 132
    .line 133
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    cmpl-float v4, v4, v5

    .line 138
    .line 139
    if-lez v4, :cond_5

    .line 140
    .line 141
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 142
    .line 143
    add-float/2addr p2, v3

    .line 144
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 145
    .line 146
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 147
    .line 148
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 149
    .line 150
    .line 151
    move-result p2

    .line 152
    const/high16 p3, 0x40800000    # 4.0f

    .line 153
    .line 154
    sub-float/2addr p2, p3

    .line 155
    const-string p3, "#"

    .line 156
    .line 157
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 158
    .line 159
    .line 160
    move-result p3

    .line 161
    div-float/2addr p2, p3

    .line 162
    float-to-int p2, p2

    .line 163
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 164
    .line 165
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    .line 166
    .line 167
    .line 168
    move-result v1

    .line 169
    const/4 v2, 0x0

    .line 170
    invoke-virtual {p3, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    :goto_1
    if-ge v2, p2, :cond_4

    .line 174
    .line 175
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 176
    .line 177
    const/16 v1, 0x23

    .line 178
    .line 179
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    add-int/lit8 v2, v2, 0x1

    .line 183
    .line 184
    goto :goto_1

    .line 185
    :cond_4
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 186
    .line 187
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object p3

    .line 191
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 192
    .line 193
    .line 194
    goto :goto_2

    .line 195
    :cond_5
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 196
    .line 197
    .line 198
    move-result p2

    .line 199
    packed-switch p2, :pswitch_data_0

    .line 200
    .line 201
    .line 202
    goto :goto_2

    .line 203
    :pswitch_0
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 204
    .line 205
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 206
    .line 207
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 208
    .line 209
    .line 210
    move-result v1

    .line 211
    int-to-float v2, v2

    .line 212
    sub-float/2addr v1, v2

    .line 213
    div-float/2addr v1, v3

    .line 214
    add-float/2addr p2, v1

    .line 215
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 216
    .line 217
    goto :goto_2

    .line 218
    :pswitch_1
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 219
    .line 220
    add-int/lit8 v2, v1, 0x2

    .line 221
    .line 222
    int-to-float v2, v2

    .line 223
    add-float/2addr p2, v2

    .line 224
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 225
    .line 226
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 227
    .line 228
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 229
    .line 230
    .line 231
    move-result v2

    .line 232
    int-to-float v1, v1

    .line 233
    sub-float/2addr v2, v1

    .line 234
    invoke-virtual {p2, v2}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 235
    .line 236
    .line 237
    goto :goto_2

    .line 238
    :pswitch_2
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 239
    .line 240
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 241
    .line 242
    .line 243
    move-result v2

    .line 244
    int-to-float v1, v1

    .line 245
    sub-float/2addr v2, v1

    .line 246
    invoke-virtual {p2, v2}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 247
    .line 248
    .line 249
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 250
    .line 251
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 252
    .line 253
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 254
    .line 255
    .line 256
    move-result v1

    .line 257
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 258
    .line 259
    .line 260
    move-result v2

    .line 261
    float-to-int v2, v2

    .line 262
    int-to-float v2, v2

    .line 263
    sub-float/2addr v1, v2

    .line 264
    sub-float/2addr v1, v3

    .line 265
    add-float/2addr p2, v1

    .line 266
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 267
    .line 268
    :goto_2
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 269
    .line 270
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 271
    .line 272
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 273
    .line 274
    .line 275
    move-result v1

    .line 276
    sub-float/2addr p2, v1

    .line 277
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 278
    .line 279
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 280
    .line 281
    .line 282
    move-result v1

    .line 283
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 284
    .line 285
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 286
    .line 287
    .line 288
    move-result v2

    .line 289
    sub-float/2addr v1, v2

    .line 290
    sub-float/2addr p2, v1

    .line 291
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 292
    .line 293
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 294
    .line 295
    sub-float/2addr v1, v0

    .line 296
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 297
    .line 298
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 299
    .line 300
    .line 301
    move-result v0

    .line 302
    sub-float/2addr v1, v0

    .line 303
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 304
    .line 305
    invoke-virtual {v0}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 306
    .line 307
    .line 308
    move-result v0

    .line 309
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 310
    .line 311
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 312
    .line 313
    .line 314
    move-result v2

    .line 315
    sub-float/2addr v0, v2

    .line 316
    sub-float/2addr v1, v0

    .line 317
    invoke-virtual {p1, p3, p2, v1, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 321
    .line 322
    .line 323
    return-void

    .line 324
    nop

    .line 325
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawGradientAndTile(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/RectF;FLandroid/graphics/Paint;)V
    .locals 16

    .line 1
    move-object/from16 v0, p5

    .line 2
    .line 3
    move/from16 v1, p6

    .line 4
    .line 5
    move-object/from16 v2, p7

    .line 6
    .line 7
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getShader()Lcom/intsig/office/common/bg/AShader;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    if-eqz v3, :cond_9

    .line 12
    .line 13
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/AShader;->getShader()Landroid/graphics/Shader;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    const/high16 v5, 0x3f800000    # 1.0f

    .line 18
    .line 19
    if-nez v4, :cond_0

    .line 20
    .line 21
    div-float v4, v5, v1

    .line 22
    .line 23
    new-instance v6, Landroid/graphics/Rect;

    .line 24
    .line 25
    iget v7, v0, Landroid/graphics/RectF;->left:F

    .line 26
    .line 27
    mul-float v7, v7, v4

    .line 28
    .line 29
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 30
    .line 31
    .line 32
    move-result v7

    .line 33
    iget v8, v0, Landroid/graphics/RectF;->top:F

    .line 34
    .line 35
    mul-float v8, v8, v4

    .line 36
    .line 37
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 38
    .line 39
    .line 40
    move-result v8

    .line 41
    iget v9, v0, Landroid/graphics/RectF;->right:F

    .line 42
    .line 43
    mul-float v9, v9, v4

    .line 44
    .line 45
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 46
    .line 47
    .line 48
    move-result v9

    .line 49
    iget v10, v0, Landroid/graphics/RectF;->bottom:F

    .line 50
    .line 51
    mul-float v10, v10, v4

    .line 52
    .line 53
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    invoke-direct {v6, v7, v8, v9, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 58
    .line 59
    .line 60
    move-object/from16 v4, p2

    .line 61
    .line 62
    move/from16 v7, p3

    .line 63
    .line 64
    invoke-virtual {v3, v4, v7, v6}, Lcom/intsig/office/common/bg/AShader;->createShader(Lcom/intsig/office/system/IControl;ILandroid/graphics/Rect;)Landroid/graphics/Shader;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    if-nez v4, :cond_0

    .line 69
    .line 70
    return-void

    .line 71
    :cond_0
    new-instance v6, Landroid/graphics/Matrix;

    .line 72
    .line 73
    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 74
    .line 75
    .line 76
    iget v7, v0, Landroid/graphics/RectF;->left:F

    .line 77
    .line 78
    iget v8, v0, Landroid/graphics/RectF;->top:F

    .line 79
    .line 80
    instance-of v9, v3, Lcom/intsig/office/common/bg/TileShader;

    .line 81
    .line 82
    if-eqz v9, :cond_1

    .line 83
    .line 84
    check-cast v3, Lcom/intsig/office/common/bg/TileShader;

    .line 85
    .line 86
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/TileShader;->getOffsetX()I

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    int-to-float v5, v5

    .line 91
    mul-float v5, v5, v1

    .line 92
    .line 93
    add-float/2addr v7, v5

    .line 94
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/TileShader;->getOffsetY()I

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    int-to-float v3, v3

    .line 99
    mul-float v3, v3, v1

    .line 100
    .line 101
    add-float/2addr v8, v3

    .line 102
    invoke-virtual {v6, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 103
    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_1
    instance-of v1, v3, Lcom/intsig/office/common/bg/PatternShader;

    .line 107
    .line 108
    if-eqz v1, :cond_2

    .line 109
    .line 110
    goto :goto_2

    .line 111
    :cond_2
    instance-of v1, v3, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 112
    .line 113
    if-eqz v1, :cond_8

    .line 114
    .line 115
    check-cast v3, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 116
    .line 117
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/LinearGradientShader;->getAngle()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    const/16 v9, 0x5a

    .line 122
    .line 123
    const/16 v10, 0x64

    .line 124
    .line 125
    const/16 v11, 0x32

    .line 126
    .line 127
    const/16 v12, -0x32

    .line 128
    .line 129
    const/4 v13, 0x0

    .line 130
    const/high16 v14, 0x3f000000    # 0.5f

    .line 131
    .line 132
    const/high16 v15, -0x41000000    # -0.5f

    .line 133
    .line 134
    if-ne v1, v9, :cond_3

    .line 135
    .line 136
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/Gradient;->getFocus()I

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-eq v1, v12, :cond_6

    .line 141
    .line 142
    if-eqz v1, :cond_4

    .line 143
    .line 144
    if-eq v1, v11, :cond_7

    .line 145
    .line 146
    if-eq v1, v10, :cond_5

    .line 147
    .line 148
    goto :goto_0

    .line 149
    :cond_3
    invoke-virtual {v3}, Lcom/intsig/office/common/bg/Gradient;->getFocus()I

    .line 150
    .line 151
    .line 152
    move-result v1

    .line 153
    if-eq v1, v12, :cond_7

    .line 154
    .line 155
    if-eqz v1, :cond_4

    .line 156
    .line 157
    if-eq v1, v11, :cond_6

    .line 158
    .line 159
    if-eq v1, v10, :cond_5

    .line 160
    .line 161
    :cond_4
    :goto_0
    const/high16 v13, 0x3f800000    # 1.0f

    .line 162
    .line 163
    goto :goto_1

    .line 164
    :cond_5
    const/4 v5, 0x0

    .line 165
    goto :goto_1

    .line 166
    :cond_6
    const/high16 v5, 0x3f000000    # 0.5f

    .line 167
    .line 168
    const/high16 v13, 0x3f000000    # 0.5f

    .line 169
    .line 170
    goto :goto_1

    .line 171
    :cond_7
    const/high16 v5, -0x41000000    # -0.5f

    .line 172
    .line 173
    const/high16 v13, -0x41000000    # -0.5f

    .line 174
    .line 175
    :goto_1
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->width()F

    .line 176
    .line 177
    .line 178
    move-result v1

    .line 179
    mul-float v5, v5, v1

    .line 180
    .line 181
    add-float/2addr v7, v5

    .line 182
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->height()F

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    mul-float v13, v13, v1

    .line 187
    .line 188
    add-float/2addr v8, v13

    .line 189
    :cond_8
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->width()F

    .line 190
    .line 191
    .line 192
    move-result v1

    .line 193
    const/high16 v3, 0x42c80000    # 100.0f

    .line 194
    .line 195
    div-float/2addr v1, v3

    .line 196
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/RectF;->height()F

    .line 197
    .line 198
    .line 199
    move-result v5

    .line 200
    div-float/2addr v5, v3

    .line 201
    invoke-virtual {v6, v1, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 202
    .line 203
    .line 204
    :goto_2
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 205
    .line 206
    .line 207
    invoke-virtual {v4, v6}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 211
    .line 212
    .line 213
    move-object/from16 v1, p1

    .line 214
    .line 215
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 216
    .line 217
    .line 218
    const/4 v0, 0x0

    .line 219
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 220
    .line 221
    .line 222
    :cond_9
    return-void
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
.end method

.method private drawNonNumericCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 2

    .line 1
    invoke-static {p2}, Lcom/intsig/office/ss/view/CellView;->isComplexText(Lcom/intsig/office/ss/model/baseModel/Cell;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->drawComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    const-string v0, "\n"

    .line 22
    .line 23
    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    const/high16 v1, 0x40800000    # 4.0f

    .line 34
    .line 35
    add-float/2addr v0, v1

    .line 36
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    cmpl-float v0, v0, v1

    .line 43
    .line 44
    if-lez v0, :cond_2

    .line 45
    .line 46
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 47
    .line 48
    .line 49
    iget-object p4, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 50
    .line 51
    invoke-virtual {p1, p4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/ss/view/CellView;->drawWrapText(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    const/16 v1, 0x400

    .line 66
    .line 67
    if-le v0, v1, :cond_3

    .line 68
    .line 69
    const/4 v0, 0x0

    .line 70
    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p3

    .line 74
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->drawNonWrapText(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawNonWrapText(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 21

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p4

    .line 6
    .line 7
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    move-object/from16 v4, p3

    .line 12
    .line 13
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 14
    .line 15
    .line 16
    move-result v5

    .line 17
    float-to-int v5, v5

    .line 18
    iget v6, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 19
    .line 20
    iget v7, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 21
    .line 22
    sub-float/2addr v6, v7

    .line 23
    float-to-double v6, v6

    .line 24
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 25
    .line 26
    .line 27
    move-result-wide v6

    .line 28
    double-to-int v6, v6

    .line 29
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 30
    .line 31
    .line 32
    move-result-object v7

    .line 33
    iget-object v8, v0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 34
    .line 35
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 36
    .line 37
    .line 38
    move-result v9

    .line 39
    invoke-virtual {v8, v9}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidthWithZoom(I)I

    .line 40
    .line 41
    .line 42
    move-result v8

    .line 43
    add-int/lit8 v9, v8, 0x4

    .line 44
    .line 45
    int-to-float v9, v9

    .line 46
    iget-object v10, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 47
    .line 48
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 49
    .line 50
    .line 51
    move-result v10

    .line 52
    cmpl-float v9, v9, v10

    .line 53
    .line 54
    if-ltz v9, :cond_0

    .line 55
    .line 56
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 57
    .line 58
    .line 59
    return-void

    .line 60
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 61
    .line 62
    .line 63
    move-result v9

    .line 64
    const/16 v10, 0xa

    .line 65
    .line 66
    const/4 v11, 0x0

    .line 67
    if-ne v9, v10, :cond_2

    .line 68
    .line 69
    add-int v9, v5, v8

    .line 70
    .line 71
    add-int/lit8 v9, v9, 0x4

    .line 72
    .line 73
    int-to-float v9, v9

    .line 74
    iget-object v12, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 75
    .line 76
    invoke-virtual {v12}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 77
    .line 78
    .line 79
    move-result v12

    .line 80
    cmpl-float v9, v9, v12

    .line 81
    .line 82
    if-lez v9, :cond_2

    .line 83
    .line 84
    iget-object v4, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 85
    .line 86
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    const/high16 v5, 0x40800000    # 4.0f

    .line 91
    .line 92
    sub-float/2addr v4, v5

    .line 93
    const-string v5, "#"

    .line 94
    .line 95
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    div-float/2addr v4, v5

    .line 100
    float-to-int v4, v4

    .line 101
    iget-object v5, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    .line 104
    .line 105
    .line 106
    move-result v8

    .line 107
    invoke-virtual {v5, v11, v8}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    const/4 v5, 0x0

    .line 111
    :goto_0
    if-ge v5, v4, :cond_1

    .line 112
    .line 113
    iget-object v8, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 114
    .line 115
    const/16 v9, 0x23

    .line 116
    .line 117
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    add-int/lit8 v5, v5, 0x1

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_1
    iget-object v4, v0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 124
    .line 125
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v4

    .line 129
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 130
    .line 131
    .line 132
    move-result v5

    .line 133
    float-to-int v5, v5

    .line 134
    const/4 v8, 0x0

    .line 135
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 136
    .line 137
    .line 138
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 139
    .line 140
    invoke-virtual {v1, v9}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 141
    .line 142
    .line 143
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 144
    .line 145
    .line 146
    move-result v9

    .line 147
    const/4 v12, 0x3

    .line 148
    if-nez v9, :cond_3

    .line 149
    .line 150
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 151
    .line 152
    .line 153
    move-result v13

    .line 154
    if-ne v13, v10, :cond_3

    .line 155
    .line 156
    const/4 v9, 0x3

    .line 157
    :cond_3
    const/4 v10, 0x2

    .line 158
    const/4 v13, 0x1

    .line 159
    const/high16 v14, 0x40000000    # 2.0f

    .line 160
    .line 161
    packed-switch v9, :pswitch_data_0

    .line 162
    .line 163
    .line 164
    goto/16 :goto_3

    .line 165
    .line 166
    :pswitch_0
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 167
    .line 168
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 169
    .line 170
    .line 171
    move-result v15

    .line 172
    int-to-float v8, v8

    .line 173
    sub-float/2addr v15, v8

    .line 174
    invoke-virtual {v9, v15}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 175
    .line 176
    .line 177
    add-int/lit8 v8, v5, 0x2

    .line 178
    .line 179
    int-to-float v8, v8

    .line 180
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 181
    .line 182
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 183
    .line 184
    .line 185
    move-result v9

    .line 186
    cmpl-float v8, v8, v9

    .line 187
    .line 188
    if-lez v8, :cond_5

    .line 189
    .line 190
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 191
    .line 192
    .line 193
    move-result v5

    .line 194
    new-array v5, v5, [F

    .line 195
    .line 196
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 197
    .line 198
    .line 199
    move-result v8

    .line 200
    invoke-virtual {v2, v4, v11, v8, v5}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 201
    .line 202
    .line 203
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 204
    .line 205
    .line 206
    move-result v8

    .line 207
    const/4 v9, 0x0

    .line 208
    :goto_1
    iget-object v11, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 209
    .line 210
    invoke-virtual {v11}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 211
    .line 212
    .line 213
    move-result v11

    .line 214
    sub-float/2addr v11, v14

    .line 215
    cmpg-float v11, v9, v11

    .line 216
    .line 217
    if-gez v11, :cond_4

    .line 218
    .line 219
    add-int/lit8 v8, v8, -0x1

    .line 220
    .line 221
    aget v11, v5, v8

    .line 222
    .line 223
    add-float/2addr v9, v11

    .line 224
    goto :goto_1

    .line 225
    :cond_4
    add-int/2addr v8, v13

    .line 226
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 227
    .line 228
    .line 229
    move-result v5

    .line 230
    invoke-virtual {v4, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 231
    .line 232
    .line 233
    move-result-object v4

    .line 234
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 235
    .line 236
    .line 237
    move-result v5

    .line 238
    float-to-int v5, v5

    .line 239
    :cond_5
    iget v8, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 240
    .line 241
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 242
    .line 243
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 244
    .line 245
    .line 246
    move-result v9

    .line 247
    int-to-float v5, v5

    .line 248
    sub-float/2addr v9, v5

    .line 249
    sub-float/2addr v9, v14

    .line 250
    add-float/2addr v8, v9

    .line 251
    iput v8, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 252
    .line 253
    goto :goto_3

    .line 254
    :pswitch_1
    iget v8, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 255
    .line 256
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 257
    .line 258
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 259
    .line 260
    .line 261
    move-result v9

    .line 262
    int-to-float v5, v5

    .line 263
    sub-float/2addr v9, v5

    .line 264
    div-float/2addr v9, v14

    .line 265
    add-float/2addr v8, v9

    .line 266
    iput v8, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 267
    .line 268
    goto :goto_3

    .line 269
    :pswitch_2
    iget v9, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 270
    .line 271
    add-int/lit8 v15, v8, 0x2

    .line 272
    .line 273
    int-to-float v15, v15

    .line 274
    add-float/2addr v9, v15

    .line 275
    iput v9, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 276
    .line 277
    iget-object v9, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 278
    .line 279
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 280
    .line 281
    .line 282
    move-result v15

    .line 283
    int-to-float v8, v8

    .line 284
    sub-float/2addr v15, v8

    .line 285
    invoke-virtual {v9, v15}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 286
    .line 287
    .line 288
    add-int/2addr v5, v10

    .line 289
    int-to-float v5, v5

    .line 290
    iget-object v8, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 291
    .line 292
    invoke-virtual {v8}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 293
    .line 294
    .line 295
    move-result v8

    .line 296
    cmpl-float v5, v5, v8

    .line 297
    .line 298
    if-lez v5, :cond_7

    .line 299
    .line 300
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 301
    .line 302
    .line 303
    move-result v5

    .line 304
    new-array v5, v5, [F

    .line 305
    .line 306
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 307
    .line 308
    .line 309
    move-result v8

    .line 310
    invoke-virtual {v2, v4, v11, v8, v5}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 311
    .line 312
    .line 313
    aget v8, v5, v11

    .line 314
    .line 315
    const/4 v9, 0x0

    .line 316
    :goto_2
    iget-object v15, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 317
    .line 318
    invoke-virtual {v15}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 319
    .line 320
    .line 321
    move-result v15

    .line 322
    sub-float/2addr v15, v14

    .line 323
    cmpg-float v15, v8, v15

    .line 324
    .line 325
    if-gez v15, :cond_6

    .line 326
    .line 327
    add-int/lit8 v9, v9, 0x1

    .line 328
    .line 329
    aget v15, v5, v9

    .line 330
    .line 331
    add-float/2addr v8, v15

    .line 332
    goto :goto_2

    .line 333
    :cond_6
    invoke-virtual {v4, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 334
    .line 335
    .line 336
    move-result-object v4

    .line 337
    :cond_7
    :goto_3
    invoke-virtual {v7}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 338
    .line 339
    .line 340
    move-result v5

    .line 341
    if-eqz v5, :cond_a

    .line 342
    .line 343
    if-eq v5, v13, :cond_9

    .line 344
    .line 345
    if-eq v5, v10, :cond_8

    .line 346
    .line 347
    if-eq v5, v12, :cond_9

    .line 348
    .line 349
    goto :goto_4

    .line 350
    :cond_8
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 351
    .line 352
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 353
    .line 354
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 355
    .line 356
    .line 357
    move-result v7

    .line 358
    int-to-float v6, v6

    .line 359
    sub-float/2addr v7, v6

    .line 360
    add-float/2addr v5, v7

    .line 361
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 362
    .line 363
    goto :goto_4

    .line 364
    :cond_9
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 365
    .line 366
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 367
    .line 368
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 369
    .line 370
    .line 371
    move-result v7

    .line 372
    int-to-float v6, v6

    .line 373
    sub-float/2addr v7, v6

    .line 374
    div-float/2addr v7, v14

    .line 375
    add-float/2addr v5, v7

    .line 376
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 377
    .line 378
    goto :goto_4

    .line 379
    :cond_a
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 380
    .line 381
    add-float/2addr v5, v14

    .line 382
    iput v5, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 383
    .line 384
    :goto_4
    iget v5, v0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 385
    .line 386
    iget-object v6, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 387
    .line 388
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 389
    .line 390
    .line 391
    move-result v6

    .line 392
    sub-float/2addr v5, v6

    .line 393
    iget-object v6, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 394
    .line 395
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 396
    .line 397
    .line 398
    move-result v6

    .line 399
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 400
    .line 401
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 402
    .line 403
    .line 404
    move-result v7

    .line 405
    sub-float/2addr v6, v7

    .line 406
    sub-float/2addr v5, v6

    .line 407
    iget v6, v0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 408
    .line 409
    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 410
    .line 411
    sub-float/2addr v6, v3

    .line 412
    iget-object v3, v0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 413
    .line 414
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 415
    .line 416
    .line 417
    move-result v3

    .line 418
    sub-float/2addr v6, v3

    .line 419
    iget-object v3, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 420
    .line 421
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 422
    .line 423
    .line 424
    move-result v3

    .line 425
    iget-object v7, v0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 426
    .line 427
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 428
    .line 429
    .line 430
    move-result v7

    .line 431
    sub-float/2addr v3, v7

    .line 432
    sub-float v3, v6, v3

    .line 433
    .line 434
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 435
    .line 436
    .line 437
    move-result v6

    .line 438
    if-ltz v6, :cond_b

    .line 439
    .line 440
    iget-object v15, v0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 441
    .line 442
    new-instance v2, Landroid/graphics/RectF;

    .line 443
    .line 444
    iget-object v6, v0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 445
    .line 446
    invoke-direct {v2, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 447
    .line 448
    .line 449
    move-object/from16 v16, p2

    .line 450
    .line 451
    move-object/from16 v17, v2

    .line 452
    .line 453
    move/from16 v18, v5

    .line 454
    .line 455
    move/from16 v19, v3

    .line 456
    .line 457
    move-object/from16 v20, v4

    .line 458
    .line 459
    invoke-virtual/range {v15 .. v20}, Lcom/intsig/office/ss/view/SheetView;->addExtendCell(Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;FFLjava/lang/Object;)V

    .line 460
    .line 461
    .line 462
    goto :goto_5

    .line 463
    :cond_b
    invoke-virtual {v1, v4, v5, v3, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 464
    .line 465
    .line 466
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 467
    .line 468
    .line 469
    return-void

    .line 470
    nop

    .line 471
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

.method private drawNotWrapComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSTRoot()Lcom/intsig/office/simpletext/view/STRoot;

    .line 14
    .line 15
    .line 16
    move-result-object v6

    .line 17
    if-nez v6, :cond_0

    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-interface {v1, v2}, Lcom/intsig/office/simpletext/view/IView;->getLayoutSpan(B)I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    int-to-float v2, v2

    .line 37
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 38
    .line 39
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    mul-float v2, v2, v3

    .line 44
    .line 45
    float-to-int v2, v2

    .line 46
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getHeight()I

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 52
    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    invoke-virtual {v1, v3}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidthWithZoom(I)I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    add-int/lit8 v3, v1, 0x4

    .line 65
    .line 66
    int-to-float v3, v3

    .line 67
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 68
    .line 69
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    cmpl-float v3, v3, v4

    .line 74
    .line 75
    if-ltz v3, :cond_1

    .line 76
    .line 77
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 78
    .line 79
    .line 80
    return-void

    .line 81
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    const/high16 v4, 0x40000000    # 2.0f

    .line 86
    .line 87
    packed-switch v3, :pswitch_data_0

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :pswitch_0
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 92
    .line 93
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    int-to-float v1, v1

    .line 98
    sub-float/2addr v5, v1

    .line 99
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 100
    .line 101
    .line 102
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 103
    .line 104
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 105
    .line 106
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    int-to-float v2, v2

    .line 111
    sub-float/2addr v3, v2

    .line 112
    sub-float/2addr v3, v4

    .line 113
    add-float/2addr v1, v3

    .line 114
    iput v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :pswitch_1
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 118
    .line 119
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 120
    .line 121
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 122
    .line 123
    .line 124
    move-result v3

    .line 125
    int-to-float v2, v2

    .line 126
    sub-float/2addr v3, v2

    .line 127
    div-float/2addr v3, v4

    .line 128
    add-float/2addr v1, v3

    .line 129
    iput v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 130
    .line 131
    goto :goto_0

    .line 132
    :pswitch_2
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 133
    .line 134
    add-float/2addr v2, v4

    .line 135
    iput v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 136
    .line 137
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 140
    .line 141
    .line 142
    move-result v3

    .line 143
    int-to-float v1, v1

    .line 144
    sub-float/2addr v3, v1

    .line 145
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 146
    .line 147
    .line 148
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    if-eqz v0, :cond_4

    .line 153
    .line 154
    const/4 v1, 0x1

    .line 155
    if-eq v0, v1, :cond_3

    .line 156
    .line 157
    const/4 v1, 0x2

    .line 158
    if-eq v0, v1, :cond_2

    .line 159
    .line 160
    const/4 v1, 0x3

    .line 161
    if-eq v0, v1, :cond_3

    .line 162
    .line 163
    goto :goto_1

    .line 164
    :cond_2
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 165
    .line 166
    add-float/2addr v0, v4

    .line 167
    iput v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 168
    .line 169
    goto :goto_1

    .line 170
    :cond_3
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 171
    .line 172
    add-float/2addr v0, v4

    .line 173
    iput v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 174
    .line 175
    goto :goto_1

    .line 176
    :cond_4
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 177
    .line 178
    add-float/2addr v0, v4

    .line 179
    iput v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 180
    .line 181
    :goto_1
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 182
    .line 183
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 184
    .line 185
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    sub-float/2addr v0, v1

    .line 190
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 191
    .line 192
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 197
    .line 198
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 199
    .line 200
    .line 201
    move-result v2

    .line 202
    sub-float/2addr v1, v2

    .line 203
    sub-float v4, v0, v1

    .line 204
    .line 205
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 206
    .line 207
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 208
    .line 209
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 210
    .line 211
    .line 212
    move-result v1

    .line 213
    sub-float/2addr v0, v1

    .line 214
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 215
    .line 216
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 217
    .line 218
    .line 219
    move-result v1

    .line 220
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 221
    .line 222
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 223
    .line 224
    .line 225
    move-result v2

    .line 226
    sub-float/2addr v1, v2

    .line 227
    sub-float v5, v0, v1

    .line 228
    .line 229
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getExpandedRangeAddressIndex()I

    .line 230
    .line 231
    .line 232
    move-result v0

    .line 233
    if-ltz v0, :cond_5

    .line 234
    .line 235
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 236
    .line 237
    new-instance v3, Landroid/graphics/RectF;

    .line 238
    .line 239
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 240
    .line 241
    invoke-direct {v3, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 242
    .line 243
    .line 244
    move-object v2, p2

    .line 245
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/ss/view/SheetView;->addExtendCell(Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;FFLjava/lang/Object;)V

    .line 246
    .line 247
    .line 248
    goto :goto_2

    .line 249
    :cond_5
    float-to-int p2, v4

    .line 250
    float-to-int v0, v5

    .line 251
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 252
    .line 253
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 254
    .line 255
    .line 256
    move-result v1

    .line 257
    invoke-virtual {v6, p1, p2, v0, v1}, Lcom/intsig/office/simpletext/view/STRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 258
    .line 259
    .line 260
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 261
    .line 262
    .line 263
    return-void

    .line 264
    nop

    .line 265
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private drawNumericCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 9

    .line 1
    invoke-virtual {p4}, Landroid/graphics/Paint;->getColor()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lez v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getNumberValue()D

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    const-wide/16 v3, 0x0

    .line 16
    .line 17
    cmpg-double v5, v1, v3

    .line 18
    .line 19
    if-gez v5, :cond_0

    .line 20
    .line 21
    invoke-static {p2}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getNegativeColor(Lcom/intsig/office/ss/model/baseModel/Cell;)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 26
    .line 27
    .line 28
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    const/16 v2, 0x8

    .line 33
    .line 34
    if-ne v1, v2, :cond_1

    .line 35
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->drawAccountCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellNumericType()S

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    const/4 v2, 0x6

    .line 45
    if-ne v1, v2, :cond_2

    .line 46
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/ss/view/CellView;->drawGeneralCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;Landroid/graphics/Paint;)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_2
    invoke-virtual {p4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iget v2, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 56
    .line 57
    iget v3, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 58
    .line 59
    sub-float/2addr v2, v3

    .line 60
    float-to-double v2, v2

    .line 61
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    .line 62
    .line 63
    .line 64
    move-result-wide v2

    .line 65
    double-to-int v2, v2

    .line 66
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    float-to-int v3, v3

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 72
    .line 73
    .line 74
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 75
    .line 76
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 77
    .line 78
    .line 79
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 84
    .line 85
    .line 86
    move-result v4

    .line 87
    const/4 v5, 0x2

    .line 88
    const/high16 v6, 0x40000000    # 2.0f

    .line 89
    .line 90
    if-eqz v4, :cond_5

    .line 91
    .line 92
    const/4 v7, 0x1

    .line 93
    if-eq v4, v7, :cond_4

    .line 94
    .line 95
    if-eq v4, v5, :cond_3

    .line 96
    .line 97
    const/4 v7, 0x3

    .line 98
    if-eq v4, v7, :cond_4

    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    iget v4, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 102
    .line 103
    iget-object v7, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 104
    .line 105
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 106
    .line 107
    .line 108
    move-result v7

    .line 109
    int-to-float v2, v2

    .line 110
    sub-float/2addr v7, v2

    .line 111
    add-float/2addr v4, v7

    .line 112
    iput v4, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_4
    iget v4, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 116
    .line 117
    iget-object v7, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 118
    .line 119
    invoke-virtual {v7}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 120
    .line 121
    .line 122
    move-result v7

    .line 123
    int-to-float v2, v2

    .line 124
    sub-float/2addr v7, v2

    .line 125
    div-float/2addr v7, v6

    .line 126
    add-float/2addr v4, v7

    .line 127
    iput v4, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_5
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 131
    .line 132
    add-float/2addr v2, v6

    .line 133
    iput v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 134
    .line 135
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 136
    .line 137
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    invoke-virtual {v2, v4}, Lcom/intsig/office/ss/view/SheetView;->getIndentWidthWithZoom(I)I

    .line 146
    .line 147
    .line 148
    move-result v2

    .line 149
    add-int v4, v3, v2

    .line 150
    .line 151
    const/4 v7, 0x4

    .line 152
    add-int/2addr v4, v7

    .line 153
    int-to-float v4, v4

    .line 154
    iget-object v8, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 155
    .line 156
    invoke-virtual {v8}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 157
    .line 158
    .line 159
    move-result v8

    .line 160
    cmpl-float v4, v4, v8

    .line 161
    .line 162
    if-lez v4, :cond_7

    .line 163
    .line 164
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 165
    .line 166
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 167
    .line 168
    .line 169
    move-result p2

    .line 170
    const/high16 p3, 0x40800000    # 4.0f

    .line 171
    .line 172
    sub-float/2addr p2, p3

    .line 173
    const-string p3, "#"

    .line 174
    .line 175
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 176
    .line 177
    .line 178
    move-result p3

    .line 179
    div-float/2addr p2, p3

    .line 180
    float-to-int p2, p2

    .line 181
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 182
    .line 183
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    .line 184
    .line 185
    .line 186
    move-result v2

    .line 187
    const/4 v3, 0x0

    .line 188
    invoke-virtual {p3, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    :goto_1
    if-ge v3, p2, :cond_6

    .line 192
    .line 193
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 194
    .line 195
    const/16 v2, 0x23

    .line 196
    .line 197
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    add-int/lit8 v3, v3, 0x1

    .line 201
    .line 202
    goto :goto_1

    .line 203
    :cond_6
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object p3

    .line 209
    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 210
    .line 211
    .line 212
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 213
    .line 214
    add-float/2addr p2, v6

    .line 215
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 216
    .line 217
    goto :goto_3

    .line 218
    :cond_7
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 219
    .line 220
    .line 221
    move-result-object v4

    .line 222
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    .line 223
    .line 224
    .line 225
    move-result v4

    .line 226
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 227
    .line 228
    .line 229
    move-result p2

    .line 230
    if-ne p2, v7, :cond_8

    .line 231
    .line 232
    if-nez v4, :cond_8

    .line 233
    .line 234
    goto :goto_2

    .line 235
    :cond_8
    move v5, v4

    .line 236
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 237
    .line 238
    .line 239
    goto :goto_3

    .line 240
    :pswitch_0
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 241
    .line 242
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 243
    .line 244
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 245
    .line 246
    .line 247
    move-result v2

    .line 248
    int-to-float v3, v3

    .line 249
    sub-float/2addr v2, v3

    .line 250
    div-float/2addr v2, v6

    .line 251
    add-float/2addr p2, v2

    .line 252
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 253
    .line 254
    goto :goto_3

    .line 255
    :pswitch_1
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 256
    .line 257
    add-int/lit8 v3, v2, 0x2

    .line 258
    .line 259
    int-to-float v3, v3

    .line 260
    add-float/2addr p2, v3

    .line 261
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 262
    .line 263
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 264
    .line 265
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 266
    .line 267
    .line 268
    move-result v3

    .line 269
    int-to-float v2, v2

    .line 270
    sub-float/2addr v3, v2

    .line 271
    invoke-virtual {p2, v3}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 272
    .line 273
    .line 274
    goto :goto_3

    .line 275
    :pswitch_2
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 276
    .line 277
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 278
    .line 279
    .line 280
    move-result v4

    .line 281
    int-to-float v2, v2

    .line 282
    sub-float/2addr v4, v2

    .line 283
    invoke-virtual {p2, v4}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 284
    .line 285
    .line 286
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 287
    .line 288
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 289
    .line 290
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 291
    .line 292
    .line 293
    move-result v2

    .line 294
    int-to-float v3, v3

    .line 295
    sub-float/2addr v2, v3

    .line 296
    sub-float/2addr v2, v6

    .line 297
    add-float/2addr p2, v2

    .line 298
    iput p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 299
    .line 300
    :goto_3
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 301
    .line 302
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 303
    .line 304
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 305
    .line 306
    .line 307
    move-result v2

    .line 308
    sub-float/2addr p2, v2

    .line 309
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 310
    .line 311
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 312
    .line 313
    .line 314
    move-result v2

    .line 315
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 316
    .line 317
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 318
    .line 319
    .line 320
    move-result v3

    .line 321
    sub-float/2addr v2, v3

    .line 322
    sub-float/2addr p2, v2

    .line 323
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 324
    .line 325
    iget v3, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 326
    .line 327
    sub-float/2addr v2, v3

    .line 328
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 329
    .line 330
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 331
    .line 332
    .line 333
    move-result v3

    .line 334
    sub-float/2addr v2, v3

    .line 335
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 336
    .line 337
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 338
    .line 339
    .line 340
    move-result v3

    .line 341
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 342
    .line 343
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 344
    .line 345
    .line 346
    move-result v4

    .line 347
    sub-float/2addr v3, v4

    .line 348
    sub-float/2addr v2, v3

    .line 349
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 350
    .line 351
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->isFrozenColumn()Z

    .line 352
    .line 353
    .line 354
    move-result v3

    .line 355
    if-eqz v3, :cond_9

    .line 356
    .line 357
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 358
    .line 359
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 360
    .line 361
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 362
    .line 363
    .line 364
    move-result v3

    .line 365
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 366
    .line 367
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 368
    .line 369
    .line 370
    move-result v4

    .line 371
    sub-float/2addr v3, v4

    .line 372
    sub-float/2addr p2, v3

    .line 373
    :cond_9
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 374
    .line 375
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->isFrozenRow()Z

    .line 376
    .line 377
    .line 378
    move-result v3

    .line 379
    if-eqz v3, :cond_a

    .line 380
    .line 381
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 382
    .line 383
    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 384
    .line 385
    sub-float/2addr v2, v1

    .line 386
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 387
    .line 388
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 389
    .line 390
    .line 391
    move-result v1

    .line 392
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 393
    .line 394
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 395
    .line 396
    .line 397
    move-result v3

    .line 398
    sub-float/2addr v1, v3

    .line 399
    sub-float/2addr v2, v1

    .line 400
    :cond_a
    invoke-virtual {p1, p3, p2, v2, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 401
    .line 402
    .line 403
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 404
    .line 405
    .line 406
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 407
    .line 408
    .line 409
    return-void

    .line 410
    nop

    .line 411
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

.method private drawWrapComplexTextCell(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSTRoot()Lcom/intsig/office/simpletext/view/STRoot;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_2

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;II)Landroid/graphics/Rect;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    check-cast v1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 54
    .line 55
    if-eqz v1, :cond_1

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 58
    .line 59
    .line 60
    move-result-wide v2

    .line 61
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 62
    .line 63
    .line 64
    move-result-wide v4

    .line 65
    sub-long/2addr v2, v4

    .line 66
    const-wide/16 v4, 0x0

    .line 67
    .line 68
    cmp-long v6, v2, v4

    .line 69
    .line 70
    if-nez v6, :cond_0

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v4, v4

    .line 86
    const/high16 v5, 0x41700000    # 15.0f

    .line 87
    .line 88
    mul-float v4, v4, v5

    .line 89
    .line 90
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 95
    .line 96
    .line 97
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    int-to-float v0, v0

    .line 106
    mul-float v0, v0, v5

    .line 107
    .line 108
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    invoke-virtual {v3, v2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 113
    .line 114
    .line 115
    new-instance v0, Lcom/intsig/office/simpletext/model/STDocument;

    .line 116
    .line 117
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/STDocument;-><init>()V

    .line 118
    .line 119
    .line 120
    invoke-interface {v0, v1}, Lcom/intsig/office/simpletext/model/IDocument;->appendSection(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 121
    .line 122
    .line 123
    new-instance v1, Lcom/intsig/office/simpletext/view/STRoot;

    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 126
    .line 127
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->getEditor()Lcom/intsig/office/simpletext/control/IWord;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/simpletext/view/STRoot;-><init>(Lcom/intsig/office/simpletext/control/IWord;Lcom/intsig/office/simpletext/model/IDocument;)V

    .line 136
    .line 137
    .line 138
    const/4 v0, 0x1

    .line 139
    invoke-virtual {v1, v0}, Lcom/intsig/office/simpletext/view/STRoot;->setWrapLine(Z)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/STRoot;->doLayout()V

    .line 143
    .line 144
    .line 145
    invoke-virtual {p2, v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)V

    .line 146
    .line 147
    .line 148
    move-object v0, v1

    .line 149
    goto :goto_1

    .line 150
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 151
    .line 152
    .line 153
    return-void

    .line 154
    :cond_2
    :goto_1
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 155
    .line 156
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 157
    .line 158
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 159
    .line 160
    .line 161
    move-result v1

    .line 162
    sub-float/2addr p2, v1

    .line 163
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 164
    .line 165
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 166
    .line 167
    .line 168
    move-result v1

    .line 169
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 170
    .line 171
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 172
    .line 173
    .line 174
    move-result v2

    .line 175
    sub-float/2addr v1, v2

    .line 176
    sub-float/2addr p2, v1

    .line 177
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 178
    .line 179
    .line 180
    move-result p2

    .line 181
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 182
    .line 183
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 184
    .line 185
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    sub-float/2addr v1, v2

    .line 190
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 191
    .line 192
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 193
    .line 194
    .line 195
    move-result v2

    .line 196
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 197
    .line 198
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 199
    .line 200
    .line 201
    move-result v3

    .line 202
    sub-float/2addr v2, v3

    .line 203
    sub-float/2addr v1, v2

    .line 204
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 205
    .line 206
    .line 207
    move-result v1

    .line 208
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 209
    .line 210
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 211
    .line 212
    .line 213
    move-result v2

    .line 214
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/intsig/office/simpletext/view/STRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 218
    .line 219
    .line 220
    return-void
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private drawWrapText(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSTRoot()Lcom/intsig/office/simpletext/view/STRoot;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;II)Landroid/graphics/Rect;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-direct {p0, p2, p3, v1, v0}, Lcom/intsig/office/ss/view/CellView;->convertToSectionElement(Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;II)Lcom/intsig/office/simpletext/model/SectionElement;

    .line 38
    .line 39
    .line 40
    move-result-object p3

    .line 41
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 42
    .line 43
    .line 44
    move-result-wide v0

    .line 45
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 46
    .line 47
    .line 48
    move-result-wide v2

    .line 49
    sub-long/2addr v0, v2

    .line 50
    const-wide/16 v2, 0x0

    .line 51
    .line 52
    cmp-long v4, v0, v2

    .line 53
    .line 54
    if-nez v4, :cond_0

    .line 55
    .line 56
    return-void

    .line 57
    :cond_0
    new-instance v0, Lcom/intsig/office/simpletext/model/STDocument;

    .line 58
    .line 59
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/STDocument;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-interface {v0, p3}, Lcom/intsig/office/simpletext/model/IDocument;->appendSection(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 63
    .line 64
    .line 65
    new-instance p3, Lcom/intsig/office/simpletext/view/STRoot;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 68
    .line 69
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/intsig/office/ss/control/Spreadsheet;->getEditor()Lcom/intsig/office/simpletext/control/IWord;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-direct {p3, v1, v0}, Lcom/intsig/office/simpletext/view/STRoot;-><init>(Lcom/intsig/office/simpletext/control/IWord;Lcom/intsig/office/simpletext/model/IDocument;)V

    .line 78
    .line 79
    .line 80
    const/4 v0, 0x1

    .line 81
    invoke-virtual {p3, v0}, Lcom/intsig/office/simpletext/view/STRoot;->setWrapLine(Z)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/view/STRoot;->doLayout()V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p2, p3}, Lcom/intsig/office/ss/model/baseModel/Cell;->setSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)V

    .line 88
    .line 89
    .line 90
    move-object v0, p3

    .line 91
    :cond_1
    iget p2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 92
    .line 93
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 94
    .line 95
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 96
    .line 97
    .line 98
    move-result p3

    .line 99
    sub-float/2addr p2, p3

    .line 100
    iget-object p3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 101
    .line 102
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 103
    .line 104
    .line 105
    move-result p3

    .line 106
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 107
    .line 108
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    sub-float/2addr p3, v1

    .line 113
    sub-float/2addr p2, p3

    .line 114
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 115
    .line 116
    .line 117
    move-result p2

    .line 118
    iget p3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 119
    .line 120
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 121
    .line 122
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    sub-float/2addr p3, v1

    .line 127
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 128
    .line 129
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 134
    .line 135
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 136
    .line 137
    .line 138
    move-result v2

    .line 139
    sub-float/2addr v1, v2

    .line 140
    sub-float/2addr p3, v1

    .line 141
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 142
    .line 143
    .line 144
    move-result p3

    .line 145
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 146
    .line 147
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/intsig/office/simpletext/view/STRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 152
    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getNonScientificGeneralContents(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;
    .locals 13

    .line 1
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    float-to-int v0, v0

    .line 6
    add-int/lit8 v0, v0, 0x4

    .line 7
    .line 8
    int-to-float v0, v0

    .line 9
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    cmpl-float v0, v0, v1

    .line 16
    .line 17
    if-lez v0, :cond_13

    .line 18
    .line 19
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const-string v1, ""

    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    if-ne v0, v2, :cond_0

    .line 27
    .line 28
    return-object v1

    .line 29
    :cond_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 30
    .line 31
    .line 32
    move-result-wide v3

    .line 33
    double-to-int v0, v3

    .line 34
    const/16 v5, 0x35

    .line 35
    .line 36
    const/16 v6, 0x39

    .line 37
    .line 38
    const/16 v7, 0x2e

    .line 39
    .line 40
    const/4 v8, 0x0

    .line 41
    if-eqz v0, :cond_5

    .line 42
    .line 43
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    float-to-int v0, v0

    .line 52
    add-int/lit8 v0, v0, 0x4

    .line 53
    .line 54
    int-to-float v0, v0

    .line 55
    iget-object v9, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 56
    .line 57
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 58
    .line 59
    .line 60
    move-result v9

    .line 61
    cmpl-float v0, v0, v9

    .line 62
    .line 63
    if-lez v0, :cond_1

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_1
    const/4 v0, 0x0

    .line 67
    :goto_0
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    float-to-int v1, v1

    .line 72
    add-int/lit8 v1, v1, 0x4

    .line 73
    .line 74
    int-to-float v1, v1

    .line 75
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 76
    .line 77
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    cmpl-float v1, v1, v3

    .line 82
    .line 83
    if-lez v1, :cond_2

    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    sub-int/2addr v0, v2

    .line 90
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    sub-int/2addr v1, v2

    .line 99
    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    goto :goto_0

    .line 104
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 105
    .line 106
    .line 107
    move-result p2

    .line 108
    sub-int/2addr p2, v2

    .line 109
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    .line 110
    .line 111
    .line 112
    move-result p2

    .line 113
    if-ne p2, v7, :cond_3

    .line 114
    .line 115
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 116
    .line 117
    .line 118
    move-result p2

    .line 119
    sub-int/2addr p2, v2

    .line 120
    invoke-virtual {p1, v8, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    goto :goto_1

    .line 125
    :cond_3
    if-gt v0, v6, :cond_4

    .line 126
    .line 127
    if-lt v0, v5, :cond_4

    .line 128
    .line 129
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/CellView;->ceilNumeric(Ljava/lang/String;)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    :cond_4
    :goto_1
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/CellView;->trimInvalidateZero(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    goto/16 :goto_a

    .line 138
    .line 139
    :cond_5
    :goto_2
    const/4 p1, 0x0

    .line 140
    :goto_3
    const/16 v0, 0xa

    .line 141
    .line 142
    int-to-double v9, v0

    .line 143
    div-double v9, v3, v9

    .line 144
    .line 145
    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    .line 146
    .line 147
    .line 148
    move-result-wide v11

    .line 149
    double-to-int v11, v11

    .line 150
    if-lez v11, :cond_6

    .line 151
    .line 152
    add-int/lit8 p1, p1, 0x1

    .line 153
    .line 154
    move-wide v3, v9

    .line 155
    goto :goto_3

    .line 156
    :cond_6
    if-lez p1, :cond_8

    .line 157
    .line 158
    const-string v9, "E+"

    .line 159
    .line 160
    if-ge p1, v0, :cond_7

    .line 161
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    const-string v9, "0"

    .line 171
    .line 172
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object p1

    .line 186
    goto :goto_4

    .line 187
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    :goto_4
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    goto :goto_6

    .line 211
    :cond_8
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    const/16 v0, 0x45

    .line 216
    .line 217
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 218
    .line 219
    .line 220
    move-result v0

    .line 221
    if-lez v0, :cond_9

    .line 222
    .line 223
    invoke-virtual {p1, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v3

    .line 227
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object p1

    .line 231
    move-object v0, v3

    .line 232
    goto :goto_6

    .line 233
    :cond_9
    const/4 p1, 0x0

    .line 234
    :goto_5
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    .line 235
    .line 236
    .line 237
    move-result-wide v9

    .line 238
    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    .line 239
    .line 240
    cmpg-double v0, v9, v11

    .line 241
    .line 242
    if-gez v0, :cond_a

    .line 243
    .line 244
    const-wide v9, 0x41dfffffffc00000L    # 2.147483647E9

    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    mul-double v9, v9, v3

    .line 250
    .line 251
    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    .line 252
    .line 253
    .line 254
    move-result-wide v9

    .line 255
    const-wide/16 v11, 0x0

    .line 256
    .line 257
    cmpl-double v0, v9, v11

    .line 258
    .line 259
    if-lez v0, :cond_a

    .line 260
    .line 261
    const-wide/high16 v9, 0x4024000000000000L    # 10.0

    .line 262
    .line 263
    mul-double v3, v3, v9

    .line 264
    .line 265
    add-int/lit8 p1, p1, 0x1

    .line 266
    .line 267
    goto :goto_5

    .line 268
    :cond_a
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v0

    .line 272
    const-string v3, "E-"

    .line 273
    .line 274
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 275
    .line 276
    .line 277
    move-result-object p1

    .line 278
    invoke-virtual {v3, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object p1

    .line 282
    :goto_6
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 283
    .line 284
    .line 285
    move-result v3

    .line 286
    const/high16 v4, 0x40800000    # 4.0f

    .line 287
    .line 288
    add-float/2addr v3, v4

    .line 289
    iget-object v9, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 290
    .line 291
    invoke-virtual {v9}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 292
    .line 293
    .line 294
    move-result v9

    .line 295
    cmpl-float v3, v3, v9

    .line 296
    .line 297
    if-ltz v3, :cond_c

    .line 298
    .line 299
    const/4 v3, 0x0

    .line 300
    :cond_b
    const/4 v9, 0x1

    .line 301
    goto :goto_8

    .line 302
    :cond_c
    const/4 v3, 0x0

    .line 303
    :goto_7
    new-instance v9, Ljava/lang/StringBuilder;

    .line 304
    .line 305
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    .line 307
    .line 308
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v9

    .line 318
    invoke-virtual {p2, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 319
    .line 320
    .line 321
    move-result v9

    .line 322
    float-to-int v9, v9

    .line 323
    add-int/lit8 v9, v9, 0x4

    .line 324
    .line 325
    int-to-float v9, v9

    .line 326
    iget-object v10, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 327
    .line 328
    invoke-virtual {v10}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 329
    .line 330
    .line 331
    move-result v10

    .line 332
    cmpl-float v9, v9, v10

    .line 333
    .line 334
    if-lez v9, :cond_d

    .line 335
    .line 336
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 337
    .line 338
    .line 339
    move-result v9

    .line 340
    if-lt v9, v2, :cond_b

    .line 341
    .line 342
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 343
    .line 344
    .line 345
    move-result v3

    .line 346
    sub-int/2addr v3, v2

    .line 347
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    .line 348
    .line 349
    .line 350
    move-result v3

    .line 351
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 352
    .line 353
    .line 354
    move-result v9

    .line 355
    sub-int/2addr v9, v2

    .line 356
    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 357
    .line 358
    .line 359
    move-result-object v0

    .line 360
    goto :goto_7

    .line 361
    :cond_d
    const/4 v9, 0x0

    .line 362
    :goto_8
    if-nez v9, :cond_11

    .line 363
    .line 364
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 365
    .line 366
    .line 367
    move-result v9

    .line 368
    if-eqz v9, :cond_11

    .line 369
    .line 370
    const-string v9, "-"

    .line 371
    .line 372
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 373
    .line 374
    .line 375
    move-result v9

    .line 376
    if-eqz v9, :cond_e

    .line 377
    .line 378
    goto :goto_9

    .line 379
    :cond_e
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 380
    .line 381
    .line 382
    move-result p2

    .line 383
    sub-int/2addr p2, v2

    .line 384
    invoke-virtual {v0, p2}, Ljava/lang/String;->charAt(I)C

    .line 385
    .line 386
    .line 387
    move-result p2

    .line 388
    if-ne p2, v7, :cond_f

    .line 389
    .line 390
    new-instance p2, Ljava/lang/StringBuilder;

    .line 391
    .line 392
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    .line 394
    .line 395
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 396
    .line 397
    .line 398
    move-result v1

    .line 399
    sub-int/2addr v1, v2

    .line 400
    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 401
    .line 402
    .line 403
    move-result-object v0

    .line 404
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    .line 406
    .line 407
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    .line 409
    .line 410
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 411
    .line 412
    .line 413
    move-result-object p1

    .line 414
    goto :goto_a

    .line 415
    :cond_f
    if-gt v3, v6, :cond_10

    .line 416
    .line 417
    if-lt v3, v5, :cond_10

    .line 418
    .line 419
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/view/CellView;->ceilNumeric(Ljava/lang/String;)Ljava/lang/String;

    .line 420
    .line 421
    .line 422
    move-result-object v0

    .line 423
    :cond_10
    new-instance p2, Ljava/lang/StringBuilder;

    .line 424
    .line 425
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 426
    .line 427
    .line 428
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    .line 433
    .line 434
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 435
    .line 436
    .line 437
    move-result-object p1

    .line 438
    goto :goto_a

    .line 439
    :cond_11
    :goto_9
    new-instance p1, Ljava/lang/StringBuilder;

    .line 440
    .line 441
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 442
    .line 443
    .line 444
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    const-string v0, "#"

    .line 448
    .line 449
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    .line 451
    .line 452
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 453
    .line 454
    .line 455
    move-result-object p1

    .line 456
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 457
    .line 458
    .line 459
    move-result p1

    .line 460
    add-float/2addr p1, v4

    .line 461
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 462
    .line 463
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 464
    .line 465
    .line 466
    move-result v2

    .line 467
    cmpg-float p1, p1, v2

    .line 468
    .line 469
    if-gez p1, :cond_12

    .line 470
    .line 471
    new-instance p1, Ljava/lang/StringBuilder;

    .line 472
    .line 473
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 474
    .line 475
    .line 476
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    .line 478
    .line 479
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    .line 481
    .line 482
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 483
    .line 484
    .line 485
    move-result-object v1

    .line 486
    goto :goto_9

    .line 487
    :cond_12
    move-object p1, v1

    .line 488
    :cond_13
    :goto_a
    return-object p1
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getRotatedX(III)I
    .locals 4

    .line 1
    int-to-float p3, p3

    .line 2
    const/high16 v0, 0x43340000    # 180.0f

    .line 3
    .line 4
    div-float/2addr p3, v0

    .line 5
    float-to-double v0, p3

    .line 6
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    mul-double v0, v0, v2

    .line 12
    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    int-to-double p2, p2

    .line 18
    mul-double v2, v2, p2

    .line 19
    .line 20
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    .line 21
    .line 22
    .line 23
    move-result-wide p2

    .line 24
    int-to-double v0, p1

    .line 25
    mul-double p2, p2, v0

    .line 26
    .line 27
    add-double/2addr v2, p2

    .line 28
    double-to-int p1, v2

    .line 29
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getRotatedY(III)I
    .locals 4

    .line 1
    int-to-float p3, p3

    .line 2
    const/high16 v0, 0x43340000    # 180.0f

    .line 3
    .line 4
    div-float/2addr p3, v0

    .line 5
    float-to-double v0, p3

    .line 6
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    mul-double v0, v0, v2

    .line 12
    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    int-to-double p2, p2

    .line 18
    mul-double v2, v2, p2

    .line 19
    .line 20
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    .line 21
    .line 22
    .line 23
    move-result-wide p2

    .line 24
    int-to-double v0, p1

    .line 25
    mul-double p2, p2, v0

    .line 26
    .line 27
    sub-double/2addr v2, p2

    .line 28
    double-to-int p1, v2

    .line 29
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getScientificGeneralContents(Ljava/lang/String;Landroid/graphics/Paint;)Ljava/lang/String;
    .locals 7

    .line 1
    const/16 v0, 0x45

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    const/4 v3, 0x1

    .line 13
    add-int/2addr v0, v3

    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-lez v0, :cond_1

    .line 23
    .line 24
    const/16 v4, 0xa

    .line 25
    .line 26
    if-ge v0, v4, :cond_0

    .line 27
    .line 28
    const-string v0, "E+0"

    .line 29
    .line 30
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const-string v0, "E+"

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const/16 v4, -0xa

    .line 43
    .line 44
    if-le v0, v4, :cond_2

    .line 45
    .line 46
    neg-int p1, v0

    .line 47
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    const-string v0, "E-0"

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    goto :goto_0

    .line 58
    :cond_2
    const-string v0, "E"

    .line 59
    .line 60
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    :goto_0
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    const/high16 v4, 0x40800000    # 4.0f

    .line 69
    .line 70
    add-float/2addr v0, v4

    .line 71
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 72
    .line 73
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    cmpl-float v0, v0, v5

    .line 78
    .line 79
    if-ltz v0, :cond_4

    .line 80
    .line 81
    const/4 v0, 0x0

    .line 82
    :cond_3
    const/4 v5, 0x1

    .line 83
    goto :goto_2

    .line 84
    :cond_4
    const/4 v0, 0x0

    .line 85
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v5

    .line 100
    invoke-virtual {p2, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    float-to-int v5, v5

    .line 105
    add-int/lit8 v5, v5, 0x4

    .line 106
    .line 107
    int-to-float v5, v5

    .line 108
    iget-object v6, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 109
    .line 110
    invoke-virtual {v6}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    cmpl-float v5, v5, v6

    .line 115
    .line 116
    if-lez v5, :cond_5

    .line 117
    .line 118
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    if-lt v5, v3, :cond_3

    .line 123
    .line 124
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    sub-int/2addr v0, v3

    .line 129
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    sub-int/2addr v5, v3

    .line 138
    invoke-virtual {v2, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    goto :goto_1

    .line 143
    :cond_5
    const/4 v5, 0x0

    .line 144
    :goto_2
    if-nez v5, :cond_9

    .line 145
    .line 146
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 147
    .line 148
    .line 149
    move-result v5

    .line 150
    if-eqz v5, :cond_9

    .line 151
    .line 152
    const-string v5, "-"

    .line 153
    .line 154
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 155
    .line 156
    .line 157
    move-result v5

    .line 158
    if-eqz v5, :cond_6

    .line 159
    .line 160
    goto :goto_3

    .line 161
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 162
    .line 163
    .line 164
    move-result p2

    .line 165
    sub-int/2addr p2, v3

    .line 166
    invoke-virtual {v2, p2}, Ljava/lang/String;->charAt(I)C

    .line 167
    .line 168
    .line 169
    move-result p2

    .line 170
    const/16 v4, 0x2e

    .line 171
    .line 172
    if-ne p2, v4, :cond_7

    .line 173
    .line 174
    new-instance p2, Ljava/lang/StringBuilder;

    .line 175
    .line 176
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .line 178
    .line 179
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 180
    .line 181
    .line 182
    move-result v0

    .line 183
    sub-int/2addr v0, v3

    .line 184
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object p1

    .line 198
    goto :goto_5

    .line 199
    :cond_7
    const/16 p2, 0x39

    .line 200
    .line 201
    if-gt v0, p2, :cond_8

    .line 202
    .line 203
    const/16 p2, 0x35

    .line 204
    .line 205
    if-lt v0, p2, :cond_8

    .line 206
    .line 207
    invoke-direct {p0, v2}, Lcom/intsig/office/ss/view/CellView;->ceilNumeric(Ljava/lang/String;)Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v2

    .line 211
    :cond_8
    new-instance p2, Ljava/lang/StringBuilder;

    .line 212
    .line 213
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 214
    .line 215
    .line 216
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    goto :goto_5

    .line 227
    :cond_9
    :goto_3
    const-string p1, ""

    .line 228
    .line 229
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 230
    .line 231
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .line 233
    .line 234
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    const-string v1, "#"

    .line 238
    .line 239
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    .line 241
    .line 242
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v0

    .line 246
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 247
    .line 248
    .line 249
    move-result v0

    .line 250
    add-float/2addr v0, v4

    .line 251
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 252
    .line 253
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 254
    .line 255
    .line 256
    move-result v2

    .line 257
    cmpg-float v0, v0, v2

    .line 258
    .line 259
    if-gez v0, :cond_a

    .line 260
    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    .line 262
    .line 263
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    .line 265
    .line 266
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    .line 268
    .line 269
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object p1

    .line 276
    goto :goto_4

    .line 277
    :cond_a
    :goto_5
    return-object p1
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private initPageProp(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/ss/model/style/CellStyle;II)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    if-eq p2, v1, :cond_2

    .line 10
    .line 11
    const/4 v2, 0x2

    .line 12
    if-eq p2, v2, :cond_1

    .line 13
    .line 14
    const/4 v2, 0x3

    .line 15
    if-eq p2, v2, :cond_2

    .line 16
    .line 17
    :cond_0
    const/4 v1, 0x0

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v1, 0x2

    .line 20
    :cond_2
    :goto_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    invoke-virtual {p2, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    int-to-float p3, p3

    .line 32
    const/high16 v1, 0x41700000    # 15.0f

    .line 33
    .line 34
    mul-float p3, p3, v1

    .line 35
    .line 36
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 37
    .line 38
    .line 39
    move-result p3

    .line 40
    invoke-virtual {p2, p1, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 41
    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    int-to-float p3, p4

    .line 48
    mul-float p3, p3, v1

    .line 49
    .line 50
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 51
    .line 52
    .line 53
    move-result p3

    .line 54
    invoke-virtual {p2, p1, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 55
    .line 56
    .line 57
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    const/high16 p3, 0x41f00000    # 30.0f

    .line 62
    .line 63
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 64
    .line 65
    .line 66
    move-result p4

    .line 67
    invoke-virtual {p2, p1, p4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 68
    .line 69
    .line 70
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 75
    .line 76
    .line 77
    move-result p3

    .line 78
    invoke-virtual {p2, p1, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 79
    .line 80
    .line 81
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 86
    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static isComplexText(Lcom/intsig/office/ss/model/baseModel/Cell;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-ltz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    invoke-virtual {v0, p0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    instance-of p0, p0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 31
    .line 32
    return p0

    .line 33
    :cond_0
    const/4 p0, 0x0

    .line 34
    return p0
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isDouble(Ljava/lang/String;)Z
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1

    .line 6
    :catch_0
    const/4 p1, 0x0

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isInteger(Ljava/lang/String;)Z
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1

    .line 6
    :catch_0
    const/4 p1, 0x0

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isNumeric(Ljava/lang/String;)Z
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/CellView;->isInteger(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/view/CellView;->isDouble(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 17
    :goto_1
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;)I
    .locals 9

    .line 1
    const-string v0, "\n"

    .line 2
    .line 3
    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p3

    .line 7
    array-length v0, p3

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v6, 0x0

    .line 10
    :goto_0
    if-ge v1, v0, :cond_0

    .line 11
    .line 12
    aget-object v5, p3, v1

    .line 13
    .line 14
    new-instance v8, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 15
    .line 16
    invoke-direct {v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 17
    .line 18
    .line 19
    int-to-long v2, v6

    .line 20
    invoke-virtual {v8, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 21
    .line 22
    .line 23
    new-instance v7, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 24
    .line 25
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v2, v3, v4, v7}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 41
    .line 42
    .line 43
    move-object v2, p0

    .line 44
    move-object v3, v8

    .line 45
    move-object v4, p2

    .line 46
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/ss/view/CellView;->processRun(Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;ILcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    int-to-long v2, v6

    .line 51
    invoke-virtual {v8, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 52
    .line 53
    .line 54
    const-wide/16 v2, 0x0

    .line 55
    .line 56
    invoke-virtual {p1, v8, v2, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 57
    .line 58
    .line 59
    add-int/lit8 v1, v1, 0x1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    return v6
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processRun(Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/ss/model/baseModel/Cell;Ljava/lang/String;ILcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 5

    .line 1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance p3, Lcom/intsig/office/simpletext/model/CellLeafElement;

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-direct {p3, p2, v0, v0}, Lcom/intsig/office/simpletext/model/CellLeafElement;-><init>(Lcom/intsig/office/ss/model/baseModel/Cell;II)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/model/CellLeafElement;->appendNewlineFlag()V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v0, v1, p2, v2, p5}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 31
    .line 32
    .line 33
    int-to-long v0, p4

    .line 34
    invoke-virtual {p3, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 35
    .line 36
    .line 37
    add-int/lit8 p4, p4, 0x1

    .line 38
    .line 39
    int-to-long v0, p4

    .line 40
    invoke-virtual {p3, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 44
    .line 45
    .line 46
    return p4

    .line 47
    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    const/4 v1, 0x0

    .line 52
    if-lez v0, :cond_2

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedString(I)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    if-nez v2, :cond_1

    .line 71
    .line 72
    new-instance v2, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 73
    .line 74
    invoke-direct {v2, p3}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    new-instance v3, Lcom/intsig/office/simpletext/model/CellLeafElement;

    .line 83
    .line 84
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 85
    .line 86
    .line 87
    move-result p3

    .line 88
    add-int/2addr p3, v2

    .line 89
    invoke-direct {v3, p2, v2, p3}, Lcom/intsig/office/simpletext/model/CellLeafElement;-><init>(Lcom/intsig/office/ss/model/baseModel/Cell;II)V

    .line 90
    .line 91
    .line 92
    move-object v2, v3

    .line 93
    :goto_0
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 94
    .line 95
    .line 96
    move-result-object p3

    .line 97
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 98
    .line 99
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 104
    .line 105
    .line 106
    move-result-object v4

    .line 107
    invoke-virtual {p3, v3, p2, v4, p5}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 108
    .line 109
    .line 110
    int-to-long p2, p4

    .line 111
    invoke-virtual {v2, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 112
    .line 113
    .line 114
    add-int/2addr p4, v0

    .line 115
    int-to-long p2, p4

    .line 116
    invoke-virtual {v2, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1, v2}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 120
    .line 121
    .line 122
    goto :goto_1

    .line 123
    :cond_2
    move-object v2, v1

    .line 124
    :goto_1
    if-eqz v2, :cond_4

    .line 125
    .line 126
    instance-of p1, v2, Lcom/intsig/office/simpletext/model/CellLeafElement;

    .line 127
    .line 128
    if-eqz p1, :cond_3

    .line 129
    .line 130
    move-object p1, v2

    .line 131
    check-cast p1, Lcom/intsig/office/simpletext/model/CellLeafElement;

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/CellLeafElement;->appendNewlineFlag()V

    .line 134
    .line 135
    .line 136
    add-int/lit8 p4, p4, 0x1

    .line 137
    .line 138
    int-to-long p1, p4

    .line 139
    invoke-virtual {v2, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 140
    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    .line 144
    .line 145
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v2, v1}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object p2

    .line 152
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    const-string p2, "\n"

    .line 156
    .line 157
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    invoke-virtual {v2, p1}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    :cond_4
    :goto_2
    return p4
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private redrawMergedCellBordersAndBackground(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/CellRangeAddress;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 9
    .line 10
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    add-float/2addr v2, v0

    .line 15
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    sub-float/2addr v2, v3

    .line 22
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 29
    .line 30
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    sub-float/2addr v3, v4

    .line 35
    sub-float/2addr v2, v3

    .line 36
    iget v3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 37
    .line 38
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 39
    .line 40
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    add-float/2addr v3, v4

    .line 45
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 46
    .line 47
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    sub-float/2addr v3, v4

    .line 52
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 53
    .line 54
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 59
    .line 60
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    sub-float/2addr v4, v5

    .line 65
    sub-float/2addr v3, v4

    .line 66
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 67
    .line 68
    .line 69
    const/4 v0, -0x1

    .line 70
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 71
    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 82
    .line 83
    .line 84
    move-result v7

    .line 85
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    .line 86
    .line 87
    .line 88
    move-result-object v8

    .line 89
    const v9, -0x382e27

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    .line 94
    .line 95
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 98
    .line 99
    .line 100
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 101
    .line 102
    iget v3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 105
    .line 106
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    add-float/2addr v1, v2

    .line 111
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 112
    .line 113
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    sub-float v4, v1, v4

    .line 118
    .line 119
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 120
    .line 121
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 122
    .line 123
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 124
    .line 125
    .line 126
    move-result v5

    .line 127
    add-float/2addr v1, v5

    .line 128
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 129
    .line 130
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 131
    .line 132
    .line 133
    move-result v5

    .line 134
    sub-float v5, v1, v5

    .line 135
    .line 136
    move-object v1, p1

    .line 137
    move-object v6, v0

    .line 138
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 142
    .line 143
    .line 144
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 145
    .line 146
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/SheetScroller;->getMinRowIndex()I

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 155
    .line 156
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getMinRowAndColumnInformation()Lcom/intsig/office/ss/other/SheetScroller;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    invoke-virtual {v2}, Lcom/intsig/office/ss/other/SheetScroller;->getMinColumnIndex()I

    .line 161
    .line 162
    .line 163
    move-result v2

    .line 164
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 165
    .line 166
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->isFrozenRow()Z

    .line 167
    .line 168
    .line 169
    move-result v3

    .line 170
    const/4 v4, 0x0

    .line 171
    if-eqz v3, :cond_0

    .line 172
    .line 173
    const/4 v1, 0x0

    .line 174
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 175
    .line 176
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->isFrozenColumn()Z

    .line 177
    .line 178
    .line 179
    move-result v3

    .line 180
    if-eqz v3, :cond_1

    .line 181
    .line 182
    const/4 v8, 0x0

    .line 183
    goto :goto_0

    .line 184
    :cond_1
    move v8, v2

    .line 185
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    const/high16 v10, 0x3f800000    # 1.0f

    .line 190
    .line 191
    if-lt v1, v2, :cond_2

    .line 192
    .line 193
    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    .line 195
    .line 196
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 197
    .line 198
    iget v3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 199
    .line 200
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 201
    .line 202
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 203
    .line 204
    .line 205
    move-result v1

    .line 206
    add-float/2addr v1, v2

    .line 207
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 208
    .line 209
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 210
    .line 211
    .line 212
    move-result v4

    .line 213
    sub-float v4, v1, v4

    .line 214
    .line 215
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 216
    .line 217
    add-float v5, v1, v10

    .line 218
    .line 219
    move-object v1, p1

    .line 220
    move-object v6, v0

    .line 221
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 222
    .line 223
    .line 224
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 225
    .line 226
    .line 227
    move-result p2

    .line 228
    if-lt v8, p2, :cond_3

    .line 229
    .line 230
    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 231
    .line 232
    .line 233
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 234
    .line 235
    iget v3, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 236
    .line 237
    add-float v4, v2, v10

    .line 238
    .line 239
    iget-object p2, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 240
    .line 241
    invoke-virtual {p2}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 242
    .line 243
    .line 244
    move-result p2

    .line 245
    add-float/2addr p2, v3

    .line 246
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 247
    .line 248
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 249
    .line 250
    .line 251
    move-result v1

    .line 252
    sub-float v5, p2, v1

    .line 253
    .line 254
    move-object v1, p1

    .line 255
    move-object v6, v0

    .line 256
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 257
    .line 258
    .line 259
    :cond_3
    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    .line 261
    .line 262
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 263
    .line 264
    .line 265
    return-void
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private scientificToNormal(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .line 1
    const/16 v0, 0x45

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gez v0, :cond_0

    .line 8
    .line 9
    return-object p1

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const/4 v3, 0x1

    .line 16
    add-int/2addr v0, v3

    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 26
    .line 27
    .line 28
    move-result-wide v4

    .line 29
    const-wide/16 v6, 0x0

    .line 30
    .line 31
    cmpg-double v8, v4, v6

    .line 32
    .line 33
    if-gez v8, :cond_1

    .line 34
    .line 35
    const/4 v4, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const/4 v4, 0x0

    .line 38
    :goto_0
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 39
    .line 40
    .line 41
    move-result v5

    .line 42
    const/16 v6, 0xa

    .line 43
    .line 44
    if-le v5, v6, :cond_2

    .line 45
    .line 46
    return-object p1

    .line 47
    :cond_2
    const-string p1, "0"

    .line 48
    .line 49
    const-string v5, "."

    .line 50
    .line 51
    const-string v7, ""

    .line 52
    .line 53
    if-gez v0, :cond_6

    .line 54
    .line 55
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    sub-int/2addr v6, v3

    .line 60
    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    const/16 v8, 0x30

    .line 65
    .line 66
    if-ne v6, v8, :cond_3

    .line 67
    .line 68
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    sub-int/2addr v6, v3

    .line 73
    invoke-virtual {v2, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    :cond_3
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    .line 84
    .line 85
    .line 86
    move-result v6

    .line 87
    invoke-virtual {v5, v1, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    :goto_1
    add-int/2addr v0, v3

    .line 91
    if-gez v0, :cond_4

    .line 92
    .line 93
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_4
    if-nez v4, :cond_5

    .line 100
    .line 101
    new-instance p1, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v0, "0."

    .line 107
    .line 108
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    goto :goto_4

    .line 128
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v0, "-0."

    .line 134
    .line 135
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 139
    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    const-string v0, "-"

    .line 148
    .line 149
    invoke-virtual {v2, v0, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    goto :goto_4

    .line 161
    :cond_6
    if-gt v0, v6, :cond_a

    .line 162
    .line 163
    const/16 v1, 0x2e

    .line 164
    .line 165
    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    .line 166
    .line 167
    .line 168
    move-result v3

    .line 169
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 170
    .line 171
    .line 172
    move-result v6

    .line 173
    add-int/lit8 v6, v6, -0x2

    .line 174
    .line 175
    if-eqz v4, :cond_7

    .line 176
    .line 177
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 178
    .line 179
    .line 180
    move-result v4

    .line 181
    add-int/lit8 v6, v4, -0x3

    .line 182
    .line 183
    :cond_7
    if-gt v6, v0, :cond_8

    .line 184
    .line 185
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    sub-int/2addr v0, v6

    .line 190
    move-object v2, v1

    .line 191
    :goto_2
    if-lez v0, :cond_a

    .line 192
    .line 193
    new-instance v1, Ljava/lang/StringBuilder;

    .line 194
    .line 195
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object v2

    .line 208
    add-int/lit8 v0, v0, -0x1

    .line 209
    .line 210
    goto :goto_2

    .line 211
    :cond_8
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    add-int/2addr v0, v3

    .line 216
    :goto_3
    if-ge v3, v0, :cond_9

    .line 217
    .line 218
    add-int/lit8 v2, v3, 0x1

    .line 219
    .line 220
    aget-char v4, p1, v2

    .line 221
    .line 222
    aput-char v4, p1, v3

    .line 223
    .line 224
    move v3, v2

    .line 225
    goto :goto_3

    .line 226
    :cond_9
    aput-char v1, p1, v3

    .line 227
    .line 228
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v2

    .line 232
    :cond_a
    :goto_4
    return-object v2
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private trimInvalidateZero(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    const/16 v0, 0x2e

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-lez v0, :cond_2

    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    array-length v1, p1

    .line 23
    add-int/lit8 v1, v1, -0x1

    .line 24
    .line 25
    :goto_0
    if-le v1, v0, :cond_1

    .line 26
    .line 27
    aget-char v2, p1, v0

    .line 28
    .line 29
    const/16 v3, 0x30

    .line 30
    .line 31
    if-ne v2, v3, :cond_1

    .line 32
    .line 33
    add-int/lit8 v0, v0, -0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    aget-char v0, p1, v1

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    invoke-static {p1, v0, v1}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    :cond_2
    :goto_1
    return-object p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/CellBorderView;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

    .line 12
    .line 13
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCell;->dispose()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 23
    .line 24
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 25
    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/ss/other/MergeCellMgr;->dispose()V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 32
    .line 33
    :cond_2
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->strBuilder:Ljava/lang/StringBuilder;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->tableStyleKit:Lcom/intsig/office/ss/model/table/TableStyleKit;

    .line 38
    .line 39
    if-eqz v1, :cond_3

    .line 40
    .line 41
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/table/TableStyleKit;->dispose()V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/office/ss/view/CellView;->tableStyleKit:Lcom/intsig/office/ss/model/table/TableStyleKit;

    .line 45
    .line 46
    :cond_3
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/other/DrawingCell;Ljava/lang/String;)V
    .locals 7

    .line 1
    if-eqz p2, :cond_4

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/ss/control/Spreadsheet;->isAbortDrawing()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto/16 :goto_2

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object p3, p0, Lcom/intsig/office/ss/view/CellView;->cellInfor:Lcom/intsig/office/ss/other/DrawingCell;

    .line 24
    .line 25
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getLeft()F

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iput v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 30
    .line 31
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getTop()F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    iput v1, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 38
    .line 39
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/MergeCell;->setWidth(F)V

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 47
    .line 48
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/MergeCell;->setHeight(F)V

    .line 53
    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 56
    .line 57
    const/4 v2, 0x0

    .line 58
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/MergeCell;->setNovisibleWidth(F)V

    .line 59
    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 62
    .line 63
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/other/MergeCell;->setNoVisibleHeight(F)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-ltz v1, :cond_2

    .line 71
    .line 72
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRangeAddressIndex()I

    .line 73
    .line 74
    .line 75
    move-result p2

    .line 76
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 81
    .line 82
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 83
    .line 84
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    invoke-virtual {v1, v2, p2, v3, v4}, Lcom/intsig/office/ss/other/MergeCellMgr;->isDrawMergeCell(Lcom/intsig/office/ss/view/SheetView;Lcom/intsig/office/ss/model/CellRangeAddress;II)Z

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    if-eqz v1, :cond_1

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellMgr:Lcom/intsig/office/ss/other/MergeCellMgr;

    .line 99
    .line 100
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 101
    .line 102
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getRowIndex()I

    .line 103
    .line 104
    .line 105
    move-result v3

    .line 106
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getColumnIndex()I

    .line 107
    .line 108
    .line 109
    move-result v4

    .line 110
    invoke-virtual {v1, v2, p2, v3, v4}, Lcom/intsig/office/ss/other/MergeCellMgr;->getMergedCellSize(Lcom/intsig/office/ss/view/SheetView;Lcom/intsig/office/ss/model/CellRangeAddress;II)Lcom/intsig/office/ss/other/MergeCell;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    iput-object v1, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 115
    .line 116
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/view/CellView;->redrawMergedCellBordersAndBackground(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/CellRangeAddress;)V

    .line 133
    .line 134
    .line 135
    move-object p2, v0

    .line 136
    goto :goto_0

    .line 137
    :cond_1
    return-void

    .line 138
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 139
    .line 140
    iget v1, p0, Lcom/intsig/office/ss/view/CellView;->left:F

    .line 141
    .line 142
    iget v2, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 143
    .line 144
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 145
    .line 146
    invoke-virtual {v3}, Lcom/intsig/office/ss/other/MergeCell;->getWidth()F

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    add-float/2addr v3, v1

    .line 151
    iget-object v4, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 152
    .line 153
    invoke-virtual {v4}, Lcom/intsig/office/ss/other/MergeCell;->getNovisibleWidth()F

    .line 154
    .line 155
    .line 156
    move-result v4

    .line 157
    sub-float/2addr v3, v4

    .line 158
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getWidth()F

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleWidth()F

    .line 163
    .line 164
    .line 165
    move-result v5

    .line 166
    sub-float/2addr v4, v5

    .line 167
    sub-float/2addr v3, v4

    .line 168
    iget v4, p0, Lcom/intsig/office/ss/view/CellView;->top:F

    .line 169
    .line 170
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 171
    .line 172
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getHeight()F

    .line 173
    .line 174
    .line 175
    move-result v5

    .line 176
    add-float/2addr v4, v5

    .line 177
    iget-object v5, p0, Lcom/intsig/office/ss/view/CellView;->mergedCellSize:Lcom/intsig/office/ss/other/MergeCell;

    .line 178
    .line 179
    invoke-virtual {v5}, Lcom/intsig/office/ss/other/MergeCell;->getNoVisibleHeight()F

    .line 180
    .line 181
    .line 182
    move-result v5

    .line 183
    sub-float/2addr v4, v5

    .line 184
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getHeight()F

    .line 185
    .line 186
    .line 187
    move-result v5

    .line 188
    invoke-virtual {p3}, Lcom/intsig/office/ss/other/DrawingCell;->getVisibleHeight()F

    .line 189
    .line 190
    .line 191
    move-result v6

    .line 192
    sub-float/2addr v5, v6

    .line 193
    sub-float/2addr v4, v5

    .line 194
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 195
    .line 196
    .line 197
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getTableInfo()Lcom/intsig/office/ss/model/table/SSTable;

    .line 198
    .line 199
    .line 200
    move-result-object v0

    .line 201
    if-eqz v0, :cond_3

    .line 202
    .line 203
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 204
    .line 205
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 210
    .line 211
    .line 212
    move-result-object v1

    .line 213
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 214
    .line 215
    .line 216
    move-result v2

    .line 217
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 218
    .line 219
    .line 220
    move-result v3

    .line 221
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/intsig/office/ss/view/CellView;->getTableCellStyle(Lcom/intsig/office/ss/model/table/SSTable;Lcom/intsig/office/ss/model/baseModel/Workbook;II)Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 222
    .line 223
    .line 224
    move-result-object v0

    .line 225
    goto :goto_1

    .line 226
    :cond_3
    const/4 v0, 0x0

    .line 227
    :goto_1
    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/intsig/office/ss/view/CellView;->drawCellBackgroundAndBorder(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/model/table/SSTableCellStyle;Ljava/lang/String;)V

    .line 228
    .line 229
    .line 230
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/ss/view/CellView;->drawCellContents(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/other/DrawingCell;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V

    .line 231
    .line 232
    .line 233
    :cond_4
    :goto_2
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public drawActiveCellBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/ss/view/CellBorderView;->drawActiveCellBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;S)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public drawCellBackgroundAndBorder(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/ss/model/table/SSTableCellStyle;Ljava/lang/String;)V
    .locals 10

    .line 1
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 10
    .line 11
    .line 12
    move-result v9

    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getCurrentSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v3, v2, p2}, Lcom/intsig/office/ss/util/ModelUtil;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-nez v3, :cond_0

    .line 40
    .line 41
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-nez v3, :cond_0

    .line 46
    .line 47
    invoke-virtual {v2, p4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 48
    .line 49
    .line 50
    move-result p4

    .line 51
    if-eqz p4, :cond_0

    .line 52
    .line 53
    const/4 p4, 0x1

    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/4 p4, 0x0

    .line 56
    :goto_0
    const/high16 v2, 0x3f800000    # 1.0f

    .line 57
    .line 58
    if-eqz v1, :cond_3

    .line 59
    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFillPatternType()B

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    if-nez v3, :cond_3

    .line 65
    .line 66
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFgColorIndex()I

    .line 75
    .line 76
    .line 77
    move-result v4

    .line 78
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFgColor()I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    if-eqz v4, :cond_1

    .line 83
    .line 84
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    :cond_1
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 89
    .line 90
    .line 91
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 92
    .line 93
    iget v1, v1, Landroid/graphics/RectF;->left:F

    .line 94
    .line 95
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 96
    .line 97
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 98
    .line 99
    .line 100
    move-result v3

    .line 101
    int-to-float v3, v3

    .line 102
    sub-float/2addr v1, v3

    .line 103
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    cmpg-float v1, v1, v2

    .line 108
    .line 109
    if-gez v1, :cond_2

    .line 110
    .line 111
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 112
    .line 113
    iget v3, v1, Landroid/graphics/RectF;->left:F

    .line 114
    .line 115
    add-float/2addr v2, v3

    .line 116
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 117
    .line 118
    iget v4, v1, Landroid/graphics/RectF;->right:F

    .line 119
    .line 120
    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    .line 121
    .line 122
    move-object v1, p1

    .line 123
    move-object v6, v0

    .line 124
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 125
    .line 126
    .line 127
    goto/16 :goto_1

    .line 128
    .line 129
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 130
    .line 131
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 132
    .line 133
    .line 134
    goto/16 :goto_1

    .line 135
    .line 136
    :cond_3
    if-eqz v1, :cond_5

    .line 137
    .line 138
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFillPatternType()B

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    const/4 v4, 0x7

    .line 143
    if-eq v3, v4, :cond_4

    .line 144
    .line 145
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFillPatternType()B

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    const/4 v4, 0x4

    .line 150
    if-ne v3, v4, :cond_5

    .line 151
    .line 152
    :cond_4
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 153
    .line 154
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getSpreadsheet()Lcom/intsig/office/ss/control/Spreadsheet;

    .line 155
    .line 156
    .line 157
    move-result-object v2

    .line 158
    invoke-virtual {v2}, Lcom/intsig/office/ss/control/Spreadsheet;->getControl()Lcom/intsig/office/system/IControl;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    iget-object v2, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 163
    .line 164
    invoke-virtual {v2}, Lcom/intsig/office/ss/view/SheetView;->getSheetIndex()I

    .line 165
    .line 166
    .line 167
    move-result v4

    .line 168
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->getFillPattern()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 169
    .line 170
    .line 171
    move-result-object v5

    .line 172
    iget-object v6, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 173
    .line 174
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 175
    .line 176
    invoke-virtual {v1}, Lcom/intsig/office/ss/view/SheetView;->getZoom()F

    .line 177
    .line 178
    .line 179
    move-result v7

    .line 180
    move-object v1, p0

    .line 181
    move-object v2, p1

    .line 182
    move-object v8, v0

    .line 183
    invoke-direct/range {v1 .. v8}, Lcom/intsig/office/ss/view/CellView;->drawGradientAndTile(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/RectF;FLandroid/graphics/Paint;)V

    .line 184
    .line 185
    .line 186
    goto :goto_1

    .line 187
    :cond_5
    if-eqz p3, :cond_7

    .line 188
    .line 189
    invoke-virtual {p3}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getFillColor()Ljava/lang/Integer;

    .line 190
    .line 191
    .line 192
    move-result-object v1

    .line 193
    if-eqz v1, :cond_7

    .line 194
    .line 195
    invoke-virtual {p3}, Lcom/intsig/office/ss/model/table/SSTableCellStyle;->getFillColor()Ljava/lang/Integer;

    .line 196
    .line 197
    .line 198
    move-result-object v1

    .line 199
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 200
    .line 201
    .line 202
    move-result v1

    .line 203
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 204
    .line 205
    .line 206
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 207
    .line 208
    iget v1, v1, Landroid/graphics/RectF;->left:F

    .line 209
    .line 210
    iget-object v3, p0, Lcom/intsig/office/ss/view/CellView;->sheetView:Lcom/intsig/office/ss/view/SheetView;

    .line 211
    .line 212
    invoke-virtual {v3}, Lcom/intsig/office/ss/view/SheetView;->getRowHeaderWidth()I

    .line 213
    .line 214
    .line 215
    move-result v3

    .line 216
    int-to-float v3, v3

    .line 217
    sub-float/2addr v1, v3

    .line 218
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    cmpg-float v1, v1, v2

    .line 223
    .line 224
    if-gez v1, :cond_6

    .line 225
    .line 226
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 227
    .line 228
    iget v3, v1, Landroid/graphics/RectF;->left:F

    .line 229
    .line 230
    add-float/2addr v2, v3

    .line 231
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 232
    .line 233
    iget v4, v1, Landroid/graphics/RectF;->right:F

    .line 234
    .line 235
    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    .line 236
    .line 237
    move-object v1, p1

    .line 238
    move-object v6, v0

    .line 239
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 240
    .line 241
    .line 242
    goto :goto_1

    .line 243
    :cond_6
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 244
    .line 245
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 246
    .line 247
    .line 248
    :cond_7
    :goto_1
    if-eqz p4, :cond_8

    .line 249
    .line 250
    sget-object p4, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 251
    .line 252
    sget v1, Lcom/intsig/office_preview/R$color;->cs_color_19BCAA_10_percent:I

    .line 253
    .line 254
    invoke-static {p4, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 255
    .line 256
    .line 257
    move-result p4

    .line 258
    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 259
    .line 260
    .line 261
    iget-object p4, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 262
    .line 263
    invoke-virtual {p1, p4, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 264
    .line 265
    .line 266
    :cond_8
    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 267
    .line 268
    .line 269
    iget-object p4, p0, Lcom/intsig/office/ss/view/CellView;->cellBorderView:Lcom/intsig/office/ss/view/CellBorderView;

    .line 270
    .line 271
    iget-object v0, p0, Lcom/intsig/office/ss/view/CellView;->cellRect:Landroid/graphics/RectF;

    .line 272
    .line 273
    invoke-virtual {p4, p1, p2, v0, p3}, Lcom/intsig/office/ss/view/CellBorderView;->draw(Landroid/graphics/Canvas;Lcom/intsig/office/ss/model/baseModel/Cell;Landroid/graphics/RectF;Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V

    .line 274
    .line 275
    .line 276
    return-void
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getTableCellStyle(Lcom/intsig/office/ss/model/table/SSTable;Lcom/intsig/office/ss/model/baseModel/Workbook;II)Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->getTableReference()Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/ss/view/CellView;->tableStyleKit:Lcom/intsig/office/ss/model/table/TableStyleKit;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->getName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-static {p2}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->getSchemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;)Ljava/util/Map;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    invoke-virtual {v1, v2, p2}, Lcom/intsig/office/ss/model/table/TableStyleKit;->getTableStyle(Ljava/lang/String;Ljava/util/Map;)Lcom/intsig/office/ss/model/table/SSTableStyle;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    if-nez p2, :cond_0

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    return-object p1

    .line 23
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isHeaderRowShown()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_d

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-ne p3, v1, :cond_1

    .line 34
    .line 35
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    return-object p1

    .line 40
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isTotalRowShown()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_5

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-ne p3, v1, :cond_5

    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 53
    .line 54
    .line 55
    move-result-object p3

    .line 56
    if-eqz p3, :cond_2

    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    return-object p1

    .line 63
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowFirstColumn()Z

    .line 64
    .line 65
    .line 66
    move-result p3

    .line 67
    if-eqz p3, :cond_3

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 70
    .line 71
    .line 72
    move-result p3

    .line 73
    if-ne p4, p3, :cond_3

    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 76
    .line 77
    .line 78
    move-result-object p3

    .line 79
    if-eqz p3, :cond_3

    .line 80
    .line 81
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    return-object p1

    .line 86
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowLastColumn()Z

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    if-eqz p1, :cond_4

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 93
    .line 94
    .line 95
    move-result p1

    .line 96
    if-ne p4, p1, :cond_4

    .line 97
    .line 98
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    if-eqz p1, :cond_4

    .line 103
    .line 104
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    return-object p1

    .line 109
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    return-object p1

    .line 114
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowFirstColumn()Z

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    if-eqz v1, :cond_6

    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    if-ne p4, v1, :cond_6

    .line 125
    .line 126
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    if-eqz v1, :cond_6

    .line 131
    .line 132
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    return-object p1

    .line 137
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowLastColumn()Z

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    if-eqz v1, :cond_7

    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    if-ne p4, v1, :cond_7

    .line 148
    .line 149
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    if-eqz v1, :cond_7

    .line 154
    .line 155
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 156
    .line 157
    .line 158
    move-result-object p1

    .line 159
    return-object p1

    .line 160
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowRowStripes()Z

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    if-eqz v1, :cond_a

    .line 165
    .line 166
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 167
    .line 168
    .line 169
    move-result v1

    .line 170
    sub-int/2addr p3, v1

    .line 171
    rem-int/lit8 p3, p3, 0x2

    .line 172
    .line 173
    const/4 v1, 0x1

    .line 174
    if-ne p3, v1, :cond_8

    .line 175
    .line 176
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    return-object p1

    .line 181
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowColumnStripes()Z

    .line 182
    .line 183
    .line 184
    move-result p1

    .line 185
    if-eqz p1, :cond_9

    .line 186
    .line 187
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    sub-int/2addr p4, p1

    .line 192
    rem-int/lit8 p4, p4, 0x2

    .line 193
    .line 194
    if-nez p4, :cond_9

    .line 195
    .line 196
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 197
    .line 198
    .line 199
    move-result-object p1

    .line 200
    return-object p1

    .line 201
    :cond_9
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 202
    .line 203
    .line 204
    move-result-object p1

    .line 205
    return-object p1

    .line 206
    :cond_a
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowColumnStripes()Z

    .line 207
    .line 208
    .line 209
    move-result p1

    .line 210
    if-eqz p1, :cond_c

    .line 211
    .line 212
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 213
    .line 214
    .line 215
    move-result p1

    .line 216
    sub-int/2addr p4, p1

    .line 217
    rem-int/lit8 p4, p4, 0x2

    .line 218
    .line 219
    if-nez p4, :cond_b

    .line 220
    .line 221
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 222
    .line 223
    .line 224
    move-result-object p1

    .line 225
    return-object p1

    .line 226
    :cond_b
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    return-object p1

    .line 231
    :cond_c
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    return-object p1

    .line 236
    :cond_d
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isTotalRowShown()Z

    .line 237
    .line 238
    .line 239
    move-result v1

    .line 240
    if-eqz v1, :cond_11

    .line 241
    .line 242
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 243
    .line 244
    .line 245
    move-result v1

    .line 246
    if-ne p3, v1, :cond_11

    .line 247
    .line 248
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 249
    .line 250
    .line 251
    move-result-object p3

    .line 252
    if-eqz p3, :cond_e

    .line 253
    .line 254
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    return-object p1

    .line 259
    :cond_e
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowFirstColumn()Z

    .line 260
    .line 261
    .line 262
    move-result p3

    .line 263
    if-eqz p3, :cond_f

    .line 264
    .line 265
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 266
    .line 267
    .line 268
    move-result p3

    .line 269
    if-ne p4, p3, :cond_f

    .line 270
    .line 271
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 272
    .line 273
    .line 274
    move-result-object p3

    .line 275
    if-eqz p3, :cond_f

    .line 276
    .line 277
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 278
    .line 279
    .line 280
    move-result-object p1

    .line 281
    return-object p1

    .line 282
    :cond_f
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowLastColumn()Z

    .line 283
    .line 284
    .line 285
    move-result p1

    .line 286
    if-eqz p1, :cond_10

    .line 287
    .line 288
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 289
    .line 290
    .line 291
    move-result p1

    .line 292
    if-ne p4, p1, :cond_10

    .line 293
    .line 294
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 295
    .line 296
    .line 297
    move-result-object p1

    .line 298
    if-eqz p1, :cond_10

    .line 299
    .line 300
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 301
    .line 302
    .line 303
    move-result-object p1

    .line 304
    return-object p1

    .line 305
    :cond_10
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 306
    .line 307
    .line 308
    move-result-object p1

    .line 309
    return-object p1

    .line 310
    :cond_11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowFirstColumn()Z

    .line 311
    .line 312
    .line 313
    move-result v1

    .line 314
    if-eqz v1, :cond_12

    .line 315
    .line 316
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 317
    .line 318
    .line 319
    move-result v1

    .line 320
    if-ne p4, v1, :cond_12

    .line 321
    .line 322
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 323
    .line 324
    .line 325
    move-result-object v1

    .line 326
    if-eqz v1, :cond_12

    .line 327
    .line 328
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 329
    .line 330
    .line 331
    move-result-object p1

    .line 332
    return-object p1

    .line 333
    :cond_12
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowLastColumn()Z

    .line 334
    .line 335
    .line 336
    move-result v1

    .line 337
    if-eqz v1, :cond_13

    .line 338
    .line 339
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 340
    .line 341
    .line 342
    move-result v1

    .line 343
    if-ne p4, v1, :cond_13

    .line 344
    .line 345
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 346
    .line 347
    .line 348
    move-result-object v1

    .line 349
    if-eqz v1, :cond_13

    .line 350
    .line 351
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 352
    .line 353
    .line 354
    move-result-object p1

    .line 355
    return-object p1

    .line 356
    :cond_13
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowRowStripes()Z

    .line 357
    .line 358
    .line 359
    move-result v1

    .line 360
    if-eqz v1, :cond_16

    .line 361
    .line 362
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 363
    .line 364
    .line 365
    move-result v1

    .line 366
    sub-int/2addr p3, v1

    .line 367
    rem-int/lit8 p3, p3, 0x2

    .line 368
    .line 369
    if-nez p3, :cond_14

    .line 370
    .line 371
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 372
    .line 373
    .line 374
    move-result-object p1

    .line 375
    return-object p1

    .line 376
    :cond_14
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowColumnStripes()Z

    .line 377
    .line 378
    .line 379
    move-result p1

    .line 380
    if-eqz p1, :cond_15

    .line 381
    .line 382
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 383
    .line 384
    .line 385
    move-result p1

    .line 386
    sub-int/2addr p4, p1

    .line 387
    rem-int/lit8 p4, p4, 0x2

    .line 388
    .line 389
    if-nez p4, :cond_15

    .line 390
    .line 391
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 392
    .line 393
    .line 394
    move-result-object p1

    .line 395
    return-object p1

    .line 396
    :cond_15
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 397
    .line 398
    .line 399
    move-result-object p1

    .line 400
    return-object p1

    .line 401
    :cond_16
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/table/SSTable;->isShowColumnStripes()Z

    .line 402
    .line 403
    .line 404
    move-result p1

    .line 405
    if-eqz p1, :cond_18

    .line 406
    .line 407
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 408
    .line 409
    .line 410
    move-result p1

    .line 411
    sub-int/2addr p4, p1

    .line 412
    rem-int/lit8 p4, p4, 0x2

    .line 413
    .line 414
    if-nez p4, :cond_17

    .line 415
    .line 416
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand1H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 417
    .line 418
    .line 419
    move-result-object p1

    .line 420
    return-object p1

    .line 421
    :cond_17
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 422
    .line 423
    .line 424
    move-result-object p1

    .line 425
    return-object p1

    .line 426
    :cond_18
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/table/SSTableStyle;->getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 427
    .line 428
    .line 429
    move-result-object p1

    .line 430
    return-object p1
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

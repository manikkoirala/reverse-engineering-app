.class public Lcom/intsig/office/ss/model/XLSModel/AWorkbook;
.super Lcom/intsig/office/ss/model/baseModel/Workbook;
.source "AWorkbook.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Workbook;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/ss/model/XLSModel/AWorkbook$ShapesThread;
    }
.end annotation


# static fields
.field public static final AUTOMATIC_COLOR:I = 0x40

.field public static final INITIAL_CAPACITY:I = 0x3

.field private static final WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;


# instance fields
.field private _udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

.field private currentSheet:I

.field private iAbortListener:Lcom/intsig/office/fc/xls/SSReader;

.field private names:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hssf/usermodel/HSSFName;",
            ">;"
        }
    .end annotation
.end field

.field private workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, "Workbook"

    .line 2
    .line 3
    const-string v1, "WORKBOOK"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sput-object v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/intsig/office/fc/xls/SSReader;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;-><init>(Z)V

    .line 3
    .line 4
    .line 5
    sget-object v1, Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;->DEFAULT:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 6
    .line 7
    iput-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->iAbortListener:Lcom/intsig/office/fc/xls/SSReader;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 12
    .line 13
    invoke-direct {v1, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getWorkbookDirEntryName(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/record/RecordFactory;->createRecords(Ljava/io/InputStream;Lcom/intsig/office/system/AbstractReader;)Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->createWorkbook(Ljava/util/List;Lcom/intsig/office/system/AbstractReader;)Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iput-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumRecords()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    iget-object v2, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSSTUniqueStringSize()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    const/4 v3, 0x0

    .line 49
    const/4 v4, 0x0

    .line 50
    :goto_0
    if-ge v4, v2, :cond_0

    .line 51
    .line 52
    iget-object v5, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 53
    .line 54
    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSSTString(I)Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 55
    .line 56
    .line 57
    move-result-object v5

    .line 58
    invoke-virtual {p0, v4, v5}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(ILjava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    add-int/lit8 v4, v4, 0x1

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->convertLabelRecords(Ljava/util/List;I)V

    .line 65
    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 68
    .line 69
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->isUsing1904DateWindowing()Z

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    iput-boolean v2, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->isUsing1904DateWindowing:Z

    .line 74
    .line 75
    iget-object v2, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 76
    .line 77
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getCustomPalette()Lcom/intsig/office/fc/hssf/record/PaletteRecord;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-static {v3, v3, v3}, Lcom/intsig/office/ss/util/ColorUtil;->rgb(III)I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    const/16 v5, 0x8

    .line 86
    .line 87
    invoke-virtual {p0, v5, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addColor(II)V

    .line 88
    .line 89
    .line 90
    const/16 v4, 0x9

    .line 91
    .line 92
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 93
    .line 94
    .line 95
    move-result-object v5

    .line 96
    :goto_1
    if-eqz v5, :cond_1

    .line 97
    .line 98
    add-int/lit8 v6, v4, 0x1

    .line 99
    .line 100
    aget-byte v7, v5, v3

    .line 101
    .line 102
    aget-byte v8, v5, v0

    .line 103
    .line 104
    const/4 v9, 0x2

    .line 105
    aget-byte v5, v5, v9

    .line 106
    .line 107
    invoke-static {v7, v8, v5}, Lcom/intsig/office/ss/util/ColorUtil;->rgb(BBB)I

    .line 108
    .line 109
    .line 110
    move-result v5

    .line 111
    invoke-virtual {p0, v4, v5}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addColor(II)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/hssf/record/PaletteRecord;->getColor(I)[B

    .line 115
    .line 116
    .line 117
    move-result-object v5

    .line 118
    move v4, v6

    .line 119
    goto :goto_1

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 121
    .line 122
    invoke-direct {p0, v2}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->processCellStyle(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 123
    .line 124
    .line 125
    new-instance v2, Lcom/intsig/office/fc/hssf/model/RecordStream;

    .line 126
    .line 127
    invoke-direct {v2, p1, v1}, Lcom/intsig/office/fc/hssf/model/RecordStream;-><init>(Ljava/util/List;I)V

    .line 128
    .line 129
    .line 130
    const/4 v1, 0x0

    .line 131
    :goto_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/model/RecordStream;->hasNext()Z

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    if-eqz v4, :cond_3

    .line 136
    .line 137
    invoke-static {v2, p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->createSheet(Lcom/intsig/office/fc/hssf/model/RecordStream;Lcom/intsig/office/system/AbstractReader;)Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    new-instance v5, Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 142
    .line 143
    invoke-direct {v5, p0, v4}, Lcom/intsig/office/ss/model/XLSModel/ASheet;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    .line 144
    .line 145
    .line 146
    iget-object v6, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 147
    .line 148
    invoke-virtual {v6, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetName(I)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v6

    .line 152
    invoke-virtual {v5, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setSheetName(Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->isChartSheet()Z

    .line 156
    .line 157
    .line 158
    move-result v4

    .line 159
    if-eqz v4, :cond_2

    .line 160
    .line 161
    invoke-virtual {v5, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setSheetType(S)V

    .line 162
    .line 163
    .line 164
    :cond_2
    iget-object v4, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    .line 165
    .line 166
    add-int/lit8 v6, v1, 0x1

    .line 167
    .line 168
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    .line 174
    .line 175
    move v1, v6

    .line 176
    goto :goto_2

    .line 177
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 178
    .line 179
    .line 180
    new-instance p1, Ljava/util/ArrayList;

    .line 181
    .line 182
    const/4 p2, 0x3

    .line 183
    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 184
    .line 185
    .line 186
    iput-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 187
    .line 188
    :goto_3
    iget-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 189
    .line 190
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumNames()I

    .line 191
    .line 192
    .line 193
    move-result p1

    .line 194
    if-ge v3, p1, :cond_4

    .line 195
    .line 196
    iget-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 197
    .line 198
    invoke-virtual {p1, v3}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    new-instance p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 203
    .line 204
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 205
    .line 206
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameCommentRecord(Lcom/intsig/office/fc/hssf/record/NameRecord;)Lcom/intsig/office/fc/hssf/record/NameCommentRecord;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-direct {p2, p0, p1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/record/NameRecord;Lcom/intsig/office/fc/hssf/record/NameCommentRecord;)V

    .line 211
    .line 212
    .line 213
    iget-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 214
    .line 215
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    .line 217
    .line 218
    add-int/lit8 v3, v3, 0x1

    .line 219
    .line 220
    goto :goto_3

    .line 221
    :cond_4
    invoke-direct {p0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->processSheet()V

    .line 222
    .line 223
    .line 224
    return-void
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method static bridge synthetic O8(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/system/IControl;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->processShapesBySheetIndex(Lcom/intsig/office/system/IControl;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method static synthetic access$000(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static synthetic access$100(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private convertLabelRecords(Ljava/util/List;I)V
    .locals 3

    .line 1
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ge p2, v0, :cond_1

    .line 6
    .line 7
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    const/16 v2, 0x204

    .line 18
    .line 19
    if-ne v1, v2, :cond_0

    .line 20
    .line 21
    check-cast v0, Lcom/intsig/office/fc/hssf/record/LabelRecord;

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sharedString:Ljava/util/Map;

    .line 24
    .line 25
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getValue()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static getWorkbookDirEntryName(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/lang/String;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->WORKBOOK_DIR_ENTRY_NAMES:[Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :goto_0
    array-length v2, v0

    .line 5
    if-ge v1, v2, :cond_0

    .line 6
    .line 7
    aget-object v2, v0, v1

    .line 8
    .line 9
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    return-object v2

    .line 13
    :catch_0
    add-int/lit8 v1, v1, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    :try_start_1
    const-string v0, "Book"

    .line 17
    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 19
    .line 20
    .line 21
    new-instance p0, Lcom/intsig/office/fc/hssf/OldExcelFormatException;

    .line 22
    .line 23
    const-string v0, "The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)"

    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hssf/OldExcelFormatException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 29
    :catch_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 30
    .line 31
    const-string v0, "The supplied POIFSFileSystem does not contain a BIFF8 \'Workbook\' entry. Is it really an excel file?"

    .line 32
    .line 33
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw p0
    .line 37
    .line 38
.end method

.method private processCellStyle(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V
    .locals 7

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->processFont(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumExFormats()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    int-to-short v0, v0

    .line 9
    const/4 v1, 0x0

    .line 10
    :goto_0
    if-ge v1, v0, :cond_6

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExFormatAt(I)Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    if-nez v2, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    new-instance v3, Lcom/intsig/office/ss/model/style/CellStyle;

    .line 20
    .line 21
    invoke-direct {v3}, Lcom/intsig/office/ss/model/style/CellStyle;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v3, v1}, Lcom/intsig/office/ss/model/style/CellStyle;->setIndex(S)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFormatIndex()S

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setNumberFormatID(S)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFormatIndex()S

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    invoke-static {p1, v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;->getFormatCode(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;S)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setFormatCode(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFontIndex()S

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setFontIndex(S)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->isHidden()Z

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setHidden(Z)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->isLocked()Z

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setLocked(Z)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getWrapText()Z

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setWrapText(Z)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getAlignment()S

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setHorizontalAlign(S)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getVerticalAlignment()S

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setVerticalAlign(S)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getRotation()S

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setRotation(S)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getIndent()S

    .line 95
    .line 96
    .line 97
    move-result v4

    .line 98
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setIndent(S)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getBorderLeft()S

    .line 102
    .line 103
    .line 104
    move-result v4

    .line 105
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderLeft(S)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getLeftBorderPaletteIdx()S

    .line 109
    .line 110
    .line 111
    move-result v4

    .line 112
    const/16 v5, 0x8

    .line 113
    .line 114
    const/16 v6, 0x40

    .line 115
    .line 116
    if-ne v4, v6, :cond_1

    .line 117
    .line 118
    const/16 v4, 0x8

    .line 119
    .line 120
    :cond_1
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderLeftColorIdx(S)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getBorderRight()S

    .line 124
    .line 125
    .line 126
    move-result v4

    .line 127
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderRight(S)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getRightBorderPaletteIdx()S

    .line 131
    .line 132
    .line 133
    move-result v4

    .line 134
    if-ne v4, v6, :cond_2

    .line 135
    .line 136
    const/16 v4, 0x8

    .line 137
    .line 138
    :cond_2
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderRightColorIdx(S)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getBorderTop()S

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderTop(S)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getTopBorderPaletteIdx()S

    .line 149
    .line 150
    .line 151
    move-result v4

    .line 152
    if-ne v4, v6, :cond_3

    .line 153
    .line 154
    const/16 v4, 0x8

    .line 155
    .line 156
    :cond_3
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderTopColorIdx(S)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getBorderBottom()S

    .line 160
    .line 161
    .line 162
    move-result v4

    .line 163
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderBottom(S)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getBottomBorderPaletteIdx()S

    .line 167
    .line 168
    .line 169
    move-result v4

    .line 170
    if-ne v4, v6, :cond_4

    .line 171
    .line 172
    goto :goto_1

    .line 173
    :cond_4
    move v5, v4

    .line 174
    :goto_1
    invoke-virtual {v3, v5}, Lcom/intsig/office/ss/model/style/CellStyle;->setBorderBottomColorIdx(S)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFillBackground()S

    .line 178
    .line 179
    .line 180
    move-result v4

    .line 181
    invoke-virtual {p0, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 182
    .line 183
    .line 184
    move-result v4

    .line 185
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setBgColor(I)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getFillForeground()S

    .line 189
    .line 190
    .line 191
    move-result v4

    .line 192
    if-ne v4, v6, :cond_5

    .line 193
    .line 194
    const/16 v4, 0x9

    .line 195
    .line 196
    :cond_5
    invoke-virtual {p0, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 197
    .line 198
    .line 199
    move-result v4

    .line 200
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/style/CellStyle;->setFgColor(I)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/ExtendedFormatRecord;->getAdtlFillPattern()S

    .line 204
    .line 205
    .line 206
    move-result v2

    .line 207
    add-int/lit8 v2, v2, -0x1

    .line 208
    .line 209
    int-to-byte v2, v2

    .line 210
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/style/CellStyle;->setFillPatternType(B)V

    .line 211
    .line 212
    .line 213
    add-int/lit8 v2, v1, 0x1

    .line 214
    .line 215
    int-to-short v2, v2

    .line 216
    invoke-virtual {p0, v1, v3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addCellStyle(ILcom/intsig/office/ss/model/style/CellStyle;)V

    .line 217
    .line 218
    .line 219
    move v1, v2

    .line 220
    goto/16 :goto_0

    .line 221
    .line 222
    :cond_6
    return-void
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private processFont(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNumberOfFontRecords()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x4

    .line 6
    if-gt v0, v1, :cond_0

    .line 7
    .line 8
    add-int/lit8 v0, v0, -0x1

    .line 9
    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    :goto_0
    if-gt v2, v0, :cond_3

    .line 13
    .line 14
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getFontRecordAt(I)Lcom/intsig/office/fc/hssf/record/FontRecord;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    new-instance v4, Lcom/intsig/office/simpletext/font/Font;

    .line 19
    .line 20
    invoke-direct {v4}, Lcom/intsig/office/simpletext/font/Font;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v4, v2}, Lcom/intsig/office/simpletext/font/Font;->setIndex(I)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getFontName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v5

    .line 30
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setName(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getFontHeight()S

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    div-int/lit8 v5, v5, 0x14

    .line 38
    .line 39
    int-to-short v5, v5

    .line 40
    int-to-double v5, v5

    .line 41
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/simpletext/font/Font;->setFontSize(D)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getColorPaletteIndex()S

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    const/16 v6, 0x7fff

    .line 49
    .line 50
    if-ne v5, v6, :cond_1

    .line 51
    .line 52
    const/16 v5, 0x8

    .line 53
    .line 54
    :cond_1
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setColorIndex(I)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->isItalic()Z

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setItalic(Z)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getBoldWeight()S

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    const/16 v6, 0x190

    .line 69
    .line 70
    if-le v5, v6, :cond_2

    .line 71
    .line 72
    const/4 v5, 0x1

    .line 73
    goto :goto_1

    .line 74
    :cond_2
    const/4 v5, 0x0

    .line 75
    :goto_1
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setBold(Z)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getSuperSubScript()S

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    int-to-byte v5, v5

    .line 83
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setSuperSubScript(B)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->isStruckout()Z

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    invoke-virtual {v4, v5}, Lcom/intsig/office/simpletext/font/Font;->setStrikeline(Z)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/FontRecord;->getUnderline()B

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    invoke-virtual {v4, v3}, Lcom/intsig/office/simpletext/font/Font;->setUnderline(I)V

    .line 98
    .line 99
    .line 100
    add-int/lit8 v3, v2, 0x1

    .line 101
    .line 102
    invoke-virtual {p0, v2, v4}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addFont(ILcom/intsig/office/simpletext/font/Font;)V

    .line 103
    .line 104
    .line 105
    move v2, v3

    .line 106
    goto :goto_0

    .line 107
    :cond_3
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processShapesBySheetIndex(Lcom/intsig/office/system/IControl;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    check-cast p2, Lcom/intsig/office/ss/model/XLSModel/ASheet;

    .line 12
    .line 13
    const/4 v0, 0x2

    .line 14
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eq v1, v0, :cond_0

    .line 19
    .line 20
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processSheetShapes(Lcom/intsig/office/system/IControl;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setState(S)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setState(S)V

    .line 28
    .line 29
    .line 30
    :cond_0
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private processSheet()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook$1WorkbookReaderHandler;

    .line 2
    .line 3
    invoke-direct {v0, p0, p0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook$1WorkbookReaderHandler;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->readerHandler:Lcom/intsig/office/system/ReaderHandler;

    .line 7
    .line 8
    new-instance v0, Landroid/os/Message;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    iput v1, v0, Landroid/os/Message;->what:I

    .line 15
    .line 16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->readerHandler:Lcom/intsig/office/system/ReaderHandler;

    .line 23
    .line 24
    invoke-virtual {v1, v0}, Lcom/intsig/office/system/ReaderHandler;->handleMessage(Landroid/os/Message;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->currentSheet:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)Lcom/intsig/office/fc/xls/SSReader;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->iAbortListener:Lcom/intsig/office/fc/xls/SSReader;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->currentSheet:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->destroy()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 8
    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;->dispose()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 45
    .line 46
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->iAbortListener:Lcom/intsig/office/fc/xls/SSReader;

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getAbstractReader()Lcom/intsig/office/system/AbstractReader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->iAbortListener:Lcom/intsig/office/fc/xls/SSReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getName(Ljava/lang/String;)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getNameIndex(Ljava/lang/String;)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getNameAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-lt v0, v1, :cond_1

    .line 9
    .line 10
    if-ltz p1, :cond_0

    .line 11
    .line 12
    if-gt p1, v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 21
    .line 22
    return-object p1

    .line 23
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 24
    .line 25
    new-instance v3, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v4, "Specified name index "

    .line 31
    .line 32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string p1, " is outside the allowable range (0.."

    .line 39
    .line 40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    sub-int/2addr v0, v1

    .line 44
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p1, ")."

    .line 48
    .line 49
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {v2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    throw v2

    .line 60
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 61
    .line 62
    const-string v0, "There are no defined names in this workbook"

    .line 63
    .line 64
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getNameIndex(Ljava/lang/String;)I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 3
    .line 4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-ge v0, v1, :cond_1

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getNameName(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    return v0

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 p1, -0x1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getNameName(I)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getNameAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;->getNameName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getNameRecord(I)Lcom/intsig/office/fc/hssf/record/NameRecord;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getNumberOfNames()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->names:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberOfSheets()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public bridge synthetic getSheetAt(I)Lcom/intsig/office/fc/ss/usermodel/Sheet;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getSheetAt(I)Lcom/intsig/office/ss/model/XLSModel/ASheet;

    move-result-object p1

    return-object p1
.end method

.method public getSheetAt(I)Lcom/intsig/office/ss/model/XLSModel/ASheet;
    .locals 1

    if-ltz p1, :cond_1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/ASheet;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getSheetIndex(Lcom/intsig/office/ss/model/baseModel/Sheet;)I
    .locals 3

    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 3
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Workbook;->sheets:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public getSheetIndex(Ljava/lang/String;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getUDFFinder()Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->_udfFinder:Lcom/intsig/office/fc/hssf/formula/udf/UDFFinder;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

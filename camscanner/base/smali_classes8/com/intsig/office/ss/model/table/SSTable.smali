.class public Lcom/intsig/office/ss/model/table/SSTable;
.super Ljava/lang/Object;
.source "SSTable.java"


# instance fields
.field private headerRowBorderDxfId:I

.field private headerRowDxfId:I

.field private headerRowShown:Z

.field private name:Ljava/lang/String;

.field private ref:Lcom/intsig/office/ss/model/CellRangeAddress;

.field private showColumnStripes:Z

.field private showFirstColumn:Z

.field private showLastColumn:Z

.field private showRowStripes:Z

.field private tableBorderDxfId:I

.field private totalRowShown:Z

.field private totalsRowBorderDxfId:I

.field private totalsRowDxfId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowShown:Z

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalRowShown:Z

    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showFirstColumn:Z

    .line 11
    .line 12
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showLastColumn:Z

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showRowStripes:Z

    .line 15
    .line 16
    iput-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showColumnStripes:Z

    .line 17
    .line 18
    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->tableBorderDxfId:I

    .line 20
    .line 21
    iput v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowDxfId:I

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowBorderDxfId:I

    .line 24
    .line 25
    iput v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowDxfId:I

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowBorderDxfId:I

    .line 28
    .line 29
    return-void
    .line 30
.end method


# virtual methods
.method public getHeaderRowBorderDxfId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowBorderDxfId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderRowDxfId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowDxfId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableBorderDxfId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->tableBorderDxfId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableReference()Lcom/intsig/office/ss/model/CellRangeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->ref:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTotalsRowBorderDxfId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowBorderDxfId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTotalsRowDxfId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowDxfId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isHeaderRowShown()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowShown:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowColumnStripes()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showColumnStripes:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowFirstColumn()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showFirstColumn:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowLastColumn()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showLastColumn:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowRowStripes()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->showRowStripes:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isTotalRowShown()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalRowShown:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setHeaderRowBorderDxfId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowBorderDxfId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHeaderRowDxfId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowDxfId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHeaderRowShown(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->headerRowShown:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShowColumnStripes(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->showColumnStripes:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShowFirstColumn(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->showFirstColumn:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShowLastColumn(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->showLastColumn:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShowRowStripes(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->showRowStripes:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTableBorderDxfId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->tableBorderDxfId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTableReference(Lcom/intsig/office/ss/model/CellRangeAddress;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->ref:Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTotalRowShown(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalRowShown:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTotalsRowBorderDxfId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowBorderDxfId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTotalsRowDxfId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/table/SSTable;->totalsRowDxfId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

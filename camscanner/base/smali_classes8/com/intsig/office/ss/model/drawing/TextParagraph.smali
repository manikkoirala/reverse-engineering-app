.class public Lcom/intsig/office/ss/model/drawing/TextParagraph;
.super Ljava/lang/Object;
.source "TextParagraph.java"


# instance fields
.field private align:Lcom/intsig/office/ss/model/style/Alignment;

.field private font:Lcom/intsig/office/simpletext/font/Font;

.field private textRun:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/ss/model/style/Alignment;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->textRun:Ljava/lang/String;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/Alignment;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFont()Lcom/intsig/office/simpletext/font/Font;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHorizontalAlign()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getHorizontalAlign()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextRun()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->textRun:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getVerticalAlign()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getVerticalAlign()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setFont(Lcom/intsig/office/simpletext/font/Font;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHorizontalAlign(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTextRun(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->textRun:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVerticalAlign(S)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/TextParagraph;->align:Lcom/intsig/office/ss/model/style/Alignment;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/office/ss/model/style/CellStyle;
.super Ljava/lang/Object;
.source "CellStyle.java"


# static fields
.field public static final ALIGN_CENTER:S = 0x2s

.field public static final ALIGN_CENTER_SELECTION:S = 0x6s

.field public static final ALIGN_FILL:S = 0x4s

.field public static final ALIGN_GENERAL:S = 0x0s

.field public static final ALIGN_JUSTIFY:S = 0x5s

.field public static final ALIGN_LEFT:S = 0x1s

.field public static final ALIGN_RIGHT:S = 0x3s

.field public static final ALT_BARS:S = 0x3s

.field public static final BIG_SPOTS:S = 0x9s

.field public static final BORDER_DASHED:S = 0x3s

.field public static final BORDER_DASH_DOT:S = 0x9s

.field public static final BORDER_DASH_DOT_DOT:S = 0xbs

.field public static final BORDER_DOTTED:S = 0x7s

.field public static final BORDER_DOUBLE:S = 0x6s

.field public static final BORDER_HAIR:S = 0x4s

.field public static final BORDER_MEDIUM:S = 0x2s

.field public static final BORDER_MEDIUM_DASHED:S = 0x8s

.field public static final BORDER_MEDIUM_DASH_DOT:S = 0xas

.field public static final BORDER_MEDIUM_DASH_DOT_DOT:S = 0xcs

.field public static final BORDER_NONE:S = 0x0s

.field public static final BORDER_SLANTED_DASH_DOT:S = 0xds

.field public static final BORDER_THICK:S = 0x5s

.field public static final BORDER_THIN:S = 0x1s

.field public static final BRICKS:S = 0xas

.field public static final DIAMONDS:S = 0x10s

.field public static final FINE_DOTS:S = 0x2s

.field public static final LEAST_DOTS:S = 0x12s

.field public static final LESS_DOTS:S = 0x11s

.field public static final NO_FILL:S = 0x0s

.field public static final SOLID_FOREGROUND:S = 0x1s

.field public static final SPARSE_DOTS:S = 0x4s

.field public static final SQUARES:S = 0xfs

.field public static final THICK_BACKWARD_DIAG:S = 0x7s

.field public static final THICK_FORWARD_DIAG:S = 0x8s

.field public static final THICK_HORZ_BANDS:S = 0x5s

.field public static final THICK_VERT_BANDS:S = 0x6s

.field public static final THIN_BACKWARD_DIAG:S = 0xds

.field public static final THIN_FORWARD_DIAG:S = 0xes

.field public static final THIN_HORZ_BANDS:S = 0xbs

.field public static final THIN_VERT_BANDS:S = 0xcs

.field public static final VERTICAL_BOTTOM:S = 0x2s

.field public static final VERTICAL_CENTER:S = 0x1s

.field public static final VERTICAL_JUSTIFY:S = 0x3s

.field public static final VERTICAL_TOP:S


# instance fields
.field private alignment:Lcom/intsig/office/ss/model/style/Alignment;

.field private cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

.field private fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private fontIndex:S

.field private index:S

.field private isHidden:Z

.field private isLocked:Z

.field private numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private checkAlignmeng()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/model/style/Alignment;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/ss/model/style/Alignment;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private checkBorder()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/model/style/CellBorder;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/ss/model/style/CellBorder;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private checkDataFormat()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/ss/model/style/NumberFormat;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private checkFillPattern()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 11
    .line 12
    const/4 v1, -0x1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellBorder;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 14
    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/Alignment;->dispose()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBgColor()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getBackgoundColor()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderBottom()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getBottomBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getStyle()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderBottomColorIdx()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getBottomBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getColor()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderLeft()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getLeftBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getStyle()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderLeftColorIdx()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getLeftBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getColor()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderRight()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getRightBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getStyle()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderRightColorIdx()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getRightBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getColor()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderTop()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getTopBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getStyle()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorderTopColorIdx()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getTopBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/BorderStyle;->getColor()S

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFgColor()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFgColorIndex()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFgColorIndex()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFillPattern()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFillPatternType()B
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fontIndex:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFormatCode()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkDataFormat()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/NumberFormat;->getFormatCode()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHorizontalAlign()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getHorizontalAlign()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndent()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getIndent()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->index:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberFormatID()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkDataFormat()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/NumberFormat;->getNumberFormatID()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRotation()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getRotaion()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getVerticalAlign()S
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getVerticalAlign()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isHidden()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->isHidden:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isLocked()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->isLocked:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isWrapText()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->isWrapText()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getHorizontalAlign()S

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v1, 0x5

    .line 19
    if-eq v0, v1, :cond_1

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/Alignment;->getVerticalAlign()S

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v1, 0x3

    .line 28
    if-ne v0, v1, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v0, 0x0

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 34
    :goto_1
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public setBgColor(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setBackgoundColor(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorder(Lcom/intsig/office/ss/model/style/CellBorder;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderBottom(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getBottomBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setStyle(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderBottomColorIdx(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getBottomBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setColor(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderLeft(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getLeftBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setStyle(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderLeftColorIdx(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getLeftBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setColor(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderRight(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getRightBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setStyle(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderRightColorIdx(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getRightBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setColor(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderTop(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getTopBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setStyle(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBorderTopColorIdx(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkBorder()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->cellBorder:Lcom/intsig/office/ss/model/style/CellBorder;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellBorder;->getTopBorder()Lcom/intsig/office/ss/model/style/BorderStyle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/BorderStyle;->setColor(S)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFgColor(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFillPattern(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFillPatternType(B)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkFillPattern()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFontIndex(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->fontIndex:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFormatCode(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkDataFormat()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/NumberFormat;->setFormatCode(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHidden(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->isHidden:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHorizontalAlign(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    if-eqz p1, :cond_6

    const-string v0, "general"

    .line 2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "left"

    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    :cond_1
    const-string v0, "center"

    .line 5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    :cond_2
    const-string v0, "right"

    .line 7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    :cond_3
    const-string v0, "fill"

    .line 9
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    :cond_4
    const-string v0, "justify"

    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x5

    if-eqz v0, :cond_5

    .line 12
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    :cond_5
    const-string v0, "distributed"

    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 14
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    goto :goto_1

    .line 15
    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    :cond_7
    :goto_1
    return-void
.end method

.method public setHorizontalAlign(S)V
    .locals 1

    .line 16
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 17
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setHorizontalAlign(S)V

    return-void
.end method

.method public setIndent(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setIndent(S)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIndex(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->index:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLocked(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->isLocked:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNumberFormat(Lcom/intsig/office/ss/model/style/NumberFormat;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNumberFormatID(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkDataFormat()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->numFmt:Lcom/intsig/office/ss/model/style/NumberFormat;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/NumberFormat;->setNumberFormatID(S)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRotation(S)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setRotation(S)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVerticalAlign(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    const-string v0, "top"

    .line 2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    goto :goto_0

    :cond_0
    const-string v0, "center"

    .line 4
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    goto :goto_0

    :cond_1
    const-string v0, "bottom"

    .line 6
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    goto :goto_0

    :cond_2
    const-string v0, "justify"

    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_3

    .line 9
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    goto :goto_0

    :cond_3
    const-string v0, "distributed"

    .line 10
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 11
    iget-object p1, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    :cond_4
    :goto_0
    return-void
.end method

.method public setVerticalAlign(S)V
    .locals 1

    .line 12
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 13
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setVerticalAlign(S)V

    return-void
.end method

.method public setWrapText(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/style/CellStyle;->checkAlignmeng()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/style/CellStyle;->alignment:Lcom/intsig/office/ss/model/style/Alignment;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/style/Alignment;->setWrapText(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

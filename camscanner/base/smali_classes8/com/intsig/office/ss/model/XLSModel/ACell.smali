.class public Lcom/intsig/office/ss/model/XLSModel/ACell;
.super Lcom/intsig/office/ss/model/baseModel/Cell;
.source "ACell.java"


# instance fields
.field private record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/ss/model/XLSModel/ASheet;IS)V
    .locals 6

    const/4 p1, 0x5

    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 19
    iput-object p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 20
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getInternalSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getXFIndexForColAt(S)S

    move-result v5

    const/4 v1, 0x3

    const/4 v2, 0x0

    move-object v0, p0

    move v3, p3

    move v4, p4

    .line 21
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/ss/model/XLSModel/ACell;->setCellType(IZISS)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V
    .locals 2

    const/4 v0, 0x3

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 2
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 3
    invoke-static {p2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->determineType(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 4
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 5
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->rowNumber:I

    .line 6
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->colNumber:I

    .line 7
    invoke-interface {p2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v0

    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->styleIndex:I

    .line 8
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 p1, 0x2

    if-eq v0, p1, :cond_2

    const/4 p1, 0x4

    if-eq v0, p1, :cond_1

    const/4 p1, 0x5

    if-eq v0, p1, :cond_0

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getErrorCellValue()B

    move-result p1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getBooleanCellValue()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    goto :goto_0

    .line 11
    :cond_2
    check-cast p2, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    invoke-direct {p0, p2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->procellFormulaCellValue(Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;)V

    goto :goto_0

    .line 12
    :cond_3
    instance-of v0, p2, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    if-eqz v0, :cond_4

    .line 13
    check-cast p2, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;->getSSTIndex()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 14
    invoke-direct {p0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->processSST()V

    goto :goto_0

    .line 15
    :cond_4
    instance-of v0, p2, Lcom/intsig/office/fc/hssf/record/LabelRecord;

    if-eqz v0, :cond_6

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object p1

    check-cast p2, Lcom/intsig/office/fc/hssf/record/LabelRecord;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/LabelRecord;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(Ljava/lang/Object;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    goto :goto_0

    .line 17
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getNumericCellValue()D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    :cond_6
    :goto_0
    return-void
.end method

.method private static checkFormulaCachedValueType(ILcom/intsig/office/fc/hssf/record/FormulaRecord;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedResultType()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-ne p1, p0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    invoke-static {p0, p1, v0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->typeMismatch(IIZ)Ljava/lang/RuntimeException;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    throw p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static determineType(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I
    .locals 3

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 p0, 0x2

    .line 6
    return p0

    .line 7
    :cond_0
    move-object v0, p0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/hssf/record/Record;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/Record;->getSid()S

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/16 v2, 0xfd

    .line 15
    .line 16
    if-eq v1, v2, :cond_3

    .line 17
    .line 18
    const/16 v2, 0x201

    .line 19
    .line 20
    if-eq v1, v2, :cond_2

    .line 21
    .line 22
    packed-switch v1, :pswitch_data_0

    .line 23
    .line 24
    .line 25
    new-instance v0, Ljava/lang/RuntimeException;

    .line 26
    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v2, "Bad cell value rec ("

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p0

    .line 45
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string p0, ")"

    .line 49
    .line 50
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    throw v0

    .line 61
    :pswitch_0
    check-cast v0, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;->isBoolean()Z

    .line 64
    .line 65
    .line 66
    move-result p0

    .line 67
    if-eqz p0, :cond_1

    .line 68
    .line 69
    const/4 p0, 0x4

    .line 70
    goto :goto_0

    .line 71
    :cond_1
    const/4 p0, 0x5

    .line 72
    :goto_0
    return p0

    .line 73
    :pswitch_1
    const/4 p0, 0x0

    .line 74
    return p0

    .line 75
    :cond_2
    const/4 p0, 0x3

    .line 76
    return p0

    .line 77
    :cond_3
    :pswitch_2
    const/4 p0, 0x1

    .line 78
    return p0

    .line 79
    :pswitch_data_0
    .packed-switch 0x203
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private static getCellTypeName(I)Ljava/lang/String;
    .locals 2

    .line 1
    if-eqz p0, :cond_5

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eq p0, v0, :cond_4

    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    if-eq p0, v0, :cond_3

    .line 8
    .line 9
    const/4 v0, 0x3

    .line 10
    if-eq p0, v0, :cond_2

    .line 11
    .line 12
    const/4 v0, 0x4

    .line 13
    if-eq p0, v0, :cond_1

    .line 14
    .line 15
    const/4 v0, 0x5

    .line 16
    if-eq p0, v0, :cond_0

    .line 17
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v1, "#unknown cell type ("

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string p0, ")#"

    .line 32
    .line 33
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    return-object p0

    .line 41
    :cond_0
    const-string p0, "error"

    .line 42
    .line 43
    return-object p0

    .line 44
    :cond_1
    const-string p0, "boolean"

    .line 45
    .line 46
    return-object p0

    .line 47
    :cond_2
    const-string p0, "blank"

    .line 48
    .line 49
    return-object p0

    .line 50
    :cond_3
    const-string p0, "formula"

    .line 51
    .line 52
    return-object p0

    .line 53
    :cond_4
    const-string p0, "text"

    .line 54
    .line 55
    return-object p0

    .line 56
    :cond_5
    const-string p0, "numeric"

    .line 57
    .line 58
    return-object p0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private procellFormulaCellValue(Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getStringRecord()Lcom/intsig/office/fc/hssf/record/StringRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/StringRecord;->getString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(Ljava/lang/Object;)I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedResultType()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    int-to-short v0, v0

    .line 40
    iput-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 41
    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    const/4 v1, 0x4

    .line 45
    if-eq v0, v1, :cond_2

    .line 46
    .line 47
    const/4 v1, 0x5

    .line 48
    if-eq v0, v1, :cond_1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedErrorValue()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    int-to-byte p1, p1

    .line 56
    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedBooleanValue()Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getValue()D

    .line 75
    .line 76
    .line 77
    move-result-wide v0

    .line 78
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 83
    .line 84
    :goto_0
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processSST()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 8
    .line 9
    check-cast v1, Ljava/lang/Integer;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    instance-of v2, v1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 20
    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    check-cast v1, Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    .line 24
    .line 25
    invoke-static {v0, v1, p0}, Lcom/intsig/office/ss/util/SectionElementFactory;->getSectionElement(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/fc/hssf/record/common/UnicodeString;Lcom/intsig/office/ss/model/baseModel/Cell;)Lcom/intsig/office/simpletext/model/SectionElement;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(Ljava/lang/Object;)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private setCellType(IZISS)V
    .locals 1

    const/4 p2, 0x5

    if-gt p1, p2, :cond_c

    if-eqz p1, :cond_a

    const/4 v0, 0x1

    if-eq p1, v0, :cond_8

    const/4 v0, 0x2

    if-eq p1, v0, :cond_6

    const/4 v0, 0x3

    if-eq p1, v0, :cond_4

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    if-eq p1, p2, :cond_0

    goto/16 :goto_6

    .line 5
    :cond_0
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eq p1, p2, :cond_1

    .line 6
    new-instance p2, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;-><init>()V

    goto :goto_0

    .line 7
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 8
    :goto_0
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 9
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 10
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 11
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    goto/16 :goto_6

    .line 12
    :cond_2
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eq p1, p2, :cond_3

    .line 13
    new-instance p2, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;-><init>()V

    goto :goto_1

    .line 14
    :cond_3
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 15
    :goto_1
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 16
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 17
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 18
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    goto/16 :goto_6

    .line 19
    :cond_4
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eq p2, p1, :cond_5

    .line 20
    new-instance p2, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/BlankRecord;-><init>()V

    goto :goto_2

    .line 21
    :cond_5
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 22
    :goto_2
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setColumn(S)V

    .line 23
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setXFIndex(S)V

    .line 24
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setRow(I)V

    .line 25
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    goto :goto_6

    .line 26
    :cond_6
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eq p2, p1, :cond_7

    .line 27
    iget-object p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    check-cast p2, Lcom/intsig/office/ss/model/XLSModel/ASheet;

    invoke-virtual {p2}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getInternalSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;

    move-result-object p2

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRowsAggregate()Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;

    move-result-object p2

    invoke-virtual {p2, p3, p4}, Lcom/intsig/office/fc/hssf/record/aggregates/RowRecordsAggregate;->createFormula(II)Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    move-result-object p2

    goto :goto_3

    .line 28
    :cond_7
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 29
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->setRow(I)V

    .line 30
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->setColumn(S)V

    .line 31
    :goto_3
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->setXFIndex(S)V

    .line 32
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    goto :goto_6

    .line 33
    :cond_8
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-ne p1, p2, :cond_9

    .line 34
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    goto :goto_4

    .line 35
    :cond_9
    new-instance p2, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;-><init>()V

    .line 36
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 37
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 38
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 39
    :goto_4
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    goto :goto_6

    .line 40
    :cond_a
    iget-short p2, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eq p1, p2, :cond_b

    .line 41
    new-instance p2, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    invoke-direct {p2}, Lcom/intsig/office/fc/hssf/record/NumberRecord;-><init>()V

    goto :goto_5

    .line 42
    :cond_b
    iget-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast p2, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 43
    :goto_5
    invoke-virtual {p2, p4}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setColumn(S)V

    .line 44
    invoke-virtual {p2, p5}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setXFIndex(S)V

    .line 45
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/hssf/record/CellRecord;->setRow(I)V

    .line 46
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    :goto_6
    int-to-short p1, p1

    .line 47
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    return-void

    .line 48
    :cond_c
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "I have no idea what type that is!"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static typeMismatch(IIZ)Ljava/lang/RuntimeException;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Cannot get a "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-static {p0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getCellTypeName(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string p0, " value from a "

    .line 19
    .line 20
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-static {p1}, Lcom/intsig/office/ss/model/XLSModel/ACell;->getCellTypeName(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string p0, " "

    .line 31
    .line 32
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    if-eqz p2, :cond_0

    .line 36
    .line 37
    const-string p0, "formula "

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const-string p0, ""

    .line 41
    .line 42
    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string p0, "cell"

    .line 46
    .line 47
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 55
    .line 56
    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-object p1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/ss/model/baseModel/Cell;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBooleanCellValue()Z
    .locals 4

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    const/4 v2, 0x4

    .line 5
    if-eq v0, v1, :cond_2

    .line 6
    .line 7
    const/4 v1, 0x3

    .line 8
    const/4 v3, 0x0

    .line 9
    if-eq v0, v1, :cond_1

    .line 10
    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 14
    .line 15
    check-cast v0, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;->getBooleanValue()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0

    .line 22
    :cond_0
    invoke-static {v2, v0, v3}, Lcom/intsig/office/ss/model/XLSModel/ACell;->typeMismatch(IIZ)Ljava/lang/RuntimeException;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    throw v0

    .line 27
    :cond_1
    return v3

    .line 28
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 29
    .line 30
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v2, v0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->checkFormulaCachedValueType(ILcom/intsig/office/fc/hssf/record/FormulaRecord;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedBooleanValue()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    return v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getCellValueRecord()Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getErrorCellValue()B
    .locals 3

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    const/4 v2, 0x5

    .line 5
    if-eq v0, v1, :cond_1

    .line 6
    .line 7
    if-ne v0, v2, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 10
    .line 11
    check-cast v0, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;->getErrorValue()B

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0

    .line 18
    :cond_0
    const/4 v1, 0x0

    .line 19
    invoke-static {v2, v0, v1}, Lcom/intsig/office/ss/model/XLSModel/ACell;->typeMismatch(IIZ)Ljava/lang/RuntimeException;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    throw v0

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 25
    .line 26
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v2, v0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->checkFormulaCachedValueType(ILcom/intsig/office/fc/hssf/record/FormulaRecord;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedErrorValue()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    int-to-byte v0, v0

    .line 40
    return v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getFormulaCachedValueType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getCachedResultType()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumericCellValue()D
    .locals 3

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eq v0, v1, :cond_1

    .line 8
    .line 9
    const/4 v1, 0x3

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    const-wide/16 v0, 0x0

    .line 13
    .line 14
    return-wide v0

    .line 15
    :cond_0
    invoke-static {v2, v0, v2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->typeMismatch(IIZ)Ljava/lang/RuntimeException;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    throw v0

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 21
    .line 22
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {v2, v0}, Lcom/intsig/office/ss/model/XLSModel/ACell;->checkFormulaCachedValueType(ILcom/intsig/office/fc/hssf/record/FormulaRecord;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->getValue()D

    .line 32
    .line 33
    .line 34
    move-result-wide v0

    .line 35
    return-wide v0

    .line 36
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 37
    .line 38
    check-cast v0, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/NumberRecord;->getValue()D

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    return-wide v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getStringCellValue()Ljava/lang/String;
    .locals 3

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x2

    .line 7
    if-eq v0, v2, :cond_2

    .line 8
    .line 9
    const/4 v2, 0x3

    .line 10
    if-ne v0, v2, :cond_0

    .line 11
    .line 12
    const-string v0, ""

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    const/4 v2, 0x0

    .line 16
    invoke-static {v1, v0, v2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->typeMismatch(IIZ)Ljava/lang/RuntimeException;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    throw v0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 22
    .line 23
    instance-of v0, v0, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 34
    .line 35
    check-cast v1, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;->getSSTIndex()I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 47
    .line 48
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-static {v1, v2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->checkFormulaCachedValueType(ILcom/intsig/office/fc/hssf/record/FormulaRecord;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getStringValue()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public setCellErrorValue(B)V
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 8
    .line 9
    check-cast v0, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;->setValue(B)V

    .line 12
    .line 13
    .line 14
    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 19
    .line 20
    :goto_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCellFormula([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 4
    .line 5
    .line 6
    move-result v4

    .line 7
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 10
    .line 11
    .line 12
    move-result v5

    .line 13
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    .line 16
    .line 17
    .line 18
    move-result v6

    .line 19
    const/4 v2, 0x2

    .line 20
    const/4 v3, 0x0

    .line 21
    move-object v1, p0

    .line 22
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/model/XLSModel/ACell;->setCellType(IZISS)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 26
    .line 27
    check-cast v0, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getFormulaRecord()Lcom/intsig/office/fc/hssf/record/FormulaRecord;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->setOptions(S)V

    .line 34
    .line 35
    .line 36
    const-wide/16 v2, 0x0

    .line 37
    .line 38
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/fc/hssf/record/FormulaRecord;->setValue(D)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->getXFIndex()S

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-nez v1, :cond_0

    .line 46
    .line 47
    const/16 v1, 0xf

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->setXFIndex(S)V

    .line 50
    .line 51
    .line 52
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/aggregates/FormulaRecordAggregate;->setParsedExpression([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setCellType(IZ)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v4

    .line 2
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v5

    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v6

    move-object v1, p0

    move v2, p1

    move v3, p2

    .line 4
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/model/XLSModel/ACell;->setCellType(IZISS)V

    return-void
.end method

.method public setCellValue(D)V
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    double-to-float p1, p1

    .line 2
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    goto :goto_0

    .line 3
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast v0, Lcom/intsig/office/fc/hssf/record/NumberRecord;

    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hssf/record/NumberRecord;->setValue(D)V

    .line 4
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setCellValue(Ljava/lang/String;)V
    .locals 7

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    .line 9
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    move-result v4

    .line 10
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    move-result v5

    .line 11
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    invoke-interface {v0}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    move-result v6

    if-nez p1, :cond_1

    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object v1, p0

    .line 12
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/model/XLSModel/ACell;->setCellType(IZISS)V

    return-void

    .line 13
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v0

    sget-object v1, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->EXCEL97:Lcom/intsig/office/fc/ss/SpreadsheetVersion;

    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/SpreadsheetVersion;->getMaxTextLength()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;->getUnicodeString()Lcom/intsig/office/fc/hssf/record/common/UnicodeString;

    move-result-object p1

    .line 15
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->addSSTString(Lcom/intsig/office/fc/hssf/record/common/UnicodeString;)I

    move-result p1

    .line 16
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast v0, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/LabelSSTRecord;->setSSTIndex(I)V

    .line 17
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    return-void

    .line 18
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The maximum length of cell contents (text) is 32,767 characters"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setCellValue(Z)V
    .locals 2

    .line 5
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ACell;->record:Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    check-cast v0, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hssf/record/BoolErrRecord;->setValue(Z)V

    .line 7
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    :goto_0
    return-void
.end method

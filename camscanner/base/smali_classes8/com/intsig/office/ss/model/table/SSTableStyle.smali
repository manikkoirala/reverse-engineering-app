.class public Lcom/intsig/office/ss/model/table/SSTableStyle;
.super Ljava/lang/Object;
.source "SSTableStyle.java"


# instance fields
.field private band1H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private band1V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private band2H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private band2V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private firstCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private firstRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private lastCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

.field private lastRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getBand1H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band1H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBand1V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band1V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBand2H()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band2H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBand2V()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band2V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFirstCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->firstCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFirstRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->firstRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLastCol()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->lastCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLastRow()Lcom/intsig/office/ss/model/table/SSTableCellStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->lastRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setBand1H(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band1H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBand1V(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band1V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBand2H(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band2H:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBand2V(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->band2V:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFirstCol(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->firstCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFirstRow(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->firstRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLastCol(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->lastCol:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLastRow(Lcom/intsig/office/ss/model/table/SSTableCellStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/table/SSTableStyle;->lastRow:Lcom/intsig/office/ss/model/table/SSTableCellStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

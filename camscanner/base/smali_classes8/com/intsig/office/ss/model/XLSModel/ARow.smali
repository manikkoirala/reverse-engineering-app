.class public Lcom/intsig/office/ss/model/XLSModel/ARow;
.super Lcom/intsig/office/ss/model/baseModel/Row;
.source "ARow.java"


# static fields
.field public static final INITIAL_CAPACITY:I = 0x5


# direct methods
.method public constructor <init>(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V
    .locals 3

    .line 1
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getLastCol()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getFirstCol()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sub-int/2addr v0, v1

    .line 10
    add-int/lit8 v0, v0, 0x5

    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Row;-><init>(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->setEmpty()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Row;->rowNumber:I

    .line 26
    .line 27
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getFirstCol()I

    .line 28
    .line 29
    .line 30
    move-result p2

    .line 31
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Row;->firstCol:I

    .line 32
    .line 33
    iget p2, p0, Lcom/intsig/office/ss/model/baseModel/Row;->lastCol:I

    .line 34
    .line 35
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getLastCol()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Row;->lastCol:I

    .line 44
    .line 45
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getXFIndex()S

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Row;->styleIndex:I

    .line 50
    .line 51
    const/4 p2, 0x0

    .line 52
    :goto_0
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Row;->styleIndex:I

    .line 53
    .line 54
    const v1, 0xffff

    .line 55
    .line 56
    .line 57
    shr-int/2addr v1, p2

    .line 58
    and-int/2addr v0, v1

    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getNumStyles()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    if-le v0, v2, :cond_0

    .line 64
    .line 65
    add-int/lit8 p2, p2, 0x1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    iget p1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->styleIndex:I

    .line 69
    .line 70
    and-int/2addr p1, v1

    .line 71
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->styleIndex:I

    .line 72
    .line 73
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getZeroHeight()Z

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->setZeroHeight(Z)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getHeight()S

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    const p2, 0x8000

    .line 85
    .line 86
    .line 87
    and-int/2addr p2, p1

    .line 88
    if-eqz p2, :cond_1

    .line 89
    .line 90
    const/16 p1, 0xff

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_1
    and-int/lit16 p1, p1, 0x7fff

    .line 94
    .line 95
    int-to-short p1, p1

    .line 96
    :goto_1
    div-int/lit8 p1, p1, 0x14

    .line 97
    .line 98
    int-to-float p1, p1

    .line 99
    const p2, 0x3faaaaab

    .line 100
    .line 101
    .line 102
    mul-float p1, p1, p2

    .line 103
    .line 104
    float-to-int p1, p1

    .line 105
    int-to-float p1, p1

    .line 106
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowPixelHeight(F)V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private isValidateCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Z
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/office/ss/model/XLSModel/ACell;->determineType(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x3

    .line 6
    const/4 v2, 0x1

    .line 7
    if-eq v0, v1, :cond_0

    .line 8
    .line 9
    return v2

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Row;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getXFIndex()S

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-nez v1, :cond_2

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowStyle()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-static {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 45
    .line 46
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnStyle(I)I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-eqz p1, :cond_1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const/4 v2, 0x0

    .line 66
    :cond_2
    :goto_0
    return v2
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public cellIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/ss/model/baseModel/Cell;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Row;->cells:Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method createCellFromRecord(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Lcom/intsig/office/ss/model/XLSModel/ACell;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Row;->cells:Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 20
    .line 21
    return-object v0

    .line 22
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ARow;->isValidateCell(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_3

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 31
    .line 32
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/ss/model/XLSModel/ACell;-><init>(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 33
    .line 34
    .line 35
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getColumn()S

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->firstCol:I

    .line 40
    .line 41
    if-ge p1, v1, :cond_1

    .line 42
    .line 43
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->firstCol:I

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->lastCol:I

    .line 47
    .line 48
    if-le p1, v1, :cond_2

    .line 49
    .line 50
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Row;->lastCol:I

    .line 51
    .line 52
    :cond_2
    :goto_0
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->addCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 53
    .line 54
    .line 55
    return-object v0

    .line 56
    :cond_3
    const/4 p1, 0x0

    .line 57
    return-object p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

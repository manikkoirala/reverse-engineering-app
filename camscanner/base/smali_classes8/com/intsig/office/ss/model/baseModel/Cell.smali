.class public Lcom/intsig/office/ss/model/baseModel/Cell;
.super Ljava/lang/Object;
.source "Cell.java"


# static fields
.field private static CALENDAR:Ljava/util/Calendar; = null

.field public static final CELL_TYPE_BLANK:S = 0x3s

.field public static final CELL_TYPE_BOOLEAN:S = 0x4s

.field public static final CELL_TYPE_ERROR:S = 0x5s

.field public static final CELL_TYPE_FORMULA:S = 0x2s

.field public static final CELL_TYPE_NUMERIC:S = 0x0s

.field public static final CELL_TYPE_NUMERIC_ACCOUNTING:S = 0x8s

.field public static final CELL_TYPE_NUMERIC_DECIMAL:S = 0x7s

.field public static final CELL_TYPE_NUMERIC_FRACTIONAL:S = 0x9s

.field public static final CELL_TYPE_NUMERIC_GENERAL:S = 0x6s

.field public static final CELL_TYPE_NUMERIC_SIMPLEDATE:S = 0xas

.field public static final CELL_TYPE_NUMERIC_STRING:S = 0xbs

.field public static final CELL_TYPE_STRING:S = 0x1s

.field private static final DAY_MILLISECONDS:J = 0x5265c00L

.field private static final HOURS_PER_DAY:I = 0x18

.field private static final MINUTES_PER_HOUR:I = 0x3c

.field private static final SECONDS_PER_DAY:I = 0x15180

.field private static final SECONDS_PER_MINUTE:I = 0x3c


# instance fields
.field protected cellType:S

.field protected colNumber:I

.field private prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

.field protected rowNumber:I

.field protected sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

.field protected styleIndex:I

.field protected value:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/GregorianCalendar;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/ss/model/baseModel/Cell;->CALENDAR:Ljava/util/Calendar;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(S)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 7
    .line 8
    invoke-direct {p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBooleanValue()Z
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    check-cast v0, Ljava/lang/Boolean;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCellFormulaValue()Ljava/lang/String;
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    check-cast v0, Ljava/lang/String;

    .line 11
    .line 12
    return-object v0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCellNumericType()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getCellNumericType()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->styleIndex:I

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCellType()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->colNumber:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDateCellValue(Z)Ljava/util/Date;
    .locals 10

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    if-nez v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    check-cast v0, Ljava/lang/Double;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide v2

    .line 19
    double-to-int v2, v2

    .line 20
    int-to-double v3, v2

    .line 21
    sub-double/2addr v0, v3

    .line 22
    const-wide v3, 0x4194997000000000L    # 8.64E7

    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    mul-double v0, v0, v3

    .line 28
    .line 29
    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    .line 30
    .line 31
    add-double/2addr v0, v3

    .line 32
    double-to-int v0, v0

    .line 33
    if-eqz p1, :cond_0

    .line 34
    .line 35
    const/16 v1, 0x770

    .line 36
    .line 37
    const/16 v4, 0x770

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const/16 v1, 0x76c

    .line 41
    .line 42
    const/16 v4, 0x76c

    .line 43
    .line 44
    :goto_0
    if-eqz p1, :cond_1

    .line 45
    .line 46
    const/4 p1, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    const/16 p1, 0x3d

    .line 49
    .line 50
    if-ge v2, p1, :cond_2

    .line 51
    .line 52
    const/4 p1, 0x0

    .line 53
    goto :goto_1

    .line 54
    :cond_2
    const/4 p1, -0x1

    .line 55
    :goto_1
    sget-object v1, Lcom/intsig/office/ss/model/baseModel/Cell;->CALENDAR:Ljava/util/Calendar;

    .line 56
    .line 57
    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    .line 58
    .line 59
    .line 60
    sget-object v3, Lcom/intsig/office/ss/model/baseModel/Cell;->CALENDAR:Ljava/util/Calendar;

    .line 61
    .line 62
    const/4 v5, 0x0

    .line 63
    add-int v6, v2, p1

    .line 64
    .line 65
    const/4 v7, 0x0

    .line 66
    const/4 v8, 0x0

    .line 67
    const/4 v9, 0x0

    .line 68
    invoke-virtual/range {v3 .. v9}, Ljava/util/Calendar;->set(IIIIII)V

    .line 69
    .line 70
    .line 71
    sget-object p1, Lcom/intsig/office/ss/model/baseModel/Cell;->CALENDAR:Ljava/util/Calendar;

    .line 72
    .line 73
    const/16 v1, 0xe

    .line 74
    .line 75
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 76
    .line 77
    .line 78
    sget-object p1, Lcom/intsig/office/ss/model/baseModel/Cell;->CALENDAR:Ljava/util/Calendar;

    .line 79
    .line 80
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    return-object p1

    .line 85
    :cond_3
    const/4 p1, 0x0

    .line 86
    return-object p1
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getErrorCodeValue()B
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    check-cast v0, Ljava/lang/Byte;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0

    .line 17
    :cond_0
    const/16 v0, -0x80

    .line 18
    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getErrorValue()I
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    check-cast v0, Ljava/lang/Byte;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0

    .line 17
    :cond_0
    const/4 v0, -0x1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getExpandedRangeAddressIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getExpandCellRangeAddressIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHyperLink()Lcom/intsig/office/common/hyperlink/Hyperlink;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getCellHyperlink()Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberValue()D
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    check-cast v0, Ljava/lang/Double;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    return-wide v0

    .line 16
    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # Double.NaN

    .line 17
    .line 18
    return-wide v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRangeAddressIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getCellMergeRangeAddressIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRowNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->rowNumber:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSTRoot()Lcom/intsig/office/simpletext/view/STRoot;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getCellSTRoot()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSTRoot(I)Lcom/intsig/office/simpletext/view/STRoot;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStringCellValueIndex()I
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    check-cast v0, Ljava/lang/Integer;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0

    .line 17
    :cond_0
    const/4 v0, -0x1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableInfo()Lcom/intsig/office/ss/model/table/SSTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->getTableInfo()Lcom/intsig/office/ss/model/table/SSTable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hasValidValue()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeSTRoot()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->removeCellSTRoot()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setCellNumericType(S)V
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCellStyle(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->styleIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCellType(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->cellType:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCellValue(Ljava/lang/Object;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->value:Ljava/lang/Object;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setColNumber(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->colNumber:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setExpandedRangeAddressIndex(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHyperLink(Lcom/intsig/office/common/hyperlink/Hyperlink;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRangeAddressIndex(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRowNumber(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->rowNumber:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getState()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x2

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 13
    .line 14
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const/4 v1, 0x4

    .line 23
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTableInfo(Lcom/intsig/office/ss/model/table/SSTable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Cell;->prop:Lcom/intsig/office/ss/model/baseModel/CellProperty;

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/ss/model/baseModel/CellProperty;->setCellProp(SLjava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

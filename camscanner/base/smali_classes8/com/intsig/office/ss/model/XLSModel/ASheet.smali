.class public Lcom/intsig/office/ss/model/XLSModel/ASheet;
.super Lcom/intsig/office/ss/model/baseModel/Sheet;
.source "ASheet.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/Sheet;


# instance fields
.field private initRowFinished:Z

.field private sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/fc/hssf/model/InternalSheet;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p2, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNumMergedRegions()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-ge v0, p1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getMergedRegionAt(I)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    new-instance v2, Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/intsig/office/ss/model/CellRangeAddress;-><init>(IIII)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addMergeRange(Lcom/intsig/office/ss/model/CellRangeAddress;)I

    .line 41
    .line 42
    .line 43
    add-int/lit8 v0, v0, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getPaneInformation()Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    if-eqz p1, :cond_1

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;->getHorizontalSplitTopRow()S

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;->getVerticalSplitLeftColumn()S

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/util/HSSFPaneInformation;->isFreezePane()Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;-><init>(SSZ)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->setPaneInformation(Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;)V

    .line 70
    .line 71
    .line 72
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getColumnInfo()Ljava/util/List;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    if-eqz p1, :cond_2

    .line 77
    .line 78
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    if-eqz p2, :cond_2

    .line 87
    .line 88
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    check-cast p2, Lcom/intsig/office/fc/hssf/util/ColumnInfo;

    .line 93
    .line 94
    new-instance v6, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 95
    .line 96
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;->getFirstCol()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;->getLastCol()I

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;->getColWidth()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    int-to-double v3, v0

    .line 109
    const-wide/high16 v7, 0x4070000000000000L    # 256.0

    .line 110
    .line 111
    div-double/2addr v3, v7

    .line 112
    const-wide/high16 v7, 0x4018000000000000L    # 6.0

    .line 113
    .line 114
    mul-double v3, v3, v7

    .line 115
    .line 116
    const-wide v7, 0x3ff5555560000000L    # 1.3333333730697632

    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    mul-double v3, v3, v7

    .line 122
    .line 123
    double-to-int v0, v3

    .line 124
    int-to-float v3, v0

    .line 125
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;->getStyle()I

    .line 126
    .line 127
    .line 128
    move-result v4

    .line 129
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/util/ColumnInfo;->isHidden()Z

    .line 130
    .line 131
    .line 132
    move-result v5

    .line 133
    move-object v0, v6

    .line 134
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {p0, v6}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addColumnInfo(Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;)V

    .line 138
    .line 139
    .line 140
    goto :goto_1

    .line 141
    :cond_2
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private ClientAnchorToTwoCellAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;)Lcom/intsig/office/ss/model/drawing/CellAnchor;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setColumn(S)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setRow(I)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setRow(I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setColumn(S)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol1()S

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx1()I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    int-to-float v3, v3

    .line 52
    const/high16 v4, 0x44800000    # 1024.0f

    .line 53
    .line 54
    div-float/2addr v3, v4

    .line 55
    mul-float v3, v3, v2

    .line 56
    .line 57
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDX(I)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getCol2()S

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnPixelWidth(I)F

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx2()I

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    int-to-float v3, v3

    .line 77
    div-float/2addr v3, v4

    .line 78
    mul-float v3, v3, v2

    .line 79
    .line 80
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    invoke-virtual {v1, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDX(I)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow1()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 92
    .line 93
    .line 94
    move-result-object v2

    .line 95
    if-nez v2, :cond_0

    .line 96
    .line 97
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 98
    .line 99
    .line 100
    move-result v2

    .line 101
    int-to-float v2, v2

    .line 102
    goto :goto_0

    .line 103
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy1()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    int-to-float v3, v3

    .line 112
    const/high16 v4, 0x43800000    # 256.0f

    .line 113
    .line 114
    div-float/2addr v3, v4

    .line 115
    mul-float v3, v3, v2

    .line 116
    .line 117
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 118
    .line 119
    .line 120
    move-result v2

    .line 121
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDY(I)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;->getRow2()I

    .line 125
    .line 126
    .line 127
    move-result v2

    .line 128
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    if-nez v2, :cond_1

    .line 133
    .line 134
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getDefaultRowHeight()I

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    int-to-float v2, v2

    .line 139
    goto :goto_1

    .line 140
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 141
    .line 142
    .line 143
    move-result v2

    .line 144
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy2()I

    .line 145
    .line 146
    .line 147
    move-result p1

    .line 148
    int-to-float p1, p1

    .line 149
    div-float/2addr p1, v4

    .line 150
    mul-float p1, p1, v2

    .line 151
    .line 152
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDY(I)V

    .line 157
    .line 158
    .line 159
    new-instance p1, Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 160
    .line 161
    const/4 v2, 0x1

    .line 162
    invoke-direct {p1, v2}, Lcom/intsig/office/ss/model/drawing/CellAnchor;-><init>(S)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setStart(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {p1, v1}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setEnd(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V

    .line 169
    .line 170
    .line 171
    return-object p1
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isGradientTile()Z

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 11
    .line 12
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 13
    .line 14
    invoke-virtual {p1, v0, p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getGradientTileBackground(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    return-object p1

    .line 19
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFillType()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/4 v2, 0x3

    .line 24
    if-ne v1, v2, :cond_1

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getBGPictureData()[B

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    new-instance v0, Lcom/intsig/office/common/picture/Picture;

    .line 33
    .line 34
    invoke-direct {v0}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/picture/Picture;->setData([B)V

    .line 38
    .line 39
    .line 40
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    new-instance p2, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 53
    .line 54
    invoke-direct {p2}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p2, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setPictureIndex(I)V

    .line 61
    .line 62
    .line 63
    move-object v0, p2

    .line 64
    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 66
    .line 67
    invoke-direct {v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 68
    .line 69
    .line 70
    const/4 p2, 0x0

    .line 71
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFillColor()I

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 79
    .line 80
    .line 81
    :cond_2
    :goto_0
    return-object v0
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private createRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/ss/model/XLSModel/ARow;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    new-instance v0, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 17
    .line 18
    invoke-direct {v0, v1, p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ARow;-><init>(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private createValidateRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/ss/model/XLSModel/ARow;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getRowNumber()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->isValidateRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    new-instance v0, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 23
    .line 24
    invoke-direct {v0, v1, p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ARow;-><init>(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 28
    .line 29
    .line 30
    return-object v0

    .line 31
    :cond_1
    const/4 p1, 0x0

    .line 32
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isValidateRow(Lcom/intsig/office/fc/hssf/record/RowRecord;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getFirstCol()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getLastCol()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    if-ne v0, v1, :cond_3

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getHeight()S

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/16 v1, 0xff

    .line 17
    .line 18
    if-eq v0, v1, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/record/RowRecord;->getXFIndex()S

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getNumStyles()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-le p1, v0, :cond_1

    .line 32
    .line 33
    and-int/lit16 p1, p1, 0xff

    .line 34
    .line 35
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-static {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_2

    .line 46
    .line 47
    return v2

    .line 48
    :cond_2
    const/4 p1, 0x0

    .line 49
    return p1

    .line 50
    :cond_3
    :goto_0
    return v2
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processHyperlinkfromSheet(Lcom/intsig/office/fc/hssf/model/InternalSheet;)V
    .locals 5

    .line 1
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getRecords()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_6

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Lcom/intsig/office/fc/hssf/record/RecordBase;

    .line 20
    .line 21
    instance-of v1, v0, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;

    .line 22
    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    check-cast v0, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;

    .line 26
    .line 27
    new-instance v1, Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 28
    .line 29
    invoke-direct {v1}, Lcom/intsig/office/common/hyperlink/Hyperlink;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->isFileLink()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_1

    .line 37
    .line 38
    const/4 v2, 0x4

    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setLinkType(I)V

    .line 40
    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->isDocumentLink()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_2

    .line 48
    .line 49
    const/4 v2, 0x2

    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setLinkType(I)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getAddress()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    if-eqz v2, :cond_3

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getAddress()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    const-string v3, "mailto:"

    .line 65
    .line 66
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-eqz v2, :cond_3

    .line 71
    .line 72
    const/4 v2, 0x3

    .line 73
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setLinkType(I)V

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_3
    const/4 v2, 0x1

    .line 78
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setLinkType(I)V

    .line 79
    .line 80
    .line 81
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getAddress()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setAddress(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getLabel()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->setTitle(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstRow()I

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {p0, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    if-nez v2, :cond_4

    .line 104
    .line 105
    new-instance v2, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 106
    .line 107
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstRow()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    invoke-direct {v2, v3}, Lcom/intsig/office/fc/hssf/record/RowRecord;-><init>(I)V

    .line 112
    .line 113
    .line 114
    new-instance v3, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 115
    .line 116
    iget-object v4, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 117
    .line 118
    invoke-direct {v3, v4, p0, v2}, Lcom/intsig/office/ss/model/XLSModel/ARow;-><init>(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 119
    .line 120
    .line 121
    const/high16 v2, 0x41900000    # 18.0f

    .line 122
    .line 123
    invoke-virtual {v3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowPixelHeight(F)V

    .line 124
    .line 125
    .line 126
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstRow()I

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 133
    .line 134
    .line 135
    move-result-object v4

    .line 136
    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    .line 138
    .line 139
    move-object v2, v3

    .line 140
    :cond_4
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstColumn()I

    .line 141
    .line 142
    .line 143
    move-result v3

    .line 144
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 145
    .line 146
    .line 147
    move-result-object v3

    .line 148
    if-nez v3, :cond_5

    .line 149
    .line 150
    new-instance v3, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 151
    .line 152
    invoke-direct {v3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;-><init>()V

    .line 153
    .line 154
    .line 155
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstRow()I

    .line 156
    .line 157
    .line 158
    move-result v4

    .line 159
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setRow(I)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/HyperlinkRecord;->getFirstColumn()I

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    int-to-short v0, v0

    .line 167
    invoke-virtual {v3, v0}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setColumn(S)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowStyle()I

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    int-to-short v0, v0

    .line 175
    invoke-virtual {v3, v0}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setXFIndex(S)V

    .line 176
    .line 177
    .line 178
    new-instance v0, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 179
    .line 180
    invoke-direct {v0, p0, v3}, Lcom/intsig/office/ss/model/XLSModel/ACell;-><init>(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->addCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 184
    .line 185
    .line 186
    move-object v3, v0

    .line 187
    :cond_5
    invoke-virtual {v3, v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setHyperLink(Lcom/intsig/office/common/hyperlink/Hyperlink;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    .line 189
    .line 190
    goto/16 :goto_0

    .line 191
    .line 192
    :catch_0
    :cond_6
    return-void
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private processMergedCells()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRangeCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    if-ge v1, v0, :cond_5

    .line 7
    .line 8
    invoke-virtual {p0, v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 17
    .line 18
    .line 19
    move-result v4

    .line 20
    sub-int/2addr v3, v4

    .line 21
    const v4, 0xffff

    .line 22
    .line 23
    .line 24
    if-eq v3, v4, :cond_4

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    sub-int/2addr v3, v4

    .line 35
    const/16 v4, 0xff

    .line 36
    .line 37
    if-ne v3, v4, :cond_0

    .line 38
    .line 39
    goto :goto_3

    .line 40
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    :goto_1
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastRow()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    if-gt v3, v4, :cond_4

    .line 49
    .line 50
    invoke-virtual {p0, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-nez v4, :cond_1

    .line 55
    .line 56
    new-instance v4, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 57
    .line 58
    invoke-direct {v4, v3}, Lcom/intsig/office/fc/hssf/record/RowRecord;-><init>(I)V

    .line 59
    .line 60
    .line 61
    new-instance v5, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 62
    .line 63
    iget-object v6, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 64
    .line 65
    invoke-direct {v5, v6, p0, v4}, Lcom/intsig/office/ss/model/XLSModel/ARow;-><init>(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/RowRecord;)V

    .line 66
    .line 67
    .line 68
    const/high16 v4, 0x41900000    # 18.0f

    .line 69
    .line 70
    invoke-virtual {v5, v4}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowPixelHeight(F)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, v5}, Lcom/intsig/office/ss/model/baseModel/Sheet;->addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V

    .line 74
    .line 75
    .line 76
    move-object v4, v5

    .line 77
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 78
    .line 79
    .line 80
    move-result v5

    .line 81
    :goto_2
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->getLastColumn()I

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    if-gt v5, v6, :cond_3

    .line 86
    .line 87
    invoke-virtual {v4, v5}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 88
    .line 89
    .line 90
    move-result-object v6

    .line 91
    if-nez v6, :cond_2

    .line 92
    .line 93
    new-instance v6, Lcom/intsig/office/fc/hssf/record/BlankRecord;

    .line 94
    .line 95
    invoke-direct {v6}, Lcom/intsig/office/fc/hssf/record/BlankRecord;-><init>()V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v6, v3}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setRow(I)V

    .line 99
    .line 100
    .line 101
    int-to-short v7, v5

    .line 102
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setColumn(S)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowStyle()I

    .line 106
    .line 107
    .line 108
    move-result v7

    .line 109
    int-to-short v7, v7

    .line 110
    invoke-virtual {v6, v7}, Lcom/intsig/office/fc/hssf/record/BlankRecord;->setXFIndex(S)V

    .line 111
    .line 112
    .line 113
    new-instance v7, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 114
    .line 115
    invoke-direct {v7, p0, v6}, Lcom/intsig/office/ss/model/XLSModel/ACell;-><init>(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v4, v7}, Lcom/intsig/office/ss/model/baseModel/Row;->addCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V

    .line 119
    .line 120
    .line 121
    move-object v6, v7

    .line 122
    :cond_2
    invoke-virtual {v6, v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setRangeAddressIndex(I)V

    .line 123
    .line 124
    .line 125
    add-int/lit8 v5, v5, 0x1

    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 129
    .line 130
    goto :goto_1

    .line 131
    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 132
    .line 133
    goto :goto_0

    .line 134
    :cond_5
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private processRowsAndCells(Lcom/intsig/office/fc/hssf/model/InternalSheet;Lcom/intsig/office/system/AbstractReader;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNextRow()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :goto_0
    const-string v1, "abort Reader"

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    invoke-direct {p0, v0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->createValidateRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getNextRow()Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    .line 24
    .line 25
    invoke-direct {p1, v1}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1

    .line 29
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getCellValueIterator()Ljava/util/Iterator;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const/4 v0, 0x0

    .line 34
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_6

    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-nez v2, :cond_5

    .line 45
    .line 46
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    check-cast v2, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;

    .line 51
    .line 52
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    .line 53
    .line 54
    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    invoke-interface {v2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    if-eq v3, v4, :cond_2

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_2
    move-object v3, v0

    .line 69
    goto :goto_3

    .line 70
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->completed()V

    .line 73
    .line 74
    .line 75
    :cond_4
    invoke-interface {v2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 84
    .line 85
    if-nez v0, :cond_2

    .line 86
    .line 87
    new-instance v3, Lcom/intsig/office/fc/hssf/record/RowRecord;

    .line 88
    .line 89
    invoke-interface {v2}, Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;->getRow()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/hssf/record/RowRecord;-><init>(I)V

    .line 94
    .line 95
    .line 96
    invoke-direct {p0, v3}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->createRowFromRecord(Lcom/intsig/office/fc/hssf/record/RowRecord;)Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    move-object v5, v3

    .line 101
    move-object v3, v0

    .line 102
    move-object v0, v5

    .line 103
    :goto_3
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/XLSModel/ARow;->createCellFromRecord(Lcom/intsig/office/fc/hssf/record/CellValueRecordInterface;)Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 104
    .line 105
    .line 106
    move-object v0, v3

    .line 107
    goto :goto_1

    .line 108
    :cond_5
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    .line 109
    .line 110
    invoke-direct {p1, v1}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    throw p1

    .line 114
    :cond_6
    if-eqz v0, :cond_7

    .line 115
    .line 116
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->completed()V

    .line 117
    .line 118
    .line 119
    :cond_7
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_5

    .line 6
    .line 7
    if-nez p2, :cond_1

    .line 8
    .line 9
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 10
    .line 11
    .line 12
    move-result-object p3

    .line 13
    check-cast p3, Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;

    .line 14
    .line 15
    if-nez p3, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 19
    .line 20
    .line 21
    move-result-object p5

    .line 22
    invoke-direct {p0, p3}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->ClientAnchorToTwoCellAnchor(Lcom/intsig/office/fc/hssf/usermodel/HSSFClientAnchor;)Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 23
    .line 24
    .line 25
    move-result-object p3

    .line 26
    invoke-virtual {p5, p0, p3}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/drawing/CellAnchor;)Lcom/intsig/office/java/awt/Rectangle;

    .line 27
    .line 28
    .line 29
    move-result-object p3

    .line 30
    if-eqz p3, :cond_3

    .line 31
    .line 32
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getRotation()I

    .line 33
    .line 34
    .line 35
    move-result p5

    .line 36
    int-to-float p5, p5

    .line 37
    invoke-static {p3, p5}, Lcom/intsig/office/ss/util/ModelUtil;->processRect(Lcom/intsig/office/java/awt/Rectangle;F)Lcom/intsig/office/java/awt/Rectangle;

    .line 38
    .line 39
    .line 40
    move-result-object p3

    .line 41
    goto/16 :goto_0

    .line 42
    .line 43
    :cond_1
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getAnchor()Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChildAnchor;

    .line 48
    .line 49
    if-nez v0, :cond_2

    .line 50
    .line 51
    return-void

    .line 52
    :cond_2
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 53
    .line 54
    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 55
    .line 56
    .line 57
    iget v2, p5, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx1()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX1()I

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    sub-int/2addr v3, v4

    .line 68
    int-to-float v3, v3

    .line 69
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX2()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX1()I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    sub-int/2addr v4, v5

    .line 78
    int-to-float v4, v4

    .line 79
    div-float/2addr v3, v4

    .line 80
    iget v4, p5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 81
    .line 82
    int-to-float v4, v4

    .line 83
    mul-float v3, v3, v4

    .line 84
    .line 85
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    add-int/2addr v2, v3

    .line 90
    iput v2, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 91
    .line 92
    iget v2, p5, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy1()I

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY1()I

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    sub-int/2addr v3, v4

    .line 103
    int-to-float v3, v3

    .line 104
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY2()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY1()I

    .line 109
    .line 110
    .line 111
    move-result v5

    .line 112
    sub-int/2addr v4, v5

    .line 113
    int-to-float v4, v4

    .line 114
    div-float/2addr v3, v4

    .line 115
    iget v4, p5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 116
    .line 117
    int-to-float v4, v4

    .line 118
    mul-float v3, v3, v4

    .line 119
    .line 120
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    add-int/2addr v2, v3

    .line 125
    iput v2, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 126
    .line 127
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx2()I

    .line 128
    .line 129
    .line 130
    move-result v2

    .line 131
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDx1()I

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    sub-int/2addr v2, v3

    .line 136
    int-to-float v2, v2

    .line 137
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX2()I

    .line 138
    .line 139
    .line 140
    move-result v3

    .line 141
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getX1()I

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    sub-int/2addr v3, v4

    .line 146
    int-to-float v3, v3

    .line 147
    div-float/2addr v2, v3

    .line 148
    iget v3, p5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 149
    .line 150
    int-to-float v3, v3

    .line 151
    mul-float v2, v2, v3

    .line 152
    .line 153
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 154
    .line 155
    .line 156
    move-result v2

    .line 157
    iput v2, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy2()I

    .line 160
    .line 161
    .line 162
    move-result v2

    .line 163
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAnchor;->getDy1()I

    .line 164
    .line 165
    .line 166
    move-result v0

    .line 167
    sub-int/2addr v2, v0

    .line 168
    int-to-float v0, v2

    .line 169
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY2()I

    .line 170
    .line 171
    .line 172
    move-result v2

    .line 173
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getY1()I

    .line 174
    .line 175
    .line 176
    move-result p3

    .line 177
    sub-int/2addr v2, p3

    .line 178
    int-to-float p3, v2

    .line 179
    div-float/2addr v0, p3

    .line 180
    iget p3, p5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 181
    .line 182
    int-to-float p3, p3

    .line 183
    mul-float v0, v0, p3

    .line 184
    .line 185
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 186
    .line 187
    .line 188
    move-result p3

    .line 189
    iput p3, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 190
    .line 191
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getRotation()I

    .line 192
    .line 193
    .line 194
    move-result p3

    .line 195
    int-to-float p3, p3

    .line 196
    invoke-static {v1, p3}, Lcom/intsig/office/ss/util/ModelUtil;->processRect(Lcom/intsig/office/java/awt/Rectangle;F)Lcom/intsig/office/java/awt/Rectangle;

    .line 197
    .line 198
    .line 199
    move-result-object p3

    .line 200
    :cond_3
    :goto_0
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 201
    .line 202
    .line 203
    move-result p5

    .line 204
    const/16 v0, 0x14

    .line 205
    .line 206
    if-eq p5, v0, :cond_6

    .line 207
    .line 208
    const/16 v0, 0x20

    .line 209
    .line 210
    if-eq p5, v0, :cond_6

    .line 211
    .line 212
    iget p5, p3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 213
    .line 214
    if-eqz p5, :cond_4

    .line 215
    .line 216
    iget p5, p3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 217
    .line 218
    if-nez p5, :cond_6

    .line 219
    .line 220
    :cond_4
    return-void

    .line 221
    :cond_5
    const/4 p3, 0x0

    .line 222
    :cond_6
    instance-of p5, p4, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 223
    .line 224
    if-eqz p5, :cond_9

    .line 225
    .line 226
    new-instance p5, Lcom/intsig/office/common/shape/GroupShape;

    .line 227
    .line 228
    invoke-direct {p5}, Lcom/intsig/office/common/shape/GroupShape;-><init>()V

    .line 229
    .line 230
    .line 231
    invoke-virtual {p5, p3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 232
    .line 233
    .line 234
    check-cast p4, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;

    .line 235
    .line 236
    invoke-virtual {p4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;->getChildren()Ljava/util/List;

    .line 237
    .line 238
    .line 239
    move-result-object v0

    .line 240
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 241
    .line 242
    .line 243
    move-result-object v6

    .line 244
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 245
    .line 246
    .line 247
    move-result v0

    .line 248
    if-eqz v0, :cond_7

    .line 249
    .line 250
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    move-object v4, v0

    .line 255
    check-cast v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 256
    .line 257
    move-object v0, p0

    .line 258
    move-object v1, p1

    .line 259
    move-object v2, p5

    .line 260
    move-object v3, p4

    .line 261
    move-object v5, p3

    .line 262
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 263
    .line 264
    .line 265
    goto :goto_1

    .line 266
    :cond_7
    if-nez p2, :cond_8

    .line 267
    .line 268
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 269
    .line 270
    invoke-interface {p1, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    .line 272
    .line 273
    goto :goto_2

    .line 274
    :cond_8
    invoke-virtual {p2, p5}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 275
    .line 276
    .line 277
    goto :goto_2

    .line 278
    :cond_9
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processSingleShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 279
    .line 280
    .line 281
    :goto_2
    return-void
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private processSingleShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    move-object/from16 v5, p4

    .line 10
    .line 11
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;

    .line 12
    .line 13
    const/4 v6, 0x1

    .line 14
    const/4 v10, 0x0

    .line 15
    if-eqz v4, :cond_9

    .line 16
    .line 17
    move-object v4, v3

    .line 18
    check-cast v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;

    .line 19
    .line 20
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getPictureData()Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;

    .line 21
    .line 22
    .line 23
    move-result-object v7

    .line 24
    if-eqz v7, :cond_4

    .line 25
    .line 26
    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->getData()[B

    .line 27
    .line 28
    .line 29
    move-result-object v6

    .line 30
    if-eqz v6, :cond_2a

    .line 31
    .line 32
    new-instance v8, Lcom/intsig/office/common/picture/Picture;

    .line 33
    .line 34
    invoke-direct {v8}, Lcom/intsig/office/common/picture/Picture;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v8, v6}, Lcom/intsig/office/common/picture/Picture;->setData([B)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v7}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPictureData;->getFormat()I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    const/4 v7, 0x2

    .line 45
    if-eq v6, v7, :cond_0

    .line 46
    .line 47
    const/4 v7, 0x3

    .line 48
    if-eq v6, v7, :cond_0

    .line 49
    .line 50
    const/4 v7, 0x6

    .line 51
    :cond_0
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/picture/Picture;->setPictureType(B)V

    .line 52
    .line 53
    .line 54
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 55
    .line 56
    .line 57
    move-result-object v6

    .line 58
    invoke-virtual {v6}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 59
    .line 60
    .line 61
    move-result-object v6

    .line 62
    invoke-virtual {v6, v8}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/common/picture/Picture;)I

    .line 63
    .line 64
    .line 65
    move-result v6

    .line 66
    new-instance v7, Lcom/intsig/office/common/shape/PictureShape;

    .line 67
    .line 68
    invoke-direct {v7}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v7, v6}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v7, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPicture;->getEscherOptRecord()Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    invoke-static {v4}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/ddf/EscherOptRecord;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    invoke-virtual {v7, v4}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v3, v7}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    if-nez v4, :cond_1

    .line 96
    .line 97
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 98
    .line 99
    .line 100
    move-result-object v4

    .line 101
    invoke-virtual {v7, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 102
    .line 103
    .line 104
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    if-nez v4, :cond_2

    .line 109
    .line 110
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    invoke-virtual {v7, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 115
    .line 116
    .line 117
    :cond_2
    if-nez v2, :cond_3

    .line 118
    .line 119
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 120
    .line 121
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    goto/16 :goto_a

    .line 125
    .line 126
    :cond_3
    invoke-virtual {v2, v7}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 127
    .line 128
    .line 129
    goto/16 :goto_a

    .line 130
    .line 131
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    if-eqz v4, :cond_5

    .line 136
    .line 137
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 138
    .line 139
    .line 140
    move-result v4

    .line 141
    if-nez v4, :cond_2a

    .line 142
    .line 143
    :cond_5
    new-instance v4, Lcom/intsig/office/common/shape/AutoShape;

    .line 144
    .line 145
    invoke-direct {v4, v6}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v4, v10}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 152
    .line 153
    .line 154
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 155
    .line 156
    .line 157
    move-result v5

    .line 158
    if-nez v5, :cond_6

    .line 159
    .line 160
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 161
    .line 162
    .line 163
    move-result-object v5

    .line 164
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 165
    .line 166
    .line 167
    :cond_6
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 168
    .line 169
    .line 170
    move-result v5

    .line 171
    if-nez v5, :cond_7

    .line 172
    .line 173
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 178
    .line 179
    .line 180
    :cond_7
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 181
    .line 182
    .line 183
    if-nez v2, :cond_8

    .line 184
    .line 185
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 186
    .line 187
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    .line 189
    .line 190
    goto/16 :goto_a

    .line 191
    .line 192
    :cond_8
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 193
    .line 194
    .line 195
    goto/16 :goto_a

    .line 196
    .line 197
    :cond_9
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 198
    .line 199
    if-eqz v4, :cond_f

    .line 200
    .line 201
    check-cast v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 202
    .line 203
    new-instance v4, Lcom/intsig/office/common/shape/AChart;

    .line 204
    .line 205
    invoke-direct {v4}, Lcom/intsig/office/common/shape/AChart;-><init>()V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 209
    .line 210
    .line 211
    invoke-static {}, Lcom/intsig/office/fc/xls/ChartConverter;->instance()Lcom/intsig/office/fc/xls/ChartConverter;

    .line 212
    .line 213
    .line 214
    move-result-object v5

    .line 215
    invoke-virtual {v5, v0, v3}, Lcom/intsig/office/fc/xls/ChartConverter;->converter(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 216
    .line 217
    .line 218
    move-result-object v5

    .line 219
    if-eqz v5, :cond_2a

    .line 220
    .line 221
    instance-of v6, v5, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 222
    .line 223
    if-eqz v6, :cond_a

    .line 224
    .line 225
    move-object v6, v5

    .line 226
    check-cast v6, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 227
    .line 228
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 229
    .line 230
    .line 231
    move-result-object v7

    .line 232
    goto :goto_0

    .line 233
    :cond_a
    instance-of v6, v5, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 234
    .line 235
    if-eqz v6, :cond_b

    .line 236
    .line 237
    move-object v6, v5

    .line 238
    check-cast v6, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 239
    .line 240
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 241
    .line 242
    .line 243
    move-result-object v7

    .line 244
    goto :goto_0

    .line 245
    :cond_b
    const/4 v7, 0x0

    .line 246
    :goto_0
    if-eqz v7, :cond_d

    .line 247
    .line 248
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 249
    .line 250
    .line 251
    move-result v6

    .line 252
    if-nez v6, :cond_c

    .line 253
    .line 254
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 255
    .line 256
    .line 257
    move-result-object v6

    .line 258
    invoke-virtual {v7, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartFrame(Lcom/intsig/office/common/borders/Line;)V

    .line 259
    .line 260
    .line 261
    :cond_c
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 262
    .line 263
    .line 264
    move-result v6

    .line 265
    if-nez v6, :cond_d

    .line 266
    .line 267
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    invoke-virtual {v7, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 272
    .line 273
    .line 274
    :cond_d
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AChart;->setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 275
    .line 276
    .line 277
    if-nez v2, :cond_e

    .line 278
    .line 279
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 280
    .line 281
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    goto/16 :goto_a

    .line 285
    .line 286
    :cond_e
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 287
    .line 288
    .line 289
    goto/16 :goto_a

    .line 290
    .line 291
    :cond_f
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFLine;

    .line 292
    .line 293
    if-eqz v4, :cond_14

    .line 294
    .line 295
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 296
    .line 297
    .line 298
    move-result v1

    .line 299
    if-nez v1, :cond_2a

    .line 300
    .line 301
    new-instance v1, Lcom/intsig/office/common/shape/LineShape;

    .line 302
    .line 303
    invoke-direct {v1}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 304
    .line 305
    .line 306
    invoke-virtual {v1, v10}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 307
    .line 308
    .line 309
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 310
    .line 311
    .line 312
    move-result v4

    .line 313
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 314
    .line 315
    .line 316
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 317
    .line 318
    .line 319
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 320
    .line 321
    .line 322
    move-result-object v4

    .line 323
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 324
    .line 325
    .line 326
    move-object v4, v3

    .line 327
    check-cast v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFLine;

    .line 328
    .line 329
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFLine;->getAdjustmentValue()[Ljava/lang/Float;

    .line 330
    .line 331
    .line 332
    move-result-object v5

    .line 333
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 334
    .line 335
    .line 336
    move-result v7

    .line 337
    const/16 v8, 0x21

    .line 338
    .line 339
    if-ne v7, v8, :cond_10

    .line 340
    .line 341
    if-nez v5, :cond_10

    .line 342
    .line 343
    new-array v5, v6, [Ljava/lang/Float;

    .line 344
    .line 345
    const/high16 v6, 0x3f800000    # 1.0f

    .line 346
    .line 347
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 348
    .line 349
    .line 350
    move-result-object v6

    .line 351
    aput-object v6, v5, v10

    .line 352
    .line 353
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 354
    .line 355
    .line 356
    goto :goto_1

    .line 357
    :cond_10
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 358
    .line 359
    .line 360
    :goto_1
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getStartArrowType()I

    .line 361
    .line 362
    .line 363
    move-result v5

    .line 364
    if-lez v5, :cond_11

    .line 365
    .line 366
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getStartArrowType()I

    .line 367
    .line 368
    .line 369
    move-result v5

    .line 370
    int-to-byte v5, v5

    .line 371
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getStartArrowWidth()I

    .line 372
    .line 373
    .line 374
    move-result v6

    .line 375
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getStartArrowLength()I

    .line 376
    .line 377
    .line 378
    move-result v7

    .line 379
    invoke-virtual {v1, v5, v6, v7}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 380
    .line 381
    .line 382
    :cond_11
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getEndArrowType()I

    .line 383
    .line 384
    .line 385
    move-result v4

    .line 386
    if-lez v4, :cond_12

    .line 387
    .line 388
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getEndArrowType()I

    .line 389
    .line 390
    .line 391
    move-result v4

    .line 392
    int-to-byte v4, v4

    .line 393
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getEndArrowWidth()I

    .line 394
    .line 395
    .line 396
    move-result v5

    .line 397
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getEndArrowLength()I

    .line 398
    .line 399
    .line 400
    move-result v6

    .line 401
    invoke-virtual {v1, v4, v5, v6}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 402
    .line 403
    .line 404
    :cond_12
    invoke-virtual {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 405
    .line 406
    .line 407
    if-nez v2, :cond_13

    .line 408
    .line 409
    iget-object v2, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 410
    .line 411
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    .line 413
    .line 414
    goto/16 :goto_a

    .line 415
    .line 416
    :cond_13
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 417
    .line 418
    .line 419
    goto/16 :goto_a

    .line 420
    .line 421
    :cond_14
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;

    .line 422
    .line 423
    if-eqz v4, :cond_22

    .line 424
    .line 425
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 426
    .line 427
    .line 428
    move-result v4

    .line 429
    if-eqz v4, :cond_15

    .line 430
    .line 431
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 432
    .line 433
    .line 434
    move-result v4

    .line 435
    if-nez v4, :cond_2a

    .line 436
    .line 437
    :cond_15
    new-instance v11, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;

    .line 438
    .line 439
    invoke-direct {v11}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;-><init>()V

    .line 440
    .line 441
    .line 442
    const/16 v4, 0xe9

    .line 443
    .line 444
    invoke-virtual {v11, v4}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 445
    .line 446
    .line 447
    invoke-virtual {v11, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 448
    .line 449
    .line 450
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 451
    .line 452
    .line 453
    move-result-object v12

    .line 454
    move-object v4, v3

    .line 455
    check-cast v4, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;

    .line 456
    .line 457
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getStartArrowType()I

    .line 458
    .line 459
    .line 460
    move-result v8

    .line 461
    const/4 v9, 0x5

    .line 462
    if-lez v8, :cond_19

    .line 463
    .line 464
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;->getStartArrowPath(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 465
    .line 466
    .line 467
    move-result-object v13

    .line 468
    if-eqz v13, :cond_19

    .line 469
    .line 470
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 471
    .line 472
    .line 473
    move-result-object v14

    .line 474
    if-eqz v14, :cond_19

    .line 475
    .line 476
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 477
    .line 478
    .line 479
    move-result-object v14

    .line 480
    new-instance v15, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 481
    .line 482
    invoke-direct {v15}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 483
    .line 484
    .line 485
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 486
    .line 487
    .line 488
    move-result-object v13

    .line 489
    invoke-virtual {v15, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 490
    .line 491
    .line 492
    invoke-virtual {v15, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 493
    .line 494
    .line 495
    if-eq v8, v9, :cond_18

    .line 496
    .line 497
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 498
    .line 499
    .line 500
    move-result v13

    .line 501
    if-eqz v13, :cond_16

    .line 502
    .line 503
    new-instance v13, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 504
    .line 505
    invoke-direct {v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 506
    .line 507
    .line 508
    invoke-virtual {v13, v10}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 509
    .line 510
    .line 511
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyleColor()I

    .line 512
    .line 513
    .line 514
    move-result v7

    .line 515
    invoke-virtual {v13, v7}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 516
    .line 517
    .line 518
    goto :goto_2

    .line 519
    :cond_16
    if-eqz v12, :cond_17

    .line 520
    .line 521
    invoke-virtual {v12}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 522
    .line 523
    .line 524
    move-result-object v13

    .line 525
    goto :goto_2

    .line 526
    :cond_17
    const/4 v13, 0x0

    .line 527
    :goto_2
    invoke-virtual {v15, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 528
    .line 529
    .line 530
    goto :goto_3

    .line 531
    :cond_18
    invoke-virtual {v15, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 532
    .line 533
    .line 534
    :goto_3
    invoke-virtual {v11, v15}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 535
    .line 536
    .line 537
    goto :goto_4

    .line 538
    :cond_19
    const/4 v14, 0x0

    .line 539
    :goto_4
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getEndArrowType()I

    .line 540
    .line 541
    .line 542
    move-result v7

    .line 543
    if-lez v7, :cond_1d

    .line 544
    .line 545
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;->getEndArrowPath(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 546
    .line 547
    .line 548
    move-result-object v13

    .line 549
    if-eqz v13, :cond_1d

    .line 550
    .line 551
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 552
    .line 553
    .line 554
    move-result-object v15

    .line 555
    if-eqz v15, :cond_1d

    .line 556
    .line 557
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 558
    .line 559
    .line 560
    move-result-object v15

    .line 561
    new-instance v10, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 562
    .line 563
    invoke-direct {v10}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 564
    .line 565
    .line 566
    invoke-virtual {v13}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 567
    .line 568
    .line 569
    move-result-object v13

    .line 570
    invoke-virtual {v10, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 571
    .line 572
    .line 573
    invoke-virtual {v10, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 574
    .line 575
    .line 576
    if-eq v7, v9, :cond_1c

    .line 577
    .line 578
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 579
    .line 580
    .line 581
    move-result v6

    .line 582
    if-eqz v6, :cond_1a

    .line 583
    .line 584
    new-instance v6, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 585
    .line 586
    invoke-direct {v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 587
    .line 588
    .line 589
    const/4 v9, 0x0

    .line 590
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 591
    .line 592
    .line 593
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLineStyleColor()I

    .line 594
    .line 595
    .line 596
    move-result v9

    .line 597
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 598
    .line 599
    .line 600
    goto :goto_5

    .line 601
    :cond_1a
    if-eqz v12, :cond_1b

    .line 602
    .line 603
    invoke-virtual {v12}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 604
    .line 605
    .line 606
    move-result-object v6

    .line 607
    goto :goto_5

    .line 608
    :cond_1b
    const/4 v6, 0x0

    .line 609
    :goto_5
    invoke-virtual {v10, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 610
    .line 611
    .line 612
    goto :goto_6

    .line 613
    :cond_1c
    invoke-virtual {v10, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 614
    .line 615
    .line 616
    :goto_6
    invoke-virtual {v11, v10}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 617
    .line 618
    .line 619
    goto :goto_7

    .line 620
    :cond_1d
    const/4 v15, 0x0

    .line 621
    :goto_7
    int-to-byte v8, v8

    .line 622
    int-to-byte v9, v7

    .line 623
    move-object/from16 v5, p4

    .line 624
    .line 625
    move-object v6, v14

    .line 626
    move v7, v8

    .line 627
    move-object v8, v15

    .line 628
    invoke-virtual/range {v4 .. v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFreeform;->getFreeformPath(Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;

    .line 629
    .line 630
    .line 631
    move-result-object v4

    .line 632
    const/4 v10, 0x0

    .line 633
    :goto_8
    array-length v5, v4

    .line 634
    if-ge v10, v5, :cond_20

    .line 635
    .line 636
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 637
    .line 638
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 639
    .line 640
    .line 641
    aget-object v6, v4, v10

    .line 642
    .line 643
    invoke-virtual {v5, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 644
    .line 645
    .line 646
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 647
    .line 648
    .line 649
    move-result v6

    .line 650
    if-nez v6, :cond_1e

    .line 651
    .line 652
    invoke-virtual {v5, v12}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 653
    .line 654
    .line 655
    :cond_1e
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 656
    .line 657
    .line 658
    move-result v6

    .line 659
    if-nez v6, :cond_1f

    .line 660
    .line 661
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 662
    .line 663
    .line 664
    move-result-object v6

    .line 665
    invoke-virtual {v5, v6}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 666
    .line 667
    .line 668
    :cond_1f
    invoke-virtual {v11, v5}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 669
    .line 670
    .line 671
    add-int/lit8 v10, v10, 0x1

    .line 672
    .line 673
    goto :goto_8

    .line 674
    :cond_20
    invoke-virtual {v0, v3, v11}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 675
    .line 676
    .line 677
    if-nez v2, :cond_21

    .line 678
    .line 679
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 680
    .line 681
    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 682
    .line 683
    .line 684
    goto/16 :goto_a

    .line 685
    .line 686
    :cond_21
    invoke-virtual {v2, v11}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 687
    .line 688
    .line 689
    goto/16 :goto_a

    .line 690
    .line 691
    :cond_22
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;

    .line 692
    .line 693
    if-eqz v4, :cond_2a

    .line 694
    .line 695
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 696
    .line 697
    .line 698
    move-result v4

    .line 699
    if-eqz v4, :cond_23

    .line 700
    .line 701
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 702
    .line 703
    .line 704
    move-result v4

    .line 705
    if-nez v4, :cond_28

    .line 706
    .line 707
    :cond_23
    new-instance v4, Lcom/intsig/office/common/shape/AutoShape;

    .line 708
    .line 709
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 710
    .line 711
    .line 712
    move-result v6

    .line 713
    invoke-direct {v4, v6}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    .line 714
    .line 715
    .line 716
    const/4 v6, 0x0

    .line 717
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 718
    .line 719
    .line 720
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 721
    .line 722
    .line 723
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 724
    .line 725
    .line 726
    move-result v6

    .line 727
    if-nez v6, :cond_24

    .line 728
    .line 729
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 730
    .line 731
    .line 732
    move-result-object v6

    .line 733
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 734
    .line 735
    .line 736
    :cond_24
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoFill()Z

    .line 737
    .line 738
    .line 739
    move-result v6

    .line 740
    if-nez v6, :cond_25

    .line 741
    .line 742
    invoke-direct {v0, v3, v1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->converFill(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 743
    .line 744
    .line 745
    move-result-object v1

    .line 746
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 747
    .line 748
    .line 749
    :cond_25
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getShapeType()I

    .line 750
    .line 751
    .line 752
    move-result v1

    .line 753
    const/16 v6, 0xca

    .line 754
    .line 755
    if-eq v1, v6, :cond_26

    .line 756
    .line 757
    move-object v1, v3

    .line 758
    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;

    .line 759
    .line 760
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFAutoShape;->getAdjustmentValue()[Ljava/lang/Float;

    .line 761
    .line 762
    .line 763
    move-result-object v1

    .line 764
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 765
    .line 766
    .line 767
    :cond_26
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 768
    .line 769
    .line 770
    if-nez v2, :cond_27

    .line 771
    .line 772
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 773
    .line 774
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 775
    .line 776
    .line 777
    goto :goto_9

    .line 778
    :cond_27
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 779
    .line 780
    .line 781
    :cond_28
    :goto_9
    move-object v1, v3

    .line 782
    check-cast v1, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;

    .line 783
    .line 784
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->getString()Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;

    .line 785
    .line 786
    .line 787
    move-result-object v4

    .line 788
    if-eqz v4, :cond_2a

    .line 789
    .line 790
    invoke-virtual {v4}, Lcom/intsig/office/fc/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    .line 791
    .line 792
    .line 793
    move-result-object v4

    .line 794
    if-eqz v4, :cond_2a

    .line 795
    .line 796
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 797
    .line 798
    .line 799
    move-result v4

    .line 800
    if-lez v4, :cond_2a

    .line 801
    .line 802
    new-instance v4, Lcom/intsig/office/common/shape/TextBox;

    .line 803
    .line 804
    invoke-direct {v4}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 805
    .line 806
    .line 807
    iget-object v6, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 808
    .line 809
    invoke-static {v6, v1, v5}, Lcom/intsig/office/ss/util/SectionElementFactory;->getSectionElement(Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/simpletext/model/SectionElement;

    .line 810
    .line 811
    .line 812
    move-result-object v6

    .line 813
    invoke-virtual {v4, v6}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 814
    .line 815
    .line 816
    invoke-virtual {v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFTextbox;->isTextboxWrapLine()Z

    .line 817
    .line 818
    .line 819
    move-result v1

    .line 820
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 821
    .line 822
    .line 823
    invoke-virtual {v4, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 824
    .line 825
    .line 826
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V

    .line 827
    .line 828
    .line 829
    if-nez v2, :cond_29

    .line 830
    .line 831
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 832
    .line 833
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 834
    .line 835
    .line 836
    goto :goto_a

    .line 837
    :cond_29
    invoke-virtual {v2, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 838
    .line 839
    .line 840
    :cond_2a
    :goto_a
    return-void
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->dispose()V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAWorkbook()Lcom/intsig/office/ss/model/XLSModel/AWorkbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDrawingEscherAggregate(Lcom/intsig/office/fc/hssf/model/InternalSheet;)Lcom/intsig/office/fc/hssf/record/EscherAggregate;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->findDrawingGroup()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/4 v2, 0x0

    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    return-object v2

    .line 20
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getDrawingManager()Lcom/intsig/office/fc/hssf/model/DrawingManager2;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->aggregateDrawingRecords(Lcom/intsig/office/fc/hssf/model/DrawingManager2;Z)I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v1, -0x1

    .line 30
    if-ne v0, v1, :cond_1

    .line 31
    .line 32
    return-object v2

    .line 33
    :cond_1
    const/16 v0, 0x2694

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->findFirstRecordBySid(S)Lcom/intsig/office/fc/hssf/record/Record;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    check-cast p1, Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 40
    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getDrawingPatriarch(Lcom/intsig/office/fc/hssf/model/InternalSheet;)Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getDrawingEscherAggregate(Lcom/intsig/office/fc/hssf/model/InternalSheet;)Lcom/intsig/office/fc/hssf/record/EscherAggregate;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;-><init>(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/record/EscherAggregate;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->setPatriarch(Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getAWorkbook()Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hssf/record/EscherAggregate;->convertRecordsToUserModel(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getInternalSheet()Lcom/intsig/office/fc/hssf/model/InternalSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public processRotationAndFlip(Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/common/shape/IShape;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getRotation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFlipH()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipHorizontal(Z)V

    .line 14
    .line 15
    .line 16
    neg-float v0, v0

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getFlipV()Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipVertical(Z)V

    .line 24
    .line 25
    .line 26
    neg-float v0, v0

    .line 27
    :cond_1
    instance-of p1, p2, Lcom/intsig/office/common/shape/LineShape;

    .line 28
    .line 29
    if-eqz p1, :cond_3

    .line 30
    .line 31
    const/high16 p1, 0x42340000    # 45.0f

    .line 32
    .line 33
    cmpl-float p1, v0, p1

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    const/high16 p1, 0x43070000    # 135.0f

    .line 38
    .line 39
    cmpl-float p1, v0, p1

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    const/high16 p1, 0x43610000    # 225.0f

    .line 44
    .line 45
    cmpl-float p1, v0, p1

    .line 46
    .line 47
    if-nez p1, :cond_3

    .line 48
    .line 49
    :cond_2
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipHorizontal()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipVertical()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-nez p1, :cond_3

    .line 60
    .line 61
    const/high16 p1, 0x42b40000    # 90.0f

    .line 62
    .line 63
    sub-float/2addr v0, p1

    .line 64
    :cond_3
    invoke-interface {p2, v0}, Lcom/intsig/office/common/shape/IShape;->setRotation(F)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public processSheet(Lcom/intsig/office/system/AbstractReader;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_0

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->initRowFinished:Z

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 13
    .line 14
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processRowsAndCells(Lcom/intsig/office/fc/hssf/model/InternalSheet;Lcom/intsig/office/system/AbstractReader;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processMergedCells()V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processHyperlinkfromSheet(Lcom/intsig/office/fc/hssf/model/InternalSheet;)V

    .line 23
    .line 24
    .line 25
    iput-boolean v1, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->initRowFinished:Z

    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public processSheetShapes(Lcom/intsig/office/system/IControl;)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "abort Reader"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v0, :cond_3

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getDrawingPatriarch(Lcom/intsig/office/fc/hssf/model/InternalSheet;)Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_2

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->getChildren()Ljava/util/List;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_1

    .line 31
    .line 32
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    move-object v9, v4

    .line 37
    check-cast v9, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;

    .line 38
    .line 39
    iget-object v4, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 40
    .line 41
    check-cast v4, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 42
    .line 43
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getAbstractReader()Lcom/intsig/office/system/AbstractReader;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    invoke-virtual {v4}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-nez v4, :cond_0

    .line 52
    .line 53
    const/4 v7, 0x0

    .line 54
    const/4 v8, 0x0

    .line 55
    const/4 v10, 0x0

    .line 56
    move-object v5, p0

    .line 57
    move-object v6, p1

    .line 58
    invoke-direct/range {v5 .. v10}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hssf/usermodel/HSSFShapeGroup;Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    .line 63
    .line 64
    invoke-direct {p1, v1}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1

    .line 68
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFPatriarch;->dispose()V

    .line 69
    .line 70
    .line 71
    :cond_2
    iput-object v2, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_3
    const/4 p1, 0x1

    .line 75
    if-ne v0, p1, :cond_8

    .line 76
    .line 77
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 78
    .line 79
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getAbstractReader()Lcom/intsig/office/system/AbstractReader;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lcom/intsig/office/system/AbstractReader;->isAborted()Z

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    if-nez p1, :cond_7

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/office/ss/model/XLSModel/ASheet;->sheet:Lcom/intsig/office/fc/hssf/model/InternalSheet;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/model/InternalSheet;->getChart()Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    new-instance v0, Lcom/intsig/office/common/shape/AChart;

    .line 98
    .line 99
    invoke-direct {v0}, Lcom/intsig/office/common/shape/AChart;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-static {}, Lcom/intsig/office/fc/xls/ChartConverter;->instance()Lcom/intsig/office/fc/xls/ChartConverter;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-virtual {v1, p0, p1}, Lcom/intsig/office/fc/xls/ChartConverter;->converter(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    if-eqz v1, :cond_8

    .line 111
    .line 112
    instance-of v3, v1, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 113
    .line 114
    if-eqz v3, :cond_4

    .line 115
    .line 116
    move-object v2, v1

    .line 117
    check-cast v2, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 118
    .line 119
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    goto :goto_1

    .line 124
    :cond_4
    instance-of v3, v1, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 125
    .line 126
    if-eqz v3, :cond_5

    .line 127
    .line 128
    move-object v2, v1

    .line 129
    check-cast v2, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 130
    .line 131
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    .line 136
    .line 137
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->isNoBorder()Z

    .line 138
    .line 139
    .line 140
    move-result v3

    .line 141
    if-nez v3, :cond_6

    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    invoke-virtual {v2, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartFrame(Lcom/intsig/office/common/borders/Line;)V

    .line 148
    .line 149
    .line 150
    :cond_6
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AChart;->setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 151
    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 154
    .line 155
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    goto :goto_2

    .line 159
    :cond_7
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    .line 160
    .line 161
    invoke-direct {p1, v1}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    throw p1

    .line 165
    :cond_8
    :goto_2
    return-void
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public rowIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/ss/model/baseModel/Row;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

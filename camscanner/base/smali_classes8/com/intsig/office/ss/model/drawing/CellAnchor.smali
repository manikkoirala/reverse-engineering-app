.class public Lcom/intsig/office/ss/model/drawing/CellAnchor;
.super Ljava/lang/Object;
.source "CellAnchor.java"


# static fields
.field public static final ONECELLANCHOR:S = 0x0s

.field public static final TWOCELLANCHOR:S = 0x1s


# instance fields
.field private end:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

.field private height:I

.field protected start:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

.field private type:S

.field private width:I


# direct methods
.method public constructor <init>(S)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-short p1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->type:S

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->start:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->dispose()V

    .line 7
    .line 8
    .line 9
    iput-object v1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->start:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->end:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->dispose()V

    .line 16
    .line 17
    .line 18
    iput-object v1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->end:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 19
    .line 20
    :cond_1
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEnd()Lcom/intsig/office/ss/model/drawing/AnchorPoint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->end:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->height:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStart()Lcom/intsig/office/ss/model/drawing/AnchorPoint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->start:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->type:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->width:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setEnd(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->end:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHeight(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->height:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStart(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->start:Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/drawing/CellAnchor;->width:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

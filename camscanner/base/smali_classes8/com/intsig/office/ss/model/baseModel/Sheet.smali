.class public Lcom/intsig/office/ss/model/baseModel/Sheet;
.super Ljava/lang/Object;
.source "Sheet.java"


# static fields
.field public static final ACTIVECELL_COLUMN:S = 0x2s

.field public static final ACTIVECELL_ROW:S = 0x1s

.field public static final ACTIVECELL_SINGLE:S = 0x0s

.field public static final INITIAL_CAPACITY:I = 0x14

.field public static final State_Accomplished:S = 0x2s

.field public static final State_NotAccomplished:S = 0x0s

.field public static final State_Reading:S = 0x1s

.field public static final TYPE_CHARTSHEET:S = 0x1s

.field public static final TYPE_WORKSHEET:S


# instance fields
.field private activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

.field private activeCellColumn:I

.field private activeCellRow:I

.field private activeCellType:S

.field protected book:Lcom/intsig/office/ss/model/baseModel/Workbook;

.field private columnInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;",
            ">;"
        }
    .end annotation
.end field

.field private defaultColWidth:I

.field private defaultRowHeight:I

.field private firstRow:I

.field private iReaderListener:Lcom/intsig/office/ss/model/interfacePart/IReaderListener;

.field private isGridsPrinted:Z

.field private lastRow:I

.field private maxScrollX:F

.field private maxScrollY:F

.field private merges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/ss/model/CellRangeAddress;",
            ">;"
        }
    .end annotation
.end field

.field private paneInformation:Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;

.field private rootViewMap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/simpletext/view/STRoot;",
            ">;"
        }
    .end annotation
.end field

.field protected rows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/ss/model/baseModel/Row;",
            ">;"
        }
    .end annotation
.end field

.field private scrollX:I

.field private scrollY:I

.field protected shapesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/common/shape/IShape;",
            ">;"
        }
    .end annotation
.end field

.field private sheetName:Ljava/lang/String;

.field private state:S

.field private tableList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/ss/model/table/SSTable;",
            ">;"
        }
    .end annotation
.end field

.field private type:S

.field private zoom:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, 0x40000000    # 2.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->zoom:F

    .line 7
    .line 8
    const/16 v0, 0x12

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 11
    .line 12
    const/16 v0, 0x48

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellType:S

    .line 18
    .line 19
    new-instance v0, Ljava/util/HashMap;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 25
    .line 26
    new-instance v0, Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 32
    .line 33
    const/high16 v0, 0x4f000000

    .line 34
    .line 35
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 36
    .line 37
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 38
    .line 39
    new-instance v0, Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private checkActiveRowAndColumnBounds()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 10
    .line 11
    const v1, 0xffff

    .line 12
    .line 13
    .line 14
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 19
    .line 20
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 21
    .line 22
    const/16 v1, 0xff

    .line 23
    .line 24
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 32
    .line 33
    const v1, 0xfffff

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 41
    .line 42
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 43
    .line 44
    const/16 v1, 0x3fff

    .line 45
    .line 46
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 51
    .line 52
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public addColumnInfo(Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public addMergeRange(Lcom/intsig/office/ss/model/CellRangeAddress;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public addRow(Lcom/intsig/office/ss/model/baseModel/Row;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v1, 0x1

    .line 24
    if-ne v0, v1, :cond_1

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 50
    .line 51
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowNumber()I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public addSTRoot(Lcom/intsig/office/simpletext/view/STRoot;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public addTable(Lcom/intsig/office/ss/model/table/SSTable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public appendShapes(Lcom/intsig/office/common/shape/IShape;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->sheetName:Ljava/lang/String;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->paneInformation:Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->iReaderListener:Lcom/intsig/office/ss/model/interfacePart/IReaderListener;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Cell;->dispose()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 18
    .line 19
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 20
    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    check-cast v2, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/baseModel/Row;->dispose()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 48
    .line 49
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 50
    .line 51
    .line 52
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 53
    .line 54
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 55
    .line 56
    if-eqz v1, :cond_4

    .line 57
    .line 58
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-eqz v2, :cond_3

    .line 67
    .line 68
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    check-cast v2, Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/CellRangeAddress;->dispose()V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 79
    .line 80
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 81
    .line 82
    .line 83
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 84
    .line 85
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 86
    .line 87
    if-eqz v1, :cond_5

    .line 88
    .line 89
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 90
    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 93
    .line 94
    :cond_5
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 95
    .line 96
    if-eqz v1, :cond_7

    .line 97
    .line 98
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 103
    .line 104
    .line 105
    move-result v2

    .line 106
    if-eqz v2, :cond_6

    .line 107
    .line 108
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    check-cast v2, Lcom/intsig/office/common/shape/IShape;

    .line 113
    .line 114
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->dispose()V

    .line 115
    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_6
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 119
    .line 120
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 121
    .line 122
    .line 123
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 124
    .line 125
    :cond_7
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 126
    .line 127
    if-eqz v1, :cond_8

    .line 128
    .line 129
    invoke-virtual {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->removeSTRoot()V

    .line 130
    .line 131
    .line 132
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 133
    .line 134
    :cond_8
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 135
    .line 136
    if-eqz v1, :cond_9

    .line 137
    .line 138
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 139
    .line 140
    .line 141
    iput-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 142
    .line 143
    :cond_9
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getActiveCell()Lcom/intsig/office/ss/model/baseModel/Cell;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActiveCellColumn()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActiveCellRow()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActiveCellType()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellType:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColumnInfo(I)Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-ge v0, v1, :cond_1

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 15
    .line 16
    add-int/lit8 v2, v0, 0x1

    .line 17
    .line 18
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-gt v1, p1, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-lt v1, p1, :cond_0

    .line 35
    .line 36
    return-object v0

    .line 37
    :cond_0
    move v0, v2

    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const/4 p1, 0x0

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getColumnPixelWidth(I)F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-ge v0, v1, :cond_1

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 15
    .line 16
    add-int/lit8 v2, v0, 0x1

    .line 17
    .line 18
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-gt v1, p1, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-lt v1, p1, :cond_0

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    return p1

    .line 41
    :cond_0
    move v0, v2

    .line 42
    goto :goto_0

    .line 43
    :cond_1
    iget p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 44
    .line 45
    int-to-float p1, p1

    .line 46
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getColumnStyle(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 8
    .line 9
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-ge v0, v2, :cond_1

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 16
    .line 17
    add-int/lit8 v3, v0, 0x1

    .line 18
    .line 19
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-gt v2, p1, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-lt v2, p1, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    return p1

    .line 42
    :cond_0
    move v0, v3

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    return v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getDefaultColWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDefaultRowHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFirstRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLastRowNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMaxScrollX()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMaxScrollY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMergeRange(I)Lcom/intsig/office/ss/model/CellRangeAddress;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 19
    .line 20
    return-object p1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getMergeRangeCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPaneInformation()Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPhysicalNumberOfRows()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getRowByColumnsStyle(I)Lcom/intsig/office/ss/model/baseModel/Row;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_5

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    const/4 v0, 0x0

    .line 29
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ge v0, v2, :cond_5

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 38
    .line 39
    add-int/lit8 v3, v0, 0x1

    .line 40
    .line 41
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {v2, v0}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    if-eqz v0, :cond_4

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getFillPatternType()B

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    if-nez v2, :cond_2

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getFgColor()I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    const v4, 0xffffff

    .line 70
    .line 71
    .line 72
    and-int/2addr v2, v4

    .line 73
    if-ne v2, v4, :cond_3

    .line 74
    .line 75
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderLeft()S

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    if-gtz v2, :cond_3

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderTop()S

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    if-gtz v2, :cond_3

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderRight()S

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-gtz v2, :cond_3

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getBorderBottom()S

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-lez v0, :cond_4

    .line 98
    .line 99
    :cond_3
    new-instance v0, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 100
    .line 101
    const/4 v1, 0x1

    .line 102
    invoke-direct {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Row;-><init>(I)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowNumber(I)V

    .line 106
    .line 107
    .line 108
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 109
    .line 110
    int-to-float v1, v1

    .line 111
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Row;->setRowPixelHeight(F)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0, p0}, Lcom/intsig/office/ss/model/baseModel/Row;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->completed()V

    .line 118
    .line 119
    .line 120
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 121
    .line 122
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    .line 128
    .line 129
    return-object v0

    .line 130
    :cond_4
    move v0, v3

    .line 131
    goto :goto_0

    .line 132
    :cond_5
    :goto_1
    return-object v1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getSTRoot(I)Lcom/intsig/office/simpletext/view/STRoot;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Lcom/intsig/office/simpletext/view/STRoot;

    .line 19
    .line 20
    return-object p1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getScrollX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollX:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScrollY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollY:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShape(I)Lcom/intsig/office/common/shape/IShape;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Lcom/intsig/office/common/shape/IShape;

    .line 19
    .line 20
    return-object p1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getShapeCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShapes()[Lcom/intsig/office/common/shape/IShape;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->shapesList:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/common/shape/IShape;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/common/shape/IShape;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheetName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->sheetName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSheetType()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->type:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public declared-synchronized getState()S
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->state:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    monitor-exit p0

    .line 5
    return v0

    .line 6
    :catchall_0
    move-exception v0

    .line 7
    monitor-exit p0

    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTables()[Lcom/intsig/office/ss/model/table/SSTable;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->tableList:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    new-array v1, v1, [Lcom/intsig/office/ss/model/table/SSTable;

    .line 10
    .line 11
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, [Lcom/intsig/office/ss/model/table/SSTable;

    .line 16
    .line 17
    return-object v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->zoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isAccomplished()Z
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->state:S

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isColumnHidden(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 8
    .line 9
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-ge v0, v2, :cond_1

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 16
    .line 17
    add-int/lit8 v3, v0, 0x1

    .line 18
    .line 19
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-gt v2, p1, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-lt v2, p1, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    return p1

    .line 42
    :cond_0
    move v0, v3

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    return v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public isGridsPrinted()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->isGridsPrinted:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeSTRoot()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x0

    .line 11
    :goto_0
    if-ge v2, v0, :cond_1

    .line 12
    .line 13
    iget-object v3, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 14
    .line 15
    add-int/lit8 v4, v2, 0x1

    .line 16
    .line 17
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    check-cast v2, Lcom/intsig/office/simpletext/view/STRoot;

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/view/STRoot;->dispose()V

    .line 26
    .line 27
    .line 28
    :cond_0
    move v2, v4

    .line 29
    goto :goto_0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rootViewMap:Ljava/util/List;

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 33
    .line 34
    .line 35
    :cond_2
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 36
    .line 37
    :goto_1
    iget v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 38
    .line 39
    if-gt v0, v2, :cond_5

    .line 40
    .line 41
    add-int/lit8 v2, v0, 0x1

    .line 42
    .line 43
    invoke-virtual {p0, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    if-eqz v0, :cond_4

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->isZeroHeight()Z

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    if-eqz v3, :cond_3

    .line 54
    .line 55
    goto :goto_3

    .line 56
    :cond_3
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Row;->setInitExpandedRangeAddress(Z)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Row;->cellCollection()Ljava/util/Collection;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_4

    .line 72
    .line 73
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    check-cast v3, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 78
    .line 79
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Cell;->removeSTRoot()V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_4
    :goto_3
    move v0, v2

    .line 84
    goto :goto_1

    .line 85
    :cond_5
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public setActiveCell(Lcom/intsig/office/ss/model/baseModel/Cell;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getRowNumber()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p1, -0x1

    .line 19
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 20
    .line 21
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setActiveCellColumn(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->checkActiveRowAndColumnBounds()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setActiveCellRow(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->checkActiveRowAndColumnBounds()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setActiveCellRowCol(II)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-short v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellType:S

    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->checkActiveRowAndColumnBounds()V

    .line 9
    .line 10
    .line 11
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-ge v0, v1, :cond_1

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->merges:Ljava/util/List;

    .line 20
    .line 21
    add-int/lit8 v2, v0, 0x1

    .line 22
    .line 23
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/office/ss/model/CellRangeAddress;

    .line 28
    .line 29
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/ss/model/CellRangeAddress;->isInRange(II)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstRow()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    iput v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellRow:I

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/CellRangeAddress;->getFirstColumn()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellColumn:I

    .line 46
    .line 47
    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_2
    const/4 p1, 0x0

    .line 67
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCell:Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 68
    .line 69
    :goto_1
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setActiveCellType(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->activeCellType:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setColumnHidden(IZ)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setColumnPixelWidth(II)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v3, p1

    .line 4
    .line 5
    move/from16 v1, p2

    .line 6
    .line 7
    iget-object v2, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 8
    .line 9
    if-eqz v2, :cond_5

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    :goto_0
    iget-object v4, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    if-ge v2, v4, :cond_4

    .line 19
    .line 20
    iget-object v4, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 21
    .line 22
    add-int/lit8 v5, v2, 0x1

    .line 23
    .line 24
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-ne v4, v3, :cond_0

    .line 35
    .line 36
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-ne v4, v3, :cond_0

    .line 41
    .line 42
    int-to-float v1, v1

    .line 43
    invoke-virtual {v2, v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setColWidth(F)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-ne v4, v3, :cond_1

    .line 52
    .line 53
    new-instance v4, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 54
    .line 55
    add-int/lit8 v7, v3, 0x1

    .line 56
    .line 57
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 58
    .line 59
    .line 60
    move-result v8

    .line 61
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 62
    .line 63
    .line 64
    move-result v9

    .line 65
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 66
    .line 67
    .line 68
    move-result v10

    .line 69
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 70
    .line 71
    .line 72
    move-result v11

    .line 73
    move-object v6, v4

    .line 74
    invoke-direct/range {v6 .. v11}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 75
    .line 76
    .line 77
    int-to-float v1, v1

    .line 78
    invoke-virtual {v2, v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setColWidth(F)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setLastCol(I)V

    .line 82
    .line 83
    .line 84
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 85
    .line 86
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    return-void

    .line 90
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    if-ne v4, v3, :cond_2

    .line 95
    .line 96
    new-instance v4, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 97
    .line 98
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 99
    .line 100
    .line 101
    move-result v7

    .line 102
    add-int/lit8 v8, v3, -0x1

    .line 103
    .line 104
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 105
    .line 106
    .line 107
    move-result v9

    .line 108
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 109
    .line 110
    .line 111
    move-result v10

    .line 112
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 113
    .line 114
    .line 115
    move-result v11

    .line 116
    move-object v6, v4

    .line 117
    invoke-direct/range {v6 .. v11}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 118
    .line 119
    .line 120
    int-to-float v1, v1

    .line 121
    invoke-virtual {v2, v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setColWidth(F)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setFirstCol(I)V

    .line 125
    .line 126
    .line 127
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 128
    .line 129
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    .line 131
    .line 132
    return-void

    .line 133
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 134
    .line 135
    .line 136
    move-result v4

    .line 137
    if-ge v4, v3, :cond_3

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 140
    .line 141
    .line 142
    move-result v4

    .line 143
    if-le v4, v3, :cond_3

    .line 144
    .line 145
    new-instance v4, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 146
    .line 147
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    add-int/lit8 v8, v3, -0x1

    .line 152
    .line 153
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 154
    .line 155
    .line 156
    move-result v9

    .line 157
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 158
    .line 159
    .line 160
    move-result v10

    .line 161
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 162
    .line 163
    .line 164
    move-result v11

    .line 165
    move-object v6, v4

    .line 166
    invoke-direct/range {v6 .. v11}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 167
    .line 168
    .line 169
    new-instance v5, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 170
    .line 171
    add-int/lit8 v13, v3, 0x1

    .line 172
    .line 173
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 174
    .line 175
    .line 176
    move-result v14

    .line 177
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 178
    .line 179
    .line 180
    move-result v15

    .line 181
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getStyle()I

    .line 182
    .line 183
    .line 184
    move-result v16

    .line 185
    invoke-virtual {v2}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 186
    .line 187
    .line 188
    move-result v17

    .line 189
    move-object v12, v5

    .line 190
    invoke-direct/range {v12 .. v17}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setFirstCol(I)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setLastCol(I)V

    .line 197
    .line 198
    .line 199
    int-to-float v1, v1

    .line 200
    invoke-virtual {v2, v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->setColWidth(F)V

    .line 201
    .line 202
    .line 203
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 204
    .line 205
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    .line 207
    .line 208
    iget-object v1, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 209
    .line 210
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    .line 212
    .line 213
    return-void

    .line 214
    :cond_3
    move v2, v5

    .line 215
    goto/16 :goto_0

    .line 216
    .line 217
    :cond_4
    iget-object v7, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 218
    .line 219
    new-instance v8, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 220
    .line 221
    int-to-float v4, v1

    .line 222
    const/4 v5, 0x0

    .line 223
    const/4 v6, 0x0

    .line 224
    move-object v1, v8

    .line 225
    move/from16 v2, p1

    .line 226
    .line 227
    move/from16 v3, p1

    .line 228
    .line 229
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 230
    .line 231
    .line 232
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    .line 234
    .line 235
    goto :goto_1

    .line 236
    :cond_5
    new-instance v7, Ljava/util/ArrayList;

    .line 237
    .line 238
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .line 240
    .line 241
    iput-object v7, v0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 242
    .line 243
    new-instance v8, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 244
    .line 245
    int-to-float v4, v1

    .line 246
    const/4 v5, 0x0

    .line 247
    const/4 v6, 0x0

    .line 248
    move-object v1, v8

    .line 249
    move/from16 v2, p1

    .line 250
    .line 251
    move/from16 v3, p1

    .line 252
    .line 253
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;-><init>(IIFIZ)V

    .line 254
    .line 255
    .line 256
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    .line 258
    .line 259
    :goto_1
    return-void
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public setDefaultColWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDefaultRowHeight(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFirstRowNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->firstRow:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGridsPrinted(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->isGridsPrinted:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLastRowNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->lastRow:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPaneInformation(Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->paneInformation:Lcom/intsig/office/ss/model/sheetProperty/PaneInformation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setReaderListener(Lcom/intsig/office/ss/model/interfacePart/IReaderListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->iReaderListener:Lcom/intsig/office/ss/model/interfacePart/IReaderListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setScroll(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollX:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollY:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setScrollX(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollX:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setScrollY(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->scrollY:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSheetName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->sheetName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSheetType(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->type:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setState(S)V
    .locals 5

    .line 1
    iput-short p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->state:S

    .line 2
    .line 3
    const/4 v0, 0x2

    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->iReaderListener:Lcom/intsig/office/ss/model/interfacePart/IReaderListener;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1}, Lcom/intsig/office/ss/model/interfacePart/IReaderListener;->OnReadingFinished()V

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 15
    .line 16
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->columnInfoList:Ljava/util/List;

    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    if-eqz p1, :cond_2

    .line 22
    .line 23
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_2

    .line 32
    .line 33
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    check-cast v1, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    sub-int/2addr v2, v3

    .line 48
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    add-int/2addr v0, v2

    .line 51
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->isHidden()Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    iget v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 59
    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getColWidth()F

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getLastCol()I

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/sheetProperty/ColumnInfo;->getFirstCol()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    sub-int/2addr v4, v1

    .line 73
    add-int/lit8 v4, v4, 0x1

    .line 74
    .line 75
    int-to-float v1, v4

    .line 76
    mul-float v3, v3, v1

    .line 77
    .line 78
    add-float/2addr v2, v3

    .line 79
    iput v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 83
    .line 84
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->rows:Ljava/util/Map;

    .line 89
    .line 90
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    if-eqz v2, :cond_3

    .line 103
    .line 104
    iget v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 105
    .line 106
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    check-cast v3, Lcom/intsig/office/ss/model/baseModel/Row;

    .line 111
    .line 112
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowPixelHeight()F

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    add-float/2addr v2, v3

    .line 117
    iput v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 121
    .line 122
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isBefore07Version()Z

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    if-nez v1, :cond_4

    .line 127
    .line 128
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 129
    .line 130
    rsub-int v0, v0, 0x4000

    .line 131
    .line 132
    iget v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 133
    .line 134
    mul-int v0, v0, v2

    .line 135
    .line 136
    int-to-float v0, v0

    .line 137
    add-float/2addr v1, v0

    .line 138
    iput v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 139
    .line 140
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 141
    .line 142
    const/high16 v1, 0x100000

    .line 143
    .line 144
    sub-int/2addr v1, p1

    .line 145
    iget p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 146
    .line 147
    mul-int v1, v1, p1

    .line 148
    .line 149
    int-to-float p1, v1

    .line 150
    add-float/2addr v0, p1

    .line 151
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 152
    .line 153
    goto :goto_2

    .line 154
    :cond_4
    iget v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 155
    .line 156
    rsub-int v0, v0, 0x100

    .line 157
    .line 158
    iget v2, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultColWidth:I

    .line 159
    .line 160
    mul-int v0, v0, v2

    .line 161
    .line 162
    int-to-float v0, v0

    .line 163
    add-float/2addr v1, v0

    .line 164
    iput v1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollX:F

    .line 165
    .line 166
    iget v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 167
    .line 168
    const/high16 v1, 0x10000

    .line 169
    .line 170
    sub-int/2addr v1, p1

    .line 171
    iget p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->defaultRowHeight:I

    .line 172
    .line 173
    mul-int v1, v1, p1

    .line 174
    .line 175
    int-to-float p1, v1

    .line 176
    add-float/2addr v0, p1

    .line 177
    iput v0, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->maxScrollY:F

    .line 178
    .line 179
    :goto_2
    return-void
    .line 180
    .line 181
    .line 182
.end method

.method public setWorkbook(Lcom/intsig/office/ss/model/baseModel/Workbook;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/ss/model/baseModel/Sheet;->zoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

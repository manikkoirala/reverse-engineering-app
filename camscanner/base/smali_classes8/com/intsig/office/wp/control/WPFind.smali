.class public Lcom/intsig/office/wp/control/WPFind;
.super Ljava/lang/Object;
.source "WPFind.java"

# interfaces
.implements Lcom/intsig/office/system/IFind;


# instance fields
.field protected findElement:Lcom/intsig/office/simpletext/model/IElement;

.field protected findString:Ljava/lang/String;

.field private isSetPointToVisible:Z

.field private mHighLightList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/simpletext/model/HighLightWord;",
            ">;"
        }
    .end annotation
.end field

.field protected pageIndex:I

.field protected query:Ljava/lang/String;

.field protected rect:Lcom/intsig/office/java/awt/Rectangle;

.field protected relativeParaIndex:I

.field protected word:Lcom/intsig/office/wp/control/Word;


# direct methods
.method public constructor <init>(Lcom/intsig/office/wp/control/Word;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->mHighLightList:Ljava/util/ArrayList;

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 12
    .line 13
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 14
    .line 15
    invoke-direct {p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private addHighlight(II)V
    .locals 6

    .line 1
    iput p1, p0, Lcom/intsig/office/wp/control/WPFind;->relativeParaIndex:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    int-to-long v2, p1

    .line 10
    add-long/2addr v0, v2

    .line 11
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    int-to-long v2, p2

    .line 18
    add-long/2addr v2, v0

    .line 19
    invoke-interface {p1, v0, v1, v2, v3}, Lcom/intsig/office/simpletext/control/IHighlight;->addHighlight(JJ)V

    .line 20
    .line 21
    .line 22
    iget p1, p0, Lcom/intsig/office/wp/control/WPFind;->relativeParaIndex:I

    .line 23
    .line 24
    add-int/2addr p1, p2

    .line 25
    iput p1, p0, Lcom/intsig/office/wp/control/WPFind;->relativeParaIndex:I

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getCurrentRootType()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    const/4 p2, 0x0

    .line 34
    const/4 v2, 0x0

    .line 35
    const/4 v3, 0x2

    .line 36
    if-ne p1, v3, :cond_5

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 39
    .line 40
    invoke-virtual {p1, v2}, Lcom/intsig/office/wp/control/Word;->getRoot(I)Lcom/intsig/office/simpletext/view/IView;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const/4 v3, 0x1

    .line 45
    if-eqz p1, :cond_3

    .line 46
    .line 47
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-nez v4, :cond_3

    .line 52
    .line 53
    check-cast p1, Lcom/intsig/office/wp/view/PageRoot;

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/office/wp/view/PageRoot;->getViewContainer()Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/office/simpletext/view/ViewContainer;->getParagraph(JZ)Lcom/intsig/office/simpletext/view/IView;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    :goto_0
    if-eqz p1, :cond_0

    .line 64
    .line 65
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    const/4 v5, 0x4

    .line 70
    if-eq v4, v5, :cond_0

    .line 71
    .line 72
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    goto :goto_0

    .line 77
    :cond_0
    if-eqz p1, :cond_3

    .line 78
    .line 79
    move-object v4, p1

    .line 80
    check-cast v4, Lcom/intsig/office/wp/view/PageView;

    .line 81
    .line 82
    invoke-virtual {v4}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 83
    .line 84
    .line 85
    move-result v4

    .line 86
    sub-int/2addr v4, v3

    .line 87
    iput v4, p0, Lcom/intsig/office/wp/control/WPFind;->pageIndex:I

    .line 88
    .line 89
    iget-object v5, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 90
    .line 91
    invoke-virtual {v5}, Lcom/intsig/office/wp/control/Word;->getCurrentPageNumber()I

    .line 92
    .line 93
    .line 94
    move-result v5

    .line 95
    sub-int/2addr v5, v3

    .line 96
    if-eq v4, v5, :cond_1

    .line 97
    .line 98
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 99
    .line 100
    iget p2, p0, Lcom/intsig/office/wp/control/WPFind;->pageIndex:I

    .line 101
    .line 102
    const/4 v0, -0x1

    .line 103
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/wp/control/Word;->showPage(II)V

    .line 104
    .line 105
    .line 106
    iput-boolean v3, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_1
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 110
    .line 111
    invoke-virtual {v4, v2, v2, v2, v2}, Lcom/intsig/office/java/awt/Rectangle;->setBounds(IIII)V

    .line 112
    .line 113
    .line 114
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 115
    .line 116
    iget-object v5, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 117
    .line 118
    invoke-virtual {v4, v0, v1, v5, v2}, Lcom/intsig/office/wp/control/Word;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 119
    .line 120
    .line 121
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 122
    .line 123
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 124
    .line 125
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getX()I

    .line 126
    .line 127
    .line 128
    move-result v4

    .line 129
    sub-int/2addr v1, v4

    .line 130
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 133
    .line 134
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 135
    .line 136
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 137
    .line 138
    .line 139
    move-result p1

    .line 140
    sub-int/2addr v1, p1

    .line 141
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 142
    .line 143
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 144
    .line 145
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getPrintWord()Lcom/intsig/office/wp/control/PrintWord;

    .line 146
    .line 147
    .line 148
    move-result-object p1

    .line 149
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/PrintWord;->getListView()Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 154
    .line 155
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 156
    .line 157
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 158
    .line 159
    invoke-virtual {p1, v1, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->isPointVisibleOnScreen(II)Z

    .line 160
    .line 161
    .line 162
    move-result p1

    .line 163
    if-nez p1, :cond_2

    .line 164
    .line 165
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 166
    .line 167
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getPrintWord()Lcom/intsig/office/wp/control/PrintWord;

    .line 168
    .line 169
    .line 170
    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/PrintWord;->getListView()Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    iget-object p2, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 176
    .line 177
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 178
    .line 179
    iget p2, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 180
    .line 181
    invoke-virtual {p1, v0, p2}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setItemPointVisibleOnScreen(II)V

    .line 182
    .line 183
    .line 184
    goto :goto_1

    .line 185
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 186
    .line 187
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getPrintWord()Lcom/intsig/office/wp/control/PrintWord;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 192
    .line 193
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getPrintWord()Lcom/intsig/office/wp/control/PrintWord;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/PrintWord;->getListView()Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 198
    .line 199
    .line 200
    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    invoke-virtual {p1, v0, p2}, Lcom/intsig/office/wp/control/PrintWord;->exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V

    .line 206
    .line 207
    .line 208
    :cond_3
    const/4 v2, 0x1

    .line 209
    :goto_1
    if-eqz v2, :cond_4

    .line 210
    .line 211
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 212
    .line 213
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 214
    .line 215
    .line 216
    :cond_4
    return-void

    .line 217
    :cond_5
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 218
    .line 219
    invoke-virtual {p1, v2, v2, v2, v2}, Lcom/intsig/office/java/awt/Rectangle;->setBounds(IIII)V

    .line 220
    .line 221
    .line 222
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 223
    .line 224
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 225
    .line 226
    invoke-virtual {p1, v0, v1, v4, v2}, Lcom/intsig/office/wp/control/Word;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 227
    .line 228
    .line 229
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 230
    .line 231
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getVisibleRect()Lcom/intsig/office/java/awt/Rectangle;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 236
    .line 237
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 238
    .line 239
    .line 240
    move-result v0

    .line 241
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 242
    .line 243
    iget v2, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 244
    .line 245
    int-to-float v2, v2

    .line 246
    mul-float v2, v2, v0

    .line 247
    .line 248
    float-to-int v2, v2

    .line 249
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 250
    .line 251
    int-to-float v1, v1

    .line 252
    mul-float v1, v1, v0

    .line 253
    .line 254
    float-to-int v1, v1

    .line 255
    invoke-virtual {p1, v2, v1}, Lcom/intsig/office/java/awt/Rectangle;->contains(II)Z

    .line 256
    .line 257
    .line 258
    move-result v4

    .line 259
    if-nez v4, :cond_8

    .line 260
    .line 261
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 262
    .line 263
    add-int/2addr v4, v2

    .line 264
    int-to-float v4, v4

    .line 265
    iget-object v5, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 266
    .line 267
    invoke-virtual {v5}, Lcom/intsig/office/wp/control/Word;->getWordWidth()I

    .line 268
    .line 269
    .line 270
    move-result v5

    .line 271
    int-to-float v5, v5

    .line 272
    mul-float v5, v5, v0

    .line 273
    .line 274
    cmpl-float v4, v4, v5

    .line 275
    .line 276
    if-lez v4, :cond_6

    .line 277
    .line 278
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 279
    .line 280
    invoke-virtual {v2}, Lcom/intsig/office/wp/control/Word;->getWordWidth()I

    .line 281
    .line 282
    .line 283
    move-result v2

    .line 284
    int-to-float v2, v2

    .line 285
    mul-float v2, v2, v0

    .line 286
    .line 287
    float-to-int v2, v2

    .line 288
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 289
    .line 290
    sub-int/2addr v2, v4

    .line 291
    :cond_6
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 292
    .line 293
    add-int/2addr v4, v1

    .line 294
    int-to-float v4, v4

    .line 295
    iget-object v5, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 296
    .line 297
    invoke-virtual {v5}, Lcom/intsig/office/wp/control/Word;->getWordHeight()I

    .line 298
    .line 299
    .line 300
    move-result v5

    .line 301
    int-to-float v5, v5

    .line 302
    mul-float v5, v5, v0

    .line 303
    .line 304
    cmpl-float v4, v4, v5

    .line 305
    .line 306
    if-lez v4, :cond_7

    .line 307
    .line 308
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 309
    .line 310
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getWordHeight()I

    .line 311
    .line 312
    .line 313
    move-result v1

    .line 314
    int-to-float v1, v1

    .line 315
    mul-float v1, v1, v0

    .line 316
    .line 317
    float-to-int v0, v1

    .line 318
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 319
    .line 320
    sub-int v1, v0, p1

    .line 321
    .line 322
    :cond_7
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 323
    .line 324
    invoke-virtual {p1, v2, v1}, Lcom/intsig/office/wp/control/Word;->scrollTo(II)V

    .line 325
    .line 326
    .line 327
    goto :goto_2

    .line 328
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 329
    .line 330
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 331
    .line 332
    .line 333
    :goto_2
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 334
    .line 335
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 336
    .line 337
    .line 338
    move-result-object p1

    .line 339
    const/16 v0, 0x14

    .line 340
    .line 341
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 342
    .line 343
    .line 344
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 345
    .line 346
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getCurrentRootType()I

    .line 347
    .line 348
    .line 349
    move-result p1

    .line 350
    if-eq p1, v3, :cond_9

    .line 351
    .line 352
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 353
    .line 354
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 355
    .line 356
    .line 357
    move-result-object p1

    .line 358
    const v0, 0x2000000a

    .line 359
    .line 360
    .line 361
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 362
    .line 363
    .line 364
    :cond_9
    return-void
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private addHighlightList(Lcom/intsig/office/simpletext/model/IElement;ILjava/lang/String;)V
    .locals 8

    .line 1
    if-lez p2, :cond_0

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    int-to-long p1, p2

    .line 8
    add-long v3, v0, p1

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->mHighLightList:Ljava/util/ArrayList;

    .line 11
    .line 12
    new-instance p2, Lcom/intsig/office/simpletext/model/HighLightWord;

    .line 13
    .line 14
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 15
    .line 16
    .line 17
    move-result p3

    .line 18
    int-to-long v0, p3

    .line 19
    add-long v5, v3, v0

    .line 20
    .line 21
    const/4 v7, 0x0

    .line 22
    move-object v2, p2

    .line 23
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/simpletext/model/HighLightWord;-><init>(JJLjava/lang/Integer;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private addHighlights(Lcom/intsig/office/simpletext/model/IElement;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/office/wp/control/WPFind;->addHighlightList(Lcom/intsig/office/simpletext/model/IElement;ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    const/4 v1, -0x1

    .line 12
    if-eq v0, v1, :cond_1

    .line 13
    .line 14
    add-int/lit8 v0, v0, 0x1

    .line 15
    .line 16
    invoke-virtual {p2, p3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/office/wp/control/WPFind;->addHighlightList(Lcom/intsig/office/simpletext/model/IElement;ILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private isSameSelectPosition(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Lcom/intsig/office/simpletext/control/IHighlight;->isSelectText()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    int-to-long v2, p1

    .line 20
    add-long/2addr v0, v2

    .line 21
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-interface {p1}, Lcom/intsig/office/simpletext/control/IHighlight;->getSelectStart()J

    .line 28
    .line 29
    .line 30
    move-result-wide v2

    .line 31
    cmp-long p1, v0, v2

    .line 32
    .line 33
    if-nez p1, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 p1, 0x0

    .line 38
    :goto_0
    return p1
.end method

.method private queryAllHighLight(Lcom/intsig/office/simpletext/model/IDocument;J)V
    .locals 1

    .line 1
    invoke-interface {p1, p2, p3}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    :goto_0
    if-eqz p2, :cond_0

    .line 6
    .line 7
    invoke-interface {p2, p1}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p3

    .line 11
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 12
    .line 13
    invoke-direct {p0, p2, p3, v0}, Lcom/intsig/office/wp/control/WPFind;->addHighlights(Lcom/intsig/office/simpletext/model/IElement;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p2}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 17
    .line 18
    .line 19
    move-result-wide p2

    .line 20
    invoke-interface {p1, p2, p3}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->mHighLightList:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-nez p1, :cond_1

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    iget-object p2, p0, Lcom/intsig/office/wp/control/WPFind;->mHighLightList:Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-interface {p1, p2}, Lcom/intsig/office/simpletext/control/IHighlight;->addHighlights(Ljava/util/ArrayList;)V

    .line 42
    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public find(Ljava/lang/String;)Z
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iput-boolean v0, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/wp/control/WPFind;->removeSearch()V

    .line 16
    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/wp/control/Word;->getCurrentRootType()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/4 v3, 0x2

    .line 25
    const/4 v4, 0x0

    .line 26
    const-wide/16 v5, 0x0

    .line 27
    .line 28
    if-ne v2, v3, :cond_3

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getPrintWord()Lcom/intsig/office/wp/control/PrintWord;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/PrintWord;->getCurrentPageView()Lcom/intsig/office/wp/view/PageView;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    :goto_0
    if-eqz v1, :cond_1

    .line 41
    .line 42
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    const/4 v3, 0x5

    .line 47
    if-eq v2, v3, :cond_1

    .line 48
    .line 49
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    goto :goto_0

    .line 54
    :cond_1
    if-eqz v1, :cond_2

    .line 55
    .line 56
    invoke-interface {v1, v4}, Lcom/intsig/office/simpletext/view/IView;->getStartOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    .line 57
    .line 58
    .line 59
    move-result-wide v1

    .line 60
    goto :goto_1

    .line 61
    :cond_2
    move-wide v1, v5

    .line 62
    goto :goto_1

    .line 63
    :cond_3
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 64
    .line 65
    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    int-to-float v3, v3

    .line 70
    div-float/2addr v3, v1

    .line 71
    float-to-int v3, v3

    .line 72
    iget-object v7, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 73
    .line 74
    invoke-virtual {v7}, Landroid/view/View;->getScrollY()I

    .line 75
    .line 76
    .line 77
    move-result v7

    .line 78
    int-to-float v7, v7

    .line 79
    div-float/2addr v7, v1

    .line 80
    float-to-int v1, v7

    .line 81
    invoke-virtual {v2, v3, v1, v0}, Lcom/intsig/office/wp/control/Word;->viewToModel(IIZ)J

    .line 82
    .line 83
    .line 84
    move-result-wide v1

    .line 85
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 86
    .line 87
    invoke-virtual {v3}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-direct {p0, v3, v5, v6}, Lcom/intsig/office/wp/control/WPFind;->queryAllHighLight(Lcom/intsig/office/simpletext/model/IDocument;J)V

    .line 92
    .line 93
    .line 94
    invoke-interface {v3, v1, v2}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    iput-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 99
    .line 100
    :goto_2
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 101
    .line 102
    if-eqz v1, :cond_5

    .line 103
    .line 104
    invoke-interface {v1, v3}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    iput-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 109
    .line 110
    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    if-ltz v1, :cond_4

    .line 115
    .line 116
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 117
    .line 118
    .line 119
    move-result p1

    .line 120
    invoke-direct {p0, v1, p1}, Lcom/intsig/office/wp/control/WPFind;->addHighlight(II)V

    .line 121
    .line 122
    .line 123
    const/4 p1, 0x1

    .line 124
    return p1

    .line 125
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 126
    .line 127
    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 128
    .line 129
    .line 130
    move-result-wide v1

    .line 131
    invoke-interface {v3, v1, v2}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    iput-object v1, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 136
    .line 137
    goto :goto_2

    .line 138
    :cond_5
    iput-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 139
    .line 140
    return v0
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public findBackward()Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    if-eqz v2, :cond_1

    .line 19
    .line 20
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 21
    .line 22
    iget v5, p0, Lcom/intsig/office/wp/control/WPFind;->relativeParaIndex:I

    .line 23
    .line 24
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 25
    .line 26
    .line 27
    move-result v6

    .line 28
    mul-int/lit8 v6, v6, 0x2

    .line 29
    .line 30
    sub-int/2addr v5, v6

    .line 31
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ltz v2, :cond_1

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/wp/control/WPFind;->addHighlight(II)V

    .line 44
    .line 45
    .line 46
    return v3

    .line 47
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 48
    .line 49
    const-wide/16 v4, 0x1

    .line 50
    .line 51
    if-nez v2, :cond_2

    .line 52
    .line 53
    const-wide/16 v6, 0x0

    .line 54
    .line 55
    invoke-interface {v0, v6, v7}, Lcom/intsig/office/simpletext/model/IDocument;->getLength(J)J

    .line 56
    .line 57
    .line 58
    move-result-wide v6

    .line 59
    goto :goto_0

    .line 60
    :cond_2
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 61
    .line 62
    .line 63
    move-result-wide v6

    .line 64
    :goto_0
    sub-long/2addr v6, v4

    .line 65
    invoke-interface {v0, v6, v7}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 70
    .line 71
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 72
    .line 73
    if-eqz v2, :cond_5

    .line 74
    .line 75
    invoke-interface {v2, v0}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 80
    .line 81
    iget-object v6, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 82
    .line 83
    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    if-ltz v2, :cond_3

    .line 88
    .line 89
    invoke-direct {p0, v2}, Lcom/intsig/office/wp/control/WPFind;->isSameSelectPosition(I)Z

    .line 90
    .line 91
    .line 92
    move-result v6

    .line 93
    if-eqz v6, :cond_3

    .line 94
    .line 95
    iget-object v6, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 96
    .line 97
    iget-object v7, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    sub-int/2addr v2, v8

    .line 104
    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    :cond_3
    if-ltz v2, :cond_4

    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 111
    .line 112
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/wp/control/WPFind;->addHighlight(II)V

    .line 117
    .line 118
    .line 119
    return v3

    .line 120
    :cond_4
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 121
    .line 122
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 123
    .line 124
    .line 125
    move-result-wide v6

    .line 126
    sub-long/2addr v6, v4

    .line 127
    invoke-interface {v0, v6, v7}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 132
    .line 133
    goto :goto_1

    .line 134
    :cond_5
    const/4 v0, 0x0

    .line 135
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 136
    .line 137
    return v1
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public findForward()Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    if-eqz v2, :cond_1

    .line 19
    .line 20
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 21
    .line 22
    iget v5, p0, Lcom/intsig/office/wp/control/WPFind;->relativeParaIndex:I

    .line 23
    .line 24
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-ltz v2, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/wp/control/WPFind;->addHighlight(II)V

    .line 37
    .line 38
    .line 39
    return v3

    .line 40
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 41
    .line 42
    if-nez v2, :cond_2

    .line 43
    .line 44
    const-wide/16 v4, 0x0

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 48
    .line 49
    .line 50
    move-result-wide v4

    .line 51
    :goto_0
    invoke-interface {v0, v4, v5}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 56
    .line 57
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 58
    .line 59
    if-eqz v2, :cond_5

    .line 60
    .line 61
    invoke-interface {v2, v0}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 66
    .line 67
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 68
    .line 69
    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    if-ltz v2, :cond_3

    .line 74
    .line 75
    invoke-direct {p0, v2}, Lcom/intsig/office/wp/control/WPFind;->isSameSelectPosition(I)Z

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    if-eqz v4, :cond_3

    .line 80
    .line 81
    iget-object v4, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 82
    .line 83
    iget-object v5, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 84
    .line 85
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 86
    .line 87
    .line 88
    move-result v6

    .line 89
    add-int/2addr v2, v6

    .line 90
    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    :cond_3
    if-ltz v2, :cond_4

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->query:Ljava/lang/String;

    .line 97
    .line 98
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    invoke-direct {p0, v2, v0}, Lcom/intsig/office/wp/control/WPFind;->addHighlight(II)V

    .line 103
    .line 104
    .line 105
    return v3

    .line 106
    :cond_4
    iget-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 107
    .line 108
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 109
    .line 110
    .line 111
    move-result-wide v4

    .line 112
    invoke-interface {v0, v4, v5}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 113
    .line 114
    .line 115
    move-result-object v2

    .line 116
    iput-object v2, p0, Lcom/intsig/office/wp/control/WPFind;->findElement:Lcom/intsig/office/simpletext/model/IElement;

    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_5
    const/4 v0, 0x0

    .line 120
    iput-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->findString:Ljava/lang/String;

    .line 121
    .line 122
    return v1
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/wp/control/WPFind;->pageIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetPointToVisible()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->mHighLightList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/intsig/office/simpletext/control/IHighlight;->removeHighlight()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/wp/control/WPFind;->word:Lcom/intsig/office/wp/control/Word;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public resetSearchResult()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setSetPointToVisible(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/wp/control/WPFind;->isSetPointToVisible:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/office/wp/model/WPDocument;
.super Lcom/intsig/office/simpletext/model/STDocument;
.source "WPDocument.java"


# instance fields
.field private pageBG:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

.field private root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

.field private table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/STDocument;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x6

    .line 5
    new-array v1, v0, [Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 6
    .line 7
    iput-object v1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 8
    .line 9
    new-array v0, v0, [Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    const/4 v0, 0x4

    .line 14
    new-array v0, v0, [Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/office/wp/model/WPDocument;->initRoot()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getRootCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;
    .locals 3

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr p1, v0

    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    cmp-long v2, p1, v0

    .line 7
    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 11
    .line 12
    const/4 p2, 0x0

    .line 13
    aget-object p1, p1, p2

    .line 14
    .line 15
    return-object p1

    .line 16
    :cond_0
    const-wide/high16 v0, 0x1000000000000000L

    .line 17
    .line 18
    cmp-long v2, p1, v0

    .line 19
    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 23
    .line 24
    const/4 p2, 0x1

    .line 25
    aget-object p1, p1, p2

    .line 26
    .line 27
    return-object p1

    .line 28
    :cond_1
    const-wide/high16 v0, 0x2000000000000000L

    .line 29
    .line 30
    cmp-long v2, p1, v0

    .line 31
    .line 32
    if-nez v2, :cond_2

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 35
    .line 36
    const/4 p2, 0x2

    .line 37
    aget-object p1, p1, p2

    .line 38
    .line 39
    return-object p1

    .line 40
    :cond_2
    const-wide/high16 v0, 0x3000000000000000L    # 1.727233711018889E-77

    .line 41
    .line 42
    cmp-long v2, p1, v0

    .line 43
    .line 44
    if-nez v2, :cond_3

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 47
    .line 48
    const/4 p2, 0x3

    .line 49
    aget-object p1, p1, p2

    .line 50
    .line 51
    return-object p1

    .line 52
    :cond_3
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 53
    .line 54
    cmp-long v2, p1, v0

    .line 55
    .line 56
    if-nez v2, :cond_4

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 59
    .line 60
    const/4 p2, 0x4

    .line 61
    aget-object p1, p1, p2

    .line 62
    .line 63
    return-object p1

    .line 64
    :cond_4
    const-wide/high16 v0, 0x5000000000000000L    # 2.315841784746324E77

    .line 65
    .line 66
    cmp-long v2, p1, v0

    .line 67
    .line 68
    if-nez v2, :cond_5

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 71
    .line 72
    const/4 p2, 0x5

    .line 73
    aget-object p1, p1, p2

    .line 74
    .line 75
    return-object p1

    .line 76
    :cond_5
    const/4 p1, 0x0

    .line 77
    return-object p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 2

    .line 1
    const-wide v0, 0xfffffff00000000L

    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    and-long/2addr p1, v0

    .line 7
    const/16 v0, 0x20

    .line 8
    .line 9
    shr-long/2addr p1, v0

    .line 10
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 11
    .line 12
    const/4 v1, 0x5

    .line 13
    aget-object v0, v0, v1

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    long-to-int p2, p1

    .line 18
    invoke-virtual {v0, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    return-object p1

    .line 23
    :cond_0
    const/4 p1, 0x0

    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private initRoot()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 4
    .line 5
    const/4 v2, 0x5

    .line 6
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 7
    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    aput-object v1, v0, v3

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 15
    .line 16
    const/4 v4, 0x3

    .line 17
    invoke-direct {v1, v4}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 18
    .line 19
    .line 20
    const/4 v5, 0x1

    .line 21
    aput-object v1, v0, v5

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 24
    .line 25
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 26
    .line 27
    invoke-direct {v1, v4}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 28
    .line 29
    .line 30
    const/4 v6, 0x2

    .line 31
    aput-object v1, v0, v6

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 34
    .line 35
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 36
    .line 37
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 38
    .line 39
    .line 40
    aput-object v1, v0, v4

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 45
    .line 46
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 47
    .line 48
    .line 49
    const/4 v7, 0x4

    .line 50
    aput-object v1, v0, v7

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 55
    .line 56
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 57
    .line 58
    .line 59
    aput-object v1, v0, v2

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 62
    .line 63
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 64
    .line 65
    const/16 v8, 0x64

    .line 66
    .line 67
    invoke-direct {v1, v8}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 68
    .line 69
    .line 70
    aput-object v1, v0, v3

    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 73
    .line 74
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 75
    .line 76
    invoke-direct {v1, v4}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 77
    .line 78
    .line 79
    aput-object v1, v0, v5

    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 82
    .line 83
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 84
    .line 85
    invoke-direct {v1, v4}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 86
    .line 87
    .line 88
    aput-object v1, v0, v6

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 91
    .line 92
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 93
    .line 94
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 95
    .line 96
    .line 97
    aput-object v1, v0, v4

    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 100
    .line 101
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 102
    .line 103
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 104
    .line 105
    .line 106
    aput-object v1, v0, v7

    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 109
    .line 110
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 111
    .line 112
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 113
    .line 114
    .line 115
    aput-object v1, v0, v2

    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 118
    .line 119
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 120
    .line 121
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 122
    .line 123
    .line 124
    aput-object v1, v0, v3

    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 127
    .line 128
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 129
    .line 130
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 131
    .line 132
    .line 133
    aput-object v1, v0, v5

    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 136
    .line 137
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 138
    .line 139
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 140
    .line 141
    .line 142
    aput-object v1, v0, v6

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 145
    .line 146
    new-instance v1, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 147
    .line 148
    invoke-direct {v1, v2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 149
    .line 150
    .line 151
    aput-object v1, v0, v4

    .line 152
    .line 153
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getRootCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    if-eqz p2, :cond_1

    .line 16
    .line 17
    invoke-virtual {p2, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V
    .locals 5

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getType()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    if-ne v0, v1, :cond_1

    .line 7
    .line 8
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getTableCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    invoke-virtual {p2, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void

    .line 18
    :cond_1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 19
    .line 20
    and-long/2addr v0, p2

    .line 21
    const-wide/high16 v2, 0x5000000000000000L    # 2.315841784746324E77

    .line 22
    .line 23
    cmp-long v4, v0, v2

    .line 24
    .line 25
    if-nez v4, :cond_2

    .line 26
    .line 27
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    check-cast v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 34
    .line 35
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_2
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getParaCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    if-eqz p2, :cond_3

    .line 44
    .line 45
    invoke-virtual {p2, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 46
    .line 47
    .line 48
    :cond_3
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public appendSection(Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 5

    .line 1
    invoke-super {p0}, Lcom/intsig/office/simpletext/model/STDocument;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    array-length v4, v3

    .line 14
    if-ge v0, v4, :cond_0

    .line 15
    .line 16
    aget-object v3, v3, v0

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->dispose()V

    .line 19
    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 22
    .line 23
    aput-object v2, v3, v0

    .line 24
    .line 25
    add-int/lit8 v0, v0, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iput-object v2, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 29
    .line 30
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 31
    .line 32
    if-eqz v0, :cond_3

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 36
    .line 37
    array-length v4, v3

    .line 38
    if-ge v0, v4, :cond_2

    .line 39
    .line 40
    aget-object v3, v3, v0

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->dispose()V

    .line 43
    .line 44
    .line 45
    iget-object v3, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 46
    .line 47
    aput-object v2, v3, v0

    .line 48
    .line 49
    add-int/lit8 v0, v0, 0x1

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_2
    iput-object v2, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 53
    .line 54
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 55
    .line 56
    if-eqz v0, :cond_5

    .line 57
    .line 58
    :goto_2
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 59
    .line 60
    array-length v3, v0

    .line 61
    if-ge v1, v3, :cond_4

    .line 62
    .line 63
    aget-object v0, v0, v1

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->dispose()V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 69
    .line 70
    aput-object v2, v0, v1

    .line 71
    .line 72
    add-int/lit8 v1, v1, 0x1

    .line 73
    .line 74
    goto :goto_2

    .line 75
    :cond_4
    iput-object v2, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 76
    .line 77
    :cond_5
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getFEElement(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getHFElement(JB)Lcom/intsig/office/simpletext/model/IElement;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getRootCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    .line 4
    move-result-object p3

    .line 5
    if-eqz p3, :cond_0

    .line 6
    .line 7
    invoke-virtual {p3, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getLength(J)J
    .locals 6

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getRootCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const-wide/high16 v1, -0x1000000000000000L    # -3.105036184601418E231

    .line 8
    .line 9
    and-long/2addr v1, p1

    .line 10
    const-wide/high16 v3, 0x5000000000000000L    # 2.315841784746324E77

    .line 11
    .line 12
    cmp-long v5, v1, v3

    .line 13
    .line 14
    if-nez v5, :cond_0

    .line 15
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 23
    .line 24
    .line 25
    move-result-wide v0

    .line 26
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 27
    .line 28
    .line 29
    move-result-wide p1

    .line 30
    sub-long/2addr v0, p1

    .line 31
    return-wide v0

    .line 32
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size()I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    add-int/lit8 p1, p1, -0x1

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 43
    .line 44
    .line 45
    move-result-wide p1

    .line 46
    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 52
    .line 53
    .line 54
    move-result-wide v0

    .line 55
    sub-long/2addr p1, v0

    .line 56
    return-wide p1

    .line 57
    :cond_1
    const-wide/16 p1, 0x0

    .line 58
    .line 59
    return-wide p1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getPageBackground()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->pageBG:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParaCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;
    .locals 3

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr p1, v0

    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    cmp-long v2, p1, v0

    .line 7
    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 11
    .line 12
    const/4 p2, 0x0

    .line 13
    aget-object p1, p1, p2

    .line 14
    .line 15
    return-object p1

    .line 16
    :cond_0
    const-wide/high16 v0, 0x1000000000000000L

    .line 17
    .line 18
    cmp-long v2, p1, v0

    .line 19
    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 23
    .line 24
    const/4 p2, 0x1

    .line 25
    aget-object p1, p1, p2

    .line 26
    .line 27
    return-object p1

    .line 28
    :cond_1
    const-wide/high16 v0, 0x2000000000000000L

    .line 29
    .line 30
    cmp-long v2, p1, v0

    .line 31
    .line 32
    if-nez v2, :cond_2

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 35
    .line 36
    const/4 p2, 0x2

    .line 37
    aget-object p1, p1, p2

    .line 38
    .line 39
    return-object p1

    .line 40
    :cond_2
    const-wide/high16 v0, 0x3000000000000000L    # 1.727233711018889E-77

    .line 41
    .line 42
    cmp-long v2, p1, v0

    .line 43
    .line 44
    if-nez v2, :cond_3

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 47
    .line 48
    const/4 p2, 0x3

    .line 49
    aget-object p1, p1, p2

    .line 50
    .line 51
    return-object p1

    .line 52
    :cond_3
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 53
    .line 54
    cmp-long v2, p1, v0

    .line 55
    .line 56
    if-nez v2, :cond_4

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 59
    .line 60
    const/4 p2, 0x4

    .line 61
    aget-object p1, p1, p2

    .line 62
    .line 63
    return-object p1

    .line 64
    :cond_4
    const-wide/high16 v0, 0x5000000000000000L    # 2.315841784746324E77

    .line 65
    .line 66
    cmp-long v2, p1, v0

    .line 67
    .line 68
    if-nez v2, :cond_5

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->para:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 71
    .line 72
    const/4 p2, 0x5

    .line 73
    aget-object p1, p1, p2

    .line 74
    .line 75
    return-object p1

    .line 76
    :cond_5
    const/4 p1, 0x0

    .line 77
    return-object p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getParaCount(J)I
    .locals 5

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr v0, p1

    .line 4
    const-wide/high16 v2, 0x5000000000000000L    # 2.315841784746324E77

    .line 5
    .line 6
    cmp-long v4, v0, v2

    .line 7
    .line 8
    if-nez v4, :cond_0

    .line 9
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/SectionElement;->getParaCollection()Lcom/intsig/office/simpletext/model/IElementCollection;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElementCollection;->size()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1

    .line 27
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getParaCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    return p1

    .line 38
    :cond_1
    const/4 p1, 0x0

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 5

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr v0, p1

    .line 4
    const-wide/high16 v2, 0x5000000000000000L    # 2.315841784746324E77

    .line 5
    .line 6
    cmp-long v4, v0, v2

    .line 7
    .line 8
    if-nez v4, :cond_0

    .line 9
    .line 10
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/SectionElement;->getParaCollection()Lcom/intsig/office/simpletext/model/IElementCollection;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-interface {v0, p1, p2}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1

    .line 27
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getParaCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    return-object p1

    .line 38
    :cond_1
    const/4 p1, 0x0

    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getParagraph0(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 3

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-ltz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/wp/model/WPDocument;->getTableCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    invoke-virtual {v1, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    return-object p1

    .line 30
    :cond_0
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParagraphForIndex(IJ)Lcom/intsig/office/simpletext/model/IElement;
    .locals 5

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr v0, p2

    .line 4
    const-wide/high16 v2, 0x5000000000000000L    # 2.315841784746324E77

    .line 5
    .line 6
    cmp-long v4, v0, v2

    .line 7
    .line 8
    if-nez v4, :cond_0

    .line 9
    .line 10
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getTextboxSectionElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/SectionElement;->getParaCollection()Lcom/intsig/office/simpletext/model/IElementCollection;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    invoke-interface {p2, p1}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1

    .line 27
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/wp/model/WPDocument;->getParaCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    if-eqz p2, :cond_1

    .line 32
    .line 33
    invoke-virtual {p2, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    return-object p1

    .line 38
    :cond_1
    const/4 p1, 0x0

    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getSection(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableCollection(J)Lcom/intsig/office/simpletext/model/ElementCollectionImpl;
    .locals 3

    .line 1
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    .line 2
    .line 3
    and-long/2addr p1, v0

    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    cmp-long v2, p1, v0

    .line 7
    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 11
    .line 12
    const/4 p2, 0x0

    .line 13
    aget-object p1, p1, p2

    .line 14
    .line 15
    return-object p1

    .line 16
    :cond_0
    const-wide/high16 v0, 0x1000000000000000L

    .line 17
    .line 18
    cmp-long v2, p1, v0

    .line 19
    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 23
    .line 24
    const/4 p2, 0x1

    .line 25
    aget-object p1, p1, p2

    .line 26
    .line 27
    return-object p1

    .line 28
    :cond_1
    const-wide/high16 v0, 0x2000000000000000L

    .line 29
    .line 30
    cmp-long v2, p1, v0

    .line 31
    .line 32
    if-nez v2, :cond_2

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 35
    .line 36
    const/4 p2, 0x2

    .line 37
    aget-object p1, p1, p2

    .line 38
    .line 39
    return-object p1

    .line 40
    :cond_2
    const-wide/high16 v0, 0x5000000000000000L    # 2.315841784746324E77

    .line 41
    .line 42
    cmp-long v2, p1, v0

    .line 43
    .line 44
    if-nez v2, :cond_3

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->table:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 47
    .line 48
    const/4 p2, 0x3

    .line 49
    aget-object p1, p1, p2

    .line 50
    .line 51
    return-object p1

    .line 52
    :cond_3
    const/4 p1, 0x0

    .line 53
    return-object p1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getTextboxSectionElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/WPDocument;->root:[Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    aget-object v0, v0, v1

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPageBackground(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/wp/model/WPDocument;->pageBG:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

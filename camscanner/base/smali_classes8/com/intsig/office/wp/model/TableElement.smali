.class public Lcom/intsig/office/wp/model/TableElement;
.super Lcom/intsig/office/simpletext/model/ParagraphElement;
.source "TableElement.java"


# instance fields
.field private rowElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 5
    .line 6
    const/16 v1, 0xa

    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/wp/model/TableElement;->rowElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public appendRow(Lcom/intsig/office/wp/model/RowElement;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/TableElement;->rowElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/TableElement;->rowElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getRowElement(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/TableElement;->rowElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;
    .locals 0

    .line 1
    const-string p1, ""

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/wp/model/RowElement;
.super Lcom/intsig/office/simpletext/model/AbstractElement;
.source "RowElement.java"


# instance fields
.field private cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/AbstractElement;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 5
    .line 6
    const/16 v1, 0xa

    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public appendCell(Lcom/intsig/office/wp/model/CellElement;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getCellElement(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getCellNumber()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public insertElementForIndex(Lcom/intsig/office/simpletext/model/IElement;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/model/RowElement;->cellElement:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->insertElementForIndex(Lcom/intsig/office/simpletext/model/IElement;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

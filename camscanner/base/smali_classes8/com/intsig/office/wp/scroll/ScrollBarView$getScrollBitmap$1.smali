.class public final Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;
.super Lcom/bumptech/glide/request/target/CustomTarget;
.source "ScrollBarView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/office/wp/scroll/ScrollBarView;->getScrollBitmap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bumptech/glide/request/target/CustomTarget<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;


# direct methods
.method constructor <init>(Lcom/intsig/office/wp/scroll/ScrollBarView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/bumptech/glide/request/target/CustomTarget;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public onLoadCleared(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/bumptech/glide/request/transition/Transition<",
            "-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    const-string p2, "resource"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object p2, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    invoke-static {p2, p1}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$setBitmapScroll$p(Lcom/intsig/office/wp/scroll/ScrollBarView;Landroid/graphics/Bitmap;)V

    .line 3
    iget-object p1, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    invoke-static {p1}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$getBitmapScroll$p(Lcom/intsig/office/wp/scroll/ScrollBarView;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    .line 4
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    .line 6
    invoke-static {p2}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$getRectBitmap$p(Lcom/intsig/office/wp/scroll/ScrollBarView;)Landroid/graphics/Rect;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, p1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 7
    invoke-static {p2}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$setRectScroll(Lcom/intsig/office/wp/scroll/ScrollBarView;)V

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    invoke-static {p1}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$getRectBitmap$p(Lcom/intsig/office/wp/scroll/ScrollBarView;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    iget-object p2, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    invoke-static {p2}, Lcom/intsig/office/wp/scroll/ScrollBarView;->access$getRectBitmap$p(Lcom/intsig/office/wp/scroll/ScrollBarView;)Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onResourceReady: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "ScrollBarView"

    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object p1, p0, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->this$0:Lcom/intsig/office/wp/scroll/ScrollBarView;

    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    return-void
.end method

.method public bridge synthetic onResourceReady(Ljava/lang/Object;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/wp/scroll/ScrollBarView$getScrollBitmap$1;->onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V

    return-void
.end method

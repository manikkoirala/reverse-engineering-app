.class public Lcom/intsig/office/wp/view/PageView;
.super Lcom/intsig/office/simpletext/view/AbstractView;
.source "PageView.java"


# instance fields
.field private footer:Lcom/intsig/office/wp/view/TitleView;

.field private hasBreakTable:Z

.field private header:Lcom/intsig/office/wp/view/TitleView;

.field private pageBRColor:I

.field private pageBorder:I

.field private pageNumber:I

.field private paint:Landroid/graphics/Paint;

.field private shapeViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/wp/view/LeafView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/view/AbstractView;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/office/wp/view/PageView;->pageBorder:I

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 8
    .line 9
    new-instance p1, Landroid/graphics/Paint;

    .line 10
    .line 11
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 15
    .line 16
    const/high16 v0, 0x40000000    # 2.0f

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private checkUpdateHeaderFooterFieldText(Lcom/intsig/office/wp/view/TitleView;I)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_3

    .line 4
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_2

    .line 5
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_1

    .line 6
    instance-of v3, v2, Lcom/intsig/office/wp/view/LeafView;

    if-eqz v3, :cond_0

    .line 7
    move-object v3, v2

    check-cast v3, Lcom/intsig/office/wp/view/LeafView;

    invoke-virtual {v3}, Lcom/intsig/office/wp/view/LeafView;->hasUpdatedFieldText()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 8
    invoke-virtual {v3, p2}, Lcom/intsig/office/wp/view/LeafView;->setNumPages(I)V

    const/4 v0, 0x1

    .line 9
    :cond_0
    invoke-interface {v2}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v2

    goto :goto_2

    .line 10
    :cond_1
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v1

    goto :goto_1

    .line 11
    :cond_2
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object p1

    goto :goto_0

    :cond_3
    return v0
.end method

.method private drawBackground(Landroid/graphics/Canvas;IIF)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    mul-float v0, v0, p4

    .line 7
    .line 8
    float-to-int v0, v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v1, v1

    .line 14
    mul-float v1, v1, p4

    .line 15
    .line 16
    float-to-int v1, v1

    .line 17
    new-instance v6, Landroid/graphics/Rect;

    .line 18
    .line 19
    add-int/2addr v0, p2

    .line 20
    add-int/2addr v1, p3

    .line 21
    invoke-direct {v6, p2, p3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/office/wp/model/WPDocument;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/wp/model/WPDocument;->getPageBackground()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 31
    .line 32
    .line 33
    move-result-object v5

    .line 34
    if-eqz v5, :cond_0

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    iget v4, p0, Lcom/intsig/office/wp/view/PageView;->pageNumber:I

    .line 41
    .line 42
    const/4 v7, 0x0

    .line 43
    move-object v2, p1

    .line 44
    move v8, p4

    .line 45
    invoke-static/range {v2 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;F)Z

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    iget-object p4, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 50
    .line 51
    const/4 v2, -0x1

    .line 52
    invoke-virtual {p4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    .line 54
    .line 55
    int-to-float v4, p2

    .line 56
    int-to-float v5, p3

    .line 57
    int-to-float v6, v0

    .line 58
    int-to-float v7, v1

    .line 59
    iget-object v8, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 60
    .line 61
    move-object v3, p1

    .line 62
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 63
    .line 64
    .line 65
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawBorder(Landroid/graphics/Canvas;IIF)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/office/wp/view/PageView;->pageBorder:I

    .line 4
    .line 5
    if-ltz v1, :cond_c

    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    int-to-float v1, v1

    .line 12
    mul-float v1, v1, p4

    .line 13
    .line 14
    float-to-int v1, v1

    .line 15
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    int-to-float v2, v2

    .line 20
    mul-float v2, v2, p4

    .line 21
    .line 22
    float-to-int v2, v2

    .line 23
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getBordersManage()Lcom/intsig/office/common/borders/BordersManage;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    iget v4, v0, Lcom/intsig/office/wp/view/PageView;->pageBorder:I

    .line 36
    .line 37
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/borders/BordersManage;->getBorders(I)Lcom/intsig/office/common/borders/Borders;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    iget-object v4, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 42
    .line 43
    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    if-eqz v3, :cond_b

    .line 48
    .line 49
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Borders;->getLeftBorder()Lcom/intsig/office/common/borders/Border;

    .line 50
    .line 51
    .line 52
    move-result-object v5

    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Borders;->getTopBorder()Lcom/intsig/office/common/borders/Border;

    .line 54
    .line 55
    .line 56
    move-result-object v6

    .line 57
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Borders;->getRightBorder()Lcom/intsig/office/common/borders/Border;

    .line 58
    .line 59
    .line 60
    move-result-object v7

    .line 61
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Borders;->getBottomBorder()Lcom/intsig/office/common/borders/Border;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    const/4 v8, 0x0

    .line 66
    if-eqz v5, :cond_2

    .line 67
    .line 68
    iget-object v9, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 69
    .line 70
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getColor()I

    .line 71
    .line 72
    .line 73
    move-result v10

    .line 74
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 78
    .line 79
    .line 80
    move-result v9

    .line 81
    int-to-float v9, v9

    .line 82
    mul-float v9, v9, p4

    .line 83
    .line 84
    float-to-int v9, v9

    .line 85
    add-int v9, v9, p2

    .line 86
    .line 87
    if-nez v6, :cond_0

    .line 88
    .line 89
    const/4 v10, 0x0

    .line 90
    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 92
    .line 93
    .line 94
    move-result v10

    .line 95
    int-to-float v10, v10

    .line 96
    mul-float v10, v10, p4

    .line 97
    .line 98
    float-to-int v10, v10

    .line 99
    :goto_0
    add-int v10, v10, p3

    .line 100
    .line 101
    int-to-float v11, v2

    .line 102
    if-nez v3, :cond_1

    .line 103
    .line 104
    goto :goto_1

    .line 105
    :cond_1
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 106
    .line 107
    .line 108
    move-result v12

    .line 109
    int-to-float v12, v12

    .line 110
    mul-float v12, v12, p4

    .line 111
    .line 112
    sub-float/2addr v11, v12

    .line 113
    :goto_1
    float-to-int v11, v11

    .line 114
    add-int v11, v11, p3

    .line 115
    .line 116
    int-to-float v15, v9

    .line 117
    int-to-float v14, v10

    .line 118
    int-to-float v9, v11

    .line 119
    iget-object v10, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 120
    .line 121
    move-object/from16 v12, p1

    .line 122
    .line 123
    move v13, v15

    .line 124
    move/from16 v16, v9

    .line 125
    .line 126
    move-object/from16 v17, v10

    .line 127
    .line 128
    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 129
    .line 130
    .line 131
    :cond_2
    if-eqz v6, :cond_5

    .line 132
    .line 133
    iget-object v9, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 134
    .line 135
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getColor()I

    .line 136
    .line 137
    .line 138
    move-result v10

    .line 139
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 143
    .line 144
    .line 145
    move-result v9

    .line 146
    int-to-float v9, v9

    .line 147
    mul-float v9, v9, p4

    .line 148
    .line 149
    float-to-int v9, v9

    .line 150
    add-int v9, v9, p3

    .line 151
    .line 152
    if-nez v5, :cond_3

    .line 153
    .line 154
    const/4 v10, 0x0

    .line 155
    goto :goto_2

    .line 156
    :cond_3
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 157
    .line 158
    .line 159
    move-result v10

    .line 160
    int-to-float v10, v10

    .line 161
    mul-float v10, v10, p4

    .line 162
    .line 163
    float-to-int v10, v10

    .line 164
    :goto_2
    add-int v10, v10, p2

    .line 165
    .line 166
    add-int/lit8 v10, v10, -0x1

    .line 167
    .line 168
    int-to-float v11, v1

    .line 169
    if-nez v7, :cond_4

    .line 170
    .line 171
    goto :goto_3

    .line 172
    :cond_4
    invoke-virtual {v7}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 173
    .line 174
    .line 175
    move-result v12

    .line 176
    int-to-float v12, v12

    .line 177
    mul-float v12, v12, p4

    .line 178
    .line 179
    sub-float/2addr v11, v12

    .line 180
    :goto_3
    float-to-int v11, v11

    .line 181
    add-int v11, v11, p2

    .line 182
    .line 183
    add-int/lit8 v11, v11, 0x1

    .line 184
    .line 185
    int-to-float v13, v10

    .line 186
    int-to-float v9, v9

    .line 187
    int-to-float v15, v11

    .line 188
    iget-object v10, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 189
    .line 190
    move-object/from16 v12, p1

    .line 191
    .line 192
    move v14, v9

    .line 193
    move/from16 v16, v9

    .line 194
    .line 195
    move-object/from16 v17, v10

    .line 196
    .line 197
    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 198
    .line 199
    .line 200
    :cond_5
    if-eqz v7, :cond_8

    .line 201
    .line 202
    iget-object v9, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 203
    .line 204
    invoke-virtual {v7}, Lcom/intsig/office/common/borders/Border;->getColor()I

    .line 205
    .line 206
    .line 207
    move-result v10

    .line 208
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 209
    .line 210
    .line 211
    int-to-float v9, v1

    .line 212
    invoke-virtual {v7}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 213
    .line 214
    .line 215
    move-result v10

    .line 216
    int-to-float v10, v10

    .line 217
    mul-float v10, v10, p4

    .line 218
    .line 219
    sub-float/2addr v9, v10

    .line 220
    float-to-int v9, v9

    .line 221
    add-int v9, v9, p2

    .line 222
    .line 223
    if-nez v6, :cond_6

    .line 224
    .line 225
    const/4 v10, 0x0

    .line 226
    goto :goto_4

    .line 227
    :cond_6
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 228
    .line 229
    .line 230
    move-result v10

    .line 231
    int-to-float v10, v10

    .line 232
    mul-float v10, v10, p4

    .line 233
    .line 234
    :goto_4
    float-to-int v10, v10

    .line 235
    add-int v10, v10, p3

    .line 236
    .line 237
    int-to-float v11, v2

    .line 238
    if-nez v3, :cond_7

    .line 239
    .line 240
    goto :goto_5

    .line 241
    :cond_7
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 242
    .line 243
    .line 244
    move-result v12

    .line 245
    int-to-float v12, v12

    .line 246
    mul-float v12, v12, p4

    .line 247
    .line 248
    sub-float/2addr v11, v12

    .line 249
    :goto_5
    float-to-int v11, v11

    .line 250
    add-int v11, v11, p3

    .line 251
    .line 252
    int-to-float v15, v9

    .line 253
    int-to-float v14, v10

    .line 254
    int-to-float v9, v11

    .line 255
    iget-object v10, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 256
    .line 257
    move-object/from16 v12, p1

    .line 258
    .line 259
    move v13, v15

    .line 260
    move/from16 v16, v9

    .line 261
    .line 262
    move-object/from16 v17, v10

    .line 263
    .line 264
    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 265
    .line 266
    .line 267
    :cond_8
    if-eqz v3, :cond_b

    .line 268
    .line 269
    iget-object v9, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 270
    .line 271
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Border;->getColor()I

    .line 272
    .line 273
    .line 274
    move-result v3

    .line 275
    invoke-virtual {v9, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 276
    .line 277
    .line 278
    int-to-float v2, v2

    .line 279
    invoke-virtual {v6}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 280
    .line 281
    .line 282
    move-result v3

    .line 283
    int-to-float v3, v3

    .line 284
    mul-float v3, v3, p4

    .line 285
    .line 286
    sub-float/2addr v2, v3

    .line 287
    float-to-int v2, v2

    .line 288
    add-int v2, v2, p3

    .line 289
    .line 290
    if-nez v5, :cond_9

    .line 291
    .line 292
    goto :goto_6

    .line 293
    :cond_9
    invoke-virtual {v5}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 294
    .line 295
    .line 296
    move-result v3

    .line 297
    int-to-float v3, v3

    .line 298
    mul-float v3, v3, p4

    .line 299
    .line 300
    float-to-int v8, v3

    .line 301
    :goto_6
    add-int v8, v8, p2

    .line 302
    .line 303
    add-int/lit8 v8, v8, -0x1

    .line 304
    .line 305
    int-to-float v1, v1

    .line 306
    if-nez v7, :cond_a

    .line 307
    .line 308
    goto :goto_7

    .line 309
    :cond_a
    invoke-virtual {v7}, Lcom/intsig/office/common/borders/Border;->getSpace()S

    .line 310
    .line 311
    .line 312
    move-result v3

    .line 313
    int-to-float v3, v3

    .line 314
    mul-float v3, v3, p4

    .line 315
    .line 316
    sub-float/2addr v1, v3

    .line 317
    :goto_7
    float-to-int v1, v1

    .line 318
    add-int v1, v1, p2

    .line 319
    .line 320
    add-int/lit8 v1, v1, 0x1

    .line 321
    .line 322
    int-to-float v10, v8

    .line 323
    int-to-float v13, v2

    .line 324
    int-to-float v12, v1

    .line 325
    iget-object v14, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 326
    .line 327
    move-object/from16 v9, p1

    .line 328
    .line 329
    move v11, v13

    .line 330
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 331
    .line 332
    .line 333
    :cond_b
    iget-object v1, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 334
    .line 335
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 336
    .line 337
    .line 338
    :cond_c
    return-void
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawPageSeparated(Landroid/graphics/Canvas;IIF)V
    .locals 28

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getLeftIndent()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-float v1, v1

    .line 8
    mul-float v1, v1, p4

    .line 9
    .line 10
    move/from16 v2, p2

    .line 11
    .line 12
    int-to-float v2, v2

    .line 13
    add-float/2addr v1, v2

    .line 14
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getTopIndent()I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    int-to-float v3, v3

    .line 19
    mul-float v3, v3, p4

    .line 20
    .line 21
    move/from16 v4, p3

    .line 22
    .line 23
    int-to-float v9, v4

    .line 24
    add-float v16, v3, v9

    .line 25
    .line 26
    iget-object v3, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 27
    .line 28
    const v4, -0x777778

    .line 29
    .line 30
    .line 31
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    .line 33
    .line 34
    const/high16 v17, 0x3f800000    # 1.0f

    .line 35
    .line 36
    sub-float v18, v1, v17

    .line 37
    .line 38
    const/16 v3, 0x1e

    .line 39
    .line 40
    int-to-float v15, v3

    .line 41
    sub-float v12, v16, v15

    .line 42
    .line 43
    iget-object v8, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 44
    .line 45
    move-object/from16 v3, p1

    .line 46
    .line 47
    move/from16 v4, v18

    .line 48
    .line 49
    move v5, v12

    .line 50
    move v6, v1

    .line 51
    move/from16 v7, v16

    .line 52
    .line 53
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 54
    .line 55
    .line 56
    sub-float v19, v1, v15

    .line 57
    .line 58
    sub-float v20, v16, v17

    .line 59
    .line 60
    iget-object v8, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 61
    .line 62
    move/from16 v4, v19

    .line 63
    .line 64
    move/from16 v5, v20

    .line 65
    .line 66
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getRightIndent()I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    sub-int/2addr v3, v4

    .line 78
    int-to-float v3, v3

    .line 79
    mul-float v3, v3, p4

    .line 80
    .line 81
    add-float/2addr v2, v3

    .line 82
    add-float v24, v2, v17

    .line 83
    .line 84
    iget-object v3, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 85
    .line 86
    move-object/from16 v10, p1

    .line 87
    .line 88
    move v11, v2

    .line 89
    move/from16 v13, v24

    .line 90
    .line 91
    move/from16 v14, v16

    .line 92
    .line 93
    move v4, v15

    .line 94
    move-object v15, v3

    .line 95
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 96
    .line 97
    .line 98
    add-float v27, v2, v4

    .line 99
    .line 100
    iget-object v15, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 101
    .line 102
    move/from16 v12, v20

    .line 103
    .line 104
    move/from16 v13, v27

    .line 105
    .line 106
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 110
    .line 111
    .line 112
    move-result v3

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getBottomIndent()I

    .line 114
    .line 115
    .line 116
    move-result v5

    .line 117
    sub-int/2addr v3, v5

    .line 118
    int-to-float v3, v3

    .line 119
    mul-float v3, v3, p4

    .line 120
    .line 121
    add-float/2addr v9, v3

    .line 122
    add-float v25, v9, v4

    .line 123
    .line 124
    iget-object v8, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 125
    .line 126
    move-object/from16 v3, p1

    .line 127
    .line 128
    move/from16 v4, v18

    .line 129
    .line 130
    move v5, v9

    .line 131
    move/from16 v7, v25

    .line 132
    .line 133
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 134
    .line 135
    .line 136
    add-float v10, v9, v17

    .line 137
    .line 138
    iget-object v8, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 139
    .line 140
    move/from16 v4, v19

    .line 141
    .line 142
    move v7, v10

    .line 143
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 144
    .line 145
    .line 146
    iget-object v1, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 147
    .line 148
    move-object/from16 v21, p1

    .line 149
    .line 150
    move/from16 v22, v2

    .line 151
    .line 152
    move/from16 v23, v9

    .line 153
    .line 154
    move-object/from16 v26, v1

    .line 155
    .line 156
    invoke-virtual/range {v21 .. v26}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 157
    .line 158
    .line 159
    iget-object v1, v0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 160
    .line 161
    move/from16 v24, v27

    .line 162
    .line 163
    move/from16 v25, v10

    .line 164
    .line 165
    move-object/from16 v26, v1

    .line 166
    .line 167
    invoke-virtual/range {v21 .. v26}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 168
    .line 169
    .line 170
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawPaper(Landroid/graphics/Canvas;IIF)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    int-to-float v0, v0

    .line 9
    mul-float v0, v0, p4

    .line 10
    .line 11
    float-to-int v0, v0

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    int-to-float v1, v1

    .line 17
    mul-float v1, v1, p4

    .line 18
    .line 19
    float-to-int p4, v1

    .line 20
    add-int/2addr v0, p2

    .line 21
    add-int/lit8 v1, v0, 0x5

    .line 22
    .line 23
    add-int/2addr p4, p3

    .line 24
    add-int/lit8 v2, p4, 0x5

    .line 25
    .line 26
    invoke-virtual {p1, p2, p3, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 30
    .line 31
    const/high16 v2, -0x1000000

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    .line 35
    .line 36
    int-to-float p2, p2

    .line 37
    int-to-float p3, p3

    .line 38
    int-to-float v0, v0

    .line 39
    iget-object v8, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 40
    .line 41
    move-object v3, p1

    .line 42
    move v4, p2

    .line 43
    move v5, p3

    .line 44
    move v6, v0

    .line 45
    move v7, p3

    .line 46
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 47
    .line 48
    .line 49
    int-to-float p4, p4

    .line 50
    iget-object v8, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 51
    .line 52
    move v6, p2

    .line 53
    move v7, p4

    .line 54
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 55
    .line 56
    .line 57
    iget-object v6, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 58
    .line 59
    move-object v1, p1

    .line 60
    move v2, v0

    .line 61
    move v3, p3

    .line 62
    move v4, v0

    .line 63
    move v5, p4

    .line 64
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 65
    .line 66
    .line 67
    iget-object v8, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 68
    .line 69
    move-object v3, p1

    .line 70
    move v4, p2

    .line 71
    move v6, v0

    .line 72
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private drawShape(Landroid/graphics/Canvas;IIFZ)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_2

    .line 12
    :cond_0
    if-eqz p5, :cond_3

    .line 13
    .line 14
    iget-object p5, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object p5

    .line 20
    :cond_1
    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_6

    .line 25
    .line 26
    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Lcom/intsig/office/wp/view/LeafView;

    .line 31
    .line 32
    instance-of v1, v0, Lcom/intsig/office/wp/view/ShapeView;

    .line 33
    .line 34
    if-eqz v1, :cond_2

    .line 35
    .line 36
    move-object v1, v0

    .line 37
    check-cast v1, Lcom/intsig/office/wp/view/ShapeView;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/wp/view/ShapeView;->isBehindDoc()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_2

    .line 44
    .line 45
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/ShapeView;->drawForWrap(Landroid/graphics/Canvas;IIF)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_2
    instance-of v1, v0, Lcom/intsig/office/wp/view/ObjView;

    .line 50
    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    check-cast v0, Lcom/intsig/office/wp/view/ObjView;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/ObjView;->isBehindDoc()Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-eqz v1, :cond_1

    .line 60
    .line 61
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/ObjView;->drawForWrap(Landroid/graphics/Canvas;IIF)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    iget-object p5, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 66
    .line 67
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object p5

    .line 71
    :cond_4
    :goto_1
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-eqz v0, :cond_6

    .line 76
    .line 77
    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    check-cast v0, Lcom/intsig/office/wp/view/LeafView;

    .line 82
    .line 83
    instance-of v1, v0, Lcom/intsig/office/wp/view/ShapeView;

    .line 84
    .line 85
    if-eqz v1, :cond_5

    .line 86
    .line 87
    move-object v1, v0

    .line 88
    check-cast v1, Lcom/intsig/office/wp/view/ShapeView;

    .line 89
    .line 90
    invoke-virtual {v1}, Lcom/intsig/office/wp/view/ShapeView;->isBehindDoc()Z

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    if-nez v2, :cond_5

    .line 95
    .line 96
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/ShapeView;->drawForWrap(Landroid/graphics/Canvas;IIF)V

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_5
    instance-of v1, v0, Lcom/intsig/office/wp/view/ObjView;

    .line 101
    .line 102
    if-eqz v1, :cond_4

    .line 103
    .line 104
    check-cast v0, Lcom/intsig/office/wp/view/ObjView;

    .line 105
    .line 106
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/ObjView;->isBehindDoc()Z

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    if-nez v1, :cond_4

    .line 111
    .line 112
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/ObjView;->drawForWrap(Landroid/graphics/Canvas;IIF)V

    .line 113
    .line 114
    .line 115
    goto :goto_1

    .line 116
    :cond_6
    :goto_2
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method


# virtual methods
.method public addShapeView(Lcom/intsig/office/wp/view/LeafView;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public checkUpdateHeaderFooterFieldText(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/wp/view/PageView;->checkUpdateHeaderFooterFieldText(Lcom/intsig/office/wp/view/TitleView;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/wp/view/PageView;->checkUpdateHeaderFooterFieldText(Lcom/intsig/office/wp/view/TitleView;I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public dispose()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/TitleView;->dispose()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/TitleView;->dispose()V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 22
    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 28
    .line 29
    .line 30
    iput-object v1, p0, Lcom/intsig/office/wp/view/PageView;->shapeViews:Ljava/util/List;

    .line 31
    .line 32
    :cond_2
    iput-object v1, p0, Lcom/intsig/office/wp/view/PageView;->paint:Landroid/graphics/Paint;

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public draw(Landroid/graphics/Canvas;IIF)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 5
    .line 6
    int-to-float v0, v0

    .line 7
    mul-float v0, v0, p4

    .line 8
    .line 9
    float-to-int v0, v0

    .line 10
    add-int/2addr v0, p2

    .line 11
    iget v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 12
    .line 13
    int-to-float v1, v1

    .line 14
    mul-float v1, v1, p4

    .line 15
    .line 16
    float-to-int v1, v1

    .line 17
    add-int v7, v1, p3

    .line 18
    .line 19
    int-to-float v1, v0

    .line 20
    int-to-float v2, v7

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    int-to-float v3, v3

    .line 26
    mul-float v3, v3, p4

    .line 27
    .line 28
    add-float/2addr v3, v1

    .line 29
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    int-to-float v4, v4

    .line 34
    mul-float v4, v4, p4

    .line 35
    .line 36
    add-float/2addr v4, v2

    .line 37
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 38
    .line 39
    .line 40
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBackground(Landroid/graphics/Canvas;IIF)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBorder(Landroid/graphics/Canvas;IIF)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawPageSeparated(Landroid/graphics/Canvas;IIF)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 50
    .line 51
    if-eqz v1, :cond_0

    .line 52
    .line 53
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 54
    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 57
    .line 58
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 59
    .line 60
    .line 61
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 62
    .line 63
    if-eqz v1, :cond_1

    .line 64
    .line 65
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 66
    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 69
    .line 70
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 71
    .line 72
    .line 73
    :cond_1
    const/4 v6, 0x1

    .line 74
    move-object v1, p0

    .line 75
    move-object v2, p1

    .line 76
    move v3, v0

    .line 77
    move v4, v7

    .line 78
    move v5, p4

    .line 79
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 80
    .line 81
    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 83
    .line 84
    .line 85
    const/4 v6, 0x0

    .line 86
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public drawForPrintMode(Landroid/graphics/Canvas;IIF)V
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    mul-float v0, v0, p4

    .line 5
    .line 6
    float-to-int v0, v0

    .line 7
    add-int/2addr v0, p2

    .line 8
    iget v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 9
    .line 10
    int-to-float v1, v1

    .line 11
    mul-float v1, v1, p4

    .line 12
    .line 13
    float-to-int v1, v1

    .line 14
    add-int v7, v1, p3

    .line 15
    .line 16
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBackground(Landroid/graphics/Canvas;IIF)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBorder(Landroid/graphics/Canvas;IIF)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawPageSeparated(Landroid/graphics/Canvas;IIF)V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 26
    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 33
    .line 34
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 38
    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 45
    .line 46
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 47
    .line 48
    .line 49
    :cond_1
    const/4 v6, 0x1

    .line 50
    move-object v1, p0

    .line 51
    move-object v2, p1

    .line 52
    move v3, v0

    .line 53
    move v4, v7

    .line 54
    move v5, p4

    .line 55
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 56
    .line 57
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 59
    .line 60
    .line 61
    const/4 v6, 0x0

    .line 62
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public drawToImage(Landroid/graphics/Canvas;IIF)V
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    mul-float v0, v0, p4

    .line 5
    .line 6
    float-to-int v0, v0

    .line 7
    add-int/2addr v0, p2

    .line 8
    iget v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 9
    .line 10
    int-to-float v1, v1

    .line 11
    mul-float v1, v1, p4

    .line 12
    .line 13
    float-to-int v1, v1

    .line 14
    add-int v7, v1, p3

    .line 15
    .line 16
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBackground(Landroid/graphics/Canvas;IIF)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, p1, v0, v7, p4}, Lcom/intsig/office/wp/view/PageView;->drawBorder(Landroid/graphics/Canvas;IIF)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 23
    .line 24
    if-eqz v1, :cond_0

    .line 25
    .line 26
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 30
    .line 31
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 32
    .line 33
    .line 34
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 35
    .line 36
    if-eqz v1, :cond_1

    .line 37
    .line 38
    invoke-virtual {v1, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 42
    .line 43
    invoke-virtual {v1, p1, v0, v7, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 44
    .line 45
    .line 46
    :cond_1
    const/4 v6, 0x1

    .line 47
    move-object v1, p0

    .line 48
    move-object v2, p1

    .line 49
    move v3, v0

    .line 50
    move v4, v7

    .line 51
    move v5, p4

    .line 52
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 53
    .line 54
    .line 55
    invoke-super {p0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/AbstractView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 56
    .line 57
    .line 58
    const/4 v6, 0x0

    .line 59
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/wp/view/PageView;->drawShape(Landroid/graphics/Canvas;IIFZ)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getFooter()Lcom/intsig/office/wp/view/TitleView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeader()Lcom/intsig/office/wp/view/TitleView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPageNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/wp/view/PageView;->pageNumber:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getView(JIZ)Lcom/intsig/office/simpletext/view/IView;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->child:Lcom/intsig/office/simpletext/view/IView;

    .line 2
    .line 3
    :goto_0
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1, p2, p4}, Lcom/intsig/office/simpletext/view/IView;->contains(JZ)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eq v1, p3, :cond_1

    .line 23
    .line 24
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    const/16 v2, 0x9

    .line 29
    .line 30
    if-eq v1, v2, :cond_1

    .line 31
    .line 32
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/IView;->getView(JIZ)Lcom/intsig/office/simpletext/view/IView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    return-object p1

    .line 37
    :cond_1
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public isHasBreakTable()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/PageView;->hasBreakTable:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/intsig/office/wp/view/PageView;->getView(JIZ)Lcom/intsig/office/simpletext/view/IView;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/IView;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 9
    .line 10
    .line 11
    :cond_0
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    add-int/2addr p1, p2

    .line 18
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 19
    .line 20
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    add-int/2addr p1, p2

    .line 27
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 28
    .line 29
    return-object p3
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public setFooter(Lcom/intsig/office/wp/view/TitleView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/wp/view/PageView;->footer:Lcom/intsig/office/wp/view/TitleView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHasBreakTable(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/wp/view/PageView;->hasBreakTable:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHeader(Lcom/intsig/office/wp/view/TitleView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/wp/view/PageView;->header:Lcom/intsig/office/wp/view/TitleView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPageBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/wp/view/PageView;->pageBRColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPageBorder(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/wp/view/PageView;->pageBorder:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPageNumber(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/wp/view/PageView;->pageNumber:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public viewToModel(IIZ)J
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sub-int/2addr p1, v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    sub-int/2addr p2, v0

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-le p2, v1, :cond_1

    .line 22
    .line 23
    :goto_0
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-lt p2, v1, :cond_0

    .line 30
    .line 31
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getHeight()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    add-int/2addr v1, v2

    .line 40
    if-ge p2, v1, :cond_0

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_0
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    goto :goto_0

    .line 48
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    :cond_2
    if-eqz v0, :cond_3

    .line 55
    .line 56
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/office/simpletext/view/IView;->viewToModel(IIZ)J

    .line 57
    .line 58
    .line 59
    move-result-wide p1

    .line 60
    return-wide p1

    .line 61
    :cond_3
    const-wide/16 p1, -0x1

    .line 62
    .line 63
    return-wide p1
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

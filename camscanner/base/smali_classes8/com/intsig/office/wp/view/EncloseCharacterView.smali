.class public Lcom/intsig/office/wp/view/EncloseCharacterView;
.super Lcom/intsig/office/wp/view/LeafView;
.source "EncloseCharacterView.java"


# instance fields
.field protected enclosePaint:Landroid/graphics/Paint;

.field protected path:Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/wp/view/LeafView;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/view/LeafView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    return-void
.end method

.method private drawEnclose(Landroid/graphics/Canvas;IIF)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    mul-float v0, v0, p4

    .line 5
    .line 6
    float-to-int v0, v0

    .line 7
    add-int/2addr v0, p2

    .line 8
    iget p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 9
    .line 10
    int-to-float p2, p2

    .line 11
    mul-float p2, p2, p4

    .line 12
    .line 13
    float-to-int p2, p2

    .line 14
    add-int/2addr p2, p3

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    int-to-float p3, p3

    .line 20
    mul-float p3, p3, p4

    .line 21
    .line 22
    float-to-int p3, p3

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    int-to-float v1, v1

    .line 28
    mul-float v1, v1, p4

    .line 29
    .line 30
    float-to-int p4, v1

    .line 31
    iget-object v1, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 32
    .line 33
    iget-byte v1, v1, Lcom/intsig/office/simpletext/view/CharAttr;->encloseType:B

    .line 34
    .line 35
    if-nez v1, :cond_0

    .line 36
    .line 37
    new-instance v3, Landroid/graphics/RectF;

    .line 38
    .line 39
    int-to-float v1, v0

    .line 40
    int-to-float v2, p2

    .line 41
    add-int/2addr v0, p3

    .line 42
    int-to-float p3, v0

    .line 43
    add-int/2addr p2, p4

    .line 44
    int-to-float p2, p2

    .line 45
    invoke-direct {v3, v1, v2, p3, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 46
    .line 47
    .line 48
    const/4 v4, 0x0

    .line 49
    const/high16 v5, 0x43b40000    # 360.0f

    .line 50
    .line 51
    const/4 v6, 0x0

    .line 52
    iget-object v7, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 53
    .line 54
    move-object v2, p1

    .line 55
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_0
    const/4 v2, 0x1

    .line 61
    if-ne v1, v2, :cond_1

    .line 62
    .line 63
    int-to-float v4, v0

    .line 64
    int-to-float v5, p2

    .line 65
    add-int/2addr v0, p3

    .line 66
    int-to-float v6, v0

    .line 67
    add-int/2addr p2, p4

    .line 68
    int-to-float v7, p2

    .line 69
    iget-object v8, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 70
    .line 71
    move-object v3, p1

    .line 72
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_1
    const/4 v2, 0x2

    .line 77
    if-ne v1, v2, :cond_2

    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 80
    .line 81
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 82
    .line 83
    .line 84
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 85
    .line 86
    div-int/lit8 v2, p3, 0x2

    .line 87
    .line 88
    add-int/2addr v2, v0

    .line 89
    int-to-float v2, v2

    .line 90
    int-to-float v3, p2

    .line 91
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 92
    .line 93
    .line 94
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 95
    .line 96
    int-to-float v2, v0

    .line 97
    add-int/2addr p2, p4

    .line 98
    int-to-float p2, p2

    .line 99
    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 100
    .line 101
    .line 102
    iget-object p4, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 103
    .line 104
    add-int/2addr v0, p3

    .line 105
    int-to-float p3, v0

    .line 106
    invoke-virtual {p4, p3, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 107
    .line 108
    .line 109
    iget-object p2, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 110
    .line 111
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 112
    .line 113
    .line 114
    iget-object p2, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 115
    .line 116
    iget-object p3, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 117
    .line 118
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 119
    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_2
    const/4 v2, 0x3

    .line 123
    if-ne v1, v2, :cond_3

    .line 124
    .line 125
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 126
    .line 127
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 128
    .line 129
    .line 130
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 131
    .line 132
    div-int/lit8 v2, p3, 0x2

    .line 133
    .line 134
    add-int/2addr v2, v0

    .line 135
    int-to-float v2, v2

    .line 136
    int-to-float v3, p2

    .line 137
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 138
    .line 139
    .line 140
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 141
    .line 142
    int-to-float v3, v0

    .line 143
    div-int/lit8 v4, p4, 0x2

    .line 144
    .line 145
    add-int/2addr v4, p2

    .line 146
    int-to-float v4, v4

    .line 147
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 148
    .line 149
    .line 150
    iget-object v1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 151
    .line 152
    add-int/2addr p2, p4

    .line 153
    int-to-float p2, p2

    .line 154
    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    .line 156
    .line 157
    iget-object p2, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 158
    .line 159
    add-int/2addr v0, p3

    .line 160
    int-to-float p3, v0

    .line 161
    invoke-virtual {p2, p3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 162
    .line 163
    .line 164
    iget-object p2, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 165
    .line 166
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 167
    .line 168
    .line 169
    iget-object p2, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 170
    .line 171
    iget-object p3, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 172
    .line 173
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 174
    .line 175
    .line 176
    :cond_3
    :goto_0
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/wp/view/LeafView;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public draw(Landroid/graphics/Canvas;IIF)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/LeafView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/EncloseCharacterView;->drawEnclose(Landroid/graphics/Canvas;IIF)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public free()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public initProperty(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/office/wp/view/LeafView;->initProperty(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    .line 2
    .line 3
    .line 4
    new-instance p1, Landroid/graphics/Paint;

    .line 5
    .line 6
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 12
    .line 13
    iget p2, p2, Lcom/intsig/office/simpletext/view/CharAttr;->fontColor:I

    .line 14
    .line 15
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 19
    .line 20
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 21
    .line 22
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->enclosePaint:Landroid/graphics/Paint;

    .line 26
    .line 27
    const/4 p2, 0x1

    .line 28
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 29
    .line 30
    .line 31
    new-instance p1, Landroid/graphics/Path;

    .line 32
    .line 33
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object p1, p0, Lcom/intsig/office/wp/view/EncloseCharacterView;->path:Landroid/graphics/Path;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

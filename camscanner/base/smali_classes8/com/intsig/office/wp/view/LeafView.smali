.class public Lcom/intsig/office/wp/view/LeafView;
.super Lcom/intsig/office/simpletext/view/AbstractView;
.source "LeafView.java"


# static fields
.field private static title:Ljava/lang/StringBuffer;


# instance fields
.field protected charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

.field private charList:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/wp/model/CharText;",
            ">;>;"
        }
    .end annotation
.end field

.field protected numPages:I

.field protected paint:Landroid/graphics/Paint;

.field random:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/wp/view/LeafView;->title:Ljava/lang/StringBuffer;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/view/AbstractView;-><init>()V

    .line 2
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->random:Ljava/util/Random;

    .line 3
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charList:Ljava/lang/ref/SoftReference;

    const/4 v0, -0x1

    .line 4
    iput v0, p0, Lcom/intsig/office/wp/view/LeafView;->numPages:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 2

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/simpletext/view/AbstractView;-><init>()V

    .line 6
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->random:Ljava/util/Random;

    .line 7
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charList:Ljava/lang/ref/SoftReference;

    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/office/wp/view/LeafView;->numPages:I

    .line 9
    iput-object p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 10
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/wp/view/LeafView;->initProperty(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    return-void
.end method

.method private getFieldTextReplacedByPage(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/office/wp/view/LeafView;->title:Ljava/lang/StringBuffer;

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 15
    .line 16
    .line 17
    :goto_0
    array-length v1, v0

    .line 18
    if-ge v3, v1, :cond_1

    .line 19
    .line 20
    aget-char v1, v0, v3

    .line 21
    .line 22
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    sget-object v1, Lcom/intsig/office/wp/view/LeafView;->title:Ljava/lang/StringBuffer;

    .line 29
    .line 30
    aget-char v2, v0, v3

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 33
    .line 34
    .line 35
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    sget-object v0, Lcom/intsig/office/wp/view/LeafView;->title:Ljava/lang/StringBuffer;

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-lez v0, :cond_2

    .line 45
    .line 46
    sget-object v0, Lcom/intsig/office/wp/view/LeafView;->title:Ljava/lang/StringBuffer;

    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    :cond_2
    return-object p1
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public doLayout(Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;IIIIJI)I
    .locals 4

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getStartOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    .line 3
    .line 4
    .line 5
    move-result-wide p2

    .line 6
    iget-object p4, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 7
    .line 8
    invoke-interface {p4}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 9
    .line 10
    .line 11
    move-result-wide p4

    .line 12
    iget-object p7, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 13
    .line 14
    invoke-interface {p7, p1}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    cmp-long p7, p2, p4

    .line 19
    .line 20
    if-lez p7, :cond_0

    .line 21
    .line 22
    sub-long p7, p2, p4

    .line 23
    .line 24
    long-to-int p8, p7

    .line 25
    iget-object p7, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 26
    .line 27
    invoke-interface {p7}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    sub-long/2addr v0, p4

    .line 32
    long-to-int p4, v0

    .line 33
    invoke-virtual {p1, p8, p4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 38
    .line 39
    .line 40
    move-result p4

    .line 41
    new-array p4, p4, [F

    .line 42
    .line 43
    iget-object p5, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 44
    .line 45
    invoke-virtual {p5, p1, p4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 46
    .line 47
    .line 48
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    .line 49
    .line 50
    .line 51
    move-result-object p5

    .line 52
    const/4 p7, 0x2

    .line 53
    invoke-virtual {p5, p10, p7}, Lcom/intsig/office/simpletext/view/ViewKit;->getBitValue(II)Z

    .line 54
    .line 55
    .line 56
    move-result p5

    .line 57
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    .line 58
    .line 59
    .line 60
    move-result-object p8

    .line 61
    const/4 p9, 0x0

    .line 62
    invoke-virtual {p8, p10, p9}, Lcom/intsig/office/simpletext/view/ViewKit;->getBitValue(II)Z

    .line 63
    .line 64
    .line 65
    move-result p8

    .line 66
    const/4 p10, 0x0

    .line 67
    const/4 v0, 0x0

    .line 68
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-ge v0, v1, :cond_7

    .line 73
    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    aget v2, p4, v0

    .line 79
    .line 80
    add-float/2addr p10, v2

    .line 81
    const/4 v3, 0x7

    .line 82
    if-eq v1, v3, :cond_6

    .line 83
    .line 84
    const/16 v3, 0xa

    .line 85
    .line 86
    if-eq v1, v3, :cond_6

    .line 87
    .line 88
    const/16 v3, 0xd

    .line 89
    .line 90
    if-ne v1, v3, :cond_1

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_1
    if-nez p5, :cond_2

    .line 94
    .line 95
    const/16 v3, 0xc

    .line 96
    .line 97
    if-ne v1, v3, :cond_2

    .line 98
    .line 99
    add-int/lit8 v0, v0, 0x1

    .line 100
    .line 101
    const/4 p7, 0x3

    .line 102
    goto :goto_4

    .line 103
    :cond_2
    const/16 v3, 0xb

    .line 104
    .line 105
    if-ne v1, v3, :cond_3

    .line 106
    .line 107
    goto :goto_2

    .line 108
    :cond_3
    int-to-float v1, p6

    .line 109
    cmpl-float v1, p10, v1

    .line 110
    .line 111
    if-lez v1, :cond_5

    .line 112
    .line 113
    sub-float/2addr p10, v2

    .line 114
    if-eqz p8, :cond_4

    .line 115
    .line 116
    if-nez v0, :cond_4

    .line 117
    .line 118
    add-float/2addr p10, v2

    .line 119
    add-int/lit8 v0, v0, 0x1

    .line 120
    .line 121
    goto :goto_3

    .line 122
    :cond_4
    const/4 p7, 0x1

    .line 123
    goto :goto_4

    .line 124
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_6
    :goto_1
    sub-float/2addr p10, v2

    .line 128
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 129
    .line 130
    goto :goto_4

    .line 131
    :cond_7
    :goto_3
    const/4 p7, 0x0

    .line 132
    :goto_4
    int-to-long p4, v0

    .line 133
    add-long/2addr p4, p2

    .line 134
    invoke-virtual {p0, p4, p5}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 135
    .line 136
    .line 137
    float-to-int p1, p10

    .line 138
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 139
    .line 140
    invoke-virtual {p2}, Landroid/graphics/Paint;->descent()F

    .line 141
    .line 142
    .line 143
    move-result p2

    .line 144
    iget-object p3, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 145
    .line 146
    invoke-virtual {p3}, Landroid/graphics/Paint;->ascent()F

    .line 147
    .line 148
    .line 149
    move-result p3

    .line 150
    sub-float/2addr p2, p3

    .line 151
    float-to-double p2, p2

    .line 152
    invoke-static {p2, p3}, Ljava/lang/Math;->ceil(D)D

    .line 153
    .line 154
    .line 155
    move-result-wide p2

    .line 156
    double-to-int p2, p2

    .line 157
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    .line 158
    .line 159
    .line 160
    return p7
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public draw(Landroid/graphics/Canvas;IIF)V
    .locals 25

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p4

    .line 4
    .line 5
    iget v2, v0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 6
    .line 7
    int-to-float v2, v2

    .line 8
    mul-float v2, v2, v1

    .line 9
    .line 10
    move/from16 v3, p2

    .line 11
    .line 12
    int-to-float v3, v3

    .line 13
    add-float/2addr v2, v3

    .line 14
    iget v4, v0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 15
    .line 16
    int-to-float v4, v4

    .line 17
    mul-float v4, v4, v1

    .line 18
    .line 19
    move/from16 v5, p3

    .line 20
    .line 21
    int-to-float v6, v5

    .line 22
    add-float v10, v4, v6

    .line 23
    .line 24
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    .line 27
    .line 28
    .line 29
    move-result v11

    .line 30
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 31
    .line 32
    iget v4, v4, Lcom/intsig/office/simpletext/view/CharAttr;->highlightedColor:I

    .line 33
    .line 34
    const/4 v5, -0x1

    .line 35
    if-eq v4, v5, :cond_0

    .line 36
    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    if-eqz v4, :cond_0

    .line 42
    .line 43
    iget-object v5, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 44
    .line 45
    iget-object v7, v0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 46
    .line 47
    iget v7, v7, Lcom/intsig/office/simpletext/view/CharAttr;->highlightedColor:I

    .line 48
    .line 49
    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    int-to-float v5, v5

    .line 57
    mul-float v5, v5, v1

    .line 58
    .line 59
    add-float v7, v2, v5

    .line 60
    .line 61
    invoke-interface {v4}, Lcom/intsig/office/simpletext/view/IView;->getHeight()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    int-to-float v4, v4

    .line 66
    mul-float v4, v4, v1

    .line 67
    .line 68
    add-float v8, v6, v4

    .line 69
    .line 70
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 71
    .line 72
    move-object/from16 v4, p1

    .line 73
    .line 74
    move v5, v2

    .line 75
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 76
    .line 77
    .line 78
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 79
    .line 80
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    .line 82
    .line 83
    :cond_0
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 84
    .line 85
    invoke-virtual {v4}, Landroid/graphics/Paint;->getTextSize()F

    .line 86
    .line 87
    .line 88
    move-result v11

    .line 89
    mul-float v4, v11, v1

    .line 90
    .line 91
    iget-object v5, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 92
    .line 93
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 94
    .line 95
    .line 96
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 97
    .line 98
    iget-short v4, v4, Lcom/intsig/office/simpletext/view/CharAttr;->subSuperScriptType:S

    .line 99
    .line 100
    const/4 v5, 0x1

    .line 101
    if-ne v4, v5, :cond_1

    .line 102
    .line 103
    float-to-double v6, v10

    .line 104
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 105
    .line 106
    invoke-virtual {v4}, Landroid/graphics/Paint;->descent()F

    .line 107
    .line 108
    .line 109
    move-result v4

    .line 110
    iget-object v8, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 111
    .line 112
    invoke-virtual {v8}, Landroid/graphics/Paint;->ascent()F

    .line 113
    .line 114
    .line 115
    move-result v8

    .line 116
    sub-float/2addr v4, v8

    .line 117
    float-to-double v8, v4

    .line 118
    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    .line 119
    .line 120
    .line 121
    move-result-wide v8

    .line 122
    sub-double/2addr v6, v8

    .line 123
    double-to-float v10, v6

    .line 124
    :cond_1
    move v12, v10

    .line 125
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->charList:Ljava/lang/ref/SoftReference;

    .line 126
    .line 127
    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    .line 128
    .line 129
    .line 130
    move-result-object v4

    .line 131
    check-cast v4, Ljava/util/ArrayList;

    .line 132
    .line 133
    const/4 v6, 0x0

    .line 134
    if-eqz v4, :cond_3

    .line 135
    .line 136
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    if-lez v3, :cond_2

    .line 141
    .line 142
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    check-cast v3, Lcom/intsig/office/wp/model/CharText;

    .line 147
    .line 148
    iget v3, v3, Lcom/intsig/office/wp/model/CharText;->zoom:F

    .line 149
    .line 150
    div-float v3, v1, v3

    .line 151
    .line 152
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 153
    .line 154
    .line 155
    move-result-object v4

    .line 156
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 157
    .line 158
    .line 159
    move-result v5

    .line 160
    if-eqz v5, :cond_2

    .line 161
    .line 162
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 163
    .line 164
    .line 165
    move-result-object v5

    .line 166
    check-cast v5, Lcom/intsig/office/wp/model/CharText;

    .line 167
    .line 168
    iget-object v6, v5, Lcom/intsig/office/wp/model/CharText;->text:Ljava/lang/String;

    .line 169
    .line 170
    iget v7, v5, Lcom/intsig/office/wp/model/CharText;->x:F

    .line 171
    .line 172
    mul-float v7, v7, v3

    .line 173
    .line 174
    add-float/2addr v7, v2

    .line 175
    iget v5, v5, Lcom/intsig/office/wp/model/CharText;->y:F

    .line 176
    .line 177
    mul-float v5, v5, v3

    .line 178
    .line 179
    add-float/2addr v5, v12

    .line 180
    iget-object v8, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 181
    .line 182
    move-object/from16 v15, p1

    .line 183
    .line 184
    invoke-virtual {v15, v6, v7, v5, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 185
    .line 186
    .line 187
    goto :goto_0

    .line 188
    :cond_2
    move-object/from16 v15, p1

    .line 189
    .line 190
    move/from16 v22, v11

    .line 191
    .line 192
    goto/16 :goto_11

    .line 193
    .line 194
    :cond_3
    move-object/from16 v15, p1

    .line 195
    .line 196
    new-instance v14, Ljava/util/ArrayList;

    .line 197
    .line 198
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .line 200
    .line 201
    iget-object v4, v0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 202
    .line 203
    const/4 v7, 0x0

    .line 204
    invoke-interface {v4, v7}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object v4

    .line 208
    iget-wide v8, v0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 209
    .line 210
    iget-object v10, v0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 211
    .line 212
    invoke-interface {v10}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 213
    .line 214
    .line 215
    move-result-wide v16

    .line 216
    sub-long v8, v8, v16

    .line 217
    .line 218
    long-to-int v9, v8

    .line 219
    iget-wide v7, v0, Lcom/intsig/office/simpletext/view/AbstractView;->end:J

    .line 220
    .line 221
    iget-object v10, v0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 222
    .line 223
    invoke-interface {v10}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 224
    .line 225
    .line 226
    move-result-wide v16

    .line 227
    sub-long v7, v7, v16

    .line 228
    .line 229
    long-to-int v8, v7

    .line 230
    iget-object v7, v0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 231
    .line 232
    iget-byte v7, v7, Lcom/intsig/office/simpletext/view/CharAttr;->pageNumberType:B

    .line 233
    .line 234
    const/4 v10, 0x2

    .line 235
    if-ne v7, v5, :cond_9

    .line 236
    .line 237
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 238
    .line 239
    .line 240
    move-result-object v7

    .line 241
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 242
    .line 243
    .line 244
    move-result-object v7

    .line 245
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 246
    .line 247
    .line 248
    move-result-object v7

    .line 249
    if-eqz v7, :cond_8

    .line 250
    .line 251
    :goto_1
    if-eqz v7, :cond_7

    .line 252
    .line 253
    instance-of v13, v7, Lcom/intsig/office/wp/view/TitleView;

    .line 254
    .line 255
    if-eqz v13, :cond_4

    .line 256
    .line 257
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 258
    .line 259
    .line 260
    move-result-object v13

    .line 261
    if-eqz v13, :cond_4

    .line 262
    .line 263
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 264
    .line 265
    .line 266
    move-result-object v7

    .line 267
    check-cast v7, Lcom/intsig/office/wp/view/PageView;

    .line 268
    .line 269
    goto :goto_2

    .line 270
    :cond_4
    instance-of v13, v7, Lcom/intsig/office/wp/view/PageView;

    .line 271
    .line 272
    if-eqz v13, :cond_5

    .line 273
    .line 274
    check-cast v7, Lcom/intsig/office/wp/view/PageView;

    .line 275
    .line 276
    goto :goto_2

    .line 277
    :cond_5
    instance-of v13, v7, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 278
    .line 279
    if-eqz v13, :cond_6

    .line 280
    .line 281
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 282
    .line 283
    .line 284
    move-result-object v7

    .line 285
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 286
    .line 287
    .line 288
    move-result-object v7

    .line 289
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 290
    .line 291
    .line 292
    move-result-object v7

    .line 293
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 294
    .line 295
    .line 296
    move-result-object v7

    .line 297
    goto :goto_1

    .line 298
    :cond_6
    invoke-interface {v7}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 299
    .line 300
    .line 301
    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 302
    goto :goto_1

    .line 303
    :cond_7
    const/4 v7, 0x0

    .line 304
    :goto_2
    if-eqz v7, :cond_8

    .line 305
    .line 306
    :try_start_1
    invoke-virtual {v7}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 307
    .line 308
    .line 309
    move-result v7

    .line 310
    invoke-direct {v0, v4, v7}, Lcom/intsig/office/wp/view/LeafView;->getFieldTextReplacedByPage(Ljava/lang/String;I)Ljava/lang/String;

    .line 311
    .line 312
    .line 313
    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 314
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 315
    .line 316
    .line 317
    move-result v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 318
    move v8, v7

    .line 319
    goto :goto_3

    .line 320
    :catch_0
    nop

    .line 321
    :goto_3
    const/4 v7, 0x1

    .line 322
    const/4 v9, 0x0

    .line 323
    goto :goto_4

    .line 324
    :catch_1
    nop

    .line 325
    const/4 v7, 0x1

    .line 326
    goto :goto_4

    .line 327
    :catch_2
    nop

    .line 328
    :cond_8
    const/4 v7, 0x0

    .line 329
    :goto_4
    move-object v13, v4

    .line 330
    goto :goto_5

    .line 331
    :cond_9
    if-ne v7, v10, :cond_a

    .line 332
    .line 333
    iget v7, v0, Lcom/intsig/office/wp/view/LeafView;->numPages:I

    .line 334
    .line 335
    if-lez v7, :cond_a

    .line 336
    .line 337
    invoke-direct {v0, v4, v7}, Lcom/intsig/office/wp/view/LeafView;->getFieldTextReplacedByPage(Ljava/lang/String;I)Ljava/lang/String;

    .line 338
    .line 339
    .line 340
    move-result-object v4

    .line 341
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 342
    .line 343
    .line 344
    move-result v8

    .line 345
    move-object v13, v4

    .line 346
    move v9, v8

    .line 347
    const/4 v7, 0x1

    .line 348
    const/4 v8, 0x0

    .line 349
    goto :goto_6

    .line 350
    :cond_a
    move-object v13, v4

    .line 351
    const/4 v7, 0x0

    .line 352
    :goto_5
    move/from16 v24, v9

    .line 353
    .line 354
    move v9, v8

    .line 355
    move/from16 v8, v24

    .line 356
    .line 357
    :goto_6
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    .line 358
    .line 359
    .line 360
    move-result v4

    .line 361
    new-array v4, v4, [F

    .line 362
    .line 363
    iget-object v5, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 364
    .line 365
    invoke-virtual {v5, v13, v4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 366
    .line 367
    .line 368
    const/16 v5, 0xa

    .line 369
    .line 370
    const/4 v6, 0x7

    .line 371
    const/16 v21, 0x0

    .line 372
    .line 373
    move/from16 v22, v11

    .line 374
    .line 375
    if-nez v7, :cond_10

    .line 376
    .line 377
    float-to-double v10, v1

    .line 378
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    .line 379
    .line 380
    cmpl-double v18, v10, v16

    .line 381
    .line 382
    if-eqz v18, :cond_10

    .line 383
    .line 384
    move v10, v8

    .line 385
    const/4 v11, 0x0

    .line 386
    :goto_7
    if-ge v10, v9, :cond_b

    .line 387
    .line 388
    aget v16, v4, v10

    .line 389
    .line 390
    add-float v11, v11, v16

    .line 391
    .line 392
    add-int/lit8 v10, v10, 0x1

    .line 393
    .line 394
    goto :goto_7

    .line 395
    :cond_b
    add-int/lit8 v10, v9, -0x1

    .line 396
    .line 397
    invoke-virtual {v13, v10}, Ljava/lang/String;->charAt(I)C

    .line 398
    .line 399
    .line 400
    move-result v7

    .line 401
    if-eq v7, v6, :cond_c

    .line 402
    .line 403
    if-eq v7, v5, :cond_c

    .line 404
    .line 405
    const/16 v5, 0xd

    .line 406
    .line 407
    if-ne v7, v5, :cond_d

    .line 408
    .line 409
    :cond_c
    aget v5, v4, v10

    .line 410
    .line 411
    sub-float/2addr v11, v5

    .line 412
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 413
    .line 414
    .line 415
    move-result-object v5

    .line 416
    if-eqz v5, :cond_f

    .line 417
    .line 418
    invoke-interface {v5}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 419
    .line 420
    .line 421
    move-result v10

    .line 422
    if-eq v10, v6, :cond_e

    .line 423
    .line 424
    invoke-interface {v5}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 425
    .line 426
    .line 427
    move-result v10

    .line 428
    const/16 v7, 0xd

    .line 429
    .line 430
    if-ne v10, v7, :cond_f

    .line 431
    .line 432
    check-cast v5, Lcom/intsig/office/wp/view/ShapeView;

    .line 433
    .line 434
    invoke-virtual {v5}, Lcom/intsig/office/wp/view/ShapeView;->isInline()Z

    .line 435
    .line 436
    .line 437
    move-result v5

    .line 438
    if-eqz v5, :cond_f

    .line 439
    .line 440
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 441
    .line 442
    .line 443
    move-result-object v5

    .line 444
    invoke-interface {v5}, Lcom/intsig/office/simpletext/view/IView;->getX()I

    .line 445
    .line 446
    .line 447
    move-result v5

    .line 448
    int-to-float v5, v5

    .line 449
    mul-float v5, v5, v1

    .line 450
    .line 451
    add-float/2addr v5, v3

    .line 452
    sub-float/2addr v5, v2

    .line 453
    sub-float/2addr v11, v5

    .line 454
    const/4 v3, 0x0

    .line 455
    goto :goto_8

    .line 456
    :cond_f
    const/4 v3, 0x0

    .line 457
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 458
    .line 459
    .line 460
    move-result v5

    .line 461
    int-to-float v5, v5

    .line 462
    mul-float v5, v5, v1

    .line 463
    .line 464
    sub-float/2addr v11, v5

    .line 465
    :goto_8
    cmpl-float v5, v11, v21

    .line 466
    .line 467
    if-eqz v5, :cond_11

    .line 468
    .line 469
    sub-int v5, v9, v8

    .line 470
    .line 471
    int-to-float v5, v5

    .line 472
    div-float/2addr v11, v5

    .line 473
    goto :goto_9

    .line 474
    :cond_10
    const/4 v3, 0x0

    .line 475
    :cond_11
    const/4 v11, 0x0

    .line 476
    :goto_9
    iget-object v5, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 477
    .line 478
    invoke-virtual {v5}, Landroid/graphics/Paint;->ascent()F

    .line 479
    .line 480
    .line 481
    move-result v5

    .line 482
    sub-float v20, v12, v5

    .line 483
    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 485
    .line 486
    .line 487
    move-result-object v5

    .line 488
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 489
    .line 490
    .line 491
    move-result-object v5

    .line 492
    instance-of v5, v5, Lcom/intsig/office/wp/control/Word;

    .line 493
    .line 494
    if-eqz v5, :cond_13

    .line 495
    .line 496
    iget-object v10, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 497
    .line 498
    move-object/from16 v4, p1

    .line 499
    .line 500
    move-object v5, v13

    .line 501
    move v6, v8

    .line 502
    move v7, v9

    .line 503
    move v3, v8

    .line 504
    move v8, v2

    .line 505
    move v11, v9

    .line 506
    move/from16 v9, v20

    .line 507
    .line 508
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 509
    .line 510
    .line 511
    new-instance v4, Lcom/intsig/office/wp/model/CharText;

    .line 512
    .line 513
    invoke-virtual {v13, v3, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 514
    .line 515
    .line 516
    move-result-object v3

    .line 517
    sub-float v5, v2, v2

    .line 518
    .line 519
    sub-float v6, v20, v12

    .line 520
    .line 521
    invoke-direct {v4, v3, v5, v6, v1}, Lcom/intsig/office/wp/model/CharText;-><init>(Ljava/lang/String;FFF)V

    .line 522
    .line 523
    .line 524
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    .line 526
    .line 527
    :cond_12
    move-object v6, v14

    .line 528
    goto/16 :goto_10

    .line 529
    .line 530
    :cond_13
    move/from16 v24, v9

    .line 531
    .line 532
    move v9, v8

    .line 533
    move/from16 v8, v24

    .line 534
    .line 535
    move v5, v2

    .line 536
    :goto_a
    if-ge v9, v8, :cond_12

    .line 537
    .line 538
    invoke-virtual {v13, v9}, Ljava/lang/String;->charAt(I)C

    .line 539
    .line 540
    .line 541
    move-result v10

    .line 542
    const/16 v3, 0xa

    .line 543
    .line 544
    if-eq v10, v3, :cond_18

    .line 545
    .line 546
    const/16 v7, 0xd

    .line 547
    .line 548
    if-eq v10, v7, :cond_17

    .line 549
    .line 550
    if-eq v10, v6, :cond_17

    .line 551
    .line 552
    const/16 v3, 0xb

    .line 553
    .line 554
    if-eq v10, v3, :cond_17

    .line 555
    .line 556
    const/16 v3, 0xc

    .line 557
    .line 558
    if-eq v10, v3, :cond_17

    .line 559
    .line 560
    const/16 v6, 0x9

    .line 561
    .line 562
    if-eq v10, v6, :cond_17

    .line 563
    .line 564
    const/16 v6, 0x20

    .line 565
    .line 566
    if-eq v10, v6, :cond_17

    .line 567
    .line 568
    const/4 v6, 0x2

    .line 569
    if-eq v10, v6, :cond_17

    .line 570
    .line 571
    if-ne v10, v3, :cond_14

    .line 572
    .line 573
    goto :goto_d

    .line 574
    :cond_14
    add-int/lit8 v3, v9, 0x1

    .line 575
    .line 576
    move v10, v3

    .line 577
    const/16 v23, 0x0

    .line 578
    .line 579
    :goto_b
    if-ge v10, v8, :cond_16

    .line 580
    .line 581
    aget v16, v4, v10

    .line 582
    .line 583
    cmpl-float v16, v16, v21

    .line 584
    .line 585
    if-eqz v16, :cond_15

    .line 586
    .line 587
    goto :goto_c

    .line 588
    :cond_15
    add-int/lit8 v23, v23, 0x1

    .line 589
    .line 590
    add-int/lit8 v10, v10, 0x1

    .line 591
    .line 592
    goto :goto_b

    .line 593
    :cond_16
    :goto_c
    add-int v3, v3, v23

    .line 594
    .line 595
    iget-object v10, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 596
    .line 597
    move-object/from16 p3, v13

    .line 598
    .line 599
    move-object/from16 v13, p1

    .line 600
    .line 601
    move-object v6, v14

    .line 602
    move-object/from16 v14, p3

    .line 603
    .line 604
    move v15, v9

    .line 605
    move/from16 v16, v3

    .line 606
    .line 607
    move/from16 v17, v5

    .line 608
    .line 609
    move/from16 v18, v20

    .line 610
    .line 611
    move-object/from16 v19, v10

    .line 612
    .line 613
    invoke-virtual/range {v13 .. v19}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 614
    .line 615
    .line 616
    new-instance v10, Lcom/intsig/office/wp/model/CharText;

    .line 617
    .line 618
    move-object/from16 v13, p3

    .line 619
    .line 620
    invoke-virtual {v13, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 621
    .line 622
    .line 623
    move-result-object v3

    .line 624
    sub-float v14, v5, v2

    .line 625
    .line 626
    sub-float v15, v20, v12

    .line 627
    .line 628
    invoke-direct {v10, v3, v14, v15, v1}, Lcom/intsig/office/wp/model/CharText;-><init>(Ljava/lang/String;FFF)V

    .line 629
    .line 630
    .line 631
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    .line 633
    .line 634
    aget v3, v4, v9

    .line 635
    .line 636
    sub-float/2addr v3, v11

    .line 637
    add-float/2addr v5, v3

    .line 638
    add-int v9, v9, v23

    .line 639
    .line 640
    goto :goto_f

    .line 641
    :cond_17
    :goto_d
    move-object v6, v14

    .line 642
    goto :goto_e

    .line 643
    :cond_18
    move-object v6, v14

    .line 644
    const/16 v7, 0xd

    .line 645
    .line 646
    :goto_e
    aget v3, v4, v9

    .line 647
    .line 648
    sub-float/2addr v3, v11

    .line 649
    add-float/2addr v5, v3

    .line 650
    :goto_f
    const/4 v3, 0x1

    .line 651
    add-int/2addr v9, v3

    .line 652
    move-object/from16 v15, p1

    .line 653
    .line 654
    move-object v14, v6

    .line 655
    const/4 v3, 0x0

    .line 656
    const/4 v6, 0x7

    .line 657
    goto :goto_a

    .line 658
    :goto_10
    new-instance v3, Ljava/lang/ref/SoftReference;

    .line 659
    .line 660
    invoke-direct {v3, v6}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 661
    .line 662
    .line 663
    iput-object v3, v0, Lcom/intsig/office/wp/view/LeafView;->charList:Ljava/lang/ref/SoftReference;

    .line 664
    .line 665
    :goto_11
    float-to-double v3, v12

    .line 666
    iget-object v5, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 667
    .line 668
    invoke-virtual {v5}, Landroid/graphics/Paint;->descent()F

    .line 669
    .line 670
    .line 671
    move-result v5

    .line 672
    iget-object v6, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 673
    .line 674
    invoke-virtual {v6}, Landroid/graphics/Paint;->ascent()F

    .line 675
    .line 676
    .line 677
    move-result v6

    .line 678
    sub-float/2addr v5, v6

    .line 679
    float-to-double v5, v5

    .line 680
    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    .line 681
    .line 682
    .line 683
    move-result-wide v5

    .line 684
    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    .line 685
    .line 686
    div-double/2addr v5, v7

    .line 687
    add-double/2addr v3, v5

    .line 688
    double-to-float v3, v3

    .line 689
    iget-object v4, v0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 690
    .line 691
    iget-boolean v5, v4, Lcom/intsig/office/simpletext/view/CharAttr;->isStrikeThrough:Z

    .line 692
    .line 693
    const/high16 v6, 0x3f800000    # 1.0f

    .line 694
    .line 695
    if-eqz v5, :cond_19

    .line 696
    .line 697
    sub-float v7, v3, v6

    .line 698
    .line 699
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 700
    .line 701
    .line 702
    move-result v4

    .line 703
    int-to-float v4, v4

    .line 704
    mul-float v4, v4, v1

    .line 705
    .line 706
    add-float v1, v2, v4

    .line 707
    .line 708
    add-float v8, v3, v6

    .line 709
    .line 710
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 711
    .line 712
    move-object/from16 v4, p1

    .line 713
    .line 714
    move v5, v2

    .line 715
    move v6, v7

    .line 716
    move v7, v1

    .line 717
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 718
    .line 719
    .line 720
    goto :goto_12

    .line 721
    :cond_19
    iget-boolean v4, v4, Lcom/intsig/office/simpletext/view/CharAttr;->isDoubleStrikeThrough:Z

    .line 722
    .line 723
    if-eqz v4, :cond_1a

    .line 724
    .line 725
    const/high16 v4, 0x40400000    # 3.0f

    .line 726
    .line 727
    sub-float v7, v3, v4

    .line 728
    .line 729
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 730
    .line 731
    .line 732
    move-result v4

    .line 733
    int-to-float v4, v4

    .line 734
    mul-float v4, v4, v1

    .line 735
    .line 736
    add-float v8, v2, v4

    .line 737
    .line 738
    sub-float v9, v3, v6

    .line 739
    .line 740
    iget-object v10, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 741
    .line 742
    move-object/from16 v4, p1

    .line 743
    .line 744
    move v5, v2

    .line 745
    move v6, v7

    .line 746
    move v7, v8

    .line 747
    move v8, v9

    .line 748
    move-object v9, v10

    .line 749
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 750
    .line 751
    .line 752
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 753
    .line 754
    .line 755
    move-result v4

    .line 756
    int-to-float v4, v4

    .line 757
    mul-float v4, v4, v1

    .line 758
    .line 759
    add-float v7, v2, v4

    .line 760
    .line 761
    const/high16 v1, 0x40000000    # 2.0f

    .line 762
    .line 763
    add-float v8, v3, v1

    .line 764
    .line 765
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 766
    .line 767
    move-object/from16 v4, p1

    .line 768
    .line 769
    move v6, v3

    .line 770
    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 771
    .line 772
    .line 773
    :cond_1a
    :goto_12
    iget-object v1, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 774
    .line 775
    move/from16 v2, v22

    .line 776
    .line 777
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 778
    .line 779
    .line 780
    return-void
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

.method public free()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBaseline()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-interface {v0, v1}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v1, "\n"

    .line 9
    .line 10
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    neg-float v0, v0

    .line 23
    float-to-int v0, v0

    .line 24
    return v0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCharAttr()Lcom/intsig/office/simpletext/view/CharAttr;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPageNumber()I
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    instance-of v1, v0, Lcom/intsig/office/wp/view/CellView;

    .line 14
    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    :cond_0
    instance-of v1, v0, Lcom/intsig/office/wp/view/PageView;

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    return v0

    .line 40
    :cond_1
    instance-of v0, v0, Lcom/intsig/office/wp/view/TitleView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    const/4 v0, -0x1

    .line 45
    return v0

    .line 46
    :catch_0
    :cond_2
    const/4 v0, 0x0

    .line 47
    return v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getTextWidth()F
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-interface {v0, v1}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-wide v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 11
    .line 12
    invoke-interface {v3}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 13
    .line 14
    .line 15
    move-result-wide v3

    .line 16
    sub-long/2addr v1, v3

    .line 17
    long-to-int v2, v1

    .line 18
    iget-wide v3, p0, Lcom/intsig/office/simpletext/view/AbstractView;->end:J

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 21
    .line 22
    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 23
    .line 24
    .line 25
    move-result-wide v5

    .line 26
    sub-long/2addr v3, v5

    .line 27
    long-to-int v1, v3

    .line 28
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    new-array v1, v1, [F

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 39
    .line 40
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 41
    .line 42
    .line 43
    const/4 v2, 0x0

    .line 44
    const/4 v3, 0x0

    .line 45
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    if-ge v3, v4, :cond_0

    .line 50
    .line 51
    aget v4, v1, v3

    .line 52
    .line 53
    add-float/2addr v2, v4

    .line 54
    add-int/lit8 v3, v3, 0x1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    return v2
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnderlinePosition()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    int-to-float v0, v0

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    int-to-float v1, v1

    .line 16
    iget-object v2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 17
    .line 18
    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    sub-float/2addr v1, v2

    .line 23
    sub-float/2addr v0, v1

    .line 24
    float-to-int v0, v0

    .line 25
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hasUpdatedFieldText()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-byte v0, v0, Lcom/intsig/office/simpletext/view/CharAttr;->pageNumberType:B

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    return v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public initProperty(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 16
    .line 17
    .line 18
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 25
    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/simpletext/view/CharAttr;

    .line 29
    .line 30
    invoke-direct {v0}, Lcom/intsig/office/simpletext/view/CharAttr;-><init>()V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 34
    .line 35
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iget-object v2, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 40
    .line 41
    invoke-interface {p2}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {v0, v2, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->fillCharAttr(Lcom/intsig/office/simpletext/view/CharAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 53
    .line 54
    iget-boolean p2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isBold:Z

    .line 55
    .line 56
    if-eqz p2, :cond_2

    .line 57
    .line 58
    iget-boolean v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isItalic:Z

    .line 59
    .line 60
    if-eqz v0, :cond_2

    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 63
    .line 64
    const p2, -0x41b33333    # -0.2f

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 71
    .line 72
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 73
    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_2
    if-eqz p2, :cond_3

    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 79
    .line 80
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_3
    iget-boolean p1, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isItalic:Z

    .line 85
    .line 86
    if-eqz p1, :cond_4

    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 89
    .line 90
    const/high16 p2, -0x41800000    # -0.25f

    .line 91
    .line 92
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 93
    .line 94
    .line 95
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 96
    .line 97
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->instance()Lcom/intsig/office/simpletext/font/FontTypefaceManage;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 102
    .line 103
    iget v0, v0, Lcom/intsig/office/simpletext/view/CharAttr;->fontIndex:I

    .line 104
    .line 105
    invoke-virtual {p2, v0}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->getFontTypeface(I)Landroid/graphics/Typeface;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 113
    .line 114
    iget-short p2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->subSuperScriptType:S

    .line 115
    .line 116
    const v0, 0x3faaaaab

    .line 117
    .line 118
    .line 119
    const/high16 v1, 0x42c80000    # 100.0f

    .line 120
    .line 121
    if-lez p2, :cond_5

    .line 122
    .line 123
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 124
    .line 125
    iget v2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontSize:I

    .line 126
    .line 127
    int-to-float v2, v2

    .line 128
    iget p1, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontScale:I

    .line 129
    .line 130
    int-to-float p1, p1

    .line 131
    div-float/2addr p1, v1

    .line 132
    mul-float v2, v2, p1

    .line 133
    .line 134
    mul-float v2, v2, v0

    .line 135
    .line 136
    const/high16 p1, 0x40000000    # 2.0f

    .line 137
    .line 138
    div-float/2addr v2, p1

    .line 139
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 140
    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_5
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 144
    .line 145
    iget v2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontSize:I

    .line 146
    .line 147
    int-to-float v2, v2

    .line 148
    iget p1, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontScale:I

    .line 149
    .line 150
    int-to-float p1, p1

    .line 151
    div-float/2addr p1, v1

    .line 152
    mul-float v2, v2, p1

    .line 153
    .line 154
    mul-float v2, v2, v0

    .line 155
    .line 156
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 157
    .line 158
    .line 159
    :goto_2
    iget-object p1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 160
    .line 161
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 162
    .line 163
    iget p2, p2, Lcom/intsig/office/simpletext/view/CharAttr;->fontColor:I

    .line 164
    .line 165
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 166
    .line 167
    .line 168
    return-void
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
    .locals 4

    .line 1
    iget-object p4, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-interface {p4, v0}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object p4

    .line 8
    iget-wide v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 11
    .line 12
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 13
    .line 14
    .line 15
    move-result-wide v2

    .line 16
    sub-long/2addr v0, v2

    .line 17
    long-to-int v1, v0

    .line 18
    iget-object v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 19
    .line 20
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 21
    .line 22
    .line 23
    move-result-wide v2

    .line 24
    sub-long/2addr p1, v2

    .line 25
    long-to-int p2, p1

    .line 26
    invoke-virtual {p4, v1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 31
    .line 32
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    float-to-int p1, p1

    .line 37
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    add-int/2addr p1, p2

    .line 44
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 45
    .line 46
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    add-int/2addr p1, p2

    .line 53
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 54
    .line 55
    const/4 p1, 0x1

    .line 56
    invoke-virtual {p0, p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 61
    .line 62
    return-object p3
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public setNumPages(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/LeafView;->hasUpdatedFieldText()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iput p1, p0, Lcom/intsig/office/wp/view/LeafView;->numPages:I

    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public viewToModel(IIZ)J
    .locals 4

    .line 1
    iget p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 2
    .line 3
    sub-int/2addr p1, p2

    .line 4
    iget-object p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 5
    .line 6
    const/4 p3, 0x0

    .line 7
    invoke-interface {p2, p3}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    iget-wide v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 12
    .line 13
    iget-object p3, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 14
    .line 15
    invoke-interface {p3}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 16
    .line 17
    .line 18
    move-result-wide v2

    .line 19
    sub-long/2addr v0, v2

    .line 20
    long-to-int p3, v0

    .line 21
    iget-wide v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->end:J

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->elem:Lcom/intsig/office/simpletext/model/IElement;

    .line 24
    .line 25
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 26
    .line 27
    .line 28
    move-result-wide v2

    .line 29
    sub-long/2addr v0, v2

    .line 30
    long-to-int v1, v0

    .line 31
    invoke-virtual {p2, p3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result p3

    .line 39
    new-array p3, p3, [F

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 42
    .line 43
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 44
    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    const/4 v1, 0x0

    .line 48
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    if-ge v0, v2, :cond_1

    .line 53
    .line 54
    int-to-float p1, p1

    .line 55
    aget v2, p3, v0

    .line 56
    .line 57
    sub-float/2addr p1, v2

    .line 58
    float-to-int p1, p1

    .line 59
    if-gtz p1, :cond_0

    .line 60
    .line 61
    int-to-float p1, p1

    .line 62
    add-float/2addr p1, v2

    .line 63
    const/high16 p2, 0x40000000    # 2.0f

    .line 64
    .line 65
    div-float/2addr v2, p2

    .line 66
    cmpl-float p1, p1, v2

    .line 67
    .line 68
    if-ltz p1, :cond_1

    .line 69
    .line 70
    add-int/lit8 v1, v1, 0x1

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 74
    .line 75
    add-int/lit8 v0, v0, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    :goto_1
    iget-wide p1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 79
    .line 80
    int-to-long v0, v1

    .line 81
    add-long/2addr p1, v0

    .line 82
    return-wide p1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

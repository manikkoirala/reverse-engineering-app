.class public Lcom/intsig/office/wp/view/BNView;
.super Lcom/intsig/office/simpletext/view/AbstractView;
.source "BNView.java"


# instance fields
.field private charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

.field private content:Ljava/lang/Object;

.field private currLevel:Lcom/intsig/office/common/bulletnumber/ListLevel;

.field private paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/view/AbstractView;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/simpletext/view/CharAttr;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/simpletext/view/CharAttr;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 10
    .line 11
    new-instance v0, Landroid/graphics/Paint;

    .line 12
    .line 13
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public declared-synchronized dispose()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    const/4 v0, 0x0

    .line 3
    :try_start_0
    iput-object v0, p0, Lcom/intsig/office/wp/view/BNView;->content:Ljava/lang/Object;

    .line 4
    .line 5
    iput-object v0, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/wp/view/BNView;->currLevel:Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getParaCount()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    add-int/lit8 v1, v1, -0x1

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setParaCount(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    .line 21
    .line 22
    :cond_0
    monitor-exit p0

    .line 23
    return-void

    .line 24
    :catchall_0
    move-exception v0

    .line 25
    monitor-exit p0

    .line 26
    throw v0
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public declared-synchronized doLayout(Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/ParagraphView;IIIII)I
    .locals 4

    monitor-enter p0

    .line 1
    :try_start_0
    iget p3, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listAlignIndent:I

    add-int/2addr p3, p6

    invoke-virtual {p0, p3, p7}, Lcom/intsig/office/simpletext/view/AbstractView;->setLocation(II)V

    .line 2
    invoke-virtual {p5}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p3

    const-string p6, ""

    .line 3
    iget p7, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listID:I

    const/4 p8, 0x0

    const/4 p9, 0x1

    if-ltz p7, :cond_9

    .line 4
    invoke-virtual {p5}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object p6

    invoke-interface {p6}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object p6

    invoke-virtual {p6}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    move-result-object p6

    iget p7, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listID:I

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p7

    invoke-virtual {p6, p7}, Lcom/intsig/office/common/bulletnumber/ListManage;->getListData(Ljava/lang/Integer;)Lcom/intsig/office/common/bulletnumber/ListData;

    move-result-object p6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p6, :cond_0

    .line 5
    monitor-exit p0

    return p8

    .line 6
    :cond_0
    :try_start_1
    invoke-virtual {p6}, Lcom/intsig/office/common/bulletnumber/ListData;->getLinkStyleID()S

    move-result p7

    if-ltz p7, :cond_2

    .line 7
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object p7

    invoke-virtual {p6}, Lcom/intsig/office/common/bulletnumber/ListData;->getLinkStyleID()S

    move-result p10

    invoke-virtual {p7, p10}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object p7

    if-eqz p7, :cond_2

    .line 8
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p6

    invoke-virtual {p7}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object p7

    invoke-virtual {p6, p7}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result p6

    .line 9
    invoke-virtual {p5}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object p5

    invoke-interface {p5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object p5

    invoke-virtual {p5}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    move-result-object p5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p6

    invoke-virtual {p5, p6}, Lcom/intsig/office/common/bulletnumber/ListManage;->getListData(Ljava/lang/Integer;)Lcom/intsig/office/common/bulletnumber/ListData;

    move-result-object p6

    if-eqz p6, :cond_1

    .line 10
    invoke-virtual {p6}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevels()[Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-result-object p5

    array-length p5, p5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p5, :cond_2

    .line 11
    :cond_1
    monitor-exit p0

    return p8

    .line 12
    :cond_2
    :try_start_2
    invoke-interface {p3}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    invoke-interface {p1, v0, v1}, Lcom/intsig/office/simpletext/model/IDocument;->getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    .line 13
    iget-byte p5, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    invoke-virtual {p6, p5}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-result-object p5

    .line 14
    invoke-static {}, Lcom/intsig/office/common/bulletnumber/ListKit;->instance()Lcom/intsig/office/common/bulletnumber/ListKit;

    move-result-object p7

    iget-byte p10, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    invoke-virtual {p7, p6, p5, p2, p10}, Lcom/intsig/office/common/bulletnumber/ListKit;->getBulletText(Lcom/intsig/office/common/bulletnumber/ListData;Lcom/intsig/office/common/bulletnumber/ListLevel;Lcom/intsig/office/simpletext/view/DocAttr;I)Ljava/lang/String;

    move-result-object p7

    .line 15
    iget-byte p10, p2, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    if-ne p10, p9, :cond_3

    .line 16
    invoke-virtual {p6}, Lcom/intsig/office/common/bulletnumber/ListData;->getNormalPreParaLevel()B

    move-result p10

    goto :goto_0

    :cond_3
    invoke-virtual {p6}, Lcom/intsig/office/common/bulletnumber/ListData;->getPreParaLevel()B

    move-result p10

    .line 17
    :goto_0
    iget-byte v0, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    if-ge v0, p10, :cond_5

    add-int/2addr v0, p9

    :goto_1
    const/16 p10, 0x9

    if-ge v0, p10, :cond_7

    .line 18
    iget-byte p10, p2, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    if-ne p10, p9, :cond_4

    .line 19
    invoke-virtual {p6, v0}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-result-object p10

    invoke-virtual {p10, p8}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNormalParaCount(I)V

    goto :goto_2

    .line 20
    :cond_4
    invoke-virtual {p6, v0}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-result-object p10

    invoke-virtual {p10, p8}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setParaCount(I)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    if-le v0, p10, :cond_7

    add-int/2addr p10, p9

    .line 21
    :goto_3
    iget-byte v0, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    if-ge p10, v0, :cond_7

    .line 22
    invoke-virtual {p6, p10}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-result-object v0

    .line 23
    iget-byte v1, p2, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    if-ne v1, p9, :cond_6

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getNormalParaCount()I

    move-result v1

    add-int/2addr v1, p9

    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNormalParaCount(I)V

    goto :goto_4

    .line 25
    :cond_6
    invoke-virtual {v0}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getParaCount()I

    move-result v1

    add-int/2addr v1, p9

    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setParaCount(I)V

    :goto_4
    add-int/lit8 p10, p10, 0x1

    goto :goto_3

    .line 26
    :cond_7
    iget-byte p2, p2, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    if-ne p2, p9, :cond_8

    .line 27
    invoke-virtual {p5}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getNormalParaCount()I

    move-result p2

    add-int/2addr p2, p9

    invoke-virtual {p5, p2}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setNormalParaCount(I)V

    .line 28
    iget-byte p2, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    invoke-virtual {p6, p2}, Lcom/intsig/office/common/bulletnumber/ListData;->setNormalPreParaLevel(B)V

    goto :goto_5

    .line 29
    :cond_8
    invoke-virtual {p5}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getParaCount()I

    move-result p2

    add-int/2addr p2, p9

    invoke-virtual {p5, p2}, Lcom/intsig/office/common/bulletnumber/ListLevel;->setParaCount(I)V

    .line 30
    iget-byte p2, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    invoke-virtual {p6, p2}, Lcom/intsig/office/common/bulletnumber/ListData;->setPreParaLevel(B)V

    .line 31
    :goto_5
    iput-object p5, p0, Lcom/intsig/office/wp/view/BNView;->currLevel:Lcom/intsig/office/common/bulletnumber/ListLevel;

    move-object p6, p7

    goto :goto_6

    .line 32
    :cond_9
    iget p2, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->pgBulletID:I

    if-ltz p2, :cond_a

    .line 33
    invoke-interface {p3}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    move-result-wide p6

    invoke-interface {p1, p6, p7}, Lcom/intsig/office/simpletext/model/IDocument;->getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    .line 34
    invoke-virtual {p5}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object p2

    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    move-result-object p2

    invoke-virtual {p2}, Lcom/intsig/office/system/SysKit;->getPGBulletText()Lcom/intsig/office/pg/model/PGBulletText;

    move-result-object p2

    iget p4, p4, Lcom/intsig/office/simpletext/view/ParaAttr;->pgBulletID:I

    invoke-virtual {p2, p4}, Lcom/intsig/office/pg/model/PGBulletText;->getBulletText(I)Ljava/lang/String;

    move-result-object p6

    if-nez p6, :cond_b

    const-string p6, ""

    goto :goto_6

    :cond_a
    const/4 p1, 0x0

    .line 35
    :cond_b
    :goto_6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    iget-object p4, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    invoke-interface {p3}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object p3

    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object p1

    invoke-virtual {p2, p4, p3, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->fillCharAttr(Lcom/intsig/office/simpletext/view/CharAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 36
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    iget-boolean p2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isBold:Z

    if-eqz p2, :cond_c

    iget-boolean p3, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isItalic:Z

    if-eqz p3, :cond_c

    .line 37
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    const p2, -0x41b33333    # -0.2f

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 38
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p9}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    goto :goto_7

    :cond_c
    if-eqz p2, :cond_d

    .line 39
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p9}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    goto :goto_7

    .line 40
    :cond_d
    iget-boolean p1, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isItalic:Z

    if-eqz p1, :cond_e

    .line 41
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    const/high16 p2, -0x41800000    # -0.25f

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 42
    :cond_e
    :goto_7
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-static {p2, p8}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 43
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    iget-object p2, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    iget p3, p2, Lcom/intsig/office/simpletext/view/CharAttr;->fontSize:I

    int-to-float p3, p3

    iget p2, p2, Lcom/intsig/office/simpletext/view/CharAttr;->fontScale:I

    int-to-float p2, p2

    const/high16 p4, 0x42c80000    # 100.0f

    div-float/2addr p2, p4

    mul-float p3, p3, p2

    const p2, 0x3faaaaab

    mul-float p3, p3, p2

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 44
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    iget-object p2, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    iget p2, p2, Lcom/intsig/office/simpletext/view/CharAttr;->fontColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 45
    invoke-virtual {p6}, Ljava/lang/String;->length()I

    move-result p1

    new-array p2, p1, [F

    .line 46
    iget-object p3, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3, p6, p2}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    const/4 p3, 0x0

    const/4 p4, 0x0

    :goto_8
    if-ge p4, p1, :cond_f

    .line 47
    aget p5, p2, p4

    add-float/2addr p3, p5

    add-int/lit8 p4, p4, 0x1

    goto :goto_8

    .line 48
    :cond_f
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    move-result p1

    int-to-float p1, p1

    add-float/2addr p1, p3

    const/high16 p2, 0x41e00000    # 28.0f

    rem-float/2addr p1, p2

    float-to-int p1, p1

    if-lez p1, :cond_10

    int-to-float p1, p1

    sub-float/2addr p2, p1

    add-float/2addr p3, p2

    :cond_10
    float-to-int p1, p3

    .line 49
    iget-object p2, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Landroid/graphics/Paint;->descent()F

    move-result p2

    iget-object p3, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3}, Landroid/graphics/Paint;->ascent()F

    move-result p3

    sub-float/2addr p2, p3

    float-to-double p2, p2

    invoke-static {p2, p3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p2

    double-to-int p2, p2

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    .line 50
    iput-object p6, p0, Lcom/intsig/office/wp/view/BNView;->content:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 51
    monitor-exit p0

    return p8

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public draw(Landroid/graphics/Canvas;IIF)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    mul-float v0, v0, p4

    .line 5
    .line 6
    float-to-int v0, v0

    .line 7
    add-int/2addr v0, p2

    .line 8
    iget p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 9
    .line 10
    int-to-float p2, p2

    .line 11
    mul-float p2, p2, p4

    .line 12
    .line 13
    float-to-int p2, p2

    .line 14
    add-int/2addr p2, p3

    .line 15
    iget-object p3, p0, Lcom/intsig/office/wp/view/BNView;->content:Ljava/lang/Object;

    .line 16
    .line 17
    if-eqz p3, :cond_1

    .line 18
    .line 19
    instance-of v1, p3, Ljava/lang/String;

    .line 20
    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    move-object v3, p3

    .line 24
    check-cast v3, Ljava/lang/String;

    .line 25
    .line 26
    iget-object p3, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 27
    .line 28
    invoke-virtual {p3}, Landroid/graphics/Paint;->getTextSize()F

    .line 29
    .line 30
    .line 31
    move-result p3

    .line 32
    iget-object v1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/office/wp/view/BNView;->charAttr:Lcom/intsig/office/simpletext/view/CharAttr;

    .line 35
    .line 36
    iget-short v2, v2, Lcom/intsig/office/simpletext/view/CharAttr;->subSuperScriptType:S

    .line 37
    .line 38
    if-lez v2, :cond_0

    .line 39
    .line 40
    const/high16 v2, 0x40000000    # 2.0f

    .line 41
    .line 42
    div-float v2, p3, v2

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    move v2, p3

    .line 46
    :goto_0
    mul-float v2, v2, p4

    .line 47
    .line 48
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 49
    .line 50
    .line 51
    const/4 v4, 0x0

    .line 52
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    int-to-float v6, v0

    .line 57
    int-to-float p2, p2

    .line 58
    iget-object p4, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 59
    .line 60
    invoke-virtual {p4}, Landroid/graphics/Paint;->ascent()F

    .line 61
    .line 62
    .line 63
    move-result p4

    .line 64
    sub-float v7, p2, p4

    .line 65
    .line 66
    iget-object v8, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 67
    .line 68
    move-object v2, p1

    .line 69
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 73
    .line 74
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 75
    .line 76
    .line 77
    :cond_1
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public free()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBaseline()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/BNView;->paint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    neg-float v0, v0

    .line 8
    float-to-int v0, v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

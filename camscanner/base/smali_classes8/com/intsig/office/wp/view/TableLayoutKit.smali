.class public Lcom/intsig/office/wp/view/TableLayoutKit;
.super Ljava/lang/Object;
.source "TableLayoutKit.java"


# instance fields
.field private breakPagesCell:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/wp/view/BreakPagesCell;",
            ">;"
        }
    .end annotation
.end field

.field private breakRowElement:Lcom/intsig/office/wp/model/RowElement;

.field private breakRowView:Lcom/intsig/office/wp/view/RowView;

.field private isRowBreakPages:Z

.field private mergedCell:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/intsig/office/wp/view/CellView;",
            ">;"
        }
    .end annotation
.end field

.field private rowIndex:S

.field private tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/simpletext/view/TableAttr;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/simpletext/view/TableAttr;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    .line 10
    .line 11
    new-instance v0, Ljava/util/Vector;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 17
    .line 18
    new-instance v0, Ljava/util/LinkedHashMap;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private clearCurrentRowBreakPageCell(Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 9

    .line 1
    new-instance v0, Ljava/util/Vector;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    check-cast v2, Ljava/lang/Integer;

    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 29
    .line 30
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    check-cast v3, Lcom/intsig/office/wp/view/BreakPagesCell;

    .line 35
    .line 36
    invoke-virtual {v3}, Lcom/intsig/office/wp/view/BreakPagesCell;->getCell()Lcom/intsig/office/wp/model/CellElement;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 41
    .line 42
    .line 43
    move-result-wide v4

    .line 44
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 45
    .line 46
    .line 47
    move-result-wide v6

    .line 48
    cmp-long v8, v4, v6

    .line 49
    .line 50
    if-ltz v8, :cond_0

    .line 51
    .line 52
    invoke-virtual {v3}, Lcom/intsig/office/wp/view/BreakPagesCell;->getCell()Lcom/intsig/office/wp/model/CellElement;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 57
    .line 58
    .line 59
    move-result-wide v3

    .line 60
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 61
    .line 62
    .line 63
    move-result-wide v5

    .line 64
    cmp-long v7, v3, v5

    .line 65
    .line 66
    if-gtz v7, :cond_0

    .line 67
    .line 68
    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eqz v0, :cond_2

    .line 81
    .line 82
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    check-cast v0, Ljava/lang/Integer;

    .line 87
    .line 88
    iget-object v1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 89
    .line 90
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_2
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private isBreakPages(Lcom/intsig/office/wp/view/RowView;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :goto_0
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-interface {p1, v1}, Lcom/intsig/office/simpletext/view/IView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    .line 13
    .line 14
    .line 15
    move-result-wide v1

    .line 16
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 17
    .line 18
    .line 19
    move-result-wide v3

    .line 20
    cmp-long v0, v1, v3

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getWidth()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-lez v0, :cond_0

    .line 29
    .line 30
    const/4 p1, 0x1

    .line 31
    return p1

    .line 32
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const/4 p1, 0x0

    .line 38
    return p1
.end method

.method private layoutCellVerticalAlign(Lcom/intsig/office/wp/view/CellView;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    .line 2
    .line 3
    iget-byte v0, v0, Lcom/intsig/office/simpletext/view/TableAttr;->cellVerticalAlign:B

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    const/4 v2, 0x1

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-interface {v0, v2}, Lcom/intsig/office/simpletext/view/IView;->getLayoutSpan(B)I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    add-int/2addr v1, v2

    .line 21
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {p1, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    sub-int/2addr v0, v1

    .line 31
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    invoke-interface {v3}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-virtual {v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-eq v1, v2, :cond_2

    .line 48
    .line 49
    const/4 v3, 0x2

    .line 50
    if-ne v1, v3, :cond_4

    .line 51
    .line 52
    :cond_2
    if-ne v1, v2, :cond_3

    .line 53
    .line 54
    div-int/lit8 v0, v0, 0x2

    .line 55
    .line 56
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    :goto_1
    if-eqz p1, :cond_4

    .line 61
    .line 62
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    add-int/2addr v1, v0

    .line 67
    invoke-interface {p1, v1}, Lcom/intsig/office/simpletext/view/IView;->setY(I)V

    .line 68
    .line 69
    .line 70
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    goto :goto_1

    .line 75
    :cond_4
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private layoutMergedCell(Lcom/intsig/office/wp/view/RowView;Lcom/intsig/office/wp/model/RowElement;Z)V
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    add-int/2addr v0, v2

    .line 14
    if-eqz p3, :cond_3

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    if-eqz p2, :cond_2

    .line 27
    .line 28
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    check-cast p2, Lcom/intsig/office/wp/view/CellView;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 35
    .line 36
    .line 37
    move-result-object p3

    .line 38
    if-eqz p3, :cond_1

    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 41
    .line 42
    .line 43
    move-result-object p3

    .line 44
    invoke-interface {p3}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 45
    .line 46
    .line 47
    move-result p3

    .line 48
    sub-int p3, v0, p3

    .line 49
    .line 50
    invoke-virtual {p2, p3}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0, p2}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCellVerticalAlign(Lcom/intsig/office/wp/view/CellView;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/util/Vector;->clear()V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :cond_3
    iget-object p3, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 64
    .line 65
    invoke-virtual {p3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 66
    .line 67
    .line 68
    move-result-object p3

    .line 69
    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    if-eqz v2, :cond_4

    .line 74
    .line 75
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    check-cast v2, Lcom/intsig/office/wp/view/CellView;

    .line 80
    .line 81
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-interface {v3}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    invoke-virtual {v2, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    add-int/2addr v3, v2

    .line 94
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    goto :goto_1

    .line 99
    :cond_4
    new-instance p3, Ljava/util/Vector;

    .line 100
    .line 101
    invoke-direct {p3}, Ljava/util/Vector;-><init>()V

    .line 102
    .line 103
    .line 104
    iget-object v2, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 105
    .line 106
    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    if-eqz v3, :cond_b

    .line 115
    .line 116
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    check-cast v3, Lcom/intsig/office/wp/view/CellView;

    .line 121
    .line 122
    invoke-virtual {v3}, Lcom/intsig/office/wp/view/CellView;->getColumn()S

    .line 123
    .line 124
    .line 125
    move-result v4

    .line 126
    invoke-virtual {p2, v4}, Lcom/intsig/office/wp/model/RowElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 127
    .line 128
    .line 129
    move-result-object v4

    .line 130
    if-nez v4, :cond_6

    .line 131
    .line 132
    goto :goto_2

    .line 133
    :cond_6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 134
    .line 135
    .line 136
    move-result-object v5

    .line 137
    invoke-interface {v4}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 138
    .line 139
    .line 140
    move-result-object v6

    .line 141
    invoke-virtual {v5, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->isTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 142
    .line 143
    .line 144
    move-result v5

    .line 145
    if-eqz v5, :cond_7

    .line 146
    .line 147
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 148
    .line 149
    .line 150
    move-result-object v5

    .line 151
    invoke-interface {v4}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 152
    .line 153
    .line 154
    move-result-object v4

    .line 155
    invoke-virtual {v5, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->isTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 156
    .line 157
    .line 158
    move-result v4

    .line 159
    if-eqz v4, :cond_5

    .line 160
    .line 161
    :cond_7
    invoke-virtual {v3, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 162
    .line 163
    .line 164
    move-result v4

    .line 165
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 166
    .line 167
    .line 168
    move-result-object v5

    .line 169
    invoke-interface {v5}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 170
    .line 171
    .line 172
    move-result v5

    .line 173
    add-int/2addr v5, v4

    .line 174
    if-ge v5, v0, :cond_8

    .line 175
    .line 176
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 177
    .line 178
    .line 179
    move-result-object v4

    .line 180
    invoke-interface {v4}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 181
    .line 182
    .line 183
    move-result v4

    .line 184
    sub-int v4, v0, v4

    .line 185
    .line 186
    invoke-virtual {v3, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 187
    .line 188
    .line 189
    invoke-direct {p0, v3}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCellVerticalAlign(Lcom/intsig/office/wp/view/CellView;)V

    .line 190
    .line 191
    .line 192
    goto :goto_4

    .line 193
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 194
    .line 195
    .line 196
    move-result v4

    .line 197
    sub-int v4, v0, v4

    .line 198
    .line 199
    invoke-virtual {p1, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 203
    .line 204
    .line 205
    move-result-object v4

    .line 206
    check-cast v4, Lcom/intsig/office/wp/view/CellView;

    .line 207
    .line 208
    :goto_3
    if-eqz v4, :cond_a

    .line 209
    .line 210
    invoke-virtual {v4}, Lcom/intsig/office/wp/view/CellView;->isMergedCell()Z

    .line 211
    .line 212
    .line 213
    move-result v5

    .line 214
    if-nez v5, :cond_9

    .line 215
    .line 216
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 217
    .line 218
    .line 219
    move-result v5

    .line 220
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 221
    .line 222
    .line 223
    move-result-object v6

    .line 224
    invoke-interface {v6}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 225
    .line 226
    .line 227
    move-result v6

    .line 228
    sub-int v6, v0, v6

    .line 229
    .line 230
    invoke-virtual {v4, v6}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 231
    .line 232
    .line 233
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 234
    .line 235
    .line 236
    move-result v6

    .line 237
    if-eq v5, v6, :cond_9

    .line 238
    .line 239
    invoke-direct {p0, v4}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCellVerticalAlign(Lcom/intsig/office/wp/view/CellView;)V

    .line 240
    .line 241
    .line 242
    :cond_9
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 243
    .line 244
    .line 245
    move-result-object v4

    .line 246
    check-cast v4, Lcom/intsig/office/wp/view/CellView;

    .line 247
    .line 248
    goto :goto_3

    .line 249
    :cond_a
    :goto_4
    invoke-virtual {p3, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 250
    .line 251
    .line 252
    goto/16 :goto_2

    .line 253
    .line 254
    :cond_b
    invoke-virtual {p3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 255
    .line 256
    .line 257
    move-result-object p2

    .line 258
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 259
    .line 260
    .line 261
    move-result p3

    .line 262
    if-eqz p3, :cond_d

    .line 263
    .line 264
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 265
    .line 266
    .line 267
    move-result-object p3

    .line 268
    check-cast p3, Lcom/intsig/office/wp/view/CellView;

    .line 269
    .line 270
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 271
    .line 272
    .line 273
    move-result-object v0

    .line 274
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 275
    .line 276
    .line 277
    move-result v0

    .line 278
    invoke-virtual {p3, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 279
    .line 280
    .line 281
    move-result v2

    .line 282
    add-int/2addr v0, v2

    .line 283
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 284
    .line 285
    .line 286
    move-result v2

    .line 287
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 288
    .line 289
    .line 290
    move-result v3

    .line 291
    add-int/2addr v2, v3

    .line 292
    if-le v0, v2, :cond_c

    .line 293
    .line 294
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 295
    .line 296
    .line 297
    move-result v0

    .line 298
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 299
    .line 300
    .line 301
    move-result v2

    .line 302
    add-int/2addr v0, v2

    .line 303
    invoke-virtual {p3}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 304
    .line 305
    .line 306
    move-result v2

    .line 307
    sub-int/2addr v0, v2

    .line 308
    invoke-virtual {p3, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 309
    .line 310
    .line 311
    :cond_c
    iget-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    .line 312
    .line 313
    invoke-virtual {v0, p3}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 314
    .line 315
    .line 316
    goto :goto_5

    .line 317
    :cond_d
    return-void
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method


# virtual methods
.method public clearBreakPages()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-short v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowView:Lcom/intsig/office/wp/view/RowView;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 5
    .line 6
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowView:Lcom/intsig/office/wp/view/RowView;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isTableBreakPages()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gtz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public layoutCell(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/CellView;JIIIIIIZ)I
    .locals 25

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    .line 1
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/wp/model/CellElement;

    .line 2
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    iget-object v4, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    invoke-virtual {v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->fillTableAttr(Lcom/intsig/office/simpletext/view/TableAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 3
    iget-object v3, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget v3, v3, Lcom/intsig/office/simpletext/view/TableAttr;->cellBackground:I

    invoke-virtual {v1, v3}, Lcom/intsig/office/wp/view/CellView;->setBackground(I)V

    .line 4
    iget-object v3, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget v4, v3, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    iget v5, v3, Lcom/intsig/office/simpletext/view/TableAttr;->topMargin:I

    iget v6, v3, Lcom/intsig/office/simpletext/view/TableAttr;->rightMargin:I

    iget v3, v3, Lcom/intsig/office/simpletext/view/TableAttr;->bottomMargin:I

    invoke-virtual {v1, v4, v5, v6, v3}, Lcom/intsig/office/simpletext/view/AbstractView;->setIndent(IIII)V

    .line 5
    iget-object v3, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget v15, v3, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    .line 6
    iget v3, v3, Lcom/intsig/office/simpletext/view/TableAttr;->topMargin:I

    .line 7
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    move-result-wide v18

    .line 8
    iget-object v4, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget v5, v4, Lcom/intsig/office/simpletext/view/TableAttr;->topMargin:I

    sub-int v5, p13, v5

    iget v6, v4, Lcom/intsig/office/simpletext/view/TableAttr;->bottomMargin:I

    sub-int/2addr v5, v6

    .line 9
    iget v6, v4, Lcom/intsig/office/simpletext/view/TableAttr;->cellWidth:I

    iget v7, v4, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    sub-int/2addr v6, v7

    iget v4, v4, Lcom/intsig/office/simpletext/view/TableAttr;->rightMargin:I

    sub-int v14, v6, v4

    const/4 v13, 0x0

    move-wide/from16 v11, p8

    move v10, v3

    move/from16 v20, v5

    const/4 v4, 0x0

    const/4 v9, 0x0

    move/from16 v3, p14

    :goto_0
    const/4 v8, 0x1

    cmp-long v21, v11, v18

    if-gez v21, :cond_2

    if-lez v20, :cond_2

    if-eq v4, v8, :cond_2

    move-object/from16 v7, p2

    .line 10
    invoke-interface {v7, v11, v12}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v4

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object/from16 p10, v2

    move-object/from16 v2, p1

    .line 11
    invoke-static {v2, v4, v6, v5}, Lcom/intsig/office/wp/view/ViewFactory;->createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;

    move-result-object v5

    check-cast v5, Lcom/intsig/office/wp/view/ParagraphView;

    .line 12
    invoke-virtual {v1, v5}, Lcom/intsig/office/simpletext/view/AbstractView;->appendChlidView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 13
    invoke-virtual {v5, v11, v12}, Lcom/intsig/office/simpletext/view/AbstractView;->setStartOffset(J)V

    .line 14
    invoke-virtual {v5, v15, v10}, Lcom/intsig/office/simpletext/view/AbstractView;->setLocation(II)V

    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object v8

    invoke-interface {v4}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v4

    move-object/from16 v2, p6

    invoke-virtual {v6, v8, v2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->fillParaAttr(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 16
    invoke-static {}, Lcom/intsig/office/wp/view/LayoutKit;->instance()Lcom/intsig/office/wp/view/LayoutKit;

    move-result-object v4

    move-object v8, v5

    move-object/from16 v5, p1

    const/4 v2, 0x0

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    move-object/from16 p8, v8

    const/4 v2, 0x1

    move-object/from16 v8, p5

    move/from16 v22, v9

    move-object/from16 v9, p6

    move/from16 v23, v10

    move-object/from16 v10, p8

    move-wide/from16 p11, v11

    move v13, v15

    move/from16 p13, v14

    move/from16 v14, v23

    move/from16 v24, v15

    move/from16 v15, p13

    move/from16 v16, v20

    move/from16 v17, v3

    invoke-virtual/range {v4 .. v17}, Lcom/intsig/office/wp/view/LayoutKit;->layoutPara(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/ParagraphView;JIIIII)I

    move-result v4

    move-object/from16 v5, p8

    .line 17
    invoke-virtual {v5, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v6

    .line 18
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v7

    if-nez v7, :cond_0

    .line 19
    invoke-virtual {v1, v5, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->deleteView(Lcom/intsig/office/simpletext/view/IView;Z)V

    move/from16 v13, v22

    goto :goto_1

    .line 20
    :cond_0
    invoke-interface/range {p3 .. p3}, Lcom/intsig/office/simpletext/view/IRoot;->getViewContainer()Lcom/intsig/office/simpletext/view/ViewContainer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 21
    invoke-interface/range {p3 .. p3}, Lcom/intsig/office/simpletext/view/IRoot;->getViewContainer()Lcom/intsig/office/simpletext/view/ViewContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/intsig/office/simpletext/view/ViewContainer;->add(Lcom/intsig/office/simpletext/view/IView;)V

    :cond_1
    add-int v10, v23, v6

    move/from16 v13, v22

    add-int v9, v13, v6

    sub-int v20, v20, v6

    const/4 v2, 0x0

    .line 22
    invoke-virtual {v5, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    move-result-wide v11

    .line 23
    invoke-virtual {v5, v11, v12}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 24
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v5}, Lcom/intsig/office/simpletext/view/ViewKit;->setBitValue(IIZ)I

    move-result v3

    move-object/from16 v2, p10

    move/from16 v14, p13

    move/from16 v15, v24

    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_2
    move-object/from16 p10, v2

    move v13, v9

    move-wide/from16 p11, v11

    move/from16 p13, v14

    const/4 v2, 0x1

    :goto_1
    if-gez v21, :cond_4

    .line 25
    iget-object v3, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move/from16 v6, p13

    if-lez v6, :cond_3

    .line 26
    iget-object v3, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v7, Lcom/intsig/office/wp/view/BreakPagesCell;

    move-object/from16 v8, p10

    move-wide/from16 v11, p11

    invoke-direct {v7, v8, v11, v12}, Lcom/intsig/office/wp/view/BreakPagesCell;-><init>(Lcom/intsig/office/wp/model/CellElement;J)V

    invoke-interface {v3, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iput-boolean v2, v0, Lcom/intsig/office/wp/view/TableLayoutKit;->isRowBreakPages:Z

    goto :goto_2

    :cond_3
    move-wide/from16 v11, p11

    goto :goto_2

    :cond_4
    move-wide/from16 v11, p11

    move/from16 v6, p13

    .line 28
    :goto_2
    invoke-virtual {v1, v11, v12}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 29
    invoke-virtual {v1, v6, v13}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    return v4
.end method

.method public layoutCellForNull(Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/CellView;JIIIIIIZ)I
    .locals 0

    .line 1
    invoke-virtual {p6}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/wp/model/CellElement;

    .line 2
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p2

    iget-object p3, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->fillTableAttr(Lcom/intsig/office/simpletext/view/TableAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 3
    iget-object p1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget p2, p1, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    iget p3, p1, Lcom/intsig/office/simpletext/view/TableAttr;->topMargin:I

    iget p4, p1, Lcom/intsig/office/simpletext/view/TableAttr;->rightMargin:I

    iget p1, p1, Lcom/intsig/office/simpletext/view/TableAttr;->bottomMargin:I

    invoke-virtual {p6, p2, p3, p4, p1}, Lcom/intsig/office/simpletext/view/AbstractView;->setIndent(IIII)V

    .line 4
    iget-object p1, p0, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget p2, p1, Lcom/intsig/office/simpletext/view/TableAttr;->cellWidth:I

    iget p3, p1, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    sub-int/2addr p2, p3

    iget p1, p1, Lcom/intsig/office/simpletext/view/TableAttr;->rightMargin:I

    sub-int/2addr p2, p1

    const/4 p1, 0x0

    .line 5
    invoke-virtual {p6, p2, p1}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    return p1
.end method

.method public layoutRow(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/RowView;JIIIIIZ)I
    .locals 34

    move-object/from16 v15, p0

    move-object/from16 v14, p7

    move/from16 v13, p15

    .line 1
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/intsig/office/wp/model/RowElement;

    .line 2
    invoke-virtual {v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    move-result-wide v10

    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v0

    invoke-virtual {v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableRowHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3d888889

    mul-float v0, v0, v1

    float-to-int v0, v0

    const/4 v9, 0x0

    move/from16 v17, p12

    move v8, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v18, 0x1

    move-wide/from16 v0, p8

    .line 4
    :goto_0
    invoke-virtual {v12}, Lcom/intsig/office/wp/model/RowElement;->getCellNumber()I

    move-result v7

    move-wide/from16 v19, v10

    const/4 v10, 0x0

    if-ge v6, v7, :cond_c

    if-eqz v13, :cond_1

    .line 5
    iget-object v7, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 6
    iget-object v2, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7
    iget-object v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/wp/view/BreakPagesCell;

    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/BreakPagesCell;->getCell()Lcom/intsig/office/wp/model/CellElement;

    move-result-object v1

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/BreakPagesCell;->getBreakOffset()J

    move-result-wide v21

    move-object/from16 p11, v12

    move-wide/from16 v11, v21

    const/4 v0, 0x0

    goto :goto_3

    .line 10
    :cond_0
    invoke-virtual {v12, v6}, Lcom/intsig/office/wp/model/RowElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v2

    move-object/from16 p11, v12

    move-wide v11, v0

    move-object v1, v2

    const/4 v0, 0x1

    goto :goto_3

    .line 11
    :cond_1
    invoke-virtual {v12, v6}, Lcom/intsig/office/wp/model/RowElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v1

    if-nez v1, :cond_2

    goto/16 :goto_a

    .line 12
    :cond_2
    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    move-result-wide v21

    .line 13
    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    move-result-wide v23

    cmp-long v0, v21, v23

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_5

    .line 14
    iget-object v2, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowView:Lcom/intsig/office/wp/view/RowView;

    if-eqz v2, :cond_5

    if-eqz v13, :cond_5

    int-to-short v7, v6

    .line 15
    invoke-virtual {v2, v7}, Lcom/intsig/office/wp/view/RowView;->getCellView(S)Lcom/intsig/office/wp/view/CellView;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 16
    invoke-virtual {v2, v10}, Lcom/intsig/office/simpletext/view/AbstractView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    move-result-wide v23

    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    move-result-wide v25

    cmp-long v0, v23, v25

    if-nez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :cond_5
    :goto_2
    move-object/from16 p11, v12

    move-wide/from16 v11, v21

    :goto_3
    const/16 v2, 0xb

    move-object/from16 v7, p1

    .line 17
    invoke-static {v7, v1, v10, v2}, Lcom/intsig/office/wp/view/ViewFactory;->createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/wp/view/CellView;

    .line 18
    invoke-virtual {v14, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->appendChlidView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 19
    invoke-virtual {v2, v11, v12}, Lcom/intsig/office/simpletext/view/AbstractView;->setStartOffset(J)V

    .line 20
    invoke-virtual {v2, v4, v9}, Lcom/intsig/office/simpletext/view/AbstractView;->setLocation(II)V

    int-to-short v9, v6

    .line 21
    invoke-virtual {v2, v9}, Lcom/intsig/office/wp/view/CellView;->setColumn(S)V

    if-eqz v0, :cond_6

    .line 22
    invoke-virtual {v2, v13}, Lcom/intsig/office/wp/view/CellView;->setFirstMergedCell(Z)V

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object v9, v2

    move-object/from16 v2, p3

    move/from16 v27, v3

    move-object/from16 v3, p4

    move/from16 v22, v4

    move-object/from16 v4, p5

    move/from16 v28, v5

    move-object/from16 v5, p6

    move/from16 v23, v6

    move-object v6, v9

    move/from16 v29, v8

    move-wide v7, v11

    move-object v11, v9

    const/4 v12, 0x0

    move/from16 v9, v22

    move-wide/from16 v30, v19

    move/from16 v10, v21

    move-object/from16 v32, v11

    move/from16 v11, v17

    move-object/from16 v19, p11

    move/from16 v12, p13

    move/from16 v13, p14

    move/from16 v14, v23

    move/from16 v15, p15

    .line 23
    invoke-virtual/range {v0 .. v15}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCellForNull(Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/CellView;JIIIIIIZ)I

    move-result v0

    move v2, v0

    move-object/from16 v0, v32

    :goto_4
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_6
    move-object/from16 v32, v2

    move/from16 v27, v3

    move/from16 v22, v4

    move/from16 v28, v5

    move/from16 v23, v6

    move/from16 v29, v8

    move-wide/from16 v30, v19

    move-object/from16 v19, p11

    if-nez p15, :cond_8

    .line 24
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v0

    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->isTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_5

    :cond_7
    move-object/from16 v15, v32

    const/4 v9, 0x0

    goto :goto_6

    :cond_8
    :goto_5
    move-object/from16 v15, v32

    const/4 v9, 0x1

    :goto_6
    invoke-virtual {v15, v9}, Lcom/intsig/office/wp/view/CellView;->setFirstMergedCell(Z)V

    .line 25
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v0

    invoke-interface {v1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->isTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    move-result v0

    invoke-virtual {v15, v0}, Lcom/intsig/office/wp/view/CellView;->setMergedCell(Z)V

    const/4 v0, 0x0

    move-wide v8, v11

    move v11, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v7, v15

    move/from16 v10, v22

    move/from16 v12, v17

    move/from16 v13, p13

    move/from16 v14, p14

    move-object/from16 v33, v15

    move/from16 v15, v23

    move/from16 v16, p15

    .line 26
    invoke-virtual/range {v0 .. v16}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCell(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/CellView;JIIIIIIZ)I

    move-result v0

    move v2, v0

    move-object/from16 v0, v33

    goto :goto_4

    .line 27
    :goto_7
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v3

    const/4 v4, 0x1

    .line 28
    invoke-virtual {v0, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v5

    if-eqz v18, :cond_9

    if-nez v5, :cond_9

    const/16 v18, 0x1

    goto :goto_8

    :cond_9
    const/16 v18, 0x0

    :goto_8
    add-int v6, v22, v3

    move/from16 v9, v28

    add-int v7, v9, v3

    sub-int v17, v17, v3

    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/CellView;->isMergedCell()Z

    move-result v3

    if-nez v3, :cond_a

    move/from16 v3, v29

    .line 30
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_9

    :cond_a
    move/from16 v3, v29

    :goto_9
    move v8, v3

    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/CellView;->isFirstMergedCell()Z

    move-result v3

    move-object/from16 v10, p0

    if-eqz v3, :cond_b

    .line 32
    iget-object v3, v10, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_b
    move/from16 v9, v27

    .line 33
    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v5, 0x0

    .line 34
    invoke-virtual {v0, v5}, Lcom/intsig/office/simpletext/view/AbstractView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    move-result-wide v11

    add-int/lit8 v0, v23, 0x1

    move-object/from16 v14, p7

    move/from16 v13, p15

    move v4, v6

    move v5, v7

    move-object v15, v10

    const/4 v9, 0x0

    move v6, v0

    move-wide v0, v11

    move-object/from16 v12, v19

    move-wide/from16 v10, v30

    goto/16 :goto_0

    :cond_c
    :goto_a
    move v9, v5

    move v3, v8

    move-object v5, v10

    move-object v10, v15

    move-wide/from16 v30, v19

    const/4 v4, 0x1

    .line 35
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/wp/view/CellView;

    :goto_b
    if-eqz v0, :cond_f

    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/CellView;->isMergedCell()Z

    move-result v1

    if-nez v1, :cond_e

    .line 37
    invoke-virtual {v0, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v1

    if-ge v1, v3, :cond_e

    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getTopIndent()I

    move-result v1

    sub-int v8, v3, v1

    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getBottomIndent()I

    move-result v1

    sub-int/2addr v8, v1

    invoke-virtual {v0, v8}, Lcom/intsig/office/simpletext/view/AbstractView;->setHeight(I)V

    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/wp/model/CellElement;

    if-eqz v1, :cond_d

    .line 40
    iget-object v6, v10, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v7

    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v1

    int-to-byte v1, v1

    iput-byte v1, v6, Lcom/intsig/office/simpletext/view/TableAttr;->cellVerticalAlign:B

    .line 41
    :cond_d
    invoke-direct {v10, v0}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutCellVerticalAlign(Lcom/intsig/office/wp/view/CellView;)V

    .line 42
    :cond_e
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/wp/view/CellView;

    goto :goto_b

    :cond_f
    move-object/from16 v0, p7

    move-wide/from16 v6, v30

    .line 43
    invoke-virtual {v0, v6, v7}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    if-eqz v18, :cond_10

    const v8, 0x7fffffff

    goto :goto_c

    :cond_10
    move v8, v3

    .line 44
    :goto_c
    invoke-virtual {v0, v9, v8}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    .line 45
    iput-object v5, v10, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowView:Lcom/intsig/office/wp/view/RowView;

    return v2
.end method

.method public layoutTable(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/TableView;JIIIIIZ)I
    .locals 27

    move-object/from16 v15, p0

    move-object/from16 v14, p7

    .line 1
    iget-object v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->mergedCell:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 2
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/intsig/office/wp/model/TableElement;

    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v0

    iget-object v1, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    invoke-virtual {v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->fillTableAttr(Lcom/intsig/office/simpletext/view/TableAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 4
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    move-result-object v0

    const/4 v12, 0x2

    const/4 v11, 0x1

    move/from16 v1, p14

    invoke-virtual {v0, v1, v12, v11}, Lcom/intsig/office/simpletext/view/ViewKit;->setBitValue(IIZ)I

    move-result v10

    .line 5
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    move-result-object v0

    const/4 v8, 0x0

    invoke-virtual {v0, v10, v8}, Lcom/intsig/office/simpletext/view/ViewKit;->getBitValue(II)Z

    move-result v0

    .line 6
    invoke-virtual {v13}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    move-result-wide v16

    const/4 v9, 0x0

    move-wide/from16 v6, p8

    move/from16 v19, p15

    move/from16 v18, v0

    move-object v1, v9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, p13

    :goto_0
    cmp-long v20, v6, v16

    if-gez v20, :cond_0

    if-gtz v0, :cond_1

    .line 7
    :cond_0
    iget-object v12, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    if-eqz v12, :cond_d

    if-eqz v19, :cond_d

    .line 8
    :cond_1
    iput-boolean v8, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->isRowBreakPages:Z

    .line 9
    iget-object v12, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    if-eqz v12, :cond_2

    if-eqz v19, :cond_2

    .line 10
    iput-object v9, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    goto :goto_1

    .line 11
    :cond_2
    iget-short v12, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    add-int/lit8 v9, v12, 0x1

    int-to-short v9, v9

    iput-short v9, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    invoke-virtual {v13, v12}, Lcom/intsig/office/wp/model/TableElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v12

    :goto_1
    if-nez v12, :cond_3

    move v8, v5

    move-wide/from16 p14, v6

    move-object/from16 v22, v13

    move-object v6, v14

    move-object v5, v15

    const/4 v2, 0x1

    const/4 v9, 0x0

    goto/16 :goto_5

    :cond_3
    if-eqz v1, :cond_6

    .line 12
    move-object v0, v12

    check-cast v0, Lcom/intsig/office/wp/model/RowElement;

    invoke-direct {v15, v1, v0, v8}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutMergedCell(Lcom/intsig/office/wp/view/RowView;Lcom/intsig/office/wp/model/RowElement;Z)V

    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    move-result v2

    invoke-virtual {v1, v11}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v3

    add-int/2addr v2, v3

    sub-int v3, p13, v2

    if-gtz v3, :cond_5

    .line 14
    invoke-direct {v15, v1}, Lcom/intsig/office/wp/view/TableLayoutKit;->isBreakPages(Lcom/intsig/office/wp/view/RowView;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 15
    iget-short v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    sub-int/2addr v0, v11

    int-to-short v0, v0

    iput-short v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/wp/model/RowElement;

    iput-object v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    goto :goto_2

    .line 17
    :cond_4
    iput-object v0, v15, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    :goto_2
    move v3, v2

    move v11, v4

    move v8, v5

    move-wide/from16 p14, v6

    move-object/from16 v22, v13

    move-object v6, v14

    move-object v5, v15

    const/4 v2, 0x1

    const/4 v9, 0x0

    goto/16 :goto_6

    :cond_5
    move v9, v2

    move/from16 v21, v9

    move/from16 v20, v3

    goto :goto_3

    :cond_6
    move/from16 v20, v0

    move v9, v2

    move/from16 v21, v3

    :goto_3
    const/16 v0, 0xa

    move-object/from16 v4, p1

    const/4 v3, 0x0

    .line 18
    invoke-static {v4, v12, v3, v0}, Lcom/intsig/office/wp/view/ViewFactory;->createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/intsig/office/wp/view/RowView;

    .line 19
    invoke-virtual {v14, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->appendChlidView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 20
    invoke-virtual {v2, v6, v7}, Lcom/intsig/office/simpletext/view/AbstractView;->setStartOffset(J)V

    .line 21
    invoke-virtual {v2, v8, v9}, Lcom/intsig/office/simpletext/view/AbstractView;->setLocation(II)V

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 p8, v2

    move-object/from16 v2, p2

    move-object/from16 v23, v3

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v24, v5

    move-object/from16 v5, p5

    move-wide/from16 p14, v6

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move/from16 v23, v9

    move-wide/from16 v8, p14

    move/from16 v25, v10

    move/from16 v10, v22

    move/from16 v11, v23

    move-object/from16 v26, v12

    move/from16 v12, p12

    move-object/from16 v22, v13

    move/from16 v13, v20

    move/from16 v14, v25

    move/from16 v15, v19

    .line 22
    invoke-virtual/range {v0 .. v15}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutRow(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/RowView;JIIIIIZ)I

    move-result v4

    move-object/from16 v0, p8

    const/4 v2, 0x1

    .line 23
    invoke-virtual {v0, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v1

    if-eqz v1, :cond_7

    sub-int v3, v20, v1

    if-gez v3, :cond_9

    :cond_7
    if-nez v18, :cond_9

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getPreView()Lcom/intsig/office/simpletext/view/IView;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/wp/view/RowView;

    move-object/from16 v5, p0

    if-eqz v1, :cond_8

    .line 25
    invoke-direct {v5, v1}, Lcom/intsig/office/wp/view/TableLayoutKit;->isBreakPages(Lcom/intsig/office/wp/view/RowView;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 26
    iget-short v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    sub-int/2addr v3, v2

    int-to-short v3, v3

    iput-short v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->rowIndex:S

    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/wp/model/RowElement;

    iput-object v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    move-object/from16 v12, v26

    .line 28
    invoke-direct {v5, v12}, Lcom/intsig/office/wp/view/TableLayoutKit;->clearCurrentRowBreakPageCell(Lcom/intsig/office/simpletext/model/IElement;)V

    goto :goto_4

    :cond_8
    move-object/from16 v12, v26

    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/wp/model/RowElement;

    iput-object v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    .line 30
    invoke-direct {v5, v12}, Lcom/intsig/office/wp/view/TableLayoutKit;->clearCurrentRowBreakPageCell(Lcom/intsig/office/simpletext/model/IElement;)V

    :goto_4
    move-object/from16 v6, p7

    .line 31
    invoke-virtual {v6, v0, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->deleteView(Lcom/intsig/office/simpletext/view/IView;Z)V

    move/from16 v3, v21

    move/from16 v8, v24

    const/4 v9, 0x0

    const/4 v11, 0x1

    goto :goto_6

    :cond_9
    move-object/from16 v5, p0

    move-object/from16 v6, p7

    if-ne v4, v2, :cond_a

    .line 32
    iget-object v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->breakPagesCell:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-gtz v3, :cond_b

    :cond_a
    iget-boolean v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->isRowBreakPages:Z

    if-eqz v3, :cond_c

    .line 33
    :cond_b
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/wp/model/RowElement;

    iput-object v3, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowElement:Lcom/intsig/office/wp/model/RowElement;

    :cond_c
    const/4 v3, 0x0

    .line 34
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    move-result v7

    move/from16 v8, v24

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    add-int v8, v23, v1

    add-int v1, v21, v1

    const/4 v9, 0x0

    .line 35
    invoke-virtual {v0, v9}, Lcom/intsig/office/simpletext/view/AbstractView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    move-result-wide v10

    move v3, v1

    move-object v15, v5

    move-object v14, v6

    move v5, v7

    move v2, v8

    move-wide v6, v10

    move-object/from16 v13, v22

    move/from16 v10, v25

    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x2

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v1, v0

    move/from16 v0, v20

    goto/16 :goto_0

    :cond_d
    move v8, v5

    move-wide/from16 p14, v6

    move-object/from16 v22, v13

    move-object v6, v14

    move-object v5, v15

    const/4 v2, 0x1

    :goto_5
    move v11, v4

    .line 36
    :goto_6
    invoke-direct {v5, v1, v9, v2}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutMergedCell(Lcom/intsig/office/wp/view/RowView;Lcom/intsig/office/wp/model/RowElement;Z)V

    move-wide/from16 v9, p14

    .line 37
    invoke-virtual {v6, v9, v10}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 38
    invoke-virtual {v6, v8, v3}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    move-object/from16 v0, p4

    .line 39
    iget-byte v0, v0, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    if-nez v0, :cond_11

    .line 40
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v0

    invoke-virtual/range {v22 .. v22}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v0

    int-to-byte v0, v0

    sub-int v3, p12, v8

    if-eq v0, v2, :cond_f

    const/4 v4, 0x2

    if-ne v0, v4, :cond_e

    goto :goto_7

    .line 41
    :cond_e
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    move-result v0

    iget-object v2, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->tableAttr:Lcom/intsig/office/simpletext/view/TableAttr;

    iget v2, v2, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    sub-int/2addr v0, v2

    .line 42
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3d888889

    mul-float v2, v2, v3

    float-to-int v2, v2

    add-int/2addr v0, v2

    .line 43
    invoke-virtual {v6, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    goto :goto_8

    :cond_f
    :goto_7
    if-ne v0, v2, :cond_10

    .line 44
    div-int/lit8 v3, v3, 0x2

    .line 45
    :cond_10
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {v6, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 46
    :cond_11
    :goto_8
    iput-object v1, v5, Lcom/intsig/office/wp/view/TableLayoutKit;->breakRowView:Lcom/intsig/office/wp/view/RowView;

    return v11
.end method

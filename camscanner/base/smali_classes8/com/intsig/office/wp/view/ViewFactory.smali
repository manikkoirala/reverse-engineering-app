.class public Lcom/intsig/office/wp/view/ViewFactory;
.super Ljava/lang/Object;
.source "ViewFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    const/4 v1, 0x0

    .line 3
    packed-switch p3, :pswitch_data_0

    .line 4
    .line 5
    .line 6
    :pswitch_0
    goto/16 :goto_2

    .line 7
    .line 8
    :pswitch_1
    new-instance v1, Lcom/intsig/office/wp/view/BNView;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/office/wp/view/BNView;-><init>()V

    .line 11
    .line 12
    .line 13
    goto/16 :goto_2

    .line 14
    .line 15
    :pswitch_2
    new-instance v1, Lcom/intsig/office/wp/view/TitleView;

    .line 16
    .line 17
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/TitleView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 18
    .line 19
    .line 20
    goto/16 :goto_2

    .line 21
    .line 22
    :pswitch_3
    new-instance v1, Lcom/intsig/office/wp/view/CellView;

    .line 23
    .line 24
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/CellView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 25
    .line 26
    .line 27
    goto/16 :goto_2

    .line 28
    .line 29
    :pswitch_4
    new-instance v1, Lcom/intsig/office/wp/view/RowView;

    .line 30
    .line 31
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/RowView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 32
    .line 33
    .line 34
    goto/16 :goto_2

    .line 35
    .line 36
    :pswitch_5
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getType()S

    .line 37
    .line 38
    .line 39
    move-result p0

    .line 40
    if-ne p0, v0, :cond_0

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/office/wp/view/TableView;

    .line 43
    .line 44
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/TableView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 45
    .line 46
    .line 47
    goto/16 :goto_2

    .line 48
    .line 49
    :cond_0
    new-instance v1, Lcom/intsig/office/wp/view/ParagraphView;

    .line 50
    .line 51
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/ParagraphView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 52
    .line 53
    .line 54
    goto/16 :goto_2

    .line 55
    .line 56
    :pswitch_6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 57
    .line 58
    .line 59
    move-result-object p3

    .line 60
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    const/16 v3, 0xd

    .line 65
    .line 66
    invoke-virtual {p3, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 67
    .line 68
    .line 69
    move-result p3

    .line 70
    if-eqz p3, :cond_4

    .line 71
    .line 72
    invoke-interface {p0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 73
    .line 74
    .line 75
    move-result-object p0

    .line 76
    invoke-virtual {p0}, Lcom/intsig/office/system/SysKit;->getWPShapeManage()Lcom/intsig/office/wp/control/WPShapeManage;

    .line 77
    .line 78
    .line 79
    move-result-object p0

    .line 80
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 81
    .line 82
    .line 83
    move-result-object p3

    .line 84
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    invoke-virtual {p3, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->getShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 89
    .line 90
    .line 91
    move-result p3

    .line 92
    invoke-virtual {p0, p3}, Lcom/intsig/office/wp/control/WPShapeManage;->getShape(I)Lcom/intsig/office/common/shape/AbstractShape;

    .line 93
    .line 94
    .line 95
    move-result-object p0

    .line 96
    if-eqz p0, :cond_3

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    .line 99
    .line 100
    .line 101
    move-result p3

    .line 102
    if-eq p3, v0, :cond_2

    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    .line 105
    .line 106
    .line 107
    move-result p3

    .line 108
    const/4 v0, 0x5

    .line 109
    if-ne p3, v0, :cond_1

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/common/shape/AbstractShape;->getType()S

    .line 113
    .line 114
    .line 115
    move-result p3

    .line 116
    if-nez p3, :cond_6

    .line 117
    .line 118
    new-instance p3, Lcom/intsig/office/wp/view/ObjView;

    .line 119
    .line 120
    check-cast p0, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 121
    .line 122
    invoke-direct {p3, p2, p1, p0}, Lcom/intsig/office/wp/view/ObjView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_2
    :goto_0
    new-instance p3, Lcom/intsig/office/wp/view/ShapeView;

    .line 127
    .line 128
    check-cast p0, Lcom/intsig/office/common/shape/AutoShape;

    .line 129
    .line 130
    invoke-direct {p3, p2, p1, p0}, Lcom/intsig/office/wp/view/ShapeView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/common/shape/AutoShape;)V

    .line 131
    .line 132
    .line 133
    :goto_1
    move-object v1, p3

    .line 134
    goto :goto_2

    .line 135
    :cond_3
    new-instance p0, Lcom/intsig/office/wp/view/ObjView;

    .line 136
    .line 137
    invoke-direct {p0, p2, p1, v1}, Lcom/intsig/office/wp/view/ObjView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/common/shape/WPAutoShape;)V

    .line 138
    .line 139
    .line 140
    move-object v1, p0

    .line 141
    goto :goto_2

    .line 142
    :cond_4
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 143
    .line 144
    .line 145
    move-result-object p0

    .line 146
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 147
    .line 148
    .line 149
    move-result-object p3

    .line 150
    const/16 v0, 0x10

    .line 151
    .line 152
    invoke-virtual {p0, p3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 153
    .line 154
    .line 155
    move-result p0

    .line 156
    if-eqz p0, :cond_5

    .line 157
    .line 158
    new-instance v1, Lcom/intsig/office/wp/view/EncloseCharacterView;

    .line 159
    .line 160
    invoke-direct {v1, p2, p1}, Lcom/intsig/office/wp/view/EncloseCharacterView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    .line 161
    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_5
    new-instance v1, Lcom/intsig/office/wp/view/LeafView;

    .line 165
    .line 166
    invoke-direct {v1, p2, p1}, Lcom/intsig/office/wp/view/LeafView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    .line 167
    .line 168
    .line 169
    goto :goto_2

    .line 170
    :pswitch_7
    new-instance v1, Lcom/intsig/office/wp/view/LineView;

    .line 171
    .line 172
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/LineView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 173
    .line 174
    .line 175
    goto :goto_2

    .line 176
    :pswitch_8
    new-instance v1, Lcom/intsig/office/wp/view/ParagraphView;

    .line 177
    .line 178
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/ParagraphView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 179
    .line 180
    .line 181
    goto :goto_2

    .line 182
    :pswitch_9
    new-instance v1, Lcom/intsig/office/wp/view/PageView;

    .line 183
    .line 184
    invoke-direct {v1, p1}, Lcom/intsig/office/wp/view/PageView;-><init>(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 185
    .line 186
    .line 187
    :cond_6
    :goto_2
    return-object v1

    .line 188
    nop

    .line 189
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

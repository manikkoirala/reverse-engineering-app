.class public Lcom/intsig/office/wp/view/ShapeView;
.super Lcom/intsig/office/wp/view/LeafView;
.source "ShapeView.java"


# static fields
.field private static final GAP:I = 0x64


# instance fields
.field private isInline:Z

.field private pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

.field private rect:Landroid/graphics/Rect;

.field private roots:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/wp/view/WPSTRoot;",
            ">;"
        }
    .end annotation
.end field

.field private wpShape:Lcom/intsig/office/common/shape/WPAutoShape;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/wp/view/LeafView;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/common/shape/AutoShape;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/wp/view/LeafView;-><init>(Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;)V

    .line 4
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 5
    check-cast p3, Lcom/intsig/office/common/shape/WPAutoShape;

    iput-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 6
    new-instance p1, Ljava/util/Hashtable;

    invoke-direct {p1}, Ljava/util/Hashtable;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    return-void
.end method

.method private drawGroupShape(Landroid/graphics/Canvas;Lcom/intsig/office/common/shape/GroupShape;Landroid/graphics/Rect;F)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v12, p1

    .line 4
    .line 5
    move-object/from16 v13, p3

    .line 6
    .line 7
    move/from16 v14, p4

    .line 8
    .line 9
    if-eqz p2, :cond_6

    .line 10
    .line 11
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 12
    .line 13
    .line 14
    move-result-object v15

    .line 15
    if-eqz v15, :cond_6

    .line 16
    .line 17
    new-instance v11, Landroid/graphics/Rect;

    .line 18
    .line 19
    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 20
    .line 21
    .line 22
    array-length v10, v15

    .line 23
    const/4 v1, 0x0

    .line 24
    const/4 v9, 0x0

    .line 25
    :goto_0
    if-ge v9, v10, :cond_6

    .line 26
    .line 27
    aget-object v8, v15, v9

    .line 28
    .line 29
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    const/4 v2, 0x7

    .line 34
    if-ne v1, v2, :cond_1

    .line 35
    .line 36
    check-cast v8, Lcom/intsig/office/common/shape/GroupShape;

    .line 37
    .line 38
    invoke-direct {v0, v12, v8, v13, v14}, Lcom/intsig/office/wp/view/ShapeView;->drawGroupShape(Landroid/graphics/Canvas;Lcom/intsig/office/common/shape/GroupShape;Landroid/graphics/Rect;F)V

    .line 39
    .line 40
    .line 41
    :cond_0
    move/from16 v19, v9

    .line 42
    .line 43
    move/from16 v16, v10

    .line 44
    .line 45
    move-object v9, v11

    .line 46
    goto/16 :goto_2

    .line 47
    .line 48
    :cond_1
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-nez v1, :cond_3

    .line 53
    .line 54
    invoke-virtual {v11}, Landroid/graphics/Rect;->setEmpty()V

    .line 55
    .line 56
    .line 57
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    iget v2, v13, Landroid/graphics/Rect;->left:I

    .line 62
    .line 63
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 64
    .line 65
    int-to-float v3, v3

    .line 66
    mul-float v3, v3, v14

    .line 67
    .line 68
    float-to-int v3, v3

    .line 69
    add-int/2addr v2, v3

    .line 70
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 71
    .line 72
    iget v3, v13, Landroid/graphics/Rect;->top:I

    .line 73
    .line 74
    iget v4, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 75
    .line 76
    int-to-float v4, v4

    .line 77
    mul-float v4, v4, v14

    .line 78
    .line 79
    float-to-int v4, v4

    .line 80
    add-int/2addr v3, v4

    .line 81
    iput v3, v11, Landroid/graphics/Rect;->top:I

    .line 82
    .line 83
    int-to-float v2, v2

    .line 84
    iget v4, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 85
    .line 86
    int-to-float v4, v4

    .line 87
    mul-float v4, v4, v14

    .line 88
    .line 89
    add-float/2addr v2, v4

    .line 90
    float-to-int v2, v2

    .line 91
    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 92
    .line 93
    int-to-float v2, v3

    .line 94
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 95
    .line 96
    int-to-float v1, v1

    .line 97
    mul-float v1, v1, v14

    .line 98
    .line 99
    add-float/2addr v2, v1

    .line 100
    float-to-int v1, v2

    .line 101
    iput v1, v11, Landroid/graphics/Rect;->bottom:I

    .line 102
    .line 103
    instance-of v1, v8, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 104
    .line 105
    if-eqz v1, :cond_2

    .line 106
    .line 107
    check-cast v8, Lcom/intsig/office/common/shape/WPPictureShape;

    .line 108
    .line 109
    invoke-virtual {v8}, Lcom/intsig/office/common/shape/WPPictureShape;->getPictureShape()Lcom/intsig/office/common/shape/PictureShape;

    .line 110
    .line 111
    .line 112
    move-result-object v8

    .line 113
    :cond_2
    if-eqz v8, :cond_0

    .line 114
    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    move-object v7, v8

    .line 124
    check-cast v7, Lcom/intsig/office/common/shape/PictureShape;

    .line 125
    .line 126
    move-object/from16 v1, p1

    .line 127
    .line 128
    move-object v4, v7

    .line 129
    move-object/from16 v5, p3

    .line 130
    .line 131
    move/from16 v6, p4

    .line 132
    .line 133
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/common/BackgroundDrawer;->drawLineAndFill(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AbstractShape;Landroid/graphics/Rect;F)V

    .line 134
    .line 135
    .line 136
    invoke-static {}, Lcom/intsig/office/common/picture/PictureKit;->instance()Lcom/intsig/office/common/picture/PictureKit;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 145
    .line 146
    .line 147
    move-result v4

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    invoke-virtual {v7, v2}, Lcom/intsig/office/common/shape/PictureShape;->getPicture(Lcom/intsig/office/system/IControl;)Lcom/intsig/office/common/picture/Picture;

    .line 153
    .line 154
    .line 155
    move-result-object v5

    .line 156
    iget v2, v11, Landroid/graphics/Rect;->left:I

    .line 157
    .line 158
    int-to-float v6, v2

    .line 159
    iget v2, v11, Landroid/graphics/Rect;->top:I

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    move/from16 p2, v2

    .line 163
    .line 164
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 165
    .line 166
    .line 167
    move-result-object v2

    .line 168
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 169
    .line 170
    int-to-float v2, v2

    .line 171
    mul-float v16, v2, v14

    .line 172
    .line 173
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 174
    .line 175
    .line 176
    move-result-object v2

    .line 177
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 178
    .line 179
    int-to-float v2, v2

    .line 180
    mul-float v17, v2, v14

    .line 181
    .line 182
    invoke-virtual {v7}, Lcom/intsig/office/common/shape/PictureShape;->getPictureEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 183
    .line 184
    .line 185
    move-result-object v18

    .line 186
    move/from16 v7, p2

    .line 187
    .line 188
    move-object/from16 v2, p1

    .line 189
    .line 190
    move/from16 v8, p4

    .line 191
    .line 192
    move/from16 v19, v9

    .line 193
    .line 194
    move/from16 v9, v16

    .line 195
    .line 196
    move/from16 v16, v10

    .line 197
    .line 198
    move/from16 v10, v17

    .line 199
    .line 200
    move-object/from16 p2, v11

    .line 201
    .line 202
    move-object/from16 v11, v18

    .line 203
    .line 204
    invoke-virtual/range {v1 .. v11}, Lcom/intsig/office/common/picture/PictureKit;->drawPicture(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/picture/Picture;FFFFFLcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 205
    .line 206
    .line 207
    goto/16 :goto_1

    .line 208
    .line 209
    :cond_3
    move/from16 v19, v9

    .line 210
    .line 211
    move/from16 v16, v10

    .line 212
    .line 213
    move-object/from16 p2, v11

    .line 214
    .line 215
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 216
    .line 217
    .line 218
    move-result v1

    .line 219
    const/4 v2, 0x2

    .line 220
    if-ne v1, v2, :cond_4

    .line 221
    .line 222
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->setEmpty()V

    .line 223
    .line 224
    .line 225
    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 226
    .line 227
    .line 228
    move-result-object v1

    .line 229
    iget v2, v13, Landroid/graphics/Rect;->left:I

    .line 230
    .line 231
    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 232
    .line 233
    int-to-float v3, v3

    .line 234
    mul-float v3, v3, v14

    .line 235
    .line 236
    float-to-int v3, v3

    .line 237
    add-int/2addr v2, v3

    .line 238
    move-object/from16 v9, p2

    .line 239
    .line 240
    iput v2, v9, Landroid/graphics/Rect;->left:I

    .line 241
    .line 242
    iget v3, v13, Landroid/graphics/Rect;->top:I

    .line 243
    .line 244
    iget v4, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 245
    .line 246
    int-to-float v4, v4

    .line 247
    mul-float v4, v4, v14

    .line 248
    .line 249
    float-to-int v4, v4

    .line 250
    add-int/2addr v3, v4

    .line 251
    iput v3, v9, Landroid/graphics/Rect;->top:I

    .line 252
    .line 253
    int-to-float v2, v2

    .line 254
    iget v4, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 255
    .line 256
    int-to-float v4, v4

    .line 257
    mul-float v4, v4, v14

    .line 258
    .line 259
    add-float/2addr v2, v4

    .line 260
    float-to-int v2, v2

    .line 261
    iput v2, v9, Landroid/graphics/Rect;->right:I

    .line 262
    .line 263
    int-to-float v2, v3

    .line 264
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 265
    .line 266
    int-to-float v1, v1

    .line 267
    mul-float v1, v1, v14

    .line 268
    .line 269
    add-float/2addr v2, v1

    .line 270
    float-to-int v1, v2

    .line 271
    iput v1, v9, Landroid/graphics/Rect;->bottom:I

    .line 272
    .line 273
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->instance()Lcom/intsig/office/common/autoshape/AutoShapeKit;

    .line 274
    .line 275
    .line 276
    move-result-object v1

    .line 277
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 278
    .line 279
    .line 280
    move-result-object v3

    .line 281
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 282
    .line 283
    .line 284
    move-result v4

    .line 285
    move-object v5, v8

    .line 286
    check-cast v5, Lcom/intsig/office/common/shape/AutoShape;

    .line 287
    .line 288
    move-object/from16 v2, p1

    .line 289
    .line 290
    move-object v6, v9

    .line 291
    move/from16 v7, p4

    .line 292
    .line 293
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->drawAutoShape(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;F)V

    .line 294
    .line 295
    .line 296
    check-cast v8, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 297
    .line 298
    invoke-virtual {v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 299
    .line 300
    .line 301
    move-result v1

    .line 302
    if-ltz v1, :cond_5

    .line 303
    .line 304
    iget-object v1, v0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 305
    .line 306
    invoke-virtual {v8}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 307
    .line 308
    .line 309
    move-result v2

    .line 310
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 311
    .line 312
    .line 313
    move-result-object v2

    .line 314
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    .line 316
    .line 317
    move-result-object v1

    .line 318
    check-cast v1, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 319
    .line 320
    if-eqz v1, :cond_5

    .line 321
    .line 322
    iget v2, v9, Landroid/graphics/Rect;->left:I

    .line 323
    .line 324
    iget v3, v9, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    invoke-virtual {v1, v12, v2, v3, v14}, Lcom/intsig/office/wp/view/WPSTRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 327
    .line 328
    .line 329
    goto :goto_2

    .line 330
    :cond_4
    :goto_1
    move-object/from16 v9, p2

    .line 331
    .line 332
    :cond_5
    :goto_2
    add-int/lit8 v1, v19, 0x1

    .line 333
    .line 334
    move-object v11, v9

    .line 335
    move/from16 v10, v16

    .line 336
    .line 337
    move v9, v1

    .line 338
    goto/16 :goto_0

    .line 339
    .line 340
    :cond_6
    return-void
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private layoutTextbox(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/common/shape/WPGroupShape;)V
    .locals 4

    .line 1
    if-eqz p2, :cond_2

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_3

    .line 8
    .line 9
    array-length p2, p1

    .line 10
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-ge v0, p2, :cond_3

    .line 12
    .line 13
    aget-object v1, p1, v0

    .line 14
    .line 15
    invoke-interface {v1}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const/4 v3, 0x7

    .line 20
    if-ne v2, v3, :cond_0

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    check-cast v1, Lcom/intsig/office/common/shape/WPGroupShape;

    .line 24
    .line 25
    invoke-direct {p0, v2, v1}, Lcom/intsig/office/wp/view/ShapeView;->layoutTextbox(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/common/shape/WPGroupShape;)V

    .line 26
    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    instance-of v2, v1, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 30
    .line 31
    if-eqz v2, :cond_1

    .line 32
    .line 33
    check-cast v1, Lcom/intsig/office/common/shape/WPAutoShape;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/wp/view/ShapeView;->layoutTextbox(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/common/shape/WPGroupShape;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    if-ltz p2, :cond_3

    .line 50
    .line 51
    new-instance p2, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getContainer()Lcom/intsig/office/simpletext/control/IWord;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    invoke-direct {p2, v0, v1, v2}, Lcom/intsig/office/wp/view/WPSTRoot;-><init>(Lcom/intsig/office/simpletext/control/IWord;Lcom/intsig/office/simpletext/model/IDocument;I)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->isTextWrapLine()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    invoke-virtual {p2, v0}, Lcom/intsig/office/wp/view/WPSTRoot;->setWrapLine(Z)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/WPSTRoot;->doLayout()V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p2, p0}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAbstractShape;->isTextWrapLine()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_3

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/WPSTRoot;->getAdjustTextboxWidth()I

    .line 105
    .line 106
    .line 107
    move-result p2

    .line 108
    iput p2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 109
    .line 110
    :cond_3
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/intsig/office/wp/view/LeafView;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    check-cast v2, Ljava/lang/Integer;

    .line 28
    .line 29
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 30
    .line 31
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    check-cast v2, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 36
    .line 37
    if-eqz v2, :cond_0

    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/wp/view/WPSTRoot;->dispose()V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 44
    .line 45
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 46
    .line 47
    .line 48
    iput-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 49
    .line 50
    :cond_2
    iput-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public doLayout(Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;IIIIJI)I
    .locals 15

    .line 1
    move-object v0, p0

    .line 2
    move-object/from16 v1, p2

    .line 3
    .line 4
    iput-object v1, v0, Lcom/intsig/office/wp/view/ShapeView;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 5
    .line 6
    move-object/from16 v2, p1

    .line 7
    .line 8
    iget-byte v2, v2, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    const/4 v4, 0x1

    .line 12
    if-eq v2, v4, :cond_1

    .line 13
    .line 14
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 15
    .line 16
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getWrap()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    const/4 v5, 0x3

    .line 21
    if-eq v2, v5, :cond_0

    .line 22
    .line 23
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getWrap()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    const/4 v5, 0x6

    .line 30
    if-eq v2, v5, :cond_0

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v2, 0x0

    .line 34
    goto :goto_1

    .line 35
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 36
    :goto_1
    iput-boolean v2, v0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 37
    .line 38
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAutoShape;->isWatermarkShape()Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    const-wide/16 v5, 0x1

    .line 45
    .line 46
    if-eqz v2, :cond_2

    .line 47
    .line 48
    iput-boolean v3, v0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    invoke-static {}, Lcom/intsig/office/wp/view/WPViewKit;->instance()Lcom/intsig/office/wp/view/WPViewKit;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    iget-wide v7, v0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 56
    .line 57
    add-long/2addr v7, v5

    .line 58
    invoke-virtual {v2, v7, v8}, Lcom/intsig/office/wp/view/WPViewKit;->getArea(J)J

    .line 59
    .line 60
    .line 61
    move-result-wide v7

    .line 62
    const-wide/high16 v9, 0x1000000000000000L

    .line 63
    .line 64
    cmp-long v2, v7, v9

    .line 65
    .line 66
    if-eqz v2, :cond_3

    .line 67
    .line 68
    invoke-static {}, Lcom/intsig/office/wp/view/WPViewKit;->instance()Lcom/intsig/office/wp/view/WPViewKit;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    iget-wide v7, v0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 73
    .line 74
    add-long/2addr v7, v5

    .line 75
    invoke-virtual {v2, v7, v8}, Lcom/intsig/office/wp/view/WPViewKit;->getArea(J)J

    .line 76
    .line 77
    .line 78
    move-result-wide v7

    .line 79
    const-wide/high16 v9, 0x2000000000000000L

    .line 80
    .line 81
    cmp-long v2, v7, v9

    .line 82
    .line 83
    if-nez v2, :cond_4

    .line 84
    .line 85
    :cond_3
    iput-boolean v4, v0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 86
    .line 87
    :cond_4
    :goto_2
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    iget-boolean v7, v0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 94
    .line 95
    if-eqz v7, :cond_5

    .line 96
    .line 97
    iget v1, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 98
    .line 99
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 100
    .line 101
    invoke-virtual {p0, v1, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    .line 102
    .line 103
    .line 104
    goto/16 :goto_8

    .line 105
    .line 106
    :cond_5
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 107
    .line 108
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAutoShape;->isWatermarkShape()Z

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    if-eqz v2, :cond_a

    .line 113
    .line 114
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 115
    .line 116
    check-cast v2, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 117
    .line 118
    new-instance v7, Landroid/graphics/Paint;

    .line 119
    .line 120
    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 121
    .line 122
    .line 123
    iput-object v7, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 124
    .line 125
    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->getWatermartString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v7

    .line 132
    if-eqz v7, :cond_b

    .line 133
    .line 134
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 135
    .line 136
    .line 137
    move-result v8

    .line 138
    if-lez v8, :cond_b

    .line 139
    .line 140
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 141
    .line 142
    .line 143
    move-result v8

    .line 144
    iget v9, v1, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 145
    .line 146
    iget v10, v1, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 147
    .line 148
    sub-int/2addr v9, v10

    .line 149
    iget v10, v1, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 150
    .line 151
    sub-int/2addr v9, v10

    .line 152
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->isAutoFontSize()Z

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    if-eqz v10, :cond_9

    .line 157
    .line 158
    div-int v10, v9, v8

    .line 159
    .line 160
    iget-object v11, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 161
    .line 162
    int-to-float v12, v10

    .line 163
    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 164
    .line 165
    .line 166
    iget-object v11, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 167
    .line 168
    iget-object v12, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 169
    .line 170
    invoke-virtual {v11, v7, v3, v8, v12}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 171
    .line 172
    .line 173
    iget-object v11, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 174
    .line 175
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    .line 176
    .line 177
    .line 178
    move-result v11

    .line 179
    if-ge v11, v9, :cond_7

    .line 180
    .line 181
    move v11, v10

    .line 182
    :goto_3
    iget-object v12, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 183
    .line 184
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    .line 185
    .line 186
    .line 187
    move-result v12

    .line 188
    if-ge v12, v9, :cond_6

    .line 189
    .line 190
    add-int/lit8 v11, v10, 0x1

    .line 191
    .line 192
    iget-object v12, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 193
    .line 194
    int-to-float v13, v11

    .line 195
    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 196
    .line 197
    .line 198
    iget-object v12, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 199
    .line 200
    iget-object v13, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 201
    .line 202
    invoke-virtual {v12, v7, v3, v8, v13}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 203
    .line 204
    .line 205
    move v14, v11

    .line 206
    move v11, v10

    .line 207
    move v10, v14

    .line 208
    goto :goto_3

    .line 209
    :cond_6
    move v10, v11

    .line 210
    goto :goto_5

    .line 211
    :cond_7
    iget-object v11, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 212
    .line 213
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    .line 214
    .line 215
    .line 216
    move-result v11

    .line 217
    if-le v11, v9, :cond_8

    .line 218
    .line 219
    move v11, v10

    .line 220
    :goto_4
    iget-object v12, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 221
    .line 222
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    .line 223
    .line 224
    .line 225
    move-result v12

    .line 226
    if-le v12, v9, :cond_8

    .line 227
    .line 228
    add-int/lit8 v10, v11, -0x1

    .line 229
    .line 230
    iget-object v12, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 231
    .line 232
    int-to-float v13, v10

    .line 233
    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 234
    .line 235
    .line 236
    iget-object v12, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 237
    .line 238
    iget-object v13, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 239
    .line 240
    invoke-virtual {v12, v7, v3, v8, v13}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 241
    .line 242
    .line 243
    move v14, v11

    .line 244
    move v11, v10

    .line 245
    move v10, v14

    .line 246
    goto :goto_4

    .line 247
    :cond_8
    :goto_5
    invoke-virtual {v2, v10}, Lcom/intsig/office/common/shape/WatermarkShape;->setFontSize(I)V

    .line 248
    .line 249
    .line 250
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 251
    .line 252
    int-to-float v10, v10

    .line 253
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 254
    .line 255
    .line 256
    goto :goto_6

    .line 257
    :cond_9
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 258
    .line 259
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->getFontSize()I

    .line 260
    .line 261
    .line 262
    move-result v10

    .line 263
    int-to-float v10, v10

    .line 264
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 265
    .line 266
    .line 267
    :goto_6
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 268
    .line 269
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->getFontColor()I

    .line 270
    .line 271
    .line 272
    move-result v10

    .line 273
    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 274
    .line 275
    .line 276
    const/high16 v9, 0x437f0000    # 255.0f

    .line 277
    .line 278
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->getOpacity()F

    .line 279
    .line 280
    .line 281
    move-result v2

    .line 282
    mul-float v2, v2, v9

    .line 283
    .line 284
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 285
    .line 286
    .line 287
    move-result v2

    .line 288
    iget-object v9, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 289
    .line 290
    invoke-virtual {v9, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 291
    .line 292
    .line 293
    iget-object v2, v0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 294
    .line 295
    iget-object v9, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 296
    .line 297
    invoke-virtual {v2, v7, v3, v8, v9}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 298
    .line 299
    .line 300
    iget v2, v1, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 301
    .line 302
    iget-object v7, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 303
    .line 304
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    .line 305
    .line 306
    .line 307
    move-result v7

    .line 308
    sub-int/2addr v2, v7

    .line 309
    div-int/lit8 v2, v2, 0x2

    .line 310
    .line 311
    invoke-virtual {p0, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 312
    .line 313
    .line 314
    iget v1, v1, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 315
    .line 316
    iget-object v2, v0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 317
    .line 318
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 319
    .line 320
    .line 321
    move-result v2

    .line 322
    sub-int/2addr v1, v2

    .line 323
    div-int/lit8 v1, v1, 0x2

    .line 324
    .line 325
    invoke-virtual {p0, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 326
    .line 327
    .line 328
    goto :goto_7

    .line 329
    :cond_a
    invoke-static {}, Lcom/intsig/office/wp/view/PositionLayoutKit;->instance()Lcom/intsig/office/wp/view/PositionLayoutKit;

    .line 330
    .line 331
    .line 332
    move-result-object v2

    .line 333
    iget-object v7, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 334
    .line 335
    invoke-virtual {v2, p0, v7, v1}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processShapePosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 336
    .line 337
    .line 338
    :cond_b
    :goto_7
    const/4 v1, 0x0

    .line 339
    :goto_8
    iget-wide v7, v0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 340
    .line 341
    add-long/2addr v7, v5

    .line 342
    invoke-virtual {p0, v7, v8}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 343
    .line 344
    .line 345
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    .line 346
    .line 347
    .line 348
    move-result-object v2

    .line 349
    move/from16 v5, p10

    .line 350
    .line 351
    invoke-virtual {v2, v5, v3}, Lcom/intsig/office/simpletext/view/ViewKit;->getBitValue(II)Z

    .line 352
    .line 353
    .line 354
    move-result v2

    .line 355
    if-nez v2, :cond_c

    .line 356
    .line 357
    move/from16 v2, p6

    .line 358
    .line 359
    if-le v1, v2, :cond_c

    .line 360
    .line 361
    const/4 v3, 0x1

    .line 362
    goto :goto_9

    .line 363
    :cond_c
    iget-object v1, v0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 364
    .line 365
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 366
    .line 367
    .line 368
    move-result-object v2

    .line 369
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/wp/view/ShapeView;->layoutTextbox(Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/common/shape/WPGroupShape;)V

    .line 370
    .line 371
    .line 372
    :goto_9
    return v3
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public declared-synchronized draw(Landroid/graphics/Canvas;IIF)V
    .locals 9

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 3
    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 7
    .line 8
    int-to-float v0, v0

    .line 9
    mul-float v0, v0, p4

    .line 10
    .line 11
    float-to-int v0, v0

    .line 12
    add-int/2addr v0, p2

    .line 13
    iget p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 14
    .line 15
    int-to-float p2, p2

    .line 16
    mul-float p2, p2, p4

    .line 17
    .line 18
    float-to-int p2, p2

    .line 19
    add-int/2addr p2, p3

    .line 20
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 21
    .line 22
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 23
    .line 24
    .line 25
    move-result-object p3

    .line 26
    iget-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 27
    .line 28
    int-to-float v2, v0

    .line 29
    iget v3, p3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    mul-float v3, v3, p4

    .line 33
    .line 34
    add-float/2addr v2, v3

    .line 35
    float-to-int v2, v2

    .line 36
    int-to-float v3, p2

    .line 37
    iget p3, p3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 38
    .line 39
    int-to-float p3, p3

    .line 40
    mul-float p3, p3, p4

    .line 41
    .line 42
    add-float/2addr v3, p3

    .line 43
    float-to-int p3, v3

    .line 44
    invoke-virtual {v1, v0, p2, v2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 45
    .line 46
    .line 47
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 48
    .line 49
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 50
    .line 51
    .line 52
    move-result-object p3

    .line 53
    if-eqz p3, :cond_0

    .line 54
    .line 55
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 56
    .line 57
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 58
    .line 59
    .line 60
    move-result-object p3

    .line 61
    iget-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 62
    .line 63
    invoke-direct {p0, p1, p3, v1, p4}, Lcom/intsig/office/wp/view/ShapeView;->drawGroupShape(Landroid/graphics/Canvas;Lcom/intsig/office/common/shape/GroupShape;Landroid/graphics/Rect;F)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 68
    .line 69
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAutoShape;->getType()S

    .line 70
    .line 71
    .line 72
    move-result p3

    .line 73
    const/4 v1, 0x2

    .line 74
    if-ne p3, v1, :cond_1

    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->instance()Lcom/intsig/office/common/autoshape/AutoShapeKit;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 85
    .line 86
    .line 87
    move-result v5

    .line 88
    iget-object v6, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 89
    .line 90
    iget-object v7, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 91
    .line 92
    move-object v3, p1

    .line 93
    move v8, p4

    .line 94
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->drawAutoShape(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;F)V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_1
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 99
    .line 100
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAutoShape;->getType()S

    .line 101
    .line 102
    .line 103
    move-result p3

    .line 104
    const/4 v1, 0x5

    .line 105
    if-ne p3, v1, :cond_2

    .line 106
    .line 107
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 108
    .line 109
    check-cast p3, Lcom/intsig/office/common/shape/WPChartShape;

    .line 110
    .line 111
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPChartShape;->getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {v1, p4}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->setZoomRate(F)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 123
    .line 124
    iget v4, p3, Landroid/graphics/Rect;->left:I

    .line 125
    .line 126
    iget v5, p3, Landroid/graphics/Rect;->top:I

    .line 127
    .line 128
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    .line 129
    .line 130
    .line 131
    move-result v6

    .line 132
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 133
    .line 134
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    .line 135
    .line 136
    .line 137
    move-result v7

    .line 138
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 139
    .line 140
    .line 141
    move-result-object p3

    .line 142
    invoke-virtual {p3}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 143
    .line 144
    .line 145
    move-result-object v8

    .line 146
    move-object v2, p1

    .line 147
    invoke-virtual/range {v1 .. v8}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->draw(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;IIIILandroid/graphics/Paint;)V

    .line 148
    .line 149
    .line 150
    :cond_2
    :goto_0
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 151
    .line 152
    invoke-interface {p3}, Ljava/util/Map;->size()I

    .line 153
    .line 154
    .line 155
    move-result p3

    .line 156
    if-lez p3, :cond_3

    .line 157
    .line 158
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 159
    .line 160
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 161
    .line 162
    .line 163
    move-result p3

    .line 164
    if-ltz p3, :cond_3

    .line 165
    .line 166
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 167
    .line 168
    iget-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 169
    .line 170
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    .line 180
    .line 181
    move-result-object p3

    .line 182
    check-cast p3, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 183
    .line 184
    if-eqz p3, :cond_3

    .line 185
    .line 186
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 187
    .line 188
    .line 189
    iget-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 190
    .line 191
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 192
    .line 193
    .line 194
    move-result v1

    .line 195
    iget-object v2, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 196
    .line 197
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    .line 198
    .line 199
    .line 200
    move-result v2

    .line 201
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 202
    .line 203
    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    .line 204
    .line 205
    .line 206
    move-result v3

    .line 207
    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {p3, p1, v0, p2, p4}, Lcom/intsig/office/wp/view/WPSTRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    .line 215
    .line 216
    :cond_3
    monitor-exit p0

    .line 217
    return-void

    .line 218
    :catchall_0
    move-exception p1

    .line 219
    monitor-exit p0

    .line 220
    throw p1
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public declared-synchronized drawForWrap(Landroid/graphics/Canvas;IIF)V
    .locals 10

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 3
    .line 4
    int-to-float v0, v0

    .line 5
    mul-float v0, v0, p4

    .line 6
    .line 7
    float-to-int v0, v0

    .line 8
    add-int/2addr v0, p2

    .line 9
    iget v1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 10
    .line 11
    int-to-float v1, v1

    .line 12
    mul-float v1, v1, p4

    .line 13
    .line 14
    float-to-int v1, v1

    .line 15
    add-int/2addr v1, p3

    .line 16
    iget-object v2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WPAutoShape;->isWatermarkShape()Z

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    if-eqz v3, :cond_0

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 31
    .line 32
    check-cast v2, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/WatermarkShape;->getWatermartString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    if-eqz v2, :cond_3

    .line 39
    .line 40
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-lez v3, :cond_3

    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 50
    .line 51
    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    iget-object v1, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 56
    .line 57
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 58
    .line 59
    check-cast v3, Lcom/intsig/office/common/shape/WatermarkShape;

    .line 60
    .line 61
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/WatermarkShape;->getFontSize()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    int-to-float v3, v3

    .line 66
    mul-float v3, v3, p4

    .line 67
    .line 68
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 69
    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 72
    .line 73
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 78
    .line 79
    iget v4, v3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 80
    .line 81
    iget v5, v3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 82
    .line 83
    sub-int/2addr v4, v5

    .line 84
    iget v6, v3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 85
    .line 86
    sub-int/2addr v4, v6

    .line 87
    iget v6, v3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 88
    .line 89
    iget v7, v3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 90
    .line 91
    sub-int/2addr v6, v7

    .line 92
    iget v3, v3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 93
    .line 94
    sub-int/2addr v6, v3

    .line 95
    int-to-float p2, p2

    .line 96
    int-to-float v3, v5

    .line 97
    int-to-float v4, v4

    .line 98
    const/high16 v5, 0x40000000    # 2.0f

    .line 99
    .line 100
    div-float/2addr v4, v5

    .line 101
    add-float/2addr v3, v4

    .line 102
    mul-float v3, v3, p4

    .line 103
    .line 104
    add-float/2addr p2, v3

    .line 105
    int-to-float p3, p3

    .line 106
    int-to-float v3, v7

    .line 107
    int-to-float v4, v6

    .line 108
    div-float/2addr v4, v5

    .line 109
    add-float/2addr v3, v4

    .line 110
    mul-float v3, v3, p4

    .line 111
    .line 112
    add-float/2addr p3, v3

    .line 113
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 114
    .line 115
    .line 116
    const/4 p2, 0x0

    .line 117
    invoke-virtual {p1, v1, p2, p2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 118
    .line 119
    .line 120
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 121
    .line 122
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    .line 123
    .line 124
    .line 125
    move-result p3

    .line 126
    neg-int p3, p3

    .line 127
    int-to-float p3, p3

    .line 128
    mul-float p3, p3, p4

    .line 129
    .line 130
    div-float/2addr p3, v5

    .line 131
    iget-object p4, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 132
    .line 133
    invoke-virtual {p1, v2, p3, p2, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 134
    .line 135
    .line 136
    iget-object p2, p0, Lcom/intsig/office/wp/view/LeafView;->paint:Landroid/graphics/Paint;

    .line 137
    .line 138
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    .line 143
    .line 144
    monitor-exit p0

    .line 145
    return-void

    .line 146
    :cond_0
    :try_start_1
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 147
    .line 148
    int-to-float p3, v0

    .line 149
    iget v3, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 150
    .line 151
    int-to-float v3, v3

    .line 152
    mul-float v3, v3, p4

    .line 153
    .line 154
    add-float/2addr p3, v3

    .line 155
    float-to-int p3, p3

    .line 156
    int-to-float v3, v1

    .line 157
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 158
    .line 159
    int-to-float v2, v2

    .line 160
    mul-float v2, v2, p4

    .line 161
    .line 162
    add-float/2addr v3, v2

    .line 163
    float-to-int v2, v3

    .line 164
    invoke-virtual {p2, v0, v1, p3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 165
    .line 166
    .line 167
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 168
    .line 169
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 170
    .line 171
    .line 172
    move-result-object p2

    .line 173
    if-eqz p2, :cond_1

    .line 174
    .line 175
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->instance()Lcom/intsig/office/common/autoshape/AutoShapeKit;

    .line 176
    .line 177
    .line 178
    move-result-object v2

    .line 179
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 180
    .line 181
    .line 182
    move-result-object v4

    .line 183
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 184
    .line 185
    .line 186
    move-result v5

    .line 187
    iget-object v6, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 188
    .line 189
    iget-object v7, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 190
    .line 191
    move-object v3, p1

    .line 192
    move v8, p4

    .line 193
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->drawAutoShape(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;F)V

    .line 194
    .line 195
    .line 196
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 197
    .line 198
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 199
    .line 200
    .line 201
    move-result-object p2

    .line 202
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 203
    .line 204
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/wp/view/ShapeView;->drawGroupShape(Landroid/graphics/Canvas;Lcom/intsig/office/common/shape/GroupShape;Landroid/graphics/Rect;F)V

    .line 205
    .line 206
    .line 207
    goto :goto_0

    .line 208
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 209
    .line 210
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getType()S

    .line 211
    .line 212
    .line 213
    move-result p2

    .line 214
    const/4 p3, 0x2

    .line 215
    if-ne p2, p3, :cond_2

    .line 216
    .line 217
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->instance()Lcom/intsig/office/common/autoshape/AutoShapeKit;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 222
    .line 223
    .line 224
    move-result-object v4

    .line 225
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/LeafView;->getPageNumber()I

    .line 226
    .line 227
    .line 228
    move-result v5

    .line 229
    iget-object v6, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 230
    .line 231
    iget-object v7, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 232
    .line 233
    move-object v3, p1

    .line 234
    move v8, p4

    .line 235
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/office/common/autoshape/AutoShapeKit;->drawAutoShape(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/shape/AutoShape;Landroid/graphics/Rect;F)V

    .line 236
    .line 237
    .line 238
    goto :goto_0

    .line 239
    :cond_2
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 240
    .line 241
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getType()S

    .line 242
    .line 243
    .line 244
    move-result p2

    .line 245
    const/4 p3, 0x5

    .line 246
    if-ne p2, p3, :cond_3

    .line 247
    .line 248
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 249
    .line 250
    check-cast p2, Lcom/intsig/office/common/shape/WPChartShape;

    .line 251
    .line 252
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPChartShape;->getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 253
    .line 254
    .line 255
    move-result-object v2

    .line 256
    invoke-virtual {v2, p4}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->setZoomRate(F)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getControl()Lcom/intsig/office/system/IControl;

    .line 260
    .line 261
    .line 262
    move-result-object v4

    .line 263
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 264
    .line 265
    iget v5, p2, Landroid/graphics/Rect;->left:I

    .line 266
    .line 267
    iget v6, p2, Landroid/graphics/Rect;->top:I

    .line 268
    .line 269
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    .line 270
    .line 271
    .line 272
    move-result v7

    .line 273
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 274
    .line 275
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    .line 276
    .line 277
    .line 278
    move-result v8

    .line 279
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 280
    .line 281
    .line 282
    move-result-object p2

    .line 283
    invoke-virtual {p2}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 284
    .line 285
    .line 286
    move-result-object v9

    .line 287
    move-object v3, p1

    .line 288
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->draw(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;IIIILandroid/graphics/Paint;)V

    .line 289
    .line 290
    .line 291
    :cond_3
    :goto_0
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 292
    .line 293
    invoke-interface {p2}, Ljava/util/Map;->size()I

    .line 294
    .line 295
    .line 296
    move-result p2

    .line 297
    if-lez p2, :cond_4

    .line 298
    .line 299
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 300
    .line 301
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 302
    .line 303
    .line 304
    move-result p2

    .line 305
    if-ltz p2, :cond_4

    .line 306
    .line 307
    iget-object p2, p0, Lcom/intsig/office/wp/view/ShapeView;->roots:Ljava/util/Map;

    .line 308
    .line 309
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 310
    .line 311
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/WPAbstractShape;->getElementIndex()I

    .line 312
    .line 313
    .line 314
    move-result p3

    .line 315
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 316
    .line 317
    .line 318
    move-result-object p3

    .line 319
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .line 321
    .line 322
    move-result-object p2

    .line 323
    check-cast p2, Lcom/intsig/office/wp/view/WPSTRoot;

    .line 324
    .line 325
    if-eqz p2, :cond_4

    .line 326
    .line 327
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 328
    .line 329
    .line 330
    iget-object p3, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 331
    .line 332
    invoke-virtual {p3}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 333
    .line 334
    .line 335
    move-result p3

    .line 336
    iget-object v2, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 337
    .line 338
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    .line 339
    .line 340
    .line 341
    move-result v2

    .line 342
    iget-object v3, p0, Lcom/intsig/office/wp/view/ShapeView;->rect:Landroid/graphics/Rect;

    .line 343
    .line 344
    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    .line 345
    .line 346
    .line 347
    move-result v3

    .line 348
    invoke-virtual {p1, p3, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 349
    .line 350
    .line 351
    invoke-virtual {p2, p1, v0, v1, p4}, Lcom/intsig/office/wp/view/WPSTRoot;->draw(Landroid/graphics/Canvas;IIF)V

    .line 352
    .line 353
    .line 354
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    .line 356
    .line 357
    goto :goto_1

    .line 358
    :catchall_0
    move-exception p1

    .line 359
    goto :goto_2

    .line 360
    :catch_0
    move-exception p1

    .line 361
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 362
    .line 363
    .line 364
    :cond_4
    :goto_1
    monitor-exit p0

    .line 365
    return-void

    .line 366
    :goto_2
    monitor-exit p0

    .line 367
    throw p1
.end method

.method public free()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBaseline()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Rectangle;->getHeight()D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    double-to-int v0, v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextWidth()F
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 12
    .line 13
    int-to-float v0, v0

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBehindDoc()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x6

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPAutoShape;->getGroupShape()Lcom/intsig/office/common/shape/WPGroupShape;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPGroupShape;->getWrapType()S

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-ne v0, v3, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v1, 0x0

    .line 26
    :goto_0
    return v1

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/wp/view/ShapeView;->wpShape:Lcom/intsig/office/common/shape/WPAutoShape;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/WPAbstractShape;->getWrap()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-ne v0, v3, :cond_2

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    const/4 v1, 0x0

    .line 37
    :goto_1
    return v1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public isInline()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/ShapeView;->isInline:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
    .locals 0

    .line 1
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    add-int/2addr p1, p2

    .line 8
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 9
    .line 10
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    add-int/2addr p1, p2

    .line 17
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 18
    .line 19
    return-object p3
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public viewToModel(IIZ)J
    .locals 0

    .line 1
    iget-wide p1, p0, Lcom/intsig/office/simpletext/view/AbstractView;->start:J

    .line 2
    .line 3
    return-wide p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

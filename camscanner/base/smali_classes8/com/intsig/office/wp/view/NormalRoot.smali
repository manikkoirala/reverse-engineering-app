.class public Lcom/intsig/office/wp/view/NormalRoot;
.super Lcom/intsig/office/simpletext/view/AbstractView;
.source "NormalRoot.java"

# interfaces
.implements Lcom/intsig/office/simpletext/view/IRoot;


# static fields
.field private static final LAYOUT_PARA:I = 0x14


# instance fields
.field private canBackLayout:Z

.field private currentLayoutOffset:J

.field private doc:Lcom/intsig/office/simpletext/model/IDocument;

.field private docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

.field private layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

.field private maxParaWidth:I

.field private pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

.field private paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

.field private prePara:Lcom/intsig/office/wp/view/ParagraphView;

.field private relayout:Z

.field private tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

.field private viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

.field private word:Lcom/intsig/office/wp/control/Word;


# direct methods
.method public constructor <init>(Lcom/intsig/office/wp/control/Word;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/view/AbstractView;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->relayout:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 14
    .line 15
    new-instance p1, Lcom/intsig/office/wp/view/LayoutThread;

    .line 16
    .line 17
    invoke-direct {p1, p0}, Lcom/intsig/office/wp/view/LayoutThread;-><init>(Lcom/intsig/office/simpletext/view/IRoot;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 21
    .line 22
    iput-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->canBackLayout:Z

    .line 23
    .line 24
    new-instance p1, Lcom/intsig/office/simpletext/view/DocAttr;

    .line 25
    .line 26
    invoke-direct {p1}, Lcom/intsig/office/simpletext/view/DocAttr;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

    .line 30
    .line 31
    iput-byte v0, p1, Lcom/intsig/office/simpletext/view/DocAttr;->rootType:B

    .line 32
    .line 33
    new-instance p1, Lcom/intsig/office/simpletext/view/PageAttr;

    .line 34
    .line 35
    invoke-direct {p1}, Lcom/intsig/office/simpletext/view/PageAttr;-><init>()V

    .line 36
    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 39
    .line 40
    new-instance p1, Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 41
    .line 42
    invoke-direct {p1}, Lcom/intsig/office/simpletext/view/ParaAttr;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 46
    .line 47
    new-instance p1, Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 48
    .line 49
    invoke-direct {p1}, Lcom/intsig/office/simpletext/view/ViewContainer;-><init>()V

    .line 50
    .line 51
    .line 52
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 53
    .line 54
    new-instance p1, Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 55
    .line 56
    invoke-direct {p1}, Lcom/intsig/office/wp/view/TableLayoutKit;-><init>()V

    .line 57
    .line 58
    .line 59
    iput-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private filteParaAttr(Lcom/intsig/office/simpletext/view/ParaAttr;)V
    .locals 2

    .line 1
    iget v0, p1, Lcom/intsig/office/simpletext/view/ParaAttr;->rightIndent:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-gez v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :cond_0
    iput v0, p1, Lcom/intsig/office/simpletext/view/ParaAttr;->rightIndent:I

    .line 8
    .line 9
    iget v0, p1, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 10
    .line 11
    if-gez v0, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    move v1, v0

    .line 15
    :goto_0
    iput v1, p1, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private layoutPara()I
    .locals 27

    .line 1
    move-object/from16 v15, p0

    .line 2
    .line 3
    const/4 v14, 0x1

    .line 4
    iput-boolean v14, v15, Lcom/intsig/office/wp/view/NormalRoot;->relayout:Z

    .line 5
    .line 6
    sget v13, Lcom/intsig/office/constant/wp/WPViewConstant;->PAGE_SPACE:I

    .line 7
    .line 8
    iget-object v0, v15, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    move v0, v13

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    add-int/2addr v0, v1

    .line 25
    :goto_0
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-interface {v1}, Lcom/intsig/office/system/IMainFrame;->isZoomAfterLayoutForWord()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 42
    .line 43
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 52
    .line 53
    int-to-float v1, v1

    .line 54
    iget-object v2, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 55
    .line 56
    invoke-virtual {v2}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    div-float/2addr v1, v2

    .line 61
    float-to-int v1, v1

    .line 62
    sget v2, Lcom/intsig/office/constant/wp/WPViewConstant;->PAGE_SPACE:I

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_1
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 66
    .line 67
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 76
    .line 77
    sget v2, Lcom/intsig/office/constant/wp/WPViewConstant;->PAGE_SPACE:I

    .line 78
    .line 79
    :goto_1
    mul-int/lit8 v2, v2, 0x2

    .line 80
    .line 81
    sub-int/2addr v1, v2

    .line 82
    move/from16 v16, v1

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/office/simpletext/view/ViewKit;->instance()Lcom/intsig/office/simpletext/view/ViewKit;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    const/4 v12, 0x0

    .line 89
    invoke-virtual {v1, v12, v12, v14}, Lcom/intsig/office/simpletext/view/ViewKit;->setBitValue(IIZ)I

    .line 90
    .line 91
    .line 92
    move-result v17

    .line 93
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 94
    .line 95
    const-wide/16 v2, 0x0

    .line 96
    .line 97
    invoke-interface {v1, v2, v3}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 98
    .line 99
    .line 100
    move-result-wide v18

    .line 101
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 102
    .line 103
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 104
    .line 105
    .line 106
    move-result-object v11

    .line 107
    const v1, 0x7fffffff

    .line 108
    .line 109
    .line 110
    move v10, v0

    .line 111
    const/4 v8, 0x0

    .line 112
    const v20, 0x7fffffff

    .line 113
    .line 114
    .line 115
    :goto_2
    const/16 v0, 0x14

    .line 116
    .line 117
    if-ge v8, v0, :cond_6

    .line 118
    .line 119
    iget-wide v0, v15, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 120
    .line 121
    cmp-long v2, v0, v18

    .line 122
    .line 123
    if-gez v2, :cond_6

    .line 124
    .line 125
    iget-boolean v2, v15, Lcom/intsig/office/wp/view/NormalRoot;->relayout:Z

    .line 126
    .line 127
    if-eqz v2, :cond_6

    .line 128
    .line 129
    invoke-interface {v11, v0, v1}, Lcom/intsig/office/simpletext/model/IDocument;->getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 138
    .line 139
    .line 140
    move-result-object v2

    .line 141
    const/16 v3, 0x100b

    .line 142
    .line 143
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    const/16 v2, 0x9

    .line 148
    .line 149
    const/4 v9, 0x0

    .line 150
    if-eqz v1, :cond_2

    .line 151
    .line 152
    move-object v0, v11

    .line 153
    check-cast v0, Lcom/intsig/office/wp/model/WPDocument;

    .line 154
    .line 155
    iget-wide v3, v15, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 156
    .line 157
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/wp/model/WPDocument;->getParagraph0(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 162
    .line 163
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    invoke-static {v1, v0, v9, v2}, Lcom/intsig/office/wp/view/ViewFactory;->createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    check-cast v1, Lcom/intsig/office/wp/view/ParagraphView;

    .line 172
    .line 173
    iget-object v3, v15, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 174
    .line 175
    if-eqz v3, :cond_3

    .line 176
    .line 177
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getElement()Lcom/intsig/office/simpletext/model/IElement;

    .line 178
    .line 179
    .line 180
    move-result-object v3

    .line 181
    if-eq v0, v3, :cond_3

    .line 182
    .line 183
    iget-object v3, v15, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 184
    .line 185
    invoke-virtual {v3}, Lcom/intsig/office/wp/view/TableLayoutKit;->clearBreakPages()V

    .line 186
    .line 187
    .line 188
    goto :goto_3

    .line 189
    :cond_2
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 190
    .line 191
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    const/4 v3, 0x5

    .line 196
    invoke-static {v1, v0, v9, v3}, Lcom/intsig/office/wp/view/ViewFactory;->createView(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IElement;Lcom/intsig/office/simpletext/model/IElement;I)Lcom/intsig/office/simpletext/view/IView;

    .line 197
    .line 198
    .line 199
    move-result-object v1

    .line 200
    check-cast v1, Lcom/intsig/office/wp/view/ParagraphView;

    .line 201
    .line 202
    :cond_3
    :goto_3
    move-object v7, v1

    .line 203
    invoke-virtual {v7, v15}, Lcom/intsig/office/simpletext/view/AbstractView;->setParentView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 204
    .line 205
    .line 206
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 207
    .line 208
    .line 209
    move-result-wide v3

    .line 210
    invoke-virtual {v7, v3, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->setStartOffset(J)V

    .line 211
    .line 212
    .line 213
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 214
    .line 215
    .line 216
    move-result-wide v3

    .line 217
    invoke-virtual {v7, v3, v4}, Lcom/intsig/office/simpletext/view/AbstractView;->setEndOffset(J)V

    .line 218
    .line 219
    .line 220
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 221
    .line 222
    if-nez v1, :cond_4

    .line 223
    .line 224
    invoke-virtual {v15, v7}, Lcom/intsig/office/simpletext/view/AbstractView;->setChildView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 225
    .line 226
    .line 227
    goto :goto_4

    .line 228
    :cond_4
    invoke-virtual {v1, v7}, Lcom/intsig/office/simpletext/view/AbstractView;->setNextView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 229
    .line 230
    .line 231
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 232
    .line 233
    invoke-virtual {v7, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setPreView(Lcom/intsig/office/simpletext/view/IView;)V

    .line 234
    .line 235
    .line 236
    :goto_4
    invoke-virtual {v7, v13, v10}, Lcom/intsig/office/simpletext/view/AbstractView;->setLocation(II)V

    .line 237
    .line 238
    .line 239
    invoke-virtual {v7}, Lcom/intsig/office/wp/view/ParagraphView;->getType()S

    .line 240
    .line 241
    .line 242
    move-result v1

    .line 243
    if-ne v1, v2, :cond_5

    .line 244
    .line 245
    iget-object v0, v15, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 246
    .line 247
    iget-object v1, v15, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 248
    .line 249
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 250
    .line 251
    .line 252
    move-result-object v1

    .line 253
    iget-object v4, v15, Lcom/intsig/office/wp/view/NormalRoot;->docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

    .line 254
    .line 255
    iget-object v5, v15, Lcom/intsig/office/wp/view/NormalRoot;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 256
    .line 257
    iget-object v6, v15, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 258
    .line 259
    move-object/from16 v21, v7

    .line 260
    .line 261
    check-cast v21, Lcom/intsig/office/wp/view/TableView;

    .line 262
    .line 263
    iget-wide v2, v15, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 264
    .line 265
    const/16 v22, 0x0

    .line 266
    .line 267
    move-wide/from16 v23, v2

    .line 268
    .line 269
    move-object v2, v11

    .line 270
    move-object/from16 v3, p0

    .line 271
    .line 272
    move-object/from16 v25, v7

    .line 273
    .line 274
    move-object/from16 v7, v21

    .line 275
    .line 276
    move/from16 v21, v8

    .line 277
    .line 278
    move-wide/from16 v8, v23

    .line 279
    .line 280
    move/from16 v23, v10

    .line 281
    .line 282
    move v10, v13

    .line 283
    move-object/from16 v24, v11

    .line 284
    .line 285
    move/from16 v11, v23

    .line 286
    .line 287
    move/from16 v12, v16

    .line 288
    .line 289
    move/from16 v26, v13

    .line 290
    .line 291
    move/from16 v13, v20

    .line 292
    .line 293
    move/from16 v14, v17

    .line 294
    .line 295
    move/from16 v15, v22

    .line 296
    .line 297
    invoke-virtual/range {v0 .. v15}, Lcom/intsig/office/wp/view/TableLayoutKit;->layoutTable(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/IRoot;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/TableView;JIIIIIZ)I

    .line 298
    .line 299
    .line 300
    const/4 v0, 0x1

    .line 301
    move-object/from16 v14, p0

    .line 302
    .line 303
    move-object/from16 v1, v25

    .line 304
    .line 305
    goto :goto_5

    .line 306
    :cond_5
    move-object/from16 v25, v7

    .line 307
    .line 308
    move/from16 v21, v8

    .line 309
    .line 310
    move/from16 v23, v10

    .line 311
    .line 312
    move-object/from16 v24, v11

    .line 313
    .line 314
    move/from16 v26, v13

    .line 315
    .line 316
    move-object v14, v15

    .line 317
    iget-object v1, v14, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 318
    .line 319
    invoke-virtual {v1}, Lcom/intsig/office/wp/view/TableLayoutKit;->clearBreakPages()V

    .line 320
    .line 321
    .line 322
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 323
    .line 324
    .line 325
    move-result-object v1

    .line 326
    iget-object v2, v14, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 327
    .line 328
    invoke-virtual {v2}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 329
    .line 330
    .line 331
    move-result-object v2

    .line 332
    iget-object v3, v14, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 333
    .line 334
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 335
    .line 336
    .line 337
    move-result-object v0

    .line 338
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->fillParaAttr(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 339
    .line 340
    .line 341
    iget-object v0, v14, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 342
    .line 343
    invoke-direct {v14, v0}, Lcom/intsig/office/wp/view/NormalRoot;->filteParaAttr(Lcom/intsig/office/simpletext/view/ParaAttr;)V

    .line 344
    .line 345
    .line 346
    invoke-static {}, Lcom/intsig/office/wp/view/LayoutKit;->instance()Lcom/intsig/office/wp/view/LayoutKit;

    .line 347
    .line 348
    .line 349
    move-result-object v0

    .line 350
    iget-object v1, v14, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 351
    .line 352
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 353
    .line 354
    .line 355
    move-result-object v1

    .line 356
    iget-object v3, v14, Lcom/intsig/office/wp/view/NormalRoot;->docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

    .line 357
    .line 358
    iget-object v4, v14, Lcom/intsig/office/wp/view/NormalRoot;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 359
    .line 360
    iget-object v5, v14, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 361
    .line 362
    iget-wide v7, v14, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 363
    .line 364
    move-object/from16 v2, v24

    .line 365
    .line 366
    move-object/from16 v6, v25

    .line 367
    .line 368
    move/from16 v9, v26

    .line 369
    .line 370
    move/from16 v11, v16

    .line 371
    .line 372
    move/from16 v12, v20

    .line 373
    .line 374
    move/from16 v13, v17

    .line 375
    .line 376
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/wp/view/LayoutKit;->layoutPara(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/IDocument;Lcom/intsig/office/simpletext/view/DocAttr;Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/wp/view/ParagraphView;JIIIII)I

    .line 377
    .line 378
    .line 379
    move-object/from16 v1, v25

    .line 380
    .line 381
    const/4 v0, 0x1

    .line 382
    :goto_5
    invoke-virtual {v1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 383
    .line 384
    .line 385
    move-result v2

    .line 386
    const/4 v3, 0x0

    .line 387
    invoke-virtual {v1, v3}, Lcom/intsig/office/simpletext/view/AbstractView;->getLayoutSpan(B)I

    .line 388
    .line 389
    .line 390
    move-result v4

    .line 391
    sget v5, Lcom/intsig/office/constant/wp/WPViewConstant;->PAGE_SPACE:I

    .line 392
    .line 393
    add-int/2addr v4, v5

    .line 394
    iget v5, v14, Lcom/intsig/office/wp/view/NormalRoot;->maxParaWidth:I

    .line 395
    .line 396
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    .line 397
    .line 398
    .line 399
    move-result v4

    .line 400
    iput v4, v14, Lcom/intsig/office/wp/view/NormalRoot;->maxParaWidth:I

    .line 401
    .line 402
    add-int v10, v23, v2

    .line 403
    .line 404
    sub-int v20, v20, v2

    .line 405
    .line 406
    const/4 v2, 0x0

    .line 407
    invoke-virtual {v1, v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J

    .line 408
    .line 409
    .line 410
    move-result-wide v4

    .line 411
    iput-wide v4, v14, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 412
    .line 413
    add-int/lit8 v8, v21, 0x1

    .line 414
    .line 415
    iput-object v1, v14, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 416
    .line 417
    iget-object v2, v14, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 418
    .line 419
    invoke-virtual {v2, v1}, Lcom/intsig/office/simpletext/view/ViewContainer;->add(Lcom/intsig/office/simpletext/view/IView;)V

    .line 420
    .line 421
    .line 422
    move-object v15, v14

    .line 423
    move-object/from16 v11, v24

    .line 424
    .line 425
    move/from16 v13, v26

    .line 426
    .line 427
    const/4 v12, 0x0

    .line 428
    const/4 v14, 0x1

    .line 429
    goto/16 :goto_2

    .line 430
    .line 431
    :cond_6
    move-object v14, v15

    .line 432
    const/4 v3, 0x0

    .line 433
    return v3
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/wp/view/NormalRoot;)Lcom/intsig/office/wp/control/Word;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public declared-synchronized backLayout()V
    .locals 10

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutPara()I

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutRoot()V

    .line 6
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 11
    .line 12
    const-wide/16 v3, 0x0

    .line 13
    .line 14
    invoke-interface {v2, v3, v4}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 15
    .line 16
    .line 17
    move-result-wide v5

    .line 18
    cmp-long v2, v0, v5

    .line 19
    .line 20
    if-ltz v2, :cond_3

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 29
    .line 30
    const/16 v2, 0x16

    .line 31
    .line 32
    invoke-interface {v0, v2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 42
    .line 43
    const/16 v2, 0x1a

    .line 44
    .line 45
    invoke-interface {v0, v2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getVisibleRect()Lcom/intsig/office/java/awt/Rectangle;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 55
    .line 56
    iget v2, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getWidth()I

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    int-to-float v5, v5

    .line 63
    iget-object v6, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 64
    .line 65
    invoke-virtual {v6}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    mul-float v5, v5, v6

    .line 70
    .line 71
    float-to-int v5, v5

    .line 72
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 73
    .line 74
    .line 75
    move-result v6

    .line 76
    int-to-float v6, v6

    .line 77
    iget-object v7, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 78
    .line 79
    invoke-virtual {v7}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 80
    .line 81
    .line 82
    move-result v7

    .line 83
    mul-float v6, v6, v7

    .line 84
    .line 85
    float-to-int v6, v6

    .line 86
    iget v7, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 87
    .line 88
    iget v8, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 89
    .line 90
    add-int v9, v7, v8

    .line 91
    .line 92
    if-le v9, v5, :cond_0

    .line 93
    .line 94
    sub-int v1, v5, v8

    .line 95
    .line 96
    :cond_0
    iget v5, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 97
    .line 98
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 99
    .line 100
    add-int v8, v5, v0

    .line 101
    .line 102
    if-le v8, v6, :cond_1

    .line 103
    .line 104
    sub-int v2, v6, v0

    .line 105
    .line 106
    :cond_1
    if-ne v1, v7, :cond_2

    .line 107
    .line 108
    if-eq v2, v5, :cond_3

    .line 109
    .line 110
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 111
    .line 112
    new-instance v5, Lcom/intsig/office/wp/view/NormalRoot$1;

    .line 113
    .line 114
    invoke-direct {v5, p0, v1, v2}, Lcom/intsig/office/wp/view/NormalRoot$1;-><init>(Lcom/intsig/office/wp/view/NormalRoot;II)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v5}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 118
    .line 119
    .line 120
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 121
    .line 122
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 126
    .line 127
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->isExportImageAfterZoom()Z

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    if-eqz v0, :cond_5

    .line 132
    .line 133
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    int-to-float v0, v0

    .line 138
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 139
    .line 140
    invoke-virtual {v1}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    mul-float v0, v0, v1

    .line 145
    .line 146
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 147
    .line 148
    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 153
    .line 154
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 155
    .line 156
    .line 157
    move-result v2

    .line 158
    add-int/2addr v1, v2

    .line 159
    int-to-float v1, v1

    .line 160
    cmpl-float v0, v0, v1

    .line 161
    .line 162
    if-gez v0, :cond_4

    .line 163
    .line 164
    iget-wide v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 165
    .line 166
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 167
    .line 168
    invoke-interface {v2, v3, v4}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 169
    .line 170
    .line 171
    move-result-wide v2

    .line 172
    cmp-long v4, v0, v2

    .line 173
    .line 174
    if-ltz v4, :cond_5

    .line 175
    .line 176
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 177
    .line 178
    const/4 v1, 0x0

    .line 179
    invoke-virtual {v0, v1}, Lcom/intsig/office/wp/control/Word;->setExportImageAfterZoom(Z)V

    .line 180
    .line 181
    .line 182
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 183
    .line 184
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    const v1, 0x2000000a

    .line 189
    .line 190
    .line 191
    const/4 v2, 0x0

    .line 192
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    .line 194
    .line 195
    :cond_5
    monitor-exit p0

    .line 196
    return-void

    .line 197
    :catchall_0
    move-exception v0

    .line 198
    monitor-exit p0

    .line 199
    throw v0
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public canBackLayout()Z
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->canBackLayout:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-wide v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 8
    .line 9
    const-wide/16 v3, 0x0

    .line 10
    .line 11
    invoke-interface {v2, v3, v4}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 12
    .line 13
    .line 14
    move-result-wide v2

    .line 15
    cmp-long v4, v0, v2

    .line 16
    .line 17
    if-gez v4, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public declared-synchronized dispose()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->dispose()V

    .line 3
    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->canBackLayout:Z

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/LayoutThread;->dispose()V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/DocAttr;->dispose()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->docAttr:Lcom/intsig/office/simpletext/view/DocAttr;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/PageAttr;->dispose()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->pageAttr:Lcom/intsig/office/simpletext/view/PageAttr;

    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/ParaAttr;->dispose()V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->paraAttr:Lcom/intsig/office/simpletext/view/ParaAttr;

    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    .line 45
    monitor-exit p0

    .line 46
    return-void

    .line 47
    :catchall_0
    move-exception v0

    .line 48
    monitor-exit p0

    .line 49
    throw v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public doLayout(IIIIII)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/NormalRoot;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object p2, p0, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 6
    .line 7
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/ViewContainer;->clear()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutPara()I

    .line 11
    .line 12
    .line 13
    iget-wide p2, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 14
    .line 15
    const-wide/16 p4, 0x0

    .line 16
    .line 17
    invoke-interface {p1, p4, p5}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 18
    .line 19
    .line 20
    move-result-wide p4

    .line 21
    cmp-long p1, p2, p4

    .line 22
    .line 23
    if-gez p1, :cond_1

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object p2, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    .line 32
    .line 33
    if-ne p1, p2, :cond_0

    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 36
    .line 37
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 38
    .line 39
    .line 40
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    const/16 p2, 0x1a

    .line 47
    .line 48
    sget-object p3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 49
    .line 50
    invoke-interface {p1, p2, p3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutRoot()V

    .line 54
    .line 55
    .line 56
    const/4 p1, 0x0

    .line 57
    return p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public draw(Landroid/graphics/Canvas;IIF)V
    .locals 4

    .line 1
    const/4 v0, -0x1

    .line 2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 3
    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/simpletext/view/AbstractView;->x:I

    .line 6
    .line 7
    int-to-float v0, v0

    .line 8
    mul-float v0, v0, p4

    .line 9
    .line 10
    float-to-int v0, v0

    .line 11
    add-int/2addr v0, p2

    .line 12
    iget p2, p0, Lcom/intsig/office/simpletext/view/AbstractView;->y:I

    .line 13
    .line 14
    int-to-float p2, p2

    .line 15
    mul-float p2, p2, p4

    .line 16
    .line 17
    float-to-int p2, p2

    .line 18
    add-int/2addr p2, p3

    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 20
    .line 21
    .line 22
    move-result-object p3

    .line 23
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const/4 v2, 0x0

    .line 28
    :goto_0
    if-eqz p3, :cond_2

    .line 29
    .line 30
    invoke-interface {p3, v1, v0, p2, p4}, Lcom/intsig/office/simpletext/view/IView;->intersection(Landroid/graphics/Rect;IIF)Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-eqz v3, :cond_0

    .line 35
    .line 36
    invoke-interface {p3, p1, v0, p2, p4}, Lcom/intsig/office/simpletext/view/IView;->draw(Landroid/graphics/Canvas;IIF)V

    .line 37
    .line 38
    .line 39
    const/4 v2, 0x1

    .line 40
    goto :goto_1

    .line 41
    :cond_0
    if-eqz v2, :cond_1

    .line 42
    .line 43
    goto :goto_2

    .line 44
    :cond_1
    :goto_1
    invoke-interface {p3}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 45
    .line 46
    .line 47
    move-result-object p3

    .line 48
    goto :goto_0

    .line 49
    :cond_2
    :goto_2
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getContainer()Lcom/intsig/office/simpletext/control/IWord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getControl()Lcom/intsig/office/system/IControl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocument()Lcom/intsig/office/simpletext/model/IDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getDocument()Lcom/intsig/office/simpletext/model/IDocument;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()S
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getViewContainer()Lcom/intsig/office/simpletext/view/ViewContainer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public declared-synchronized layoutAll()I
    .locals 9

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->dispose()V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->tableLayout:Lcom/intsig/office/wp/view/TableLayoutKit;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/TableLayoutKit;->clearBreakPages()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/common/bulletnumber/ListManage;->resetForNormalView()V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/view/ViewContainer;->clear()V

    .line 30
    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->maxParaWidth:I

    .line 34
    .line 35
    const/4 v1, 0x0

    .line 36
    iput-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 37
    .line 38
    const-wide/16 v2, 0x0

    .line 39
    .line 40
    iput-wide v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutPara()I

    .line 43
    .line 44
    .line 45
    iget-wide v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 46
    .line 47
    iget-object v6, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 48
    .line 49
    invoke-interface {v6, v2, v3}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 50
    .line 51
    .line 52
    move-result-wide v6

    .line 53
    cmp-long v8, v4, v6

    .line 54
    .line 55
    if-gez v8, :cond_1

    .line 56
    .line 57
    const/4 v4, 0x1

    .line 58
    iput-boolean v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->canBackLayout:Z

    .line 59
    .line 60
    iget-object v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 61
    .line 62
    invoke-virtual {v4}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    sget-object v5, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    .line 67
    .line 68
    if-ne v4, v5, :cond_0

    .line 69
    .line 70
    iget-object v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->layoutThread:Lcom/intsig/office/wp/view/LayoutThread;

    .line 71
    .line 72
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 73
    .line 74
    .line 75
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 76
    .line 77
    invoke-virtual {v4}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 82
    .line 83
    const/16 v6, 0x1a

    .line 84
    .line 85
    invoke-interface {v4, v6, v5}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/wp/view/NormalRoot;->layoutRoot()V

    .line 89
    .line 90
    .line 91
    iget-object v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 92
    .line 93
    invoke-virtual {v4}, Lcom/intsig/office/wp/control/Word;->isExportImageAfterZoom()Z

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    if-eqz v4, :cond_3

    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    int-to-float v4, v4

    .line 104
    iget-object v5, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 105
    .line 106
    invoke-virtual {v5}, Lcom/intsig/office/wp/control/Word;->getZoom()F

    .line 107
    .line 108
    .line 109
    move-result v5

    .line 110
    mul-float v4, v4, v5

    .line 111
    .line 112
    iget-object v5, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 113
    .line 114
    invoke-virtual {v5}, Landroid/view/View;->getScrollY()I

    .line 115
    .line 116
    .line 117
    move-result v5

    .line 118
    iget-object v6, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 119
    .line 120
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    add-int/2addr v5, v6

    .line 125
    int-to-float v5, v5

    .line 126
    cmpl-float v4, v4, v5

    .line 127
    .line 128
    if-gez v4, :cond_2

    .line 129
    .line 130
    iget-wide v4, p0, Lcom/intsig/office/wp/view/NormalRoot;->currentLayoutOffset:J

    .line 131
    .line 132
    iget-object v6, p0, Lcom/intsig/office/wp/view/NormalRoot;->doc:Lcom/intsig/office/simpletext/model/IDocument;

    .line 133
    .line 134
    invoke-interface {v6, v2, v3}, Lcom/intsig/office/simpletext/model/IDocument;->getAreaEnd(J)J

    .line 135
    .line 136
    .line 137
    move-result-wide v2

    .line 138
    cmp-long v6, v4, v2

    .line 139
    .line 140
    if-ltz v6, :cond_3

    .line 141
    .line 142
    :cond_2
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 143
    .line 144
    invoke-virtual {v2, v0}, Lcom/intsig/office/wp/control/Word;->setExportImageAfterZoom(Z)V

    .line 145
    .line 146
    .line 147
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 148
    .line 149
    invoke-virtual {v2}, Lcom/intsig/office/wp/control/Word;->getControl()Lcom/intsig/office/system/IControl;

    .line 150
    .line 151
    .line 152
    move-result-object v2

    .line 153
    const v3, 0x2000000a

    .line 154
    .line 155
    .line 156
    invoke-interface {v2, v3, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    .line 158
    .line 159
    :cond_3
    monitor-exit p0

    .line 160
    return v0

    .line 161
    :catchall_0
    move-exception v0

    .line 162
    monitor-exit p0

    .line 163
    throw v0
.end method

.method public layoutRoot()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->word:Lcom/intsig/office/wp/control/Word;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->maxParaWidth:I

    .line 12
    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iget-object v1, p0, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    iget-object v2, p0, Lcom/intsig/office/wp/view/NormalRoot;->prePara:Lcom/intsig/office/wp/view/ParagraphView;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    add-int/2addr v1, v2

    .line 30
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setSize(II)V

    .line 31
    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->viewContainer:Lcom/intsig/office/simpletext/view/ViewContainer;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p4}, Lcom/intsig/office/simpletext/view/ViewContainer;->getParagraph(JZ)Lcom/intsig/office/simpletext/view/IView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/IView;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 10
    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    :goto_0
    if-eqz p1, :cond_0

    .line 17
    .line 18
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getType()S

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    const/4 p4, 0x1

    .line 23
    if-eq p2, p4, :cond_0

    .line 24
    .line 25
    iget p2, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 26
    .line 27
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getX()I

    .line 28
    .line 29
    .line 30
    move-result p4

    .line 31
    add-int/2addr p2, p4

    .line 32
    iput p2, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 33
    .line 34
    iget p2, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 35
    .line 36
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 37
    .line 38
    .line 39
    move-result p4

    .line 40
    add-int/2addr p2, p4

    .line 41
    iput p2, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 42
    .line 43
    invoke-interface {p1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    goto :goto_0

    .line 48
    :cond_0
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    add-int/2addr p1, p2

    .line 55
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 56
    .line 57
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 60
    .line 61
    .line 62
    move-result p2

    .line 63
    add-int/2addr p1, p2

    .line 64
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 65
    .line 66
    return-object p3
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public stopBackLayout()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->canBackLayout:Z

    .line 3
    .line 4
    iput-boolean v0, p0, Lcom/intsig/office/wp/view/NormalRoot;->relayout:Z

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public viewToModel(IIZ)J
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getX()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sub-int/2addr p1, v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    sub-int/2addr p2, v0

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-wide/16 v1, -0x1

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    return-wide v1

    .line 20
    :cond_0
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-le p2, v3, :cond_2

    .line 25
    .line 26
    :goto_0
    if-eqz v0, :cond_2

    .line 27
    .line 28
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-lt p2, v3, :cond_1

    .line 33
    .line 34
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getY()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    const/4 v4, 0x1

    .line 39
    invoke-interface {v0, v4}, Lcom/intsig/office/simpletext/view/IView;->getLayoutSpan(B)I

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    add-int/2addr v3, v4

    .line 44
    if-ge p2, v3, :cond_1

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getNextView()Lcom/intsig/office/simpletext/view/IView;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    goto :goto_0

    .line 52
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/view/AbstractView;->getChildView()Lcom/intsig/office/simpletext/view/IView;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    :cond_3
    if-eqz v0, :cond_4

    .line 59
    .line 60
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/office/simpletext/view/IView;->viewToModel(IIZ)J

    .line 61
    .line 62
    .line 63
    move-result-wide p1

    .line 64
    return-wide p1

    .line 65
    :cond_4
    return-wide v1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public Lcom/intsig/office/wp/view/PositionLayoutKit;
.super Ljava/lang/Object;
.source "PositionLayoutKit.java"


# static fields
.field private static kit:Lcom/intsig/office/wp/view/PositionLayoutKit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/wp/view/PositionLayoutKit;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/wp/view/PositionLayoutKit;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/wp/view/PositionLayoutKit;->kit:Lcom/intsig/office/wp/view/PositionLayoutKit;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static instance()Lcom/intsig/office/wp/view/PositionLayoutKit;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/wp/view/PositionLayoutKit;->kit:Lcom/intsig/office/wp/view/PositionLayoutKit;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processHorizontalPosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorPositionType()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x2

    .line 10
    const/4 v3, 0x1

    .line 11
    if-ne v0, v3, :cond_7

    .line 12
    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorRelativeValue()I

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    int-to-float p2, p2

    .line 18
    const/high16 v0, 0x447a0000    # 1000.0f

    .line 19
    .line 20
    div-float/2addr p2, v0

    .line 21
    if-ne v1, v2, :cond_0

    .line 22
    .line 23
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 24
    .line 25
    int-to-float p3, p3

    .line 26
    mul-float p3, p3, p2

    .line 27
    .line 28
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 33
    .line 34
    .line 35
    goto/16 :goto_0

    .line 36
    .line 37
    :cond_0
    if-ne v1, v3, :cond_1

    .line 38
    .line 39
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 40
    .line 41
    iget v1, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 42
    .line 43
    sub-int/2addr v1, v0

    .line 44
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 45
    .line 46
    sub-int/2addr v1, p3

    .line 47
    int-to-float p3, v1

    .line 48
    mul-float p3, p3, p2

    .line 49
    .line 50
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    add-int/2addr v0, p2

    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_1
    const/4 v0, 0x4

    .line 61
    if-ne v1, v0, :cond_2

    .line 62
    .line 63
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 64
    .line 65
    int-to-float p3, p3

    .line 66
    mul-float p3, p3, p2

    .line 67
    .line 68
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 73
    .line 74
    .line 75
    goto/16 :goto_0

    .line 76
    .line 77
    :cond_2
    const/4 v0, 0x5

    .line 78
    if-ne v1, v0, :cond_3

    .line 79
    .line 80
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 81
    .line 82
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 83
    .line 84
    sub-int/2addr v0, p3

    .line 85
    int-to-float p3, p3

    .line 86
    mul-float p3, p3, p2

    .line 87
    .line 88
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result p2

    .line 92
    add-int/2addr v0, p2

    .line 93
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 94
    .line 95
    .line 96
    goto/16 :goto_0

    .line 97
    .line 98
    :cond_3
    const/16 v0, 0x9

    .line 99
    .line 100
    if-ne v1, v0, :cond_5

    .line 101
    .line 102
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    if-eqz v0, :cond_d

    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    if-eqz v0, :cond_d

    .line 117
    .line 118
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    if-eqz v0, :cond_d

    .line 131
    .line 132
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 145
    .line 146
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    rem-int/2addr v0, v2

    .line 151
    if-ne v0, v3, :cond_4

    .line 152
    .line 153
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 154
    .line 155
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 156
    .line 157
    sub-int/2addr v0, p3

    .line 158
    int-to-float p3, p3

    .line 159
    mul-float p3, p3, p2

    .line 160
    .line 161
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 162
    .line 163
    .line 164
    move-result p2

    .line 165
    add-int/2addr v0, p2

    .line 166
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 167
    .line 168
    .line 169
    goto/16 :goto_0

    .line 170
    .line 171
    :cond_4
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 172
    .line 173
    int-to-float p3, p3

    .line 174
    mul-float p3, p3, p2

    .line 175
    .line 176
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 177
    .line 178
    .line 179
    move-result p2

    .line 180
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 181
    .line 182
    .line 183
    goto :goto_0

    .line 184
    :cond_5
    const/16 v0, 0x8

    .line 185
    .line 186
    if-ne v1, v0, :cond_d

    .line 187
    .line 188
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 189
    .line 190
    .line 191
    move-result-object v0

    .line 192
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 201
    .line 202
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 203
    .line 204
    .line 205
    move-result v0

    .line 206
    rem-int/2addr v0, v2

    .line 207
    if-ne v0, v3, :cond_6

    .line 208
    .line 209
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 210
    .line 211
    int-to-float p3, p3

    .line 212
    mul-float p3, p3, p2

    .line 213
    .line 214
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 215
    .line 216
    .line 217
    move-result p2

    .line 218
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 219
    .line 220
    .line 221
    goto :goto_0

    .line 222
    :cond_6
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 223
    .line 224
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 225
    .line 226
    sub-int/2addr v0, p3

    .line 227
    int-to-float p3, p3

    .line 228
    mul-float p3, p3, p2

    .line 229
    .line 230
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 231
    .line 232
    .line 233
    move-result p2

    .line 234
    add-int/2addr v0, p2

    .line 235
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 236
    .line 237
    .line 238
    goto :goto_0

    .line 239
    :cond_7
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalAlignment()B

    .line 240
    .line 241
    .line 242
    move-result v0

    .line 243
    if-nez v0, :cond_8

    .line 244
    .line 245
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Absolute(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 246
    .line 247
    .line 248
    goto :goto_0

    .line 249
    :cond_8
    if-ne v0, v3, :cond_9

    .line 250
    .line 251
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Left(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 252
    .line 253
    .line 254
    goto :goto_0

    .line 255
    :cond_9
    if-ne v0, v2, :cond_a

    .line 256
    .line 257
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Center(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 258
    .line 259
    .line 260
    goto :goto_0

    .line 261
    :cond_a
    const/4 v1, 0x3

    .line 262
    if-ne v0, v1, :cond_b

    .line 263
    .line 264
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Right(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 265
    .line 266
    .line 267
    goto :goto_0

    .line 268
    :cond_b
    const/4 v1, 0x6

    .line 269
    if-ne v0, v1, :cond_c

    .line 270
    .line 271
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Inside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 272
    .line 273
    .line 274
    goto :goto_0

    .line 275
    :cond_c
    const/4 v1, 0x7

    .line 276
    if-ne v0, v1, :cond_d

    .line 277
    .line 278
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition_Outside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 279
    .line 280
    .line 281
    :cond_d
    :goto_0
    return-void
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processHorizontalPosition_Absolute(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    const/4 v1, 0x1

    .line 10
    if-eq p2, v1, :cond_7

    .line 11
    .line 12
    const/16 v2, 0xa

    .line 13
    .line 14
    if-eq p2, v2, :cond_7

    .line 15
    .line 16
    if-eqz p2, :cond_7

    .line 17
    .line 18
    const/4 v2, 0x3

    .line 19
    if-ne p2, v2, :cond_0

    .line 20
    .line 21
    goto/16 :goto_1

    .line 22
    .line 23
    :cond_0
    const/4 v2, 0x2

    .line 24
    if-eq p2, v2, :cond_6

    .line 25
    .line 26
    const/4 v3, 0x4

    .line 27
    if-ne p2, v3, :cond_1

    .line 28
    .line 29
    goto/16 :goto_0

    .line 30
    .line 31
    :cond_1
    const/4 v3, 0x5

    .line 32
    if-ne p2, v3, :cond_2

    .line 33
    .line 34
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 35
    .line 36
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 37
    .line 38
    sub-int/2addr p2, p3

    .line 39
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 40
    .line 41
    add-int/2addr p2, p3

    .line 42
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 43
    .line 44
    .line 45
    goto/16 :goto_2

    .line 46
    .line 47
    :cond_2
    const/16 v3, 0x9

    .line 48
    .line 49
    if-ne p2, v3, :cond_4

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    if-eqz p2, :cond_8

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    if-eqz p2, :cond_8

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    if-eqz p2, :cond_8

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 90
    .line 91
    .line 92
    move-result-object p2

    .line 93
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 94
    .line 95
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 96
    .line 97
    .line 98
    move-result p2

    .line 99
    rem-int/2addr p2, v2

    .line 100
    if-ne p2, v1, :cond_3

    .line 101
    .line 102
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 103
    .line 104
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 105
    .line 106
    sub-int/2addr p2, p3

    .line 107
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 108
    .line 109
    add-int/2addr p2, p3

    .line 110
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_3
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 115
    .line 116
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 117
    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_4
    const/16 v3, 0x8

    .line 121
    .line 122
    if-ne p2, v3, :cond_8

    .line 123
    .line 124
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 125
    .line 126
    .line 127
    move-result-object p2

    .line 128
    if-eqz p2, :cond_8

    .line 129
    .line 130
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 131
    .line 132
    .line 133
    move-result-object p2

    .line 134
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    if-eqz p2, :cond_8

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 145
    .line 146
    .line 147
    move-result-object p2

    .line 148
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 149
    .line 150
    .line 151
    move-result-object p2

    .line 152
    if-eqz p2, :cond_8

    .line 153
    .line 154
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 159
    .line 160
    .line 161
    move-result-object p2

    .line 162
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 167
    .line 168
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 169
    .line 170
    .line 171
    move-result p2

    .line 172
    rem-int/2addr p2, v2

    .line 173
    if-ne p2, v1, :cond_5

    .line 174
    .line 175
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 176
    .line 177
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 178
    .line 179
    .line 180
    goto :goto_2

    .line 181
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 182
    .line 183
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 184
    .line 185
    sub-int/2addr p2, p3

    .line 186
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 187
    .line 188
    add-int/2addr p2, p3

    .line 189
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 190
    .line 191
    .line 192
    goto :goto_2

    .line 193
    :cond_6
    :goto_0
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 194
    .line 195
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 196
    .line 197
    .line 198
    goto :goto_2

    .line 199
    :cond_7
    :goto_1
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 200
    .line 201
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 202
    .line 203
    add-int/2addr p2, p3

    .line 204
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 205
    .line 206
    .line 207
    :cond_8
    :goto_2
    return-void
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processHorizontalPosition_Center(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    div-int/2addr v0, v1

    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    if-ne p2, v1, :cond_0

    .line 14
    .line 15
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 16
    .line 17
    div-int/2addr p2, v1

    .line 18
    sub-int/2addr p2, v0

    .line 19
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 20
    .line 21
    .line 22
    goto/16 :goto_1

    .line 23
    .line 24
    :cond_0
    const/4 v2, 0x1

    .line 25
    if-eq p2, v2, :cond_8

    .line 26
    .line 27
    if-nez p2, :cond_1

    .line 28
    .line 29
    goto/16 :goto_0

    .line 30
    .line 31
    :cond_1
    const/4 v3, 0x3

    .line 32
    if-ne p2, v3, :cond_2

    .line 33
    .line 34
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 35
    .line 36
    sub-int/2addr p2, v0

    .line 37
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 38
    .line 39
    .line 40
    goto/16 :goto_1

    .line 41
    .line 42
    :cond_2
    const/4 v3, 0x4

    .line 43
    if-ne p2, v3, :cond_3

    .line 44
    .line 45
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 46
    .line 47
    div-int/2addr p2, v1

    .line 48
    sub-int/2addr p2, v0

    .line 49
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 50
    .line 51
    .line 52
    goto/16 :goto_1

    .line 53
    .line 54
    :cond_3
    const/4 v3, 0x5

    .line 55
    if-ne p2, v3, :cond_4

    .line 56
    .line 57
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 58
    .line 59
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 60
    .line 61
    div-int/2addr p3, v1

    .line 62
    sub-int/2addr p2, p3

    .line 63
    sub-int/2addr p2, v0

    .line 64
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 65
    .line 66
    .line 67
    goto/16 :goto_1

    .line 68
    .line 69
    :cond_4
    const/16 v3, 0x9

    .line 70
    .line 71
    if-ne p2, v3, :cond_6

    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    if-eqz p2, :cond_9

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    if-eqz p2, :cond_9

    .line 88
    .line 89
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 90
    .line 91
    .line 92
    move-result-object p2

    .line 93
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    if-eqz p2, :cond_9

    .line 102
    .line 103
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 108
    .line 109
    .line 110
    move-result-object p2

    .line 111
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 116
    .line 117
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 118
    .line 119
    .line 120
    move-result p2

    .line 121
    rem-int/2addr p2, v1

    .line 122
    if-ne p2, v2, :cond_5

    .line 123
    .line 124
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 125
    .line 126
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 127
    .line 128
    div-int/2addr p3, v1

    .line 129
    sub-int/2addr p2, p3

    .line 130
    sub-int/2addr p2, v0

    .line 131
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 132
    .line 133
    .line 134
    goto :goto_1

    .line 135
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 136
    .line 137
    div-int/2addr p2, v1

    .line 138
    sub-int/2addr p2, v0

    .line 139
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 140
    .line 141
    .line 142
    goto :goto_1

    .line 143
    :cond_6
    const/16 v3, 0x8

    .line 144
    .line 145
    if-ne p2, v3, :cond_9

    .line 146
    .line 147
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 148
    .line 149
    .line 150
    move-result-object p2

    .line 151
    if-eqz p2, :cond_9

    .line 152
    .line 153
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 154
    .line 155
    .line 156
    move-result-object p2

    .line 157
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 158
    .line 159
    .line 160
    move-result-object p2

    .line 161
    if-eqz p2, :cond_9

    .line 162
    .line 163
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 164
    .line 165
    .line 166
    move-result-object p2

    .line 167
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 168
    .line 169
    .line 170
    move-result-object p2

    .line 171
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    if-eqz p2, :cond_9

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 178
    .line 179
    .line 180
    move-result-object p2

    .line 181
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 182
    .line 183
    .line 184
    move-result-object p2

    .line 185
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 186
    .line 187
    .line 188
    move-result-object p2

    .line 189
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 190
    .line 191
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 192
    .line 193
    .line 194
    move-result p2

    .line 195
    rem-int/2addr p2, v1

    .line 196
    if-ne p2, v2, :cond_7

    .line 197
    .line 198
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 199
    .line 200
    div-int/2addr p2, v1

    .line 201
    sub-int/2addr p2, v0

    .line 202
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 203
    .line 204
    .line 205
    goto :goto_1

    .line 206
    :cond_7
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 207
    .line 208
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 209
    .line 210
    div-int/2addr p3, v1

    .line 211
    sub-int/2addr p2, p3

    .line 212
    sub-int/2addr p2, v0

    .line 213
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 214
    .line 215
    .line 216
    goto :goto_1

    .line 217
    :cond_8
    :goto_0
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 218
    .line 219
    iget v2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 220
    .line 221
    sub-int/2addr v2, p2

    .line 222
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 223
    .line 224
    sub-int/2addr v2, p3

    .line 225
    div-int/2addr v2, v1

    .line 226
    add-int/2addr p2, v2

    .line 227
    sub-int/2addr p2, v0

    .line 228
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 229
    .line 230
    .line 231
    :cond_9
    :goto_1
    return-void
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processHorizontalPosition_Inside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_3

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 44
    .line 45
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 50
    .line 51
    .line 52
    move-result p2

    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    const/4 v2, 0x2

    .line 58
    rem-int/2addr v0, v2

    .line 59
    const/4 v3, 0x1

    .line 60
    if-ne v0, v3, :cond_1

    .line 61
    .line 62
    if-ne p2, v2, :cond_0

    .line 63
    .line 64
    const/4 p2, 0x0

    .line 65
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    if-ne p2, v3, :cond_3

    .line 70
    .line 71
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 72
    .line 73
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    if-ne p2, v2, :cond_2

    .line 78
    .line 79
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 80
    .line 81
    iget p3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 82
    .line 83
    sub-int/2addr p2, p3

    .line 84
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    if-ne p2, v3, :cond_3

    .line 89
    .line 90
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 91
    .line 92
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 93
    .line 94
    sub-int/2addr p2, p3

    .line 95
    iget p3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 96
    .line 97
    sub-int/2addr p2, p3

    .line 98
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 99
    .line 100
    .line 101
    :cond_3
    :goto_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processHorizontalPosition_Left(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 5
    .line 6
    .line 7
    move-result p2

    .line 8
    const/4 v0, 0x1

    .line 9
    if-eq p2, v0, :cond_7

    .line 10
    .line 11
    const/16 v1, 0xa

    .line 12
    .line 13
    if-eq p2, v1, :cond_7

    .line 14
    .line 15
    if-eqz p2, :cond_7

    .line 16
    .line 17
    const/4 v1, 0x3

    .line 18
    if-ne p2, v1, :cond_0

    .line 19
    .line 20
    goto/16 :goto_1

    .line 21
    .line 22
    :cond_0
    const/4 v1, 0x0

    .line 23
    const/4 v2, 0x2

    .line 24
    if-eq p2, v2, :cond_6

    .line 25
    .line 26
    const/4 v3, 0x4

    .line 27
    if-ne p2, v3, :cond_1

    .line 28
    .line 29
    goto/16 :goto_0

    .line 30
    .line 31
    :cond_1
    const/4 v3, 0x5

    .line 32
    if-ne p2, v3, :cond_2

    .line 33
    .line 34
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 35
    .line 36
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 37
    .line 38
    sub-int/2addr p2, p3

    .line 39
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_2

    .line 43
    .line 44
    :cond_2
    const/16 v3, 0x9

    .line 45
    .line 46
    if-ne p2, v3, :cond_4

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    if-eqz p2, :cond_8

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 55
    .line 56
    .line 57
    move-result-object p2

    .line 58
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    if-eqz p2, :cond_8

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 65
    .line 66
    .line 67
    move-result-object p2

    .line 68
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    if-eqz p2, :cond_8

    .line 77
    .line 78
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 79
    .line 80
    .line 81
    move-result-object p2

    .line 82
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 91
    .line 92
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 93
    .line 94
    .line 95
    move-result p2

    .line 96
    rem-int/2addr p2, v2

    .line 97
    if-ne p2, v0, :cond_3

    .line 98
    .line 99
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 100
    .line 101
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 102
    .line 103
    sub-int/2addr p2, p3

    .line 104
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 105
    .line 106
    .line 107
    goto :goto_2

    .line 108
    :cond_3
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 109
    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_4
    const/16 v3, 0x8

    .line 113
    .line 114
    if-ne p2, v3, :cond_8

    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 117
    .line 118
    .line 119
    move-result-object p2

    .line 120
    if-eqz p2, :cond_8

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 123
    .line 124
    .line 125
    move-result-object p2

    .line 126
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 127
    .line 128
    .line 129
    move-result-object p2

    .line 130
    if-eqz p2, :cond_8

    .line 131
    .line 132
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 133
    .line 134
    .line 135
    move-result-object p2

    .line 136
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 137
    .line 138
    .line 139
    move-result-object p2

    .line 140
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    if-eqz p2, :cond_8

    .line 145
    .line 146
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 151
    .line 152
    .line 153
    move-result-object p2

    .line 154
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 159
    .line 160
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 161
    .line 162
    .line 163
    move-result p2

    .line 164
    rem-int/2addr p2, v2

    .line 165
    if-ne p2, v0, :cond_5

    .line 166
    .line 167
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 168
    .line 169
    .line 170
    goto :goto_2

    .line 171
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 172
    .line 173
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 174
    .line 175
    sub-int/2addr p2, p3

    .line 176
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 177
    .line 178
    .line 179
    goto :goto_2

    .line 180
    :cond_6
    :goto_0
    invoke-virtual {p1, v1}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 181
    .line 182
    .line 183
    goto :goto_2

    .line 184
    :cond_7
    :goto_1
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 185
    .line 186
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 187
    .line 188
    .line 189
    :cond_8
    :goto_2
    return-void
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processHorizontalPosition_Outside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_3

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 44
    .line 45
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 50
    .line 51
    .line 52
    move-result p2

    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    const/4 v2, 0x2

    .line 58
    rem-int/2addr v0, v2

    .line 59
    const/4 v3, 0x1

    .line 60
    if-ne v0, v3, :cond_1

    .line 61
    .line 62
    if-ne p2, v2, :cond_0

    .line 63
    .line 64
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 65
    .line 66
    iget p3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 67
    .line 68
    sub-int/2addr p2, p3

    .line 69
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_0
    if-ne p2, v3, :cond_3

    .line 74
    .line 75
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 76
    .line 77
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 78
    .line 79
    sub-int/2addr p2, p3

    .line 80
    iget p3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 81
    .line 82
    sub-int/2addr p2, p3

    .line 83
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    if-ne p2, v2, :cond_2

    .line 88
    .line 89
    const/4 p2, 0x0

    .line 90
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_2
    if-ne p2, v3, :cond_3

    .line 95
    .line 96
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 97
    .line 98
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 99
    .line 100
    .line 101
    :cond_3
    :goto_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processHorizontalPosition_Right(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getHorizontalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    const/4 v1, 0x2

    .line 10
    if-eq p2, v1, :cond_8

    .line 11
    .line 12
    const/4 v2, 0x5

    .line 13
    if-ne p2, v2, :cond_0

    .line 14
    .line 15
    goto/16 :goto_2

    .line 16
    .line 17
    :cond_0
    const/4 v2, 0x1

    .line 18
    if-eq p2, v2, :cond_7

    .line 19
    .line 20
    if-nez p2, :cond_1

    .line 21
    .line 22
    goto/16 :goto_1

    .line 23
    .line 24
    :cond_1
    const/4 v3, 0x3

    .line 25
    if-eq p2, v3, :cond_6

    .line 26
    .line 27
    const/4 v3, 0x4

    .line 28
    if-ne p2, v3, :cond_2

    .line 29
    .line 30
    goto/16 :goto_0

    .line 31
    .line 32
    :cond_2
    const/16 v3, 0x9

    .line 33
    .line 34
    if-ne p2, v3, :cond_4

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    if-eqz p2, :cond_9

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    if-eqz p2, :cond_9

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    if-eqz p2, :cond_9

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 79
    .line 80
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 81
    .line 82
    .line 83
    move-result p2

    .line 84
    rem-int/2addr p2, v1

    .line 85
    if-ne p2, v2, :cond_3

    .line 86
    .line 87
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 88
    .line 89
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 90
    .line 91
    sub-int/2addr p2, p3

    .line 92
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 93
    .line 94
    .line 95
    goto/16 :goto_3

    .line 96
    .line 97
    :cond_3
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 98
    .line 99
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 100
    .line 101
    sub-int/2addr p2, p3

    .line 102
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 103
    .line 104
    .line 105
    goto :goto_3

    .line 106
    :cond_4
    const/16 v3, 0x8

    .line 107
    .line 108
    if-ne p2, v3, :cond_9

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 111
    .line 112
    .line 113
    move-result-object p2

    .line 114
    if-eqz p2, :cond_9

    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 117
    .line 118
    .line 119
    move-result-object p2

    .line 120
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 121
    .line 122
    .line 123
    move-result-object p2

    .line 124
    if-eqz p2, :cond_9

    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 127
    .line 128
    .line 129
    move-result-object p2

    .line 130
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 131
    .line 132
    .line 133
    move-result-object p2

    .line 134
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    if-eqz p2, :cond_9

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 145
    .line 146
    .line 147
    move-result-object p2

    .line 148
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 149
    .line 150
    .line 151
    move-result-object p2

    .line 152
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 153
    .line 154
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 155
    .line 156
    .line 157
    move-result p2

    .line 158
    rem-int/2addr p2, v1

    .line 159
    if-ne p2, v2, :cond_5

    .line 160
    .line 161
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 162
    .line 163
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 164
    .line 165
    sub-int/2addr p2, p3

    .line 166
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 167
    .line 168
    .line 169
    goto :goto_3

    .line 170
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 171
    .line 172
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 173
    .line 174
    sub-int/2addr p2, p3

    .line 175
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 176
    .line 177
    .line 178
    goto :goto_3

    .line 179
    :cond_6
    :goto_0
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 180
    .line 181
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 182
    .line 183
    sub-int/2addr p2, p3

    .line 184
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 185
    .line 186
    .line 187
    goto :goto_3

    .line 188
    :cond_7
    :goto_1
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 189
    .line 190
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 191
    .line 192
    sub-int/2addr p2, p3

    .line 193
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 194
    .line 195
    sub-int/2addr p2, p3

    .line 196
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 197
    .line 198
    .line 199
    goto :goto_3

    .line 200
    :cond_8
    :goto_2
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 201
    .line 202
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 203
    .line 204
    sub-int/2addr p2, p3

    .line 205
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setX(I)V

    .line 206
    .line 207
    .line 208
    :cond_9
    :goto_3
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 6

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerPositionType()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x7

    .line 10
    const/4 v3, 0x6

    .line 11
    const/4 v4, 0x2

    .line 12
    const/4 v5, 0x1

    .line 13
    if-ne v0, v5, :cond_6

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerRelativeValue()I

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    int-to-float p2, p2

    .line 20
    const/high16 v0, 0x447a0000    # 1000.0f

    .line 21
    .line 22
    div-float/2addr p2, v0

    .line 23
    if-ne v1, v4, :cond_0

    .line 24
    .line 25
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 26
    .line 27
    int-to-float p3, p3

    .line 28
    mul-float p3, p3, p2

    .line 29
    .line 30
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 31
    .line 32
    .line 33
    move-result p2

    .line 34
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 35
    .line 36
    .line 37
    goto/16 :goto_0

    .line 38
    .line 39
    :cond_0
    if-ne v1, v5, :cond_1

    .line 40
    .line 41
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 42
    .line 43
    iget v1, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 44
    .line 45
    sub-int/2addr v1, v0

    .line 46
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 47
    .line 48
    sub-int/2addr v1, p3

    .line 49
    int-to-float p3, v1

    .line 50
    mul-float p3, p3, p2

    .line 51
    .line 52
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    add-int/2addr v0, p2

    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 58
    .line 59
    .line 60
    goto/16 :goto_0

    .line 61
    .line 62
    :cond_1
    if-ne v1, v3, :cond_2

    .line 63
    .line 64
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 65
    .line 66
    int-to-float p3, p3

    .line 67
    mul-float p3, p3, p2

    .line 68
    .line 69
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 70
    .line 71
    .line 72
    move-result p2

    .line 73
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 74
    .line 75
    .line 76
    goto/16 :goto_0

    .line 77
    .line 78
    :cond_2
    if-ne v1, v2, :cond_3

    .line 79
    .line 80
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 81
    .line 82
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 83
    .line 84
    sub-int/2addr v0, p3

    .line 85
    int-to-float p3, p3

    .line 86
    mul-float p3, p3, p2

    .line 87
    .line 88
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 89
    .line 90
    .line 91
    move-result p2

    .line 92
    add-int/2addr v0, p2

    .line 93
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 94
    .line 95
    .line 96
    goto/16 :goto_0

    .line 97
    .line 98
    :cond_3
    const/16 v0, 0x9

    .line 99
    .line 100
    if-eq v1, v0, :cond_4

    .line 101
    .line 102
    const/16 v0, 0x8

    .line 103
    .line 104
    if-ne v1, v0, :cond_c

    .line 105
    .line 106
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    if-eqz v0, :cond_c

    .line 111
    .line 112
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    if-eqz v0, :cond_c

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    if-eqz v0, :cond_c

    .line 135
    .line 136
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-interface {v0}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    check-cast v0, Lcom/intsig/office/wp/view/PageView;

    .line 149
    .line 150
    invoke-virtual {v0}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 151
    .line 152
    .line 153
    move-result v0

    .line 154
    rem-int/2addr v0, v4

    .line 155
    if-ne v0, v5, :cond_5

    .line 156
    .line 157
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 158
    .line 159
    int-to-float p3, p3

    .line 160
    mul-float p3, p3, p2

    .line 161
    .line 162
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 163
    .line 164
    .line 165
    move-result p2

    .line 166
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 167
    .line 168
    .line 169
    goto :goto_0

    .line 170
    :cond_5
    iget v0, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 171
    .line 172
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 173
    .line 174
    sub-int/2addr v0, p3

    .line 175
    int-to-float p3, p3

    .line 176
    mul-float p3, p3, p2

    .line 177
    .line 178
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    .line 179
    .line 180
    .line 181
    move-result p2

    .line 182
    add-int/2addr v0, p2

    .line 183
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 184
    .line 185
    .line 186
    goto :goto_0

    .line 187
    :cond_6
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalAlignment()B

    .line 188
    .line 189
    .line 190
    move-result v0

    .line 191
    if-nez v0, :cond_7

    .line 192
    .line 193
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Absolute(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 194
    .line 195
    .line 196
    goto :goto_0

    .line 197
    :cond_7
    const/4 v1, 0x4

    .line 198
    if-ne v0, v1, :cond_8

    .line 199
    .line 200
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Top(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 201
    .line 202
    .line 203
    goto :goto_0

    .line 204
    :cond_8
    if-ne v0, v4, :cond_9

    .line 205
    .line 206
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Center(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 207
    .line 208
    .line 209
    goto :goto_0

    .line 210
    :cond_9
    const/4 v1, 0x5

    .line 211
    if-ne v0, v1, :cond_a

    .line 212
    .line 213
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Bottom(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 214
    .line 215
    .line 216
    goto :goto_0

    .line 217
    :cond_a
    if-ne v0, v3, :cond_b

    .line 218
    .line 219
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Inside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 220
    .line 221
    .line 222
    goto :goto_0

    .line 223
    :cond_b
    if-ne v0, v2, :cond_c

    .line 224
    .line 225
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition_Outside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 226
    .line 227
    .line 228
    :cond_c
    :goto_0
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Absolute(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    const/4 v1, 0x2

    .line 10
    if-eq p2, v1, :cond_7

    .line 11
    .line 12
    const/4 v2, 0x6

    .line 13
    if-ne p2, v2, :cond_0

    .line 14
    .line 15
    goto/16 :goto_2

    .line 16
    .line 17
    :cond_0
    const/16 v2, 0x8

    .line 18
    .line 19
    const/4 v3, 0x1

    .line 20
    if-eq p2, v2, :cond_5

    .line 21
    .line 22
    const/16 v2, 0x9

    .line 23
    .line 24
    if-ne p2, v2, :cond_1

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    if-ne p2, v3, :cond_2

    .line 28
    .line 29
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 30
    .line 31
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 32
    .line 33
    add-int/2addr p2, p3

    .line 34
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 35
    .line 36
    .line 37
    goto/16 :goto_3

    .line 38
    .line 39
    :cond_2
    const/16 v1, 0xa

    .line 40
    .line 41
    if-eq p2, v1, :cond_4

    .line 42
    .line 43
    const/16 v1, 0xb

    .line 44
    .line 45
    if-ne p2, v1, :cond_3

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_3
    const/4 v1, 0x7

    .line 49
    if-ne p2, v1, :cond_8

    .line 50
    .line 51
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 52
    .line 53
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 54
    .line 55
    sub-int/2addr p2, p3

    .line 56
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 57
    .line 58
    add-int/2addr p2, p3

    .line 59
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 60
    .line 61
    .line 62
    goto/16 :goto_3

    .line 63
    .line 64
    :cond_4
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 65
    .line 66
    .line 67
    move-result-object p2

    .line 68
    if-eqz p2, :cond_8

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    instance-of p2, p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 79
    .line 80
    if-eqz p2, :cond_8

    .line 81
    .line 82
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 91
    .line 92
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 93
    .line 94
    .line 95
    move-result p2

    .line 96
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 97
    .line 98
    add-int/2addr p2, p3

    .line 99
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 100
    .line 101
    .line 102
    goto :goto_3

    .line 103
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    if-eqz p2, :cond_8

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    if-eqz p2, :cond_8

    .line 118
    .line 119
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 120
    .line 121
    .line 122
    move-result-object p2

    .line 123
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 124
    .line 125
    .line 126
    move-result-object p2

    .line 127
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 128
    .line 129
    .line 130
    move-result-object p2

    .line 131
    if-eqz p2, :cond_8

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 134
    .line 135
    .line 136
    move-result-object p2

    .line 137
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 138
    .line 139
    .line 140
    move-result-object p2

    .line 141
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 142
    .line 143
    .line 144
    move-result-object p2

    .line 145
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 146
    .line 147
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 148
    .line 149
    .line 150
    move-result p2

    .line 151
    rem-int/2addr p2, v1

    .line 152
    if-ne p2, v3, :cond_6

    .line 153
    .line 154
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 155
    .line 156
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 157
    .line 158
    .line 159
    goto :goto_3

    .line 160
    :cond_6
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 161
    .line 162
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 163
    .line 164
    sub-int/2addr p2, p3

    .line 165
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 166
    .line 167
    add-int/2addr p2, p3

    .line 168
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 169
    .line 170
    .line 171
    goto :goto_3

    .line 172
    :cond_7
    :goto_2
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 173
    .line 174
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 175
    .line 176
    .line 177
    :cond_8
    :goto_3
    return-void
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Bottom(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    const/4 v1, 0x2

    .line 10
    if-eq p2, v1, :cond_7

    .line 11
    .line 12
    const/4 v2, 0x7

    .line 13
    if-ne p2, v2, :cond_0

    .line 14
    .line 15
    goto/16 :goto_1

    .line 16
    .line 17
    :cond_0
    const/4 v2, 0x1

    .line 18
    if-ne p2, v2, :cond_1

    .line 19
    .line 20
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 21
    .line 22
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 23
    .line 24
    sub-int/2addr p2, p3

    .line 25
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 26
    .line 27
    sub-int/2addr p2, p3

    .line 28
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 29
    .line 30
    .line 31
    goto/16 :goto_2

    .line 32
    .line 33
    :cond_1
    const/16 v3, 0xa

    .line 34
    .line 35
    if-eq p2, v3, :cond_6

    .line 36
    .line 37
    const/16 v3, 0xb

    .line 38
    .line 39
    if-ne p2, v3, :cond_2

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    const/4 v3, 0x6

    .line 43
    if-ne p2, v3, :cond_3

    .line 44
    .line 45
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 46
    .line 47
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 48
    .line 49
    sub-int/2addr p2, p3

    .line 50
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_2

    .line 54
    .line 55
    :cond_3
    const/16 v3, 0x8

    .line 56
    .line 57
    if-eq p2, v3, :cond_4

    .line 58
    .line 59
    const/16 v3, 0x9

    .line 60
    .line 61
    if-ne p2, v3, :cond_8

    .line 62
    .line 63
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    if-eqz p2, :cond_8

    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    if-eqz p2, :cond_8

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    if-eqz p2, :cond_8

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 106
    .line 107
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 108
    .line 109
    .line 110
    move-result p2

    .line 111
    rem-int/2addr p2, v1

    .line 112
    if-ne p2, v2, :cond_5

    .line 113
    .line 114
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 115
    .line 116
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 117
    .line 118
    sub-int/2addr p2, p3

    .line 119
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 120
    .line 121
    .line 122
    goto :goto_2

    .line 123
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 124
    .line 125
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 126
    .line 127
    sub-int/2addr p2, p3

    .line 128
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 129
    .line 130
    .line 131
    goto :goto_2

    .line 132
    :cond_6
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 133
    .line 134
    .line 135
    move-result-object p2

    .line 136
    if-eqz p2, :cond_8

    .line 137
    .line 138
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 139
    .line 140
    .line 141
    move-result-object p2

    .line 142
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 143
    .line 144
    .line 145
    move-result-object p2

    .line 146
    instance-of p2, p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 147
    .line 148
    if-eqz p2, :cond_8

    .line 149
    .line 150
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 151
    .line 152
    .line 153
    move-result-object p2

    .line 154
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 159
    .line 160
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 161
    .line 162
    .line 163
    move-result p3

    .line 164
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 165
    .line 166
    .line 167
    move-result p2

    .line 168
    add-int/2addr p3, p2

    .line 169
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 170
    .line 171
    sub-int/2addr p3, p2

    .line 172
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 173
    .line 174
    .line 175
    goto :goto_2

    .line 176
    :cond_7
    :goto_1
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 177
    .line 178
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 179
    .line 180
    sub-int/2addr p2, p3

    .line 181
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 182
    .line 183
    .line 184
    :cond_8
    :goto_2
    return-void
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Center(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    div-int/2addr v0, v1

    .line 13
    if-ne p2, v1, :cond_0

    .line 14
    .line 15
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 16
    .line 17
    div-int/2addr p2, v1

    .line 18
    sub-int/2addr p2, v0

    .line 19
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 20
    .line 21
    .line 22
    goto/16 :goto_1

    .line 23
    .line 24
    :cond_0
    const/4 v2, 0x1

    .line 25
    if-ne p2, v2, :cond_1

    .line 26
    .line 27
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 28
    .line 29
    iget v2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 30
    .line 31
    sub-int/2addr v2, p2

    .line 32
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 33
    .line 34
    sub-int/2addr v2, p3

    .line 35
    div-int/2addr v2, v1

    .line 36
    add-int/2addr p2, v2

    .line 37
    sub-int/2addr p2, v0

    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 39
    .line 40
    .line 41
    goto/16 :goto_1

    .line 42
    .line 43
    :cond_1
    const/4 v3, 0x6

    .line 44
    if-ne p2, v3, :cond_2

    .line 45
    .line 46
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 47
    .line 48
    div-int/2addr p2, v1

    .line 49
    sub-int/2addr p2, v0

    .line 50
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_1

    .line 54
    .line 55
    :cond_2
    const/16 v3, 0x8

    .line 56
    .line 57
    if-eq p2, v3, :cond_6

    .line 58
    .line 59
    const/16 v3, 0x9

    .line 60
    .line 61
    if-ne p2, v3, :cond_3

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_3
    const/4 v2, 0x7

    .line 65
    if-ne p2, v2, :cond_4

    .line 66
    .line 67
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 68
    .line 69
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 70
    .line 71
    div-int/2addr p3, v1

    .line 72
    sub-int/2addr p2, p3

    .line 73
    sub-int/2addr p2, v0

    .line 74
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_1

    .line 78
    .line 79
    :cond_4
    const/16 p3, 0xa

    .line 80
    .line 81
    if-eq p2, p3, :cond_5

    .line 82
    .line 83
    const/16 p3, 0xb

    .line 84
    .line 85
    if-ne p2, p3, :cond_8

    .line 86
    .line 87
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    if-eqz p2, :cond_8

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    instance-of p2, p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 102
    .line 103
    if-eqz p2, :cond_8

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 114
    .line 115
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 116
    .line 117
    .line 118
    move-result p2

    .line 119
    sub-int/2addr p2, v0

    .line 120
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_6
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 125
    .line 126
    .line 127
    move-result-object p2

    .line 128
    if-eqz p2, :cond_8

    .line 129
    .line 130
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 131
    .line 132
    .line 133
    move-result-object p2

    .line 134
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    if-eqz p2, :cond_8

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 145
    .line 146
    .line 147
    move-result-object p2

    .line 148
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 149
    .line 150
    .line 151
    move-result-object p2

    .line 152
    if-eqz p2, :cond_8

    .line 153
    .line 154
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 159
    .line 160
    .line 161
    move-result-object p2

    .line 162
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 167
    .line 168
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 169
    .line 170
    .line 171
    move-result p2

    .line 172
    rem-int/2addr p2, v1

    .line 173
    if-ne p2, v2, :cond_7

    .line 174
    .line 175
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 176
    .line 177
    div-int/2addr p2, v1

    .line 178
    sub-int/2addr p2, v0

    .line 179
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 180
    .line 181
    .line 182
    goto :goto_1

    .line 183
    :cond_7
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 184
    .line 185
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 186
    .line 187
    div-int/2addr p3, v1

    .line 188
    sub-int/2addr p2, p3

    .line 189
    sub-int/2addr p2, v0

    .line 190
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 191
    .line 192
    .line 193
    :cond_8
    :goto_1
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Inside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 10

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_f

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_f

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    if-eqz v1, :cond_f

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    check-cast v1, Lcom/intsig/office/wp/view/PageView;

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    const/4 v2, 0x2

    .line 58
    rem-int/2addr v1, v2

    .line 59
    const/16 v3, 0x9

    .line 60
    .line 61
    const/16 v4, 0x8

    .line 62
    .line 63
    const/4 v5, 0x7

    .line 64
    const/4 v6, 0x6

    .line 65
    const/16 v7, 0xb

    .line 66
    .line 67
    const/16 v8, 0xa

    .line 68
    .line 69
    const/4 v9, 0x1

    .line 70
    if-ne v1, v9, :cond_7

    .line 71
    .line 72
    if-ne p2, v2, :cond_0

    .line 73
    .line 74
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->headerMargin:I

    .line 75
    .line 76
    div-int/2addr p2, v2

    .line 77
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 78
    .line 79
    .line 80
    goto/16 :goto_2

    .line 81
    .line 82
    :cond_0
    if-ne p2, v9, :cond_1

    .line 83
    .line 84
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 85
    .line 86
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 87
    .line 88
    .line 89
    goto/16 :goto_2

    .line 90
    .line 91
    :cond_1
    if-eq p2, v8, :cond_6

    .line 92
    .line 93
    if-ne p2, v7, :cond_2

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_2
    const/4 v0, 0x0

    .line 97
    if-ne p2, v6, :cond_3

    .line 98
    .line 99
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 100
    .line 101
    .line 102
    goto/16 :goto_2

    .line 103
    .line 104
    :cond_3
    if-ne p2, v5, :cond_4

    .line 105
    .line 106
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 107
    .line 108
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 109
    .line 110
    sub-int/2addr p2, p3

    .line 111
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_2

    .line 115
    .line 116
    :cond_4
    if-eq p2, v4, :cond_5

    .line 117
    .line 118
    if-ne p2, v3, :cond_f

    .line 119
    .line 120
    :cond_5
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 121
    .line 122
    .line 123
    goto/16 :goto_2

    .line 124
    .line 125
    :cond_6
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 126
    .line 127
    .line 128
    move-result-object p2

    .line 129
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 130
    .line 131
    .line 132
    move-result-object p2

    .line 133
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 134
    .line 135
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 136
    .line 137
    .line 138
    move-result p2

    .line 139
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 140
    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_7
    if-ne p2, v2, :cond_8

    .line 144
    .line 145
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 146
    .line 147
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->footerMargin:I

    .line 148
    .line 149
    sub-int/2addr p2, p3

    .line 150
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :cond_8
    if-ne p2, v9, :cond_9

    .line 155
    .line 156
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 157
    .line 158
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 159
    .line 160
    sub-int/2addr p2, p3

    .line 161
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 162
    .line 163
    sub-int/2addr p2, p3

    .line 164
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 165
    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_9
    if-eq p2, v8, :cond_e

    .line 169
    .line 170
    if-ne p2, v7, :cond_a

    .line 171
    .line 172
    goto :goto_1

    .line 173
    :cond_a
    if-ne p2, v6, :cond_b

    .line 174
    .line 175
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 176
    .line 177
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 178
    .line 179
    sub-int/2addr p2, p3

    .line 180
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 181
    .line 182
    .line 183
    goto :goto_2

    .line 184
    :cond_b
    if-ne p2, v5, :cond_c

    .line 185
    .line 186
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 187
    .line 188
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 189
    .line 190
    sub-int/2addr p2, p3

    .line 191
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 192
    .line 193
    .line 194
    goto :goto_2

    .line 195
    :cond_c
    if-eq p2, v4, :cond_d

    .line 196
    .line 197
    if-ne p2, v3, :cond_f

    .line 198
    .line 199
    :cond_d
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 200
    .line 201
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 202
    .line 203
    sub-int/2addr p2, p3

    .line 204
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 205
    .line 206
    .line 207
    goto :goto_2

    .line 208
    :cond_e
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 209
    .line 210
    .line 211
    move-result-object p2

    .line 212
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 213
    .line 214
    .line 215
    move-result-object p2

    .line 216
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 217
    .line 218
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 219
    .line 220
    .line 221
    move-result p3

    .line 222
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 223
    .line 224
    .line 225
    move-result p2

    .line 226
    add-int/2addr p3, p2

    .line 227
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 228
    .line 229
    sub-int/2addr p3, p2

    .line 230
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 231
    .line 232
    .line 233
    :cond_f
    :goto_2
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Outside(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 10

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_f

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_f

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    if-eqz v1, :cond_f

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-interface {v1}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    check-cast v1, Lcom/intsig/office/wp/view/PageView;

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    const/4 v2, 0x2

    .line 58
    rem-int/2addr v1, v2

    .line 59
    const/16 v3, 0x9

    .line 60
    .line 61
    const/16 v4, 0x8

    .line 62
    .line 63
    const/4 v5, 0x7

    .line 64
    const/4 v6, 0x6

    .line 65
    const/16 v7, 0xb

    .line 66
    .line 67
    const/16 v8, 0xa

    .line 68
    .line 69
    const/4 v9, 0x1

    .line 70
    if-ne v1, v9, :cond_7

    .line 71
    .line 72
    if-ne p2, v2, :cond_0

    .line 73
    .line 74
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 75
    .line 76
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->footerMargin:I

    .line 77
    .line 78
    sub-int/2addr p2, p3

    .line 79
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 80
    .line 81
    .line 82
    goto/16 :goto_2

    .line 83
    .line 84
    :cond_0
    if-ne p2, v9, :cond_1

    .line 85
    .line 86
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 87
    .line 88
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 89
    .line 90
    sub-int/2addr p2, p3

    .line 91
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 92
    .line 93
    sub-int/2addr p2, p3

    .line 94
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 95
    .line 96
    .line 97
    goto/16 :goto_2

    .line 98
    .line 99
    :cond_1
    if-eq p2, v8, :cond_6

    .line 100
    .line 101
    if-ne p2, v7, :cond_2

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_2
    if-ne p2, v6, :cond_3

    .line 105
    .line 106
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 107
    .line 108
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 109
    .line 110
    sub-int/2addr p2, p3

    .line 111
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_2

    .line 115
    .line 116
    :cond_3
    if-ne p2, v5, :cond_4

    .line 117
    .line 118
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 119
    .line 120
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 121
    .line 122
    sub-int/2addr p2, p3

    .line 123
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 124
    .line 125
    .line 126
    goto/16 :goto_2

    .line 127
    .line 128
    :cond_4
    if-eq p2, v4, :cond_5

    .line 129
    .line 130
    if-ne p2, v3, :cond_f

    .line 131
    .line 132
    :cond_5
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 133
    .line 134
    iget p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 135
    .line 136
    sub-int/2addr p2, p3

    .line 137
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 138
    .line 139
    .line 140
    goto :goto_2

    .line 141
    :cond_6
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 142
    .line 143
    .line 144
    move-result-object p2

    .line 145
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 146
    .line 147
    .line 148
    move-result-object p2

    .line 149
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 150
    .line 151
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 152
    .line 153
    .line 154
    move-result p3

    .line 155
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getHeight()I

    .line 156
    .line 157
    .line 158
    move-result p2

    .line 159
    add-int/2addr p3, p2

    .line 160
    iget p2, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 161
    .line 162
    sub-int/2addr p3, p2

    .line 163
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 164
    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_7
    if-ne p2, v2, :cond_8

    .line 168
    .line 169
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->headerMargin:I

    .line 170
    .line 171
    div-int/2addr p2, v2

    .line 172
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 173
    .line 174
    .line 175
    goto :goto_2

    .line 176
    :cond_8
    if-ne p2, v9, :cond_9

    .line 177
    .line 178
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 179
    .line 180
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 181
    .line 182
    .line 183
    goto :goto_2

    .line 184
    :cond_9
    if-eq p2, v8, :cond_e

    .line 185
    .line 186
    if-ne p2, v7, :cond_a

    .line 187
    .line 188
    goto :goto_1

    .line 189
    :cond_a
    if-ne p2, v6, :cond_b

    .line 190
    .line 191
    const/4 p2, 0x0

    .line 192
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 193
    .line 194
    .line 195
    goto :goto_2

    .line 196
    :cond_b
    if-ne p2, v5, :cond_c

    .line 197
    .line 198
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 199
    .line 200
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 201
    .line 202
    sub-int/2addr p2, p3

    .line 203
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 204
    .line 205
    .line 206
    goto :goto_2

    .line 207
    :cond_c
    if-eq p2, v4, :cond_d

    .line 208
    .line 209
    if-ne p2, v3, :cond_f

    .line 210
    .line 211
    :cond_d
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 212
    .line 213
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 214
    .line 215
    sub-int/2addr p2, p3

    .line 216
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 217
    .line 218
    .line 219
    goto :goto_2

    .line 220
    :cond_e
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 221
    .line 222
    .line 223
    move-result-object p2

    .line 224
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 225
    .line 226
    .line 227
    move-result-object p2

    .line 228
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 229
    .line 230
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 231
    .line 232
    .line 233
    move-result p2

    .line 234
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 235
    .line 236
    .line 237
    :cond_f
    :goto_2
    return-void
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private processVerticalPosition_Top(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAutoShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/WPAbstractShape;->getVerticalRelativeTo()B

    .line 5
    .line 6
    .line 7
    move-result p2

    .line 8
    const/4 v0, 0x0

    .line 9
    const/4 v1, 0x2

    .line 10
    if-eq p2, v1, :cond_7

    .line 11
    .line 12
    const/4 v2, 0x6

    .line 13
    if-ne p2, v2, :cond_0

    .line 14
    .line 15
    goto/16 :goto_2

    .line 16
    .line 17
    :cond_0
    const/16 v2, 0x8

    .line 18
    .line 19
    const/4 v3, 0x1

    .line 20
    if-eq p2, v2, :cond_5

    .line 21
    .line 22
    const/16 v2, 0x9

    .line 23
    .line 24
    if-ne p2, v2, :cond_1

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    if-ne p2, v3, :cond_2

    .line 28
    .line 29
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 30
    .line 31
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 32
    .line 33
    .line 34
    goto/16 :goto_3

    .line 35
    .line 36
    :cond_2
    const/16 v0, 0xa

    .line 37
    .line 38
    if-eq p2, v0, :cond_4

    .line 39
    .line 40
    const/16 v0, 0xb

    .line 41
    .line 42
    if-ne p2, v0, :cond_3

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_3
    const/4 v0, 0x7

    .line 46
    if-ne p2, v0, :cond_8

    .line 47
    .line 48
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 49
    .line 50
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 51
    .line 52
    sub-int/2addr p2, p3

    .line 53
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 54
    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_4
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    if-eqz p2, :cond_8

    .line 62
    .line 63
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    instance-of p2, p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 72
    .line 73
    if-eqz p2, :cond_8

    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    check-cast p2, Lcom/intsig/office/wp/view/ParagraphView;

    .line 84
    .line 85
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/AbstractView;->getY()I

    .line 86
    .line 87
    .line 88
    move-result p2

    .line 89
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 90
    .line 91
    .line 92
    goto :goto_3

    .line 93
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    if-eqz p2, :cond_8

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 100
    .line 101
    .line 102
    move-result-object p2

    .line 103
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    if-eqz p2, :cond_8

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    if-eqz p2, :cond_8

    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/AbstractView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 124
    .line 125
    .line 126
    move-result-object p2

    .line 127
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 128
    .line 129
    .line 130
    move-result-object p2

    .line 131
    invoke-interface {p2}, Lcom/intsig/office/simpletext/view/IView;->getParentView()Lcom/intsig/office/simpletext/view/IView;

    .line 132
    .line 133
    .line 134
    move-result-object p2

    .line 135
    check-cast p2, Lcom/intsig/office/wp/view/PageView;

    .line 136
    .line 137
    invoke-virtual {p2}, Lcom/intsig/office/wp/view/PageView;->getPageNumber()I

    .line 138
    .line 139
    .line 140
    move-result p2

    .line 141
    rem-int/2addr p2, v1

    .line 142
    if-ne p2, v3, :cond_6

    .line 143
    .line 144
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 145
    .line 146
    .line 147
    goto :goto_3

    .line 148
    :cond_6
    iget p2, p3, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 149
    .line 150
    iget p3, p3, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 151
    .line 152
    sub-int/2addr p2, p3

    .line 153
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 154
    .line 155
    .line 156
    goto :goto_3

    .line 157
    :cond_7
    :goto_2
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/view/AbstractView;->setY(I)V

    .line 158
    .line 159
    .line 160
    :cond_8
    :goto_3
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public processShapePosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processHorizontalPosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/wp/view/PositionLayoutKit;->processVerticalPosition(Lcom/intsig/office/wp/view/LeafView;Lcom/intsig/office/common/shape/WPAutoShape;Lcom/intsig/office/simpletext/view/PageAttr;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public Lcom/intsig/office/res/ResConstant;
.super Ljava/lang/Object;
.source "ResConstant.java"


# static fields
.field public static final DIALOG_CANNOT_ENCRYPTED_FILE:Ljava/lang/String; = "Cannot process encrypted file"

.field public static final DIALOG_ENCODING_TITLE:Ljava/lang/String; = "Text Encoding"

.field public static final DIALOG_FORMAT_ERROR:Ljava/lang/String; = "Bad file"

.field public static final DIALOG_OLD_DOCUMENT:Ljava/lang/String; = "The document is too old - Office 95 or older, which is not supported"

.field public static final DIALOG_PARSE_ERROR:Ljava/lang/String; = "File parsing error"

.field public static final DIALOG_PASSWORD_INCORRECT:Ljava/lang/String; = "Password is incorrect!"

.field public static final DIALOG_RTF_FILE:Ljava/lang/String; = "The document is really a RTF file, which is not supported"

.field public static final DIALOG_SYSTEM_CRASH:Ljava/lang/String; = "System crash, terminate running"

.field public static final SD_CARD_ERROR:Ljava/lang/String; = "SD Card Error"

.field public static final SD_CARD_NOSPACELEFT:Ljava/lang/String; = "SD Card has no space left"

.field public static final SD_CARD_WRITEDENIED:Ljava/lang/String; = "SD Card write permission denied"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

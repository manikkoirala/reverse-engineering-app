.class public interface abstract Lcom/intsig/office/system/IReader;
.super Ljava/lang/Object;
.source "IReader.java"


# virtual methods
.method public abstract abortReader()V
.end method

.method public abstract backReader()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract dispose()V
.end method

.method public abstract getControl()Lcom/intsig/office/system/IControl;
.end method

.method public abstract getModel()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract isAborted()Z
.end method

.method public abstract isReaderFinish()Z
.end method

.method public abstract searchContent(Ljava/io/File;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

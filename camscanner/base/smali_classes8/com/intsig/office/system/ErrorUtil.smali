.class public Lcom/intsig/office/system/ErrorUtil;
.super Ljava/lang/Object;
.source "ErrorUtil.java"


# static fields
.field public static final BAD_FILE:I = 0x2

.field public static final INSUFFICIENT_MEMORY:I = 0x0

.field public static final OLD_DOCUMENT:I = 0x3

.field public static final PARSE_ERROR:I = 0x4

.field public static final PASSWORD_DOCUMENT:I = 0x6

.field public static final PASSWORD_INCORRECT:I = 0x7

.field public static final RTF_DOCUMENT:I = 0x5

.field public static final SD_CARD_ERROR:I = 0x8

.field public static final SD_CARD_NOSPACELEFT:I = 0xa

.field public static final SD_CARD_WRITEDENIED:I = 0x9

.field public static final SYSTEM_CRASH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ErrorUtil"

.field private static final VERSION:Ljava/lang/String; = "2.0.0.4"

.field private static final sdf_24:Ljava/text/SimpleDateFormat;


# instance fields
.field private logFile:Ljava/io/File;

.field private message:Landroid/app/AlertDialog;

.field private sysKit:Lcom/intsig/office/system/SysKit;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 2
    .line 3
    const-string v1, "yyyy-MM-dd HH:mm:ss"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/system/ErrorUtil;->sdf_24:Ljava/text/SimpleDateFormat;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Lcom/intsig/office/system/SysKit;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getControl()Lcom/intsig/office/system/IControl;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->isWriteLog()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getControl()Lcom/intsig/office/system/IControl;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getTemporaryDirectory()Ljava/io/File;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 33
    .line 34
    if-eqz p1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 43
    .line 44
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_1

    .line 49
    .line 50
    new-instance p1, Ljava/io/File;

    .line 51
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 58
    .line 59
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    sget-char v1, Ljava/io/File;->separatorChar:C

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string v1, "ASReader"

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    iput-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    if-nez p1, :cond_0

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 92
    .line 93
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 94
    .line 95
    .line 96
    :cond_0
    new-instance p1, Ljava/io/File;

    .line 97
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    iget-object v1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 104
    .line 105
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    sget-char v1, Ljava/io/File;->separatorChar:C

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v1, "errorLog.txt"

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    iput-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    .line 130
    .line 131
    nop

    .line 132
    :cond_1
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processThrowable(Ljava/lang/Throwable;Z)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getControl()Lcom/intsig/office/system/IControl;

    .line 4
    .line 5
    .line 6
    move-result-object v6

    .line 7
    invoke-interface {v6}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    if-nez v4, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    invoke-interface {v6}, Lcom/intsig/office/system/IControl;->isAutoTest()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    invoke-static {p1}, Ljava/lang/System;->exit(I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->message:Landroid/app/AlertDialog;

    .line 30
    .line 31
    if-nez v0, :cond_2

    .line 32
    .line 33
    invoke-interface {v6}, Lcom/intsig/office/system/IControl;->getActivity()Landroid/app/Activity;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    new-instance v7, Lcom/intsig/office/system/ErrorUtil$1;

    .line 46
    .line 47
    move-object v1, v7

    .line 48
    move-object v2, p0

    .line 49
    move-object v3, p1

    .line 50
    move v5, p2

    .line 51
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/system/ErrorUtil$1;-><init>(Lcom/intsig/office/system/ErrorUtil;Ljava/lang/Throwable;Landroid/app/Activity;ZLcom/intsig/office/system/IControl;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v7}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 55
    .line 56
    .line 57
    :cond_2
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/system/ErrorUtil;)Landroid/app/AlertDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/system/ErrorUtil;->message:Landroid/app/AlertDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/system/ErrorUtil;)Lcom/intsig/office/system/SysKit;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/system/ErrorUtil;Landroid/app/AlertDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->message:Landroid/app/AlertDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writerLog(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    return-void
.end method

.method public writerLog(Ljava/lang/Throwable;Z)V
    .locals 1

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;ZZ)V

    return-void
.end method

.method public writerLog(Ljava/lang/Throwable;ZZ)V
    .locals 5

    .line 3
    :try_start_0
    instance-of v0, p1, Lcom/intsig/office/system/AbortReaderError;

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    if-nez v0, :cond_1

    .line 5
    new-instance p1, Ljava/lang/Throwable;

    const-string v0, "SD CARD ERROR"

    invoke-direct {p1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 6
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7
    new-instance p1, Ljava/lang/Throwable;

    const-string v0, "Write Permission denied"

    invoke-direct {p1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 8
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    move-result-object v0

    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->isWriteLog()Z

    move-result v0

    if-eqz v0, :cond_3

    instance-of v0, p1, Ljava/lang/OutOfMemoryError;

    if-nez v0, :cond_3

    .line 9
    new-instance v0, Ljava/io/FileWriter;

    iget-object v1, p0, Lcom/intsig/office/system/ErrorUtil;->logFile:Ljava/io/File;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    .line 10
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    .line 11
    invoke-virtual {v1}, Ljava/io/PrintWriter;->println()V

    const-string v2, "--------------------------------------------------------------------------"

    .line 12
    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurs: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/intsig/office/system/ErrorUtil;->sdf_24:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "2.0.0.4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 15
    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    :cond_3
    :goto_0
    if-eqz p3, :cond_4

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/system/ErrorUtil;->processThrowable(Ljava/lang/Throwable;Z)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    return-void

    .line 17
    :catch_1
    iget-object p1, p0, Lcom/intsig/office/system/ErrorUtil;->sysKit:Lcom/intsig/office/system/SysKit;

    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getControl()Lcom/intsig/office/system/IControl;

    move-result-object p1

    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    move-result-object p1

    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->onBackPressed()V

    :cond_4
    :goto_1
    return-void
.end method

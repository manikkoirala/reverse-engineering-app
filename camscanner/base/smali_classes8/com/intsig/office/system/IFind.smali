.class public interface abstract Lcom/intsig/office/system/IFind;
.super Ljava/lang/Object;
.source "IFind.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract find(Ljava/lang/String;)Z
.end method

.method public abstract findBackward()Z
.end method

.method public abstract findForward()Z
.end method

.method public abstract getPageIndex()I
.end method

.method public abstract removeSearch()V
.end method

.method public abstract resetSearchResult()V
.end method

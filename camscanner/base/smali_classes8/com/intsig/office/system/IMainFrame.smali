.class public interface abstract Lcom/intsig/office/system/IMainFrame;
.super Ljava/lang/Object;
.source "IMainFrame.java"


# static fields
.field public static final ON_CLICK:B = 0xat

.field public static final ON_DOUBLE_TAP:B = 0x8t

.field public static final ON_DOUBLE_TAP_EVENT:B = 0x9t

.field public static final ON_DOWN:B = 0x1t

.field public static final ON_FLING:B = 0x6t

.field public static final ON_HYPER_LINK_BOOK_MARK:B = 0xft

.field public static final ON_HYPER_LINK_URL:B = 0xet

.field public static final ON_LONG_PRESS:B = 0x5t

.field public static final ON_SCROLL:B = 0x4t

.field public static final ON_SHOW_PRESS:B = 0x2t

.field public static final ON_SINGLE_TAP_CONFIRMED:B = 0x7t

.field public static final ON_SINGLE_TAP_UP:B = 0x3t

.field public static final ON_TOUCH:B = 0x0t

.field public static final ON_ZOOM_CHANGE:B = 0xdt

.field public static final ON_ZOOM_END:B = 0xct

.field public static final ON_ZOOM_START:B = 0xbt


# virtual methods
.method public abstract changeZoom(I)V
.end method

.method public abstract dispose()V
.end method

.method public abstract doActionEvent(ILjava/lang/Object;)Z
.end method

.method public abstract fullScreen(Z)V
.end method

.method public abstract getAppName()Ljava/lang/String;
.end method

.method public abstract getBottomBarHeight()I
.end method

.method public abstract getCurActivity()Landroid/app/Activity;
.end method

.method public abstract getLocalString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getPageListViewMovingPosition()B
.end method

.method public abstract getTXTDefaultEncode()Ljava/lang/String;
.end method

.method public abstract getTemporaryDirectory()Ljava/io/File;
.end method

.method public abstract getViewBackground()Ljava/lang/Object;
.end method

.method public abstract getWordDefaultView()B
.end method

.method public abstract hidePDFScroll()V
.end method

.method public abstract isChangePage()Z
.end method

.method public abstract isDrawPageNumber()Z
.end method

.method public abstract isIgnoreOriginalSize()Z
.end method

.method public abstract isPopUpErrorDlg()Z
.end method

.method public abstract isShowProgressBar()Z
.end method

.method public abstract isShowTXTEncodeDlg()Z
.end method

.method public abstract isTouchZoom()Z
.end method

.method public abstract isWriteLog()Z
.end method

.method public abstract isZoomAfterLayoutForWord()Z
.end method

.method public abstract onError(I)V
.end method

.method public abstract onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z
.end method

.method public abstract onLoadComplete(I)V
.end method

.method public abstract onWordScrollPercentY(F)V
.end method

.method public abstract openFileFinish()V
.end method

.method public abstract pageChanged(II)V
.end method

.method public abstract setFindBackForwardState(Z)V
.end method

.method public abstract showProgressBar(Z)V
.end method

.method public abstract tap(Landroid/view/MotionEvent;)Z
.end method

.method public abstract touchPDFScroll()V
.end method

.method public abstract updateViewImages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

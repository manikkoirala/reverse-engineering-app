.class public Lcom/intsig/office/system/BackReaderThread;
.super Ljava/lang/Thread;
.source "BackReaderThread.java"


# instance fields
.field private control:Lcom/intsig/office/system/IControl;

.field private die:Z

.field private reader:Lcom/intsig/office/system/IReader;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IReader;Lcom/intsig/office/system/IControl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    const/16 v1, 0x18

    .line 4
    .line 5
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :goto_0
    iget-boolean v0, p0, Lcom/intsig/office/system/BackReaderThread;->die:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    const/16 v1, 0x17

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    :try_start_0
    iget-object v3, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 20
    .line 21
    invoke-interface {v3}, Lcom/intsig/office/system/IReader;->isReaderFinish()Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-nez v3, :cond_1

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 28
    .line 29
    invoke-interface {v3}, Lcom/intsig/office/system/IReader;->backReader()V

    .line 30
    .line 31
    .line 32
    const-wide/16 v3, 0x32

    .line 33
    .line 34
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 39
    .line 40
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 41
    .line 42
    invoke-interface {v3, v1, v4}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 43
    .line 44
    .line 45
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 46
    .line 47
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :catch_0
    move-exception v3

    .line 51
    iget-object v4, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 52
    .line 53
    invoke-interface {v4}, Lcom/intsig/office/system/IReader;->isAborted()Z

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-nez v4, :cond_2

    .line 58
    .line 59
    iget-object v4, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 60
    .line 61
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    invoke-virtual {v4, v3, v0}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 73
    .line 74
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 75
    .line 76
    invoke-interface {v0, v1, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 80
    .line 81
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 82
    .line 83
    goto :goto_1

    .line 84
    :catch_1
    move-exception v3

    .line 85
    iget-object v4, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 86
    .line 87
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    invoke-virtual {v4, v3, v0}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;Z)V

    .line 96
    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 99
    .line 100
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 101
    .line 102
    invoke-interface {v0, v1, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 103
    .line 104
    .line 105
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->control:Lcom/intsig/office/system/IControl;

    .line 106
    .line 107
    iput-object v2, p0, Lcom/intsig/office/system/BackReaderThread;->reader:Lcom/intsig/office/system/IReader;

    .line 108
    .line 109
    :cond_2
    :goto_1
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public setDie(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/system/BackReaderThread;->die:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

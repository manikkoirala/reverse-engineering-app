.class public interface abstract Lcom/intsig/office/system/IControl;
.super Ljava/lang/Object;
.source "IControl.java"


# virtual methods
.method public abstract actionEvent(ILjava/lang/Object;)V
.end method

.method public abstract canBackLayout()Z
.end method

.method public abstract dispose()V
.end method

.method public abstract getActionValue(ILjava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract getActivity()Landroid/app/Activity;
.end method

.method public abstract getApplicationType()B
.end method

.method public abstract getCurrentViewIndex()I
.end method

.method public abstract getCustomDialog()Lcom/intsig/office/common/ICustomDialog;
.end method

.method public abstract getDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
.end method

.method public abstract getFind()Lcom/intsig/office/system/IFind;
.end method

.method public abstract getMainFrame()Lcom/intsig/office/system/IMainFrame;
.end method

.method public abstract getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;
.end method

.method public abstract getReader()Lcom/intsig/office/system/IReader;
.end method

.method public abstract getSlideShow()Lcom/intsig/office/common/ISlideShow;
.end method

.method public abstract getSysKit()Lcom/intsig/office/system/SysKit;
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract isAutoTest()Z
.end method

.method public abstract isSlideShow()Z
.end method

.method public abstract layoutView(IIII)V
.end method

.method public abstract openFile(Ljava/lang/String;)Z
.end method

.method public abstract setLayoutThreadDied(Z)V
.end method

.method public abstract setStopDraw(Z)V
.end method

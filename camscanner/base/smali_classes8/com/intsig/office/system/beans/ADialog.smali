.class public Lcom/intsig/office/system/beans/ADialog;
.super Landroid/app/Dialog;
.source "ADialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field protected static final GAP:I = 0x5

.field protected static final MARGIN:I = 0x1e


# instance fields
.field protected action:Lcom/intsig/office/system/IDialogAction;

.field protected cancel:Landroid/widget/Button;

.field protected control:Lcom/intsig/office/system/IControl;

.field protected dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

.field protected dialogID:I

.field protected model:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected ok:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Landroid/content/Context;Lcom/intsig/office/system/IDialogAction;Ljava/util/Vector;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Landroid/content/Context;",
            "Lcom/intsig/office/system/IDialogAction;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;II)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/system/beans/ADialog;-><init>(Lcom/intsig/office/system/IControl;Landroid/content/Context;Lcom/intsig/office/system/IDialogAction;Ljava/util/Vector;ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/system/IControl;Landroid/content/Context;Lcom/intsig/office/system/IDialogAction;Ljava/util/Vector;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Landroid/content/Context;",
            "Lcom/intsig/office/system/IDialogAction;",
            "Ljava/util/Vector<",
            "Ljava/lang/Object;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/system/beans/ADialog;->control:Lcom/intsig/office/system/IControl;

    .line 4
    iput p5, p0, Lcom/intsig/office/system/beans/ADialog;->dialogID:I

    .line 5
    iput-object p4, p0, Lcom/intsig/office/system/beans/ADialog;->model:Ljava/util/Vector;

    .line 6
    iput-object p3, p0, Lcom/intsig/office/system/beans/ADialog;->action:Lcom/intsig/office/system/IDialogAction;

    .line 7
    new-instance p1, Lcom/intsig/office/system/beans/ADialogFrame;

    invoke-direct {p1, p2, p0}, Lcom/intsig/office/system/beans/ADialogFrame;-><init>(Landroid/content/Context;Lcom/intsig/office/system/beans/ADialog;)V

    iput-object p1, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 8
    invoke-virtual {p0, p6}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/ADialog;->dispose()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->control:Lcom/intsig/office/system/IControl;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/system/beans/ADialog;->model:Ljava/util/Vector;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->model:Ljava/util/Vector;

    .line 12
    .line 13
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->action:Lcom/intsig/office/system/IDialogAction;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/ADialogFrame;->dispose()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 23
    .line 24
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->ok:Landroid/widget/Button;

    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->cancel:Landroid/widget/Button;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
.end method

.method public doLayout()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onBackPressed()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 5
    .line 6
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/system/beans/ADialog$1;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/office/system/beans/ADialog$1;-><init>(Lcom/intsig/office/system/beans/ADialog;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected setSize(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/ADialog;->dialogFrame:Lcom/intsig/office/system/beans/ADialogFrame;

    .line 2
    .line 3
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 4
    .line 5
    invoke-direct {v1, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

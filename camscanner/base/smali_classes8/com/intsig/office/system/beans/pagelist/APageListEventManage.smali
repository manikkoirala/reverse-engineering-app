.class public Lcom/intsig/office/system/beans/pagelist/APageListEventManage;
.super Ljava/lang/Object;
.source "APageListEventManage.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Ljava/lang/Runnable;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final MAX_ZOOM:F = 3.0f

.field private static final MOVING_DIAGONALLY:I = 0x0

.field private static final MOVING_DOWN:I = 0x4

.field private static final MOVING_LEFT:I = 0x1

.field private static final MOVING_RIGHT:I = 0x2

.field private static final MOVING_UP:I = 0x3


# instance fields
.field private final control:Lcom/intsig/office/system/IControl;

.field private eventPointerCount:I

.field protected gesture:Landroid/view/GestureDetector;

.field private isDoubleTap:Z

.field private isOnFling:Z

.field private isOnScroll:Z

.field private isProcessOnScroll:Z

.field private isScaling:Z

.field private isTouchEventIn:Z

.field private listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field protected mScroller:Landroid/widget/Scroller;

.field private mScrollerLastX:I

.field private mScrollerLastY:I

.field private mXScroll:I

.field private mYScroll:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Lcom/intsig/office/system/beans/pagelist/APageListView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 10
    .line 11
    new-instance p1, Landroid/view/GestureDetector;

    .line 12
    .line 13
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-direct {p1, v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->gesture:Landroid/view/GestureDetector;

    .line 21
    .line 22
    new-instance p1, Landroid/widget/Scroller;

    .line 23
    .line 24
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-direct {p1, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    .line 29
    .line 30
    .line 31
    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 32
    .line 33
    new-instance p1, Landroid/view/ScaleGestureDetector;

    .line 34
    .line 35
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object p2

    .line 39
    invoke-direct {p1, p2, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 40
    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method protected directionOfTravel(FF)I
    .locals 4

    .line 1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/high16 v2, 0x40000000    # 2.0f

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    cmpl-float v0, v0, v1

    .line 15
    .line 16
    if-lez v0, :cond_1

    .line 17
    .line 18
    cmpl-float p1, p1, v3

    .line 19
    .line 20
    if-lez p1, :cond_0

    .line 21
    .line 22
    const/4 p1, 0x2

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p1, 0x1

    .line 25
    :goto_0
    return p1

    .line 26
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    mul-float p1, p1, v2

    .line 35
    .line 36
    cmpl-float p1, v0, p1

    .line 37
    .line 38
    if-lez p1, :cond_3

    .line 39
    .line 40
    cmpl-float p1, p2, v3

    .line 41
    .line 42
    if-lez p1, :cond_2

    .line 43
    .line 44
    const/4 p1, 0x4

    .line 45
    goto :goto_1

    .line 46
    :cond_2
    const/4 p1, 0x3

    .line 47
    :goto_1
    return p1

    .line 48
    :cond_3
    const/4 p1, 0x0

    .line 49
    return p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getScrollX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getScrollY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected isOnFling()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected isScrollerFinished()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected isTouchEventIn()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x0

    .line 11
    const/high16 v4, -0x40800000    # -1.0f

    .line 12
    .line 13
    const/high16 v5, -0x40800000    # -1.0f

    .line 14
    .line 15
    const/16 v6, 0xa

    .line 16
    .line 17
    invoke-interface/range {v0 .. v6}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 16
    .line 17
    const/4 v5, 0x0

    .line 18
    const/high16 v6, -0x40800000    # -1.0f

    .line 19
    .line 20
    const/high16 v7, -0x40800000    # -1.0f

    .line 21
    .line 22
    const/16 v8, 0x8

    .line 23
    .line 24
    move-object v4, p1

    .line 25
    invoke-interface/range {v2 .. v8}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 26
    .line 27
    .line 28
    return v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 3
    .line 4
    const/4 v1, 0x1

    .line 5
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 14
    .line 15
    const/4 v5, 0x0

    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/high16 v7, -0x40800000    # -1.0f

    .line 19
    .line 20
    const/16 v8, 0x9

    .line 21
    .line 22
    move-object v4, p1

    .line 23
    invoke-interface/range {v2 .. v8}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 24
    .line 25
    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 14
    .line 15
    const/4 v5, 0x0

    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/high16 v7, -0x40800000    # -1.0f

    .line 19
    .line 20
    const/4 v8, 0x1

    .line 21
    move-object v4, p1

    .line 22
    invoke-interface/range {v2 .. v8}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 23
    .line 24
    .line 25
    return v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v7, 0x6

    .line 10
    move-object v3, p1

    .line 11
    move-object v4, p2

    .line 12
    move v5, p3

    .line 13
    move v6, p4

    .line 14
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 15
    .line 16
    .line 17
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 18
    .line 19
    const/4 p2, 0x1

    .line 20
    if-eqz p1, :cond_9

    .line 21
    .line 22
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 23
    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    goto/16 :goto_1

    .line 27
    .line 28
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    if-eqz p1, :cond_9

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(Landroid/view/View;)Landroid/graphics/Rect;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-interface {v1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageListViewMovingPosition()B

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-nez v1, :cond_4

    .line 53
    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 59
    .line 60
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-le p1, v1, :cond_1

    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isChangePage()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-eqz p1, :cond_8

    .line 77
    .line 78
    :cond_1
    invoke-virtual {p0, p3, p4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->directionOfTravel(FF)I

    .line 79
    .line 80
    .line 81
    move-result p1

    .line 82
    if-eq p1, p2, :cond_3

    .line 83
    .line 84
    const/4 v1, 0x2

    .line 85
    if-eq p1, v1, :cond_2

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    iget p1, v0, Landroid/graphics/Rect;->right:I

    .line 89
    .line 90
    if-gtz p1, :cond_8

    .line 91
    .line 92
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 95
    .line 96
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->previousPageview()V

    .line 97
    .line 98
    .line 99
    return p2

    .line 100
    :cond_3
    iget p1, v0, Landroid/graphics/Rect;->left:I

    .line 101
    .line 102
    if-ltz p1, :cond_8

    .line 103
    .line 104
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 107
    .line 108
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->nextPageView()V

    .line 109
    .line 110
    .line 111
    return p2

    .line 112
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 113
    .line 114
    .line 115
    move-result p1

    .line 116
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 117
    .line 118
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    if-le p1, v1, :cond_5

    .line 123
    .line 124
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isChangePage()Z

    .line 131
    .line 132
    .line 133
    move-result p1

    .line 134
    if-eqz p1, :cond_8

    .line 135
    .line 136
    :cond_5
    invoke-virtual {p0, p3, p4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->directionOfTravel(FF)I

    .line 137
    .line 138
    .line 139
    move-result p1

    .line 140
    const/4 v1, 0x3

    .line 141
    if-eq p1, v1, :cond_7

    .line 142
    .line 143
    const/4 v1, 0x4

    .line 144
    if-eq p1, v1, :cond_6

    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_6
    iget p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 148
    .line 149
    if-gtz p1, :cond_8

    .line 150
    .line 151
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 154
    .line 155
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->previousPageview()V

    .line 156
    .line 157
    .line 158
    return p2

    .line 159
    :cond_7
    iget p1, v0, Landroid/graphics/Rect;->top:I

    .line 160
    .line 161
    if-ltz p1, :cond_8

    .line 162
    .line 163
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 164
    .line 165
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 166
    .line 167
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->nextPageView()V

    .line 168
    .line 169
    .line 170
    return p2

    .line 171
    :cond_8
    :goto_0
    const/4 p1, 0x0

    .line 172
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastY:I

    .line 173
    .line 174
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastX:I

    .line 175
    .line 176
    new-instance v1, Landroid/graphics/Rect;

    .line 177
    .line 178
    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 179
    .line 180
    .line 181
    const/16 v2, -0x64

    .line 182
    .line 183
    invoke-virtual {v1, v2, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {p0, v0, p3, p4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->withinBoundsInDirectionOfTravel(Landroid/graphics/Rect;FF)Z

    .line 187
    .line 188
    .line 189
    move-result v2

    .line 190
    if-eqz v2, :cond_9

    .line 191
    .line 192
    invoke-virtual {v1, p1, p1}, Landroid/graphics/Rect;->contains(II)Z

    .line 193
    .line 194
    .line 195
    move-result p1

    .line 196
    if-eqz p1, :cond_9

    .line 197
    .line 198
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 199
    .line 200
    const/4 v2, 0x0

    .line 201
    const/4 v3, 0x0

    .line 202
    float-to-int v4, p3

    .line 203
    float-to-int v5, p4

    .line 204
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 205
    .line 206
    iget v7, v0, Landroid/graphics/Rect;->right:I

    .line 207
    .line 208
    iget v8, v0, Landroid/graphics/Rect;->top:I

    .line 209
    .line 210
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 211
    .line 212
    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 213
    .line 214
    .line 215
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 216
    .line 217
    invoke-virtual {p1, p0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 218
    .line 219
    .line 220
    :cond_9
    :goto_1
    return p2
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const/high16 v5, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/high16 v6, -0x40800000    # -1.0f

    .line 13
    .line 14
    const/4 v7, 0x5

    .line 15
    move-object v3, p1

    .line 16
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->eventPointerCount:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-le v0, v1, :cond_2

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isTouchZoom()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto/16 :goto_0

    .line 19
    .line 20
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getZoom()F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getZoom()F

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    mul-float v2, v2, v3

    .line 39
    .line 40
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getFitZoom()F

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 51
    .line 52
    invoke-virtual {v3}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getFitZoom()F

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    const/high16 v4, 0x40400000    # 3.0f

    .line 57
    .line 58
    mul-float v3, v3, v4

    .line 59
    .line 60
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    const v3, 0x4b189680    # 1.0E7f

    .line 65
    .line 66
    .line 67
    mul-float v4, v2, v3

    .line 68
    .line 69
    float-to-int v4, v4

    .line 70
    mul-float v3, v3, v0

    .line 71
    .line 72
    float-to-int v3, v3

    .line 73
    if-eq v4, v3, :cond_1

    .line 74
    .line 75
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnScroll:Z

    .line 76
    .line 77
    div-float v0, v2, v0

    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 80
    .line 81
    const/4 v4, 0x0

    .line 82
    invoke-virtual {v3, v2, v4}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setZoom(FZ)V

    .line 83
    .line 84
    .line 85
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 86
    .line 87
    invoke-virtual {v3}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    if-eqz v3, :cond_1

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    float-to-int v4, v4

    .line 98
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    .line 99
    .line 100
    .line 101
    move-result v5

    .line 102
    iget v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 103
    .line 104
    add-int/2addr v5, v6

    .line 105
    sub-int/2addr v4, v5

    .line 106
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    .line 107
    .line 108
    .line 109
    move-result p1

    .line 110
    float-to-int p1, p1

    .line 111
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    iget v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 116
    .line 117
    add-int/2addr v3, v5

    .line 118
    sub-int/2addr p1, v3

    .line 119
    iget v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 120
    .line 121
    int-to-float v3, v3

    .line 122
    int-to-float v4, v4

    .line 123
    mul-float v6, v4, v0

    .line 124
    .line 125
    sub-float/2addr v4, v6

    .line 126
    add-float/2addr v3, v4

    .line 127
    float-to-int v3, v3

    .line 128
    iput v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 129
    .line 130
    int-to-float v3, v5

    .line 131
    int-to-float p1, p1

    .line 132
    mul-float v0, v0, p1

    .line 133
    .line 134
    sub-float/2addr p1, v0

    .line 135
    add-float/2addr v3, p1

    .line 136
    float-to-int p1, v3

    .line 137
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 140
    .line 141
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    .line 142
    .line 143
    .line 144
    :cond_1
    const/high16 p1, 0x42c80000    # 100.0f

    .line 145
    .line 146
    mul-float v2, v2, p1

    .line 147
    .line 148
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 149
    .line 150
    .line 151
    move-result p1

    .line 152
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 153
    .line 154
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-interface {v0, p1}, Lcom/intsig/office/system/IMainFrame;->changeZoom(I)V

    .line 159
    .line 160
    .line 161
    :cond_2
    :goto_0
    return v1
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .line 1
    iget p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->eventPointerCount:I

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-le p1, v0, :cond_1

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isTouchZoom()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScaling:Z

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 23
    .line 24
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 25
    .line 26
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 27
    .line 28
    :cond_1
    :goto_0
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 7

    .line 1
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    const/high16 v4, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/high16 v5, -0x40800000    # -1.0f

    .line 13
    .line 14
    const/16 v6, 0xc

    .line 15
    .line 16
    invoke-interface/range {v0 .. v6}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 17
    .line 18
    .line 19
    iget p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->eventPointerCount:I

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    if-le p1, v0, :cond_1

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isTouchZoom()Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 p1, 0x0

    .line 38
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScaling:Z

    .line 39
    .line 40
    :cond_1
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v7, 0x4

    .line 10
    move-object v3, p1

    .line 11
    move-object v4, p2

    .line 12
    move v5, p3

    .line 13
    move v6, p4

    .line 14
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 15
    .line 16
    .line 17
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 18
    .line 19
    const/4 p2, 0x1

    .line 20
    if-eqz p1, :cond_2

    .line 21
    .line 22
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 23
    .line 24
    if-nez p1, :cond_2

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const/4 v0, 0x0

    .line 33
    invoke-interface {p1, v0}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->setDrawPictrue(Z)V

    .line 34
    .line 35
    .line 36
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnScroll:Z

    .line 37
    .line 38
    iget p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 39
    .line 40
    int-to-float p1, p1

    .line 41
    sub-float/2addr p1, p3

    .line 42
    float-to-int p1, p1

    .line 43
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 44
    .line 45
    iget p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 46
    .line 47
    int-to-float p1, p1

    .line 48
    sub-float/2addr p1, p4

    .line 49
    float-to-int p1, p1

    .line 50
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isChangePage()Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-nez p1, :cond_1

    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    if-eqz p1, :cond_1

    .line 71
    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 73
    .line 74
    .line 75
    move-result p4

    .line 76
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 77
    .line 78
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    if-le p4, v1, :cond_1

    .line 83
    .line 84
    const/4 p4, 0x0

    .line 85
    cmpl-float v1, p3, p4

    .line 86
    .line 87
    if-lez v1, :cond_0

    .line 88
    .line 89
    iget-object p3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 90
    .line 91
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    .line 92
    .line 93
    .line 94
    move-result p3

    .line 95
    iget p4, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 96
    .line 97
    sub-int/2addr p3, p4

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    .line 99
    .line 100
    .line 101
    move-result p4

    .line 102
    sub-int/2addr p3, p4

    .line 103
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 104
    .line 105
    .line 106
    move-result p4

    .line 107
    if-le p3, p4, :cond_1

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageIndex()I

    .line 110
    .line 111
    .line 112
    move-result p3

    .line 113
    iget-object p4, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 114
    .line 115
    invoke-virtual {p4}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageCount()I

    .line 116
    .line 117
    .line 118
    move-result p4

    .line 119
    sub-int/2addr p4, p2

    .line 120
    if-ge p3, p4, :cond_1

    .line 121
    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 123
    .line 124
    .line 125
    move-result p3

    .line 126
    iget-object p4, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 127
    .line 128
    invoke-virtual {p4}, Landroid/view/View;->getWidth()I

    .line 129
    .line 130
    .line 131
    move-result p4

    .line 132
    sub-int/2addr p3, p4

    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    add-int/2addr p3, p1

    .line 138
    neg-int p1, p3

    .line 139
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 140
    .line 141
    goto :goto_0

    .line 142
    :cond_0
    cmpg-float p3, p3, p4

    .line 143
    .line 144
    if-gez p3, :cond_1

    .line 145
    .line 146
    iget p3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 147
    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    .line 149
    .line 150
    .line 151
    move-result p4

    .line 152
    add-int/2addr p3, p4

    .line 153
    if-lez p3, :cond_1

    .line 154
    .line 155
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageIndex()I

    .line 156
    .line 157
    .line 158
    move-result p1

    .line 159
    if-eqz p1, :cond_1

    .line 160
    .line 161
    iput v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 162
    .line 163
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    .line 166
    .line 167
    .line 168
    :cond_2
    return p2
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const/high16 v5, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/high16 v6, -0x40800000    # -1.0f

    .line 13
    .line 14
    const/4 v7, 0x2

    .line 15
    move-object v3, p1

    .line 16
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const/high16 v5, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/high16 v6, -0x40800000    # -1.0f

    .line 13
    .line 14
    const/4 v7, 0x7

    .line 15
    move-object v3, p1

    .line 16
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const/high16 v5, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/high16 v6, -0x40800000    # -1.0f

    .line 13
    .line 14
    const/4 v7, 0x3

    .line 15
    move-object v3, p1

    .line 16
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v4, 0x0

    .line 8
    const/high16 v5, -0x40800000    # -1.0f

    .line 9
    .line 10
    const/high16 v6, -0x40800000    # -1.0f

    .line 11
    .line 12
    const/4 v7, 0x0

    .line 13
    move-object v2, p1

    .line 14
    move-object v3, p2

    .line 15
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    return p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected processOnTouch(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->eventPointerCount:I

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling:Z

    .line 16
    .line 17
    iput-boolean v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 24
    .line 25
    .line 26
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScaling:Z

    .line 27
    .line 28
    if-nez v0, :cond_2

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->gesture:Landroid/view/GestureDetector;

    .line 31
    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 35
    .line 36
    .line 37
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-ne p1, v2, :cond_5

    .line 42
    .line 43
    iput-boolean v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isProcessOnScroll:Z

    .line 44
    .line 45
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-eqz p1, :cond_4

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 56
    .line 57
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-eqz v0, :cond_3

    .line 62
    .line 63
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 64
    .line 65
    if-nez v0, :cond_3

    .line 66
    .line 67
    invoke-virtual {p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->slideViewOntoScreen(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 68
    .line 69
    .line 70
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 71
    .line 72
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_4

    .line 77
    .line 78
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnScroll:Z

    .line 79
    .line 80
    if-eqz v0, :cond_4

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-interface {v0, v2}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->setDrawPictrue(Z)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 92
    .line 93
    invoke-virtual {v0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 94
    .line 95
    .line 96
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isDoubleTap:Z

    .line 97
    .line 98
    iput-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnScroll:Z

    .line 99
    .line 100
    :cond_5
    return v2
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-interface {v0, v1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->setDrawPictrue(Z)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 31
    .line 32
    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    iget v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 37
    .line 38
    iget v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastX:I

    .line 39
    .line 40
    sub-int v3, v0, v3

    .line 41
    .line 42
    add-int/2addr v2, v3

    .line 43
    iput v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 44
    .line 45
    iget v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 46
    .line 47
    iget v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastY:I

    .line 48
    .line 49
    sub-int v3, v1, v3

    .line 50
    .line 51
    add-int/2addr v2, v3

    .line 52
    iput v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 53
    .line 54
    iput v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastX:I

    .line 55
    .line 56
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastY:I

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 64
    .line 65
    invoke-virtual {v0, p0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn:Z

    .line 70
    .line 71
    if-nez v0, :cond_1

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    const/4 v1, 0x0

    .line 89
    invoke-interface {v0, v1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->updateStutus(Ljava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    const/4 v1, 0x1

    .line 99
    invoke-interface {v0, v1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->setDrawPictrue(Z)V

    .line 100
    .line 101
    .line 102
    :cond_1
    :goto_0
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected setScrollAxisValue(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mXScroll:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mYScroll:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected slideViewOntoScreen(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(Landroid/view/View;)Landroid/graphics/Rect;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget v4, v0, Landroid/graphics/Point;->x:I

    .line 12
    .line 13
    if-nez v4, :cond_0

    .line 14
    .line 15
    iget v1, v0, Landroid/graphics/Point;->y:I

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    :cond_0
    const/4 v1, 0x0

    .line 20
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastY:I

    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScrollerLastX:I

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->mScroller:Landroid/widget/Scroller;

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    const/4 v3, 0x0

    .line 28
    iget v5, v0, Landroid/graphics/Point;->y:I

    .line 29
    .line 30
    const/16 v6, 0x190

    .line 31
    .line 32
    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 36
    .line 37
    invoke-virtual {v0, p0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 38
    .line 39
    .line 40
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->listView:Lcom/intsig/office/system/beans/pagelist/APageListView;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-interface {v0, p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->resetSearchResult(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected withinBoundsInDirectionOfTravel(Landroid/graphics/Rect;FF)Z
    .locals 2

    .line 1
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->directionOfTravel(FF)I

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    const/4 p3, 0x0

    .line 6
    if-eqz p2, :cond_8

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    if-eq p2, v0, :cond_6

    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    if-eq p2, v1, :cond_4

    .line 13
    .line 14
    const/4 v1, 0x3

    .line 15
    if-eq p2, v1, :cond_2

    .line 16
    .line 17
    const/4 v1, 0x4

    .line 18
    if-ne p2, v1, :cond_1

    .line 19
    .line 20
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 21
    .line 22
    if-ltz p1, :cond_0

    .line 23
    .line 24
    const/4 p3, 0x1

    .line 25
    :cond_0
    return p3

    .line 26
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    .line 27
    .line 28
    invoke-direct {p1}, Ljava/util/NoSuchElementException;-><init>()V

    .line 29
    .line 30
    .line 31
    throw p1

    .line 32
    :cond_2
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 33
    .line 34
    if-gtz p1, :cond_3

    .line 35
    .line 36
    const/4 p3, 0x1

    .line 37
    :cond_3
    return p3

    .line 38
    :cond_4
    iget p1, p1, Landroid/graphics/Rect;->right:I

    .line 39
    .line 40
    if-ltz p1, :cond_5

    .line 41
    .line 42
    const/4 p3, 0x1

    .line 43
    :cond_5
    return p3

    .line 44
    :cond_6
    iget p1, p1, Landroid/graphics/Rect;->left:I

    .line 45
    .line 46
    if-gtz p1, :cond_7

    .line 47
    .line 48
    const/4 p3, 0x1

    .line 49
    :cond_7
    return p3

    .line 50
    :cond_8
    invoke-virtual {p1, p3, p3}, Landroid/graphics/Rect;->contains(II)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    return p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method protected zoom(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

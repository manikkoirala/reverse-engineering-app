.class public Lcom/intsig/office/system/beans/CalloutView/CalloutView;
.super Landroid/view/View;
.source "CalloutView.java"


# static fields
.field private static final TOUCH_TOLERANCE:F = 4.0f


# instance fields
.field private calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

.field private control:Lcom/intsig/office/system/IControl;

.field private index:I

.field private left:I

.field private mListener:Lcom/intsig/office/system/beans/CalloutView/IExportListener;

.field private mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

.field private mPathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/system/beans/CalloutView/PathInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mX:F

.field private mY:F

.field private final offset:I

.field private runnable:Ljava/lang/Runnable;

.field private top:I

.field private zoom:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/intsig/office/system/IControl;Lcom/intsig/office/system/beans/CalloutView/IExportListener;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    const/high16 p1, 0x40000000    # 2.0f

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->zoom:F

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 12
    .line 13
    const/4 v0, 0x5

    .line 14
    iput v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->offset:I

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->left:I

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->top:I

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->runnable:Ljava/lang/Runnable;

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->index:I

    .line 24
    .line 25
    const/4 v0, 0x2

    .line 26
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 27
    .line 28
    .line 29
    iput-object p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->control:Lcom/intsig/office/system/IControl;

    .line 30
    .line 31
    iput-object p3, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mListener:Lcom/intsig/office/system/beans/CalloutView/IExportListener;

    .line 32
    .line 33
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getCalloutManager()Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private exportImage()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->runnable:Ljava/lang/Runnable;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/office/system/beans/CalloutView/CalloutView$1;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/office/system/beans/CalloutView/CalloutView$1;-><init>(Lcom/intsig/office/system/beans/CalloutView/CalloutView;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->runnable:Ljava/lang/Runnable;

    .line 14
    .line 15
    const-wide/16 v1, 0x3e8

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private touch_move(FF)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_1

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->zoom:F

    .line 11
    .line 12
    div-float/2addr p1, v0

    .line 13
    div-float/2addr p2, v0

    .line 14
    iget v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 15
    .line 16
    sub-float v0, p1, v0

    .line 17
    .line 18
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iget v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 23
    .line 24
    sub-float v1, p2, v1

    .line 25
    .line 26
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/high16 v2, 0x40800000    # 4.0f

    .line 31
    .line 32
    cmpl-float v0, v0, v2

    .line 33
    .line 34
    if-gez v0, :cond_0

    .line 35
    .line 36
    cmpl-float v0, v1, v2

    .line 37
    .line 38
    if-ltz v0, :cond_1

    .line 39
    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 43
    .line 44
    iget v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 47
    .line 48
    add-float v3, p1, v1

    .line 49
    .line 50
    const/high16 v4, 0x40000000    # 2.0f

    .line 51
    .line 52
    div-float/2addr v3, v4

    .line 53
    add-float v5, p2, v2

    .line 54
    .line 55
    div-float/2addr v5, v4

    .line 56
    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 57
    .line 58
    .line 59
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 60
    .line 61
    iput p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 62
    .line 63
    :cond_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private touch_start(FF)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->zoom:F

    .line 2
    .line 3
    div-float/2addr p1, v0

    .line 4
    div-float/2addr p2, v0

    .line 5
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 6
    .line 7
    iput p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v1, 0x1

    .line 16
    if-ne v0, v1, :cond_0

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 19
    .line 20
    invoke-direct {v0}, Lcom/intsig/office/system/beans/CalloutView/PathInfo;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 24
    .line 25
    new-instance v2, Landroid/graphics/Path;

    .line 26
    .line 27
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v2, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 35
    .line 36
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 40
    .line 41
    iget-object p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 42
    .line 43
    invoke-virtual {p2}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getColor()I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    iput p2, p1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->color:I

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getWidth()I

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    iput p2, p1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->width:I

    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 60
    .line 61
    iget p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->index:I

    .line 62
    .line 63
    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getPath(IZ)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    iput-object p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 68
    .line 69
    iget-object p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 70
    .line 71
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    :cond_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private touch_up()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathInfo:Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 22
    .line 23
    iget v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 24
    .line 25
    const/high16 v2, 0x3f800000    # 1.0f

    .line 26
    .line 27
    add-float/2addr v1, v2

    .line 28
    iput v1, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->x:F

    .line 29
    .line 30
    iget v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 31
    .line 32
    add-float/2addr v1, v2

    .line 33
    iput v1, v0, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->y:F

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    const/4 v1, 0x2

    .line 43
    if-ne v0, v1, :cond_2

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 46
    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    const/4 v0, 0x0

    .line 50
    const/4 v1, 0x0

    .line 51
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 52
    .line 53
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-ge v1, v2, :cond_2

    .line 58
    .line 59
    iget-object v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 60
    .line 61
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    check-cast v2, Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 66
    .line 67
    new-instance v3, Landroid/graphics/Path;

    .line 68
    .line 69
    iget-object v4, v2, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 70
    .line 71
    invoke-direct {v3, v4}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 72
    .line 73
    .line 74
    iget v4, v2, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->x:F

    .line 75
    .line 76
    iget v2, v2, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->y:F

    .line 77
    .line 78
    invoke-virtual {v3, v4, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 79
    .line 80
    .line 81
    new-instance v2, Landroid/graphics/RectF;

    .line 82
    .line 83
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3, v2, v0}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 87
    .line 88
    .line 89
    new-instance v4, Landroid/graphics/Region;

    .line 90
    .line 91
    invoke-direct {v4}, Landroid/graphics/Region;-><init>()V

    .line 92
    .line 93
    .line 94
    new-instance v5, Landroid/graphics/Region;

    .line 95
    .line 96
    iget v6, v2, Landroid/graphics/RectF;->left:F

    .line 97
    .line 98
    float-to-int v6, v6

    .line 99
    iget v7, v2, Landroid/graphics/RectF;->top:F

    .line 100
    .line 101
    float-to-int v7, v7

    .line 102
    iget v8, v2, Landroid/graphics/RectF;->right:F

    .line 103
    .line 104
    float-to-int v8, v8

    .line 105
    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 106
    .line 107
    float-to-int v2, v2

    .line 108
    invoke-direct {v5, v6, v7, v8, v2}, Landroid/graphics/Region;-><init>(IIII)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v4, v3, v5}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 112
    .line 113
    .line 114
    new-instance v2, Landroid/graphics/Region;

    .line 115
    .line 116
    iget v3, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mX:F

    .line 117
    .line 118
    float-to-int v5, v3

    .line 119
    add-int/lit8 v5, v5, -0x5

    .line 120
    .line 121
    iget v6, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mY:F

    .line 122
    .line 123
    float-to-int v7, v6

    .line 124
    add-int/lit8 v7, v7, -0x5

    .line 125
    .line 126
    float-to-int v3, v3

    .line 127
    add-int/lit8 v3, v3, 0x5

    .line 128
    .line 129
    float-to-int v6, v6

    .line 130
    add-int/lit8 v6, v6, 0x5

    .line 131
    .line 132
    invoke-direct {v2, v5, v7, v3, v6}, Landroid/graphics/Region;-><init>(IIII)V

    .line 133
    .line 134
    .line 135
    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    .line 136
    .line 137
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    .line 138
    .line 139
    .line 140
    move-result v2

    .line 141
    if-eqz v2, :cond_1

    .line 142
    .line 143
    iget-object v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 144
    .line 145
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 146
    .line 147
    .line 148
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 149
    .line 150
    goto :goto_0

    .line 151
    :cond_2
    :goto_1
    return-void
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/system/beans/CalloutView/CalloutView;)Lcom/intsig/office/system/beans/CalloutView/IExportListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mListener:Lcom/intsig/office/system/beans/CalloutView/IExportListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->index:I

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getPath(IZ)Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    iput-object v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-ge v3, v1, :cond_0

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->mPathList:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/office/system/beans/CalloutView/MyPaint;

    .line 38
    .line 39
    invoke-direct {v2}, Lcom/intsig/office/system/beans/CalloutView/MyPaint;-><init>()V

    .line 40
    .line 41
    .line 42
    iget v4, v1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->width:I

    .line 43
    .line 44
    int-to-float v4, v4

    .line 45
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 46
    .line 47
    .line 48
    iget v4, v1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->color:I

    .line 49
    .line 50
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 54
    .line 55
    .line 56
    iget v4, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->left:I

    .line 57
    .line 58
    iget v5, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->top:I

    .line 59
    .line 60
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 61
    .line 62
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 63
    .line 64
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 65
    .line 66
    .line 67
    iget v4, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->zoom:F

    .line 68
    .line 69
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 70
    .line 71
    .line 72
    iget-object v1, v1, Lcom/intsig/office/system/beans/CalloutView/PathInfo;->path:Landroid/graphics/Path;

    .line 73
    .line 74
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 78
    .line 79
    .line 80
    add-int/lit8 v3, v3, 0x1

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->calloutMgr:Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return p1

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    const/4 v2, 0x1

    .line 24
    if-eqz p1, :cond_3

    .line 25
    .line 26
    if-eq p1, v2, :cond_2

    .line 27
    .line 28
    const/4 v3, 0x2

    .line 29
    if-eq p1, v3, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->touch_move(FF)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    invoke-direct {p0}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->touch_up()V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->exportImage()V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->touch_start(FF)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 53
    .line 54
    .line 55
    :goto_0
    return v2
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setClip(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->left:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->top:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->index:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->zoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

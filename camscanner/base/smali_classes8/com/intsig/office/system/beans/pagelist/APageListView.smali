.class public Lcom/intsig/office/system/beans/pagelist/APageListView;
.super Landroid/widget/AdapterView;
.source "APageListView.java"

# interfaces
.implements Lcom/intsig/office/pg/control/PageListViewDelegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView<",
        "Landroid/widget/Adapter;",
        ">;",
        "Lcom/intsig/office/pg/control/PageListViewDelegate;"
    }
.end annotation


# static fields
.field private static final GAP:I = 0x14


# instance fields
.field private childViewsCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/intsig/office/system/beans/pagelist/APageListItem;",
            ">;"
        }
    .end annotation
.end field

.field private currentIndex:I

.field private eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

.field private isConfigurationChanged:Z

.field private isDoRequestLayout:Z

.field private isInit:Z

.field private isInitZoom:Z

.field private isResetLayout:Z

.field private pageAdapter:Landroid/widget/Adapter;

.field private pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

.field private pageViewCache:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/office/system/beans/pagelist/APageListItem;",
            ">;"
        }
    .end annotation
.end field

.field private zoom:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isDoRequestLayout:Z

    const/high16 p1, 0x3f800000    # 1.0f

    .line 3
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 4
    new-instance p1, Landroid/util/SparseArray;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Landroid/util/SparseArray;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 5
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/office/system/IControl;Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;)V
    .locals 2

    .line 6
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isDoRequestLayout:Z

    const/high16 v0, 0x3f800000    # 1.0f

    .line 8
    iput v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 9
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 10
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 11
    iput-object p3, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 12
    new-instance p3, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-direct {p3, p2, p0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;-><init>(Lcom/intsig/office/system/IControl;Lcom/intsig/office/system/beans/pagelist/APageListView;)V

    iput-object p3, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 13
    new-instance p2, Lcom/intsig/office/system/beans/pagelist/APageListAdapter;

    invoke-direct {p2, p0}, Lcom/intsig/office/system/beans/pagelist/APageListAdapter;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;)V

    iput-object p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 14
    invoke-virtual {p0, p1}, Landroid/view/View;->setLongClickable(Z)V

    .line 15
    new-instance p1, Lcom/intsig/office/system/beans/pagelist/APageListView$1;

    invoke-direct {p1, p0}, Lcom/intsig/office/system/beans/pagelist/APageListView$1;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/office/system/beans/pagelist/APageListView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isResetLayout:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 8
    .line 9
    if-nez v0, :cond_2

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    check-cast v1, Landroid/view/View;

    .line 30
    .line 31
    :goto_0
    invoke-interface {v0, p1, v1, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 36
    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    if-nez v1, :cond_1

    .line 42
    .line 43
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 44
    .line 45
    const/4 v2, -0x2

    .line 46
    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 47
    .line 48
    .line 49
    :cond_1
    const/4 v2, 0x0

    .line 50
    const/4 v3, 0x1

    .line 51
    invoke-virtual {p0, v0, v2, v1, v3}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 52
    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 55
    .line 56
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageWidth()I

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    int-to-float p1, p1

    .line 64
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 65
    .line 66
    mul-float p1, p1, v1

    .line 67
    .line 68
    float-to-int p1, p1

    .line 69
    const/high16 v1, 0x40000000    # 2.0f

    .line 70
    .line 71
    or-int/2addr p1, v1

    .line 72
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageHeight()I

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    int-to-float v2, v2

    .line 77
    iget v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 78
    .line 79
    mul-float v2, v2, v3

    .line 80
    .line 81
    float-to-int v2, v2

    .line 82
    or-int/2addr v1, v2

    .line 83
    invoke-virtual {v0, p1, v1}, Landroid/view/View;->measure(II)V

    .line 84
    .line 85
    .line 86
    :cond_2
    return-object v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private layout_Horizontal()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isResetLayout:Z

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    const/4 v3, 0x1

    .line 15
    if-nez v1, :cond_5

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    if-ge v1, v4, :cond_1

    .line 32
    .line 33
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    add-int/2addr v4, v5

    .line 46
    iget v5, v1, Landroid/graphics/Point;->x:I

    .line 47
    .line 48
    add-int/2addr v4, v5

    .line 49
    add-int/lit8 v4, v4, 0xa

    .line 50
    .line 51
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 52
    .line 53
    invoke-virtual {v5}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    .line 54
    .line 55
    .line 56
    move-result v5

    .line 57
    add-int/2addr v4, v5

    .line 58
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    div-int/lit8 v5, v5, 0x2

    .line 63
    .line 64
    if-ge v4, v5, :cond_0

    .line 65
    .line 66
    iget v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 67
    .line 68
    add-int/2addr v4, v3

    .line 69
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 70
    .line 71
    invoke-interface {v5}, Landroid/widget/Adapter;->getCount()I

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    if-ge v4, v5, :cond_0

    .line 76
    .line 77
    iget-object v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 78
    .line 79
    invoke-virtual {v4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling()Z

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    if-nez v4, :cond_0

    .line 84
    .line 85
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postUnRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 86
    .line 87
    .line 88
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 89
    .line 90
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 91
    .line 92
    .line 93
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 94
    .line 95
    add-int/2addr v1, v3

    .line 96
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 104
    .line 105
    sub-int/2addr v4, v1

    .line 106
    add-int/lit8 v4, v4, -0xa

    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 109
    .line 110
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    add-int/2addr v4, v1

    .line 115
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 116
    .line 117
    .line 118
    move-result v1

    .line 119
    div-int/lit8 v1, v1, 0x2

    .line 120
    .line 121
    if-lt v4, v1, :cond_1

    .line 122
    .line 123
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 124
    .line 125
    if-lez v1, :cond_1

    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 128
    .line 129
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling()Z

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    if-nez v1, :cond_1

    .line 134
    .line 135
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postUnRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 136
    .line 137
    .line 138
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 139
    .line 140
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 141
    .line 142
    .line 143
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 144
    .line 145
    sub-int/2addr v1, v3

    .line 146
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 147
    .line 148
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 149
    .line 150
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    new-array v4, v1, [I

    .line 155
    .line 156
    const/4 v5, 0x0

    .line 157
    :goto_1
    if-ge v5, v1, :cond_2

    .line 158
    .line 159
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 160
    .line 161
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I

    .line 162
    .line 163
    .line 164
    move-result v6

    .line 165
    aput v6, v4, v5

    .line 166
    .line 167
    add-int/lit8 v5, v5, 0x1

    .line 168
    .line 169
    goto :goto_1

    .line 170
    :cond_2
    const/4 v5, 0x0

    .line 171
    :goto_2
    if-ge v5, v1, :cond_c

    .line 172
    .line 173
    aget v6, v4, v5

    .line 174
    .line 175
    iget v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 176
    .line 177
    add-int/lit8 v8, v7, -0x1

    .line 178
    .line 179
    if-lt v6, v8, :cond_3

    .line 180
    .line 181
    add-int/lit8 v7, v7, 0x1

    .line 182
    .line 183
    if-le v6, v7, :cond_4

    .line 184
    .line 185
    :cond_3
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 186
    .line 187
    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 188
    .line 189
    .line 190
    move-result-object v6

    .line 191
    check-cast v6, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 192
    .line 193
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->releaseResources()V

    .line 194
    .line 195
    .line 196
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 197
    .line 198
    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 199
    .line 200
    .line 201
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 202
    .line 203
    .line 204
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 205
    .line 206
    aget v7, v4, v5

    .line 207
    .line 208
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 209
    .line 210
    .line 211
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 212
    .line 213
    goto :goto_2

    .line 214
    :cond_5
    iput-boolean v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isResetLayout:Z

    .line 215
    .line 216
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 217
    .line 218
    invoke-virtual {v1, v2, v2}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->setScrollAxisValue(II)V

    .line 219
    .line 220
    .line 221
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 222
    .line 223
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 224
    .line 225
    .line 226
    move-result v1

    .line 227
    new-array v4, v1, [I

    .line 228
    .line 229
    const/4 v5, 0x0

    .line 230
    :goto_3
    if-ge v5, v1, :cond_6

    .line 231
    .line 232
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 233
    .line 234
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I

    .line 235
    .line 236
    .line 237
    move-result v6

    .line 238
    aput v6, v4, v5

    .line 239
    .line 240
    add-int/lit8 v5, v5, 0x1

    .line 241
    .line 242
    goto :goto_3

    .line 243
    :cond_6
    const/4 v5, 0x0

    .line 244
    const/4 v6, 0x0

    .line 245
    :goto_4
    if-ge v5, v1, :cond_a

    .line 246
    .line 247
    aget v7, v4, v5

    .line 248
    .line 249
    iget v8, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 250
    .line 251
    add-int/lit8 v9, v8, -0x1

    .line 252
    .line 253
    if-lt v7, v9, :cond_7

    .line 254
    .line 255
    add-int/lit8 v8, v8, 0x1

    .line 256
    .line 257
    if-le v7, v8, :cond_9

    .line 258
    .line 259
    :cond_7
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 260
    .line 261
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 262
    .line 263
    .line 264
    move-result-object v6

    .line 265
    check-cast v6, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 266
    .line 267
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->releaseResources()V

    .line 268
    .line 269
    .line 270
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 271
    .line 272
    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 273
    .line 274
    .line 275
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 276
    .line 277
    .line 278
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 279
    .line 280
    aget v7, v4, v5

    .line 281
    .line 282
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 283
    .line 284
    .line 285
    aget v6, v4, v5

    .line 286
    .line 287
    iget v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 288
    .line 289
    if-ne v6, v7, :cond_8

    .line 290
    .line 291
    const/4 v6, 0x1

    .line 292
    goto :goto_5

    .line 293
    :cond_8
    const/4 v6, 0x0

    .line 294
    :cond_9
    :goto_5
    add-int/lit8 v5, v5, 0x1

    .line 295
    .line 296
    goto :goto_4

    .line 297
    :cond_a
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 298
    .line 299
    const/high16 v4, 0x42c80000    # 100.0f

    .line 300
    .line 301
    mul-float v1, v1, v4

    .line 302
    .line 303
    float-to-int v1, v1

    .line 304
    const/16 v4, 0x64

    .line 305
    .line 306
    if-ne v1, v4, :cond_b

    .line 307
    .line 308
    if-nez v6, :cond_c

    .line 309
    .line 310
    :cond_b
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 311
    .line 312
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 313
    .line 314
    .line 315
    :cond_c
    if-nez v0, :cond_d

    .line 316
    .line 317
    const/4 v0, 0x1

    .line 318
    goto :goto_6

    .line 319
    :cond_d
    const/4 v0, 0x0

    .line 320
    :goto_6
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 321
    .line 322
    invoke-direct {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 323
    .line 324
    .line 325
    move-result-object v1

    .line 326
    invoke-virtual {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 327
    .line 328
    .line 329
    move-result-object v4

    .line 330
    if-eqz v0, :cond_e

    .line 331
    .line 332
    iget v0, v4, Landroid/graphics/Point;->x:I

    .line 333
    .line 334
    iget v5, v4, Landroid/graphics/Point;->y:I

    .line 335
    .line 336
    goto :goto_7

    .line 337
    :cond_e
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    .line 338
    .line 339
    .line 340
    move-result v0

    .line 341
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 342
    .line 343
    invoke-virtual {v5}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    .line 344
    .line 345
    .line 346
    move-result v5

    .line 347
    add-int/2addr v0, v5

    .line 348
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    .line 349
    .line 350
    .line 351
    move-result v5

    .line 352
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 353
    .line 354
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    .line 355
    .line 356
    .line 357
    move-result v6

    .line 358
    add-int/2addr v5, v6

    .line 359
    :goto_7
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 360
    .line 361
    invoke-virtual {v6, v2, v2}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->setScrollAxisValue(II)V

    .line 362
    .line 363
    .line 364
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    .line 365
    .line 366
    .line 367
    move-result v2

    .line 368
    add-int/2addr v2, v0

    .line 369
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 370
    .line 371
    .line 372
    move-result v6

    .line 373
    add-int/2addr v6, v5

    .line 374
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 375
    .line 376
    invoke-virtual {v7}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn()Z

    .line 377
    .line 378
    .line 379
    move-result v7

    .line 380
    if-nez v7, :cond_f

    .line 381
    .line 382
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 383
    .line 384
    invoke-virtual {v7}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScrollerFinished()Z

    .line 385
    .line 386
    .line 387
    move-result v7

    .line 388
    if-eqz v7, :cond_f

    .line 389
    .line 390
    invoke-virtual {p0, v0, v5, v2, v6}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    .line 391
    .line 392
    .line 393
    move-result-object v7

    .line 394
    invoke-virtual {p0, v7}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 395
    .line 396
    .line 397
    move-result-object v7

    .line 398
    iget v8, v7, Landroid/graphics/Point;->x:I

    .line 399
    .line 400
    add-int/2addr v2, v8

    .line 401
    add-int/2addr v0, v8

    .line 402
    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 403
    .line 404
    goto :goto_8

    .line 405
    :cond_f
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 406
    .line 407
    .line 408
    move-result v7

    .line 409
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 410
    .line 411
    .line 412
    move-result v8

    .line 413
    if-gt v7, v8, :cond_10

    .line 414
    .line 415
    invoke-virtual {p0, v0, v5, v2, v6}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    .line 416
    .line 417
    .line 418
    move-result-object v7

    .line 419
    invoke-virtual {p0, v7}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 420
    .line 421
    .line 422
    move-result-object v7

    .line 423
    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 424
    .line 425
    :goto_8
    add-int/2addr v5, v7

    .line 426
    add-int/2addr v6, v7

    .line 427
    :cond_10
    invoke-virtual {v1, v0, v5, v2, v6}, Landroid/view/View;->layout(IIII)V

    .line 428
    .line 429
    .line 430
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 431
    .line 432
    if-lez v1, :cond_11

    .line 433
    .line 434
    sub-int/2addr v1, v3

    .line 435
    invoke-direct {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 436
    .line 437
    .line 438
    move-result-object v1

    .line 439
    invoke-virtual {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 440
    .line 441
    .line 442
    move-result-object v7

    .line 443
    iget v7, v7, Landroid/graphics/Point;->x:I

    .line 444
    .line 445
    add-int/lit8 v7, v7, 0x14

    .line 446
    .line 447
    iget v8, v4, Landroid/graphics/Point;->x:I

    .line 448
    .line 449
    add-int/2addr v7, v8

    .line 450
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    .line 451
    .line 452
    .line 453
    move-result v8

    .line 454
    sub-int v8, v0, v8

    .line 455
    .line 456
    sub-int/2addr v8, v7

    .line 457
    add-int v9, v6, v5

    .line 458
    .line 459
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 460
    .line 461
    .line 462
    move-result v10

    .line 463
    sub-int v10, v9, v10

    .line 464
    .line 465
    div-int/lit8 v10, v10, 0x2

    .line 466
    .line 467
    sub-int/2addr v0, v7

    .line 468
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 469
    .line 470
    .line 471
    move-result v7

    .line 472
    add-int/2addr v9, v7

    .line 473
    div-int/lit8 v9, v9, 0x2

    .line 474
    .line 475
    invoke-virtual {v1, v8, v10, v0, v9}, Landroid/view/View;->layout(IIII)V

    .line 476
    .line 477
    .line 478
    :cond_11
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 479
    .line 480
    add-int/2addr v0, v3

    .line 481
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 482
    .line 483
    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    .line 484
    .line 485
    .line 486
    move-result v1

    .line 487
    if-ge v0, v1, :cond_12

    .line 488
    .line 489
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 490
    .line 491
    add-int/2addr v0, v3

    .line 492
    invoke-direct {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 493
    .line 494
    .line 495
    move-result-object v0

    .line 496
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 497
    .line 498
    .line 499
    move-result-object v1

    .line 500
    iget v3, v4, Landroid/graphics/Point;->x:I

    .line 501
    .line 502
    add-int/lit8 v3, v3, 0x14

    .line 503
    .line 504
    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 505
    .line 506
    add-int/2addr v3, v1

    .line 507
    add-int v1, v2, v3

    .line 508
    .line 509
    add-int/2addr v6, v5

    .line 510
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 511
    .line 512
    .line 513
    move-result v4

    .line 514
    sub-int v4, v6, v4

    .line 515
    .line 516
    div-int/lit8 v4, v4, 0x2

    .line 517
    .line 518
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 519
    .line 520
    .line 521
    move-result v5

    .line 522
    add-int/2addr v2, v5

    .line 523
    add-int/2addr v2, v3

    .line 524
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 525
    .line 526
    .line 527
    move-result v3

    .line 528
    add-int/2addr v6, v3

    .line 529
    div-int/lit8 v6, v6, 0x2

    .line 530
    .line 531
    invoke-virtual {v0, v1, v4, v2, v6}, Landroid/view/View;->layout(IIII)V

    .line 532
    .line 533
    .line 534
    :cond_12
    return-void
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private layout_Vertical()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isResetLayout:Z

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    const/4 v3, 0x1

    .line 15
    if-nez v1, :cond_5

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    add-int/2addr v4, v5

    .line 32
    iget v5, v1, Landroid/graphics/Point;->y:I

    .line 33
    .line 34
    add-int/2addr v4, v5

    .line 35
    add-int/lit8 v4, v4, 0xa

    .line 36
    .line 37
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 38
    .line 39
    invoke-virtual {v5}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    add-int/2addr v4, v5

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    div-int/lit8 v5, v5, 0x2

    .line 49
    .line 50
    if-ge v4, v5, :cond_0

    .line 51
    .line 52
    iget v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 53
    .line 54
    add-int/2addr v4, v3

    .line 55
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 56
    .line 57
    invoke-interface {v5}, Landroid/widget/Adapter;->getCount()I

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    if-ge v4, v5, :cond_0

    .line 62
    .line 63
    iget-object v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 64
    .line 65
    invoke-virtual {v4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling()Z

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    if-nez v4, :cond_0

    .line 70
    .line 71
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postUnRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 72
    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 75
    .line 76
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 77
    .line 78
    .line 79
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 80
    .line 81
    add-int/2addr v1, v3

    .line 82
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 83
    .line 84
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    iget v1, v1, Landroid/graphics/Point;->y:I

    .line 93
    .line 94
    sub-int/2addr v4, v1

    .line 95
    add-int/lit8 v4, v4, -0xa

    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 98
    .line 99
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    add-int/2addr v4, v1

    .line 104
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    div-int/lit8 v1, v1, 0x2

    .line 109
    .line 110
    if-lt v4, v1, :cond_1

    .line 111
    .line 112
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 113
    .line 114
    if-lez v1, :cond_1

    .line 115
    .line 116
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 117
    .line 118
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isOnFling()Z

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    if-nez v1, :cond_1

    .line 123
    .line 124
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postUnRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 125
    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 128
    .line 129
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 130
    .line 131
    .line 132
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 133
    .line 134
    sub-int/2addr v1, v3

    .line 135
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 136
    .line 137
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 141
    .line 142
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 143
    .line 144
    .line 145
    move-result v1

    .line 146
    new-array v4, v1, [I

    .line 147
    .line 148
    const/4 v5, 0x0

    .line 149
    :goto_1
    if-ge v5, v1, :cond_2

    .line 150
    .line 151
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 152
    .line 153
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I

    .line 154
    .line 155
    .line 156
    move-result v6

    .line 157
    aput v6, v4, v5

    .line 158
    .line 159
    add-int/lit8 v5, v5, 0x1

    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_2
    const/4 v5, 0x0

    .line 163
    :goto_2
    if-ge v5, v1, :cond_c

    .line 164
    .line 165
    aget v6, v4, v5

    .line 166
    .line 167
    iget v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 168
    .line 169
    add-int/lit8 v8, v7, -0x1

    .line 170
    .line 171
    if-lt v6, v8, :cond_3

    .line 172
    .line 173
    add-int/lit8 v7, v7, 0x1

    .line 174
    .line 175
    if-le v6, v7, :cond_4

    .line 176
    .line 177
    :cond_3
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 178
    .line 179
    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 180
    .line 181
    .line 182
    move-result-object v6

    .line 183
    check-cast v6, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 184
    .line 185
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->releaseResources()V

    .line 186
    .line 187
    .line 188
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 189
    .line 190
    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 194
    .line 195
    .line 196
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 197
    .line 198
    aget v7, v4, v5

    .line 199
    .line 200
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 201
    .line 202
    .line 203
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 204
    .line 205
    goto :goto_2

    .line 206
    :cond_5
    iput-boolean v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isResetLayout:Z

    .line 207
    .line 208
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 209
    .line 210
    invoke-virtual {v1, v2, v2}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->setScrollAxisValue(II)V

    .line 211
    .line 212
    .line 213
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 214
    .line 215
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 216
    .line 217
    .line 218
    move-result v1

    .line 219
    new-array v4, v1, [I

    .line 220
    .line 221
    const/4 v5, 0x0

    .line 222
    :goto_3
    if-ge v5, v1, :cond_6

    .line 223
    .line 224
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 225
    .line 226
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I

    .line 227
    .line 228
    .line 229
    move-result v6

    .line 230
    aput v6, v4, v5

    .line 231
    .line 232
    add-int/lit8 v5, v5, 0x1

    .line 233
    .line 234
    goto :goto_3

    .line 235
    :cond_6
    const/4 v5, 0x0

    .line 236
    const/4 v6, 0x0

    .line 237
    :goto_4
    if-ge v5, v1, :cond_a

    .line 238
    .line 239
    aget v7, v4, v5

    .line 240
    .line 241
    iget v8, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 242
    .line 243
    add-int/lit8 v9, v8, -0x1

    .line 244
    .line 245
    if-lt v7, v9, :cond_7

    .line 246
    .line 247
    add-int/lit8 v8, v8, 0x1

    .line 248
    .line 249
    if-le v7, v8, :cond_9

    .line 250
    .line 251
    :cond_7
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 252
    .line 253
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 254
    .line 255
    .line 256
    move-result-object v6

    .line 257
    check-cast v6, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 258
    .line 259
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->releaseResources()V

    .line 260
    .line 261
    .line 262
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 263
    .line 264
    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 268
    .line 269
    .line 270
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 271
    .line 272
    aget v7, v4, v5

    .line 273
    .line 274
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->remove(I)V

    .line 275
    .line 276
    .line 277
    aget v6, v4, v5

    .line 278
    .line 279
    iget v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 280
    .line 281
    if-ne v6, v7, :cond_8

    .line 282
    .line 283
    const/4 v6, 0x1

    .line 284
    goto :goto_5

    .line 285
    :cond_8
    const/4 v6, 0x0

    .line 286
    :cond_9
    :goto_5
    add-int/lit8 v5, v5, 0x1

    .line 287
    .line 288
    goto :goto_4

    .line 289
    :cond_a
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 290
    .line 291
    const/high16 v4, 0x42c80000    # 100.0f

    .line 292
    .line 293
    mul-float v1, v1, v4

    .line 294
    .line 295
    float-to-int v1, v1

    .line 296
    const/16 v4, 0x64

    .line 297
    .line 298
    if-ne v1, v4, :cond_b

    .line 299
    .line 300
    if-nez v6, :cond_c

    .line 301
    .line 302
    :cond_b
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 303
    .line 304
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 305
    .line 306
    .line 307
    :cond_c
    if-nez v0, :cond_d

    .line 308
    .line 309
    const/4 v0, 0x1

    .line 310
    goto :goto_6

    .line 311
    :cond_d
    const/4 v0, 0x0

    .line 312
    :goto_6
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 313
    .line 314
    invoke-direct {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 315
    .line 316
    .line 317
    move-result-object v1

    .line 318
    invoke-virtual {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 319
    .line 320
    .line 321
    move-result-object v4

    .line 322
    if-eqz v0, :cond_e

    .line 323
    .line 324
    iget v0, v4, Landroid/graphics/Point;->x:I

    .line 325
    .line 326
    iget v5, v4, Landroid/graphics/Point;->y:I

    .line 327
    .line 328
    goto :goto_7

    .line 329
    :cond_e
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    .line 330
    .line 331
    .line 332
    move-result v0

    .line 333
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 334
    .line 335
    invoke-virtual {v5}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    .line 336
    .line 337
    .line 338
    move-result v5

    .line 339
    add-int/2addr v0, v5

    .line 340
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    .line 341
    .line 342
    .line 343
    move-result v5

    .line 344
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 345
    .line 346
    invoke-virtual {v6}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    .line 347
    .line 348
    .line 349
    move-result v6

    .line 350
    add-int/2addr v5, v6

    .line 351
    :goto_7
    iget-object v6, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 352
    .line 353
    invoke-virtual {v6, v2, v2}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->setScrollAxisValue(II)V

    .line 354
    .line 355
    .line 356
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    .line 357
    .line 358
    .line 359
    move-result v2

    .line 360
    add-int/2addr v2, v0

    .line 361
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 362
    .line 363
    .line 364
    move-result v6

    .line 365
    add-int/2addr v6, v5

    .line 366
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 367
    .line 368
    invoke-virtual {v7}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn()Z

    .line 369
    .line 370
    .line 371
    move-result v7

    .line 372
    if-nez v7, :cond_f

    .line 373
    .line 374
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 375
    .line 376
    invoke-virtual {v7}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScrollerFinished()Z

    .line 377
    .line 378
    .line 379
    move-result v7

    .line 380
    if-eqz v7, :cond_f

    .line 381
    .line 382
    invoke-virtual {p0, v0, v5, v2, v6}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    .line 383
    .line 384
    .line 385
    move-result-object v7

    .line 386
    invoke-virtual {p0, v7}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 387
    .line 388
    .line 389
    move-result-object v7

    .line 390
    iget v8, v7, Landroid/graphics/Point;->x:I

    .line 391
    .line 392
    add-int/2addr v2, v8

    .line 393
    add-int/2addr v0, v8

    .line 394
    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 395
    .line 396
    add-int/2addr v5, v7

    .line 397
    add-int/2addr v6, v7

    .line 398
    goto :goto_8

    .line 399
    :cond_f
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    .line 400
    .line 401
    .line 402
    move-result v7

    .line 403
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 404
    .line 405
    .line 406
    move-result v8

    .line 407
    if-gt v7, v8, :cond_10

    .line 408
    .line 409
    invoke-virtual {p0, v0, v5, v2, v6}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    .line 410
    .line 411
    .line 412
    move-result-object v7

    .line 413
    invoke-virtual {p0, v7}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 414
    .line 415
    .line 416
    move-result-object v7

    .line 417
    iget v7, v7, Landroid/graphics/Point;->x:I

    .line 418
    .line 419
    add-int/2addr v2, v7

    .line 420
    add-int/2addr v0, v7

    .line 421
    :cond_10
    :goto_8
    new-instance v7, Ljava/lang/StringBuilder;

    .line 422
    .line 423
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 424
    .line 425
    .line 426
    const-string v8, "layout_Vertical: "

    .line 427
    .line 428
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    iget v8, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 432
    .line 433
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 434
    .line 435
    .line 436
    const-string v8, ", cvTop: "

    .line 437
    .line 438
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    .line 440
    .line 441
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 442
    .line 443
    .line 444
    const-string v8, ", cvLeft: "

    .line 445
    .line 446
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    .line 448
    .line 449
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 450
    .line 451
    .line 452
    const-string v8, ", cvRight: "

    .line 453
    .line 454
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    .line 456
    .line 457
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 458
    .line 459
    .line 460
    const-string v8, " , cvBottom: "

    .line 461
    .line 462
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463
    .line 464
    .line 465
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 466
    .line 467
    .line 468
    invoke-virtual {v1, v0, v5, v2, v6}, Landroid/view/View;->layout(IIII)V

    .line 469
    .line 470
    .line 471
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 472
    .line 473
    if-lez v1, :cond_11

    .line 474
    .line 475
    sub-int/2addr v1, v3

    .line 476
    invoke-direct {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 477
    .line 478
    .line 479
    move-result-object v1

    .line 480
    invoke-virtual {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 481
    .line 482
    .line 483
    move-result-object v7

    .line 484
    iget v7, v7, Landroid/graphics/Point;->y:I

    .line 485
    .line 486
    add-int/lit8 v7, v7, 0x14

    .line 487
    .line 488
    iget v8, v4, Landroid/graphics/Point;->y:I

    .line 489
    .line 490
    add-int/2addr v7, v8

    .line 491
    sub-int v8, v5, v7

    .line 492
    .line 493
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 494
    .line 495
    .line 496
    move-result v9

    .line 497
    sub-int/2addr v8, v9

    .line 498
    sub-int v7, v6, v7

    .line 499
    .line 500
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 501
    .line 502
    .line 503
    move-result v9

    .line 504
    sub-int/2addr v7, v9

    .line 505
    invoke-virtual {v1, v0, v8, v2, v7}, Landroid/view/View;->layout(IIII)V

    .line 506
    .line 507
    .line 508
    :cond_11
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 509
    .line 510
    add-int/2addr v1, v3

    .line 511
    iget-object v7, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 512
    .line 513
    invoke-interface {v7}, Landroid/widget/Adapter;->getCount()I

    .line 514
    .line 515
    .line 516
    move-result v7

    .line 517
    if-ge v1, v7, :cond_12

    .line 518
    .line 519
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 520
    .line 521
    add-int/2addr v1, v3

    .line 522
    invoke-direct {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->createPageView(I)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 523
    .line 524
    .line 525
    move-result-object v1

    .line 526
    invoke-virtual {p0, v1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 527
    .line 528
    .line 529
    move-result-object v3

    .line 530
    iget v4, v4, Landroid/graphics/Point;->y:I

    .line 531
    .line 532
    add-int/lit8 v4, v4, 0x14

    .line 533
    .line 534
    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 535
    .line 536
    add-int/2addr v4, v3

    .line 537
    add-int/2addr v5, v4

    .line 538
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 539
    .line 540
    .line 541
    move-result v3

    .line 542
    add-int/2addr v5, v3

    .line 543
    add-int/2addr v6, v4

    .line 544
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 545
    .line 546
    .line 547
    move-result v3

    .line 548
    add-int/2addr v6, v3

    .line 549
    invoke-virtual {v1, v0, v5, v2, v6}, Landroid/view/View;->layout(IIII)V

    .line 550
    .line 551
    .line 552
    :cond_12
    return-void
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/system/beans/pagelist/APageListView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/system/beans/pagelist/APageListView;)Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/system/beans/pagelist/APageListView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInit:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 14
    .line 15
    instance-of v2, v1, Lcom/intsig/office/system/beans/pagelist/APageListAdapter;

    .line 16
    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    check-cast v1, Lcom/intsig/office/system/beans/pagelist/APageListAdapter;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListAdapter;->dispose()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 25
    .line 26
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 27
    .line 28
    if-eqz v1, :cond_3

    .line 29
    .line 30
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    const/4 v2, 0x0

    .line 35
    :goto_0
    if-ge v2, v1, :cond_2

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 38
    .line 39
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    check-cast v3, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 44
    .line 45
    invoke-virtual {v3}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->dispose()V

    .line 46
    .line 47
    .line 48
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 52
    .line 53
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 57
    .line 58
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 59
    .line 60
    if-eqz v1, :cond_5

    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-eqz v2, :cond_4

    .line 71
    .line 72
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    check-cast v2, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 77
    .line 78
    invoke-virtual {v2}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->dispose()V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_4
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 85
    .line 86
    .line 87
    iput-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageViewCache:Ljava/util/LinkedList;

    .line 88
    .line 89
    :cond_5
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageIndex()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 6
    .line 7
    if-ne v0, v1, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isTouchEventIn()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->isScrollerFinished()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 27
    .line 28
    invoke-interface {v0, p1, p2}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getAdapter()Landroid/widget/Adapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Point;

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 11
    .line 12
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    iget v3, p1, Landroid/graphics/Rect;->top:I

    .line 17
    .line 18
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 23
    .line 24
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 29
    .line 30
    .line 31
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getCurrentPageNumber()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDisplayedPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFitSizeState()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    sub-int/2addr v1, v2

    .line 16
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    sub-int/2addr v0, v2

    .line 29
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    const/4 v2, 0x2

    .line 34
    if-ge v1, v2, :cond_0

    .line 35
    .line 36
    if-ge v0, v2, :cond_0

    .line 37
    .line 38
    const/4 v2, 0x3

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    if-ge v1, v2, :cond_1

    .line 41
    .line 42
    if-lt v0, v2, :cond_1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    if-lt v1, v2, :cond_2

    .line 46
    .line 47
    if-ge v0, v2, :cond_2

    .line 48
    .line 49
    const/4 v2, 0x1

    .line 50
    goto :goto_0

    .line 51
    :cond_2
    const/4 v2, 0x0

    .line 52
    :goto_0
    return v2
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getFitZoom()F
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getFitZoom(I)F

    move-result v0

    return v0
.end method

.method public getFitZoom(I)F
    .locals 7

    .line 2
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    const/high16 v1, 0x3f800000    # 1.0f

    if-ltz v0, :cond_7

    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    invoke-interface {v2}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    goto/16 :goto_2

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    iget v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    invoke-interface {v0, v2}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageSize(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    :goto_0
    if-nez v2, :cond_2

    if-eqz v4, :cond_2

    .line 7
    instance-of v5, v4, Landroid/view/View;

    if-nez v5, :cond_1

    goto :goto_1

    .line 8
    :cond_1
    move-object v2, v4

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 9
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 10
    invoke-interface {v4}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    move v6, v3

    move v3, v2

    move v2, v6

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v2, :cond_7

    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    const/high16 v4, 0x40400000    # 3.0f

    if-nez p1, :cond_5

    .line 11
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->isIgnoreOriginalSize()Z

    move-result p1

    if-nez p1, :cond_4

    int-to-float p1, v2

    .line 12
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr p1, v2

    int-to-float v2, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v2, v0

    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    return p1

    :cond_4
    int-to-float p1, v2

    .line 13
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr p1, v1

    int-to-float v1, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v1, v0

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    return p1

    :cond_5
    const/4 v5, 0x1

    if-ne p1, v5, :cond_6

    int-to-float p1, v2

    .line 14
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    return p1

    :cond_6
    const/4 v2, 0x2

    if-ne p1, v2, :cond_7

    int-to-float p1, v3

    .line 15
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result p1

    return p1

    :cond_7
    :goto_2
    return v1
.end method

.method public getModel()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getModel()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getPageListItem(ILandroid/view/View;Landroid/view/ViewGroup;)Lcom/intsig/office/system/beans/pagelist/APageListItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageListItem(ILandroid/view/View;Landroid/view/ViewGroup;)Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method protected getPageListViewListener()Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Point;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    sub-int/2addr v1, v2

    .line 12
    div-int/lit8 v1, v1, 0x2

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    sub-int/2addr v3, p1

    .line 28
    div-int/lit8 v3, v3, 0x2

    .line 29
    .line 30
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 35
    .line 36
    .line 37
    return-object v0
    .line 38
.end method

.method protected getScrollBounds(IIII)Landroid/graphics/Rect;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int/2addr v0, p3

    neg-int p1, p1

    .line 2
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p3

    sub-int/2addr p3, p4

    neg-int p2, p2

    if-le v0, p1, :cond_0

    add-int/2addr v0, p1

    .line 3
    div-int/lit8 v0, v0, 0x2

    move p1, v0

    :cond_0
    if-le p3, p2, :cond_1

    add-int/2addr p3, p2

    .line 4
    div-int/lit8 p3, p3, 0x2

    move p2, p3

    .line 5
    :cond_1
    new-instance p4, Landroid/graphics/Rect;

    invoke-direct {p4, v0, p3, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object p4
.end method

.method protected getScrollBounds(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4

    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {v1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    move-result v1

    add-int/2addr v0, v1

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {v2}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {v3}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    move-result v3

    add-int/2addr v2, v3

    .line 9
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    add-int/2addr v3, p1

    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {p1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    move-result p1

    add-int/2addr v3, p1

    .line 10
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    move-result-object p1

    return-object p1
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public gotoPage(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public init()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInit:Z

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isInit()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInit:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isInitZoom()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInitZoom:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPointVisibleOnScreen(II)Z
    .locals 4

    .line 1
    int-to-float p1, p1

    .line 2
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 3
    .line 4
    mul-float p1, p1, v0

    .line 5
    .line 6
    float-to-int p1, p1

    .line 7
    int-to-float p2, p2

    .line 8
    mul-float p2, p2, v0

    .line 9
    .line 10
    float-to-int p2, p2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    return v1

    .line 19
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    sub-int/2addr v2, v3

    .line 32
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    sub-int/2addr v3, v0

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    add-int/2addr v0, v2

    .line 50
    if-ge v2, v0, :cond_1

    .line 51
    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    add-int/2addr v0, v3

    .line 57
    if-ge v3, v0, :cond_1

    .line 58
    .line 59
    if-lt p1, v2, :cond_1

    .line 60
    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    add-int/2addr v2, v0

    .line 66
    if-ge p1, v2, :cond_1

    .line 67
    .line 68
    if-lt p2, v3, :cond_1

    .line 69
    .line 70
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    add-int/2addr v3, p1

    .line 75
    if-ge p2, v3, :cond_1

    .line 76
    .line 77
    const/4 v1, 0x1

    .line 78
    :cond_1
    return v1
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public measureNeedZoom()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public nextPageView()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 6
    .line 7
    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 17
    .line 18
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->slideViewOntoScreen(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/intsig/office/system/beans/pagelist/APageListView$4;

    .line 41
    .line 42
    invoke-direct {v0, p0}, Lcom/intsig/office/system/beans/pagelist/APageListView$4;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;)V

    .line 43
    .line 44
    .line 45
    const-wide/16 v1, 0x1

    .line 46
    .line 47
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 51
    .line 52
    const/4 v1, 0x0

    .line 53
    invoke-interface {v0, v1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->updateStutus(Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public notifyDataChange()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isConfigurationChanged:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 2
    .line 3
    .line 4
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInit:Z

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 10
    .line 11
    invoke-interface {p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->getPageListViewMovingPosition()B

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->layout_Horizontal()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->layout_Vertical()V

    .line 22
    .line 23
    .line 24
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 25
    .line 26
    .line 27
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isConfigurationChanged:Z

    .line 28
    .line 29
    if-eqz p1, :cond_2

    .line 30
    .line 31
    const/4 p1, 0x0

    .line 32
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isConfigurationChanged:Z

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    invoke-virtual {p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 41
    .line 42
    .line 43
    :cond_2
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 p2, 0x0

    .line 9
    :goto_0
    if-ge p2, p1, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    instance-of v1, v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageWidth()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    int-to-float v1, v1

    .line 26
    iget v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 27
    .line 28
    mul-float v1, v1, v2

    .line 29
    .line 30
    float-to-int v1, v1

    .line 31
    const/high16 v2, 0x40000000    # 2.0f

    .line 32
    .line 33
    or-int/2addr v1, v2

    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getPageHeight()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    int-to-float v3, v3

    .line 39
    iget v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 40
    .line 41
    mul-float v3, v3, v4

    .line 42
    .line 43
    float-to-int v3, v3

    .line 44
    or-int/2addr v2, v3

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 46
    .line 47
    .line 48
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isConfigurationChanged:Z

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getFitZoom()F

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iget p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 13
    .line 14
    cmpg-float p2, p2, p1

    .line 15
    .line 16
    if-gez p2, :cond_0

    .line 17
    .line 18
    const/4 p2, 0x0

    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setZoom(FZ)V

    .line 20
    .line 21
    .line 22
    iput-boolean p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInit:Z

    .line 23
    .line 24
    new-instance p2, Lcom/intsig/office/system/beans/pagelist/APageListView$2;

    .line 25
    .line 26
    invoke-direct {p2, p0}, Lcom/intsig/office/system/beans/pagelist/APageListView$2;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;)V

    .line 27
    .line 28
    .line 29
    const-wide/16 p3, 0x1

    .line 30
    .line 31
    invoke-virtual {p0, p2, p3, p4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 32
    .line 33
    .line 34
    iget-object p2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 35
    .line 36
    const/high16 p3, 0x42c80000    # 100.0f

    .line 37
    .line 38
    mul-float p1, p1, p3

    .line 39
    .line 40
    float-to-int p1, p1

    .line 41
    invoke-interface {p2, p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->changeZoom(I)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListItem;->getControl()Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getCalloutManager()Lcom/intsig/office/system/beans/CalloutView/CalloutManager;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/CalloutView/CalloutManager;->getDrawingMode()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    const/4 p1, 0x0

    .line 26
    return p1

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 28
    .line 29
    invoke-virtual {v0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->processOnTouch(Landroid/view/MotionEvent;)Z

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 33
    .line 34
    const/4 v4, 0x0

    .line 35
    const/high16 v5, -0x40800000    # -1.0f

    .line 36
    .line 37
    const/high16 v6, -0x40800000    # -1.0f

    .line 38
    .line 39
    const/4 v7, 0x0

    .line 40
    move-object v2, p0

    .line 41
    move-object v3, p1

    .line 42
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 43
    .line 44
    .line 45
    const/4 p1, 0x1

    .line 46
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Lcom/intsig/office/system/beans/pagelist/APageListView$7;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView$7;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected postUnRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Lcom/intsig/office/system/beans/pagelist/APageListView$6;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView$6;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public previousPageview()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 7
    .line 8
    add-int/lit8 v0, v0, -0x1

    .line 9
    .line 10
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 19
    .line 20
    add-int/lit8 v1, v1, -0x1

    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    .line 25
    .line 26
    invoke-virtual {v1, v0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->slideViewOntoScreen(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 27
    .line 28
    .line 29
    :cond_1
    return-void
    .line 30
.end method

.method public requestLayout()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isDoRequestLayout:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDoRequstLayout(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isDoRequestLayout:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFitSize(I)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getFitZoom(I)F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setZoom(FZ)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setInitZoom(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInitZoom:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setItemPointVisibleOnScreen(II)V
    .locals 9

    .line 1
    if-gez p1, :cond_0

    .line 2
    .line 3
    if-gez p2, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_8

    .line 11
    .line 12
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/system/beans/pagelist/APageListView;->isPointVisibleOnScreen(II)Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-nez v1, :cond_8

    .line 17
    .line 18
    int-to-float p1, p1

    .line 19
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 20
    .line 21
    mul-float p1, p1, v1

    .line 22
    .line 23
    float-to-int p1, p1

    .line 24
    int-to-float p2, p2

    .line 25
    mul-float p2, p2, v1

    .line 26
    .line 27
    float-to-int p2, p2

    .line 28
    const/4 v1, 0x0

    .line 29
    if-lez p1, :cond_2

    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    add-int/2addr v2, p1

    .line 36
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-le v2, v3, :cond_1

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    sub-int/2addr p1, v2

    .line 51
    :cond_1
    neg-int p1, p1

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    const/4 p1, 0x0

    .line 54
    :goto_0
    if-lez p2, :cond_4

    .line 55
    .line 56
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    add-int/2addr v1, p2

    .line 61
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-le v1, v2, :cond_3

    .line 66
    .line 67
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 68
    .line 69
    .line 70
    move-result p2

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    sub-int/2addr p2, v1

    .line 76
    :cond_3
    neg-int v1, p2

    .line 77
    :cond_4
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    add-int/2addr v2, p1

    .line 86
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    add-int/2addr v3, v1

    .line 91
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    if-gt v4, v5, :cond_5

    .line 100
    .line 101
    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScrollBounds(IIII)Landroid/graphics/Rect;

    .line 102
    .line 103
    .line 104
    move-result-object v4

    .line 105
    invoke-virtual {p0, v4}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCorrection(Landroid/graphics/Rect;)Landroid/graphics/Point;

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    iget v4, v4, Landroid/graphics/Point;->y:I

    .line 110
    .line 111
    add-int/2addr v1, v4

    .line 112
    add-int/2addr v3, v4

    .line 113
    :cond_5
    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 114
    .line 115
    .line 116
    iget v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 117
    .line 118
    if-lez v4, :cond_6

    .line 119
    .line 120
    iget-object v5, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 121
    .line 122
    add-int/lit8 v4, v4, -0x1

    .line 123
    .line 124
    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    move-result-object v4

    .line 128
    check-cast v4, Landroid/view/View;

    .line 129
    .line 130
    if-eqz v4, :cond_6

    .line 131
    .line 132
    invoke-virtual {p0, v4}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 133
    .line 134
    .line 135
    move-result-object v5

    .line 136
    iget v5, v5, Landroid/graphics/Point;->x:I

    .line 137
    .line 138
    add-int/lit8 v5, v5, 0x14

    .line 139
    .line 140
    iget v6, p2, Landroid/graphics/Point;->x:I

    .line 141
    .line 142
    add-int/2addr v5, v6

    .line 143
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    .line 144
    .line 145
    .line 146
    move-result v6

    .line 147
    sub-int v6, p1, v6

    .line 148
    .line 149
    sub-int/2addr v6, v5

    .line 150
    add-int v7, v3, v1

    .line 151
    .line 152
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    .line 153
    .line 154
    .line 155
    move-result v8

    .line 156
    sub-int v8, v7, v8

    .line 157
    .line 158
    div-int/lit8 v8, v8, 0x2

    .line 159
    .line 160
    sub-int/2addr p1, v5

    .line 161
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    .line 162
    .line 163
    .line 164
    move-result v5

    .line 165
    add-int/2addr v7, v5

    .line 166
    div-int/lit8 v7, v7, 0x2

    .line 167
    .line 168
    invoke-virtual {v4, v6, v8, p1, v7}, Landroid/view/View;->layout(IIII)V

    .line 169
    .line 170
    .line 171
    :cond_6
    iget p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 172
    .line 173
    add-int/lit8 p1, p1, 0x1

    .line 174
    .line 175
    iget-object v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 176
    .line 177
    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    .line 178
    .line 179
    .line 180
    move-result v4

    .line 181
    if-ge p1, v4, :cond_7

    .line 182
    .line 183
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->childViewsCache:Landroid/util/SparseArray;

    .line 184
    .line 185
    iget v4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 186
    .line 187
    add-int/lit8 v4, v4, 0x1

    .line 188
    .line 189
    invoke-virtual {p1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    check-cast p1, Landroid/view/View;

    .line 194
    .line 195
    if-eqz p1, :cond_7

    .line 196
    .line 197
    invoke-virtual {p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getScreenSizeOffset(Landroid/view/View;)Landroid/graphics/Point;

    .line 198
    .line 199
    .line 200
    move-result-object v4

    .line 201
    iget p2, p2, Landroid/graphics/Point;->x:I

    .line 202
    .line 203
    add-int/lit8 p2, p2, 0x14

    .line 204
    .line 205
    iget v4, v4, Landroid/graphics/Point;->x:I

    .line 206
    .line 207
    add-int/2addr p2, v4

    .line 208
    add-int v4, v2, p2

    .line 209
    .line 210
    add-int/2addr v3, v1

    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    .line 212
    .line 213
    .line 214
    move-result v1

    .line 215
    sub-int v1, v3, v1

    .line 216
    .line 217
    div-int/lit8 v1, v1, 0x2

    .line 218
    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    .line 220
    .line 221
    .line 222
    move-result v5

    .line 223
    add-int/2addr v2, v5

    .line 224
    add-int/2addr v2, p2

    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    .line 226
    .line 227
    .line 228
    move-result p2

    .line 229
    add-int/2addr v3, p2

    .line 230
    div-int/lit8 v3, v3, 0x2

    .line 231
    .line 232
    invoke-virtual {p1, v4, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 233
    .line 234
    .line 235
    :cond_7
    invoke-virtual {p0, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V

    .line 236
    .line 237
    .line 238
    :cond_8
    return-void
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public setSelection(I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoom(FII)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setZoom(FIIZ)V

    return-void
.end method

.method public setZoom(FIIZ)V
    .locals 3

    const v0, 0x4b189680    # 1.0E7f

    mul-float v1, p1, v0

    float-to-int v1, v1

    .line 3
    iget v2, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    mul-float v2, v2, v0

    float-to-int v0, v2

    if-ne v1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->isInitZoom:Z

    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_1

    if-ne p3, v0, :cond_1

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p3

    div-int/lit8 p3, p3, 0x2

    .line 7
    :cond_1
    iget v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 8
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    .line 9
    iget-object v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float p1, p1, v2

    float-to-int p1, p1

    invoke-interface {v1, p1}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->changeZoom(I)V

    .line 10
    new-instance p1, Lcom/intsig/office/system/beans/pagelist/APageListView$5;

    invoke-direct {p1, p0, p4}, Lcom/intsig/office/system/beans/pagelist/APageListView$5;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;Z)V

    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    if-eqz p4, :cond_3

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 12
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p4

    .line 13
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    goto :goto_0

    :cond_2
    const/4 p4, 0x0

    const/4 p1, 0x0

    .line 14
    :goto_0
    iget v1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->zoom:F

    div-float/2addr v1, v0

    .line 15
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {v0}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollX()I

    move-result v0

    add-int/2addr p4, v0

    sub-int/2addr p2, p4

    .line 16
    iget-object p4, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    invoke-virtual {p4}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->getScrollY()I

    move-result p4

    add-int/2addr p1, p4

    sub-int/2addr p3, p1

    .line 17
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->eventManage:Lcom/intsig/office/system/beans/pagelist/APageListEventManage;

    int-to-float p2, p2

    mul-float p4, p2, v1

    sub-float/2addr p2, p4

    float-to-int p2, p2

    int-to-float p3, p3

    mul-float v1, v1, p3

    sub-float/2addr p3, v1

    float-to-int p3, p3

    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/system/beans/pagelist/APageListEventManage;->setScrollAxisValue(II)V

    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/pagelist/APageListView;->requestLayout()V

    :cond_3
    return-void
.end method

.method public setZoom(FZ)V
    .locals 1

    const/high16 v0, -0x80000000

    .line 2
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/intsig/office/system/beans/pagelist/APageListView;->setZoom(FIIZ)V

    return-void
.end method

.method public showPDFPageForIndex(I)V
    .locals 3

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageAdapter:Landroid/widget/Adapter;

    .line 4
    .line 5
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iput p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->currentIndex:I

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/office/system/beans/pagelist/APageListView$3;

    .line 15
    .line 16
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/system/beans/pagelist/APageListView$3;-><init>(Lcom/intsig/office/system/beans/pagelist/APageListView;I)V

    .line 17
    .line 18
    .line 19
    const-wide/16 v1, 0x1

    .line 20
    .line 21
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/office/system/beans/pagelist/APageListView;->pageListViewListener:Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    invoke-interface {p1, v0}, Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;->updateStutus(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public interface abstract Lcom/intsig/office/system/beans/pagelist/IPageListViewListener;
.super Ljava/lang/Object;
.source "IPageListViewListener.java"


# static fields
.field public static final Moving_Horizontal:B = 0x0t

.field public static final Moving_Vertical:B = 0x1t

.field public static final ON_CLICK:B = 0xat

.field public static final ON_DOUBLE_TAP:B = 0x8t

.field public static final ON_DOUBLE_TAP_EVENT:B = 0x9t

.field public static final ON_DOWN:B = 0x1t

.field public static final ON_FLING:B = 0x6t

.field public static final ON_LONG_PRESS:B = 0x5t

.field public static final ON_SCROLL:B = 0x4t

.field public static final ON_SHOW_PRESS:B = 0x2t

.field public static final ON_SINGLE_TAP_CONFIRMED:B = 0x7t

.field public static final ON_SINGLE_TAP_UP:B = 0x3t

.field public static final ON_TOUCH:B


# virtual methods
.method public abstract changeZoom(I)V
.end method

.method public abstract exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V
.end method

.method public abstract getModel()Ljava/lang/Object;
.end method

.method public abstract getPageCount()I
.end method

.method public abstract getPageListItem(ILandroid/view/View;Landroid/view/ViewGroup;)Lcom/intsig/office/system/beans/pagelist/APageListItem;
.end method

.method public abstract getPageListViewMovingPosition()B
.end method

.method public abstract getPageSize(I)Landroid/graphics/Rect;
.end method

.method public abstract isChangePage()Z
.end method

.method public abstract isIgnoreOriginalSize()Z
.end method

.method public abstract isInit()Z
.end method

.method public abstract isTouchZoom()Z
.end method

.method public abstract onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z
.end method

.method public abstract resetSearchResult(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V
.end method

.method public abstract setDrawPictrue(Z)V
.end method

.method public abstract updateStutus(Ljava/lang/Object;)V
.end method

.class public abstract Lcom/intsig/office/system/beans/AEventManage;
.super Ljava/lang/Object;
.source "AEventManage.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected control:Lcom/intsig/office/system/IControl;

.field distance:F

.field private downDistance:F

.field private downZoom:F

.field protected gesture:Landroid/view/GestureDetector;

.field protected isFling:Z

.field protected isScroll:Z

.field private isZoom:Z

.field protected mActivePointerId:I

.field protected mMaximumVelocity:I

.field protected mMinimumVelocity:I

.field protected mScroller:Landroid/widget/Scroller;

.field protected mVelocityTracker:Landroid/view/VelocityTracker;

.field protected midXDoublePoint:I

.field protected midYDoublePoint:I

.field protected singleTabup:Z

.field protected zoomChange:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/intsig/office/system/IControl;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isZoom:Z

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput v1, p0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->singleTabup:Z

    .line 11
    .line 12
    const/4 v0, -0x1

    .line 13
    iput v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I

    .line 14
    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/office/system/beans/AEventManage;->downZoom:F

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/office/system/beans/AEventManage;->downDistance:F

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 22
    .line 23
    new-instance p2, Landroid/view/GestureDetector;

    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-direct {p2, p1, p0, v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    .line 28
    .line 29
    .line 30
    iput-object p2, p0, Lcom/intsig/office/system/beans/AEventManage;->gesture:Landroid/view/GestureDetector;

    .line 31
    .line 32
    new-instance p2, Landroid/widget/Scroller;

    .line 33
    .line 34
    invoke-direct {p2, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    iput-object p2, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 38
    .line 39
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    iput p2, p0, Lcom/intsig/office/system/beans/AEventManage;->mMinimumVelocity:I

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    iput p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mMaximumVelocity:I

    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private calculateDistance(Landroid/view/MotionEvent;)F
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    const/4 v2, 0x1

    .line 7
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    sub-float/2addr v1, v3

    .line 12
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    sub-float/2addr v0, p1

    .line 21
    mul-float v1, v1, v1

    .line 22
    .line 23
    mul-float v0, v0, v0

    .line 24
    .line 25
    add-float/2addr v1, v0

    .line 26
    float-to-double v0, v1

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    double-to-float p1, v0

    .line 32
    return p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public computeScroll()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isFling:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isFling:Z

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 17
    .line 18
    const v1, 0x2000000a

    .line 19
    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 26
    .line 27
    const/16 v1, 0x14

    .line 28
    .line 29
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->gesture:Landroid/view/GestureDetector;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 19
    .line 20
    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 21
    .line 22
    .line 23
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public fling(II)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getMiddleXOfDoublePoint()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/AEventManage;->midXDoublePoint:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMiddleYOfDoublePoint()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/system/beans/AEventManage;->midYDoublePoint:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    const/4 v3, 0x0

    .line 15
    const/high16 v4, -0x40800000    # -1.0f

    .line 16
    .line 17
    const/high16 v5, -0x40800000    # -1.0f

    .line 18
    .line 19
    const/16 v6, 0xa

    .line 20
    .line 21
    invoke-interface/range {v0 .. v6}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 10

    .line 1
    const/high16 v0, -0x40800000    # -1.0f

    .line 2
    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 4
    .line 5
    const v2, 0x20000005

    .line 6
    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    invoke-interface {v1, v2, v3}, Lcom/intsig/office/system/IControl;->getActionValue(ILjava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Ljava/lang/Float;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    .line 16
    .line 17
    .line 18
    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 19
    :try_start_1
    iget-object v2, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 20
    .line 21
    const v4, 0x20000006

    .line 22
    .line 23
    .line 24
    invoke-interface {v2, v4, v3}, Lcom/intsig/office/system/IControl;->getActionValue(ILjava/lang/Object;)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Ljava/lang/Float;

    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 31
    .line 32
    .line 33
    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 34
    move v8, v0

    .line 35
    move v7, v1

    .line 36
    goto :goto_1

    .line 37
    :catch_0
    move-exception v2

    .line 38
    goto :goto_0

    .line 39
    :catch_1
    move-exception v2

    .line 40
    const/high16 v1, -0x40800000    # -1.0f

    .line 41
    .line 42
    :goto_0
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 43
    .line 44
    .line 45
    move v7, v1

    .line 46
    const/high16 v8, -0x40800000    # -1.0f

    .line 47
    .line 48
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 49
    .line 50
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 55
    .line 56
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    const/4 v6, 0x0

    .line 61
    const/16 v9, 0x8

    .line 62
    .line 63
    move-object v5, p1

    .line 64
    invoke-interface/range {v3 .. v9}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    return p1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    const/high16 v5, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/16 v7, 0x9

    .line 19
    .line 20
    move-object v3, p1

    .line 21
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    const/high16 v5, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/4 v7, 0x1

    .line 19
    move-object v3, p1

    .line 20
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    return p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v7, 0x6

    .line 14
    move-object v3, p1

    .line 15
    move-object v4, p2

    .line 16
    move v5, p3

    .line 17
    move v6, p4

    .line 18
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    const/high16 v5, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/4 v7, 0x5

    .line 19
    move-object v3, p1

    .line 20
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const/4 v7, 0x4

    .line 17
    move-object v3, p1

    .line 18
    move-object v4, p2

    .line 19
    move v5, p3

    .line 20
    move v6, p4

    .line 21
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    const/high16 v5, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/4 v7, 0x2

    .line 19
    move-object v3, p1

    .line 20
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    const/high16 v5, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v6, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/4 v7, 0x7

    .line 19
    move-object v3, p1

    .line 20
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    return p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->singleTabup:Z

    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    const/4 v4, 0x0

    .line 21
    const/high16 v5, -0x40800000    # -1.0f

    .line 22
    .line 23
    const/high16 v6, -0x40800000    # -1.0f

    .line 24
    .line 25
    const/4 v7, 0x3

    .line 26
    move-object v3, p1

    .line 27
    invoke-interface/range {v1 .. v7}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->gesture:Landroid/view/GestureDetector;

    .line 3
    .line 4
    if-nez v1, :cond_0

    .line 5
    .line 6
    return v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v5, 0x0

    .line 14
    const/high16 v6, -0x40800000    # -1.0f

    .line 15
    .line 16
    const/high16 v7, -0x40800000    # -1.0f

    .line 17
    .line 18
    const/4 v8, 0x0

    .line 19
    move-object v3, p1

    .line 20
    move-object v4, p2

    .line 21
    invoke-interface/range {v2 .. v8}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 22
    .line 23
    .line 24
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    const/4 v1, 0x2

    .line 29
    const/4 v2, 0x1

    .line 30
    if-ne p1, v1, :cond_1

    .line 31
    .line 32
    iput-boolean v2, p0, Lcom/intsig/office/system/beans/AEventManage;->isZoom:Z

    .line 33
    .line 34
    invoke-virtual {p0, p2}, Lcom/intsig/office/system/beans/AEventManage;->zoom(Landroid/view/MotionEvent;)Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    return p1

    .line 39
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/AEventManage;->isZoom:Z

    .line 40
    .line 41
    if-eqz p1, :cond_3

    .line 42
    .line 43
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-ne p1, v2, :cond_2

    .line 48
    .line 49
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isZoom:Z

    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 52
    .line 53
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    const/4 v4, 0x0

    .line 58
    const/4 v5, 0x0

    .line 59
    const/4 v6, 0x0

    .line 60
    const/high16 v7, -0x40800000    # -1.0f

    .line 61
    .line 62
    const/high16 v8, -0x40800000    # -1.0f

    .line 63
    .line 64
    const/16 v9, 0xc

    .line 65
    .line 66
    invoke-interface/range {v3 .. v9}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 67
    .line 68
    .line 69
    :cond_2
    return v2

    .line 70
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->gesture:Landroid/view/GestureDetector;

    .line 71
    .line 72
    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 73
    .line 74
    .line 75
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 76
    .line 77
    if-nez p1, :cond_4

    .line 78
    .line 79
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    iput-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 84
    .line 85
    :cond_4
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 86
    .line 87
    invoke-virtual {p1, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    if-eqz p1, :cond_13

    .line 95
    .line 96
    const/4 p2, -0x1

    .line 97
    const/4 v3, 0x0

    .line 98
    if-eq p1, v2, :cond_6

    .line 99
    .line 100
    const/4 v1, 0x3

    .line 101
    if-eq p1, v1, :cond_5

    .line 102
    .line 103
    goto/16 :goto_6

    .line 104
    .line 105
    :cond_5
    iput p2, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I

    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 108
    .line 109
    if-eqz p1, :cond_14

    .line 110
    .line 111
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    .line 112
    .line 113
    .line 114
    iput-object v3, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 115
    .line 116
    goto/16 :goto_6

    .line 117
    .line 118
    :cond_6
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/AEventManage;->singleTabup:Z

    .line 119
    .line 120
    if-nez p1, :cond_11

    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 123
    .line 124
    iget v4, p0, Lcom/intsig/office/system/beans/AEventManage;->mMaximumVelocity:I

    .line 125
    .line 126
    int-to-float v4, v4

    .line 127
    const/16 v5, 0x3e8

    .line 128
    .line 129
    invoke-virtual {p1, v5, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 130
    .line 131
    .line 132
    iget v4, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I

    .line 133
    .line 134
    invoke-virtual {p1, v4}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    .line 135
    .line 136
    .line 137
    move-result v4

    .line 138
    float-to-int v4, v4

    .line 139
    iget v5, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I

    .line 140
    .line 141
    invoke-virtual {p1, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    float-to-int p1, p1

    .line 146
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    .line 147
    .line 148
    .line 149
    move-result v5

    .line 150
    iget v6, p0, Lcom/intsig/office/system/beans/AEventManage;->mMinimumVelocity:I

    .line 151
    .line 152
    if-gt v5, v6, :cond_8

    .line 153
    .line 154
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    .line 155
    .line 156
    .line 157
    move-result v5

    .line 158
    iget v6, p0, Lcom/intsig/office/system/beans/AEventManage;->mMinimumVelocity:I

    .line 159
    .line 160
    if-le v5, v6, :cond_7

    .line 161
    .line 162
    goto :goto_0

    .line 163
    :cond_7
    const/4 v2, 0x0

    .line 164
    goto :goto_2

    .line 165
    :cond_8
    :goto_0
    iget-boolean v5, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 166
    .line 167
    if-nez v5, :cond_a

    .line 168
    .line 169
    iget-object v5, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 170
    .line 171
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getApplicationType()B

    .line 172
    .line 173
    .line 174
    move-result v5

    .line 175
    if-ne v5, v1, :cond_9

    .line 176
    .line 177
    const/4 v5, 0x1

    .line 178
    goto :goto_1

    .line 179
    :cond_9
    const/4 v5, 0x0

    .line 180
    :goto_1
    iput-boolean v5, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 181
    .line 182
    :cond_a
    iget-boolean v5, p0, Lcom/intsig/office/system/beans/AEventManage;->zoomChange:Z

    .line 183
    .line 184
    if-nez v5, :cond_b

    .line 185
    .line 186
    neg-int p1, p1

    .line 187
    neg-int v4, v4

    .line 188
    invoke-virtual {p0, p1, v4}, Lcom/intsig/office/system/beans/AEventManage;->fling(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 189
    .line 190
    .line 191
    :cond_b
    :goto_2
    :try_start_1
    iput p2, p0, Lcom/intsig/office/system/beans/AEventManage;->midXDoublePoint:I

    .line 192
    .line 193
    iput p2, p0, Lcom/intsig/office/system/beans/AEventManage;->midYDoublePoint:I

    .line 194
    .line 195
    iput p2, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I

    .line 196
    .line 197
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 198
    .line 199
    if-eqz p1, :cond_c

    .line 200
    .line 201
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    .line 202
    .line 203
    .line 204
    iput-object v3, p0, Lcom/intsig/office/system/beans/AEventManage;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 205
    .line 206
    :cond_c
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 207
    .line 208
    if-eqz p1, :cond_10

    .line 209
    .line 210
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 211
    .line 212
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 213
    .line 214
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getApplicationType()B

    .line 215
    .line 216
    .line 217
    move-result p1

    .line 218
    const p2, 0x2000000a

    .line 219
    .line 220
    .line 221
    if-nez p1, :cond_d

    .line 222
    .line 223
    iget-boolean p1, p0, Lcom/intsig/office/system/beans/AEventManage;->zoomChange:Z

    .line 224
    .line 225
    if-eqz p1, :cond_d

    .line 226
    .line 227
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 228
    .line 229
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 230
    .line 231
    .line 232
    move-result-object p1

    .line 233
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->isZoomAfterLayoutForWord()Z

    .line 234
    .line 235
    .line 236
    move-result p1

    .line 237
    if-nez p1, :cond_d

    .line 238
    .line 239
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 240
    .line 241
    invoke-interface {p1, p2, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 242
    .line 243
    .line 244
    :cond_d
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 245
    .line 246
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getApplicationType()B

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    if-ne p1, v1, :cond_e

    .line 251
    .line 252
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 253
    .line 254
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->isSlideShow()Z

    .line 255
    .line 256
    .line 257
    move-result p1

    .line 258
    if-nez p1, :cond_f

    .line 259
    .line 260
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 261
    .line 262
    invoke-interface {p1, p2, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 263
    .line 264
    .line 265
    goto :goto_3

    .line 266
    :cond_e
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 267
    .line 268
    invoke-interface {p1, p2, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 269
    .line 270
    .line 271
    :cond_f
    :goto_3
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 272
    .line 273
    const/16 p2, 0x14

    .line 274
    .line 275
    invoke-interface {p1, p2, v3}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 276
    .line 277
    .line 278
    :cond_10
    iget-object p1, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 279
    .line 280
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getApplicationType()B

    .line 281
    .line 282
    .line 283
    move-result p1

    .line 284
    if-eqz p1, :cond_12

    .line 285
    .line 286
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->zoomChange:Z

    .line 287
    .line 288
    goto :goto_4

    .line 289
    :cond_11
    const/4 v2, 0x0

    .line 290
    :cond_12
    :goto_4
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->singleTabup:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 291
    .line 292
    move v0, v2

    .line 293
    goto :goto_6

    .line 294
    :catch_0
    move-exception p1

    .line 295
    move v0, v2

    .line 296
    goto :goto_5

    .line 297
    :cond_13
    :try_start_2
    invoke-virtual {p0}, Lcom/intsig/office/system/beans/AEventManage;->stopFling()V

    .line 298
    .line 299
    .line 300
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    .line 301
    .line 302
    .line 303
    move-result p1

    .line 304
    iput p1, p0, Lcom/intsig/office/system/beans/AEventManage;->mActivePointerId:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 305
    .line 306
    goto :goto_6

    .line 307
    :catch_1
    move-exception p1

    .line 308
    :goto_5
    iget-object p2, p0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 309
    .line 310
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 311
    .line 312
    .line 313
    move-result-object p2

    .line 314
    invoke-virtual {p2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 315
    .line 316
    .line 317
    move-result-object p2

    .line 318
    invoke-virtual {p2, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 319
    .line 320
    .line 321
    :cond_14
    :goto_6
    return v0
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public stopFling()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/office/system/beans/AEventManage;->isFling:Z

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/system/beans/AEventManage;->mScroller:Landroid/widget/Scroller;

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected zoom(Landroid/view/MotionEvent;)Z
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    iget-object v2, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 6
    .line 7
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-interface {v2}, Lcom/intsig/office/system/IMainFrame;->isTouchZoom()Z

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/4 v3, 0x1

    .line 16
    if-nez v2, :cond_0

    .line 17
    .line 18
    return v3

    .line 19
    :cond_0
    iget-object v2, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 20
    .line 21
    const v4, 0x20000005

    .line 22
    .line 23
    .line 24
    const/4 v5, 0x0

    .line 25
    invoke-interface {v2, v4, v5}, Lcom/intsig/office/system/IControl;->getActionValue(ILjava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Ljava/lang/Float;

    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    iget-object v6, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 36
    .line 37
    const v7, 0x20000006

    .line 38
    .line 39
    .line 40
    invoke-interface {v6, v7, v5}, Lcom/intsig/office/system/IControl;->getActionValue(ILjava/lang/Object;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    check-cast v5, Ljava/lang/Float;

    .line 45
    .line 46
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    const v6, 0x461c4000    # 10000.0f

    .line 51
    .line 52
    .line 53
    mul-float v7, v2, v6

    .line 54
    .line 55
    float-to-int v7, v7

    .line 56
    mul-float v8, v5, v6

    .line 57
    .line 58
    float-to-int v8, v8

    .line 59
    const/4 v9, 0x0

    .line 60
    if-ne v7, v8, :cond_1

    .line 61
    .line 62
    const/4 v7, 0x1

    .line 63
    goto :goto_0

    .line 64
    :cond_1
    const/4 v7, 0x0

    .line 65
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 66
    .line 67
    .line 68
    move-result v8

    .line 69
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 70
    .line 71
    const/4 v12, 0x2

    .line 72
    if-eq v8, v12, :cond_3

    .line 73
    .line 74
    const/4 v5, 0x5

    .line 75
    if-eq v8, v5, :cond_2

    .line 76
    .line 77
    goto/16 :goto_7

    .line 78
    .line 79
    :cond_2
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->getX(I)F

    .line 80
    .line 81
    .line 82
    move-result v5

    .line 83
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->getY(I)F

    .line 84
    .line 85
    .line 86
    move-result v7

    .line 87
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getX(I)F

    .line 88
    .line 89
    .line 90
    move-result v8

    .line 91
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getY(I)F

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    invoke-static {v5, v8}, Ljava/lang/Math;->min(FF)F

    .line 96
    .line 97
    .line 98
    move-result v13

    .line 99
    sub-float/2addr v5, v8

    .line 100
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 101
    .line 102
    .line 103
    move-result v8

    .line 104
    const/high16 v14, 0x40000000    # 2.0f

    .line 105
    .line 106
    div-float/2addr v8, v14

    .line 107
    add-float/2addr v13, v8

    .line 108
    float-to-int v8, v13

    .line 109
    iput v8, v0, Lcom/intsig/office/system/beans/AEventManage;->midXDoublePoint:I

    .line 110
    .line 111
    invoke-static {v7, v1}, Ljava/lang/Math;->min(FF)F

    .line 112
    .line 113
    .line 114
    move-result v8

    .line 115
    sub-float/2addr v7, v1

    .line 116
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    div-float/2addr v1, v14

    .line 121
    add-float/2addr v8, v1

    .line 122
    float-to-int v1, v8

    .line 123
    iput v1, v0, Lcom/intsig/office/system/beans/AEventManage;->midYDoublePoint:I

    .line 124
    .line 125
    mul-float v5, v5, v5

    .line 126
    .line 127
    mul-float v7, v7, v7

    .line 128
    .line 129
    add-float/2addr v5, v7

    .line 130
    float-to-double v7, v5

    .line 131
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 132
    .line 133
    .line 134
    move-result-wide v7

    .line 135
    div-double/2addr v7, v10

    .line 136
    double-to-float v1, v7

    .line 137
    iput v1, v0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 138
    .line 139
    goto/16 :goto_7

    .line 140
    .line 141
    :cond_3
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->getX(I)F

    .line 142
    .line 143
    .line 144
    move-result v8

    .line 145
    invoke-virtual {v1, v9}, Landroid/view/MotionEvent;->getY(I)F

    .line 146
    .line 147
    .line 148
    move-result v13

    .line 149
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getX(I)F

    .line 150
    .line 151
    .line 152
    move-result v14

    .line 153
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getY(I)F

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    sub-float/2addr v8, v14

    .line 158
    mul-float v8, v8, v8

    .line 159
    .line 160
    sub-float/2addr v13, v1

    .line 161
    mul-float v13, v13, v13

    .line 162
    .line 163
    add-float/2addr v8, v13

    .line 164
    float-to-double v13, v8

    .line 165
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    .line 166
    .line 167
    .line 168
    move-result-wide v13

    .line 169
    div-double/2addr v13, v10

    .line 170
    double-to-float v1, v13

    .line 171
    iget v8, v0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 172
    .line 173
    sub-float/2addr v8, v1

    .line 174
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    .line 175
    .line 176
    .line 177
    move-result v8

    .line 178
    const/high16 v10, 0x41000000    # 8.0f

    .line 179
    .line 180
    cmpl-float v8, v8, v10

    .line 181
    .line 182
    if-lez v8, :cond_c

    .line 183
    .line 184
    iget v8, v0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 185
    .line 186
    cmpl-float v8, v1, v8

    .line 187
    .line 188
    if-lez v8, :cond_4

    .line 189
    .line 190
    const/4 v8, 0x1

    .line 191
    goto :goto_1

    .line 192
    :cond_4
    const/4 v8, 0x0

    .line 193
    :goto_1
    sub-float v10, v2, v5

    .line 194
    .line 195
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    .line 196
    .line 197
    .line 198
    move-result v10

    .line 199
    float-to-double v10, v10

    .line 200
    const-wide v13, 0x3f847ae147ae147bL    # 0.01

    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    cmpg-double v15, v10, v13

    .line 206
    .line 207
    if-gez v15, :cond_5

    .line 208
    .line 209
    if-nez v8, :cond_5

    .line 210
    .line 211
    if-eqz v7, :cond_5

    .line 212
    .line 213
    goto :goto_2

    .line 214
    :cond_5
    const/high16 v10, 0x40400000    # 3.0f

    .line 215
    .line 216
    sub-float v11, v2, v10

    .line 217
    .line 218
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    .line 219
    .line 220
    .line 221
    move-result v11

    .line 222
    float-to-double v13, v11

    .line 223
    const-wide v15, 0x3f50624dd2f1a9fcL    # 0.001

    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    cmpg-double v11, v13, v15

    .line 229
    .line 230
    if-gez v11, :cond_6

    .line 231
    .line 232
    if-eqz v8, :cond_6

    .line 233
    .line 234
    :goto_2
    const/4 v5, 0x0

    .line 235
    goto :goto_5

    .line 236
    :cond_6
    const v11, 0x3dcccccd    # 0.1f

    .line 237
    .line 238
    .line 239
    if-eqz v8, :cond_7

    .line 240
    .line 241
    add-float/2addr v2, v11

    .line 242
    goto :goto_3

    .line 243
    :cond_7
    sub-float/2addr v2, v11

    .line 244
    :goto_3
    cmpl-float v11, v2, v10

    .line 245
    .line 246
    if-lez v11, :cond_8

    .line 247
    .line 248
    const/high16 v5, 0x40400000    # 3.0f

    .line 249
    .line 250
    goto :goto_4

    .line 251
    :cond_8
    cmpg-float v10, v2, v5

    .line 252
    .line 253
    if-gez v10, :cond_9

    .line 254
    .line 255
    goto :goto_4

    .line 256
    :cond_9
    move v5, v2

    .line 257
    :goto_4
    if-eqz v8, :cond_a

    .line 258
    .line 259
    if-eqz v7, :cond_a

    .line 260
    .line 261
    const/high16 v2, 0x41200000    # 10.0f

    .line 262
    .line 263
    mul-float v5, v5, v2

    .line 264
    .line 265
    float-to-int v5, v5

    .line 266
    int-to-float v5, v5

    .line 267
    div-float/2addr v5, v2

    .line 268
    :cond_a
    move v2, v5

    .line 269
    const/4 v5, 0x1

    .line 270
    :goto_5
    if-eqz v5, :cond_b

    .line 271
    .line 272
    goto :goto_6

    .line 273
    :cond_b
    iget v1, v0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 274
    .line 275
    :goto_6
    iput v1, v0, Lcom/intsig/office/system/beans/AEventManage;->distance:F

    .line 276
    .line 277
    goto :goto_8

    .line 278
    :cond_c
    :goto_7
    const/4 v5, 0x0

    .line 279
    :goto_8
    if-eqz v5, :cond_f

    .line 280
    .line 281
    iput-boolean v3, v0, Lcom/intsig/office/system/beans/AEventManage;->isScroll:Z

    .line 282
    .line 283
    iput-boolean v3, v0, Lcom/intsig/office/system/beans/AEventManage;->zoomChange:Z

    .line 284
    .line 285
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 286
    .line 287
    const/4 v5, 0x3

    .line 288
    new-array v5, v5, [I

    .line 289
    .line 290
    mul-float v6, v6, v2

    .line 291
    .line 292
    float-to-int v6, v6

    .line 293
    aput v6, v5, v9

    .line 294
    .line 295
    iget v6, v0, Lcom/intsig/office/system/beans/AEventManage;->midXDoublePoint:I

    .line 296
    .line 297
    aput v6, v5, v3

    .line 298
    .line 299
    iget v6, v0, Lcom/intsig/office/system/beans/AEventManage;->midYDoublePoint:I

    .line 300
    .line 301
    aput v6, v5, v12

    .line 302
    .line 303
    invoke-interface {v1, v4, v5}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 304
    .line 305
    .line 306
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 307
    .line 308
    instance-of v4, v1, Lcom/intsig/office/ss/control/SSControl;

    .line 309
    .line 310
    if-eqz v4, :cond_d

    .line 311
    .line 312
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getFind()Lcom/intsig/office/system/IFind;

    .line 313
    .line 314
    .line 315
    move-result-object v1

    .line 316
    if-eqz v1, :cond_d

    .line 317
    .line 318
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 319
    .line 320
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getFind()Lcom/intsig/office/system/IFind;

    .line 321
    .line 322
    .line 323
    move-result-object v1

    .line 324
    check-cast v1, Lcom/intsig/office/ss/control/Spreadsheet;

    .line 325
    .line 326
    invoke-virtual {v1}, Landroid/view/View;->postInvalidate()V

    .line 327
    .line 328
    .line 329
    goto :goto_9

    .line 330
    :cond_d
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 331
    .line 332
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getView()Landroid/view/View;

    .line 333
    .line 334
    .line 335
    move-result-object v1

    .line 336
    invoke-virtual {v1}, Landroid/view/View;->postInvalidate()V

    .line 337
    .line 338
    .line 339
    :goto_9
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 340
    .line 341
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getApplicationType()B

    .line 342
    .line 343
    .line 344
    move-result v1

    .line 345
    if-ne v1, v12, :cond_e

    .line 346
    .line 347
    iget-object v1, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 348
    .line 349
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->isSlideShow()Z

    .line 350
    .line 351
    .line 352
    move-result v1

    .line 353
    if-eqz v1, :cond_e

    .line 354
    .line 355
    return v3

    .line 356
    :cond_e
    const/high16 v1, 0x42c80000    # 100.0f

    .line 357
    .line 358
    mul-float v2, v2, v1

    .line 359
    .line 360
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 361
    .line 362
    .line 363
    move-result v1

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    .line 365
    .line 366
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    .line 368
    .line 369
    const-string v4, "zoom: "

    .line 370
    .line 371
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    .line 373
    .line 374
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 375
    .line 376
    .line 377
    iget-object v2, v0, Lcom/intsig/office/system/beans/AEventManage;->control:Lcom/intsig/office/system/IControl;

    .line 378
    .line 379
    invoke-interface {v2}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 380
    .line 381
    .line 382
    move-result-object v2

    .line 383
    invoke-interface {v2, v1}, Lcom/intsig/office/system/IMainFrame;->changeZoom(I)V

    .line 384
    .line 385
    .line 386
    :cond_f
    return v3
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

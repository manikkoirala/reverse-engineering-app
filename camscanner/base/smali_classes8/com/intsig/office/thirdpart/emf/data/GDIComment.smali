.class public Lcom/intsig/office/thirdpart/emf/data/GDIComment;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "GDIComment.java"


# instance fields
.field private comment:Ljava/lang/String;

.field private image:Landroid/graphics/Bitmap;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x46

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    const-string v0, ""

    .line 2
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/GDIComment;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/GDIComment;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 7
    .line 8
    .line 9
    move-result p3

    .line 10
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->type:I

    .line 15
    .line 16
    const v1, 0x2b464d45

    .line 17
    .line 18
    .line 19
    if-ne v0, v1, :cond_0

    .line 20
    .line 21
    add-int/lit8 v0, p3, -0x4

    .line 22
    .line 23
    invoke-virtual {p2, v0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 24
    .line 25
    .line 26
    rem-int/lit8 p3, p3, 0x4

    .line 27
    .line 28
    if-eqz p3, :cond_6

    .line 29
    .line 30
    rsub-int/lit8 p3, p3, 0x4

    .line 31
    .line 32
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    .line 33
    .line 34
    .line 35
    goto/16 :goto_0

    .line 36
    .line 37
    :cond_0
    const/4 v1, 0x2

    .line 38
    if-ne v0, v1, :cond_1

    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 44
    .line 45
    .line 46
    move-result p3

    .line 47
    if-lez p3, :cond_6

    .line 48
    .line 49
    new-instance v0, Ljava/lang/String;

    .line 50
    .line 51
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    invoke-direct {v0, p2}, Ljava/lang/String;-><init>([B)V

    .line 56
    .line 57
    .line 58
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    const/4 v1, 0x3

    .line 62
    if-ne v0, v1, :cond_2

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    const v1, 0x40000004    # 2.000001f

    .line 66
    .line 67
    .line 68
    if-ne v0, v1, :cond_3

    .line 69
    .line 70
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 71
    .line 72
    .line 73
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 74
    .line 75
    .line 76
    add-int/lit8 p3, p3, -0x4

    .line 77
    .line 78
    add-int/lit8 p3, p3, -0x8

    .line 79
    .line 80
    new-instance v0, Ljava/lang/String;

    .line 81
    .line 82
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    .line 90
    .line 91
    rem-int/lit8 p3, p3, 0x4

    .line 92
    .line 93
    if-eqz p3, :cond_6

    .line 94
    .line 95
    rsub-int/lit8 p3, p3, 0x4

    .line 96
    .line 97
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    const v1, -0x7fffffff

    .line 102
    .line 103
    .line 104
    if-ne v0, v1, :cond_4

    .line 105
    .line 106
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 107
    .line 108
    .line 109
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 110
    .line 111
    .line 112
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 113
    .line 114
    .line 115
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 116
    .line 117
    .line 118
    move-result p3

    .line 119
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 120
    .line 121
    .line 122
    move-result-object p2

    .line 123
    new-instance p3, Ljava/io/ByteArrayInputStream;

    .line 124
    .line 125
    invoke-direct {p3, p2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 126
    .line 127
    .line 128
    invoke-static {p3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    .line 129
    .line 130
    .line 131
    move-result-object p2

    .line 132
    iput-object p2, p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->image:Landroid/graphics/Bitmap;

    .line 133
    .line 134
    return-object p0

    .line 135
    :cond_4
    add-int/lit8 p3, p3, -0x4

    .line 136
    .line 137
    if-lez p3, :cond_5

    .line 138
    .line 139
    new-instance v0, Ljava/lang/String;

    .line 140
    .line 141
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 146
    .line 147
    .line 148
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    .line 149
    .line 150
    rem-int/lit8 p3, p3, 0x4

    .line 151
    .line 152
    if-eqz p3, :cond_6

    .line 153
    .line 154
    rsub-int/lit8 p3, p3, 0x4

    .line 155
    .line 156
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    .line 157
    .line 158
    .line 159
    goto :goto_0

    .line 160
    :cond_5
    const-string p2, ""

    .line 161
    .line 162
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    .line 163
    .line 164
    :cond_6
    :goto_0
    return-object p1
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  length: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/GDIComment;->comment:Ljava/lang/String;

    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

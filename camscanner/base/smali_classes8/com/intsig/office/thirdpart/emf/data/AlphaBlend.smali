.class public Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "AlphaBlend.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field private static final size:I = 0x6c


# instance fields
.field private bkg:Lcom/intsig/office/java/awt/Color;

.field private bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private dwROP:Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

.field private height:I

.field private image:Landroid/graphics/Bitmap;

.field private transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field private usage:I

.field private width:I

.field private x:I

.field private xSrc:I

.field private y:I

.field private ySrc:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x72

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;IIIILcom/intsig/office/java/awt/geom/AffineTransform;Landroid/graphics/Bitmap;Lcom/intsig/office/java/awt/Color;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->x:I

    .line 5
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->y:I

    .line 6
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->width:I

    .line 7
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->height:I

    .line 8
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    const/16 p2, 0xff

    const/4 p3, 0x1

    const/4 p4, 0x0

    invoke-direct {p1, p4, p4, p2, p3}, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;-><init>(IIII)V

    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->dwROP:Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    .line 9
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->xSrc:I

    .line 10
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->ySrc:I

    .line 11
    iput-object p6, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-nez p8, :cond_0

    .line 12
    new-instance p8, Lcom/intsig/office/java/awt/Color;

    invoke-direct {p8, p4, p4, p4, p4}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    :cond_0
    iput-object p8, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 13
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->usage:I

    .line 14
    iput-object p7, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->image:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    .line 15
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->x:I

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->y:I

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->width:I

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->height:I

    .line 35
    .line 36
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    .line 37
    .line 38
    invoke-direct {v0, p2}, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 39
    .line 40
    .line 41
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->dwROP:Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    .line 42
    .line 43
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->xSrc:I

    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->ySrc:I

    .line 54
    .line 55
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readXFORM()Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 60
    .line 61
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readCOLORREF()Lcom/intsig/office/java/awt/Color;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 66
    .line 67
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->usage:I

    .line 72
    .line 73
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 74
    .line 75
    .line 76
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 81
    .line 82
    .line 83
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 84
    .line 85
    .line 86
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 87
    .line 88
    .line 89
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 90
    .line 91
    .line 92
    if-lez v0, :cond_0

    .line 93
    .line 94
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 95
    .line 96
    invoke-direct {v0, p2}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 97
    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_0
    const/4 v0, 0x0

    .line 101
    :goto_0
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 102
    .line 103
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    iget v2, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->width:I

    .line 108
    .line 109
    iget v3, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->height:I

    .line 110
    .line 111
    add-int/lit8 p3, p3, -0x64

    .line 112
    .line 113
    add-int/lit8 v5, p3, -0x28

    .line 114
    .line 115
    iget-object v6, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->dwROP:Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    .line 116
    .line 117
    move-object v4, p2

    .line 118
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/thirdpart/emf/EMFImageLoader;->readImage(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;IILcom/intsig/office/thirdpart/emf/EMFInputStream;ILcom/intsig/office/thirdpart/emf/data/BlendFunction;)Landroid/graphics/Bitmap;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    iput-object p2, p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->image:Landroid/graphics/Bitmap;

    .line 123
    .line 124
    return-object p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 6

    .line 1
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->image:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v1, :cond_0

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->x:I

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->y:I

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->width:I

    .line 10
    .line 11
    iget v5, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->height:I

    .line 12
    .line 13
    move-object v0, p1

    .line 14
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawImage(Landroid/graphics/Bitmap;IIII)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  bounds: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n  x, y, w, h: "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->x:I

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, " "

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->y:I

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->width:I

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->height:I

    .line 55
    .line 56
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v2, "\n  dwROP: "

    .line 60
    .line 61
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->dwROP:Lcom/intsig/office/thirdpart/emf/data/BlendFunction;

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v2, "\n  xSrc, ySrc: "

    .line 70
    .line 71
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->xSrc:I

    .line 75
    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->ySrc:I

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    const-string v1, "\n  transform: "

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    const-string v1, "\n  bkg: "

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 103
    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v1, "\n  usage: "

    .line 108
    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->usage:I

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v1, "\n"

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 123
    .line 124
    if-eqz v1, :cond_0

    .line 125
    .line 126
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->toString()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    goto :goto_0

    .line 131
    :cond_0
    const-string v1, "  bitmap: null"

    .line 132
    .line 133
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    return-object v0
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

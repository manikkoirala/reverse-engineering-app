.class public Lcom/intsig/office/thirdpart/emf/EMFImageLoader;
.super Ljava/lang/Object;
.source "EMFImageLoader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static readImage(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;IILcom/intsig/office/thirdpart/emf/EMFInputStream;ILcom/intsig/office/thirdpart/emf/data/BlendFunction;)Landroid/graphics/Bitmap;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    move/from16 v0, p1

    .line 2
    .line 3
    move/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    const/16 v4, 0x8

    .line 12
    .line 13
    const/4 v5, -0x1

    .line 14
    const/4 v6, 0x0

    .line 15
    const/4 v7, 0x1

    .line 16
    if-ne v3, v7, :cond_4

    .line 17
    .line 18
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 23
    .line 24
    .line 25
    move-result v8

    .line 26
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 27
    .line 28
    .line 29
    move-result v9

    .line 30
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 31
    .line 32
    .line 33
    new-instance v10, Lcom/intsig/office/java/awt/Color;

    .line 34
    .line 35
    invoke-direct {v10, v9, v8, v3}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 43
    .line 44
    .line 45
    move-result v8

    .line 46
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 47
    .line 48
    .line 49
    move-result v9

    .line 50
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 51
    .line 52
    .line 53
    move-result v10

    .line 54
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 55
    .line 56
    .line 57
    new-instance v11, Lcom/intsig/office/java/awt/Color;

    .line 58
    .line 59
    invoke-direct {v11, v10, v9, v8}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v11}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 63
    .line 64
    .line 65
    move-result v8

    .line 66
    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 67
    .line 68
    invoke-static {v0, v1, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 69
    .line 70
    .line 71
    move-result-object v9

    .line 72
    add-int/lit8 v10, p4, -0x8

    .line 73
    .line 74
    invoke-virtual {v2, v10}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    rem-int/lit8 v10, v0, 0x8

    .line 79
    .line 80
    if-eqz v10, :cond_0

    .line 81
    .line 82
    rsub-int/lit8 v10, v10, 0x8

    .line 83
    .line 84
    :cond_0
    new-array v4, v4, [I

    .line 85
    .line 86
    fill-array-data v4, :array_0

    .line 87
    .line 88
    .line 89
    sub-int/2addr v1, v7

    .line 90
    const/4 v7, 0x0

    .line 91
    :goto_0
    if-le v1, v5, :cond_3

    .line 92
    .line 93
    const/4 v11, 0x0

    .line 94
    :goto_1
    if-ge v11, v0, :cond_2

    .line 95
    .line 96
    div-int/lit8 v12, v7, 0x8

    .line 97
    .line 98
    aget v12, v2, v12

    .line 99
    .line 100
    rem-int/lit8 v13, v7, 0x8

    .line 101
    .line 102
    aget v13, v4, v13

    .line 103
    .line 104
    and-int/2addr v12, v13

    .line 105
    add-int/lit8 v7, v7, 0x1

    .line 106
    .line 107
    if-lez v12, :cond_1

    .line 108
    .line 109
    invoke-virtual {v9, v11, v1, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 110
    .line 111
    .line 112
    goto :goto_2

    .line 113
    :cond_1
    invoke-virtual {v9, v11, v1, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 114
    .line 115
    .line 116
    :goto_2
    add-int/lit8 v11, v11, 0x1

    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_2
    add-int/2addr v7, v10

    .line 120
    add-int/lit8 v1, v1, -0x1

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_3
    return-object v9

    .line 124
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 125
    .line 126
    .line 127
    move-result v3

    .line 128
    const/16 v8, 0x100

    .line 129
    .line 130
    const/4 v9, 0x2

    .line 131
    const/4 v10, 0x4

    .line 132
    if-ne v3, v10, :cond_a

    .line 133
    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getCompression()I

    .line 135
    .line 136
    .line 137
    move-result v3

    .line 138
    if-nez v3, :cond_a

    .line 139
    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getClrUsed()I

    .line 141
    .line 142
    .line 143
    move-result v3

    .line 144
    mul-int/lit8 v10, v3, 0x4

    .line 145
    .line 146
    invoke-virtual {v2, v10}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 147
    .line 148
    .line 149
    move-result-object v11

    .line 150
    sub-int v10, p4, v10

    .line 151
    .line 152
    new-array v12, v10, [I

    .line 153
    .line 154
    const/4 v13, 0x0

    .line 155
    :goto_3
    div-int/lit8 v14, v10, 0xc

    .line 156
    .line 157
    if-ge v13, v14, :cond_5

    .line 158
    .line 159
    const/16 v14, 0xa

    .line 160
    .line 161
    invoke-virtual {v2, v14}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 162
    .line 163
    .line 164
    move-result-object v15

    .line 165
    invoke-virtual {v2, v9}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 166
    .line 167
    .line 168
    mul-int/lit8 v9, v13, 0xa

    .line 169
    .line 170
    invoke-static {v15, v6, v12, v9, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 171
    .line 172
    .line 173
    add-int/lit8 v13, v13, 0x1

    .line 174
    .line 175
    const/4 v9, 0x2

    .line 176
    goto :goto_3

    .line 177
    :cond_5
    new-array v2, v8, [I

    .line 178
    .line 179
    const/4 v9, 0x0

    .line 180
    const/4 v13, 0x0

    .line 181
    :goto_4
    if-ge v9, v3, :cond_6

    .line 182
    .line 183
    new-instance v14, Lcom/intsig/office/java/awt/Color;

    .line 184
    .line 185
    add-int/lit8 v15, v13, 0x2

    .line 186
    .line 187
    aget v15, v11, v15

    .line 188
    .line 189
    add-int/lit8 v16, v13, 0x1

    .line 190
    .line 191
    aget v4, v11, v16

    .line 192
    .line 193
    aget v13, v11, v13

    .line 194
    .line 195
    invoke-direct {v14, v15, v4, v13}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v14}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 199
    .line 200
    .line 201
    move-result v4

    .line 202
    aput v4, v2, v9

    .line 203
    .line 204
    add-int/lit8 v9, v9, 0x1

    .line 205
    .line 206
    mul-int/lit8 v13, v9, 0x4

    .line 207
    .line 208
    const/16 v4, 0x8

    .line 209
    .line 210
    goto :goto_4

    .line 211
    :cond_6
    if-ge v3, v8, :cond_7

    .line 212
    .line 213
    invoke-static {v2, v3, v8, v6}, Ljava/util/Arrays;->fill([IIII)V

    .line 214
    .line 215
    .line 216
    :cond_7
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 217
    .line 218
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 219
    .line 220
    .line 221
    move-result-object v3

    .line 222
    sub-int/2addr v1, v7

    .line 223
    const/4 v4, 0x0

    .line 224
    :goto_5
    if-le v1, v5, :cond_9

    .line 225
    .line 226
    const/4 v7, 0x0

    .line 227
    :goto_6
    if-ge v7, v0, :cond_8

    .line 228
    .line 229
    if-ge v4, v10, :cond_8

    .line 230
    .line 231
    aget v8, v12, v4

    .line 232
    .line 233
    const/16 v9, 0x8

    .line 234
    .line 235
    rem-int/2addr v8, v9

    .line 236
    aget v8, v2, v8

    .line 237
    .line 238
    invoke-virtual {v3, v7, v1, v8}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 239
    .line 240
    .line 241
    add-int/lit8 v8, v7, 0x1

    .line 242
    .line 243
    aget v11, v12, v4

    .line 244
    .line 245
    rem-int/2addr v11, v9

    .line 246
    aget v9, v2, v11

    .line 247
    .line 248
    invoke-virtual {v3, v8, v1, v9}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 249
    .line 250
    .line 251
    add-int/lit8 v4, v4, 0x1

    .line 252
    .line 253
    add-int/lit8 v7, v7, 0x2

    .line 254
    .line 255
    goto :goto_6

    .line 256
    :cond_8
    add-int/lit8 v1, v1, -0x1

    .line 257
    .line 258
    goto :goto_5

    .line 259
    :cond_9
    return-object v3

    .line 260
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 261
    .line 262
    .line 263
    move-result v3

    .line 264
    const/16 v4, 0x8

    .line 265
    .line 266
    if-ne v3, v4, :cond_10

    .line 267
    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getCompression()I

    .line 269
    .line 270
    .line 271
    move-result v3

    .line 272
    if-nez v3, :cond_10

    .line 273
    .line 274
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getClrUsed()I

    .line 275
    .line 276
    .line 277
    move-result v3

    .line 278
    mul-int/lit8 v4, v3, 0x4

    .line 279
    .line 280
    invoke-virtual {v2, v4}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 281
    .line 282
    .line 283
    move-result-object v9

    .line 284
    sub-int v4, p4, v4

    .line 285
    .line 286
    invoke-virtual {v2, v4}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte(I)[I

    .line 287
    .line 288
    .line 289
    move-result-object v2

    .line 290
    new-array v4, v8, [I

    .line 291
    .line 292
    const/4 v10, 0x0

    .line 293
    const/4 v11, 0x0

    .line 294
    :goto_7
    if-ge v10, v3, :cond_b

    .line 295
    .line 296
    new-instance v12, Lcom/intsig/office/java/awt/Color;

    .line 297
    .line 298
    add-int/lit8 v13, v11, 0x2

    .line 299
    .line 300
    aget v13, v9, v13

    .line 301
    .line 302
    add-int/lit8 v14, v11, 0x1

    .line 303
    .line 304
    aget v14, v9, v14

    .line 305
    .line 306
    aget v11, v9, v11

    .line 307
    .line 308
    invoke-direct {v12, v13, v14, v11}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 309
    .line 310
    .line 311
    invoke-virtual {v12}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 312
    .line 313
    .line 314
    move-result v11

    .line 315
    aput v11, v4, v10

    .line 316
    .line 317
    add-int/lit8 v10, v10, 0x1

    .line 318
    .line 319
    mul-int/lit8 v11, v10, 0x4

    .line 320
    .line 321
    goto :goto_7

    .line 322
    :cond_b
    if-ge v3, v8, :cond_c

    .line 323
    .line 324
    invoke-static {v4, v3, v8, v6}, Ljava/util/Arrays;->fill([IIII)V

    .line 325
    .line 326
    .line 327
    :cond_c
    rem-int/lit8 v3, v0, 0x4

    .line 328
    .line 329
    if-eqz v3, :cond_d

    .line 330
    .line 331
    rsub-int/lit8 v3, v3, 0x4

    .line 332
    .line 333
    :cond_d
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 334
    .line 335
    invoke-static {v0, v1, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 336
    .line 337
    .line 338
    move-result-object v8

    .line 339
    sub-int/2addr v1, v7

    .line 340
    const/4 v7, 0x0

    .line 341
    :goto_8
    if-le v1, v5, :cond_f

    .line 342
    .line 343
    const/4 v9, 0x0

    .line 344
    :goto_9
    if-ge v9, v0, :cond_e

    .line 345
    .line 346
    add-int/lit8 v10, v7, 0x1

    .line 347
    .line 348
    aget v7, v2, v7

    .line 349
    .line 350
    aget v7, v4, v7

    .line 351
    .line 352
    invoke-virtual {v8, v9, v1, v7}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 353
    .line 354
    .line 355
    add-int/lit8 v9, v9, 0x1

    .line 356
    .line 357
    move v7, v10

    .line 358
    goto :goto_9

    .line 359
    :cond_e
    add-int/2addr v7, v3

    .line 360
    add-int/lit8 v1, v1, -0x1

    .line 361
    .line 362
    goto :goto_8

    .line 363
    :cond_f
    return-object v8

    .line 364
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 365
    .line 366
    .line 367
    move-result v3

    .line 368
    const/16 v4, 0x10

    .line 369
    .line 370
    if-ne v3, v4, :cond_13

    .line 371
    .line 372
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getCompression()I

    .line 373
    .line 374
    .line 375
    move-result v3

    .line 376
    if-nez v3, :cond_13

    .line 377
    .line 378
    div-int/lit8 v1, p4, 0x4

    .line 379
    .line 380
    invoke-virtual {v2, v1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD(I)[I

    .line 381
    .line 382
    .line 383
    move-result-object v1

    .line 384
    rem-int/lit8 v2, v0, 0x2

    .line 385
    .line 386
    add-int/2addr v0, v2

    .line 387
    const/4 v2, 0x2

    .line 388
    div-int/2addr v0, v2

    .line 389
    array-length v3, v1

    .line 390
    div-int/2addr v3, v0

    .line 391
    div-int/2addr v3, v2

    .line 392
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 393
    .line 394
    invoke-static {v0, v3, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 395
    .line 396
    .line 397
    move-result-object v2

    .line 398
    sub-int/2addr v3, v7

    .line 399
    const/4 v4, 0x0

    .line 400
    :goto_a
    if-le v3, v5, :cond_12

    .line 401
    .line 402
    const/4 v7, 0x0

    .line 403
    :goto_b
    if-ge v7, v0, :cond_11

    .line 404
    .line 405
    add-int v8, v4, v0

    .line 406
    .line 407
    aget v8, v1, v8

    .line 408
    .line 409
    add-int/lit8 v9, v4, 0x1

    .line 410
    .line 411
    aget v4, v1, v4

    .line 412
    .line 413
    new-instance v10, Lcom/intsig/office/java/awt/Color;

    .line 414
    .line 415
    and-int/lit16 v11, v4, 0x7c00

    .line 416
    .line 417
    and-int/lit16 v12, v8, 0x7c00

    .line 418
    .line 419
    add-int/2addr v11, v12

    .line 420
    int-to-float v11, v11

    .line 421
    const/high16 v12, 0x47780000    # 63488.0f

    .line 422
    .line 423
    div-float/2addr v11, v12

    .line 424
    and-int/lit16 v12, v4, 0x3e0

    .line 425
    .line 426
    and-int/lit16 v13, v8, 0x3e0

    .line 427
    .line 428
    add-int/2addr v12, v13

    .line 429
    int-to-float v12, v12

    .line 430
    const/high16 v13, 0x44f80000    # 1984.0f

    .line 431
    .line 432
    div-float/2addr v12, v13

    .line 433
    and-int/lit8 v4, v4, 0x1f

    .line 434
    .line 435
    and-int/lit8 v8, v8, 0x1f

    .line 436
    .line 437
    add-int/2addr v4, v8

    .line 438
    int-to-float v4, v4

    .line 439
    const/high16 v8, 0x42780000    # 62.0f

    .line 440
    .line 441
    div-float/2addr v4, v8

    .line 442
    invoke-direct {v10, v11, v12, v4}, Lcom/intsig/office/java/awt/Color;-><init>(FFF)V

    .line 443
    .line 444
    .line 445
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 446
    .line 447
    .line 448
    move-result v4

    .line 449
    invoke-virtual {v2, v7, v3, v4}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 450
    .line 451
    .line 452
    add-int/lit8 v7, v7, 0x1

    .line 453
    .line 454
    move v4, v9

    .line 455
    goto :goto_b

    .line 456
    :cond_11
    add-int/lit8 v3, v3, -0x1

    .line 457
    .line 458
    add-int/2addr v4, v0

    .line 459
    goto :goto_a

    .line 460
    :cond_12
    return-object v2

    .line 461
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 462
    .line 463
    .line 464
    move-result v3

    .line 465
    const/16 v8, 0x20

    .line 466
    .line 467
    if-ne v3, v8, :cond_1d

    .line 468
    .line 469
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getCompression()I

    .line 470
    .line 471
    .line 472
    move-result v3

    .line 473
    if-nez v3, :cond_1d

    .line 474
    .line 475
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 476
    .line 477
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 478
    .line 479
    .line 480
    move-result-object v3

    .line 481
    div-int/lit8 v8, p4, 0x4

    .line 482
    .line 483
    const/16 v9, 0xff

    .line 484
    .line 485
    if-eqz p5, :cond_14

    .line 486
    .line 487
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->getSourceConstantAlpha()I

    .line 488
    .line 489
    .line 490
    move-result v10

    .line 491
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->getAlphaFormat()I

    .line 492
    .line 493
    .line 494
    move-result v11

    .line 495
    goto :goto_c

    .line 496
    :cond_14
    const/16 v10, 0xff

    .line 497
    .line 498
    const/4 v11, 0x0

    .line 499
    :goto_c
    const v12, 0xff00

    .line 500
    .line 501
    .line 502
    const/high16 v13, 0xff0000

    .line 503
    .line 504
    if-eq v11, v7, :cond_16

    .line 505
    .line 506
    sub-int/2addr v1, v7

    .line 507
    const/4 v7, 0x0

    .line 508
    :goto_d
    if-le v1, v5, :cond_1c

    .line 509
    .line 510
    if-ge v7, v8, :cond_1c

    .line 511
    .line 512
    const/4 v11, 0x0

    .line 513
    :goto_e
    if-ge v11, v0, :cond_15

    .line 514
    .line 515
    if-ge v7, v8, :cond_15

    .line 516
    .line 517
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 518
    .line 519
    .line 520
    move-result v14

    .line 521
    new-instance v15, Lcom/intsig/office/java/awt/Color;

    .line 522
    .line 523
    and-int v16, v14, v13

    .line 524
    .line 525
    shr-int/lit8 v6, v16, 0x10

    .line 526
    .line 527
    and-int v16, v14, v12

    .line 528
    .line 529
    const/16 v17, 0x8

    .line 530
    .line 531
    shr-int/lit8 v12, v16, 0x8

    .line 532
    .line 533
    and-int/2addr v14, v9

    .line 534
    invoke-direct {v15, v6, v12, v14, v10}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 535
    .line 536
    .line 537
    invoke-virtual {v15}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 538
    .line 539
    .line 540
    move-result v6

    .line 541
    invoke-virtual {v3, v11, v1, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 542
    .line 543
    .line 544
    add-int/lit8 v11, v11, 0x1

    .line 545
    .line 546
    add-int/lit8 v7, v7, 0x1

    .line 547
    .line 548
    const/4 v6, 0x0

    .line 549
    const v12, 0xff00

    .line 550
    .line 551
    .line 552
    goto :goto_e

    .line 553
    :cond_15
    add-int/lit8 v1, v1, -0x1

    .line 554
    .line 555
    const/4 v6, 0x0

    .line 556
    const v12, 0xff00

    .line 557
    .line 558
    .line 559
    goto :goto_d

    .line 560
    :cond_16
    const/high16 v6, -0x1000000

    .line 561
    .line 562
    if-ne v10, v9, :cond_19

    .line 563
    .line 564
    sub-int/2addr v1, v7

    .line 565
    const/4 v7, 0x0

    .line 566
    :goto_f
    if-le v1, v5, :cond_1c

    .line 567
    .line 568
    if-ge v7, v8, :cond_1c

    .line 569
    .line 570
    const/4 v10, 0x0

    .line 571
    :goto_10
    if-ge v10, v0, :cond_18

    .line 572
    .line 573
    if-ge v7, v8, :cond_18

    .line 574
    .line 575
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 576
    .line 577
    .line 578
    move-result v11

    .line 579
    and-int v12, v11, v6

    .line 580
    .line 581
    shr-int/lit8 v12, v12, 0x18

    .line 582
    .line 583
    if-ne v12, v5, :cond_17

    .line 584
    .line 585
    const/16 v12, 0xff

    .line 586
    .line 587
    :cond_17
    new-instance v14, Lcom/intsig/office/java/awt/Color;

    .line 588
    .line 589
    and-int v15, v11, v13

    .line 590
    .line 591
    shr-int/2addr v15, v4

    .line 592
    const v16, 0xff00

    .line 593
    .line 594
    .line 595
    and-int v18, v11, v16

    .line 596
    .line 597
    const/16 v16, 0x8

    .line 598
    .line 599
    shr-int/lit8 v4, v18, 0x8

    .line 600
    .line 601
    and-int/lit16 v11, v11, 0xff

    .line 602
    .line 603
    invoke-direct {v14, v15, v4, v11, v12}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 604
    .line 605
    .line 606
    invoke-virtual {v14}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 607
    .line 608
    .line 609
    move-result v4

    .line 610
    invoke-virtual {v3, v10, v1, v4}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 611
    .line 612
    .line 613
    add-int/lit8 v10, v10, 0x1

    .line 614
    .line 615
    add-int/lit8 v7, v7, 0x1

    .line 616
    .line 617
    const/16 v4, 0x10

    .line 618
    .line 619
    goto :goto_10

    .line 620
    :cond_18
    add-int/lit8 v1, v1, -0x1

    .line 621
    .line 622
    const/16 v4, 0x10

    .line 623
    .line 624
    goto :goto_f

    .line 625
    :cond_19
    sub-int/2addr v1, v7

    .line 626
    const/4 v4, 0x0

    .line 627
    :goto_11
    if-le v1, v5, :cond_1c

    .line 628
    .line 629
    if-ge v4, v8, :cond_1c

    .line 630
    .line 631
    const/4 v7, 0x0

    .line 632
    :goto_12
    if-ge v7, v0, :cond_1b

    .line 633
    .line 634
    if-ge v4, v8, :cond_1b

    .line 635
    .line 636
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 637
    .line 638
    .line 639
    move-result v11

    .line 640
    and-int v12, v11, v6

    .line 641
    .line 642
    shr-int/lit8 v12, v12, 0x18

    .line 643
    .line 644
    if-ne v12, v5, :cond_1a

    .line 645
    .line 646
    const/16 v12, 0xff

    .line 647
    .line 648
    :cond_1a
    mul-int v12, v12, v10

    .line 649
    .line 650
    div-int/2addr v12, v9

    .line 651
    new-instance v14, Lcom/intsig/office/java/awt/Color;

    .line 652
    .line 653
    and-int v15, v11, v13

    .line 654
    .line 655
    const/16 v16, 0x10

    .line 656
    .line 657
    shr-int/lit8 v15, v15, 0x10

    .line 658
    .line 659
    const v18, 0xff00

    .line 660
    .line 661
    .line 662
    and-int v19, v11, v18

    .line 663
    .line 664
    const/16 v17, 0x8

    .line 665
    .line 666
    shr-int/lit8 v5, v19, 0x8

    .line 667
    .line 668
    and-int/lit16 v11, v11, 0xff

    .line 669
    .line 670
    invoke-direct {v14, v15, v5, v11, v12}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 671
    .line 672
    .line 673
    invoke-virtual {v14}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 674
    .line 675
    .line 676
    move-result v5

    .line 677
    invoke-virtual {v3, v7, v1, v5}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 678
    .line 679
    .line 680
    add-int/lit8 v7, v7, 0x1

    .line 681
    .line 682
    add-int/lit8 v4, v4, 0x1

    .line 683
    .line 684
    const/4 v5, -0x1

    .line 685
    goto :goto_12

    .line 686
    :cond_1b
    const/16 v16, 0x10

    .line 687
    .line 688
    const/16 v17, 0x8

    .line 689
    .line 690
    const v18, 0xff00

    .line 691
    .line 692
    .line 693
    add-int/lit8 v1, v1, -0x1

    .line 694
    .line 695
    const/4 v5, -0x1

    .line 696
    goto :goto_11

    .line 697
    :cond_1c
    return-object v3

    .line 698
    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getBitCount()I

    .line 699
    .line 700
    .line 701
    move-result v0

    .line 702
    const/4 v1, 0x0

    .line 703
    if-ne v0, v8, :cond_1e

    .line 704
    .line 705
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getCompression()I

    .line 706
    .line 707
    .line 708
    move-result v0

    .line 709
    const/4 v3, 0x3

    .line 710
    if-ne v0, v3, :cond_1e

    .line 711
    .line 712
    invoke-virtual/range {p3 .. p4}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 713
    .line 714
    .line 715
    return-object v1

    .line 716
    :cond_1e
    invoke-virtual/range {p3 .. p4}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 717
    .line 718
    .line 719
    return-object v1

    .line 720
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x4
        0x8
        0x10
        0x20
        0x40
        0x80
    .end array-data
.end method

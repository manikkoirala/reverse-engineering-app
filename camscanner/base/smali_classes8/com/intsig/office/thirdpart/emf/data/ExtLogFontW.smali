.class public Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;
.super Ljava/lang/Object;
.source "ExtLogFontW.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;
.implements Lcom/intsig/office/thirdpart/emf/data/GDIObject;


# instance fields
.field private culture:I

.field private font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

.field private fullName:Ljava/lang/String;

.field private match:I

.field private panose:Lcom/intsig/office/thirdpart/emf/data/Panose;

.field private style:Ljava/lang/String;

.field private styleSize:I

.field private vendorID:[B

.field private version:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/simpletext/font/Font;)V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    invoke-direct {v0, p1}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;-><init>(Lcom/intsig/office/simpletext/font/Font;)V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    const-string p1, ""

    .line 13
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->fullName:Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->style:Ljava/lang/String;

    const/4 p1, 0x0

    .line 15
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->version:I

    .line 16
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->styleSize:I

    .line 17
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->match:I

    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 18
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->vendorID:[B

    .line 19
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->culture:I

    .line 20
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Panose;

    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Panose;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->panose:Lcom/intsig/office/thirdpart/emf/data/Panose;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    invoke-direct {v0, p1}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    const/16 v0, 0x40

    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWCHAR(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->fullName:Ljava/lang/String;

    const/16 v0, 0x20

    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWCHAR(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->style:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->version:I

    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->styleSize:I

    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->match:I

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    const/4 v0, 0x4

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->vendorID:[B

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->culture:I

    .line 31
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/Panose;

    invoke-direct {v0, p1}, Lcom/intsig/office/thirdpart/emf/data/Panose;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->panose:Lcom/intsig/office/thirdpart/emf/data/Panose;

    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWORD()I

    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->popBuffer()[B

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/data/LogFontW;Ljava/lang/String;Ljava/lang/String;III[BILcom/intsig/office/thirdpart/emf/data/Panose;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 3
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->fullName:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->style:Ljava/lang/String;

    .line 5
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->version:I

    .line 6
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->styleSize:I

    .line 7
    iput p6, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->match:I

    .line 8
    iput-object p7, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->vendorID:[B

    .line 9
    iput p8, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->culture:I

    .line 10
    iput-object p9, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->panose:Lcom/intsig/office/thirdpart/emf/data/Panose;

    return-void
.end method


# virtual methods
.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->getFont()Lcom/intsig/office/simpletext/font/Font;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setFont(Lcom/intsig/office/simpletext/font/Font;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->getEscapement()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setEscapement(I)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  LogFontW\n"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->font:Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v1, "\n    fullname: "

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->fullName:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, "\n    style: "

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->style:Ljava/lang/String;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v1, "\n    version: "

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->version:I

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string v1, "\n    stylesize: "

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->styleSize:I

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v1, "\n    match: "

    .line 68
    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->match:I

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v1, "\n    vendorID: "

    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->vendorID:[B

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    const-string v1, "\n    culture: "

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->culture:I

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    const-string v1, "\n"

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogFontW;->panose:Lcom/intsig/office/thirdpart/emf/data/Panose;

    .line 103
    .line 104
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/Panose;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    return-object v0
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

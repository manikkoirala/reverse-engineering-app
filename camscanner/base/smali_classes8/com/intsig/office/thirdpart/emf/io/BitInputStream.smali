.class public Lcom/intsig/office/thirdpart/emf/io/BitInputStream;
.super Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;
.source "BitInputStream.java"


# static fields
.field protected static final BIT_MASK:[I

.field protected static final FIELD_MASK:[I

.field protected static final MASK_SIZE:I = 0x8

.field protected static final ONES:I = -0x1

.field protected static final ZERO:I


# instance fields
.field private bits:I

.field private validBits:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v1, v0, [I

    .line 4
    .line 5
    sput-object v1, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->BIT_MASK:[I

    .line 6
    .line 7
    new-array v1, v0, [I

    .line 8
    .line 9
    sput-object v1, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->FIELD_MASK:[I

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x1

    .line 14
    const/4 v4, 0x1

    .line 15
    :goto_0
    if-ge v2, v0, :cond_0

    .line 16
    .line 17
    sget-object v5, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->BIT_MASK:[I

    .line 18
    .line 19
    aput v3, v5, v2

    .line 20
    .line 21
    sget-object v5, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->FIELD_MASK:[I

    .line 22
    .line 23
    aput v4, v5, v2

    .line 24
    .line 25
    shl-int/2addr v3, v1

    .line 26
    shl-int/2addr v4, v1

    .line 27
    add-int/2addr v4, v1

    .line 28
    add-int/lit8 v2, v2, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->bits:I

    .line 6
    .line 7
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public byteAlign()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected fetchByte()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->read()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->bits:I

    .line 6
    .line 7
    if-ltz v0, :cond_0

    .line 8
    .line 9
    const/16 v0, 0x8

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance v0, Ljava/io/EOFException;

    .line 15
    .line 16
    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    .line 17
    .line 18
    .line 19
    throw v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readBitFlag()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->fetchByte()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->bits:I

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->BIT_MASK:[I

    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 13
    .line 14
    const/4 v3, 0x1

    .line 15
    sub-int/2addr v2, v3

    .line 16
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 17
    .line 18
    aget v1, v1, v2

    .line 19
    .line 20
    and-int/2addr v0, v1

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v3, 0x0

    .line 25
    :goto_0
    return v3
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readFBits(I)F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return p1

    .line 5
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->readSBits(I)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    long-to-float p1, v0

    .line 10
    const/high16 v0, 0x45800000    # 4096.0f

    .line 11
    .line 12
    div-float/2addr p1, v0

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public readSBits(I)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-wide/16 v0, 0x0

    .line 4
    .line 5
    return-wide v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->readBitFlag()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, -0x1

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    const/4 v0, -0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_1
    const/4 v0, 0x0

    .line 16
    :goto_0
    add-int/2addr p1, v1

    .line 17
    shl-int/2addr v0, p1

    .line 18
    int-to-long v0, v0

    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->readUBits(I)J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    or-long/2addr v0, v2

    .line 24
    return-wide v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public readUBits(I)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    :goto_0
    if-lez p1, :cond_2

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 6
    .line 7
    if-nez v2, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->fetchByte()V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 13
    .line 14
    if-le p1, v2, :cond_1

    .line 15
    .line 16
    move v3, v2

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    move v3, p1

    .line 19
    :goto_1
    iget v4, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->bits:I

    .line 20
    .line 21
    sub-int v5, v2, v3

    .line 22
    .line 23
    shr-int/2addr v4, v5

    .line 24
    sget-object v5, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->FIELD_MASK:[I

    .line 25
    .line 26
    add-int/lit8 v6, v3, -0x1

    .line 27
    .line 28
    aget v5, v5, v6

    .line 29
    .line 30
    and-int/2addr v4, v5

    .line 31
    sub-int/2addr v2, v3

    .line 32
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/io/BitInputStream;->validBits:I

    .line 33
    .line 34
    sub-int/2addr p1, v3

    .line 35
    shl-long/2addr v0, v3

    .line 36
    int-to-long v2, v4

    .line 37
    or-long/2addr v0, v2

    .line 38
    goto :goto_0

    .line 39
    :cond_2
    return-wide v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

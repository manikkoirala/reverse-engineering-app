.class public Lcom/intsig/office/thirdpart/emf/data/GradientFill;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "GradientFill.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# instance fields
.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private gradients:[Lcom/intsig/office/thirdpart/emf/data/Gradient;

.field private mode:I

.field private vertices:[Lcom/intsig/office/thirdpart/emf/data/TriVertex;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x76

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;I[Lcom/intsig/office/thirdpart/emf/data/TriVertex;[Lcom/intsig/office/thirdpart/emf/data/Gradient;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/GradientFill;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->mode:I

    .line 5
    iput-object p3, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->vertices:[Lcom/intsig/office/thirdpart/emf/data/TriVertex;

    .line 6
    iput-object p4, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->gradients:[Lcom/intsig/office/thirdpart/emf/data/Gradient;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 6
    .line 7
    .line 8
    move-result p3

    .line 9
    new-array v0, p3, [Lcom/intsig/office/thirdpart/emf/data/TriVertex;

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    new-array v2, v1, [Lcom/intsig/office/thirdpart/emf/data/Gradient;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readULONG()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    const/4 v4, 0x0

    .line 22
    const/4 v5, 0x0

    .line 23
    :goto_0
    if-ge v5, p3, :cond_0

    .line 24
    .line 25
    new-instance v6, Lcom/intsig/office/thirdpart/emf/data/TriVertex;

    .line 26
    .line 27
    invoke-direct {v6, p2}, Lcom/intsig/office/thirdpart/emf/data/TriVertex;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 28
    .line 29
    .line 30
    aput-object v6, v0, v5

    .line 31
    .line 32
    add-int/lit8 v5, v5, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    :goto_1
    if-ge v4, v1, :cond_2

    .line 36
    .line 37
    const/4 p3, 0x2

    .line 38
    if-ne v3, p3, :cond_1

    .line 39
    .line 40
    new-instance p3, Lcom/intsig/office/thirdpart/emf/data/GradientTriangle;

    .line 41
    .line 42
    invoke-direct {p3, p2}, Lcom/intsig/office/thirdpart/emf/data/GradientTriangle;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 43
    .line 44
    .line 45
    aput-object p3, v2, v4

    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_1
    new-instance p3, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;

    .line 49
    .line 50
    invoke-direct {p3, p2}, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 51
    .line 52
    .line 53
    aput-object p3, v2, v4

    .line 54
    .line 55
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_2
    new-instance p2, Lcom/intsig/office/thirdpart/emf/data/GradientFill;

    .line 59
    .line 60
    invoke-direct {p2, p1, v3, v0, v2}, Lcom/intsig/office/thirdpart/emf/data/GradientFill;-><init>(Lcom/intsig/office/java/awt/Rectangle;I[Lcom/intsig/office/thirdpart/emf/data/TriVertex;[Lcom/intsig/office/thirdpart/emf/data/Gradient;)V

    .line 61
    .line 62
    .line 63
    return-object p2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 16
    .line 17
    .line 18
    const-string v2, "  bounds: "

    .line 19
    .line 20
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 21
    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 24
    .line 25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 29
    .line 30
    .line 31
    const-string v2, "  mode: "

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 34
    .line 35
    .line 36
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->mode:I

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    .line 43
    .line 44
    const/4 v2, 0x0

    .line 45
    const/4 v3, 0x0

    .line 46
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->vertices:[Lcom/intsig/office/thirdpart/emf/data/TriVertex;

    .line 47
    .line 48
    array-length v4, v4

    .line 49
    const-string v5, "]: "

    .line 50
    .line 51
    if-ge v3, v4, :cond_0

    .line 52
    .line 53
    const-string v4, "  vertex["

    .line 54
    .line 55
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 62
    .line 63
    .line 64
    iget-object v4, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->vertices:[Lcom/intsig/office/thirdpart/emf/data/TriVertex;

    .line 65
    .line 66
    aget-object v4, v4, v3

    .line 67
    .line 68
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    .line 73
    .line 74
    add-int/lit8 v3, v3, 0x1

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->gradients:[Lcom/intsig/office/thirdpart/emf/data/Gradient;

    .line 78
    .line 79
    array-length v3, v3

    .line 80
    if-ge v2, v3, :cond_1

    .line 81
    .line 82
    const-string v3, "  gradient["

    .line 83
    .line 84
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/data/GradientFill;->gradients:[Lcom/intsig/office/thirdpart/emf/data/Gradient;

    .line 94
    .line 95
    aget-object v3, v3, v2

    .line 96
    .line 97
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    .line 102
    .line 103
    add-int/lit8 v2, v2, 0x1

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    return-object v0
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

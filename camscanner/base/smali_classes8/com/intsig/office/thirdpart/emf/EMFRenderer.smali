.class public Lcom/intsig/office/thirdpart/emf/EMFRenderer;
.super Ljava/lang/Object;
.source "EMFRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;
    }
.end annotation


# static fields
.field public static TWIP_SCALE:D

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private arcDirection:I

.field private bkMode:I

.field private brushOrigin:Landroid/graphics/Point;

.field private brushPaint:Landroid/graphics/Paint;

.field private dcStack:Ljava/util/Stack;

.field private escapement:I

.field private figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

.field private gdiObjects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

.field private header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

.field private initialClip:Lcom/intsig/office/java/awt/Shape;

.field private initialMatrix:Landroid/graphics/Matrix;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

.field private mapModeIsotropic:Z

.field private mapModeTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field private meterLimit:I

.field private path:Lcom/intsig/office/java/awt/geom/GeneralPath;

.field private pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field private penPaint:Landroid/graphics/Paint;

.field private penStroke:Lcom/intsig/office/java/awt/Stroke;

.field private rop2:I

.field private scaleMode:I

.field private tags:Ljava/util/Vector;

.field private textAlignMode:I

.field private textColor:Lcom/intsig/office/java/awt/Color;

.field private useCreatePen:Z

.field private viewportOrigin:Landroid/graphics/Point;

.field private viewportSize:Lcom/intsig/office/java/awt/Dimension;

.field private windingRule:I

.field private windowOrigin:Landroid/graphics/Point;

.field private windowSize:Lcom/intsig/office/java/awt/Dimension;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, "com.intsig.office.thirdpart.emf"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    .line 8
    .line 9
    const-wide v0, 0x3fc693e93e93e93fL    # 0.1763888888888889

    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    sput-wide v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->TWIP_SCALE:D

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowOrigin:Landroid/graphics/Point;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportOrigin:Landroid/graphics/Point;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeIsotropic:Z

    .line 17
    .line 18
    sget-wide v2, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->TWIP_SCALE:D

    .line 19
    .line 20
    invoke-static {v2, v3, v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 25
    .line 26
    const/16 v2, 0x100

    .line 27
    .line 28
    new-array v2, v2, [Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 29
    .line 30
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->gdiObjects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 31
    .line 32
    new-instance v2, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;

    .line 33
    .line 34
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penStroke:Lcom/intsig/office/java/awt/Stroke;

    .line 38
    .line 39
    new-instance v2, Landroid/graphics/Paint;

    .line 40
    .line 41
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 42
    .line 43
    .line 44
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 45
    .line 46
    new-instance v2, Landroid/graphics/Paint;

    .line 47
    .line 48
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 49
    .line 50
    .line 51
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 52
    .line 53
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textAlignMode:I

    .line 54
    .line 55
    sget-object v2, Lcom/intsig/office/java/awt/Color;->BLACK:Lcom/intsig/office/java/awt/Color;

    .line 56
    .line 57
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textColor:Lcom/intsig/office/java/awt/Color;

    .line 58
    .line 59
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 60
    .line 61
    const/4 v3, 0x2

    .line 62
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    .line 63
    .line 64
    const/4 v3, 0x1

    .line 65
    iput-boolean v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    .line 66
    .line 67
    const/16 v4, 0xa

    .line 68
    .line 69
    iput v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 70
    .line 71
    const/16 v4, 0xd

    .line 72
    .line 73
    iput v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->rop2:I

    .line 74
    .line 75
    const/4 v4, 0x4

    .line 76
    iput v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->scaleMode:I

    .line 77
    .line 78
    new-instance v4, Landroid/graphics/Point;

    .line 79
    .line 80
    invoke-direct {v4, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 81
    .line 82
    .line 83
    iput-object v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushOrigin:Landroid/graphics/Point;

    .line 84
    .line 85
    new-instance v4, Ljava/util/Vector;

    .line 86
    .line 87
    invoke-direct {v4, v1}, Ljava/util/Vector;-><init>(I)V

    .line 88
    .line 89
    .line 90
    iput-object v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->tags:Ljava/util/Vector;

    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 93
    .line 94
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 95
    .line 96
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 97
    .line 98
    .line 99
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 100
    .line 101
    new-instance v0, Ljava/util/Stack;

    .line 102
    .line 103
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 104
    .line 105
    .line 106
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->dcStack:Ljava/util/Stack;

    .line 107
    .line 108
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->arcDirection:I

    .line 109
    .line 110
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->escapement:I

    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 113
    .line 114
    new-instance v3, Lcom/intsig/office/java/awt/Color;

    .line 115
    .line 116
    invoke-direct {v3, v1, v1, v1, v1}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 127
    .line 128
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readHeader()Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 140
    .line 141
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->readTag()Lcom/intsig/office/thirdpart/emf/io/Tag;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    .line 147
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->tags:Ljava/util/Vector;

    .line 148
    .line 149
    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 154
    .line 155
    .line 156
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private appendToPath(Lcom/intsig/office/java/awt/Shape;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 15
    .line 16
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/Shape;Z)V

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x1

    .line 20
    return p1

    .line 21
    :cond_1
    return v1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static createMatrix(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F
    .locals 6

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    new-array v1, v0, [D

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getMatrix([D)V

    .line 6
    .line 7
    .line 8
    new-array p0, v0, [F

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    aget-wide v2, v1, v0

    .line 12
    .line 13
    double-to-float v2, v2

    .line 14
    aput v2, p0, v0

    .line 15
    .line 16
    const/4 v0, 0x2

    .line 17
    aget-wide v2, v1, v0

    .line 18
    .line 19
    double-to-float v2, v2

    .line 20
    const/4 v3, 0x1

    .line 21
    aput v2, p0, v3

    .line 22
    .line 23
    const/4 v2, 0x4

    .line 24
    aget-wide v4, v1, v2

    .line 25
    .line 26
    double-to-float v4, v4

    .line 27
    aput v4, p0, v0

    .line 28
    .line 29
    aget-wide v3, v1, v3

    .line 30
    .line 31
    double-to-float v0, v3

    .line 32
    const/4 v3, 0x3

    .line 33
    aput v0, p0, v3

    .line 34
    .line 35
    aget-wide v3, v1, v3

    .line 36
    .line 37
    double-to-float v0, v3

    .line 38
    aput v0, p0, v2

    .line 39
    .line 40
    const/4 v0, 0x5

    .line 41
    aget-wide v2, v1, v0

    .line 42
    .line 43
    double-to-float v1, v2

    .line 44
    aput v1, p0, v0

    .line 45
    .line 46
    const/4 v0, 0x6

    .line 47
    const/4 v1, 0x0

    .line 48
    aput v1, p0, v0

    .line 49
    .line 50
    const/4 v0, 0x7

    .line 51
    aput v1, p0, v0

    .line 52
    .line 53
    const/16 v0, 0x8

    .line 54
    .line 55
    const/high16 v1, 0x3f800000    # 1.0f

    .line 56
    .line 57
    aput v1, p0, v0

    .line 58
    .line 59
    return-object p0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private createShape([I)Lcom/intsig/office/java/awt/Shape;
    .locals 9

    .line 1
    new-instance v7, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    invoke-direct {v7}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :goto_0
    array-length v1, p1

    .line 8
    if-ge v0, v1, :cond_4

    .line 9
    .line 10
    aget v1, p1, v0

    .line 11
    .line 12
    const/4 v2, -0x5

    .line 13
    if-eq v1, v2, :cond_4

    .line 14
    .line 15
    const/4 v2, -0x4

    .line 16
    if-eq v1, v2, :cond_3

    .line 17
    .line 18
    const/4 v2, -0x3

    .line 19
    if-eq v1, v2, :cond_2

    .line 20
    .line 21
    const/4 v2, -0x2

    .line 22
    if-eq v1, v2, :cond_1

    .line 23
    .line 24
    const/4 v2, -0x1

    .line 25
    if-eq v1, v2, :cond_0

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 29
    .line 30
    aget v1, p1, v0

    .line 31
    .line 32
    int-to-float v1, v1

    .line 33
    add-int/lit8 v0, v0, 0x1

    .line 34
    .line 35
    aget v2, p1, v0

    .line 36
    .line 37
    int-to-float v2, v2

    .line 38
    invoke-virtual {v7, v1, v2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 43
    .line 44
    aget v1, p1, v0

    .line 45
    .line 46
    int-to-float v1, v1

    .line 47
    add-int/lit8 v0, v0, 0x1

    .line 48
    .line 49
    aget v2, p1, v0

    .line 50
    .line 51
    int-to-float v2, v2

    .line 52
    invoke-virtual {v7, v1, v2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 57
    .line 58
    aget v1, p1, v0

    .line 59
    .line 60
    int-to-float v1, v1

    .line 61
    add-int/lit8 v0, v0, 0x1

    .line 62
    .line 63
    aget v2, p1, v0

    .line 64
    .line 65
    int-to-float v2, v2

    .line 66
    add-int/lit8 v0, v0, 0x1

    .line 67
    .line 68
    aget v3, p1, v0

    .line 69
    .line 70
    int-to-float v3, v3

    .line 71
    add-int/lit8 v0, v0, 0x1

    .line 72
    .line 73
    aget v4, p1, v0

    .line 74
    .line 75
    int-to-float v4, v4

    .line 76
    invoke-virtual {v7, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->quadTo(FFFF)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 81
    .line 82
    aget v1, p1, v0

    .line 83
    .line 84
    int-to-float v1, v1

    .line 85
    add-int/lit8 v0, v0, 0x1

    .line 86
    .line 87
    aget v2, p1, v0

    .line 88
    .line 89
    int-to-float v2, v2

    .line 90
    add-int/lit8 v0, v0, 0x1

    .line 91
    .line 92
    aget v3, p1, v0

    .line 93
    .line 94
    int-to-float v3, v3

    .line 95
    add-int/lit8 v0, v0, 0x1

    .line 96
    .line 97
    aget v4, p1, v0

    .line 98
    .line 99
    int-to-float v4, v4

    .line 100
    add-int/lit8 v0, v0, 0x1

    .line 101
    .line 102
    aget v5, p1, v0

    .line 103
    .line 104
    int-to-float v5, v5

    .line 105
    add-int/lit8 v8, v0, 0x1

    .line 106
    .line 107
    aget v0, p1, v8

    .line 108
    .line 109
    int-to-float v6, v0

    .line 110
    move-object v0, v7

    .line 111
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->curveTo(FFFFFF)V

    .line 112
    .line 113
    .line 114
    move v0, v8

    .line 115
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_4
    return-object v7
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private drawOrAppend(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->appendToPath(Lcom/intsig/office/java/awt/Shape;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawShape(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    :cond_0
    return-void
.end method

.method private drawShape(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penStroke:Lcom/intsig/office/java/awt/Stroke;

    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setStroke(Lcom/intsig/office/java/awt/Stroke;)V

    .line 2
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->rop2:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/intsig/office/java/awt/Color;->black:Lcom/intsig/office/java/awt/Color;

    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    :cond_0
    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    :cond_1
    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 6
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    :cond_2
    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 7
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 8
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/intsig/office/java/awt/Color;->white:Lcom/intsig/office/java/awt/Color;

    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 9
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    :cond_4
    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    .line 10
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 11
    :cond_5
    sget-object v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got unsupported ROP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->rop2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 12
    :goto_0
    invoke-direct {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getPath(Lcom/intsig/office/java/awt/Shape;)Landroid/graphics/Path;

    move-result-object p2

    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private fillAndDrawOrAppend(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V
    .locals 2

    .line 1
    invoke-direct {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->appendToPath(Lcom/intsig/office/java/awt/Shape;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    if-eqz v0, :cond_1

    .line 3
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 4
    invoke-virtual {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillShape(Lcom/intsig/office/java/awt/Shape;)V

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillShape(Lcom/intsig/office/java/awt/Shape;)V

    goto :goto_0

    .line 6
    :cond_1
    invoke-virtual {p0, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillShape(Lcom/intsig/office/java/awt/Shape;)V

    .line 7
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawShape(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    :cond_2
    return-void
.end method

.method private getCurrentSegment(Lcom/intsig/office/java/awt/geom/PathIterator;Landroid/graphics/Path;)V
    .locals 13

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [F

    .line 3
    .line 4
    invoke-interface {p1, v0}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([F)I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz p1, :cond_4

    .line 11
    .line 12
    if-eq p1, v2, :cond_3

    .line 13
    .line 14
    const/4 v3, 0x3

    .line 15
    const/4 v4, 0x2

    .line 16
    if-eq p1, v4, :cond_2

    .line 17
    .line 18
    const/4 v5, 0x4

    .line 19
    if-eq p1, v3, :cond_1

    .line 20
    .line 21
    if-eq p1, v5, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    aget v7, v0, v1

    .line 29
    .line 30
    aget v8, v0, v2

    .line 31
    .line 32
    aget v9, v0, v4

    .line 33
    .line 34
    aget v10, v0, v3

    .line 35
    .line 36
    aget v11, v0, v5

    .line 37
    .line 38
    const/4 p1, 0x5

    .line 39
    aget v12, v0, p1

    .line 40
    .line 41
    move-object v6, p2

    .line 42
    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    aget p1, v0, v1

    .line 47
    .line 48
    aget v1, v0, v2

    .line 49
    .line 50
    aget v2, v0, v4

    .line 51
    .line 52
    aget v0, v0, v3

    .line 53
    .line 54
    invoke-virtual {p2, p1, v1, v2, v0}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_3
    aget p1, v0, v1

    .line 59
    .line 60
    aget v0, v0, v2

    .line 61
    .line 62
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    aget p1, v0, v1

    .line 67
    .line 68
    aget v0, v0, v2

    .line 69
    .line 70
    invoke-virtual {p2, p1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getPath(Lcom/intsig/office/java/awt/Shape;)Landroid/graphics/Path;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    const/4 v1, 0x0

    .line 2
    invoke-interface {p1, v1}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    move-result-object p1

    .line 3
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getCurrentSegment(Lcom/intsig/office/java/awt/geom/PathIterator;Landroid/graphics/Path;)V

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private resetMatrix(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->initialMatrix:Landroid/graphics/Matrix;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    .line 10
    .line 11
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 15
    .line 16
    .line 17
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 22
    .line 23
    if-eqz v1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 32
    .line 33
    .line 34
    move-result-wide v2

    .line 35
    div-double/2addr v0, v2

    .line 36
    double-to-float v0, v0

    .line 37
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 40
    .line 41
    .line 42
    move-result-wide v1

    .line 43
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 44
    .line 45
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 46
    .line 47
    .line 48
    move-result-wide v3

    .line 49
    div-double/2addr v1, v3

    .line 50
    double-to-float v1, v1

    .line 51
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 52
    .line 53
    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private setStroke(Lcom/intsig/office/java/awt/Stroke;)V
    .locals 4

    .line 1
    check-cast p1, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 4
    .line 5
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;->getLineWidth()F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;->getEndCap()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v1, 0x2

    .line 24
    const/4 v2, 0x1

    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 28
    .line 29
    sget-object v3, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    .line 30
    .line 31
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    if-ne v0, v2, :cond_1

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 38
    .line 39
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 40
    .line 41
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    if-ne v0, v1, :cond_2

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 48
    .line 49
    sget-object v3, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    .line 50
    .line 51
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 52
    .line 53
    .line 54
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;->getLineJoin()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-nez v0, :cond_3

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 61
    .line 62
    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 65
    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_3
    if-ne v0, v2, :cond_4

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 71
    .line 72
    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_4
    if-ne v0, v1, :cond_5

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 81
    .line 82
    sget-object v1, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 85
    .line 86
    .line 87
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 88
    .line 89
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/data/BasicStroke;->getMiterLimit()F

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public appendFigure()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    :try_start_0
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->appendToPath(Lcom/intsig/office/java/awt/Shape;)Z

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;
    :try_end_0
    .catch Lcom/intsig/office/java/awt/geom/IllegalPathStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :catch_0
    sget-object v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    .line 14
    .line 15
    const-string v1, "no figure to append"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public clip(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getPath(Lcom/intsig/office/java/awt/Shape;)Landroid/graphics/Path;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public closeFigure()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->appendToPath(Lcom/intsig/office/java/awt/Shape;)Z

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;
    :try_end_0
    .catch Lcom/intsig/office/java/awt/geom/IllegalPathStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :catch_0
    sget-object v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    .line 19
    .line 20
    const-string v1, "no figure to close"

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public closePath()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V
    :try_end_0
    .catch Lcom/intsig/office/java/awt/geom/IllegalPathStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    sget-object v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    .line 10
    .line 11
    const-string v1, "no figure to close"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public drawImage(Landroid/graphics/Bitmap;IIII)V
    .locals 1

    .line 4
    new-instance v0, Landroid/graphics/Rect;

    add-int/2addr p4, p2

    add-int/2addr p5, p3

    invoke-direct {v0, p2, p3, p4, p5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5
    iget-object p2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3, v0, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method public drawImage(Landroid/graphics/Bitmap;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 2
    invoke-static {p2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->createMatrix(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->setValues([F)V

    .line 3
    iget-object p2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    return-void
.end method

.method public drawOrAppend(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawOrAppend(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    return-void
.end method

.method public drawOrAppendText(Ljava/lang/String;FF)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textColor:Lcom/intsig/office/java/awt/Color;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 22
    .line 23
    .line 24
    const/16 v1, 0xa8c

    .line 25
    .line 26
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->escapement:I

    .line 27
    .line 28
    if-ne v1, v2, :cond_0

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ge v1, v2, :cond_2

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 38
    .line 39
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    int-to-float v4, v1

    .line 48
    iget-object v5, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 49
    .line 50
    invoke-virtual {v5}, Landroid/graphics/Paint;->getTextSize()F

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    mul-float v4, v4, v5

    .line 55
    .line 56
    add-float/2addr v4, p3

    .line 57
    iget-object v5, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 58
    .line 59
    invoke-virtual {v2, v3, p2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 60
    .line 61
    .line 62
    add-int/lit8 v1, v1, 0x1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_0
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textAlignMode:I

    .line 66
    .line 67
    if-nez v1, :cond_1

    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 70
    .line 71
    invoke-virtual {v1}, Landroid/graphics/Paint;->getTextSize()F

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    const/high16 v2, 0x40400000    # 3.0f

    .line 76
    .line 77
    sub-float/2addr v1, v2

    .line 78
    add-float/2addr p3, v1

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 80
    .line 81
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 82
    .line 83
    invoke-virtual {v1, p1, p2, p3, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 84
    .line 85
    .line 86
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public drawShape(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawShape(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    return-void
.end method

.method public fillAndDrawOrAppend(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillAndDrawOrAppend(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    return-void
.end method

.method public fillAndDrawShape(Lcom/intsig/office/java/awt/Shape;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 8
    .line 9
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 10
    .line 11
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 15
    .line 16
    invoke-direct {p0, v1, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawShape(Landroid/graphics/Canvas;Lcom/intsig/office/java/awt/Shape;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public fillShape(Lcom/intsig/office/java/awt/Shape;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 8
    .line 9
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 10
    .line 11
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 15
    .line 16
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getPath(Lcom/intsig/office/java/awt/Shape;)Landroid/graphics/Path;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 21
    .line 22
    invoke-virtual {v1, p1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public fixViewportSize()V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeIsotropic:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 14
    .line 15
    .line 16
    move-result-wide v1

    .line 17
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 18
    .line 19
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 20
    .line 21
    .line 22
    move-result-wide v3

    .line 23
    iget-object v5, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 24
    .line 25
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 26
    .line 27
    .line 28
    move-result-wide v5

    .line 29
    iget-object v7, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 30
    .line 31
    invoke-virtual {v7}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 32
    .line 33
    .line 34
    move-result-wide v7

    .line 35
    div-double/2addr v5, v7

    .line 36
    mul-double v3, v3, v5

    .line 37
    .line 38
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/Dimension;->setSize(DD)V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getArcDirection()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->arcDirection:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBrushOrigin()Landroid/graphics/Point;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushOrigin:Landroid/graphics/Point;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getClip()Lcom/intsig/office/java/awt/Shape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFigure()Lcom/intsig/office/java/awt/geom/GeneralPath;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGDIObject(I)Lcom/intsig/office/thirdpart/emf/data/GDIObject;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->gdiObjects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getInitialClip()Lcom/intsig/office/java/awt/Shape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->initialClip:Lcom/intsig/office/java/awt/Shape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMapModeTransform()Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMeterLimit()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPath()Lcom/intsig/office/java/awt/geom/GeneralPath;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    return-object v0
.end method

.method public getPathTransform()Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPenStroke()Lcom/intsig/office/java/awt/Stroke;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penStroke:Lcom/intsig/office/java/awt/Stroke;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSize()Lcom/intsig/office/java/awt/Dimension;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Rectangle;->getSize()Lcom/intsig/office/java/awt/Dimension;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextAlignMode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textAlignMode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public paint(Landroid/graphics/Canvas;)V
    .locals 11

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/16 v2, 0xc

    .line 12
    .line 13
    new-array v2, v2, [I

    .line 14
    .line 15
    const/4 v3, -0x1

    .line 16
    const/4 v4, 0x0

    .line 17
    aput v3, v2, v4

    .line 18
    .line 19
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 20
    .line 21
    const/4 v5, 0x1

    .line 22
    aput v3, v2, v5

    .line 23
    .line 24
    iget v6, v0, Landroid/graphics/Rect;->left:I

    .line 25
    .line 26
    const/4 v7, 0x2

    .line 27
    aput v6, v2, v7

    .line 28
    .line 29
    const/4 v8, 0x3

    .line 30
    const/4 v9, -0x2

    .line 31
    aput v9, v2, v8

    .line 32
    .line 33
    const/4 v8, 0x4

    .line 34
    aput v3, v2, v8

    .line 35
    .line 36
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 37
    .line 38
    const/4 v10, 0x5

    .line 39
    aput v3, v2, v10

    .line 40
    .line 41
    const/4 v10, 0x6

    .line 42
    aput v9, v2, v10

    .line 43
    .line 44
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 45
    .line 46
    const/4 v10, 0x7

    .line 47
    aput v0, v2, v10

    .line 48
    .line 49
    const/16 v10, 0x8

    .line 50
    .line 51
    aput v3, v2, v10

    .line 52
    .line 53
    const/16 v3, 0x9

    .line 54
    .line 55
    aput v9, v2, v3

    .line 56
    .line 57
    const/16 v3, 0xa

    .line 58
    .line 59
    aput v0, v2, v3

    .line 60
    .line 61
    const/16 v0, 0xb

    .line 62
    .line 63
    aput v6, v2, v0

    .line 64
    .line 65
    new-instance v0, Lcom/intsig/office/java/awt/geom/Area;

    .line 66
    .line 67
    invoke-direct {p0, v2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->createShape([I)Lcom/intsig/office/java/awt/Shape;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-direct {v0, v2}, Lcom/intsig/office/java/awt/geom/Area;-><init>(Lcom/intsig/office/java/awt/Shape;)V

    .line 72
    .line 73
    .line 74
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 77
    .line 78
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 82
    .line 83
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 87
    .line 88
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setDither(Z)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->initialMatrix:Landroid/graphics/Matrix;

    .line 96
    .line 97
    const/4 v0, 0x0

    .line 98
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 99
    .line 100
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 101
    .line 102
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 103
    .line 104
    iput v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 105
    .line 106
    iput v7, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    .line 107
    .line 108
    iput-boolean v5, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    .line 109
    .line 110
    iput v8, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->scaleMode:I

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowOrigin:Landroid/graphics/Point;

    .line 113
    .line 114
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportOrigin:Landroid/graphics/Point;

    .line 115
    .line 116
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 117
    .line 118
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 119
    .line 120
    iput-boolean v4, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeIsotropic:Z

    .line 121
    .line 122
    sget-wide v2, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->TWIP_SCALE:D

    .line 123
    .line 124
    invoke-static {v2, v3, v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 129
    .line 130
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->resetMatrix(Landroid/graphics/Canvas;)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

    .line 134
    .line 135
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->initialClip:Lcom/intsig/office/java/awt/Shape;

    .line 136
    .line 137
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->tags:Ljava/util/Vector;

    .line 138
    .line 139
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    .line 140
    .line 141
    .line 142
    move-result v0

    .line 143
    if-ge v4, v0, :cond_1

    .line 144
    .line 145
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->tags:Ljava/util/Vector;

    .line 146
    .line 147
    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    check-cast v0, Lcom/intsig/office/thirdpart/emf/io/Tag;

    .line 152
    .line 153
    instance-of v2, v0, Lcom/intsig/office/thirdpart/emf/EMFTag;

    .line 154
    .line 155
    if-eqz v2, :cond_0

    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->tags:Ljava/util/Vector;

    .line 158
    .line 159
    invoke-virtual {v0, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    check-cast v0, Lcom/intsig/office/thirdpart/emf/EMFTag;

    .line 164
    .line 165
    invoke-virtual {v0, p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V

    .line 166
    .line 167
    .line 168
    goto :goto_1

    .line 169
    :cond_0
    sget-object v2, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->logger:Ljava/util/logging/Logger;

    .line 170
    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    .line 172
    .line 173
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 174
    .line 175
    .line 176
    const-string v6, "unknown tag: "

    .line 177
    .line 178
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    invoke-virtual {v2, v0}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 192
    .line 193
    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 195
    .line 196
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 197
    .line 198
    .line 199
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 200
    .line 201
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 202
    .line 203
    .line 204
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 205
    .line 206
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setDither(Z)V

    .line 207
    .line 208
    .line 209
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 210
    .line 211
    .line 212
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->initialClip:Lcom/intsig/office/java/awt/Shape;

    .line 213
    .line 214
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setClip(Lcom/intsig/office/java/awt/Shape;)V

    .line 215
    .line 216
    .line 217
    return-void
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public resetTransformation()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->resetMatrix(Landroid/graphics/Canvas;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public retoreDC()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->dcStack:Ljava/util/Stack;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->dcStack:Ljava/util/Stack;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;

    .line 16
    .line 17
    iget v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇〇888:I

    .line 18
    .line 19
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 20
    .line 21
    iget v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->o〇0:I

    .line 22
    .line 23
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 24
    .line 25
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->O8:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 26
    .line 27
    iput-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 28
    .line 29
    iget v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->Oo08:I

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    .line 32
    .line 33
    iget-boolean v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->oO80:Z

    .line 34
    .line 35
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    .line 36
    .line 37
    iget v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇80〇808〇O:I

    .line 38
    .line 39
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->scaleMode:I

    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->OO0o〇〇〇〇0:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 42
    .line 43
    iput-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penStroke:Lcom/intsig/office/java/awt/Stroke;

    .line 46
    .line 47
    invoke-direct {p0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setStroke(Lcom/intsig/office/java/awt/Stroke;)V

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇o00〇〇Oo(Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;)Landroid/graphics/Matrix;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 57
    .line 58
    .line 59
    invoke-static {v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇080(Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;)Lcom/intsig/office/java/awt/Shape;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setClip(Lcom/intsig/office/java/awt/Shape;)V

    .line 64
    .line 65
    .line 66
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public saveDC()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;-><init>(Lcom/intsig/office/thirdpart/emf/EMFRenderer;Lcom/intsig/office/thirdpart/emf/〇080;)V

    .line 5
    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->Oo08(Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;Landroid/graphics/Paint;)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 13
    .line 14
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->O8(Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;Landroid/graphics/Matrix;)V

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇o〇(Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;Lcom/intsig/office/java/awt/Shape;)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 27
    .line 28
    iput-object v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->O8:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 29
    .line 30
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 31
    .line 32
    iput v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇〇888:I

    .line 33
    .line 34
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 35
    .line 36
    iput v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->o〇0:I

    .line 37
    .line 38
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    .line 39
    .line 40
    iput v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->Oo08:I

    .line 41
    .line 42
    iget-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    .line 43
    .line 44
    iput-boolean v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->oO80:Z

    .line 45
    .line 46
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->scaleMode:I

    .line 47
    .line 48
    iput v1, v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer$DC;->〇80〇808〇O:I

    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->dcStack:Ljava/util/Stack;

    .line 51
    .line 52
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 56
    .line 57
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public setArcDirection(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->arcDirection:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBkMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->bkMode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBrushOrigin(Landroid/graphics/Point;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushOrigin:Landroid/graphics/Point;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBrushPaint(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v1, v2, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setBrushPaint(Lcom/intsig/office/java/awt/Color;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->brushPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setClip(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Area;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/java/awt/geom/Area;-><init>(Lcom/intsig/office/java/awt/Shape;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCurrClip:Lcom/intsig/office/java/awt/geom/Area;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEscapement(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->escapement:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFigure(Lcom/intsig/office/java/awt/geom/GeneralPath;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->figure:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFont(Lcom/intsig/office/simpletext/font/Font;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->getName()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->getStyle()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v0, :cond_5

    .line 13
    .line 14
    const-string v2, "Serif"

    .line 15
    .line 16
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-nez v2, :cond_4

    .line 21
    .line 22
    const-string v2, "TimesRoman"

    .line 23
    .line 24
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const-string v2, "SansSerif"

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    const-string v3, "sans-serif"

    .line 38
    .line 39
    if-nez v2, :cond_6

    .line 40
    .line 41
    const-string v2, "Helvetica"

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_2

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    const-string v2, "Monospaced"

    .line 51
    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-nez v2, :cond_3

    .line 57
    .line 58
    const-string v2, "Courier"

    .line 59
    .line 60
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_6

    .line 65
    .line 66
    :cond_3
    const-string v3, "monospace"

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_4
    :goto_0
    const-string v3, "serif"

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_5
    const-string v3, ""

    .line 73
    .line 74
    :cond_6
    :goto_1
    if-eqz v1, :cond_a

    .line 75
    .line 76
    const/4 v0, 0x1

    .line 77
    if-eq v1, v0, :cond_9

    .line 78
    .line 79
    const/4 v0, 0x2

    .line 80
    if-eq v1, v0, :cond_8

    .line 81
    .line 82
    const/4 v0, 0x3

    .line 83
    if-eq v1, v0, :cond_7

    .line 84
    .line 85
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_7
    invoke-static {v3, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    goto :goto_2

    .line 93
    :cond_8
    invoke-static {v3, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    goto :goto_2

    .line 98
    :cond_9
    invoke-static {v3, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    goto :goto_2

    .line 103
    :cond_a
    const/4 v0, 0x0

    .line 104
    invoke-static {v3, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    :goto_2
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->getFontSize()D

    .line 111
    .line 112
    .line 113
    move-result-wide v2

    .line 114
    double-to-float p1, v2

    .line 115
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 119
    .line 120
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setMapModeIsotropic(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeIsotropic:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mapModeTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setMeterLimit(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->meterLimit:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPath(Lcom/intsig/office/java/awt/geom/GeneralPath;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->path:Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPathTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->pathTransform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPenPaint(Lcom/intsig/office/java/awt/Color;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPenStroke(Lcom/intsig/office/java/awt/Stroke;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->penStroke:Lcom/intsig/office/java/awt/Stroke;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRop2(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->rop2:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setScaleMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->scaleMode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTextAlignMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textAlignMode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTextBkColor()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textColor:Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setBrushPaint(Lcom/intsig/office/java/awt/Color;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setTextColor(Lcom/intsig/office/java/awt/Color;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->textColor:Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUseCreatePen(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->useCreatePen:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setViewportOrigin(Landroid/graphics/Point;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportOrigin:Landroid/graphics/Point;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 6
    .line 7
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 8
    .line 9
    neg-int v1, v1

    .line 10
    int-to-float v1, v1

    .line 11
    iget p1, p1, Landroid/graphics/Point;->y:I

    .line 12
    .line 13
    neg-int p1, p1

    .line 14
    int-to-float p1, p1

    .line 15
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setViewportSize(Lcom/intsig/office/java/awt/Dimension;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->viewportSize:Lcom/intsig/office/java/awt/Dimension;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fixViewportSize()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->resetTransformation()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWindingRule(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windingRule:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWindowOrigin(Landroid/graphics/Point;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowOrigin:Landroid/graphics/Point;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 6
    .line 7
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 8
    .line 9
    neg-int v1, v1

    .line 10
    int-to-float v1, v1

    .line 11
    iget p1, p1, Landroid/graphics/Point;->y:I

    .line 12
    .line 13
    neg-int p1, p1

    .line 14
    int-to-float p1, p1

    .line 15
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWindowSize(Lcom/intsig/office/java/awt/Dimension;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->windowSize:Lcom/intsig/office/java/awt/Dimension;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fixViewportSize()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->resetTransformation()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public storeGDIObject(ILcom/intsig/office/thirdpart/emf/data/GDIObject;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->gdiObjects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 2
    .line 3
    aput-object p2, v0, p1

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->createMatrix(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->mCanvas:Landroid/graphics/Canvas;

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

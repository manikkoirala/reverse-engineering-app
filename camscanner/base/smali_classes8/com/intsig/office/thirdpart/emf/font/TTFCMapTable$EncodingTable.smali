.class public Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;
.super Ljava/lang/Object;
.source "TTFCMapTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EncodingTable"
.end annotation


# instance fields
.field public encodingID:I

.field public format:I

.field public length:I

.field public offset:J

.field public platformID:I

.field public tableFormat:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;

.field final synthetic this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

.field public version:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public readBody()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 4
    .line 5
    iget-wide v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->offset:J

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->seek(J)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->format:I

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->length:I

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 31
    .line 32
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->version:I

    .line 39
    .line 40
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->format:I

    .line 41
    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    const/4 v1, 0x2

    .line 45
    if-eq v0, v1, :cond_1

    .line 46
    .line 47
    const/4 v1, 0x4

    .line 48
    if-eq v0, v1, :cond_0

    .line 49
    .line 50
    const/4 v1, 0x6

    .line 51
    if-eq v0, v1, :cond_1

    .line 52
    .line 53
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 54
    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v2, "Illegal value for encoding table format: "

    .line 61
    .line 62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->format:I

    .line 66
    .line 67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    new-instance v0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 81
    .line 82
    invoke-direct {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;-><init>(Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;)V

    .line 83
    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->tableFormat:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_1
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 89
    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string v2, "Unimplementet encoding table format: "

    .line 96
    .line 97
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->format:I

    .line 101
    .line 102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_2
    new-instance v0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat0;

    .line 114
    .line 115
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 116
    .line 117
    invoke-direct {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat0;-><init>(Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;)V

    .line 118
    .line 119
    .line 120
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->tableFormat:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;

    .line 121
    .line 122
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->tableFormat:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;

    .line 123
    .line 124
    if-eqz v0, :cond_3

    .line 125
    .line 126
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;->read()V

    .line 127
    .line 128
    .line 129
    :cond_3
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readHeader()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->platformID:I

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->encodingID:I

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readULong()J

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    iput-wide v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->offset:J

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[encoding] PID:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->platformID:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, " EID:"

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->encodingID:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, " format:"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->format:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, " v"

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->version:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$EncodingTable;->tableFormat:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;

    .line 47
    .line 48
    if-eqz v1, :cond_0

    .line 49
    .line 50
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const-string v1, " [no data read]"

    .line 56
    .line 57
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    return-object v0
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

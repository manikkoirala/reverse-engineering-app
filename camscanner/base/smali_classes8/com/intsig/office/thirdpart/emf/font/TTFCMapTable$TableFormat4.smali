.class public Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;
.super Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;
.source "TTFCMapTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TableFormat4"
.end annotation


# instance fields
.field public endCount:[I

.field public idDelta:[S

.field public idRangeOffset:[I

.field public segCount:I

.field public startCount:[I

.field final synthetic this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;


# direct methods
.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat;-><init>(Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getGlyphIndex(I)I
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public read()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    div-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->segCount:I

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 35
    .line 36
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 37
    .line 38
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->segCount:I

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShortArray(I)[I

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 47
    .line 48
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    .line 56
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 57
    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    const-string v3, "reservedPad not 0, but "

    .line 64
    .line 65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string v0, "."

    .line 72
    .line 73
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 84
    .line 85
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 86
    .line 87
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 88
    .line 89
    array-length v1, v1

    .line 90
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShortArray(I)[I

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->startCount:[I

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 97
    .line 98
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 99
    .line 100
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 101
    .line 102
    array-length v1, v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readShortArray(I)[S

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->idDelta:[S

    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable;

    .line 110
    .line 111
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 112
    .line 113
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 114
    .line 115
    array-length v1, v1

    .line 116
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShortArray(I)[I

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->idRangeOffset:[I

    .line 121
    .line 122
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "\n   "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 12
    .line 13
    array-length v1, v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, " sections:"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v1, 0x0

    .line 27
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 28
    .line 29
    array-length v2, v2

    .line 30
    if-ge v1, v2, :cond_0

    .line 31
    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v0, "\n    "

    .line 41
    .line 42
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->startCount:[I

    .line 46
    .line 47
    aget v0, v0, v1

    .line 48
    .line 49
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v0, " to "

    .line 53
    .line 54
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->endCount:[I

    .line 58
    .line 59
    aget v0, v0, v1

    .line 60
    .line 61
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    const-string v0, " : "

    .line 65
    .line 66
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->idDelta:[S

    .line 70
    .line 71
    aget-short v0, v0, v1

    .line 72
    .line 73
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v0, " ("

    .line 77
    .line 78
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFCMapTable$TableFormat4;->idRangeOffset:[I

    .line 82
    .line 83
    aget v0, v0, v1

    .line 84
    .line 85
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v0, ")"

    .line 89
    .line 90
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    add-int/lit8 v1, v1, 0x1

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_0
    return-object v0
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

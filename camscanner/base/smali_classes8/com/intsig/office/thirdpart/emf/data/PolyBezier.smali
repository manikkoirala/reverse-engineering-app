.class public Lcom/intsig/office/thirdpart/emf/data/PolyBezier;
.super Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;
.source "PolyBezier.java"


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;-><init>(IILcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V

    return-void
.end method

.method protected constructor <init>(IILcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V
    .locals 0

    .line 3
    invoke-direct/range {p0 .. p5}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;-><init>(IILcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V
    .locals 6

    const/4 v1, 0x2

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    .line 2
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;-><init>(IILcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 6
    .line 7
    .line 8
    move-result p3

    .line 9
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/PolyBezier;

    .line 10
    .line 11
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readPOINTL(I)[Landroid/graphics/Point;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    invoke-direct {v0, p1, p3, p2}, Lcom/intsig/office/thirdpart/emf/data/PolyBezier;-><init>(Lcom/intsig/office/java/awt/Rectangle;I[Landroid/graphics/Point;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 13

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;->getPoints()[Landroid/graphics/Point;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolygon;->getNumberOfPoints()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    array-length v2, v0

    .line 12
    if-lez v2, :cond_2

    .line 13
    .line 14
    new-instance v2, Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getWindingRule()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-direct {v2, v3}, Lcom/intsig/office/java/awt/geom/GeneralPath;-><init>(I)V

    .line 21
    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    aget-object v3, v0, v3

    .line 25
    .line 26
    iget v4, v3, Landroid/graphics/Point;->x:I

    .line 27
    .line 28
    int-to-float v4, v4

    .line 29
    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    invoke-virtual {v2, v4, v3}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 33
    .line 34
    .line 35
    const/4 v3, 0x1

    .line 36
    const/4 v10, 0x1

    .line 37
    :goto_0
    if-ge v10, v1, :cond_1

    .line 38
    .line 39
    aget-object v3, v0, v10

    .line 40
    .line 41
    add-int/lit8 v4, v10, 0x1

    .line 42
    .line 43
    aget-object v4, v0, v4

    .line 44
    .line 45
    add-int/lit8 v5, v10, 0x2

    .line 46
    .line 47
    aget-object v5, v0, v5

    .line 48
    .line 49
    if-lez v10, :cond_0

    .line 50
    .line 51
    iget v6, v3, Landroid/graphics/Point;->x:I

    .line 52
    .line 53
    int-to-float v6, v6

    .line 54
    iget v3, v3, Landroid/graphics/Point;->y:I

    .line 55
    .line 56
    int-to-float v7, v3

    .line 57
    iget v3, v4, Landroid/graphics/Point;->x:I

    .line 58
    .line 59
    int-to-float v8, v3

    .line 60
    iget v3, v4, Landroid/graphics/Point;->y:I

    .line 61
    .line 62
    int-to-float v9, v3

    .line 63
    iget v3, v5, Landroid/graphics/Point;->x:I

    .line 64
    .line 65
    int-to-float v11, v3

    .line 66
    iget v3, v5, Landroid/graphics/Point;->y:I

    .line 67
    .line 68
    int-to-float v12, v3

    .line 69
    move-object v3, v2

    .line 70
    move v4, v6

    .line 71
    move v5, v7

    .line 72
    move v6, v8

    .line 73
    move v7, v9

    .line 74
    move v8, v11

    .line 75
    move v9, v12

    .line 76
    invoke-virtual/range {v3 .. v9}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->curveTo(FFFFFF)V

    .line 77
    .line 78
    .line 79
    :cond_0
    add-int/lit8 v10, v10, 0x3

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {p1, v2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillAndDrawOrAppend(Lcom/intsig/office/java/awt/Shape;)V

    .line 83
    .line 84
    .line 85
    :cond_2
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;
.super Ljava/lang/Object;
.source "BitmapInfo.java"


# instance fields
.field private header:Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;


# direct methods
.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    invoke-direct {v0, p1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->header:Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->header:Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    return-void
.end method


# virtual methods
.method public getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->header:Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  BitmapInfo\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->header:Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->toString()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;
.super Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;
.source "TTFMaxPTable.java"


# instance fields
.field public maxComponentDepth:I

.field public maxComponentElements:I

.field public maxCompositeContours:I

.field public maxCompositePoints:I

.field public maxContours:I

.field public maxFunctionDefs:I

.field public maxInstructionDefs:I

.field public maxPoints:I

.field public maxSizeOfInstructions:I

.field public maxStackElements:I

.field public maxStorage:I

.field public maxTwilightPoints:I

.field public maxZones:I

.field public numGlyphs:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getTag()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "maxp"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readTable()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;->readVersion()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->numGlyphs:I

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxPoints:I

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxContours:I

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxCompositePoints:I

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxCompositeContours:I

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxZones:I

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxTwilightPoints:I

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxStorage:I

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxFunctionDefs:I

    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 77
    .line 78
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxInstructionDefs:I

    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxStackElements:I

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxSizeOfInstructions:I

    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 101
    .line 102
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxComponentElements:I

    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 109
    .line 110
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 111
    .line 112
    .line 113
    move-result v0

    .line 114
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->maxComponentDepth:I

    .line 115
    .line 116
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  numGlyphs: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->numGlyphs:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
.end method

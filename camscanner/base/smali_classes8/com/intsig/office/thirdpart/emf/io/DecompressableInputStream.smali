.class public Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;
.super Lcom/intsig/office/thirdpart/emf/io/DecodingInputStream;
.source "DecompressableInputStream.java"


# instance fields
.field private b:[B

.field private decompress:Z

.field private i:I

.field private iis:Ljava/util/zip/InflaterInputStream;

.field private in:Ljava/io/InputStream;

.field private len:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/io/DecodingInputStream;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->b:[B

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->len:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->i:I

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->in:Ljava/io/InputStream;

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->decompress:Z

    .line 15
    .line 16
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->len:I

    .line 21
    .line 22
    new-array p1, p1, [B

    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->b:[B

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->in:Ljava/io/InputStream;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :catch_0
    move-exception p1

    .line 33
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
.end method


# virtual methods
.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->i:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->len:I

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    return v0

    .line 9
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->b:[B

    .line 10
    .line 11
    add-int/lit8 v2, v0, 0x1

    .line 12
    .line 13
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->i:I

    .line 14
    .line 15
    aget-byte v0, v1, v0

    .line 16
    .line 17
    and-int/lit16 v0, v0, 0xff

    .line 18
    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public skip(J)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->i:I

    .line 2
    .line 3
    int-to-long v0, v0

    .line 4
    add-long/2addr v0, p1

    .line 5
    long-to-int v1, v0

    .line 6
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->i:I

    .line 7
    .line 8
    return-wide p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public startDecompressing()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->decompress:Z

    .line 3
    .line 4
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->in:Ljava/io/InputStream;

    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/DecompressableInputStream;->iis:Ljava/util/zip/InflaterInputStream;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/SetMapMode;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "SetMapMode.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# instance fields
.field private mode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x11

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;->mode:I

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    invoke-direct {p1, p2}, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;-><init>(I)V

    .line 8
    .line 9
    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;->mode:I

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeIsotropic(Z)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v1, 0x5

    .line 13
    if-ne v0, v1, :cond_1

    .line 14
    .line 15
    const-wide v0, 0x3f9a027525460aa6L    # 0.0254

    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1, v0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    const/4 v1, 0x3

    .line 29
    if-ne v0, v1, :cond_2

    .line 30
    .line 31
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1, v0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    const/4 v1, 0x7

    .line 45
    const/4 v2, 0x1

    .line 46
    if-ne v0, v1, :cond_3

    .line 47
    .line 48
    invoke-virtual {p1, v2}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeIsotropic(Z)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fixViewportSize()V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_3
    const/4 v1, 0x4

    .line 56
    if-ne v0, v1, :cond_4

    .line 57
    .line 58
    const-wide v0, 0x3fd04189374bc6a8L    # 0.254

    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    invoke-static {v0, v1, v0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_4
    const/4 v1, 0x2

    .line 72
    if-ne v0, v1, :cond_5

    .line 73
    .line 74
    const-wide v0, 0x3fb999999999999aL    # 0.1

    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    invoke-static {v0, v1, v0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_5
    if-ne v0, v2, :cond_6

    .line 88
    .line 89
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 90
    .line 91
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 92
    .line 93
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_6
    const/4 v1, 0x6

    .line 102
    if-ne v0, v1, :cond_7

    .line 103
    .line 104
    sget-wide v0, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->TWIP_SCALE:D

    .line 105
    .line 106
    invoke-static {v0, v1, v0, v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setMapModeTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 111
    .line 112
    .line 113
    :cond_7
    :goto_0
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  mode: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;->mode:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/StockObjects;
.super Ljava/lang/Object;
.source "StockObjects.java"


# static fields
.field private static final ANSI_FIXED_FONT:I = 0xb

.field private static final ANSI_VAR_FONT:I = 0xc

.field private static final BLACK_BRUSH:I = 0x4

.field private static final BLACK_PEN:I = 0x7

.field private static final DC_BRUSH:I = 0x12

.field private static final DC_PEN:I = 0x13

.field private static final DEFAULT_GUI_FONT:I = 0x11

.field private static final DEFAULT_PALETTE:I = 0xf

.field private static final DEVICE_DEFAULT_FONT:I = 0xe

.field private static final DKGRAY_BRUSH:I = 0x3

.field private static final GRAY_BRUSH:I = 0x2

.field private static final LTGRAY_BRUSH:I = 0x1

.field private static final NULL_BRUSH:I = 0x5

.field private static final NULL_PEN:I = 0x8

.field private static final OEM_FIXED_FONT:I = 0xa

.field private static final SYSTEM_FIXED_FONT:I = 0x10

.field private static final SYSTEM_FONT:I = 0xd

.field private static final WHITE_BRUSH:I = 0x0

.field private static final WHITE_PEN:I = 0x6

.field private static final objects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1
    const/16 v0, 0x14

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 4
    .line 5
    sput-object v0, Lcom/intsig/office/thirdpart/emf/data/StockObjects;->objects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/office/java/awt/Color;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-direct {v1, v2, v2, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 11
    .line 12
    .line 13
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 14
    .line 15
    sget-object v4, Lcom/intsig/office/java/awt/Color;->WHITE:Lcom/intsig/office/java/awt/Color;

    .line 16
    .line 17
    invoke-direct {v3, v2, v4, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 18
    .line 19
    .line 20
    aput-object v3, v0, v2

    .line 21
    .line 22
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 23
    .line 24
    sget-object v5, Lcom/intsig/office/java/awt/Color;->LIGHT_GRAY:Lcom/intsig/office/java/awt/Color;

    .line 25
    .line 26
    invoke-direct {v3, v2, v5, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 27
    .line 28
    .line 29
    const/4 v5, 0x1

    .line 30
    aput-object v3, v0, v5

    .line 31
    .line 32
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 33
    .line 34
    sget-object v6, Lcom/intsig/office/java/awt/Color;->GRAY:Lcom/intsig/office/java/awt/Color;

    .line 35
    .line 36
    invoke-direct {v3, v2, v6, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 37
    .line 38
    .line 39
    const/4 v6, 0x2

    .line 40
    aput-object v3, v0, v6

    .line 41
    .line 42
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 43
    .line 44
    sget-object v6, Lcom/intsig/office/java/awt/Color;->DARK_GRAY:Lcom/intsig/office/java/awt/Color;

    .line 45
    .line 46
    invoke-direct {v3, v2, v6, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 47
    .line 48
    .line 49
    const/4 v6, 0x3

    .line 50
    aput-object v3, v0, v6

    .line 51
    .line 52
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 53
    .line 54
    sget-object v6, Lcom/intsig/office/java/awt/Color;->BLACK:Lcom/intsig/office/java/awt/Color;

    .line 55
    .line 56
    invoke-direct {v3, v2, v6, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 57
    .line 58
    .line 59
    const/4 v7, 0x4

    .line 60
    aput-object v3, v0, v7

    .line 61
    .line 62
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;

    .line 63
    .line 64
    invoke-direct {v3, v5, v1, v2}, Lcom/intsig/office/thirdpart/emf/data/LogBrush32;-><init>(ILcom/intsig/office/java/awt/Color;I)V

    .line 65
    .line 66
    .line 67
    const/4 v7, 0x5

    .line 68
    aput-object v3, v0, v7

    .line 69
    .line 70
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogPen;

    .line 71
    .line 72
    invoke-direct {v3, v2, v5, v4}, Lcom/intsig/office/thirdpart/emf/data/LogPen;-><init>(IILcom/intsig/office/java/awt/Color;)V

    .line 73
    .line 74
    .line 75
    const/4 v4, 0x6

    .line 76
    aput-object v3, v0, v4

    .line 77
    .line 78
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogPen;

    .line 79
    .line 80
    invoke-direct {v3, v2, v5, v6}, Lcom/intsig/office/thirdpart/emf/data/LogPen;-><init>(IILcom/intsig/office/java/awt/Color;)V

    .line 81
    .line 82
    .line 83
    const/4 v4, 0x7

    .line 84
    aput-object v3, v0, v4

    .line 85
    .line 86
    new-instance v3, Lcom/intsig/office/thirdpart/emf/data/LogPen;

    .line 87
    .line 88
    invoke-direct {v3, v7, v5, v1}, Lcom/intsig/office/thirdpart/emf/data/LogPen;-><init>(IILcom/intsig/office/java/awt/Color;)V

    .line 89
    .line 90
    .line 91
    const/16 v1, 0x8

    .line 92
    .line 93
    aput-object v3, v0, v1

    .line 94
    .line 95
    new-instance v1, Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 96
    .line 97
    new-instance v3, Lcom/intsig/office/simpletext/font/Font;

    .line 98
    .line 99
    const-string v4, "Monospaced"

    .line 100
    .line 101
    const/16 v5, 0xc

    .line 102
    .line 103
    invoke-direct {v3, v4, v2, v5}, Lcom/intsig/office/simpletext/font/Font;-><init>(Ljava/lang/String;II)V

    .line 104
    .line 105
    .line 106
    invoke-direct {v1, v3}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;-><init>(Lcom/intsig/office/simpletext/font/Font;)V

    .line 107
    .line 108
    .line 109
    const/16 v3, 0xa

    .line 110
    .line 111
    aput-object v1, v0, v3

    .line 112
    .line 113
    const/16 v4, 0xb

    .line 114
    .line 115
    aput-object v1, v0, v4

    .line 116
    .line 117
    new-instance v1, Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 118
    .line 119
    new-instance v4, Lcom/intsig/office/simpletext/font/Font;

    .line 120
    .line 121
    const-string v6, "SansSerif"

    .line 122
    .line 123
    invoke-direct {v4, v6, v2, v5}, Lcom/intsig/office/simpletext/font/Font;-><init>(Ljava/lang/String;II)V

    .line 124
    .line 125
    .line 126
    invoke-direct {v1, v4}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;-><init>(Lcom/intsig/office/simpletext/font/Font;)V

    .line 127
    .line 128
    .line 129
    aput-object v1, v0, v5

    .line 130
    .line 131
    new-instance v1, Lcom/intsig/office/thirdpart/emf/data/LogFontW;

    .line 132
    .line 133
    new-instance v4, Lcom/intsig/office/simpletext/font/Font;

    .line 134
    .line 135
    const-string v6, "Dialog"

    .line 136
    .line 137
    invoke-direct {v4, v6, v2, v5}, Lcom/intsig/office/simpletext/font/Font;-><init>(Ljava/lang/String;II)V

    .line 138
    .line 139
    .line 140
    invoke-direct {v1, v4}, Lcom/intsig/office/thirdpart/emf/data/LogFontW;-><init>(Lcom/intsig/office/simpletext/font/Font;)V

    .line 141
    .line 142
    .line 143
    const/16 v2, 0xd

    .line 144
    .line 145
    aput-object v1, v0, v2

    .line 146
    .line 147
    const/16 v2, 0xe

    .line 148
    .line 149
    aget-object v4, v0, v5

    .line 150
    .line 151
    aput-object v4, v0, v2

    .line 152
    .line 153
    const/16 v2, 0x10

    .line 154
    .line 155
    aget-object v3, v0, v3

    .line 156
    .line 157
    aput-object v3, v0, v2

    .line 158
    .line 159
    const/16 v2, 0x11

    .line 160
    .line 161
    aput-object v1, v0, v2

    .line 162
    .line 163
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static getStockObject(I)Lcom/intsig/office/thirdpart/emf/data/GDIObject;
    .locals 3

    .line 1
    if-gez p0, :cond_2

    .line 2
    .line 3
    const/high16 v0, -0x80000000

    .line 4
    .line 5
    xor-int/2addr p0, v0

    .line 6
    sget-object v0, Lcom/intsig/office/thirdpart/emf/data/StockObjects;->objects:[Lcom/intsig/office/thirdpart/emf/data/GDIObject;

    .line 7
    .line 8
    array-length v1, v0

    .line 9
    if-ge p0, v1, :cond_1

    .line 10
    .line 11
    aget-object v0, v0, p0

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "Stock object not yet supported: "

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p0

    .line 35
    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    throw v0

    .line 39
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 40
    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v2, "Stock object is out of bounds: "

    .line 47
    .line 48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    throw v0

    .line 62
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 63
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v2, "Value does not represent a stock object: "

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p0

    .line 81
    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    throw v0
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

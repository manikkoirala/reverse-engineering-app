.class public abstract Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;
.super Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;
.source "TaggedInputStream.java"


# instance fields
.field protected actionSet:Lcom/intsig/office/thirdpart/emf/io/ActionSet;

.field private tagHeader:Lcom/intsig/office/thirdpart/emf/io/TagHeader;

.field protected tagSet:Lcom/intsig/office/thirdpart/emf/io/TagSet;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/io/TagSet;Lcom/intsig/office/thirdpart/emf/io/ActionSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/io/TagSet;Lcom/intsig/office/thirdpart/emf/io/ActionSet;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/io/TagSet;Lcom/intsig/office/thirdpart/emf/io/ActionSet;Z)V
    .locals 1

    const/16 v0, 0x8

    .line 2
    invoke-direct {p0, p1, p4, v0}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;-><init>(Ljava/io/InputStream;ZI)V

    .line 3
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagSet:Lcom/intsig/office/thirdpart/emf/io/TagSet;

    .line 4
    iput-object p3, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->actionSet:Lcom/intsig/office/thirdpart/emf/io/ActionSet;

    return-void
.end method


# virtual methods
.method public addAction(Lcom/intsig/office/thirdpart/emf/io/Action;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->actionSet:Lcom/intsig/office/thirdpart/emf/io/ActionSet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/thirdpart/emf/io/ActionSet;->addAction(Lcom/intsig/office/thirdpart/emf/io/Action;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagSet:Lcom/intsig/office/thirdpart/emf/io/TagSet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTagHeader()Lcom/intsig/office/thirdpart/emf/io/TagHeader;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagHeader:Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readAction()Lcom/intsig/office/thirdpart/emf/io/Action;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->readActionHeader()Lcom/intsig/office/thirdpart/emf/io/ActionHeader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/io/ActionHeader;->getLength()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    long-to-int v2, v1

    .line 14
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->actionSet:Lcom/intsig/office/thirdpart/emf/io/ActionSet;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/io/ActionHeader;->getAction()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-virtual {v1, v3}, Lcom/intsig/office/thirdpart/emf/io/ActionSet;->get(I)Lcom/intsig/office/thirdpart/emf/io/Action;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->pushBuffer(I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/io/ActionHeader;->getAction()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-virtual {v1, v0, p0, v2}, Lcom/intsig/office/thirdpart/emf/io/Action;->read(ILcom/intsig/office/thirdpart/emf/io/TaggedInputStream;I)Lcom/intsig/office/thirdpart/emf/io/Action;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->popBuffer()[B

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    if-nez v1, :cond_1

    .line 40
    .line 41
    return-object v0

    .line 42
    :cond_1
    new-instance v2, Lcom/intsig/office/thirdpart/emf/io/IncompleteActionException;

    .line 43
    .line 44
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/thirdpart/emf/io/IncompleteActionException;-><init>(Lcom/intsig/office/thirdpart/emf/io/Action;[B)V

    .line 45
    .line 46
    .line 47
    throw v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract readActionHeader()Lcom/intsig/office/thirdpart/emf/io/ActionHeader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public readTag()Lcom/intsig/office/thirdpart/emf/io/Tag;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->readTagHeader()Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagHeader:Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/io/TagHeader;->getLength()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    long-to-int v1, v0

    .line 16
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagSet:Lcom/intsig/office/thirdpart/emf/io/TagSet;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagHeader:Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/emf/io/TagHeader;->getTag()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->get(I)Lcom/intsig/office/thirdpart/emf/io/Tag;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p0, v1}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->pushBuffer(I)V

    .line 29
    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;->tagHeader:Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/emf/io/TagHeader;->getTag()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v0, v2, p0, v1}, Lcom/intsig/office/thirdpart/emf/io/Tag;->read(ILcom/intsig/office/thirdpart/emf/io/TaggedInputStream;I)Lcom/intsig/office/thirdpart/emf/io/Tag;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->popBuffer()[B

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-nez v1, :cond_1

    .line 46
    .line 47
    return-object v0

    .line 48
    :cond_1
    new-instance v2, Lcom/intsig/office/thirdpart/emf/io/IncompleteTagException;

    .line 49
    .line 50
    invoke-direct {v2, v0, v1}, Lcom/intsig/office/thirdpart/emf/io/IncompleteTagException;-><init>(Lcom/intsig/office/thirdpart/emf/io/Tag;[B)V

    .line 51
    .line 52
    .line 53
    throw v2
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract readTagHeader()Lcom/intsig/office/thirdpart/emf/io/TagHeader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.class public Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;
.super Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;
.source "TTFHHeaTable.java"


# instance fields
.field public advanceWidthMax:I

.field public ascender:S

.field public caretSlopeRise:S

.field public caretSlopeRun:S

.field public descender:S

.field public lineGap:S

.field public metricDataFormat:S

.field public minLeftSideBearing:S

.field public minRightSideBearing:S

.field public numberOfHMetrics:I

.field public xMaxExtent:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getTag()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "hhea"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readTable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;->readVersion()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->ascender:S

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->descender:S

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->lineGap:S

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUFWord()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->advanceWidthMax:I

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->minLeftSideBearing:S

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->minRightSideBearing:S

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->xMaxExtent:S

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readShort()S

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->caretSlopeRise:S

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readShort()S

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->caretSlopeRun:S

    .line 75
    .line 76
    const/4 v0, 0x0

    .line 77
    :goto_0
    const/4 v1, 0x5

    .line 78
    if-ge v0, v1, :cond_0

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 81
    .line 82
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->checkShortZero()V

    .line 83
    .line 84
    .line 85
    add-int/lit8 v0, v0, 0x1

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readShort()S

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    iput-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->metricDataFormat:S

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 97
    .line 98
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readUShort()I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->numberOfHMetrics:I

    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;->toString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v0, "\n  asc:"

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->ascender:S

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v0, " desc:"

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->descender:S

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v0, " lineGap:"

    .line 34
    .line 35
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->lineGap:S

    .line 39
    .line 40
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v0, " maxAdvance:"

    .line 44
    .line 45
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->advanceWidthMax:I

    .line 49
    .line 50
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v0, "\n  metricDataFormat:"

    .line 66
    .line 67
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    iget-short v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->metricDataFormat:S

    .line 71
    .line 72
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    const-string v0, " #HMetrics:"

    .line 76
    .line 77
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFHHeaTable;->numberOfHMetrics:I

    .line 81
    .line 82
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    return-object v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

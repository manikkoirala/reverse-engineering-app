.class public Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;
.super Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;
.source "TTFGlyfTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$CompositeGlyph;,
        Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$SimpleGlyph;,
        Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;
    }
.end annotation


# static fields
.field private static final READ_GLYPHS:Z = false


# instance fields
.field public glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

.field private offsets:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getGlyph(I)Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 2
    .line 3
    aget-object v0, v0, p1

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->pushPos()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->offsets:[J

    .line 16
    .line 17
    aget-wide v2, v1, p1

    .line 18
    .line 19
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->seek(J)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readShort()S

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-ltz v0, :cond_1

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 31
    .line 32
    new-instance v2, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$SimpleGlyph;

    .line 33
    .line 34
    invoke-direct {v2, p0, v0}, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$SimpleGlyph;-><init>(Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;I)V

    .line 35
    .line 36
    .line 37
    aput-object v2, v1, p1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$CompositeGlyph;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$CompositeGlyph;-><init>(Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;)V

    .line 45
    .line 46
    .line 47
    aput-object v1, v0, p1

    .line 48
    .line 49
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 50
    .line 51
    aget-object v0, v0, p1

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->read()V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 57
    .line 58
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->popPos()V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 62
    .line 63
    aget-object p1, v0, p1

    .line 64
    .line 65
    return-object p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "glyf"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readTable()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-string v0, "maxp"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->getTable(Ljava/lang/String;)Lcom/intsig/office/thirdpart/emf/font/TTFTable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;

    .line 8
    .line 9
    iget v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFMaxPTable;->numGlyphs:I

    .line 10
    .line 11
    new-array v0, v0, [Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 14
    .line 15
    const-string v0, "loca"

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->getTable(Ljava/lang/String;)Lcom/intsig/office/thirdpart/emf/font/TTFTable;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/office/thirdpart/emf/font/TTFLocaTable;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFLocaTable;->offset:[J

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->offsets:[J

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFVersionTable;->toString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 7
    .line 8
    array-length v2, v2

    .line 9
    if-ge v1, v2, :cond_0

    .line 10
    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v0, "\n  #"

    .line 20
    .line 21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v0, ": "

    .line 28
    .line 29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;->glyphs:[Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;

    .line 33
    .line 34
    aget-object v0, v0, v1

    .line 35
    .line 36
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    add-int/lit8 v1, v1, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

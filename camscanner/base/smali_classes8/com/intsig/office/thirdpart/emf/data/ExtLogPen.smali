.class public Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;
.super Lcom/intsig/office/thirdpart/emf/data/AbstractPen;
.source "ExtLogPen.java"


# instance fields
.field private brushStyle:I

.field private color:Lcom/intsig/office/java/awt/Color;

.field private hatch:I

.field private penStyle:I

.field private style:[I

.field private width:I


# direct methods
.method public constructor <init>(IIILcom/intsig/office/java/awt/Color;I[I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/AbstractPen;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->penStyle:I

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->width:I

    .line 4
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->brushStyle:I

    .line 5
    iput-object p4, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->color:Lcom/intsig/office/java/awt/Color;

    .line 6
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->hatch:I

    .line 7
    iput-object p6, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->style:[I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/AbstractPen;-><init>()V

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->penStyle:I

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->width:I

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readUINT()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->brushStyle:I

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readCOLORREF()Lcom/intsig/office/java/awt/Color;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->color:Lcom/intsig/office/java/awt/Color;

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readULONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->hatch:I

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v1, 0x2c

    if-le p2, v1, :cond_0

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 16
    :cond_0
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD(I)[I

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->style:[I

    return-void
.end method


# virtual methods
.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setUseCreatePen(Z)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->color:Lcom/intsig/office/java/awt/Color;

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setPenPaint(Lcom/intsig/office/java/awt/Color;)V

    .line 8
    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->penStyle:I

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->style:[I

    .line 13
    .line 14
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->width:I

    .line 15
    .line 16
    int-to-float v2, v2

    .line 17
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/intsig/office/thirdpart/emf/data/AbstractPen;->createStroke(Lcom/intsig/office/thirdpart/emf/EMFRenderer;I[IF)Lcom/intsig/office/java/awt/Stroke;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setPenStroke(Lcom/intsig/office/java/awt/Stroke;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  ExtLogPen\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9
    .line 10
    .line 11
    const-string v1, "    penStyle: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 14
    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->penStyle:I

    .line 17
    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 23
    .line 24
    .line 25
    const-string v1, "\n"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 28
    .line 29
    .line 30
    const-string v2, "    width: "

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 33
    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->width:I

    .line 36
    .line 37
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    .line 42
    .line 43
    const-string v2, "    brushStyle: "

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    .line 47
    .line 48
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->brushStyle:I

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 54
    .line 55
    .line 56
    const-string v2, "    color: "

    .line 57
    .line 58
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 59
    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->color:Lcom/intsig/office/java/awt/Color;

    .line 62
    .line 63
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 67
    .line 68
    .line 69
    const-string v2, "    hatch: "

    .line 70
    .line 71
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    .line 73
    .line 74
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->hatch:I

    .line 75
    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    .line 81
    .line 82
    const/4 v2, 0x0

    .line 83
    :goto_0
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->style:[I

    .line 84
    .line 85
    array-length v3, v3

    .line 86
    if-ge v2, v3, :cond_0

    .line 87
    .line 88
    const-string v3, "      style["

    .line 89
    .line 90
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 94
    .line 95
    .line 96
    const-string v3, "]: "

    .line 97
    .line 98
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 99
    .line 100
    .line 101
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/data/ExtLogPen;->style:[I

    .line 102
    .line 103
    aget v3, v3, v2

    .line 104
    .line 105
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 109
    .line 110
    .line 111
    add-int/lit8 v2, v2, 0x1

    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    return-object v0
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

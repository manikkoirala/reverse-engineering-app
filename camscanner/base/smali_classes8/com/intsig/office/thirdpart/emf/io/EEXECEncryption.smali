.class public Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;
.super Ljava/io/OutputStream;
.source "EEXECEncryption.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/io/EEXECConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption$IntOutputStream;
    }
.end annotation


# instance fields
.field private c1:I

.field private c2:I

.field private first:Z

.field private n:I

.field private out:Ljava/io/OutputStream;

.field private r:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2

    const v0, 0xd971

    const/4 v1, 0x4

    .line 1
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;-><init>(Ljava/io/OutputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1

    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;-><init>(Ljava/io/OutputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;II)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->first:Z

    .line 5
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->out:Ljava/io/OutputStream;

    const p1, 0xce6d

    .line 6
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->c1:I

    const/16 p1, 0x58bf

    .line 7
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->c2:I

    .line 8
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->r:I

    .line 9
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->n:I

    return-void
.end method

.method private encrypt(I)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->r:I

    .line 2
    .line 3
    ushr-int/lit8 v1, v0, 0x8

    .line 4
    .line 5
    xor-int/2addr p1, v1

    .line 6
    rem-int/lit16 p1, p1, 0x100

    .line 7
    .line 8
    add-int/2addr v0, p1

    .line 9
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->c1:I

    .line 10
    .line 11
    mul-int v0, v0, v1

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->c2:I

    .line 14
    .line 15
    add-int/2addr v0, v1

    .line 16
    const/high16 v1, 0x10000

    .line 17
    .line 18
    rem-int/2addr v0, v1

    .line 19
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->r:I

    .line 20
    .line 21
    return p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static encryptString([III)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption$IntOutputStream;

    .line 2
    .line 3
    array-length v1, p0

    .line 4
    add-int/lit8 v1, v1, 0x4

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption$IntOutputStream;-><init>(ILcom/intsig/office/thirdpart/emf/io/〇080;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;

    .line 11
    .line 12
    invoke-direct {v1, v0, p1, p2}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;-><init>(Ljava/io/OutputStream;II)V

    .line 13
    .line 14
    .line 15
    const/4 p1, 0x0

    .line 16
    :goto_0
    array-length p2, p0

    .line 17
    if-ge p1, p2, :cond_0

    .line 18
    .line 19
    aget p2, p0, p1

    .line 20
    .line 21
    invoke-virtual {v1, p2}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->write(I)V

    .line 22
    .line 23
    .line 24
    add-int/lit8 p1, p1, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-static {v0}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption$IntOutputStream;->Oo08(Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption$IntOutputStream;)[I

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    return-object p0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->flush()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Ljava/io/OutputStream;->close()V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->out:Ljava/io/OutputStream;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/io/OutputStream;->flush()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->out:Ljava/io/OutputStream;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->first:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    const/4 v1, 0x0

    .line 7
    :goto_0
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->n:I

    .line 8
    .line 9
    if-ge v1, v2, :cond_0

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->out:Ljava/io/OutputStream;

    .line 12
    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->encrypt(I)I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 18
    .line 19
    .line 20
    add-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->first:Z

    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->out:Ljava/io/OutputStream;

    .line 26
    .line 27
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/EEXECEncryption;->encrypt(I)I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
.end method

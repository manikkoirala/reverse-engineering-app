.class public Lcom/intsig/office/thirdpart/emf/EMFTagSet;
.super Lcom/intsig/office/thirdpart/emf/io/TagSet;
.source "EMFTagSet.java"


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/io/TagSet;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    if-lt p1, v0, :cond_0

    .line 6
    .line 7
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyBezier;

    .line 8
    .line 9
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyBezier;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 13
    .line 14
    .line 15
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/EMFPolygon;

    .line 16
    .line 17
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/EMFPolygon;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 21
    .line 22
    .line 23
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Polyline;

    .line 24
    .line 25
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Polyline;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 29
    .line 30
    .line 31
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyBezierTo;

    .line 32
    .line 33
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyBezierTo;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 37
    .line 38
    .line 39
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolylineTo;

    .line 40
    .line 41
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolylineTo;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 45
    .line 46
    .line 47
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;

    .line 48
    .line 49
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;-><init>()V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 53
    .line 54
    .line 55
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyPolygon;

    .line 56
    .line 57
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyPolygon;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 61
    .line 62
    .line 63
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetWindowExtEx;

    .line 64
    .line 65
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetWindowExtEx;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 69
    .line 70
    .line 71
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetWindowOrgEx;

    .line 72
    .line 73
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetWindowOrgEx;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 77
    .line 78
    .line 79
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetViewportExtEx;

    .line 80
    .line 81
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetViewportExtEx;-><init>()V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 85
    .line 86
    .line 87
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetViewportOrgEx;

    .line 88
    .line 89
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetViewportOrgEx;-><init>()V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 93
    .line 94
    .line 95
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetBrushOrgEx;

    .line 96
    .line 97
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetBrushOrgEx;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 101
    .line 102
    .line 103
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/EOF;

    .line 104
    .line 105
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/EOF;-><init>()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 109
    .line 110
    .line 111
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetPixelV;

    .line 112
    .line 113
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetPixelV;-><init>()V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 117
    .line 118
    .line 119
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetMapperFlags;

    .line 120
    .line 121
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetMapperFlags;-><init>()V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 125
    .line 126
    .line 127
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;

    .line 128
    .line 129
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetMapMode;-><init>()V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 133
    .line 134
    .line 135
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetBkMode;

    .line 136
    .line 137
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetBkMode;-><init>()V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 141
    .line 142
    .line 143
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetPolyFillMode;

    .line 144
    .line 145
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetPolyFillMode;-><init>()V

    .line 146
    .line 147
    .line 148
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 149
    .line 150
    .line 151
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetROP2;

    .line 152
    .line 153
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetROP2;-><init>()V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 157
    .line 158
    .line 159
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetStretchBltMode;

    .line 160
    .line 161
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetStretchBltMode;-><init>()V

    .line 162
    .line 163
    .line 164
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 165
    .line 166
    .line 167
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetTextAlign;

    .line 168
    .line 169
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetTextAlign;-><init>()V

    .line 170
    .line 171
    .line 172
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 173
    .line 174
    .line 175
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetTextColor;

    .line 176
    .line 177
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetTextColor;-><init>()V

    .line 178
    .line 179
    .line 180
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 181
    .line 182
    .line 183
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetBkColor;

    .line 184
    .line 185
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetBkColor;-><init>()V

    .line 186
    .line 187
    .line 188
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 189
    .line 190
    .line 191
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/OffsetClipRgn;

    .line 192
    .line 193
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/OffsetClipRgn;-><init>()V

    .line 194
    .line 195
    .line 196
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 197
    .line 198
    .line 199
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/MoveToEx;

    .line 200
    .line 201
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/MoveToEx;-><init>()V

    .line 202
    .line 203
    .line 204
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 205
    .line 206
    .line 207
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetMetaRgn;

    .line 208
    .line 209
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetMetaRgn;-><init>()V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 213
    .line 214
    .line 215
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExcludeClipRect;

    .line 216
    .line 217
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExcludeClipRect;-><init>()V

    .line 218
    .line 219
    .line 220
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 221
    .line 222
    .line 223
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/IntersectClipRect;

    .line 224
    .line 225
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/IntersectClipRect;-><init>()V

    .line 226
    .line 227
    .line 228
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 229
    .line 230
    .line 231
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ScaleViewportExtEx;

    .line 232
    .line 233
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ScaleViewportExtEx;-><init>()V

    .line 234
    .line 235
    .line 236
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 237
    .line 238
    .line 239
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ScaleWindowExtEx;

    .line 240
    .line 241
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ScaleWindowExtEx;-><init>()V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 245
    .line 246
    .line 247
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SaveDC;

    .line 248
    .line 249
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SaveDC;-><init>()V

    .line 250
    .line 251
    .line 252
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 253
    .line 254
    .line 255
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/RestoreDC;

    .line 256
    .line 257
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/RestoreDC;-><init>()V

    .line 258
    .line 259
    .line 260
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 261
    .line 262
    .line 263
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetWorldTransform;

    .line 264
    .line 265
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetWorldTransform;-><init>()V

    .line 266
    .line 267
    .line 268
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 269
    .line 270
    .line 271
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ModifyWorldTransform;

    .line 272
    .line 273
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ModifyWorldTransform;-><init>()V

    .line 274
    .line 275
    .line 276
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 277
    .line 278
    .line 279
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SelectObject;

    .line 280
    .line 281
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SelectObject;-><init>()V

    .line 282
    .line 283
    .line 284
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 285
    .line 286
    .line 287
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/CreatePen;

    .line 288
    .line 289
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/CreatePen;-><init>()V

    .line 290
    .line 291
    .line 292
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 293
    .line 294
    .line 295
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/CreateBrushIndirect;

    .line 296
    .line 297
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/CreateBrushIndirect;-><init>()V

    .line 298
    .line 299
    .line 300
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 301
    .line 302
    .line 303
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/DeleteObject;

    .line 304
    .line 305
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/DeleteObject;-><init>()V

    .line 306
    .line 307
    .line 308
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 309
    .line 310
    .line 311
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/AngleArc;

    .line 312
    .line 313
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/AngleArc;-><init>()V

    .line 314
    .line 315
    .line 316
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 317
    .line 318
    .line 319
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Ellipse;

    .line 320
    .line 321
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Ellipse;-><init>()V

    .line 322
    .line 323
    .line 324
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 325
    .line 326
    .line 327
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/EMFRectangle;

    .line 328
    .line 329
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/EMFRectangle;-><init>()V

    .line 330
    .line 331
    .line 332
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 333
    .line 334
    .line 335
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/RoundRect;

    .line 336
    .line 337
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/RoundRect;-><init>()V

    .line 338
    .line 339
    .line 340
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 341
    .line 342
    .line 343
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Arc;

    .line 344
    .line 345
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Arc;-><init>()V

    .line 346
    .line 347
    .line 348
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 349
    .line 350
    .line 351
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Chord;

    .line 352
    .line 353
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Chord;-><init>()V

    .line 354
    .line 355
    .line 356
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 357
    .line 358
    .line 359
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Pie;

    .line 360
    .line 361
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Pie;-><init>()V

    .line 362
    .line 363
    .line 364
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 365
    .line 366
    .line 367
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SelectPalette;

    .line 368
    .line 369
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SelectPalette;-><init>()V

    .line 370
    .line 371
    .line 372
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 373
    .line 374
    .line 375
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ResizePalette;

    .line 376
    .line 377
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ResizePalette;-><init>()V

    .line 378
    .line 379
    .line 380
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 381
    .line 382
    .line 383
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/RealizePalette;

    .line 384
    .line 385
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/RealizePalette;-><init>()V

    .line 386
    .line 387
    .line 388
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 389
    .line 390
    .line 391
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtFloodFill;

    .line 392
    .line 393
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtFloodFill;-><init>()V

    .line 394
    .line 395
    .line 396
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 397
    .line 398
    .line 399
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/LineTo;

    .line 400
    .line 401
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/LineTo;-><init>()V

    .line 402
    .line 403
    .line 404
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 405
    .line 406
    .line 407
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ArcTo;

    .line 408
    .line 409
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ArcTo;-><init>()V

    .line 410
    .line 411
    .line 412
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 413
    .line 414
    .line 415
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyDraw;

    .line 416
    .line 417
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyDraw;-><init>()V

    .line 418
    .line 419
    .line 420
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 421
    .line 422
    .line 423
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetArcDirection;

    .line 424
    .line 425
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetArcDirection;-><init>()V

    .line 426
    .line 427
    .line 428
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 429
    .line 430
    .line 431
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetMiterLimit;

    .line 432
    .line 433
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetMiterLimit;-><init>()V

    .line 434
    .line 435
    .line 436
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 437
    .line 438
    .line 439
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/BeginPath;

    .line 440
    .line 441
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/BeginPath;-><init>()V

    .line 442
    .line 443
    .line 444
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 445
    .line 446
    .line 447
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/EndPath;

    .line 448
    .line 449
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/EndPath;-><init>()V

    .line 450
    .line 451
    .line 452
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 453
    .line 454
    .line 455
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/CloseFigure;

    .line 456
    .line 457
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/CloseFigure;-><init>()V

    .line 458
    .line 459
    .line 460
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 461
    .line 462
    .line 463
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/FillPath;

    .line 464
    .line 465
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/FillPath;-><init>()V

    .line 466
    .line 467
    .line 468
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 469
    .line 470
    .line 471
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/StrokeAndFillPath;

    .line 472
    .line 473
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/StrokeAndFillPath;-><init>()V

    .line 474
    .line 475
    .line 476
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 477
    .line 478
    .line 479
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/StrokePath;

    .line 480
    .line 481
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/StrokePath;-><init>()V

    .line 482
    .line 483
    .line 484
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 485
    .line 486
    .line 487
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/FlattenPath;

    .line 488
    .line 489
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/FlattenPath;-><init>()V

    .line 490
    .line 491
    .line 492
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 493
    .line 494
    .line 495
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/WidenPath;

    .line 496
    .line 497
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/WidenPath;-><init>()V

    .line 498
    .line 499
    .line 500
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 501
    .line 502
    .line 503
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SelectClipPath;

    .line 504
    .line 505
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SelectClipPath;-><init>()V

    .line 506
    .line 507
    .line 508
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 509
    .line 510
    .line 511
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/AbortPath;

    .line 512
    .line 513
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/AbortPath;-><init>()V

    .line 514
    .line 515
    .line 516
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 517
    .line 518
    .line 519
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/GDIComment;

    .line 520
    .line 521
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/GDIComment;-><init>()V

    .line 522
    .line 523
    .line 524
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 525
    .line 526
    .line 527
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;

    .line 528
    .line 529
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;-><init>()V

    .line 530
    .line 531
    .line 532
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 533
    .line 534
    .line 535
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;

    .line 536
    .line 537
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/BitBlt;-><init>()V

    .line 538
    .line 539
    .line 540
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 541
    .line 542
    .line 543
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;

    .line 544
    .line 545
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;-><init>()V

    .line 546
    .line 547
    .line 548
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 549
    .line 550
    .line 551
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtCreateFontIndirectW;

    .line 552
    .line 553
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtCreateFontIndirectW;-><init>()V

    .line 554
    .line 555
    .line 556
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 557
    .line 558
    .line 559
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtTextOutA;

    .line 560
    .line 561
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtTextOutA;-><init>()V

    .line 562
    .line 563
    .line 564
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 565
    .line 566
    .line 567
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtTextOutW;

    .line 568
    .line 569
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtTextOutW;-><init>()V

    .line 570
    .line 571
    .line 572
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 573
    .line 574
    .line 575
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyBezier16;

    .line 576
    .line 577
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyBezier16;-><init>()V

    .line 578
    .line 579
    .line 580
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 581
    .line 582
    .line 583
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Polygon16;

    .line 584
    .line 585
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Polygon16;-><init>()V

    .line 586
    .line 587
    .line 588
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 589
    .line 590
    .line 591
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Polyline16;

    .line 592
    .line 593
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/Polyline16;-><init>()V

    .line 594
    .line 595
    .line 596
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 597
    .line 598
    .line 599
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyBezierTo16;

    .line 600
    .line 601
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyBezierTo16;-><init>()V

    .line 602
    .line 603
    .line 604
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 605
    .line 606
    .line 607
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolylineTo16;

    .line 608
    .line 609
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolylineTo16;-><init>()V

    .line 610
    .line 611
    .line 612
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 613
    .line 614
    .line 615
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline16;

    .line 616
    .line 617
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline16;-><init>()V

    .line 618
    .line 619
    .line 620
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 621
    .line 622
    .line 623
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyPolygon16;

    .line 624
    .line 625
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyPolygon16;-><init>()V

    .line 626
    .line 627
    .line 628
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 629
    .line 630
    .line 631
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/PolyDraw16;

    .line 632
    .line 633
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/PolyDraw16;-><init>()V

    .line 634
    .line 635
    .line 636
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 637
    .line 638
    .line 639
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;

    .line 640
    .line 641
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;-><init>()V

    .line 642
    .line 643
    .line 644
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 645
    .line 646
    .line 647
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/ExtCreatePen;

    .line 648
    .line 649
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/ExtCreatePen;-><init>()V

    .line 650
    .line 651
    .line 652
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 653
    .line 654
    .line 655
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/SetICMMode;

    .line 656
    .line 657
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/SetICMMode;-><init>()V

    .line 658
    .line 659
    .line 660
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 661
    .line 662
    .line 663
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;

    .line 664
    .line 665
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/AlphaBlend;-><init>()V

    .line 666
    .line 667
    .line 668
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 669
    .line 670
    .line 671
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/GradientFill;

    .line 672
    .line 673
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/GradientFill;-><init>()V

    .line 674
    .line 675
    .line 676
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/TagSet;->addTag(Lcom/intsig/office/thirdpart/emf/io/Tag;)V

    .line 677
    .line 678
    .line 679
    :cond_0
    return-void
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;
.super Lcom/intsig/office/thirdpart/emf/data/AbstractClipPath;
.source "ExtSelectClipRgn.java"


# instance fields
.field private rgn:Lcom/intsig/office/thirdpart/emf/data/Region;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x5

    const/16 v2, 0x4b

    .line 1
    invoke-direct {p0, v2, v0, v1}, Lcom/intsig/office/thirdpart/emf/data/AbstractClipPath;-><init>(III)V

    return-void
.end method

.method public constructor <init>(ILcom/intsig/office/thirdpart/emf/data/Region;)V
    .locals 2

    const/16 v0, 0x4b

    const/4 v1, 0x1

    .line 2
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/office/thirdpart/emf/data/AbstractClipPath;-><init>(III)V

    .line 3
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;->rgn:Lcom/intsig/office/thirdpart/emf/data/Region;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 6
    .line 7
    .line 8
    move-result p3

    .line 9
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;

    .line 10
    .line 11
    const/16 v1, 0x8

    .line 12
    .line 13
    if-le p1, v1, :cond_0

    .line 14
    .line 15
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/Region;

    .line 16
    .line 17
    invoke-direct {p1, p2}, Lcom/intsig/office/thirdpart/emf/data/Region;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    invoke-direct {v0, p3, p1}, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;-><init>(ILcom/intsig/office/thirdpart/emf/data/Region;)V

    .line 23
    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;->rgn:Lcom/intsig/office/thirdpart/emf/data/Region;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/Region;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/ExtSelectClipRgn;->rgn:Lcom/intsig/office/thirdpart/emf/data/Region;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/Region;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/emf/data/AbstractClipPath;->render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;Lcom/intsig/office/java/awt/Shape;)V

    .line 19
    .line 20
    .line 21
    :cond_1
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

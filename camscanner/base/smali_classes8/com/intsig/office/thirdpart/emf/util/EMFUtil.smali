.class public Lcom/intsig/office/thirdpart/emf/util/EMFUtil;
.super Ljava/lang/Object;
.source "EMFUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static convert(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/FileInputStream;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;

    .line 7
    .line 8
    sget v1, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->DEFAULT_VERSION:I

    .line 9
    .line 10
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readHeader()Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getFrame()Lcom/intsig/office/java/awt/Rectangle;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Rectangle;->getWidth()D

    .line 22
    .line 23
    .line 24
    move-result-wide v1

    .line 25
    double-to-int v1, v1

    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getFrame()Lcom/intsig/office/java/awt/Rectangle;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/Rectangle;->getHeight()D

    .line 31
    .line 32
    .line 33
    move-result-wide v2

    .line 34
    double-to-int v2, v2

    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getDevice()Lcom/intsig/office/java/awt/Dimension;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    iget v3, v3, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getDevice()Lcom/intsig/office/java/awt/Dimension;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    iget v4, v4, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getMillimeters()Lcom/intsig/office/java/awt/Dimension;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 52
    .line 53
    .line 54
    move-result-wide v5

    .line 55
    double-to-int v5, v5

    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getMillimeters()Lcom/intsig/office/java/awt/Dimension;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    invoke-virtual {v6}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 61
    .line 62
    .line 63
    move-result-wide v6

    .line 64
    double-to-int v6, v6

    .line 65
    mul-int v1, v1, v3

    .line 66
    .line 67
    div-int/2addr v1, v5

    .line 68
    const/16 v7, 0x64

    .line 69
    .line 70
    div-int/2addr v1, v7

    .line 71
    add-int/lit8 v1, v1, 0x1

    .line 72
    .line 73
    mul-int v2, v2, v4

    .line 74
    .line 75
    div-int/2addr v2, v6

    .line 76
    div-int/2addr v2, v7

    .line 77
    add-int/lit8 v2, v2, 0x1

    .line 78
    .line 79
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getFrame()Lcom/intsig/office/java/awt/Rectangle;

    .line 80
    .line 81
    .line 82
    move-result-object v8

    .line 83
    iget v8, v8, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;->getFrame()Lcom/intsig/office/java/awt/Rectangle;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 90
    .line 91
    mul-int v8, v8, v3

    .line 92
    .line 93
    div-int/2addr v8, v5

    .line 94
    div-int/2addr v8, v7

    .line 95
    mul-int v0, v0, v4

    .line 96
    .line 97
    div-int/2addr v0, v6

    .line 98
    div-int/2addr v0, v7

    .line 99
    new-instance v3, Lcom/intsig/office/thirdpart/emf/EMFRenderer;

    .line 100
    .line 101
    invoke-direct {v3, p0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 102
    .line 103
    .line 104
    mul-int p0, p2, p3

    .line 105
    .line 106
    mul-int v4, v1, v2

    .line 107
    .line 108
    if-ge p0, v4, :cond_0

    .line 109
    .line 110
    sget-object p0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 111
    .line 112
    invoke-static {p2, p3, p0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 113
    .line 114
    .line 115
    move-result-object p0

    .line 116
    new-instance v4, Landroid/graphics/Canvas;

    .line 117
    .line 118
    invoke-direct {v4, p0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 119
    .line 120
    .line 121
    int-to-float p2, p2

    .line 122
    int-to-float v1, v1

    .line 123
    div-float/2addr p2, v1

    .line 124
    int-to-float p3, p3

    .line 125
    int-to-float v1, v2

    .line 126
    div-float/2addr p3, v1

    .line 127
    invoke-virtual {v4, p2, p3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 128
    .line 129
    .line 130
    goto :goto_0

    .line 131
    :cond_0
    sget-object p0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 132
    .line 133
    invoke-static {v1, v2, p0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 134
    .line 135
    .line 136
    move-result-object p0

    .line 137
    new-instance v4, Landroid/graphics/Canvas;

    .line 138
    .line 139
    invoke-direct {v4, p0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 140
    .line 141
    .line 142
    :goto_0
    neg-int p2, v8

    .line 143
    int-to-float p2, p2

    .line 144
    neg-int p3, v0

    .line 145
    int-to-float p3, p3

    .line 146
    invoke-virtual {v4, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v3, v4}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->paint(Landroid/graphics/Canvas;)V

    .line 150
    .line 151
    .line 152
    new-instance p2, Ljava/io/FileOutputStream;

    .line 153
    .line 154
    invoke-direct {p2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    sget-object p1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 158
    .line 159
    invoke-virtual {p0, p1, v7, p2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 160
    .line 161
    .line 162
    invoke-virtual {p2}, Ljava/io/FileOutputStream;->close()V

    .line 163
    .line 164
    .line 165
    return-object p0
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.class public abstract Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;
.super Ljava/lang/Object;
.source "TTFGlyfTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Glyph"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

.field public xMax:I

.field public xMin:I

.field public yMax:I

.field public yMin:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getBBox()Lcom/intsig/office/java/awt/Rectangle;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMin:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMin:I

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMax:I

    .line 8
    .line 9
    sub-int/2addr v3, v1

    .line 10
    iget v4, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMax:I

    .line 11
    .line 12
    sub-int/2addr v4, v2

    .line 13
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 14
    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getShape()Lcom/intsig/office/java/awt/geom/GeneralPath;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public read()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMin:I

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMin:I

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMax:I

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->this$0:Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable;

    .line 32
    .line 33
    iget-object v0, v0, Lcom/intsig/office/thirdpart/emf/font/TTFTable;->ttf:Lcom/intsig/office/thirdpart/emf/font/TTFInput;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;->readFWord()S

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMax:I

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public toDetailedString()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->toString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "["

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->getType()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, "] ("

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMin:I

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v1, ","

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMin:I

    .line 34
    .line 35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v2, "):("

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->xMax:I

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFGlyfTable$Glyph;->yMax:I

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, ")"

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

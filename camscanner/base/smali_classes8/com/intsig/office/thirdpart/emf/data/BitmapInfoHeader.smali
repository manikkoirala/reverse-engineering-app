.class public Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;
.super Ljava/lang/Object;
.source "BitmapInfoHeader.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field public static final size:I = 0x28


# instance fields
.field private bitCount:I

.field private clrImportant:I

.field private clrUsed:I

.field private compression:I

.field private height:I

.field private planes:I

.field private sizeImage:I

.field private width:I

.field private xPelsPerMeter:I

.field private yPelsPerMeter:I


# direct methods
.method public constructor <init>(IIIIIIIII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->width:I

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->height:I

    const/4 p1, 0x1

    .line 4
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->planes:I

    .line 5
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->bitCount:I

    .line 6
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->compression:I

    .line 7
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->sizeImage:I

    .line 8
    iput p6, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->xPelsPerMeter:I

    .line 9
    iput p7, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->yPelsPerMeter:I

    .line 10
    iput p8, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrUsed:I

    .line 11
    iput p9, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrImportant:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->width:I

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->height:I

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->planes:I

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->bitCount:I

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->compression:I

    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->sizeImage:I

    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->xPelsPerMeter:I

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->yPelsPerMeter:I

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrUsed:I

    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrImportant:I

    return-void
.end method


# virtual methods
.method public getBitCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->bitCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getClrUsed()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrUsed:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCompression()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->compression:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->height:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->width:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "    size: 40\n    width: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->width:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "\n    height: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->height:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, "\n    planes: "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->planes:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, "\n    bitCount: "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->bitCount:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, "\n    compression: "

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->compression:I

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, "\n    sizeImage: "

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->sizeImage:I

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, "\n    xPelsPerMeter: "

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->xPelsPerMeter:I

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v1, "\n    yPelsPerMeter: "

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->yPelsPerMeter:I

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v1, "\n    clrUsed: "

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrUsed:I

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v1, "\n    clrImportant: "

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->clrImportant:I

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    return-object v0
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

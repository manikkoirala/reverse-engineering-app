.class public Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "CreateDIBPatternBrushPt.java"


# instance fields
.field private bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

.field private image:Landroid/graphics/Bitmap;

.field private index:I

.field private usage:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/16 v0, 0x5e

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->image:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->index:I

    .line 11
    .line 12
    const/16 v0, 0x18

    .line 13
    .line 14
    invoke-virtual {p2, v0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 15
    .line 16
    .line 17
    new-instance v1, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 18
    .line 19
    invoke-direct {v1, p2}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 20
    .line 21
    .line 22
    iput-object v1, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    iput v1, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->usage:I

    .line 29
    .line 30
    iget-object v1, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    iget-object v1, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getWidth()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    iget-object v1, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 47
    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    add-int/lit8 p3, p3, -0x4

    .line 57
    .line 58
    sub-int/2addr p3, v0

    .line 59
    add-int/lit8 p3, p3, -0x28

    .line 60
    .line 61
    add-int/lit8 v6, p3, -0x4

    .line 62
    .line 63
    const/4 v7, 0x0

    .line 64
    move-object v5, p2

    .line 65
    invoke-static/range {v2 .. v7}, Lcom/intsig/office/thirdpart/emf/EMFImageLoader;->readImage(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;IILcom/intsig/office/thirdpart/emf/EMFInputStream;ILcom/intsig/office/thirdpart/emf/data/BlendFunction;)Landroid/graphics/Bitmap;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    iput-object p2, p1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->image:Landroid/graphics/Bitmap;

    .line 70
    .line 71
    return-object p1
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->index:I

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt$1;-><init>(Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->storeGDIObject(ILcom/intsig/office/thirdpart/emf/data/GDIObject;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  usage: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->usage:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/CreateDIBPatternBrushPt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/BlendFunction;
.super Ljava/lang/Object;
.source "BlendFunction.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field public static final size:I = 0x4


# instance fields
.field private alphaFormat:I

.field private blendFlags:I

.field private blendOp:I

.field private sourceConstantAlpha:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->blendOp:I

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->blendFlags:I

    .line 4
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->sourceConstantAlpha:I

    .line 5
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->alphaFormat:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->blendOp:I

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->blendFlags:I

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->sourceConstantAlpha:I

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->alphaFormat:I

    return-void
.end method


# virtual methods
.method public getAlphaFormat()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->alphaFormat:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSourceConstantAlpha()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BlendFunction;->sourceConstantAlpha:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "BlendFunction"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

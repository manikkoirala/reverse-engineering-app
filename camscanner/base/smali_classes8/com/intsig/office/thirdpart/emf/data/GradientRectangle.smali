.class public Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;
.super Lcom/intsig/office/thirdpart/emf/data/Gradient;
.source "GradientRectangle.java"


# instance fields
.field private lowerRight:I

.field private upperLeft:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/Gradient;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->upperLeft:I

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->lowerRight:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/Gradient;-><init>()V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readULONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->upperLeft:I

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readULONG()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->lowerRight:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  GradientRectangle: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->upperLeft:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, ", "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/GradientRectangle;->lowerRight:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    return-object v0
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/LogFontW;
.super Ljava/lang/Object;
.source "LogFontW.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;
.implements Lcom/intsig/office/thirdpart/emf/data/GDIObject;


# instance fields
.field private charSet:I

.field private clipPrecision:I

.field private escapement:I

.field private faceFamily:Ljava/lang/String;

.field private font:Lcom/intsig/office/simpletext/font/Font;

.field private height:I

.field private italic:Z

.field private orientation:I

.field private outPrecision:I

.field private pitchAndFamily:I

.field private quality:I

.field private strikeout:Z

.field private underline:Z

.field private weight:I

.field private width:I


# direct methods
.method public constructor <init>(IIIIIZZZIIIIILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->height:I

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->width:I

    .line 4
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->escapement:I

    .line 5
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->orientation:I

    .line 6
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->weight:I

    .line 7
    iput-boolean p6, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->italic:Z

    .line 8
    iput-boolean p7, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->underline:Z

    .line 9
    iput-boolean p8, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->strikeout:Z

    .line 10
    iput p9, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->charSet:I

    .line 11
    iput p10, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->outPrecision:I

    .line 12
    iput p11, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->clipPrecision:I

    .line 13
    iput p12, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->quality:I

    .line 14
    iput p13, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->pitchAndFamily:I

    .line 15
    iput-object p14, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->faceFamily:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/simpletext/font/Font;)V
    .locals 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->getFontSize()D

    move-result-wide v0

    neg-double v0, v0

    double-to-int v0, v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->height:I

    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->width:I

    .line 19
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->escapement:I

    .line 20
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->orientation:I

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->isBold()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x2bc

    goto :goto_0

    :cond_0
    const/16 v1, 0x190

    :goto_0
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->weight:I

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->isItalic()Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->italic:Z

    .line 23
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->underline:Z

    .line 24
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->strikeout:Z

    .line 25
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->charSet:I

    .line 26
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->outPrecision:I

    .line 27
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->clipPrecision:I

    const/4 v1, 0x4

    .line 28
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->quality:I

    .line 29
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->pitchAndFamily:I

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/font/Font;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->faceFamily:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->height:I

    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->width:I

    .line 34
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->escapement:I

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->orientation:I

    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->weight:I

    .line 37
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBOOLEAN()Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->italic:Z

    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBOOLEAN()Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->underline:Z

    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBOOLEAN()Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->strikeout:Z

    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->charSet:I

    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->outPrecision:I

    .line 42
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->clipPrecision:I

    .line 43
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->quality:I

    .line 44
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->pitchAndFamily:I

    const/16 v0, 0x20

    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readWCHAR(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->faceFamily:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getEscapement()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->escapement:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFont()Lcom/intsig/office/simpletext/font/Font;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 2
    .line 3
    if-nez v0, :cond_2

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->italic:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x2

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->weight:I

    .line 13
    .line 14
    const/16 v2, 0x190

    .line 15
    .line 16
    if-le v1, v2, :cond_1

    .line 17
    .line 18
    or-int/lit8 v0, v0, 0x1

    .line 19
    .line 20
    :cond_1
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->height:I

    .line 21
    .line 22
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    new-instance v2, Lcom/intsig/office/simpletext/font/Font;

    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->faceFamily:Ljava/lang/String;

    .line 29
    .line 30
    invoke-direct {v2, v3, v0, v1}, Lcom/intsig/office/simpletext/font/Font;-><init>(Ljava/lang/String;II)V

    .line 31
    .line 32
    .line 33
    iput-object v2, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 34
    .line 35
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->font:Lcom/intsig/office/simpletext/font/Font;

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->setFont(Lcom/intsig/office/simpletext/font/Font;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  LogFontW\n    height: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->height:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "\n    width: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->width:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, "\n    orientation: "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->orientation:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, "\n    weight: "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->weight:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, "\n    italic: "

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->italic:Z

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, "\n    underline: "

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->underline:Z

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, "\n    strikeout: "

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    iget-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->strikeout:Z

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v1, "\n    charSet: "

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->charSet:I

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v1, "\n    outPrecision: "

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->outPrecision:I

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v1, "\n    clipPrecision: "

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->clipPrecision:I

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    const-string v1, "\n    quality: "

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->quality:I

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    const-string v1, "\n    pitchAndFamily: "

    .line 117
    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->pitchAndFamily:I

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v1, "\n    faceFamily: "

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/LogFontW;->faceFamily:Ljava/lang/String;

    .line 132
    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    return-object v0
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

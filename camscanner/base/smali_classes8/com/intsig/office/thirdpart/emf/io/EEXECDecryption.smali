.class public Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;
.super Ljava/io/InputStream;
.source "EEXECDecryption.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/io/EEXECConstants;


# instance fields
.field private c1:I

.field private c2:I

.field private first:Z

.field private in:Ljava/io/InputStream;

.field private n:I

.field private r:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    const v0, 0xd971

    const/4 v1, 0x4

    .line 1
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;-><init>(Ljava/io/InputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->first:Z

    .line 4
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 5
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->r:I

    .line 6
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->n:I

    const p1, 0xce6d

    .line 7
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->c1:I

    const/16 p1, 0x58bf

    .line 8
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->c2:I

    return-void
.end method

.method private decrypt(I)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->r:I

    .line 2
    .line 3
    ushr-int/lit8 v1, v0, 0x8

    .line 4
    .line 5
    xor-int/2addr v1, p1

    .line 6
    rem-int/lit16 v1, v1, 0x100

    .line 7
    .line 8
    add-int/2addr p1, v0

    .line 9
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->c1:I

    .line 10
    .line 11
    mul-int p1, p1, v0

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->c2:I

    .line 14
    .line 15
    add-int/2addr p1, v0

    .line 16
    const/high16 v0, 0x10000

    .line 17
    .line 18
    rem-int/2addr p1, v0

    .line 19
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->r:I

    .line 20
    .line 21
    return v1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->first:Z

    .line 2
    .line 3
    if-eqz v0, :cond_7

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->n:I

    .line 6
    .line 7
    new-array v1, v0, [B

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x0

    .line 11
    const/4 v4, 0x0

    .line 12
    :goto_0
    const/4 v5, 0x1

    .line 13
    if-ge v3, v0, :cond_3

    .line 14
    .line 15
    iget-object v6, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 16
    .line 17
    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    .line 18
    .line 19
    .line 20
    move-result v6

    .line 21
    int-to-byte v7, v6

    .line 22
    aput-byte v7, v1, v3

    .line 23
    .line 24
    int-to-char v7, v6

    .line 25
    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    .line 26
    .line 27
    .line 28
    move-result v7

    .line 29
    if-nez v7, :cond_2

    .line 30
    .line 31
    const/16 v7, 0x61

    .line 32
    .line 33
    if-lt v6, v7, :cond_0

    .line 34
    .line 35
    const/16 v7, 0x66

    .line 36
    .line 37
    if-le v6, v7, :cond_2

    .line 38
    .line 39
    :cond_0
    const/16 v7, 0x41

    .line 40
    .line 41
    if-lt v6, v7, :cond_1

    .line 42
    .line 43
    const/16 v7, 0x46

    .line 44
    .line 45
    if-le v6, v7, :cond_2

    .line 46
    .line 47
    :cond_1
    const/4 v4, 0x1

    .line 48
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_3
    if-eqz v4, :cond_4

    .line 52
    .line 53
    const/4 v3, 0x0

    .line 54
    :goto_1
    if-ge v3, v0, :cond_6

    .line 55
    .line 56
    aget-byte v4, v1, v3

    .line 57
    .line 58
    and-int/lit16 v4, v4, 0xff

    .line 59
    .line 60
    invoke-direct {p0, v4}, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->decrypt(I)I

    .line 61
    .line 62
    .line 63
    add-int/lit8 v3, v3, 0x1

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_4
    new-instance v0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;

    .line 67
    .line 68
    new-instance v3, Ljava/io/ByteArrayInputStream;

    .line 69
    .line 70
    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 71
    .line 72
    .line 73
    invoke-direct {v0, v3, v5}, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 74
    .line 75
    .line 76
    const/4 v1, 0x0

    .line 77
    :goto_2
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    if-ltz v3, :cond_5

    .line 82
    .line 83
    invoke-direct {p0, v3}, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->decrypt(I)I

    .line 84
    .line 85
    .line 86
    add-int/lit8 v1, v1, 0x1

    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_5
    new-instance v0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;

    .line 90
    .line 91
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 92
    .line 93
    invoke-direct {v0, v3, v5}, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 94
    .line 95
    .line 96
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 97
    .line 98
    :goto_3
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->n:I

    .line 99
    .line 100
    if-ge v1, v0, :cond_6

    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 103
    .line 104
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->decrypt(I)I

    .line 109
    .line 110
    .line 111
    add-int/lit8 v1, v1, 0x1

    .line 112
    .line 113
    goto :goto_3

    .line 114
    :cond_6
    iput-boolean v2, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->first:Z

    .line 115
    .line 116
    :cond_7
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->in:Ljava/io/InputStream;

    .line 117
    .line 118
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    const/4 v1, -0x1

    .line 123
    if-ne v0, v1, :cond_8

    .line 124
    .line 125
    return v1

    .line 126
    :cond_8
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/emf/io/EEXECDecryption;->decrypt(I)I

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    return v0
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

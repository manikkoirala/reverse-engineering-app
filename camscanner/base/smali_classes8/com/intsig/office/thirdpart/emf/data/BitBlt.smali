.class public Lcom/intsig/office/thirdpart/emf/data/BitBlt;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "BitBlt.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field private static final size:I = 0x64


# instance fields
.field private bkg:Lcom/intsig/office/java/awt/Color;

.field private bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private dwROP:I

.field private height:I

.field private image:Landroid/graphics/Bitmap;

.field private transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field private usage:I

.field private width:I

.field private x:I

.field private xSrc:I

.field private y:I

.field private ySrc:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x4c

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;IIIILcom/intsig/office/java/awt/geom/AffineTransform;Landroid/graphics/Bitmap;Lcom/intsig/office/java/awt/Color;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/BitBlt;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->x:I

    .line 5
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->y:I

    .line 6
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->width:I

    .line 7
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->height:I

    const p1, 0xcc0020

    .line 8
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->dwROP:I

    const/4 p1, 0x0

    .line 9
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->xSrc:I

    .line 10
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->ySrc:I

    .line 11
    iput-object p6, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 12
    iput-object p8, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 13
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->usage:I

    .line 14
    iput-object p7, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->image:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    .line 15
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/BitBlt;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 7
    .line 8
    .line 9
    move-result-object p3

    .line 10
    iput-object p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 13
    .line 14
    .line 15
    move-result p3

    .line 16
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->x:I

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 19
    .line 20
    .line 21
    move-result p3

    .line 22
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->y:I

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->width:I

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 31
    .line 32
    .line 33
    move-result p3

    .line 34
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->height:I

    .line 35
    .line 36
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 37
    .line 38
    .line 39
    move-result p3

    .line 40
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->dwROP:I

    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 43
    .line 44
    .line 45
    move-result p3

    .line 46
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->xSrc:I

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 49
    .line 50
    .line 51
    move-result p3

    .line 52
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->ySrc:I

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readXFORM()Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 55
    .line 56
    .line 57
    move-result-object p3

    .line 58
    iput-object p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readCOLORREF()Lcom/intsig/office/java/awt/Color;

    .line 61
    .line 62
    .line 63
    move-result-object p3

    .line 64
    iput-object p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 65
    .line 66
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 67
    .line 68
    .line 69
    move-result p3

    .line 70
    iput p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->usage:I

    .line 71
    .line 72
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 76
    .line 77
    .line 78
    move-result p3

    .line 79
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 80
    .line 81
    .line 82
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 83
    .line 84
    .line 85
    move-result v4

    .line 86
    const/4 v0, 0x0

    .line 87
    if-lez p3, :cond_0

    .line 88
    .line 89
    new-instance p3, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 90
    .line 91
    invoke-direct {p3, p2}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 92
    .line 93
    .line 94
    iput-object p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_0
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 98
    .line 99
    :goto_0
    if-lez v4, :cond_1

    .line 100
    .line 101
    iget-object p3, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 102
    .line 103
    if-eqz p3, :cond_1

    .line 104
    .line 105
    invoke-virtual {p3}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    iget v1, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->width:I

    .line 110
    .line 111
    iget v2, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->height:I

    .line 112
    .line 113
    const/4 v5, 0x0

    .line 114
    move-object v3, p2

    .line 115
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/EMFImageLoader;->readImage(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;IILcom/intsig/office/thirdpart/emf/EMFInputStream;ILcom/intsig/office/thirdpart/emf/data/BlendFunction;)Landroid/graphics/Bitmap;

    .line 116
    .line 117
    .line 118
    move-result-object p2

    .line 119
    iput-object p2, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->image:Landroid/graphics/Bitmap;

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_1
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->image:Landroid/graphics/Bitmap;

    .line 123
    .line 124
    :goto_1
    return-object p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->image:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 6
    .line 7
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawImage(Landroid/graphics/Bitmap;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/Rectangle;->isEmpty()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->dwROP:I

    .line 20
    .line 21
    const v1, 0xf00021

    .line 22
    .line 23
    .line 24
    if-ne v0, v1, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->x:I

    .line 29
    .line 30
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->y:I

    .line 33
    .line 34
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillShape(Lcom/intsig/office/java/awt/Shape;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->getFigure()Lcom/intsig/office/java/awt/geom/GeneralPath;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillAndDrawShape(Lcom/intsig/office/java/awt/Shape;)V

    .line 46
    .line 47
    .line 48
    :cond_2
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  bounds: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n  x, y, w, h: "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->x:I

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, " "

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->y:I

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->width:I

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->height:I

    .line 55
    .line 56
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v2, "\n  dwROP: 0x"

    .line 60
    .line 61
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->dwROP:I

    .line 65
    .line 66
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v2, "\n  xSrc, ySrc: "

    .line 74
    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->xSrc:I

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->ySrc:I

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v1, "\n  transform: "

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->transform:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v1, "\n  bkg: "

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v1, "\n  usage: "

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->usage:I

    .line 117
    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    const-string v1, "\n"

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/BitBlt;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 127
    .line 128
    if-eqz v1, :cond_0

    .line 129
    .line 130
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->toString()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    goto :goto_0

    .line 135
    :cond_0
    const-string v1, "  bitmap: null"

    .line 136
    .line 137
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    return-object v0
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

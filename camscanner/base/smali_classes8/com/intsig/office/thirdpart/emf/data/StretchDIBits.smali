.class public Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "StretchDIBits.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field public static final size:I = 0x50


# instance fields
.field private bkg:Lcom/intsig/office/java/awt/Color;

.field private bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private dwROP:I

.field private height:I

.field private heightSrc:I

.field private image:Landroid/graphics/Bitmap;

.field private usage:I

.field private width:I

.field private widthSrc:I

.field private x:I

.field private xSrc:I

.field private y:I

.field private ySrc:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x51

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;IIIILandroid/graphics/Bitmap;Lcom/intsig/office/java/awt/Color;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->x:I

    .line 5
    iput p3, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->y:I

    .line 6
    iput p4, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->width:I

    .line 7
    iput p5, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->height:I

    const/4 p1, 0x0

    .line 8
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->xSrc:I

    .line 9
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->ySrc:I

    .line 10
    invoke-virtual {p6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->widthSrc:I

    .line 11
    invoke-virtual {p6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->heightSrc:I

    .line 12
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->usage:I

    const p1, 0xcc0020

    .line 13
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->dwROP:I

    .line 14
    iput-object p7, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 15
    iput-object p6, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->image:Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    .line 16
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;

    .line 2
    .line 3
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->x:I

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->y:I

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->xSrc:I

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->ySrc:I

    .line 35
    .line 36
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->width:I

    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->height:I

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 52
    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 55
    .line 56
    .line 57
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->usage:I

    .line 65
    .line 66
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->dwROP:I

    .line 71
    .line 72
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->widthSrc:I

    .line 77
    .line 78
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    iput v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->heightSrc:I

    .line 83
    .line 84
    new-instance v0, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 85
    .line 86
    invoke-direct {v0, p2}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->getHeader()Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    iget v2, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->width:I

    .line 96
    .line 97
    iget v3, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->height:I

    .line 98
    .line 99
    add-int/lit8 p3, p3, -0x48

    .line 100
    .line 101
    add-int/lit8 v5, p3, -0x28

    .line 102
    .line 103
    const/4 v6, 0x0

    .line 104
    move-object v4, p2

    .line 105
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/thirdpart/emf/EMFImageLoader;->readImage(Lcom/intsig/office/thirdpart/emf/data/BitmapInfoHeader;IILcom/intsig/office/thirdpart/emf/EMFInputStream;ILcom/intsig/office/thirdpart/emf/data/BlendFunction;)Landroid/graphics/Bitmap;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    iput-object p2, p1, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->image:Landroid/graphics/Bitmap;

    .line 110
    .line 111
    return-object p1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 6

    .line 1
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->image:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v1, :cond_0

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->x:I

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->y:I

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->widthSrc:I

    .line 10
    .line 11
    iget v5, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->heightSrc:I

    .line 12
    .line 13
    move-object v0, p1

    .line 14
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->drawImage(Landroid/graphics/Bitmap;IIII)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  bounds: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n  x, y, w, h: "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->x:I

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, " "

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->y:I

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->width:I

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->height:I

    .line 55
    .line 56
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v2, "\n  xSrc, ySrc, widthSrc, heightSrc: "

    .line 60
    .line 61
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->xSrc:I

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->ySrc:I

    .line 73
    .line 74
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->widthSrc:I

    .line 81
    .line 82
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->heightSrc:I

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v1, "\n  usage: "

    .line 94
    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->usage:I

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    const-string v1, "\n  dwROP: "

    .line 104
    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->dwROP:I

    .line 109
    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    const-string v1, "\n  bkg: "

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bkg:Lcom/intsig/office/java/awt/Color;

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    const-string v1, "\n"

    .line 124
    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/StretchDIBits;->bmi:Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;

    .line 129
    .line 130
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/emf/data/BitmapInfo;->toString()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    return-object v0
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

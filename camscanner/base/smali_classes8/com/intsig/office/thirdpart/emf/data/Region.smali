.class public Lcom/intsig/office/thirdpart/emf/data/Region;
.super Ljava/lang/Object;
.source "Region.java"


# instance fields
.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private region:Lcom/intsig/office/java/awt/Rectangle;


# direct methods
.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 3
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->region:Lcom/intsig/office/java/awt/Rectangle;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->region:Lcom/intsig/office/java/awt/Rectangle;

    const/16 v1, 0x10

    :goto_0
    if-ge v1, v0, :cond_0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    add-int/lit8 v1, v1, 0x10

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public length()I
    .locals 1

    .line 1
    const/16 v0, 0x30

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  Region\n    bounds: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "\n    region: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/Region;->region:Lcom/intsig/office/java/awt/Rectangle;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    return-object v0
.end method

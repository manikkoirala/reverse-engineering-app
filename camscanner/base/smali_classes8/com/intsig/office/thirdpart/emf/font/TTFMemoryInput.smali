.class public Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;
.super Lcom/intsig/office/thirdpart/emf/font/TTFInput;
.source "TTFMemoryInput.java"


# instance fields
.field private data:[B

.field private pointer:I


# direct methods
.method public constructor <init>([B)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFInput;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method getPointer()J
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 2
    .line 3
    int-to-long v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readByte()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 8
    .line 9
    aget-byte v0, v0, v1

    .line 10
    .line 11
    and-int/lit16 v0, v0, 0xff

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readChar()B
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 8
    .line 9
    aget-byte v0, v0, v1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readFully([B)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    array-length v1, p1

    .line 3
    if-ge v0, v1, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 6
    .line 7
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 8
    .line 9
    add-int/lit8 v3, v2, 0x1

    .line 10
    .line 11
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 12
    .line 13
    aget-byte v1, v1, v2

    .line 14
    .line 15
    aput-byte v1, p1, v0

    .line 16
    .line 17
    add-int/lit8 v0, v0, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public readLong()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    aget-byte v1, v0, v1

    .line 8
    .line 9
    shl-int/lit8 v1, v1, 0x18

    .line 10
    .line 11
    add-int/lit8 v3, v2, 0x1

    .line 12
    .line 13
    aget-byte v2, v0, v2

    .line 14
    .line 15
    shl-int/lit8 v2, v2, 0x10

    .line 16
    .line 17
    or-int/2addr v1, v2

    .line 18
    add-int/lit8 v2, v3, 0x1

    .line 19
    .line 20
    aget-byte v3, v0, v3

    .line 21
    .line 22
    shl-int/lit8 v3, v3, 0x8

    .line 23
    .line 24
    or-int/2addr v1, v3

    .line 25
    add-int/lit8 v3, v2, 0x1

    .line 26
    .line 27
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 28
    .line 29
    aget-byte v0, v0, v2

    .line 30
    .line 31
    or-int/2addr v0, v1

    .line 32
    int-to-short v0, v0

    .line 33
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readRawByte()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    iput v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 8
    .line 9
    aget-byte v0, v0, v1

    .line 10
    .line 11
    and-int/lit16 v0, v0, 0xff

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readShort()S
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    aget-byte v1, v0, v1

    .line 8
    .line 9
    shl-int/lit8 v1, v1, 0x8

    .line 10
    .line 11
    add-int/lit8 v3, v2, 0x1

    .line 12
    .line 13
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 14
    .line 15
    aget-byte v0, v0, v2

    .line 16
    .line 17
    or-int/2addr v0, v1

    .line 18
    int-to-short v0, v0

    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readULong()J
    .locals 7

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    invoke-virtual {p0, v1}, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->readFully([B)V

    .line 5
    .line 6
    .line 7
    const-wide/16 v2, 0x0

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    :goto_0
    if-ge v4, v0, :cond_0

    .line 11
    .line 12
    rsub-int/lit8 v5, v4, 0x3

    .line 13
    .line 14
    aget-byte v5, v1, v5

    .line 15
    .line 16
    and-int/lit16 v5, v5, 0xff

    .line 17
    .line 18
    mul-int/lit8 v6, v4, 0x8

    .line 19
    .line 20
    shl-int/2addr v5, v6

    .line 21
    int-to-long v5, v5

    .line 22
    or-long/2addr v2, v5

    .line 23
    add-int/lit8 v4, v4, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    return-wide v2
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readUShort()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->data:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    aget-byte v1, v0, v1

    .line 8
    .line 9
    shl-int/lit8 v1, v1, 0x8

    .line 10
    .line 11
    add-int/lit8 v3, v2, 0x1

    .line 12
    .line 13
    iput v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 14
    .line 15
    aget-byte v0, v0, v2

    .line 16
    .line 17
    or-int/2addr v0, v1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public seek(J)V
    .locals 0

    .line 1
    long-to-int p2, p1

    .line 2
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFMemoryInput;->pointer:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

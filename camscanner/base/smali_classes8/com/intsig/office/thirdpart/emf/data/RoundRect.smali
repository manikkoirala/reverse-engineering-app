.class public Lcom/intsig/office/thirdpart/emf/data/RoundRect;
.super Lcom/intsig/office/thirdpart/emf/EMFTag;
.source "RoundRect.java"


# instance fields
.field private bounds:Lcom/intsig/office/java/awt/Rectangle;

.field private corner:Lcom/intsig/office/java/awt/Dimension;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x2c

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/thirdpart/emf/EMFTag;-><init>(II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/java/awt/Dimension;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/data/RoundRect;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    iput-object p2, p0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->corner:Lcom/intsig/office/java/awt/Dimension;

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/thirdpart/emf/data/RoundRect;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 4
    .line 5
    .line 6
    move-result-object p3

    .line 7
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readSIZEL()Lcom/intsig/office/java/awt/Dimension;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    invoke-direct {p1, p3, p2}, Lcom/intsig/office/thirdpart/emf/data/RoundRect;-><init>(Lcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/java/awt/Dimension;)V

    .line 12
    .line 13
    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public render(Lcom/intsig/office/thirdpart/emf/EMFRenderer;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v14, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;

    .line 4
    .line 5
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    iget v2, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 8
    .line 9
    int-to-double v3, v2

    .line 10
    int-to-double v5, v2

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Rectangle;->getWidth()D

    .line 12
    .line 13
    .line 14
    move-result-wide v7

    .line 15
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Rectangle;->getHeight()D

    .line 18
    .line 19
    .line 20
    move-result-wide v9

    .line 21
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->corner:Lcom/intsig/office/java/awt/Dimension;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getWidth()D

    .line 24
    .line 25
    .line 26
    move-result-wide v11

    .line 27
    iget-object v1, v0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->corner:Lcom/intsig/office/java/awt/Dimension;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/Dimension;->getHeight()D

    .line 30
    .line 31
    .line 32
    move-result-wide v15

    .line 33
    move-object v1, v14

    .line 34
    move-wide v2, v3

    .line 35
    move-wide v4, v5

    .line 36
    move-wide v6, v7

    .line 37
    move-wide v8, v9

    .line 38
    move-wide v10, v11

    .line 39
    move-wide v12, v15

    .line 40
    invoke-direct/range {v1 .. v13}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;-><init>(DDDDDD)V

    .line 41
    .line 42
    .line 43
    move-object/from16 v1, p1

    .line 44
    .line 45
    invoke-virtual {v1, v14}, Lcom/intsig/office/thirdpart/emf/EMFRenderer;->fillAndDrawOrAppend(Lcom/intsig/office/java/awt/Shape;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/EMFTag;->toString()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v1, "\n  bounds: "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->bounds:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "\n  corner: "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/data/RoundRect;->corner:Lcom/intsig/office/java/awt/Dimension;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;
.super Lcom/intsig/office/thirdpart/emf/data/AbstractPolyPolyline;
.source "PolyPolyline.java"


# instance fields
.field private end:I

.field private start:I


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolyPolyline;-><init>(IILcom/intsig/office/java/awt/Rectangle;[I[[Landroid/graphics/Point;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;II[I[[Landroid/graphics/Point;)V
    .locals 6

    const/4 v1, 0x7

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p4

    move-object v5, p5

    .line 2
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/data/AbstractPolyPolyline;-><init>(IILcom/intsig/office/java/awt/Rectangle;[I[[Landroid/graphics/Point;)V

    .line 3
    iput p2, p0, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;->start:I

    .line 4
    array-length p1, p4

    add-int/lit8 p1, p1, -0x1

    invoke-static {p3, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;->end:I

    return-void
.end method


# virtual methods
.method public read(ILcom/intsig/office/thirdpart/emf/EMFInputStream;I)Lcom/intsig/office/thirdpart/emf/EMFTag;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readRECTL()Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    .line 4
    move-result-object v1

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 10
    .line 11
    .line 12
    new-array v4, p1, [I

    .line 13
    .line 14
    new-array v5, p1, [[Landroid/graphics/Point;

    .line 15
    .line 16
    const/4 p3, 0x0

    .line 17
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-ge v0, p1, :cond_0

    .line 19
    .line 20
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    aput v2, v4, v0

    .line 25
    .line 26
    new-array v2, v2, [Landroid/graphics/Point;

    .line 27
    .line 28
    aput-object v2, v5, v0

    .line 29
    .line 30
    add-int/lit8 v0, v0, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    :goto_1
    if-ge p3, p1, :cond_1

    .line 34
    .line 35
    aget v0, v4, p3

    .line 36
    .line 37
    invoke-virtual {p2, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readPOINTL(I)[Landroid/graphics/Point;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    aput-object v0, v5, p3

    .line 42
    .line 43
    add-int/lit8 p3, p3, 0x1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    new-instance p2, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;

    .line 47
    .line 48
    const/4 v2, 0x0

    .line 49
    add-int/lit8 v3, p1, -0x1

    .line 50
    .line 51
    move-object v0, p2

    .line 52
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/thirdpart/emf/data/PolyPolyline;-><init>(Lcom/intsig/office/java/awt/Rectangle;II[I[[Landroid/graphics/Point;)V

    .line 53
    .line 54
    .line 55
    return-object p2
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public Lcom/intsig/office/thirdpart/emf/font/TTFFile;
.super Lcom/intsig/office/thirdpart/emf/font/TTFFont;
.source "TTFFile.java"


# static fields
.field private static final mode:Ljava/lang/String; = "r"


# instance fields
.field private entrySelector:I

.field private fileName:Ljava/lang/String;

.field private numberOfTables:I

.field private rangeShift:I

.field private searchRange:I

.field private sfntMajorVersion:I

.field private sfntMinorVersion:I

.field private ttf:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFFont;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->fileName:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v0, Ljava/io/RandomAccessFile;

    .line 7
    .line 8
    const-string v1, "r"

    .line 9
    .line 10
    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 14
    .line 15
    const-wide/16 v1, 0x0

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->sfntMajorVersion:I

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 29
    .line 30
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->sfntMinorVersion:I

    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->numberOfTables:I

    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 45
    .line 46
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->searchRange:I

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 53
    .line 54
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->entrySelector:I

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 61
    .line 62
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->readUnsignedShort()I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->rangeShift:I

    .line 67
    .line 68
    const/4 p1, 0x0

    .line 69
    :goto_0
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->numberOfTables:I

    .line 70
    .line 71
    if-ge p1, v0, :cond_0

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 74
    .line 75
    mul-int/lit8 v1, p1, 0x10

    .line 76
    .line 77
    add-int/lit8 v1, v1, 0xc

    .line 78
    .line 79
    int-to-long v1, v1

    .line 80
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 81
    .line 82
    .line 83
    const/4 v0, 0x4

    .line 84
    new-array v0, v0, [B

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 87
    .line 88
    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 89
    .line 90
    .line 91
    new-instance v1, Ljava/lang/String;

    .line 92
    .line 93
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 97
    .line 98
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->readInt()I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 103
    .line 104
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readInt()I

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    iget-object v3, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 109
    .line 110
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->readInt()I

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    new-instance v12, Lcom/intsig/office/thirdpart/emf/font/TTFFileInput;

    .line 115
    .line 116
    iget-object v5, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 117
    .line 118
    int-to-long v6, v2

    .line 119
    int-to-long v8, v3

    .line 120
    int-to-long v10, v0

    .line 121
    move-object v4, v12

    .line 122
    invoke-direct/range {v4 .. v11}, Lcom/intsig/office/thirdpart/emf/font/TTFFileInput;-><init>(Ljava/io/RandomAccessFile;JJJ)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p0, v1, v12}, Lcom/intsig/office/thirdpart/emf/font/TTFFont;->newTable(Ljava/lang/String;Lcom/intsig/office/thirdpart/emf/font/TTFInput;)V

    .line 126
    .line 127
    .line 128
    add-int/lit8 p1, p1, 0x1

    .line 129
    .line 130
    goto :goto_0

    .line 131
    :cond_0
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFFont;->close()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->ttf:Ljava/io/RandomAccessFile;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->sfntMajorVersion:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public show()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/office/thirdpart/emf/font/TTFFont;->show()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "Font: "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->fileName:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "  sfnt: "

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->sfntMajorVersion:I

    .line 41
    .line 42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v2, "."

    .line 46
    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->sfntMinorVersion:I

    .line 51
    .line 52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 63
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v2, "  numTables: "

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->numberOfTables:I

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 87
    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v2, "  searchRange: "

    .line 94
    .line 95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->searchRange:I

    .line 99
    .line 100
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 111
    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    const-string v2, "  entrySelector: "

    .line 118
    .line 119
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->entrySelector:I

    .line 123
    .line 124
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 135
    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    .line 137
    .line 138
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .line 140
    .line 141
    const-string v2, "  rangeShift: "

    .line 142
    .line 143
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    iget v2, p0, Lcom/intsig/office/thirdpart/emf/font/TTFFile;->rangeShift:I

    .line 147
    .line 148
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    return-void
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public interface abstract Lcom/intsig/office/thirdpart/emf/EMFConstants;
.super Ljava/lang/Object;
.source "EMFConstants.java"


# static fields
.field public static final AC_SRC_ALPHA:I = 0x1

.field public static final AC_SRC_OVER:I = 0x0

.field public static final AD_CLOCKWISE:I = 0x2

.field public static final AD_COUNTERCLOCKWISE:I = 0x1

.field public static final ALTERNATE:I = 0x1

.field public static final BI_BITFIELDS:I = 0x3

.field public static final BI_JPEG:I = 0x4

.field public static final BI_PNG:I = 0x5

.field public static final BI_RGB:I = 0x0

.field public static final BI_RLE4:I = 0x2

.field public static final BI_RLE8:I = 0x1

.field public static final BKG_OPAQUE:I = 0x2

.field public static final BKG_TRANSPARENT:I = 0x1

.field public static final BLACKONWHITE:I = 0x1

.field public static final BS_DIBPATTERN:I = 0x5

.field public static final BS_DIBPATTERN8X8:I = 0x8

.field public static final BS_DIBPATTERNPT:I = 0x6

.field public static final BS_HATCHED:I = 0x2

.field public static final BS_HOLLOW:I = 0x1

.field public static final BS_INDEXED:I = 0x4

.field public static final BS_MONOPATTERN:I = 0x9

.field public static final BS_NULL:I = 0x1

.field public static final BS_PATTERN:I = 0x3

.field public static final BS_PATTERN8X8:I = 0x7

.field public static final BS_SOLID:I = 0x0

.field public static final COLORONCOLOR:I = 0x3

.field public static final DIB_PAL_COLORS:I = 0x1

.field public static final DIB_RGB_COLORS:I = 0x0

.field public static final ETO_CLIPPED:I = 0x4

.field public static final ETO_GLYPH_INDEX:I = 0x10

.field public static final ETO_IGNORELANGUAGE:I = 0x1000

.field public static final ETO_NUMERICSLATIN:I = 0x800

.field public static final ETO_NUMERICSLOCAL:I = 0x400

.field public static final ETO_OPAQUE:I = 0x2

.field public static final ETO_PDY:I = 0x2000

.field public static final ETO_RTLREADING:I = 0x80

.field public static final FLOODFILLBORDER:I = 0x0

.field public static final FLOODFILLSURFACE:I = 0x1

.field public static final FW_BOLD:I = 0x2bc

.field public static final FW_DONTCARE:I = 0x0

.field public static final FW_EXTRABOLD:I = 0x320

.field public static final FW_EXTRALIGHT:I = 0xc8

.field public static final FW_HEAVY:I = 0x384

.field public static final FW_LIGHT:I = 0x12c

.field public static final FW_MEDIUM:I = 0x1f4

.field public static final FW_NORMAL:I = 0x190

.field public static final FW_SEMIBOLD:I = 0x258

.field public static final FW_THIN:I = 0x64

.field public static final GDICOMMENT_BEGINGROUP:I = 0x2

.field public static final GDICOMMENT_ENDGROUP:I = 0x3

.field public static final GDICOMMENT_IDENTIFIER:I = 0x43494447

.field public static final GDICOMMENT_MULTIFORMATS:I = 0x40000004

.field public static final GDICOMMENT_UNICODE_END:I = 0x80

.field public static final GDICOMMENT_UNICODE_STRING:I = 0x40

.field public static final GDICOMMENT_WINDOWS_METAFILE:I = -0x7fffffff

.field public static final GM_ADVANCED:I = 0x2

.field public static final GM_COMPATIBLE:I = 0x1

.field public static final GRADIENT_FILL_RECT_H:I = 0x0

.field public static final GRADIENT_FILL_RECT_V:I = 0x1

.field public static final GRADIENT_FILL_TRIANGLE:I = 0x2

.field public static final HALFTONE:I = 0x4

.field public static final HS_BDIAGONAL:I = 0x3

.field public static final HS_CROSS:I = 0x4

.field public static final HS_DIAGCROSS:I = 0x5

.field public static final HS_FDIAGONAL:I = 0x2

.field public static final HS_HORIZONTAL:I = 0x0

.field public static final HS_VERTICAL:I = 0x1

.field public static final ICM_DONE_OUTSIDEDC:I = 0x4

.field public static final ICM_OFF:I = 0x1

.field public static final ICM_ON:I = 0x2

.field public static final ICM_QUERY:I = 0x3

.field public static final MM_ANISOTROPIC:I = 0x8

.field public static final MM_HIENGLISH:I = 0x5

.field public static final MM_HIMETRIC:I = 0x3

.field public static final MM_ISOTROPIC:I = 0x7

.field public static final MM_LOENGLISH:I = 0x4

.field public static final MM_LOMETRIC:I = 0x2

.field public static final MM_TEXT:I = 0x1

.field public static final MM_TWIPS:I = 0x6

.field public static final MWT_IDENTITY:I = 0x1

.field public static final MWT_LEFTMULTIPLY:I = 0x2

.field public static final MWT_RIGHTMULTIPLY:I = 0x3

.field public static final PAN_ANY:I = 0x0

.field public static final PAN_NO_FIT:I = 0x1

.field public static final PS_COSMETIC:I = 0x0

.field public static final PS_DASH:I = 0x1

.field public static final PS_DASHDOT:I = 0x3

.field public static final PS_DASHDOTDOT:I = 0x4

.field public static final PS_DOT:I = 0x2

.field public static final PS_ENDCAP_FLAT:I = 0x200

.field public static final PS_ENDCAP_ROUND:I = 0x0

.field public static final PS_ENDCAP_SQUARE:I = 0x100

.field public static final PS_GEOMETRIC:I = 0x10000

.field public static final PS_INSIDEFRAME:I = 0x6

.field public static final PS_JOIN_BEVEL:I = 0x1000

.field public static final PS_JOIN_MITER:I = 0x2000

.field public static final PS_JOIN_ROUND:I = 0x0

.field public static final PS_NULL:I = 0x5

.field public static final PS_SOLID:I = 0x0

.field public static final PS_USERSTYLE:I = 0x7

.field public static final PT_BEZIERTO:I = 0x4

.field public static final PT_CLOSEFIGURE:I = 0x1

.field public static final PT_LINETO:I = 0x2

.field public static final PT_MOVETO:I = 0x6

.field public static final R2_BLACK:I = 0x1

.field public static final R2_COPYPEN:I = 0xd

.field public static final R2_MASKNOTPEN:I = 0x3

.field public static final R2_MASKPEN:I = 0x9

.field public static final R2_MASKPENNOT:I = 0x5

.field public static final R2_MERGENOTPEN:I = 0xc

.field public static final R2_MERGEPEN:I = 0xf

.field public static final R2_MERGEPENNOT:I = 0xe

.field public static final R2_NOP:I = 0xb

.field public static final R2_NOT:I = 0x6

.field public static final R2_NOTCOPYPEN:I = 0x4

.field public static final R2_NOTMASKPEN:I = 0x8

.field public static final R2_NOTMERGEPEN:I = 0x2

.field public static final R2_NOTXORPEN:I = 0xa

.field public static final R2_WHITE:I = 0x10

.field public static final R2_XORPEN:I = 0x7

.field public static final RGN_AND:I = 0x1

.field public static final RGN_COPY:I = 0x5

.field public static final RGN_DIFF:I = 0x4

.field public static final RGN_MAX:I = 0x5

.field public static final RGN_MIN:I = 0x1

.field public static final RGN_OR:I = 0x2

.field public static final RGN_XOR:I = 0x3

.field public static final SRCCOPY:I = 0xcc0020

.field public static final STRETCH_ANDSCANS:I = 0x1

.field public static final STRETCH_DELETESCANS:I = 0x3

.field public static final STRETCH_HALFTONE:I = 0x4

.field public static final STRETCH_ORSCANS:I = 0x2

.field public static final TA_BASELINE:I = 0x18

.field public static final TA_BOTTOM:I = 0x8

.field public static final TA_CENTER:I = 0x6

.field public static final TA_LEFT:I = 0x0

.field public static final TA_NOUPDATECP:I = 0x0

.field public static final TA_RIGHT:I = 0x2

.field public static final TA_RTLREADING:I = 0x100

.field public static final TA_TOP:I = 0x0

.field public static final TA_UPDATECP:I = 0x1

.field public static final TWIPS:I = 0x14

.field public static final UNITS_PER_PIXEL:I = 0x1

.field public static final WHITEONBLACK:I = 0x2

.field public static final WINDING:I = 0x2

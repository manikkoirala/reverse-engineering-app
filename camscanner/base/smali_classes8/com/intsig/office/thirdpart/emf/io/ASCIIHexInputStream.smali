.class public Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;
.super Lcom/intsig/office/thirdpart/emf/io/DecodingInputStream;
.source "ASCIIHexInputStream.java"


# instance fields
.field private endReached:Z

.field private ignoreIllegalChars:Z

.field private in:Ljava/io/InputStream;

.field private lineNo:I

.field private prev:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/io/DecodingInputStream;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->in:Ljava/io/InputStream;

    .line 4
    iput-boolean p2, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->ignoreIllegalChars:Z

    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->endReached:Z

    const/4 p1, -0x1

    .line 6
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    const/4 p1, 0x1

    .line 7
    iput p1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    return-void
.end method

.method private readPart()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/intsig/office/thirdpart/emf/io/EncodingException;
        }
    .end annotation

    .line 1
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->in:Ljava/io/InputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    const/4 v2, 0x1

    .line 9
    if-eq v0, v1, :cond_6

    .line 10
    .line 11
    if-eqz v0, :cond_5

    .line 12
    .line 13
    const/16 v3, 0x9

    .line 14
    .line 15
    if-eq v0, v3, :cond_5

    .line 16
    .line 17
    const/16 v4, 0xa

    .line 18
    .line 19
    const/16 v5, 0xd

    .line 20
    .line 21
    if-eq v0, v4, :cond_3

    .line 22
    .line 23
    const/16 v6, 0xc

    .line 24
    .line 25
    if-eq v0, v6, :cond_5

    .line 26
    .line 27
    if-eq v0, v5, :cond_2

    .line 28
    .line 29
    const/16 v7, 0x20

    .line 30
    .line 31
    if-eq v0, v7, :cond_5

    .line 32
    .line 33
    const/16 v7, 0x3e

    .line 34
    .line 35
    if-eq v0, v7, :cond_1

    .line 36
    .line 37
    packed-switch v0, :pswitch_data_0

    .line 38
    .line 39
    .line 40
    packed-switch v0, :pswitch_data_1

    .line 41
    .line 42
    .line 43
    packed-switch v0, :pswitch_data_2

    .line 44
    .line 45
    .line 46
    iget-boolean v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->ignoreIllegalChars:Z

    .line 47
    .line 48
    if-eqz v1, :cond_0

    .line 49
    .line 50
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    new-instance v1, Lcom/intsig/office/thirdpart/emf/io/EncodingException;

    .line 54
    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v3, "Illegal char "

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    const-string v0, " in HexStream"

    .line 69
    .line 70
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-direct {v1, v0}, Lcom/intsig/office/thirdpart/emf/io/EncodingException;-><init>(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    throw v1

    .line 81
    :pswitch_0
    const/16 v0, 0xf

    .line 82
    .line 83
    return v0

    .line 84
    :pswitch_1
    const/16 v0, 0xe

    .line 85
    .line 86
    return v0

    .line 87
    :pswitch_2
    return v5

    .line 88
    :pswitch_3
    return v6

    .line 89
    :pswitch_4
    const/16 v0, 0xb

    .line 90
    .line 91
    return v0

    .line 92
    :pswitch_5
    return v4

    .line 93
    :pswitch_6
    return v3

    .line 94
    :pswitch_7
    const/16 v0, 0x8

    .line 95
    .line 96
    return v0

    .line 97
    :pswitch_8
    const/4 v0, 0x7

    .line 98
    return v0

    .line 99
    :pswitch_9
    const/4 v0, 0x6

    .line 100
    return v0

    .line 101
    :pswitch_a
    const/4 v0, 0x5

    .line 102
    return v0

    .line 103
    :pswitch_b
    const/4 v0, 0x4

    .line 104
    return v0

    .line 105
    :pswitch_c
    const/4 v0, 0x3

    .line 106
    return v0

    .line 107
    :pswitch_d
    const/4 v0, 0x2

    .line 108
    return v0

    .line 109
    :pswitch_e
    return v2

    .line 110
    :pswitch_f
    const/4 v0, 0x0

    .line 111
    return v0

    .line 112
    :cond_1
    iput-boolean v2, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->endReached:Z

    .line 113
    .line 114
    return v1

    .line 115
    :cond_2
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    .line 116
    .line 117
    add-int/2addr v1, v2

    .line 118
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    .line 119
    .line 120
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_3
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    .line 124
    .line 125
    if-eq v1, v5, :cond_4

    .line 126
    .line 127
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    .line 128
    .line 129
    add-int/2addr v1, v2

    .line 130
    iput v1, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    .line 131
    .line 132
    :cond_4
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    .line 133
    .line 134
    goto/16 :goto_0

    .line 135
    .line 136
    :cond_5
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->prev:I

    .line 137
    .line 138
    goto/16 :goto_0

    .line 139
    .line 140
    :cond_6
    iput-boolean v2, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->endReached:Z

    .line 141
    .line 142
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->ignoreIllegalChars:Z

    .line 143
    .line 144
    if-eqz v0, :cond_7

    .line 145
    .line 146
    return v1

    .line 147
    :cond_7
    new-instance v0, Lcom/intsig/office/thirdpart/emf/io/EncodingException;

    .line 148
    .line 149
    const-string v1, "missing \'>\' at end of ASCII HEX stream"

    .line 150
    .line 151
    invoke-direct {v0, v1}, Lcom/intsig/office/thirdpart/emf/io/EncodingException;-><init>(Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    throw v0

    .line 155
    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    :pswitch_data_1
    .packed-switch 0x41
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x61
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getLineNo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->lineNo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->endReached:Z

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->readPart()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    return v1

    .line 14
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/emf/io/ASCIIHexInputStream;->readPart()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-ne v2, v1, :cond_2

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    :cond_2
    shl-int/lit8 v0, v0, 0x4

    .line 22
    .line 23
    or-int/2addr v0, v2

    .line 24
    and-int/lit16 v0, v0, 0xff

    .line 25
    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
.end method

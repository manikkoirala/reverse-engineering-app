.class public Lcom/intsig/office/thirdpart/emf/data/Panose;
.super Ljava/lang/Object;
.source "Panose.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# instance fields
.field private armStyle:I

.field private contrast:I

.field private familyType:I

.field private letterForm:I

.field private midLine:I

.field private proportion:I

.field private serifStyle:I

.field private strokeVariation:I

.field private weight:I

.field private xHeight:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->familyType:I

    .line 3
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->serifStyle:I

    .line 4
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->proportion:I

    .line 5
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->weight:I

    .line 6
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->contrast:I

    .line 7
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->strokeVariation:I

    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->armStyle:I

    .line 9
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->letterForm:I

    .line 10
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->midLine:I

    .line 11
    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->xHeight:I

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->familyType:I

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->serifStyle:I

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->proportion:I

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->weight:I

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->contrast:I

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->strokeVariation:I

    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->armStyle:I

    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->letterForm:I

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->midLine:I

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->xHeight:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  Panose\n    familytype: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->familyType:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "\n    serifStyle: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->serifStyle:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, "\n    weight: "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->weight:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, "\n    proportion: "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->proportion:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, "\n    contrast: "

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->contrast:I

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, "\n    strokeVariation: "

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->strokeVariation:I

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, "\n    armStyle: "

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->armStyle:I

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v1, "\n    letterForm: "

    .line 77
    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->letterForm:I

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v1, "\n    midLine: "

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->midLine:I

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v1, "\n    xHeight: "

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    iget v1, p0, Lcom/intsig/office/thirdpart/emf/data/Panose;->xHeight:I

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    return-object v0
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

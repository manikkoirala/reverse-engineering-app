.class public Lcom/intsig/office/thirdpart/emf/EMFInputStream;
.super Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;
.source "EMFInputStream.java"

# interfaces
.implements Lcom/intsig/office/thirdpart/emf/EMFConstants;


# static fields
.field public static DEFAULT_VERSION:I = 0x1


# instance fields
.field private header:Lcom/intsig/office/thirdpart/emf/EMFHeader;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->DEFAULT_VERSION:I

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/thirdpart/emf/EMFTagSet;

    invoke-direct {v0, p2}, Lcom/intsig/office/thirdpart/emf/EMFTagSet;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/EMFTagSet;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/EMFTagSet;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/intsig/office/thirdpart/emf/io/TaggedInputStream;-><init>(Ljava/io/InputStream;Lcom/intsig/office/thirdpart/emf/io/TagSet;Lcom/intsig/office/thirdpart/emf/io/ActionSet;Z)V

    return-void
.end method


# virtual methods
.method public getVersion()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->DEFAULT_VERSION:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected readActionHeader()Lcom/intsig/office/thirdpart/emf/io/ActionHeader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readBOOLEAN()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readBYTE()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte()B

    move-result v0

    return v0
.end method

.method public readBYTE(I)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    new-array v0, p1, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readBYTE()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readCOLOR16()Lcom/intsig/office/java/awt/Color;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    shr-int/lit8 v1, v1, 0x8

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    shr-int/lit8 v2, v2, 0x8

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    shr-int/lit8 v3, v3, 0x8

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    shr-int/lit8 v4, v4, 0x8

    .line 26
    .line 27
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    .line 28
    .line 29
    .line 30
    return-object v0
.end method

.method public readCOLORREF()Lcom/intsig/office/java/awt/Color;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte()B

    .line 19
    .line 20
    .line 21
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readDWORD()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedInt()J

    move-result-wide v0

    long-to-int v1, v0

    return v1
.end method

.method public readDWORD(I)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    new-array v0, p1, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readFLOAT()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readFloat()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readHeader()Lcom/intsig/office/thirdpart/emf/EMFHeader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/office/thirdpart/emf/EMFHeader;-><init>(Lcom/intsig/office/thirdpart/emf/EMFInputStream;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->header:Lcom/intsig/office/thirdpart/emf/EMFHeader;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readLONG()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readInt()I

    move-result v0

    return v0
.end method

.method public readLONG(I)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    new-array v0, p1, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readPOINTL()Landroid/graphics/Point;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    move-result v1

    .line 3
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method public readPOINTL(I)[Landroid/graphics/Point;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    new-array v0, p1, [Landroid/graphics/Point;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readPOINTL()Landroid/graphics/Point;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readPOINTS()Landroid/graphics/Point;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    move-result v0

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readShort()S

    move-result v1

    .line 3
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method public readPOINTS(I)[Landroid/graphics/Point;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    new-array v0, p1, [Landroid/graphics/Point;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readPOINTS()Landroid/graphics/Point;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readRECTL()Lcom/intsig/office/java/awt/Rectangle;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    sub-int/2addr v2, v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    sub-int/2addr v3, v1

    .line 19
    new-instance v4, Lcom/intsig/office/java/awt/Rectangle;

    .line 20
    .line 21
    invoke-direct {v4, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 22
    .line 23
    .line 24
    return-object v4
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readSIZEL()Lcom/intsig/office/java/awt/Dimension;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Dimension;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readLONG()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/Dimension;-><init>(II)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected readTagHeader()Lcom/intsig/office/thirdpart/emf/io/TagHeader;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteCountInputStream;->read()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    return-object v0

    .line 10
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    shl-int/lit8 v1, v1, 0x8

    .line 15
    .line 16
    or-int/2addr v0, v1

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    shl-int/lit8 v1, v1, 0x10

    .line 22
    .line 23
    or-int/2addr v0, v1

    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedByte()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    shl-int/lit8 v1, v1, 0x18

    .line 29
    .line 30
    or-int/2addr v0, v1

    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readDWORD()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    int-to-long v1, v1

    .line 36
    new-instance v3, Lcom/intsig/office/thirdpart/emf/io/TagHeader;

    .line 37
    .line 38
    const-wide/16 v4, 0x8

    .line 39
    .line 40
    sub-long/2addr v1, v4

    .line 41
    invoke-direct {v3, v0, v1, v2}, Lcom/intsig/office/thirdpart/emf/io/TagHeader;-><init>(IJ)V

    .line 42
    .line 43
    .line 44
    return-object v3
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readUINT()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedInt()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    long-to-int v1, v0

    .line 6
    return v1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readULONG()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedInt()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    long-to-int v1, v0

    .line 6
    return v1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readWCHAR(I)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    mul-int/lit8 p1, p1, 0x2

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readByte(I)[B

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_0
    if-ge v2, p1, :cond_1

    .line 10
    .line 11
    aget-byte v3, v0, v2

    .line 12
    .line 13
    if-nez v3, :cond_0

    .line 14
    .line 15
    add-int/lit8 v3, v2, 0x1

    .line 16
    .line 17
    aget-byte v3, v0, v3

    .line 18
    .line 19
    if-nez v3, :cond_0

    .line 20
    .line 21
    move p1, v2

    .line 22
    goto :goto_1

    .line 23
    :cond_0
    add-int/lit8 v2, v2, 0x2

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/String;

    .line 27
    .line 28
    const-string v3, "UTF-16LE"

    .line 29
    .line 30
    invoke-direct {v2, v0, v1, p1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-object v2
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public readWORD()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/io/ByteOrderInputStream;->readUnsignedShort()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readXFORM()Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v7, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 20
    .line 21
    .line 22
    move-result v5

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/emf/EMFInputStream;->readFLOAT()F

    .line 24
    .line 25
    .line 26
    move-result v6

    .line 27
    move-object v0, v7

    .line 28
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(FFFFFF)V

    .line 29
    .line 30
    .line 31
    return-object v7
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

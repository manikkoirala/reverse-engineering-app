.class public Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;
.super Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;
.source "EUCKRStatistics.java"


# static fields
.field static mFirstByteFreq:[F

.field static mFirstByteMean:F

.field static mFirstByteStdDev:F

.field static mFirstByteWeight:F

.field static mSecondByteFreq:[F

.field static mSecondByteMean:F

.field static mSecondByteStdDev:F

.field static mSecondByteWeight:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x5e

    .line 5
    .line 6
    new-array v1, v0, [F

    .line 7
    .line 8
    fill-array-data v1, :array_0

    .line 9
    .line 10
    .line 11
    sput-object v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteFreq:[F

    .line 12
    .line 13
    const v1, 0x3cd1a869    # 0.025593f

    .line 14
    .line 15
    .line 16
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteStdDev:F

    .line 17
    .line 18
    const v1, 0x3c2e4b02    # 0.010638f

    .line 19
    .line 20
    .line 21
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteMean:F

    .line 22
    .line 23
    const v2, 0x3f25be6e

    .line 24
    .line 25
    .line 26
    sput v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteWeight:F

    .line 27
    .line 28
    new-array v0, v0, [F

    .line 29
    .line 30
    fill-array-data v0, :array_1

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteFreq:[F

    .line 34
    .line 35
    const v0, 0x3c645804    # 0.013937f

    .line 36
    .line 37
    .line 38
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteStdDev:F

    .line 39
    .line 40
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteMean:F

    .line 41
    .line 42
    const v0, 0x3eb48323

    .line 43
    .line 44
    .line 45
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteWeight:F

    .line 46
    .line 47
    return-void

    .line 48
    nop

    .line 49
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x39d801b4    # 4.12E-4f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3d6b8738
        0x3d07e9d9
        0x3b1491f3    # 0.002267f
        0x3c83b1d1    # 0.016076f
        0x3c6fbf40    # 0.014633f
        0x3d0711d8
        0x3b8711d8    # 0.004122f
        0x3c39baa1    # 0.011336f
        0x3d6fc04d
        0x3cc8eac0    # 0.024526f
        0x3cd4bcf1    # 0.025969f
        0x3d5ede12
        0x3ca0663c    # 0.01958f
        0x3d819546
        0x3de96b33
        0x3cf4d163    # 0.029885f
        0x3e19a459    # 0.150041f
        0x3d724852
        0x3b2f9229    # 0.002679f
        0x3c221640    # 0.009893f
        0x3c731f47    # 0.014839f
        0x3cd81cf8    # 0.026381f
        0x3c767f4e    # 0.015045f
        0x3d8e3ef3
        0x3db80885    # 0.08986f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    :array_1
    .array-data 4
        0x3c88c1db    # 0.016694f
        0x0
        0x3c515ad1    # 0.012778f
        0x3cf68167    # 0.030091f
        0x3b2f9229    # 0.002679f
        0x3bd81adf    # 0.006595f
        0x3af32379    # 0.001855f
        0x3a5801b4    # 8.24E-4f
        0x3bc3dab6    # 0.005977f
        0x3b9b5200    # 0.00474f
        0x3b4aa327    # 0.003092f
        0x3a5801b4    # 8.24E-4f
        0x3ca0663c    # 0.01958f
        0x3d18cc14    # 0.037304f
        0x3c0711d8    # 0.008244f
        0x3c6fbf40    # 0.014633f
        0x3a87229f    # 0.001031f
        0x0
        0x3b582342    # 0.003298f
        0x3b0711d8    # 0.002061f
        0x3bca9ac3    # 0.006183f
        0x3bc3dab6    # 0.005977f
        0x3a5801b4    # 8.24E-4f
        0x3cb2f87b    # 0.021847f
        0x3c731f47    # 0.014839f
        0x3d58f4f9    # 0.052968f
        0x3c8dd1e5    # 0.017312f
        0x3bf9e386    # 0.007626f
        0x39d801b4    # 4.12E-4f
        0x3a5801b4    # 8.24E-4f
        0x3c365669    # 0.011129f
        0x0
        0x39d801b4    # 4.12E-4f
        0x3ad82342    # 0.001649f
        0x3bc3dab6    # 0.005977f
        0x3d86a5d7
        0x3ca57647    # 0.020198f
        0x3caf965b    # 0.021434f
        0x3c6fbf40    # 0.014633f
        0x3b8711d8    # 0.004122f
        0x3ad82342    # 0.001649f
        0x3a5801b4    # 8.24E-4f
        0x3a5801b4    # 8.24E-4f
        0x3d54bbe4
        0x3ca0663c    # 0.01958f
        0x3cbec893    # 0.023289f
        0x3cd81cf8    # 0.026381f
        0x3d257647
        0x3c1491f3    # 0.009068f
        0x3abd230c    # 0.001443f
        0x3b732379    # 0.00371f
        0x3bf32379    # 0.00742f
        0x3abd230c    # 0.001443f
        0x3c581adf    # 0.01319f
        0x3b3d1245    # 0.002885f
        0x39d801b4    # 4.12E-4f
        0x3b582342    # 0.003298f
        0x3cd4bcf1    # 0.025969f
        0x39d801b4    # 4.12E-4f
        0x39d801b4    # 4.12E-4f
        0x3bca9ac3    # 0.006183f
        0x3b582342    # 0.003298f
        0x3d892e62    # 0.066983f
        0x3b2f9229    # 0.002679f
        0x3b1491f3    # 0.002267f
        0x3c365669    # 0.011129f
        0x39d801b4    # 4.12E-4f
        0x3c257647    # 0.010099f
        0x3c79df55    # 0.015251f
        0x3bf9e386    # 0.007626f
        0x3d33cf70    # 0.043899f
        0x3b732379    # 0.00371f
        0x3b2f9229    # 0.002679f
        0x3abd230c    # 0.001443f
        0x3c32f662    # 0.010923f
        0x3b3d1245    # 0.002885f
        0x3c1491f3    # 0.009068f
        0x3ca3c643    # 0.019992f
        0x39d801b4    # 4.12E-4f
        0x3c0a71de    # 0.00845f
        0x3ba8da7f    # 0.005153f
        0x0
        0x3c257647    # 0.010099f
        0x0
        0x3ad82342    # 0.001649f
        0x3c473abd    # 0.01216f
        0x3c3d1aa8    # 0.011542f
        0x3bd81adf    # 0.006595f
        0x3af32379    # 0.001855f
        0x3c32f662    # 0.010923f
        0x39d801b4    # 4.12E-4f
        0x3cc22ab2    # 0.023702f
        0x3b732379    # 0.00371f
        0x3af32379    # 0.001855f
    .end array-data
.end method


# virtual methods
.method public mFirstByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mFirstByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;->mSecondByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

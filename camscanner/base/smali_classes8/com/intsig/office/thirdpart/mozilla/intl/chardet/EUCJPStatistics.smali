.class public Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;
.super Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;
.source "EUCJPStatistics.java"


# static fields
.field static mFirstByteFreq:[F

.field static mFirstByteMean:F

.field static mFirstByteStdDev:F

.field static mFirstByteWeight:F

.field static mSecondByteFreq:[F

.field static mSecondByteMean:F

.field static mSecondByteStdDev:F

.field static mSecondByteWeight:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x5e

    .line 5
    .line 6
    new-array v1, v0, [F

    .line 7
    .line 8
    fill-array-data v1, :array_0

    .line 9
    .line 10
    .line 11
    sput-object v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteFreq:[F

    .line 12
    .line 13
    const v1, 0x3d4e7792    # 0.050407f

    .line 14
    .line 15
    .line 16
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteStdDev:F

    .line 17
    .line 18
    const v1, 0x3c2e4b02    # 0.010638f

    .line 19
    .line 20
    .line 21
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteMean:F

    .line 22
    .line 23
    const v2, 0x3f24101f

    .line 24
    .line 25
    .line 26
    sput v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteWeight:F

    .line 27
    .line 28
    new-array v0, v0, [F

    .line 29
    .line 30
    fill-array-data v0, :array_1

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteFreq:[F

    .line 34
    .line 35
    const v0, 0x3ce76641    # 0.028247f

    .line 36
    .line 37
    .line 38
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteStdDev:F

    .line 39
    .line 40
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteMean:F

    .line 41
    .line 42
    const v0, 0x3eb7dfc2

    .line 43
    .line 44
    .line 45
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteWeight:F

    .line 46
    .line 47
    return-void

    .line 48
    nop

    .line 49
    :array_0
    .array-data 4
        0x3ebac81d
        0x0
        0x0
        0x3e14d014
        0x3e9c1aac
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3af08462    # 0.001835f
        0x3c3078d9    # 0.010771f
        0x3bd3bf2f    # 0.006462f
        0x3a97a67a    # 0.001157f
        0x3b0a8b09    # 0.002114f
        0x3b53bf2f    # 0.003231f
        0x3ab1bbcf    # 0.001356f
        0x3bf32379    # 0.00742f
        0x3b8943e1    # 0.004189f
        0x3b53bf2f    # 0.003231f
        0x3b46b485    # 0.003032f
        0x3d07f23d    # 0.03319f
        0x3bce8965    # 0.006303f
        0x3bc6b485    # 0.006064f
        0x3c2365cb    # 0.009973f
        0x3b1a4591    # 0.002354f
        0x3b708462    # 0.00367f
        0x3c15aaf8    # 0.009135f
        0x3adb8bac    # 0.001675f
        0x3b36f9fd    # 0.002792f
        0x3b0fc936    # 0.002194f
        0x3c712c28    # 0.01472f
        0x3c436da8    # 0.011928f
        0x3a662995    # 8.78E-4f
        0x3c57060c    # 0.013124f
        0x3a8d2a20    # 0.001077f
        0x3c184a0e    # 0.009295f
        0x3b6379b7    # 0.003471f
        0x3b3c382a    # 0.002872f
        0x3b1f72f7    # 0.002433f
        0x3a7adf2f    # 9.57E-4f
        0x3ad66f0d    # 0.001636f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x38a7c5ac    # 8.0E-5f
        0x399246bf    # 2.79E-4f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x38a7c5ac    # 8.0E-5f
        0x0
    .end array-data

    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    :array_1
    .array-data 4
        0x3b22120e    # 0.002473f
        0x3d204af9    # 0.039134f
        0x3e1c692f
        0x3c1ed395    # 0.009694f
        0x39bc382a    # 3.59E-4f
        0x3cb5b2d5    # 0.02218f
        0x3a46b485    # 7.58E-4f
        0x3b8d2a20    # 0.004308f
        0x3927c5ac    # 1.6E-4f
        0x3b24b125    # 0.002513f
        0x3b49539c    # 0.003072f
        0x3aac7da2    # 0.001316f
        0x3b7b00bd    # 0.00383f
        0x3a87ebf2    # 0.001037f
        0x3b6b4635    # 0.00359f
        0x3a7adf2f    # 9.57E-4f
        0x3927c5ac    # 1.6E-4f
        0x397a9c13    # 2.39E-4f
        0x3bd3bf2f    # 0.006462f
        0x3ad130e0    # 0.001596f
        0x3d013ec4
        0x3aac7da2    # 0.001316f
        0x3b0fc936    # 0.002194f
        0x3c879e5a    # 0.016555f
        0x3b565e46    # 0.003271f
        0x3a31bbcf    # 6.78E-4f
        0x3a1cc31a    # 5.98E-4f
        0x3e53647c
        0x3a3c382a    # 7.18E-4f
        0x3a8d2a20    # 0.001077f
        0x3b732379    # 0.00371f
        0x3ab1bbcf    # 0.001356f
        0x3ab1bbcf    # 0.001356f
        0x39e62995    # 4.39E-4f
        0x3b8fc936    # 0.004388f
        0x3bbae89f    # 0.005704f
        0x3a662995    # 8.78E-4f
        0x3c26a876    # 0.010172f
        0x3be75ff6    # 0.007061f
        0x3c708462    # 0.01468f
        0x3a273f75    # 6.38E-4f
        0x3cd2c7b9    # 0.02573f
        0x3b36f9fd    # 0.002792f
        0x3a3c382a    # 7.18E-4f
        0x3aeb4635    # 0.001795f
        0x3dbb7f17
        0x3a46b485    # 7.58E-4f
        0x3b801712    # 0.003909f
        0x3a1246bf    # 5.58E-4f
        0x3cff8ca8    # 0.031195f
        0x3be75ff6    # 0.007061f
        0x3aac7da2    # 0.001316f
        0x3cb8f798    # 0.022579f
        0x3be4c0df    # 0.006981f
        0x3bede54b    # 0.00726f
        0x3a92684d    # 0.001117f
        0x397a9c13    # 2.39E-4f
        0x3c46b053    # 0.012127f
        0x3a662995    # 8.78E-4f
        0x3b7861a6    # 0.00379f
        0x3a8d2a20    # 0.001077f
        0x3a46b485    # 7.58E-4f
        0x3b0a8b09    # 0.002114f
        0x3b12684d    # 0.002234f
        0x3a31bbcf    # 6.78E-4f
        0x3b44156e    # 0.002992f
        0x3b58fd5d    # 0.003311f
        0x3cbfd2e9    # 0.023416f
        0x3aa222d5    # 0.001237f
        0x3b346bad    # 0.002753f
        0x3ba89fc7    # 0.005146f
        0x3b0fc936    # 0.002194f
        0x3be6106b    # 0.007021f
        0x3c0b3700    # 0.008497f
        0x3c617e35    # 0.013763f
        0x3c40ce92    # 0.011768f
        0x3bce8965    # 0.006303f
        0x3afb00bd    # 0.001915f
        0x3a273f75    # 6.38E-4f
        0x3c0fc936    # 0.008776f
        0x3a70a5f0    # 9.18E-4f
        0x3b60daa1    # 0.003431f
        0x3d6bf120
        0x39e62995    # 4.39E-4f
        0x39e62995    # 4.39E-4f
        0x3a46b485    # 7.58E-4f
        0x3b3c382a    # 0.002872f
        0x3adb8bac    # 0.001675f
        0x3c350b0f    # 0.01105f
        0x0
        0x399246bf    # 2.79E-4f
        0x3c46b053    # 0.012127f
        0x3a3c382a    # 7.18E-4f
        0x3bf1d3ed    # 0.00738f
    .end array-data
.end method


# virtual methods
.method public mFirstByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mFirstByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;->mSecondByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

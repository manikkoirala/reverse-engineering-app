.class public abstract Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;
.super Ljava/lang/Object;
.source "nsPSMDetector.java"


# static fields
.field public static final ALL:I = 0x0

.field public static final CHINESE:I = 0x2

.field public static final JAPANESE:I = 0x1

.field public static final KOREAN:I = 0x5

.field public static final MAX_VERIFIERS:I = 0x10

.field public static final NO_OF_LANGUAGES:I = 0x6

.field public static final SIMPLIFIED_CHINESE:I = 0x3

.field public static final TRADITIONAL_CHINESE:I = 0x4


# instance fields
.field mClassItems:I

.field mClassRunSampler:Z

.field mDone:Z

.field mItemIdx:[I

.field mItems:I

.field mRunSampler:Z

.field mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

.field mState:[B

.field mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

.field mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-direct {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 3
    iput-object v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    new-array v0, v0, [I

    .line 4
    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->initVerifiers(I)V

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Reset()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-direct {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 9
    iput-object v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    new-array v0, v0, [I

    .line 10
    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->initVerifiers(I)V

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Reset()V

    return-void
.end method

.method public constructor <init>(I[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;)V
    .locals 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-direct {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 15
    iput-object v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    new-array v0, v0, [I

    .line 16
    iput-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassRunSampler:Z

    .line 18
    iput-object p3, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 19
    iput-object p2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 20
    iput p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassItems:I

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Reset()V

    return-void
.end method


# virtual methods
.method public DataEnd()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 8
    .line 9
    const/4 v2, 0x2

    .line 10
    const/4 v3, 0x0

    .line 11
    if-ne v0, v2, :cond_2

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 16
    .line 17
    aget v2, v2, v3

    .line 18
    .line 19
    aget-object v0, v0, v2

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v2, "GB18030"

    .line 26
    .line 27
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 36
    .line 37
    aget v2, v2, v1

    .line 38
    .line 39
    aget-object v0, v0, v2

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 52
    .line 53
    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 54
    .line 55
    aget v4, v4, v1

    .line 56
    .line 57
    aget-object v0, v0, v4

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_2

    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 70
    .line 71
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 72
    .line 73
    aget v2, v2, v3

    .line 74
    .line 75
    aget-object v0, v0, v2

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 85
    .line 86
    :cond_2
    :goto_0
    iget v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 87
    .line 88
    const/4 v2, 0x4

    .line 89
    if-ne v0, v2, :cond_3

    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 92
    .line 93
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 94
    .line 95
    aget v2, v2, v1

    .line 96
    .line 97
    aget-object v0, v0, v2

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    const-string v2, "Shift_JIS"

    .line 104
    .line 105
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-eqz v0, :cond_3

    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 112
    .line 113
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 114
    .line 115
    aget v2, v2, v1

    .line 116
    .line 117
    aget-object v0, v0, v2

    .line 118
    .line 119
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 127
    .line 128
    :cond_3
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mRunSampler:Z

    .line 129
    .line 130
    if-eqz v0, :cond_4

    .line 131
    .line 132
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0, v0, v3, v1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Sample([BIZ)V

    .line 134
    .line 135
    .line 136
    :cond_4
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public HandleData([BI)Z
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, p2, :cond_9

    .line 4
    .line 5
    aget-byte v2, p1, v1

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    :cond_0
    :goto_1
    iget v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 9
    .line 10
    const/4 v5, 0x1

    .line 11
    if-ge v3, v4, :cond_3

    .line 12
    .line 13
    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 14
    .line 15
    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 16
    .line 17
    aget v6, v6, v3

    .line 18
    .line 19
    aget-object v4, v4, v6

    .line 20
    .line 21
    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    .line 22
    .line 23
    aget-byte v6, v6, v3

    .line 24
    .line 25
    invoke-static {v4, v2, v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->getNextState(Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;BB)B

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    const/4 v6, 0x2

    .line 30
    if-ne v4, v6, :cond_1

    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 33
    .line 34
    iget-object p2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 35
    .line 36
    aget p2, p2, v3

    .line 37
    .line 38
    aget-object p1, p1, p2

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iput-boolean v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 48
    .line 49
    return v5

    .line 50
    :cond_1
    if-ne v4, v5, :cond_2

    .line 51
    .line 52
    iget v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 53
    .line 54
    sub-int/2addr v4, v5

    .line 55
    iput v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 56
    .line 57
    if-ge v3, v4, :cond_0

    .line 58
    .line 59
    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 60
    .line 61
    aget v6, v5, v4

    .line 62
    .line 63
    aput v6, v5, v3

    .line 64
    .line 65
    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    .line 66
    .line 67
    aget-byte v4, v5, v4

    .line 68
    .line 69
    aput-byte v4, v5, v3

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_2
    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    .line 73
    .line 74
    add-int/lit8 v6, v3, 0x1

    .line 75
    .line 76
    aput-byte v4, v5, v3

    .line 77
    .line 78
    move v3, v6

    .line 79
    goto :goto_1

    .line 80
    :cond_3
    if-gt v4, v5, :cond_5

    .line 81
    .line 82
    if-ne v5, v4, :cond_4

    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 85
    .line 86
    iget-object p2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 87
    .line 88
    aget p2, p2, v0

    .line 89
    .line 90
    aget-object p1, p1, p2

    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    :cond_4
    iput-boolean v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 100
    .line 101
    return v5

    .line 102
    :cond_5
    const/4 v2, 0x0

    .line 103
    const/4 v3, 0x0

    .line 104
    const/4 v4, 0x0

    .line 105
    :goto_2
    iget v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 106
    .line 107
    if-ge v2, v6, :cond_7

    .line 108
    .line 109
    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 110
    .line 111
    iget-object v7, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 112
    .line 113
    aget v7, v7, v2

    .line 114
    .line 115
    aget-object v6, v6, v7

    .line 116
    .line 117
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->isUCS2()Z

    .line 118
    .line 119
    .line 120
    move-result v6

    .line 121
    if-nez v6, :cond_6

    .line 122
    .line 123
    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 124
    .line 125
    iget-object v7, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 126
    .line 127
    aget v7, v7, v2

    .line 128
    .line 129
    aget-object v6, v6, v7

    .line 130
    .line 131
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->isUCS2()Z

    .line 132
    .line 133
    .line 134
    move-result v6

    .line 135
    if-nez v6, :cond_6

    .line 136
    .line 137
    add-int/lit8 v3, v3, 0x1

    .line 138
    .line 139
    move v4, v2

    .line 140
    :cond_6
    add-int/lit8 v2, v2, 0x1

    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_7
    if-ne v5, v3, :cond_8

    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 146
    .line 147
    iget-object p2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 148
    .line 149
    aget p2, p2, v4

    .line 150
    .line 151
    aget-object p1, p1, p2

    .line 152
    .line 153
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object p1

    .line 157
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iput-boolean v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 161
    .line 162
    return v5

    .line 163
    :cond_8
    add-int/lit8 v1, v1, 0x1

    .line 164
    .line 165
    goto/16 :goto_0

    .line 166
    .line 167
    :cond_9
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mRunSampler:Z

    .line 168
    .line 169
    if-eqz v0, :cond_a

    .line 170
    .line 171
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Sample([BI)V

    .line 172
    .line 173
    .line 174
    :cond_a
    iget-boolean p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 175
    .line 176
    return p1
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public abstract Report(Ljava/lang/String;)V
.end method

.method public Reset()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassRunSampler:Z

    .line 2
    .line 3
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mRunSampler:Z

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassItems:I

    .line 9
    .line 10
    iput v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    iget v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 14
    .line 15
    if-ge v1, v2, :cond_0

    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mState:[B

    .line 18
    .line 19
    aput-byte v0, v2, v1

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 22
    .line 23
    aput v1, v2, v1

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->Reset()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public Sample([BI)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Sample([BIZ)V

    return-void
.end method

.method public Sample([BIZ)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 2
    :goto_0
    iget v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    if-ge v1, v4, :cond_2

    .line 3
    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v5, v5, v1

    aget-object v4, v4, v5

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 4
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->isUCS2()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v5, v5, v1

    aget-object v4, v4, v5

    .line 5
    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GB18030"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    if-le v2, v1, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 6
    :goto_1
    iput-boolean v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mRunSampler:Z

    if-eqz v4, :cond_a

    .line 7
    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-virtual {v4, p1, p2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->Sample([BI)Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mRunSampler:Z

    if-eqz p3, :cond_4

    .line 8
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->GetSomeData()Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->EnoughData()Z

    move-result p1

    if-eqz p1, :cond_a

    :cond_5
    if-ne v2, v3, :cond_a

    .line 9
    iget-object p1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->CalFreq()V

    const/4 p1, -0x1

    const/4 p2, 0x0

    const/4 p3, 0x0

    .line 10
    :goto_2
    iget v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    if-ge v0, v2, :cond_9

    .line 11
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v3, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v3, v3, v0

    aget-object v2, v2, v3

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    aget-object v2, v2, v3

    .line 12
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Big5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 13
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mSampler:Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;

    iget-object v3, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v4, v4, v0

    aget-object v3, v3, v4

    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;->mFirstByteFreq()[F

    move-result-object v3

    iget-object v4, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v5, v5, v0

    aget-object v4, v4, v5

    .line 15
    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;->mFirstByteWeight()F

    move-result v4

    iget-object v5, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v6, v6, v0

    aget-object v5, v5, v6

    .line 16
    invoke-virtual {v5}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;->mSecondByteFreq()[F

    move-result-object v5

    iget-object v6, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    iget-object v7, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget v7, v7, v0

    aget-object v6, v6, v7

    .line 17
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;->mSecondByteWeight()F

    move-result v6

    .line 18
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCSampler;->GetScore([FF[FF)F

    move-result v2

    add-int/lit8 v3, p3, 0x1

    if-eqz p3, :cond_6

    cmpl-float p3, p2, v2

    if-lez p3, :cond_7

    :cond_6
    move p1, v0

    move p2, v2

    :cond_7
    move p3, v3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    if-ltz p1, :cond_a

    .line 19
    iget-object p2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    iget-object p3, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    aget p1, p3, p1

    aget-object p1, p2, p1

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->Report(Ljava/lang/String;)V

    .line 20
    iput-boolean v1, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mDone:Z

    :cond_a
    return-void
.end method

.method public getProbableCharsets()[Ljava/lang/String;
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "nomatch"

    .line 6
    .line 7
    filled-new-array {v0}, [Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0

    .line 12
    :cond_0
    new-array v0, v0, [Ljava/lang/String;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    iget v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItems:I

    .line 16
    .line 17
    if-ge v1, v2, :cond_1

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mItemIdx:[I

    .line 22
    .line 23
    aget v3, v3, v1

    .line 24
    .line 25
    aget-object v2, v2, v3

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;->charset()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    aput-object v2, v0, v1

    .line 32
    .line 33
    add-int/lit8 v1, v1, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected initVerifiers(I)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p1

    .line 4
    .line 5
    const/4 v2, 0x6

    .line 6
    const/4 v3, 0x0

    .line 7
    if-ltz v1, :cond_0

    .line 8
    .line 9
    if-ge v1, v2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v1, 0x0

    .line 13
    :goto_0
    const/4 v4, 0x0

    .line 14
    iput-object v4, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 15
    .line 16
    iput-object v4, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 17
    .line 18
    const/4 v5, 0x7

    .line 19
    const/4 v6, 0x3

    .line 20
    const/4 v7, 0x2

    .line 21
    const/4 v8, 0x5

    .line 22
    const/4 v9, 0x4

    .line 23
    const/4 v10, 0x1

    .line 24
    if-ne v1, v9, :cond_1

    .line 25
    .line 26
    new-array v1, v5, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 27
    .line 28
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 29
    .line 30
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 31
    .line 32
    .line 33
    aput-object v11, v1, v3

    .line 34
    .line 35
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;

    .line 36
    .line 37
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;-><init>()V

    .line 38
    .line 39
    .line 40
    aput-object v11, v1, v10

    .line 41
    .line 42
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;

    .line 43
    .line 44
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;-><init>()V

    .line 45
    .line 46
    .line 47
    aput-object v11, v1, v7

    .line 48
    .line 49
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;

    .line 50
    .line 51
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;-><init>()V

    .line 52
    .line 53
    .line 54
    aput-object v11, v1, v6

    .line 55
    .line 56
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 57
    .line 58
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 59
    .line 60
    .line 61
    aput-object v11, v1, v9

    .line 62
    .line 63
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 64
    .line 65
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 66
    .line 67
    .line 68
    aput-object v11, v1, v8

    .line 69
    .line 70
    new-instance v11, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 71
    .line 72
    invoke-direct {v11}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 73
    .line 74
    .line 75
    aput-object v11, v1, v2

    .line 76
    .line 77
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 78
    .line 79
    new-array v1, v5, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 80
    .line 81
    aput-object v4, v1, v3

    .line 82
    .line 83
    new-instance v5, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;

    .line 84
    .line 85
    invoke-direct {v5}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;-><init>()V

    .line 86
    .line 87
    .line 88
    aput-object v5, v1, v10

    .line 89
    .line 90
    aput-object v4, v1, v7

    .line 91
    .line 92
    new-instance v5, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;

    .line 93
    .line 94
    invoke-direct {v5}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;-><init>()V

    .line 95
    .line 96
    .line 97
    aput-object v5, v1, v6

    .line 98
    .line 99
    aput-object v4, v1, v9

    .line 100
    .line 101
    aput-object v4, v1, v8

    .line 102
    .line 103
    aput-object v4, v1, v2

    .line 104
    .line 105
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 106
    .line 107
    goto/16 :goto_1

    .line 108
    .line 109
    :cond_1
    if-ne v1, v8, :cond_2

    .line 110
    .line 111
    new-array v1, v2, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 112
    .line 113
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 114
    .line 115
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 116
    .line 117
    .line 118
    aput-object v2, v1, v3

    .line 119
    .line 120
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCKRVerifier;

    .line 121
    .line 122
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCKRVerifier;-><init>()V

    .line 123
    .line 124
    .line 125
    aput-object v2, v1, v10

    .line 126
    .line 127
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022KRVerifier;

    .line 128
    .line 129
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022KRVerifier;-><init>()V

    .line 130
    .line 131
    .line 132
    aput-object v2, v1, v7

    .line 133
    .line 134
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 135
    .line 136
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 137
    .line 138
    .line 139
    aput-object v2, v1, v6

    .line 140
    .line 141
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 142
    .line 143
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 144
    .line 145
    .line 146
    aput-object v2, v1, v9

    .line 147
    .line 148
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 149
    .line 150
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 151
    .line 152
    .line 153
    aput-object v2, v1, v8

    .line 154
    .line 155
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 156
    .line 157
    goto/16 :goto_1

    .line 158
    .line 159
    :cond_2
    const/16 v11, 0x8

    .line 160
    .line 161
    if-ne v1, v6, :cond_3

    .line 162
    .line 163
    new-array v1, v11, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 164
    .line 165
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 166
    .line 167
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 168
    .line 169
    .line 170
    aput-object v4, v1, v3

    .line 171
    .line 172
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;

    .line 173
    .line 174
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;-><init>()V

    .line 175
    .line 176
    .line 177
    aput-object v4, v1, v10

    .line 178
    .line 179
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;

    .line 180
    .line 181
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;-><init>()V

    .line 182
    .line 183
    .line 184
    aput-object v4, v1, v7

    .line 185
    .line 186
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;

    .line 187
    .line 188
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;-><init>()V

    .line 189
    .line 190
    .line 191
    aput-object v4, v1, v6

    .line 192
    .line 193
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;

    .line 194
    .line 195
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;-><init>()V

    .line 196
    .line 197
    .line 198
    aput-object v4, v1, v9

    .line 199
    .line 200
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 201
    .line 202
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 203
    .line 204
    .line 205
    aput-object v4, v1, v8

    .line 206
    .line 207
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 208
    .line 209
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 210
    .line 211
    .line 212
    aput-object v4, v1, v2

    .line 213
    .line 214
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 215
    .line 216
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 217
    .line 218
    .line 219
    aput-object v2, v1, v5

    .line 220
    .line 221
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 222
    .line 223
    goto/16 :goto_1

    .line 224
    .line 225
    :cond_3
    if-ne v1, v10, :cond_4

    .line 226
    .line 227
    new-array v1, v5, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 228
    .line 229
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 230
    .line 231
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 232
    .line 233
    .line 234
    aput-object v4, v1, v3

    .line 235
    .line 236
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsSJISVerifier;

    .line 237
    .line 238
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsSJISVerifier;-><init>()V

    .line 239
    .line 240
    .line 241
    aput-object v4, v1, v10

    .line 242
    .line 243
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCJPVerifier;

    .line 244
    .line 245
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCJPVerifier;-><init>()V

    .line 246
    .line 247
    .line 248
    aput-object v4, v1, v7

    .line 249
    .line 250
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022JPVerifier;

    .line 251
    .line 252
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022JPVerifier;-><init>()V

    .line 253
    .line 254
    .line 255
    aput-object v4, v1, v6

    .line 256
    .line 257
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 258
    .line 259
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 260
    .line 261
    .line 262
    aput-object v4, v1, v9

    .line 263
    .line 264
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 265
    .line 266
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 267
    .line 268
    .line 269
    aput-object v4, v1, v8

    .line 270
    .line 271
    new-instance v4, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 272
    .line 273
    invoke-direct {v4}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 274
    .line 275
    .line 276
    aput-object v4, v1, v2

    .line 277
    .line 278
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 279
    .line 280
    goto/16 :goto_1

    .line 281
    .line 282
    :cond_4
    const/16 v12, 0x9

    .line 283
    .line 284
    const/16 v13, 0xa

    .line 285
    .line 286
    if-ne v1, v7, :cond_5

    .line 287
    .line 288
    new-array v1, v13, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 289
    .line 290
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 291
    .line 292
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 293
    .line 294
    .line 295
    aput-object v14, v1, v3

    .line 296
    .line 297
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;

    .line 298
    .line 299
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;-><init>()V

    .line 300
    .line 301
    .line 302
    aput-object v14, v1, v10

    .line 303
    .line 304
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;

    .line 305
    .line 306
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;-><init>()V

    .line 307
    .line 308
    .line 309
    aput-object v14, v1, v7

    .line 310
    .line 311
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;

    .line 312
    .line 313
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;-><init>()V

    .line 314
    .line 315
    .line 316
    aput-object v14, v1, v6

    .line 317
    .line 318
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;

    .line 319
    .line 320
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;-><init>()V

    .line 321
    .line 322
    .line 323
    aput-object v14, v1, v9

    .line 324
    .line 325
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;

    .line 326
    .line 327
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;-><init>()V

    .line 328
    .line 329
    .line 330
    aput-object v14, v1, v8

    .line 331
    .line 332
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;

    .line 333
    .line 334
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;-><init>()V

    .line 335
    .line 336
    .line 337
    aput-object v14, v1, v2

    .line 338
    .line 339
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 340
    .line 341
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 342
    .line 343
    .line 344
    aput-object v14, v1, v5

    .line 345
    .line 346
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 347
    .line 348
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 349
    .line 350
    .line 351
    aput-object v14, v1, v11

    .line 352
    .line 353
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 354
    .line 355
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 356
    .line 357
    .line 358
    aput-object v14, v1, v12

    .line 359
    .line 360
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 361
    .line 362
    new-array v1, v13, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 363
    .line 364
    aput-object v4, v1, v3

    .line 365
    .line 366
    new-instance v13, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;

    .line 367
    .line 368
    invoke-direct {v13}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;-><init>()V

    .line 369
    .line 370
    .line 371
    aput-object v13, v1, v10

    .line 372
    .line 373
    aput-object v4, v1, v7

    .line 374
    .line 375
    new-instance v7, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;

    .line 376
    .line 377
    invoke-direct {v7}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;-><init>()V

    .line 378
    .line 379
    .line 380
    aput-object v7, v1, v6

    .line 381
    .line 382
    aput-object v4, v1, v9

    .line 383
    .line 384
    aput-object v4, v1, v8

    .line 385
    .line 386
    new-instance v6, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;

    .line 387
    .line 388
    invoke-direct {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;-><init>()V

    .line 389
    .line 390
    .line 391
    aput-object v6, v1, v2

    .line 392
    .line 393
    aput-object v4, v1, v5

    .line 394
    .line 395
    aput-object v4, v1, v11

    .line 396
    .line 397
    aput-object v4, v1, v12

    .line 398
    .line 399
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 400
    .line 401
    goto/16 :goto_1

    .line 402
    .line 403
    :cond_5
    if-nez v1, :cond_6

    .line 404
    .line 405
    const/16 v1, 0xf

    .line 406
    .line 407
    new-array v14, v1, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 408
    .line 409
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;

    .line 410
    .line 411
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUTF8Verifier;-><init>()V

    .line 412
    .line 413
    .line 414
    aput-object v15, v14, v3

    .line 415
    .line 416
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsSJISVerifier;

    .line 417
    .line 418
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsSJISVerifier;-><init>()V

    .line 419
    .line 420
    .line 421
    aput-object v15, v14, v10

    .line 422
    .line 423
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCJPVerifier;

    .line 424
    .line 425
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCJPVerifier;-><init>()V

    .line 426
    .line 427
    .line 428
    aput-object v15, v14, v7

    .line 429
    .line 430
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022JPVerifier;

    .line 431
    .line 432
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022JPVerifier;-><init>()V

    .line 433
    .line 434
    .line 435
    aput-object v15, v14, v6

    .line 436
    .line 437
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCKRVerifier;

    .line 438
    .line 439
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCKRVerifier;-><init>()V

    .line 440
    .line 441
    .line 442
    aput-object v15, v14, v9

    .line 443
    .line 444
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022KRVerifier;

    .line 445
    .line 446
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022KRVerifier;-><init>()V

    .line 447
    .line 448
    .line 449
    aput-object v15, v14, v8

    .line 450
    .line 451
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;

    .line 452
    .line 453
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsBIG5Verifier;-><init>()V

    .line 454
    .line 455
    .line 456
    aput-object v15, v14, v2

    .line 457
    .line 458
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;

    .line 459
    .line 460
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCTWVerifier;-><init>()V

    .line 461
    .line 462
    .line 463
    aput-object v15, v14, v5

    .line 464
    .line 465
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;

    .line 466
    .line 467
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB2312Verifier;-><init>()V

    .line 468
    .line 469
    .line 470
    aput-object v15, v14, v11

    .line 471
    .line 472
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;

    .line 473
    .line 474
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsGB18030Verifier;-><init>()V

    .line 475
    .line 476
    .line 477
    aput-object v15, v14, v12

    .line 478
    .line 479
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;

    .line 480
    .line 481
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsISO2022CNVerifier;-><init>()V

    .line 482
    .line 483
    .line 484
    aput-object v15, v14, v13

    .line 485
    .line 486
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;

    .line 487
    .line 488
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsHZVerifier;-><init>()V

    .line 489
    .line 490
    .line 491
    const/16 v16, 0xb

    .line 492
    .line 493
    aput-object v15, v14, v16

    .line 494
    .line 495
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;

    .line 496
    .line 497
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsCP1252Verifier;-><init>()V

    .line 498
    .line 499
    .line 500
    const/16 v17, 0xc

    .line 501
    .line 502
    aput-object v15, v14, v17

    .line 503
    .line 504
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;

    .line 505
    .line 506
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2BEVerifier;-><init>()V

    .line 507
    .line 508
    .line 509
    const/16 v18, 0xd

    .line 510
    .line 511
    aput-object v15, v14, v18

    .line 512
    .line 513
    new-instance v15, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;

    .line 514
    .line 515
    invoke-direct {v15}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsUCS2LEVerifier;-><init>()V

    .line 516
    .line 517
    .line 518
    const/16 v19, 0xe

    .line 519
    .line 520
    aput-object v15, v14, v19

    .line 521
    .line 522
    iput-object v14, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 523
    .line 524
    new-array v1, v1, [Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 525
    .line 526
    aput-object v4, v1, v3

    .line 527
    .line 528
    aput-object v4, v1, v10

    .line 529
    .line 530
    new-instance v14, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;

    .line 531
    .line 532
    invoke-direct {v14}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCJPStatistics;-><init>()V

    .line 533
    .line 534
    .line 535
    aput-object v14, v1, v7

    .line 536
    .line 537
    aput-object v4, v1, v6

    .line 538
    .line 539
    new-instance v6, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;

    .line 540
    .line 541
    invoke-direct {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCKRStatistics;-><init>()V

    .line 542
    .line 543
    .line 544
    aput-object v6, v1, v9

    .line 545
    .line 546
    aput-object v4, v1, v8

    .line 547
    .line 548
    new-instance v6, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;

    .line 549
    .line 550
    invoke-direct {v6}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/Big5Statistics;-><init>()V

    .line 551
    .line 552
    .line 553
    aput-object v6, v1, v2

    .line 554
    .line 555
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;

    .line 556
    .line 557
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/EUCTWStatistics;-><init>()V

    .line 558
    .line 559
    .line 560
    aput-object v2, v1, v5

    .line 561
    .line 562
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;

    .line 563
    .line 564
    invoke-direct {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;-><init>()V

    .line 565
    .line 566
    .line 567
    aput-object v2, v1, v11

    .line 568
    .line 569
    aput-object v4, v1, v12

    .line 570
    .line 571
    aput-object v4, v1, v13

    .line 572
    .line 573
    aput-object v4, v1, v16

    .line 574
    .line 575
    aput-object v4, v1, v17

    .line 576
    .line 577
    aput-object v4, v1, v18

    .line 578
    .line 579
    aput-object v4, v1, v19

    .line 580
    .line 581
    iput-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 582
    .line 583
    :cond_6
    :goto_1
    iget-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mStatisticsData:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;

    .line 584
    .line 585
    if-eqz v1, :cond_7

    .line 586
    .line 587
    const/4 v3, 0x1

    .line 588
    :cond_7
    iput-boolean v3, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassRunSampler:Z

    .line 589
    .line 590
    iget-object v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mVerifier:[Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsVerifier;

    .line 591
    .line 592
    array-length v1, v1

    .line 593
    iput v1, v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->mClassItems:I

    .line 594
    .line 595
    return-void
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

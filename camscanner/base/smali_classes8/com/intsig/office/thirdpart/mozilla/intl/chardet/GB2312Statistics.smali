.class public Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;
.super Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;
.source "GB2312Statistics.java"


# static fields
.field static mFirstByteFreq:[F

.field static mFirstByteMean:F

.field static mFirstByteStdDev:F

.field static mFirstByteWeight:F

.field static mSecondByteFreq:[F

.field static mSecondByteMean:F

.field static mSecondByteStdDev:F

.field static mSecondByteWeight:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsEUCStatistics;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x5e

    .line 5
    .line 6
    new-array v1, v0, [F

    .line 7
    .line 8
    fill-array-data v1, :array_0

    .line 9
    .line 10
    .line 11
    sput-object v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteFreq:[F

    .line 12
    .line 13
    const v1, 0x3ca480e9    # 0.020081f

    .line 14
    .line 15
    .line 16
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteStdDev:F

    .line 17
    .line 18
    const v1, 0x3c2e4b02    # 0.010638f

    .line 19
    .line 20
    .line 21
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteMean:F

    .line 22
    .line 23
    const v2, 0x3f162707    # 0.586533f

    .line 24
    .line 25
    .line 26
    sput v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteWeight:F

    .line 27
    .line 28
    new-array v0, v0, [F

    .line 29
    .line 30
    fill-array-data v0, :array_1

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteFreq:[F

    .line 34
    .line 35
    const v0, 0x3c67ee91    # 0.014156f

    .line 36
    .line 37
    .line 38
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteStdDev:F

    .line 39
    .line 40
    sput v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteMean:F

    .line 41
    .line 42
    const v0, 0x3ed3b1f2    # 0.413467f

    .line 43
    .line 44
    .line 45
    sput v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteWeight:F

    .line 46
    .line 47
    return-void

    .line 48
    nop

    .line 49
    :array_0
    .array-data 4
        0x3c3e835e    # 0.011628f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3c3e835e    # 0.011628f
        0x3c4b35f4    # 0.012403f
        0x3c18676a    # 0.009302f
        0x3b7e047d    # 0.003876f
        0x3c920e1f    # 0.017829f
        0x3d186877    # 0.037209f
        0x3c0bb4d5    # 0.008527f
        0x3c251e32    # 0.010078f
        0x3c9ec2ce    # 0.01938f
        0x3d5e43ee
        0x3c251e32    # 0.010078f
        0x3d2848bf
        0x3cab7564    # 0.02093f
        0x3c986983    # 0.018605f
        0x3c251e32    # 0.010078f
        0x3c57e88a    # 0.013178f
        0x3c855b8a    # 0.016279f
        0x3bcb3a26    # 0.006202f
        0x3c18676a    # 0.009302f
        0x3c8bb4d5    # 0.017054f
        0x3c3e835e    # 0.011628f
        0x3c0bb4d5    # 0.008527f
        0x3b98676a    # 0.004651f
        0x3bcb3a26    # 0.006202f
        0x3c920e1f    # 0.017829f
        0x3ccb35f4    # 0.024806f
        0x3ca51c19    # 0.020155f
        0x3c649b20    # 0.013953f
        0x3d055b8a
        0x3d120f2c    # 0.035659f
        0x3d8bb55b
        0x3c31d0c8    # 0.010853f
        0x3d153bd1
        0x3defba02    # 0.117054f
        0x3ce49d39    # 0.027907f
        0x3dce6320
        0x3c251e32    # 0.010078f
        0x3c920e1f    # 0.017829f
        0x3d7e047d    # 0.062016f
        0x3c4b35f4    # 0.012403f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3acb295f    # 0.00155f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    :array_1
    .array-data 4
        0x3bcb3a26    # 0.006202f
        0x3cfe047d    # 0.031008f
        0x3bb1cc96    # 0.005426f
        0x3b4b3a26    # 0.003101f
        0x3acb295f    # 0.00155f
        0x3b4b3a26    # 0.003101f
        0x3da84945    # 0.082171f
        0x3c7151e7    # 0.014729f
        0x3be49f51    # 0.006977f
        0x3acb295f    # 0.00155f
        0x3c649b20    # 0.013953f
        0x0
        0x3c649b20    # 0.013953f
        0x3c251e32    # 0.010078f
        0x3c0bb4d5    # 0.008527f
        0x3be49f51    # 0.006977f
        0x3b98676a    # 0.004651f
        0x3b4b3a26    # 0.003101f
        0x3b4b3a26    # 0.003101f
        0x3b4b3a26    # 0.003101f
        0x3c0bb4d5    # 0.008527f
        0x3b4b3a26    # 0.003101f
        0x3bb1cc96    # 0.005426f
        0x3bb1cc96    # 0.005426f
        0x3bb1cc96    # 0.005426f
        0x3b4b3a26    # 0.003101f
        0x3acb295f    # 0.00155f
        0x3bcb3a26    # 0.006202f
        0x3c7151e7    # 0.014729f
        0x3c31d0c8    # 0.010853f
        0x0
        0x3c3e835e    # 0.011628f
        0x0
        0x3d022ee4    # 0.031783f
        0x3c649b20    # 0.013953f
        0x3cf7ab32    # 0.030233f
        0x3d21ef74    # 0.039535f
        0x3c0bb4d5    # 0.008527f
        0x3c7e047d    # 0.015504f
        0x0
        0x3b4b3a26    # 0.003101f
        0x3c0bb4d5    # 0.008527f
        0x3c855b8a    # 0.016279f
        0x3bb1cc96    # 0.005426f
        0x3acb295f    # 0.00155f
        0x3c649b20    # 0.013953f
        0x3c649b20    # 0.013953f
        0x3d382907
        0x3b4b3a26    # 0.003101f
        0x3b98676a    # 0.004651f
        0x3be49f51    # 0.006977f
        0x3acb295f    # 0.00155f
        0x3bb1cc96    # 0.005426f
        0x3c4b35f4    # 0.012403f
        0x3acb295f    # 0.00155f
        0x3c7e047d    # 0.015504f
        0x0
        0x3bcb3a26    # 0.006202f
        0x3acb295f    # 0.00155f
        0x0
        0x3bfe047d    # 0.007752f
        0x3be49f51    # 0.006977f
        0x3acb295f    # 0.00155f
        0x3c18676a    # 0.009302f
        0x3c3e835e    # 0.011628f
        0x3b98676a    # 0.004651f
        0x3c31d0c8    # 0.010853f
        0x3c4b35f4    # 0.012403f
        0x3c920e1f    # 0.017829f
        0x3bb1cc96    # 0.005426f
        0x3ccb35f4    # 0.024806f
        0x0
        0x3bcb3a26    # 0.006202f
        0x0
        0x3da84945    # 0.082171f
        0x3c7e047d    # 0.015504f
        0x3b98676a    # 0.004651f
        0x0
        0x3be49f51    # 0.006977f
        0x3b98676a    # 0.004651f
        0x0
        0x3c0bb4d5    # 0.008527f
        0x3c4b35f4    # 0.012403f
        0x3b98676a    # 0.004651f
        0x3b7e047d    # 0.003876f
        0x3b4b3a26    # 0.003101f
        0x3cb82a13    # 0.022481f
        0x3cc4dca9    # 0.024031f
        0x3acb295f    # 0.00155f
        0x3d41b003
        0x3c18676a    # 0.009302f
        0x3acb295f    # 0.00155f
        0x3bb1cc96    # 0.005426f
        0x3c8bb4d5    # 0.017054f
    .end array-data
.end method


# virtual methods
.method public mFirstByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mFirstByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mFirstByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteFreq()[F
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteFreq:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteMean()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteMean:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteStdDev()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteStdDev:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mSecondByteWeight()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/GB2312Statistics;->mSecondByteWeight:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

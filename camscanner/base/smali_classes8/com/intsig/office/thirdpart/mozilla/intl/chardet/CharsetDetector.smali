.class public Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;
.super Ljava/lang/Object;
.source "CharsetDetector.java"


# static fields
.field public static charsetStr:Ljava/lang/String; = null

.field public static found:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static detect(Ljava/io/BufferedInputStream;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    sput-boolean v0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->found:Z

    const-string v1, "ASCII"

    .line 2
    sput-object v1, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->charsetStr:Ljava/lang/String;

    .line 3
    new-instance v2, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsDetector;

    invoke-direct {v2, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsDetector;-><init>(I)V

    .line 4
    new-instance v3, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector$1;

    invoke-direct {v3}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector$1;-><init>()V

    invoke-virtual {v2, v3}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsDetector;->Init(Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsICharsetDetectionObserver;)V

    const/16 v3, 0x400

    new-array v4, v3, [B

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 5
    :goto_0
    invoke-virtual {p0, v4, v0, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_6

    const/16 v11, 0x32

    if-gt v6, v11, :cond_6

    if-nez v6, :cond_3

    aget-byte v11, v4, v0

    const/4 v12, -0x2

    if-ne v11, v10, :cond_0

    aget-byte v13, v4, v5

    if-eq v13, v12, :cond_1

    :cond_0
    aget-byte v13, v4, v5

    if-ne v13, v12, :cond_2

    if-ne v11, v10, :cond_2

    :cond_1
    const-string p0, "Unicode"

    .line 6
    sput-object p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->charsetStr:Ljava/lang/String;

    return-object p0

    :cond_2
    const/16 v10, -0x11

    if-ne v11, v10, :cond_3

    const/16 v10, -0x45

    if-ne v13, v10, :cond_3

    const/4 v10, 0x2

    aget-byte v10, v4, v10

    const/16 v11, -0x41

    if-ne v10, v11, :cond_3

    const-string p0, "UTF-8"

    .line 7
    sput-object p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->charsetStr:Ljava/lang/String;

    return-object p0

    :cond_3
    if-eqz v7, :cond_4

    .line 8
    invoke-virtual {v2, v4, v9}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsDetector;->isAscii([BI)Z

    move-result v7

    :cond_4
    if-nez v7, :cond_5

    if-nez v8, :cond_5

    .line 9
    invoke-virtual {v2, v4, v9, v0}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsDetector;->DoIt([BIZ)Z

    move-result v8

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 10
    :cond_6
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/nsPSMDetector;->DataEnd()V

    if-eqz v7, :cond_7

    return-object v1

    .line 11
    :cond_7
    sget-boolean p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->found:Z

    if-nez p0, :cond_8

    const/4 p0, 0x0

    return-object p0

    .line 12
    :cond_8
    sget-object p0, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->charsetStr:Ljava/lang/String;

    return-object p0
.end method

.method public static detect(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 13
    invoke-static {p2, p1}, Lcom/intsig/office/ss/util/StreamUtils;->getInputStream(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p0

    .line 14
    new-instance p1, Ljava/io/BufferedInputStream;

    invoke-direct {p1, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 15
    invoke-static {p1}, Lcom/intsig/office/thirdpart/mozilla/intl/chardet/CharsetDetector;->detect(Ljava/io/BufferedInputStream;)Ljava/lang/String;

    move-result-object p0

    .line 16
    invoke-virtual {p1}, Ljava/io/BufferedInputStream;->close()V

    return-object p0
.end method

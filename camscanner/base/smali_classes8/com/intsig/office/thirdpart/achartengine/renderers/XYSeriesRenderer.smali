.class public Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;
.super Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;
.source "XYSeriesRenderer.java"


# instance fields
.field private mFillBelowLine:Z

.field private mFillColor:I

.field private mFillPoints:Z

.field private mLineWidth:F

.field private mPointStyle:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillPoints:Z

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillBelowLine:Z

    .line 9
    .line 10
    const/16 v1, 0x7d

    .line 11
    .line 12
    const/16 v2, 0xc8

    .line 13
    .line 14
    invoke-static {v1, v0, v0, v2}, Landroid/graphics/Color;->argb(IIII)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillColor:I

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->POINT:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mPointStyle:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 23
    .line 24
    const/high16 v0, 0x40400000    # 3.0f

    .line 25
    .line 26
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mLineWidth:F

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
.end method


# virtual methods
.method public getFillBelowLineColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLineWidth()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mLineWidth:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPointStyle()Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mPointStyle:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFillBelowLine()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillBelowLine:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFillPoints()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillPoints:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setFillBelowLine(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillBelowLine:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFillBelowLineColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFillPoints(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mFillPoints:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLineWidth(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mLineWidth:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPointStyle(Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->mPointStyle:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

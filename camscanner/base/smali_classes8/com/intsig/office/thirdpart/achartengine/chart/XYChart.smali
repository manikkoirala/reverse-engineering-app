.class public abstract Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;
.super Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
.source "XYChart.java"


# static fields
.field protected static final SHAPE_WIDTH:I = 0xc

.field public static final TAG:Ljava/lang/String; = "XYChart"


# instance fields
.field private mCalcRange:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[D>;"
        }
    .end annotation
.end field

.field private mCenter:Landroid/graphics/PointF;

.field protected mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

.field protected mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

.field private mScale:F

.field private mScreenR:Landroid/graphics/Rect;

.field private mTranslate:F


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;-><init>()V

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 6
    iput-object p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    return-void
.end method

.method private getLabelLinePos(Landroid/graphics/Paint$Align;)I
    .locals 1

    .line 1
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const/4 p1, -0x4

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x4

    .line 8
    :goto_0
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getValidLabels(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Ljava/lang/Double;

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/Double;->isNaN()Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-eqz v2, :cond_0

    .line 27
    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getXTitleTextAreaSize(IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    int-to-float p1, p1

    .line 14
    const v0, 0x3f4ccccd    # 0.8f

    .line 15
    .line 16
    .line 17
    mul-float v4, p1, v0

    .line 18
    .line 19
    int-to-float p1, p2

    .line 20
    const p2, 0x3e4ccccd    # 0.2f

    .line 21
    .line 22
    .line 23
    mul-float v5, p1, p2

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitleTextSize()F

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iget-object p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    mul-float v3, p1, p2

    .line 44
    .line 45
    move-object v1, p0

    .line 46
    move-object v6, p3

    .line 47
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getTextSize(Ljava/lang/String;FFFLandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    return-object p1

    .line 52
    :cond_0
    const/4 p1, 0x0

    .line 53
    return-object p1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getYTitleTextAreaSize(IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitle()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    int-to-float p2, p2

    .line 14
    const v0, 0x3f4ccccd    # 0.8f

    .line 15
    .line 16
    .line 17
    mul-float v4, p2, v0

    .line 18
    .line 19
    int-to-float p1, p1

    .line 20
    const p2, 0x3e4ccccd    # 0.2f

    .line 21
    .line 22
    .line 23
    mul-float v5, p1, p2

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitle()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitleTextSize()F

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iget-object p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    mul-float v3, p1, p2

    .line 44
    .line 45
    move-object v1, p0

    .line 46
    move-object v6, p3

    .line 47
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getTextSize(Ljava/lang/String;FFFLandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iget p2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 52
    .line 53
    iget p3, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 54
    .line 55
    iput p3, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 56
    .line 57
    iput p2, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 58
    .line 59
    return-object p1

    .line 60
    :cond_0
    const/4 p1, 0x0

    .line 61
    return-object p1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V
    .locals 0

    .line 1
    invoke-virtual {p6, p1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p6, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p6, p3}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p6, p5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p6, p4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method private transform(Landroid/graphics/Canvas;FZ)V
    .locals 2

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    iget p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScale:F

    .line 6
    .line 7
    div-float/2addr v0, p3

    .line 8
    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 9
    .line 10
    .line 11
    iget p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mTranslate:F

    .line 12
    .line 13
    neg-float v0, p3

    .line 14
    invoke-virtual {p1, p3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 15
    .line 16
    .line 17
    neg-float p2, p2

    .line 18
    iget-object p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCenter:Landroid/graphics/PointF;

    .line 19
    .line 20
    iget v0, p3, Landroid/graphics/PointF;->x:F

    .line 21
    .line 22
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 23
    .line 24
    invoke-virtual {p1, p2, v0, p3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCenter:Landroid/graphics/PointF;

    .line 29
    .line 30
    iget v1, p3, Landroid/graphics/PointF;->x:F

    .line 31
    .line 32
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 33
    .line 34
    invoke-virtual {p1, p2, v1, p3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 35
    .line 36
    .line 37
    iget p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mTranslate:F

    .line 38
    .line 39
    neg-float p3, p2

    .line 40
    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 41
    .line 42
    .line 43
    iget p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScale:F

    .line 44
    .line 45
    div-float/2addr v0, p2

    .line 46
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;IIIILandroid/graphics/Paint;)V
    .locals 72

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move/from16 v14, p3

    move/from16 v15, p4

    move/from16 v10, p5

    move/from16 v11, p6

    move-object/from16 v8, p7

    .line 1
    new-instance v4, Landroid/graphics/Rect;

    add-int v9, v14, v10

    add-int v7, v15, v11

    invoke-direct {v4, v14, v15, v9, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 3
    invoke-virtual {v13, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 4
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isAntialiasing()Z

    move-result v0

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 5
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    .line 6
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v5

    .line 7
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v16, v5

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackgroundAndFrame(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 8
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendHeight()I

    move-result v0

    .line 9
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLegend()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 10
    div-int/lit8 v0, v11, 0x5

    :cond_0
    move/from16 v17, v0

    .line 11
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result v5

    .line 12
    new-array v4, v5, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    .line 13
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v1, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getTitle()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v12, v0, v10, v11, v8}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getTitleTextAreaSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v2

    .line 15
    invoke-direct {v12, v10, v11, v8}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getXTitleTextAreaSize(IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    .line 16
    invoke-direct {v12, v10, v11, v8}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getYTitleTextAreaSize(IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    if-eqz v2, :cond_2

    .line 17
    iget v3, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    sub-int v3, v11, v3

    move/from16 v18, v3

    goto :goto_1

    :cond_2
    move/from16 v18, v11

    .line 18
    :goto_1
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    move/from16 v19, v6

    move-object v6, v0

    move-object/from16 v0, p0

    move-object v13, v1

    move-object v1, v3

    move-object v3, v2

    move-object v2, v4

    move-object/from16 v20, v3

    const/4 v8, 0x0

    move/from16 v3, p5

    move-object/from16 v21, v4

    move/from16 v4, v18

    move/from16 v22, v5

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendAutoSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v5

    .line 19
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getMargins()[D

    move-result-object v18

    const/4 v4, 0x1

    .line 20
    aget-wide v0, v18, v4

    int-to-double v2, v10

    mul-double v0, v0, v2

    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitleTextSize()F

    move-result v4

    iget-object v8, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v8

    mul-float v4, v4, v8

    move/from16 v24, v7

    float-to-double v7, v4

    add-double/2addr v0, v7

    double-to-int v0, v0

    add-int/2addr v0, v14

    if-eqz v6, :cond_3

    .line 21
    iget v1, v6, Lcom/intsig/office/java/awt/Rectangle;->width:I

    add-int/2addr v0, v1

    :cond_3
    const/4 v1, 0x0

    .line 22
    aget-wide v7, v18, v1

    move v4, v0

    int-to-double v0, v11

    mul-double v7, v7, v0

    double-to-int v7, v7

    add-int/2addr v7, v15

    move-object/from16 v8, v20

    move-object/from16 v20, v6

    if-eqz v8, :cond_4

    .line 23
    iget v6, v8, Lcom/intsig/office/java/awt/Rectangle;->height:I

    add-int/2addr v7, v6

    :cond_4
    const/4 v6, 0x3

    .line 24
    aget-wide v25, v18, v6

    move/from16 v27, v7

    mul-double v6, v25, v2

    double-to-int v6, v6

    sub-int v6, v9, v6

    const/4 v7, 0x2

    move-wide/from16 v25, v2

    if-eqz v5, :cond_6

    .line 25
    iget-byte v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    if-eqz v2, :cond_5

    if-ne v2, v7, :cond_6

    .line 26
    :cond_5
    iget v2, v5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    sub-int/2addr v6, v2

    .line 27
    :cond_6
    aget-wide v2, v18, v7

    mul-double v2, v2, v0

    double-to-int v2, v2

    sub-int v2, v24, v2

    if-eqz v5, :cond_8

    .line 28
    iget-byte v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    const/4 v7, 0x1

    if-eq v3, v7, :cond_7

    const/4 v7, 0x3

    if-ne v3, v7, :cond_8

    .line 29
    :cond_7
    iget v3, v5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    sub-int/2addr v2, v3

    :cond_8
    if-eqz v13, :cond_9

    .line 30
    iget v3, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    sub-int/2addr v2, v3

    .line 31
    :cond_9
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v3

    iget-object v7, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v7

    mul-float v3, v3, v7

    move-object/from16 v7, p7

    move-object/from16 v23, v13

    const/4 v13, 0x0

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 32
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    int-to-float v2, v2

    .line 33
    iget v13, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v13, v3

    sub-float/2addr v2, v13

    float-to-int v2, v2

    .line 34
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 35
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v13, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v13}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getTextTypefaceName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 36
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Typeface;->getStyle()I

    move-result v3

    iget-object v13, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v13}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getTextTypefaceStyle()I

    move-result v13

    if-eq v3, v13, :cond_b

    .line 37
    :cond_a
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getTextTypefaceName()Ljava/lang/String;

    move-result-object v3

    iget-object v13, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 38
    invoke-virtual {v13}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getTextTypefaceStyle()I

    move-result v13

    .line 39
    invoke-static {v3, v13}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 40
    :cond_b
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getOrientation()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    move-result-object v13

    .line 41
    sget-object v3, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v13, v3, :cond_c

    sub-int v6, v6, v17

    add-int/lit8 v17, v17, -0x14

    add-int v2, v2, v17

    :cond_c
    move v3, v6

    move v6, v2

    .line 42
    invoke-virtual {v13}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->getAngle()I

    move-result v2

    move-wide/from16 v30, v0

    const/16 v0, 0x5a

    if-ne v2, v0, :cond_d

    const/16 v17, 0x1

    goto :goto_2

    :cond_d
    const/16 v17, 0x0

    :goto_2
    int-to-float v1, v11

    int-to-float v0, v10

    move-object/from16 v32, v5

    div-float v5, v1, v0

    .line 43
    iput v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScale:F

    sub-int v5, v10, v11

    .line 44
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    const/16 v29, 0x2

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iput v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mTranslate:F

    move/from16 v33, v0

    .line 45
    iget v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScale:F

    const/high16 v34, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v34

    if-gez v0, :cond_e

    const/high16 v0, -0x40800000    # -1.0f

    mul-float v5, v5, v0

    .line 46
    iput v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mTranslate:F

    .line 47
    :cond_e
    new-instance v0, Landroid/graphics/PointF;

    div-int/lit8 v5, v9, 0x2

    int-to-float v5, v5

    move/from16 v34, v1

    div-int/lit8 v1, v24, 0x2

    int-to-float v1, v1

    invoke-direct {v0, v5, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCenter:Landroid/graphics/PointF;

    if-eqz v17, :cond_f

    int-to-float v0, v2

    move-object/from16 v5, p1

    const/4 v1, 0x0

    .line 48
    invoke-direct {v12, v5, v0, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    goto :goto_3

    :cond_f
    move-object/from16 v5, p1

    :goto_3
    const v0, -0x7fffffff

    move/from16 v15, v22

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v15, :cond_10

    move/from16 v22, v2

    .line 49
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v2, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    move/from16 v2, v22

    goto :goto_4

    :cond_10
    move/from16 v22, v2

    const/4 v1, 0x1

    add-int/lit8 v2, v0, 0x1

    if-gez v2, :cond_11

    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    .line 51
    :cond_11
    new-array v1, v2, [D

    .line 52
    new-array v0, v2, [D

    .line 53
    new-array v14, v2, [D

    move-object/from16 v35, v13

    .line 54
    new-array v13, v2, [D

    move-object/from16 v36, v8

    .line 55
    new-array v8, v2, [Z

    move/from16 v37, v9

    .line 56
    new-array v9, v2, [Z

    .line 57
    new-array v10, v2, [Z

    .line 58
    new-array v11, v2, [Z

    move/from16 v38, v15

    const/4 v15, 0x0

    :goto_5
    if-ge v15, v2, :cond_13

    .line 59
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v39

    aput-wide v39, v1, v15

    .line 60
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v39

    aput-wide v39, v0, v15

    .line 61
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v39

    aput-wide v39, v14, v15

    .line 62
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v39

    aput-wide v39, v13, v15

    .line 63
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v5

    aput-boolean v5, v8, v15

    .line 64
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v5

    aput-boolean v5, v9, v15

    .line 65
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinYSet(I)Z

    move-result v5

    aput-boolean v5, v10, v15

    .line 66
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v5

    aput-boolean v5, v11, v15

    .line 67
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    move-object/from16 v39, v11

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v5, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_12

    .line 68
    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v40, v10

    const/4 v10, 0x4

    new-array v10, v10, [D

    invoke-interface {v5, v11, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_12
    move-object/from16 v40, v10

    :goto_6
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v5, p1

    move-object/from16 v11, v39

    move-object/from16 v10, v40

    goto :goto_5

    :cond_13
    move-object/from16 v40, v10

    move-object/from16 v39, v11

    .line 69
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    const/4 v5, 0x0

    :goto_7
    if-ge v5, v2, :cond_14

    .line 70
    iget-object v10, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v10}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v10

    iget-object v11, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v11}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v11

    mul-float v10, v10, v11

    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 71
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v10

    .line 72
    iget v11, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v10, v10, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v11, v10

    sub-int v10, v6, v27

    int-to-float v10, v10

    div-float/2addr v10, v11

    float-to-int v10, v10

    const/4 v11, 0x2

    .line 73
    div-int/2addr v10, v11

    .line 74
    iget-object v11, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v11}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabels()I

    move-result v11

    invoke-static {v11, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 75
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v42, v0

    move-object/from16 v41, v1

    aget-wide v0, v14, v5

    move-object/from16 v43, v8

    move-object/from16 v44, v9

    aget-wide v8, v13, v5

    invoke-static {v0, v1, v8, v9, v10}, Lcom/intsig/office/thirdpart/achartengine/util/MathHelper;->getLabels(DDI)Ljava/util/List;

    move-result-object v0

    invoke-direct {v12, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getValidLabels(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v15, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v1, v41

    move-object/from16 v0, v42

    move-object/from16 v8, v43

    move-object/from16 v9, v44

    goto :goto_7

    :cond_14
    move-object/from16 v42, v0

    move-object/from16 v41, v1

    move-object/from16 v43, v8

    move-object/from16 v44, v9

    const/4 v0, 0x0

    :goto_8
    const-wide/16 v45, 0x0

    if-ge v0, v2, :cond_16

    .line 76
    aget-wide v8, v14, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v1, v8, v10

    if-lez v1, :cond_15

    .line 77
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    const/4 v5, 0x0

    .line 78
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const/4 v10, 0x1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v47

    sub-double v10, v10, v47

    sub-double/2addr v8, v10

    .line 79
    aget-wide v10, v14, v0

    cmpl-double v1, v10, v45

    if-lez v1, :cond_15

    cmpl-double v1, v8, v45

    if-lez v1, :cond_15

    .line 80
    aput-wide v8, v14, v0

    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 81
    :cond_16
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v0

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v0, v0, v1

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v2, :cond_18

    .line 82
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v15, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 83
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x0

    :goto_a
    if-ge v9, v8, :cond_17

    .line 84
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move/from16 v48, v2

    move/from16 v47, v3

    .line 85
    aget-wide v2, v14, v1

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    aput-wide v2, v14, v1

    .line 86
    aget-wide v2, v13, v1

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    aput-wide v2, v13, v1

    .line 87
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getFormatCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v10, v11, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    add-int/lit8 v9, v9, 0x1

    move/from16 v3, v47

    move/from16 v2, v48

    goto :goto_a

    :cond_17
    move/from16 v48, v2

    move/from16 v47, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_18
    move/from16 v48, v2

    move/from16 v47, v3

    int-to-float v1, v4

    add-float/2addr v1, v0

    float-to-int v10, v1

    .line 88
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    if-nez v0, :cond_19

    .line 89
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 90
    :cond_19
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    move/from16 v8, v27

    move/from16 v3, v47

    invoke-virtual {v0, v10, v8, v3, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 91
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    move-object/from16 v5, p1

    invoke-virtual {v12, v0, v5, v1, v7}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawSeriesBackgroundAndFrame(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move/from16 v2, v48

    .line 92
    new-array v11, v2, [D

    .line 93
    new-array v9, v2, [D

    move/from16 v4, v38

    const/4 v0, 0x0

    :goto_b
    if-ge v0, v4, :cond_1f

    .line 94
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v1, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v27

    .line 96
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getItemCount()I

    move-result v38

    if-nez v38, :cond_1b

    move/from16 v38, v4

    move/from16 v47, v6

    :cond_1a
    move/from16 v29, v8

    const/4 v6, 0x3

    goto/16 :goto_e

    .line 97
    :cond_1b
    aget-boolean v38, v43, v27

    if-nez v38, :cond_1c

    move/from16 v38, v4

    .line 98
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinX()D

    move-result-wide v4

    move/from16 v47, v6

    .line 99
    aget-wide v6, v41, v27

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    aput-wide v4, v41, v27

    .line 100
    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [D

    aget-wide v5, v41, v27

    const/4 v7, 0x0

    aput-wide v5, v4, v7

    goto :goto_c

    :cond_1c
    move/from16 v38, v4

    move/from16 v47, v6

    .line 101
    :goto_c
    aget-boolean v4, v44, v27

    if-nez v4, :cond_1d

    .line 102
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxX()D

    move-result-wide v4

    .line 103
    aget-wide v6, v42, v27

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    aput-wide v4, v42, v27

    .line 104
    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [D

    aget-wide v5, v42, v27

    const/4 v7, 0x1

    aput-wide v5, v4, v7

    .line 105
    :cond_1d
    aget-boolean v4, v40, v27

    if-nez v4, :cond_1e

    .line 106
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinY()D

    move-result-wide v4

    .line 107
    aget-wide v6, v14, v27

    double-to-float v4, v4

    float-to-double v4, v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    aput-wide v4, v14, v27

    .line 108
    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [D

    aget-wide v5, v14, v27

    const/4 v7, 0x2

    aput-wide v5, v4, v7

    goto :goto_d

    :cond_1e
    const/4 v7, 0x2

    .line 109
    :goto_d
    aget-boolean v4, v39, v27

    if-nez v4, :cond_1a

    .line 110
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxY()D

    move-result-wide v4

    move/from16 v29, v8

    .line 111
    aget-wide v7, v13, v27

    double-to-float v1, v4

    float-to-double v4, v1

    invoke-static {v7, v8, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    aput-wide v4, v13, v27

    .line 112
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [D

    aget-wide v4, v13, v27

    const/4 v6, 0x3

    aput-wide v4, v1, v6

    :goto_e
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v5, p1

    move-object/from16 v7, p7

    move/from16 v8, v29

    move/from16 v4, v38

    move/from16 v6, v47

    goto/16 :goto_b

    :cond_1f
    move/from16 v38, v4

    move/from16 v47, v6

    move/from16 v29, v8

    const/4 v6, 0x3

    const/4 v0, 0x0

    :goto_f
    if-ge v0, v2, :cond_22

    .line 113
    aget-wide v4, v42, v0

    aget-wide v7, v41, v0

    sub-double v27, v4, v7

    cmpl-double v1, v27, v45

    if-eqz v1, :cond_20

    sub-int v1, v3, v10

    move/from16 v27, v2

    int-to-double v1, v1

    sub-double/2addr v4, v7

    div-double/2addr v1, v4

    .line 114
    aput-wide v1, v11, v0

    goto :goto_10

    :cond_20
    move/from16 v27, v2

    .line 115
    :goto_10
    aget-wide v1, v13, v0

    aget-wide v4, v14, v0

    sub-double v7, v1, v4

    cmpl-double v28, v7, v45

    if-eqz v28, :cond_21

    sub-int v7, v47, v29

    int-to-double v7, v7

    sub-double/2addr v1, v4

    div-double/2addr v7, v1

    double-to-float v1, v7

    float-to-double v1, v1

    .line 116
    aput-wide v1, v9, v0

    :cond_21
    add-int/lit8 v0, v0, 0x1

    move/from16 v2, v27

    goto :goto_f

    :cond_22
    move/from16 v27, v2

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "draw minX: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    aget-wide v4, v41, v1

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v2, " , maxX: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v4, v42, v1

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", maxValue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 118
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v0

    const/high16 v28, 0x40000000    # 2.0f

    div-float v0, v0, v28

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v39

    move/from16 v4, v38

    const/4 v0, 0x0

    :goto_11
    if-ge v0, v4, :cond_24

    .line 119
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v1, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getItemCount()I

    move-result v1

    if-lez v1, :cond_23

    const/4 v0, 0x1

    goto :goto_12

    :cond_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_24
    const/4 v0, 0x0

    .line 120
    :goto_12
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLabels()Z

    move-result v1

    if-eqz v1, :cond_25

    if-eqz v0, :cond_25

    const/16 v38, 0x1

    goto :goto_13

    :cond_25
    const/16 v38, 0x0

    .line 121
    :goto_13
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowGridH()Z

    move-result v40

    .line 122
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowCustomTextGrid()Z

    move-result v43

    if-nez v38, :cond_27

    if-eqz v40, :cond_26

    goto :goto_14

    :cond_26
    move/from16 v13, p3

    move/from16 v8, p6

    move/from16 v65, v4

    move/from16 v64, v10

    move/from16 v58, v19

    move/from16 v55, v22

    move/from16 v48, v27

    move/from16 v69, v29

    move-object/from16 v68, v32

    move-object/from16 v15, v35

    move-object/from16 v67, v36

    move/from16 v20, v37

    move-object/from16 v34, v41

    move/from16 v60, v47

    move/from16 v10, p5

    move-object/from16 v32, v11

    move-object/from16 v19, v14

    move/from16 v11, v24

    move-object/from16 v14, p7

    move-object/from16 v24, v9

    move v9, v3

    goto/16 :goto_2c

    .line 123
    :cond_27
    :goto_14
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabels()I

    move-result v0

    if-lez v0, :cond_28

    const/4 v0, 0x0

    aget-wide v1, v42, v0

    iget-object v5, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabels()I

    move-result v5

    int-to-double v7, v5

    cmpl-double v5, v1, v7

    if-lez v5, :cond_28

    .line 124
    aget-wide v1, v42, v0

    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabels()I

    move-result v0

    int-to-double v7, v0

    div-double/2addr v1, v7

    double-to-int v7, v1

    goto :goto_15

    :cond_28
    const/4 v7, 0x1

    .line 125
    :goto_15
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getChartType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Scatter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 127
    aget-wide v49, v41, v2

    move v5, v3

    int-to-double v2, v7

    add-double v49, v49, v2

    :goto_16
    const/4 v8, 0x0

    .line 128
    aget-wide v51, v42, v8

    cmpg-double v8, v49, v51

    if-gtz v8, :cond_29

    move/from16 v44, v7

    .line 129
    invoke-static/range {v49 .. v50}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    .line 130
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v52, v4

    const-string v4, "draw xLabels count:  "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", add xLabel:"

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    add-double v49, v49, v2

    move/from16 v7, v44

    move/from16 v4, v52

    const/4 v6, 0x3

    goto :goto_16

    :cond_29
    move/from16 v52, v4

    move/from16 v44, v7

    move-object v2, v0

    move/from16 p2, v5

    const/4 v0, 0x0

    goto :goto_17

    :cond_2a
    move v5, v3

    move/from16 v52, v4

    move/from16 v44, v7

    const/4 v0, 0x0

    .line 132
    aget-wide v2, v41, v0

    aget-wide v6, v42, v0

    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXLabels()I

    move-result v4

    invoke-static {v2, v3, v6, v7, v4}, Lcom/intsig/office/thirdpart/achartengine/util/MathHelper;->getLabels(DDI)Ljava/util/List;

    move-result-object v2

    invoke-direct {v12, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getValidLabels(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 133
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    aput-wide v3, v41, v0

    .line 134
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v42, v0

    sub-int v3, v5, v10

    move/from16 p2, v5

    int-to-double v4, v3

    .line 135
    aget-wide v49, v41, v0

    sub-double v6, v6, v49

    div-double/2addr v4, v6

    aput-wide v4, v11, v0

    .line 136
    :goto_17
    aget-wide v3, v11, v0

    move/from16 v7, v44

    int-to-double v5, v7

    mul-double v3, v3, v5

    aput-wide v3, v11, v0

    if-eqz v38, :cond_2b

    .line 137
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v0

    move-object/from16 v7, p7

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v0

    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v3

    mul-float v0, v0, v3

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 139
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXLabelsAlign()Landroid/graphics/Paint$Align;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 140
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXLabelsAlign()Landroid/graphics/Paint$Align;

    move-result-object v0

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    if-ne v0, v3, :cond_2c

    int-to-float v0, v10

    .line 141
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v3

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    move v5, v0

    goto :goto_18

    :cond_2b
    move-object/from16 v7, p7

    :cond_2c
    move v5, v10

    :goto_18
    move/from16 v6, v47

    int-to-float v0, v6

    const/4 v3, 0x0

    .line 142
    aget-wide v49, v14, v3

    cmpg-double v4, v49, v45

    if-gez v4, :cond_2d

    int-to-double v7, v6

    .line 143
    aget-wide v53, v9, v3

    mul-double v53, v53, v49

    add-double v7, v7, v53

    double-to-float v0, v7

    :cond_2d
    move v7, v0

    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getChartType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 145
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabelLocations()[Ljava/lang/Double;

    move-result-object v8

    aget-wide v49, v11, v3

    aget-wide v53, v41, v3

    move-object/from16 v0, p0

    move-object/from16 v3, v23

    move/from16 v23, v34

    move-object/from16 v34, v41

    move-object v1, v2

    move/from16 v55, v22

    move-object/from16 v22, v13

    move/from16 v13, v27

    move-object v2, v8

    move/from16 v56, p2

    move-object v8, v3

    move-object/from16 v3, p1

    move/from16 v27, v52

    move-object/from16 v4, p7

    move-object/from16 v57, v32

    move/from16 v60, v6

    move/from16 v58, v19

    move-object/from16 v59, v20

    move/from16 v6, v29

    move/from16 v61, v24

    move-object/from16 v62, v8

    move-object/from16 v24, v9

    move-object/from16 v19, v14

    move-object/from16 v63, v36

    move/from16 v20, v37

    move-object/from16 v14, p7

    move-wide/from16 v8, v49

    move/from16 v64, v10

    move-object/from16 v32, v11

    move-wide/from16 v10, v53

    invoke-virtual/range {v0 .. v11}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawXLabels(Ljava/util/List;[Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIFDD)V

    goto :goto_19

    :cond_2e
    move/from16 v56, p2

    move/from16 v60, v6

    move/from16 v64, v10

    move/from16 v58, v19

    move-object/from16 v59, v20

    move/from16 v55, v22

    move-object/from16 v62, v23

    move/from16 v61, v24

    move-object/from16 v57, v32

    move/from16 v23, v34

    move-object/from16 v63, v36

    move/from16 v20, v37

    move-object/from16 v34, v41

    move-object/from16 v24, v9

    move-object/from16 v32, v11

    move-object/from16 v22, v13

    move-object/from16 v19, v14

    move/from16 v13, v27

    move/from16 v27, v52

    move-object/from16 v14, p7

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 146
    aget-wide v8, v32, v0

    aget-wide v10, v34, v0

    move-object/from16 v0, p0

    move-object v1, v2

    move-object v2, v3

    move-object/from16 v3, p1

    move-object/from16 v4, p7

    move/from16 v6, v29

    invoke-virtual/range {v0 .. v11}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawXLabels(Ljava/util/List;[Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIFDD)V

    :goto_19
    const/4 v7, 0x0

    :goto_1a
    if-ge v7, v13, :cond_37

    .line 147
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 148
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/List;

    const/4 v0, 0x0

    .line 149
    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    aget-wide v3, v19, v0

    sub-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    const-wide v3, 0x3eb0c6f7a0000000L    # 9.999999974752427E-7

    cmpl-double v5, v1, v3

    if-lez v5, :cond_2f

    .line 150
    aget-wide v1, v19, v0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_2f
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x0

    :goto_1b
    if-ge v10, v9, :cond_36

    .line 152
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 153
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v2

    .line 154
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;

    move-result-object v3

    move/from16 v11, v60

    if-eqz v3, :cond_30

    const/4 v3, 0x1

    goto :goto_1c

    :cond_30
    const/4 v3, 0x0

    :goto_1c
    int-to-double v4, v11

    .line 155
    aget-wide v36, v24, v7

    aget-wide v41, v19, v7

    sub-double v41, v0, v41

    mul-double v36, v36, v41

    sub-double v4, v4, v36

    double-to-float v6, v4

    .line 156
    sget-object v4, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    move-object/from16 v5, v35

    if-ne v5, v4, :cond_33

    if-eqz v38, :cond_32

    if-nez v3, :cond_32

    .line 157
    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v3

    invoke-virtual {v14, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    if-ne v2, v3, :cond_31

    .line 159
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getFormatCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v0, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move/from16 v4, v64

    int-to-float v3, v4

    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getFormatCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v0, v1, v4}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    sub-float/2addr v3, v0

    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 160
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v35

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 p2, v9

    move/from16 v9, v64

    move v4, v6

    move-object/from16 v36, v15

    move-object v15, v5

    move-object/from16 v5, p7

    move/from16 v37, v6

    move/from16 v6, v35

    .line 161
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    move-object/from16 v41, v8

    move/from16 v8, v56

    goto :goto_1d

    :cond_31
    move/from16 v37, v6

    move/from16 p2, v9

    move-object/from16 v36, v15

    move/from16 v9, v64

    move-object v15, v5

    .line 162
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getFormatCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v0, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move/from16 v6, v56

    int-to-float v3, v6

    sub-float v4, v37, v28

    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v35

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v5, p7

    move-object/from16 v41, v8

    move v8, v6

    move/from16 v6, v35

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto :goto_1d

    :cond_32
    move/from16 v37, v6

    move-object/from16 v41, v8

    move/from16 p2, v9

    move-object/from16 v36, v15

    move/from16 v8, v56

    move/from16 v9, v64

    move-object v15, v5

    :goto_1d
    if-eqz v40, :cond_35

    .line 163
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v9

    sub-float v2, v37, v39

    int-to-float v3, v8

    add-float v4, v37, v39

    move-object/from16 v0, p1

    move-object/from16 v5, p7

    .line 164
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1e

    :cond_33
    move/from16 v37, v6

    move-object/from16 v41, v8

    move/from16 p2, v9

    move-object/from16 v36, v15

    move/from16 v8, v56

    move/from16 v9, v64

    move-object v15, v5

    .line 165
    sget-object v2, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v2, :cond_35

    if-eqz v38, :cond_34

    if-nez v3, :cond_34

    .line 166
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v2

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 167
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getFormatCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v0, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v8, 0xa

    int-to-float v3, v3

    sub-float v4, v37, v28

    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    :cond_34
    if-eqz v40, :cond_35

    .line 168
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v8

    .line 169
    invoke-static/range {v37 .. v37}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/4 v6, 0x1

    sub-int/2addr v0, v6

    int-to-float v2, v0

    int-to-float v3, v9

    invoke-static/range {v37 .. v37}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v4, v0

    move-object/from16 v0, p1

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1f

    :cond_35
    :goto_1e
    const/4 v6, 0x1

    :goto_1f
    add-int/lit8 v10, v10, 0x1

    move/from16 v56, v8

    move/from16 v64, v9

    move/from16 v60, v11

    move-object/from16 v35, v15

    move-object/from16 v15, v36

    move-object/from16 v8, v41

    move/from16 v9, p2

    goto/16 :goto_1b

    :cond_36
    move-object/from16 v36, v15

    move-object/from16 v15, v35

    move/from16 v8, v56

    move/from16 v11, v60

    move/from16 v9, v64

    const/4 v6, 0x1

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v15, v36

    goto/16 :goto_1a

    :cond_37
    move-object/from16 v15, v35

    move/from16 v8, v56

    move/from16 v11, v60

    move/from16 v9, v64

    const/4 v6, 0x1

    if-eqz v38, :cond_3d

    .line 170
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v7, 0x0

    :goto_20
    if-ge v7, v13, :cond_3d

    .line 171
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v10

    .line 172
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTextLabelLocations(I)[Ljava/lang/Double;

    move-result-object v5

    .line 173
    array-length v4, v5

    const/4 v3, 0x0

    :goto_21
    if-ge v3, v4, :cond_3c

    aget-object v0, v5, v3

    .line 174
    aget-wide v1, v19, v7

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v35

    cmpg-double v37, v1, v35

    if-gtz v37, :cond_3a

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    aget-wide v35, v22, v7

    cmpg-double v37, v1, v35

    if-gtz v37, :cond_3a

    int-to-double v1, v11

    .line 175
    aget-wide v35, v24, v7

    .line 176
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v40

    aget-wide v47, v19, v7

    sub-double v40, v40, v47

    mul-double v35, v35, v40

    sub-double v1, v1, v35

    double-to-float v2, v1

    .line 177
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1, v0, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;

    move-result-object v35

    .line 178
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_39

    .line 180
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    if-ne v10, v0, :cond_38

    .line 181
    invoke-direct {v12, v10}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v0

    add-int/2addr v0, v9

    int-to-float v1, v0

    int-to-float v0, v9

    move/from16 v36, v0

    move-object/from16 v0, p1

    move/from16 v37, v2

    move/from16 v40, v3

    move/from16 v3, v36

    move/from16 v41, v4

    move/from16 v4, v37

    move-object/from16 v42, v5

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sub-float v4, v37, v28

    .line 182
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v44

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    move/from16 v48, v13

    const/4 v13, 0x1

    move/from16 v6, v44

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto :goto_22

    :cond_38
    move/from16 v37, v2

    move/from16 v40, v3

    move/from16 v41, v4

    move-object/from16 v42, v5

    move/from16 v48, v13

    const/4 v13, 0x1

    int-to-float v6, v8

    .line 183
    invoke-direct {v12, v10}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v0

    add-int v3, v8, v0

    int-to-float v3, v3

    move-object/from16 v0, p1

    move v1, v6

    move/from16 v4, v37

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sub-float v4, v37, v28

    .line 184
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v36

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    move v3, v6

    move/from16 v6, v36

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    :goto_22
    if-eqz v43, :cond_3b

    .line 185
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v9

    int-to-float v3, v8

    move-object/from16 v0, p1

    move/from16 v2, v37

    move/from16 v4, v37

    move-object/from16 v5, p7

    .line 186
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_23

    :cond_39
    move/from16 v37, v2

    move/from16 v40, v3

    move/from16 v41, v4

    move-object/from16 v42, v5

    move/from16 v48, v13

    const/4 v13, 0x1

    .line 187
    invoke-direct {v12, v10}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabelLinePos(Landroid/graphics/Paint$Align;)I

    move-result v0

    sub-int v3, v8, v0

    int-to-float v1, v3

    int-to-float v6, v8

    move-object/from16 v0, p1

    move v3, v6

    move/from16 v4, v37

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v3, v8, 0xa

    int-to-float v3, v3

    sub-float v4, v37, v28

    .line 188
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYLabelsAngle()F

    move-result v36

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    move/from16 v35, v6

    move/from16 v6, v36

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    if-eqz v43, :cond_3b

    .line 189
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, v9

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v37

    move/from16 v4, v37

    move-object/from16 v5, p7

    .line 190
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_23

    :cond_3a
    move/from16 v40, v3

    move/from16 v41, v4

    move-object/from16 v42, v5

    move/from16 v48, v13

    const/4 v13, 0x1

    :cond_3b
    :goto_23
    add-int/lit8 v3, v40, 0x1

    move/from16 v4, v41

    move-object/from16 v5, v42

    move/from16 v13, v48

    const/4 v6, 0x1

    goto/16 :goto_21

    :cond_3c
    move/from16 v48, v13

    const/4 v13, 0x1

    add-int/lit8 v7, v7, 0x1

    move/from16 v13, v48

    const/4 v6, 0x1

    goto/16 :goto_20

    :cond_3d
    move/from16 v48, v13

    const/4 v13, 0x1

    if-eqz v38, :cond_48

    .line 191
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v0

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 193
    invoke-virtual {v14, v13}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 194
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_46

    .line 195
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowChartTitle()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 196
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitleTextSize()F

    move-result v0

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v0, v0, v1

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    move/from16 v10, p5

    move/from16 v7, p6

    .line 197
    invoke-virtual {v12, v10, v7}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getMaxTitleAreaSize(II)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    .line 198
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitle()Ljava/lang/String;

    move-result-object v2

    div-int/lit8 v1, v10, 0x2

    move/from16 v6, p3

    add-int/2addr v1, v6

    int-to-float v4, v1

    move/from16 v5, p4

    move/from16 v1, v27

    int-to-float v13, v5

    iget-object v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitleTextSize()F

    move-result v3

    move/from16 v22, v1

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v3, v3, v1

    mul-float v3, v3, v28

    add-float/2addr v13, v3

    iget v1, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-float v3, v1

    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-float v1, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v65, v22

    move/from16 v22, v1

    move-object/from16 v1, p1

    move/from16 v28, v3

    const/high16 v3, 0x3f800000    # 1.0f

    move v5, v13

    move v13, v6

    move/from16 v6, v28

    move/from16 v7, v22

    move/from16 v66, v8

    move-object/from16 v8, p7

    move/from16 v60, v11

    move v11, v9

    move/from16 v9, v27

    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawTitle(Landroid/graphics/Canvas;Ljava/lang/String;FFFFFLandroid/graphics/Paint;F)V

    goto :goto_24

    :cond_3e
    move/from16 v13, p3

    move/from16 v10, p5

    move/from16 v66, v8

    move/from16 v60, v11

    move/from16 v65, v27

    move v11, v9

    :goto_24
    const v22, 0x3e4ccccd    # 0.2f

    const v27, 0x3f4ccccd    # 0.8f

    move-object/from16 v9, v59

    if-eqz v9, :cond_40

    .line 199
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitleTextSize()F

    move-result v0

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v0, v0, v1

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    mul-float v6, v23, v27

    mul-float v7, v33, v22

    move-object/from16 v8, v63

    if-eqz v8, :cond_3f

    .line 200
    iget v0, v9, Lcom/intsig/office/java/awt/Rectangle;->height:I

    float-to-int v1, v6

    if-ne v0, v1, :cond_3f

    .line 201
    iget v0, v8, Lcom/intsig/office/java/awt/Rectangle;->height:I

    add-int v0, p4, v0

    move/from16 v5, p6

    div-int/lit8 v1, v5, 0x2

    add-int/2addr v0, v1

    goto :goto_25

    :cond_3f
    move/from16 v5, p6

    .line 202
    div-int/lit8 v0, v5, 0x2

    add-int v0, p4, v0

    :goto_25
    int-to-float v0, v0

    move/from16 v28, v0

    .line 203
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitle()Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v0, v13

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 204
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitleTextSize()F

    move-result v1

    iget-object v4, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v4

    mul-float v1, v1, v4

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v1, v1, v4

    add-float v4, v0, v1

    const/high16 v35, -0x3d4c0000    # -90.0f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v5, v28

    move-object/from16 v67, v8

    move-object/from16 v8, p7

    move/from16 v64, v11

    move-object v11, v9

    move/from16 v9, v35

    .line 205
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawTitle(Landroid/graphics/Canvas;Ljava/lang/String;FFFFFLandroid/graphics/Paint;F)V

    goto :goto_26

    :cond_40
    move/from16 v64, v11

    move-object/from16 v67, v63

    move-object v11, v9

    :goto_26
    move-object/from16 v0, v62

    if-eqz v0, :cond_45

    mul-float v6, v33, v27

    mul-float v7, v23, v22

    .line 206
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitleTextSize()F

    move-result v1

    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v2

    mul-float v1, v1, v2

    invoke-virtual {v14, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 207
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    .line 208
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    move/from16 v9, v61

    sub-int v2, v9, v0

    int-to-float v2, v2

    if-eqz v11, :cond_41

    .line 209
    iget v3, v11, Lcom/intsig/office/java/awt/Rectangle;->width:I

    add-int/2addr v3, v10

    const/4 v11, 0x2

    div-int/2addr v3, v11

    goto :goto_27

    :cond_41
    const/4 v11, 0x2

    .line 210
    div-int/lit8 v3, v10, 0x2

    :goto_27
    add-int/2addr v3, v13

    int-to-float v3, v3

    move v4, v3

    move-object/from16 v8, v57

    if-eqz v8, :cond_43

    .line 211
    iget-byte v3, v12, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    const/4 v5, 0x1

    if-eq v3, v5, :cond_42

    const/4 v5, 0x3

    if-ne v3, v5, :cond_44

    goto :goto_28

    :cond_42
    const/4 v5, 0x3

    .line 212
    :goto_28
    iget v2, v8, Lcom/intsig/office/java/awt/Rectangle;->height:I

    sub-int v2, v9, v2

    sub-int/2addr v2, v0

    int-to-float v2, v2

    goto :goto_29

    :cond_43
    const/4 v5, 0x3

    .line 213
    :cond_44
    :goto_29
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    move-result-object v3

    const/high16 v22, 0x3f800000    # 1.0f

    iget v0, v1, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float v23, v2, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v3

    move/from16 v3, v22

    move/from16 v5, v23

    move-object/from16 v68, v8

    move-object/from16 v8, p7

    move v11, v9

    move/from16 v9, v27

    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawTitle(Landroid/graphics/Canvas;Ljava/lang/String;FFFFFLandroid/graphics/Paint;F)V

    goto/16 :goto_2a

    :cond_45
    move-object/from16 v68, v57

    move/from16 v11, v61

    goto/16 :goto_2a

    :cond_46
    move/from16 v13, p3

    move/from16 v10, p5

    move/from16 v66, v8

    move/from16 v64, v9

    move/from16 v60, v11

    move/from16 v65, v27

    move-object/from16 v68, v57

    move/from16 v11, v61

    move-object/from16 v67, v63

    .line 214
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_47

    .line 215
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTitle()Ljava/lang/String;

    move-result-object v2

    div-int/lit8 v0, v10, 0x2

    add-int/2addr v0, v13

    int-to-float v3, v0

    int-to-float v4, v11

    const/high16 v6, -0x3d4c0000    # -90.0f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 216
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitle()Ljava/lang/String;

    move-result-object v2

    move/from16 v9, v66

    int-to-float v0, v9

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    const/high16 v3, 0x41a00000    # 20.0f

    mul-float v1, v1, v3

    add-float v3, v0, v1

    move/from16 v8, p6

    div-int/lit8 v7, v8, 0x2

    add-int v0, p4, v7

    int-to-float v4, v0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 217
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitleTextSize()F

    move-result v0

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v0, v0, v1

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 218
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitle()Ljava/lang/String;

    move-result-object v2

    int-to-float v3, v13

    move/from16 v6, v29

    add-int/2addr v7, v6

    int-to-float v4, v7

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v69, v6

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    goto :goto_2b

    :cond_47
    :goto_2a
    move/from16 v8, p6

    move/from16 v69, v29

    move/from16 v9, v66

    :goto_2b
    const/4 v0, 0x0

    .line 219
    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    goto :goto_2c

    :cond_48
    move/from16 v13, p3

    move/from16 v10, p5

    move/from16 v64, v9

    move/from16 v60, v11

    move/from16 v65, v27

    move/from16 v69, v29

    move-object/from16 v68, v57

    move/from16 v11, v61

    move-object/from16 v67, v63

    move v9, v8

    move/from16 v8, p6

    :goto_2c
    move/from16 v6, v65

    const/4 v7, 0x0

    :goto_2d
    if-ge v7, v6, :cond_4e

    .line 220
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-virtual {v0, v7}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v5

    .line 221
    invoke-virtual {v5}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getScaleNumber()I

    move-result v22

    .line 222
    invoke-virtual {v5}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getItemCount()I

    move-result v0

    if-nez v0, :cond_49

    move/from16 v37, v6

    move/from16 v27, v7

    move/from16 v61, v11

    move/from16 v10, v60

    move v11, v8

    goto/16 :goto_31

    .line 223
    :cond_49
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    move-result-object v23

    .line 224
    invoke-virtual {v5}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getItemCount()I

    move-result v0

    const/4 v1, 0x2

    mul-int/lit8 v4, v0, 0x2

    .line 225
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_2e
    if-ge v2, v4, :cond_4c

    .line 226
    div-int/lit8 v0, v2, 0x2

    .line 227
    invoke-virtual {v5, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getY(I)D

    move-result-wide v27

    const-wide v35, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v1, v27, v35

    if-eqz v1, :cond_4a

    move/from16 v38, v6

    move/from16 v29, v7

    move/from16 v1, v64

    int-to-double v6, v1

    .line 228
    aget-wide v35, v32, v22

    invoke-virtual {v5, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getX(I)D

    move-result-wide v40

    aget-wide v42, v34, v22

    sub-double v40, v40, v42

    mul-double v35, v35, v40

    add-double v6, v6, v35

    double-to-float v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v7, v60

    int-to-double v0, v7

    .line 229
    aget-wide v35, v24, v22

    aget-wide v40, v19, v22

    sub-double v27, v27, v40

    mul-double v35, v35, v27

    sub-double v0, v0, v35

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2f

    :cond_4a
    move/from16 v38, v6

    move/from16 v29, v7

    move/from16 v7, v60

    .line 230
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4b

    int-to-float v0, v7

    move v6, v2

    int-to-double v1, v7

    .line 231
    aget-wide v27, v24, v22

    aget-wide v35, v19, v22

    mul-double v27, v27, v35

    add-double v1, v1, v27

    double-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v27

    move-object/from16 v0, p0

    move/from16 v2, v64

    move-object v1, v5

    move/from16 v28, v6

    move v6, v2

    move-object/from16 v2, p1

    move-object/from16 v33, v3

    move-object/from16 v3, p7

    move/from16 v35, v4

    move-object/from16 v4, v33

    move-object/from16 v36, v5

    move-object/from16 v5, v23

    move/from16 v61, v11

    move/from16 v37, v38

    move v11, v6

    move/from16 v6, v27

    move v10, v7

    move/from16 v27, v29

    move/from16 v7, v27

    move/from16 v64, v11

    move v11, v8

    move-object v8, v15

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FILcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;)V

    .line 232
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->clear()V

    goto :goto_30

    :cond_4b
    :goto_2f
    move/from16 v28, v2

    move-object/from16 v33, v3

    move/from16 v35, v4

    move-object/from16 v36, v5

    move v10, v7

    move/from16 v61, v11

    move/from16 v27, v29

    move/from16 v37, v38

    move v11, v8

    :goto_30
    add-int/lit8 v2, v28, 0x2

    move/from16 v60, v10

    move v8, v11

    move/from16 v7, v27

    move-object/from16 v3, v33

    move/from16 v4, v35

    move-object/from16 v5, v36

    move/from16 v6, v37

    move/from16 v11, v61

    move/from16 v10, p5

    goto/16 :goto_2e

    :cond_4c
    move-object/from16 v33, v3

    move-object/from16 v36, v5

    move/from16 v37, v6

    move/from16 v27, v7

    move/from16 v61, v11

    move/from16 v10, v60

    move v11, v8

    .line 233
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4d

    int-to-float v0, v10

    int-to-double v1, v10

    .line 234
    aget-wide v3, v24, v22

    aget-wide v5, v19, v22

    mul-double v3, v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v6

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move-object/from16 v3, p7

    move-object/from16 v4, v33

    move-object/from16 v5, v23

    move/from16 v7, v27

    move-object v8, v15

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FILcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;)V

    .line 235
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :cond_4d
    :goto_31
    add-int/lit8 v7, v27, 0x1

    move/from16 v60, v10

    move v8, v11

    move/from16 v6, v37

    move/from16 v11, v61

    move/from16 v10, p5

    goto/16 :goto_2d

    :cond_4e
    move/from16 v61, v11

    move/from16 v10, v60

    move v11, v8

    .line 236
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    sub-int v6, v11, v10

    const/4 v8, 0x1

    .line 237
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v22

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move/from16 v3, p3

    move v4, v10

    move/from16 v5, p5

    move-object/from16 v7, p7

    move/from16 v56, v9

    move/from16 v9, v22

    .line 238
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 239
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    const/4 v0, 0x0

    aget-wide v2, v18, v0

    mul-double v2, v2, v30

    double-to-int v6, v2

    .line 240
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v9

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p4

    .line 241
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 242
    sget-object v9, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v9, :cond_4f

    .line 243
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    sub-int v5, v64, v13

    sub-int v22, v11, p4

    const/4 v8, 0x1

    .line 244
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v23

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v6, v22

    move-object/from16 v7, p7

    move/from16 v60, v10

    move-object v10, v9

    move/from16 v9, v23

    .line 245
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 246
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    const/4 v9, 0x3

    aget-wide v2, v18, v9

    mul-double v2, v2, v25

    double-to-int v5, v2

    .line 247
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v18

    move-object/from16 v2, p1

    move/from16 v3, v56

    const/4 v14, 0x3

    move/from16 v9, v18

    .line 248
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    goto :goto_32

    :cond_4f
    move/from16 v60, v10

    const/4 v14, 0x3

    move-object v10, v9

    .line 249
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_50

    .line 250
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    move/from16 v8, v56

    move/from16 v9, v60

    sub-int v5, p5, v8

    sub-int v18, v11, p4

    const/16 v22, 0x1

    .line 251
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v23

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move v3, v8

    move/from16 v4, p4

    move/from16 v6, v18

    move-object/from16 v7, p7

    move/from16 v70, v8

    move/from16 v8, v22

    move/from16 v71, v9

    move/from16 v9, v23

    .line 252
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    .line 253
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    sub-int v5, v64, v13

    const/4 v8, 0x1

    .line 254
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMarginsColor()I

    move-result v9

    move/from16 v3, p3

    .line 255
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V

    goto :goto_33

    :cond_50
    :goto_32
    move/from16 v70, v56

    move/from16 v71, v60

    :goto_33
    if-ne v15, v10, :cond_55

    .line 256
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLegend()Z

    move-result v0

    if-eqz v0, :cond_56

    move-object/from16 v0, v68

    .line 257
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 258
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 259
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendPosition()B

    move-result v0

    if-eqz v0, :cond_53

    const/4 v1, 0x1

    if-eq v0, v1, :cond_51

    const/4 v1, 0x2

    if-eq v0, v1, :cond_53

    if-eq v0, v14, :cond_52

    move/from16 v5, p4

    move v4, v13

    goto :goto_36

    :cond_51
    const/4 v1, 0x2

    :cond_52
    sub-int v0, p5, v6

    .line 260
    div-int/2addr v0, v1

    add-int/2addr v0, v13

    sub-int v1, v61, v7

    :goto_34
    move v4, v0

    move v5, v1

    goto :goto_36

    :cond_53
    sub-int v9, v20, v6

    .line 261
    iget-object v0, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendTextSize()F

    move-result v0

    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    move-result v1

    mul-float v0, v0, v1

    float-to-int v0, v0

    sub-int v0, v9, v0

    move-object/from16 v1, v67

    if-eqz v1, :cond_54

    .line 262
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    add-int/2addr v1, v11

    const/4 v2, 0x2

    div-int/2addr v1, v2

    goto :goto_35

    :cond_54
    const/4 v2, 0x2

    sub-int v1, v11, v7

    .line 263
    div-int/2addr v1, v2

    :goto_35
    add-int v1, p4, v1

    goto :goto_34

    .line 264
    :goto_36
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, v21

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegend(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;IIIILandroid/graphics/Paint;Z)I

    goto :goto_37

    .line 265
    :cond_55
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_56

    move/from16 v10, v55

    int-to-float v14, v10

    move-object/from16 v9, p1

    const/4 v0, 0x1

    .line 266
    invoke-direct {v12, v9, v14, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    .line 267
    iget-object v2, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    const/4 v0, 0x2

    add-int/lit8 v4, v13, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, v21

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object v11, v9

    move v9, v13

    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegend(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;IIIILandroid/graphics/Paint;Z)I

    const/4 v0, 0x0

    .line 268
    invoke-direct {v12, v11, v14, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    goto :goto_38

    :cond_56
    :goto_37
    move-object/from16 v11, p1

    move/from16 v10, v55

    const/4 v0, 0x0

    .line 269
    :goto_38
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowAxes()Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 270
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getAxesColor()I

    move-result v1

    move-object/from16 v6, p7

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x1

    .line 271
    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    move/from16 v2, v71

    int-to-float v7, v2

    .line 272
    aget-wide v1, v19, v0

    cmpg-double v3, v1, v45

    if-gez v3, :cond_57

    .line 273
    aget-wide v1, v24, v0

    :cond_57
    move/from16 v0, v64

    int-to-float v8, v0

    .line 274
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    sub-float v2, v0, v39

    move/from16 v0, v70

    int-to-float v9, v0

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    add-float v4, v0, v39

    move-object/from16 v0, p1

    move v1, v8

    move v3, v9

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move/from16 v0, v48

    const/4 v3, 0x0

    const/4 v13, 0x0

    :goto_39
    if-ge v3, v0, :cond_59

    if-nez v13, :cond_59

    .line 275
    iget-object v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v1, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisAlign(I)Landroid/graphics/Paint$Align;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    if-ne v1, v2, :cond_58

    const/4 v13, 0x1

    goto :goto_3a

    :cond_58
    const/4 v13, 0x0

    :goto_3a
    add-int/lit8 v3, v3, 0x1

    goto :goto_39

    .line 276
    :cond_59
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v0, :cond_5a

    sub-float v1, v8, v39

    move/from16 v0, v69

    int-to-float v14, v0

    add-float v3, v8, v39

    move-object/from16 v0, p1

    move v2, v14

    move v4, v7

    move-object/from16 v5, p7

    .line 277
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    if-eqz v13, :cond_5b

    sub-float v1, v9, v39

    add-float v3, v9, v39

    move-object/from16 v0, p1

    move v2, v14

    move v4, v7

    move-object/from16 v5, p7

    .line 278
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_3b

    :cond_5a
    move/from16 v0, v69

    .line 279
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    if-ne v15, v1, :cond_5b

    sub-float v1, v9, v39

    int-to-float v2, v0

    add-float v3, v9, v39

    move-object/from16 v0, p1

    move v4, v7

    move-object/from16 v5, p7

    .line 280
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_5b
    :goto_3b
    const/4 v0, 0x0

    .line 281
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    goto :goto_3c

    :cond_5c
    move-object/from16 v6, p7

    :goto_3c
    if-eqz v17, :cond_5d

    int-to-float v0, v10

    const/4 v1, 0x1

    .line 282
    invoke-direct {v12, v11, v0, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->transform(Landroid/graphics/Canvas;FZ)V

    :cond_5d
    move/from16 v0, v58

    .line 283
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    move/from16 v0, v16

    .line 284
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 285
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected drawChartValuesText(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;Landroid/graphics/Paint;[FI)V
    .locals 9

    .line 1
    const/4 p5, 0x0

    .line 2
    :goto_0
    array-length v0, p4

    .line 3
    if-ge p5, v0, :cond_0

    .line 4
    .line 5
    div-int/lit8 v0, p5, 0x2

    .line 6
    .line 7
    invoke-virtual {p2, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getY(I)D

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getLabel(D)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    aget v5, p4, p5

    .line 16
    .line 17
    add-int/lit8 v0, p5, 0x1

    .line 18
    .line 19
    aget v0, p4, v0

    .line 20
    .line 21
    const/high16 v1, 0x40600000    # 3.5f

    .line 22
    .line 23
    sub-float v6, v0, v1

    .line 24
    .line 25
    const/4 v8, 0x0

    .line 26
    move-object v2, p0

    .line 27
    move-object v3, p1

    .line 28
    move-object v7, p3

    .line 29
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 30
    .line 31
    .line 32
    add-int/lit8 p5, p5, 0x2

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public abstract drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;[FLcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FI)V
.end method

.method protected drawSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/util/List;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FILcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;",
            "Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;",
            "FI",
            "Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v7, p3

    .line 2
    .line 3
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getStroke()Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;

    .line 4
    .line 5
    .line 6
    move-result-object v8

    .line 7
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeCap()Landroid/graphics/Paint$Cap;

    .line 8
    .line 9
    .line 10
    move-result-object v9

    .line 11
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeJoin()Landroid/graphics/Paint$Join;

    .line 12
    .line 13
    .line 14
    move-result-object v10

    .line 15
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStrokeMiter()F

    .line 16
    .line 17
    .line 18
    move-result v11

    .line 19
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getPathEffect()Landroid/graphics/PathEffect;

    .line 20
    .line 21
    .line 22
    move-result-object v12

    .line 23
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    .line 24
    .line 25
    .line 26
    move-result-object v13

    .line 27
    if-eqz v8, :cond_1

    .line 28
    .line 29
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getIntervals()[F

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    new-instance v0, Landroid/graphics/DashPathEffect;

    .line 36
    .line 37
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getIntervals()[F

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getPhase()F

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v0, 0x0

    .line 50
    :goto_0
    move-object v5, v0

    .line 51
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getCap()Landroid/graphics/Paint$Cap;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getJoin()Landroid/graphics/Paint$Join;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/BasicStroke;->getMiter()F

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    .line 64
    .line 65
    move-object/from16 v0, p0

    .line 66
    .line 67
    move-object/from16 v6, p3

    .line 68
    .line 69
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V

    .line 70
    .line 71
    .line 72
    :cond_1
    invoke-static/range {p4 .. p4}, Lcom/intsig/office/thirdpart/achartengine/util/MathHelper;->getFloats(Ljava/util/List;)[F

    .line 73
    .line 74
    .line 75
    move-result-object v14

    .line 76
    move-object/from16 v0, p0

    .line 77
    .line 78
    move-object/from16 v1, p2

    .line 79
    .line 80
    move-object/from16 v2, p3

    .line 81
    .line 82
    move-object v3, v14

    .line 83
    move-object/from16 v4, p5

    .line 84
    .line 85
    move/from16 v5, p6

    .line 86
    .line 87
    move/from16 v6, p7

    .line 88
    .line 89
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;[FLcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FI)V

    .line 90
    .line 91
    .line 92
    move-object/from16 v15, p0

    .line 93
    .line 94
    move-object/from16 v6, p5

    .line 95
    .line 96
    invoke-virtual {v15, v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->isRenderPoints(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_2

    .line 101
    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getPointsChart()Lcom/intsig/office/thirdpart/achartengine/chart/ScatterChart;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    if-eqz v0, :cond_2

    .line 107
    .line 108
    move-object/from16 v1, p2

    .line 109
    .line 110
    move-object/from16 v2, p3

    .line 111
    .line 112
    move-object v3, v14

    .line 113
    move-object/from16 v4, p5

    .line 114
    .line 115
    move/from16 v5, p6

    .line 116
    .line 117
    move/from16 v6, p7

    .line 118
    .line 119
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/ScatterChart;->drawSeries(Landroid/graphics/Canvas;Landroid/graphics/Paint;[FLcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FI)V

    .line 120
    .line 121
    .line 122
    :cond_2
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getChartValuesTextSize()F

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 127
    .line 128
    .line 129
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 130
    .line 131
    move-object/from16 v1, p8

    .line 132
    .line 133
    if-ne v1, v0, :cond_3

    .line 134
    .line 135
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 136
    .line 137
    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 138
    .line 139
    .line 140
    goto :goto_1

    .line 141
    :cond_3
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 142
    .line 143
    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 144
    .line 145
    .line 146
    :goto_1
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->isDisplayChartValues()Z

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-eqz v0, :cond_4

    .line 151
    .line 152
    move-object/from16 v0, p0

    .line 153
    .line 154
    move-object/from16 v1, p2

    .line 155
    .line 156
    move-object/from16 v2, p1

    .line 157
    .line 158
    move-object/from16 v3, p3

    .line 159
    .line 160
    move-object v4, v14

    .line 161
    move/from16 v5, p7

    .line 162
    .line 163
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawChartValuesText(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;Landroid/graphics/Paint;[FI)V

    .line 164
    .line 165
    .line 166
    :cond_4
    if-eqz v8, :cond_5

    .line 167
    .line 168
    move-object/from16 v0, p0

    .line 169
    .line 170
    move-object v1, v9

    .line 171
    move-object v2, v10

    .line 172
    move v3, v11

    .line 173
    move-object v4, v13

    .line 174
    move-object v5, v12

    .line 175
    move-object/from16 v6, p3

    .line 176
    .line 177
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->setStroke(Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLandroid/graphics/Paint$Style;Landroid/graphics/PathEffect;Landroid/graphics/Paint;)V

    .line 178
    .line 179
    .line 180
    :cond_5
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
.end method

.method protected drawSeriesBackgroundAndFrame(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .locals 11

    .line 1
    invoke-virtual {p4}, Landroid/graphics/Paint;->getAlpha()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v10, Landroid/graphics/Path;

    .line 6
    .line 7
    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    .line 8
    .line 9
    .line 10
    iget v1, p3, Landroid/graphics/Rect;->left:I

    .line 11
    .line 12
    int-to-float v2, v1

    .line 13
    iget v1, p3, Landroid/graphics/Rect;->top:I

    .line 14
    .line 15
    int-to-float v3, v1

    .line 16
    iget v1, p3, Landroid/graphics/Rect;->right:I

    .line 17
    .line 18
    int-to-float v4, v1

    .line 19
    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    .line 20
    .line 21
    int-to-float v5, v1

    .line 22
    sget-object v6, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 23
    .line 24
    move-object v1, v10

    .line 25
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getSeriesBackgroundColor()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    if-eqz v4, :cond_0

    .line 33
    .line 34
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 35
    .line 36
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    .line 38
    .line 39
    const/4 v2, 0x0

    .line 40
    const/4 v3, 0x1

    .line 41
    const/4 v6, 0x0

    .line 42
    const/high16 v7, 0x3f800000    # 1.0f

    .line 43
    .line 44
    move-object v1, p2

    .line 45
    move-object v5, p3

    .line 46
    move-object v8, v10

    .line 47
    move-object v9, p4

    .line 48
    invoke-static/range {v1 .. v9}, Lcom/intsig/office/common/BackgroundDrawer;->drawPathBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 52
    .line 53
    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getSeriesFrame()Lcom/intsig/office/common/borders/Line;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    if-eqz p1, :cond_2

    .line 59
    .line 60
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 61
    .line 62
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    .line 64
    .line 65
    const/high16 v1, 0x40000000    # 2.0f

    .line 66
    .line 67
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/office/common/borders/Line;->isDash()Z

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    if-eqz v1, :cond_1

    .line 75
    .line 76
    new-instance v1, Landroid/graphics/DashPathEffect;

    .line 77
    .line 78
    const/4 v2, 0x2

    .line 79
    new-array v2, v2, [F

    .line 80
    .line 81
    fill-array-data v2, :array_0

    .line 82
    .line 83
    .line 84
    const/high16 v3, 0x41200000    # 10.0f

    .line 85
    .line 86
    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p4, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 90
    .line 91
    .line 92
    :cond_1
    const/4 v2, 0x0

    .line 93
    const/4 v3, 0x1

    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 95
    .line 96
    .line 97
    move-result-object v4

    .line 98
    const/4 v6, 0x0

    .line 99
    const/high16 v7, 0x3f800000    # 1.0f

    .line 100
    .line 101
    move-object v1, p2

    .line 102
    move-object v5, p3

    .line 103
    move-object v8, v10

    .line 104
    move-object v9, p4

    .line 105
    invoke-static/range {v1 .. v9}, Lcom/intsig/office/common/BackgroundDrawer;->drawPathBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 106
    .line 107
    .line 108
    sget-object p1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 109
    .line 110
    invoke-virtual {p4, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 114
    .line 115
    .line 116
    :cond_2
    invoke-virtual {p4}, Landroid/graphics/Paint;->reset()V

    .line 117
    .line 118
    .line 119
    const/4 p1, 0x1

    .line 120
    invoke-virtual {p4, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 121
    .line 122
    .line 123
    return-void

    .line 124
    nop

    .line 125
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method protected drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getOrientation()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->getAngle()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    neg-int v0, v0

    .line 12
    int-to-float v0, v0

    .line 13
    add-float/2addr v0, p6

    .line 14
    const/4 p6, 0x0

    .line 15
    cmpl-float p6, v0, p6

    .line 16
    .line 17
    if-eqz p6, :cond_0

    .line 18
    .line 19
    invoke-virtual {p1, v0, p3, p4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 23
    .line 24
    .line 25
    if-eqz p6, :cond_1

    .line 26
    .line 27
    neg-float p2, v0

    .line 28
    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method protected drawXLabels(Ljava/util/List;[Ljava/lang/Double;Landroid/graphics/Canvas;Landroid/graphics/Paint;IIFDD)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;[",
            "Ljava/lang/Double;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "IIFDD)V"
        }
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p2

    .line 4
    .line 5
    move-object/from16 v9, p4

    .line 6
    .line 7
    move/from16 v10, p5

    .line 8
    .line 9
    move/from16 v11, p6

    .line 10
    .line 11
    move-wide/from16 v12, p8

    .line 12
    .line 13
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v6

    .line 17
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLabels()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iget-object v1, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowGridV()Z

    .line 26
    .line 27
    .line 28
    move-result v14

    .line 29
    iget-object v1, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowCustomTextGrid()Z

    .line 32
    .line 33
    .line 34
    move-result v15

    .line 35
    iget-object v1, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const/high16 v16, 0x40000000    # 2.0f

    .line 42
    .line 43
    div-float v1, v1, v16

    .line 44
    .line 45
    const/high16 v2, 0x3f000000    # 0.5f

    .line 46
    .line 47
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    .line 48
    .line 49
    .line 50
    move-result v17

    .line 51
    const/high16 v18, 0x40800000    # 4.0f

    .line 52
    .line 53
    const/4 v1, 0x0

    .line 54
    if-eqz v8, :cond_6

    .line 55
    .line 56
    array-length v2, v8

    .line 57
    if-nez v2, :cond_0

    .line 58
    .line 59
    goto/16 :goto_4

    .line 60
    .line 61
    :cond_0
    if-eqz v0, :cond_9

    .line 62
    .line 63
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    .line 71
    .line 72
    array-length v0, v8

    .line 73
    const-wide/16 v19, 0x0

    .line 74
    .line 75
    const/4 v2, 0x1

    .line 76
    if-le v0, v2, :cond_2

    .line 77
    .line 78
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 79
    .line 80
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXFormatCode()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    const-string v3, "yyyy/m/d"

    .line 85
    .line 86
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    if-eqz v0, :cond_2

    .line 91
    .line 92
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getMaxXTextLabelPos()D

    .line 95
    .line 96
    .line 97
    move-result-wide v3

    .line 98
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 99
    .line 100
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 101
    .line 102
    .line 103
    move-result-object v3

    .line 104
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabel(Ljava/lang/Double;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    new-instance v3, Landroid/graphics/Rect;

    .line 109
    .line 110
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    invoke-virtual {v9, v0, v1, v4, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    .line 121
    .line 122
    .line 123
    move-result v4

    .line 124
    int-to-double v4, v4

    .line 125
    cmpl-double v6, v4, v12

    .line 126
    .line 127
    if-lez v6, :cond_2

    .line 128
    .line 129
    iget-object v4, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 130
    .line 131
    const/high16 v5, -0x3dcc0000    # -45.0f

    .line 132
    .line 133
    invoke-virtual {v4, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXLabelsAngle(F)V

    .line 134
    .line 135
    .line 136
    iget-object v4, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 137
    .line 138
    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    .line 139
    .line 140
    .line 141
    move-result v4

    .line 142
    const v5, 0x3f4ccccd    # 0.8f

    .line 143
    .line 144
    .line 145
    mul-float v4, v4, v5

    .line 146
    .line 147
    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 151
    .line 152
    .line 153
    move-result v4

    .line 154
    invoke-virtual {v9, v0, v1, v4, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    int-to-double v4, v0

    .line 162
    const-wide/high16 v1, 0x4000000000000000L    # 2.0

    .line 163
    .line 164
    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->pow(DD)D

    .line 165
    .line 166
    .line 167
    move-result-wide v4

    .line 168
    mul-double v4, v4, v1

    .line 169
    .line 170
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    .line 171
    .line 172
    .line 173
    move-result-wide v4

    .line 174
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    move/from16 v21, v14

    .line 179
    .line 180
    move/from16 v22, v15

    .line 181
    .line 182
    int-to-double v14, v3

    .line 183
    invoke-static {v14, v15, v1, v2}, Ljava/lang/Math;->pow(DD)D

    .line 184
    .line 185
    .line 186
    move-result-wide v14

    .line 187
    div-double/2addr v14, v1

    .line 188
    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    .line 189
    .line 190
    .line 191
    move-result-wide v14

    .line 192
    div-double/2addr v14, v1

    .line 193
    double-to-float v1, v14

    .line 194
    cmpl-double v2, v4, v12

    .line 195
    .line 196
    if-lez v2, :cond_1

    .line 197
    .line 198
    cmpl-double v2, v12, v19

    .line 199
    .line 200
    if-lez v2, :cond_1

    .line 201
    .line 202
    div-double/2addr v4, v12

    .line 203
    double-to-int v2, v4

    .line 204
    const/4 v3, 0x1

    .line 205
    add-int/2addr v2, v3

    .line 206
    move v14, v1

    .line 207
    move v15, v2

    .line 208
    goto :goto_1

    .line 209
    :cond_1
    const/4 v3, 0x1

    .line 210
    move v14, v1

    .line 211
    goto :goto_0

    .line 212
    :cond_2
    move/from16 v21, v14

    .line 213
    .line 214
    move/from16 v22, v15

    .line 215
    .line 216
    const/4 v3, 0x1

    .line 217
    const/4 v1, 0x0

    .line 218
    const/4 v14, 0x0

    .line 219
    :goto_0
    const/4 v15, 0x1

    .line 220
    :goto_1
    array-length v6, v8

    .line 221
    const/4 v5, 0x0

    .line 222
    :goto_2
    if-ge v5, v6, :cond_9

    .line 223
    .line 224
    aget-object v4, v8, v5

    .line 225
    .line 226
    int-to-double v0, v10

    .line 227
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    .line 228
    .line 229
    .line 230
    move-result-wide v2

    .line 231
    sub-double v2, v2, p10

    .line 232
    .line 233
    mul-double v2, v2, v12

    .line 234
    .line 235
    add-double/2addr v0, v2

    .line 236
    double-to-float v3, v0

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    .line 238
    .line 239
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .line 241
    .line 242
    const-string v1, "drawXLabels xLabel:"

    .line 243
    .line 244
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    .line 246
    .line 247
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    const-string v1, ",xPixelsPerUnit:"

    .line 251
    .line 252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    .line 254
    .line 255
    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 256
    .line 257
    .line 258
    const-string v1, ",location:"

    .line 259
    .line 260
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    .line 262
    .line 263
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 264
    .line 265
    .line 266
    const-string v1, ",mZoomRate:"

    .line 267
    .line 268
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .line 270
    .line 271
    iget-object v1, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 272
    .line 273
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 274
    .line 275
    .line 276
    move-result v1

    .line 277
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object v0

    .line 284
    const-string v1, "XYChart"

    .line 285
    .line 286
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .line 288
    .line 289
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 290
    .line 291
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    .line 292
    .line 293
    .line 294
    move-result v0

    .line 295
    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 296
    .line 297
    .line 298
    if-eqz v21, :cond_3

    .line 299
    .line 300
    double-to-float v0, v12

    .line 301
    div-float v0, v0, v16

    .line 302
    .line 303
    add-float/2addr v0, v3

    .line 304
    sub-float v1, v0, v17

    .line 305
    .line 306
    int-to-float v2, v11

    .line 307
    add-float v23, v0, v17

    .line 308
    .line 309
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 310
    .line 311
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 312
    .line 313
    .line 314
    move-result v0

    .line 315
    mul-float v0, v0, v18

    .line 316
    .line 317
    add-float v24, p7, v0

    .line 318
    .line 319
    move-object/from16 v0, p3

    .line 320
    .line 321
    move/from16 v25, v3

    .line 322
    .line 323
    move/from16 v3, v23

    .line 324
    .line 325
    move/from16 v23, v6

    .line 326
    .line 327
    move-object v6, v4

    .line 328
    move/from16 v4, v24

    .line 329
    .line 330
    move/from16 v24, v5

    .line 331
    .line 332
    move-object/from16 v5, p4

    .line 333
    .line 334
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 335
    .line 336
    .line 337
    goto :goto_3

    .line 338
    :cond_3
    move/from16 v25, v3

    .line 339
    .line 340
    move/from16 v24, v5

    .line 341
    .line 342
    move/from16 v23, v6

    .line 343
    .line 344
    move-object v6, v4

    .line 345
    double-to-float v0, v12

    .line 346
    div-float v0, v0, v16

    .line 347
    .line 348
    add-float v3, v25, v0

    .line 349
    .line 350
    sub-float v1, v3, v17

    .line 351
    .line 352
    add-float v3, v3, v17

    .line 353
    .line 354
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 355
    .line 356
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 357
    .line 358
    .line 359
    move-result v0

    .line 360
    mul-float v0, v0, v18

    .line 361
    .line 362
    add-float v4, p7, v0

    .line 363
    .line 364
    move-object/from16 v0, p3

    .line 365
    .line 366
    move/from16 v2, p7

    .line 367
    .line 368
    move-object/from16 v5, p4

    .line 369
    .line 370
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 371
    .line 372
    .line 373
    :goto_3
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 374
    .line 375
    invoke-virtual {v0, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXTextLabel(Ljava/lang/Double;)Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v2

    .line 379
    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    .line 380
    .line 381
    .line 382
    move-result-wide v0

    .line 383
    int-to-double v3, v15

    .line 384
    rem-double/2addr v0, v3

    .line 385
    cmpl-double v3, v0, v19

    .line 386
    .line 387
    if-nez v3, :cond_4

    .line 388
    .line 389
    add-float v0, p7, v14

    .line 390
    .line 391
    iget-object v1, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 392
    .line 393
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    .line 394
    .line 395
    .line 396
    move-result v1

    .line 397
    iget-object v3, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 398
    .line 399
    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 400
    .line 401
    .line 402
    move-result v3

    .line 403
    mul-float v1, v1, v3

    .line 404
    .line 405
    add-float v4, v0, v1

    .line 406
    .line 407
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 408
    .line 409
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXLabelsAngle()F

    .line 410
    .line 411
    .line 412
    move-result v6

    .line 413
    move-object/from16 v0, p0

    .line 414
    .line 415
    move-object/from16 v1, p3

    .line 416
    .line 417
    move/from16 v3, v25

    .line 418
    .line 419
    move-object/from16 v5, p4

    .line 420
    .line 421
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/graphics/Paint;F)V

    .line 422
    .line 423
    .line 424
    :cond_4
    if-eqz v22, :cond_5

    .line 425
    .line 426
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 427
    .line 428
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    .line 429
    .line 430
    .line 431
    move-result v0

    .line 432
    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 433
    .line 434
    .line 435
    double-to-float v0, v12

    .line 436
    div-float v0, v0, v16

    .line 437
    .line 438
    add-float v3, v25, v0

    .line 439
    .line 440
    sub-float v1, v3, v17

    .line 441
    .line 442
    add-float v3, v3, v17

    .line 443
    .line 444
    int-to-float v4, v11

    .line 445
    move-object/from16 v0, p3

    .line 446
    .line 447
    move/from16 v2, p7

    .line 448
    .line 449
    move-object/from16 v5, p4

    .line 450
    .line 451
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 452
    .line 453
    .line 454
    :cond_5
    add-int/lit8 v5, v24, 0x1

    .line 455
    .line 456
    move/from16 v6, v23

    .line 457
    .line 458
    goto/16 :goto_2

    .line 459
    .line 460
    :cond_6
    :goto_4
    move/from16 v21, v14

    .line 461
    .line 462
    move/from16 v22, v15

    .line 463
    .line 464
    const/4 v8, 0x0

    .line 465
    :goto_5
    if-ge v8, v6, :cond_9

    .line 466
    .line 467
    move-object/from16 v14, p1

    .line 468
    .line 469
    invoke-interface {v14, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 470
    .line 471
    .line 472
    move-result-object v0

    .line 473
    check-cast v0, Ljava/lang/Double;

    .line 474
    .line 475
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    .line 476
    .line 477
    .line 478
    move-result-wide v0

    .line 479
    int-to-double v2, v10

    .line 480
    sub-double v0, v0, p10

    .line 481
    .line 482
    mul-double v0, v0, v12

    .line 483
    .line 484
    add-double/2addr v2, v0

    .line 485
    double-to-float v15, v2

    .line 486
    if-eqz v21, :cond_7

    .line 487
    .line 488
    sub-float v1, v15, v17

    .line 489
    .line 490
    int-to-float v2, v11

    .line 491
    add-float v3, v15, v17

    .line 492
    .line 493
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 494
    .line 495
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 496
    .line 497
    .line 498
    move-result v0

    .line 499
    mul-float v0, v0, v18

    .line 500
    .line 501
    add-float v4, p7, v0

    .line 502
    .line 503
    move-object/from16 v0, p3

    .line 504
    .line 505
    move-object/from16 v5, p4

    .line 506
    .line 507
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 508
    .line 509
    .line 510
    goto :goto_6

    .line 511
    :cond_7
    sub-float v1, v15, v17

    .line 512
    .line 513
    add-float v3, v15, v17

    .line 514
    .line 515
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 516
    .line 517
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 518
    .line 519
    .line 520
    move-result v0

    .line 521
    mul-float v0, v0, v18

    .line 522
    .line 523
    add-float v4, p7, v0

    .line 524
    .line 525
    move-object/from16 v0, p3

    .line 526
    .line 527
    move/from16 v2, p7

    .line 528
    .line 529
    move-object/from16 v5, p4

    .line 530
    .line 531
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 532
    .line 533
    .line 534
    :goto_6
    if-eqz v22, :cond_8

    .line 535
    .line 536
    iget-object v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 537
    .line 538
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getGridColor()I

    .line 539
    .line 540
    .line 541
    move-result v0

    .line 542
    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 543
    .line 544
    .line 545
    double-to-float v0, v12

    .line 546
    div-float v0, v0, v16

    .line 547
    .line 548
    add-float/2addr v15, v0

    .line 549
    sub-float v1, v15, v17

    .line 550
    .line 551
    add-float v3, v15, v17

    .line 552
    .line 553
    int-to-float v4, v11

    .line 554
    move-object/from16 v0, p3

    .line 555
    .line 556
    move/from16 v2, p7

    .line 557
    .line 558
    move-object/from16 v5, p4

    .line 559
    .line 560
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 561
    .line 562
    .line 563
    :cond_8
    add-int/lit8 v8, v8, 0x1

    .line 564
    .line 565
    goto :goto_5

    .line 566
    :cond_9
    return-void
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public getCalcRange(I)[D
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, [D

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract getChartType()Ljava/lang/String;
.end method

.method public getDataset()Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDefaultMinimum()D
    .locals 2

    .line 1
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getLabel(D)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    const-string v2, ""

    cmpl-double v3, p1, v0

    if-nez v3, :cond_0

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method protected getLabel(DLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getLabel label:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", formatCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "XYChart"

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "0.00%"

    .line 5
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    mul-double p1, p1, v0

    double-to-float p1, p1

    invoke-static {p1}, Lcom/intsig/utils/DecimalFormatUtil;->〇o00〇〇Oo(F)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "%"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "yyyy/m/d"

    .line 7
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8
    invoke-static {p1, p2}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(D)Ljava/util/Date;

    move-result-object p1

    .line 9
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    move-result-object p2

    invoke-virtual {p2, p3, p1}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 10
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    const-string p3, ""

    cmpl-double v2, p1, v0

    if-nez v2, :cond_2

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 12
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getPointsChart()Lcom/intsig/office/thirdpart/achartengine/chart/ScatterChart;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getScreenR()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoomRate()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isRenderPoints(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCalcRange([DI)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mCalcRange:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected setDatasetRenderer(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mDataset:Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected setScreenR(Landroid/graphics/Rect;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoomRate(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setZoomRate(F)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toRealPoint(FF)[D
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->toRealPoint(FFI)[D

    move-result-object p1

    return-object p1
.end method

.method public toRealPoint(FFI)[D
    .locals 11

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v0

    .line 3
    iget-object v2, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v2

    .line 4
    iget-object v4, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v4

    .line 5
    iget-object v6, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v6, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v6

    const/4 p3, 0x2

    new-array p3, p3, [D

    .line 6
    iget-object v8, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v9, v8, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    sub-float/2addr p1, v9

    float-to-double v9, p1

    sub-double/2addr v2, v0

    mul-double v9, v9, v2

    .line 7
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result p1

    int-to-double v2, p1

    div-double/2addr v9, v2

    add-double/2addr v9, v0

    const/4 p1, 0x0

    aput-wide v9, p3, p1

    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 8
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    add-int/2addr v0, p1

    int-to-float p1, v0

    sub-float/2addr p1, p2

    float-to-double p1, p1

    sub-double/2addr v6, v4

    mul-double p1, p1, v6

    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr p1, v0

    add-double/2addr p1, v4

    const/4 v0, 0x1

    aput-wide p1, p3, v0

    return-object p3
.end method

.method public toScreenPoint([D)[D
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->toScreenPoint([DI)[D

    move-result-object p1

    return-object p1
.end method

.method public toScreenPoint([DI)[D
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 2
    iget-object v2, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v2, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v2

    .line 3
    iget-object v4, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v4, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v4

    .line 4
    iget-object v6, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v6, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v6

    .line 5
    iget-object v8, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v8, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v8

    .line 6
    iget-object v10, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v10, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v10

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v13, 0x0

    if-eqz v10, :cond_0

    iget-object v10, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v10, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v10, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-virtual {v10, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 7
    :cond_0
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getCalcRange(I)[D

    move-result-object v1

    .line 8
    aget-wide v2, v1, v13

    .line 9
    aget-wide v4, v1, v12

    .line 10
    aget-wide v6, v1, v11

    const/4 v8, 0x3

    .line 11
    aget-wide v8, v1, v8

    :cond_1
    new-array v1, v11, [D

    .line 12
    aget-wide v10, p1, v13

    sub-double/2addr v10, v2

    iget-object v14, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    .line 13
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-double v14, v14

    mul-double v10, v10, v14

    sub-double/2addr v4, v2

    div-double/2addr v10, v4

    iget-object v2, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-double v3, v3

    add-double/2addr v10, v3

    aput-wide v10, v1, v13

    aget-wide v3, p1, v12

    sub-double v3, v8, v3

    .line 14
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-double v10, v2

    mul-double v3, v3, v10

    sub-double/2addr v8, v6

    div-double/2addr v3, v8

    iget-object v2, v0, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->mScreenR:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-double v5, v2

    add-double/2addr v3, v5

    aput-wide v3, v1, v12

    return-object v1
.end method

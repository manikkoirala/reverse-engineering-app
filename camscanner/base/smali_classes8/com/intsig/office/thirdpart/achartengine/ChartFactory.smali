.class public Lcom/intsig/office/thirdpart/achartengine/ChartFactory;
.super Ljava/lang/Object;
.source "ChartFactory.java"


# static fields
.field public static final CHART:Ljava/lang/String; = "chart"

.field public static final TITLE:Ljava/lang/String; = "title"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static checkMultipleSeriesItems(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;I)Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;->getCategoriesCount()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    :goto_0
    if-ge v2, p1, :cond_1

    .line 10
    .line 11
    if-eqz v3, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;->getValues(I)[D

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    array-length v3, v3

    .line 18
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;->getTitles(I)[Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    array-length v4, v4

    .line 23
    if-ne v3, v4, :cond_0

    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    goto :goto_1

    .line 27
    :cond_0
    const/4 v3, 0x0

    .line 28
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    return v3
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->getItemCount()I

    move-result p0

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    move-result p1

    if-ne p0, p1, :cond_0

    return-void

    .line 4
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Dataset and renderer should be not null and the dataset number of items should be equal to the number of series renderers"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    move-result p1

    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkMultipleSeriesItems(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;I)Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    .line 6
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Titles and values should be not null and the dataset number of items should be equal to the number of series renderers"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result p0

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    move-result p1

    if-ne p0, p1, :cond_0

    return-void

    .line 2
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Dataset and renderer should be not null and should have the same number of series"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getBubbleChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/BubbleChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/BubbleChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getColumnBarChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final getCombinedXYChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Ljava/lang/String;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 2

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    array-length v1, p2

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/CombinedXYChart;

    .line 18
    .line 19
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/chart/CombinedXYChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-object v0

    .line 23
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 24
    .line 25
    const-string p1, "Dataset, renderer and types should be not null and the datasets series count should be equal to the types length"

    .line 26
    .line 27
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    throw p0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final getDialChartView(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DialRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/DialChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/DialChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DialRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getDoughnutChartView(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/DoughnutChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/DoughnutChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/MultipleCategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getLineChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/LineChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/LineChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getPieChart(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/PieChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/PieChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getRangeBarChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/RangeBarChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/chart/RangeBarChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final getScatterChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/ScatterChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/ScatterChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final getTimeChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Ljava/lang/String;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->checkParameters(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/chart/TimeChart;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/TimeChart;-><init>(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/chart/TimeChart;->setDateFormat(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

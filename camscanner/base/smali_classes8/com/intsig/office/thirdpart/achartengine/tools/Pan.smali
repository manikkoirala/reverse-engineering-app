.class public Lcom/intsig/office/thirdpart/achartengine/tools/Pan;
.super Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;
.source "Pan.java"


# direct methods
.method public constructor <init>(Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;-><init>(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public apply(FFFF)V
    .locals 27

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    iget-object v0, v6, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getScalesCount()I

    .line 6
    .line 7
    .line 8
    move-result v7

    .line 9
    iget-object v0, v6, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getPanLimits()[D

    .line 12
    .line 13
    .line 14
    move-result-object v8

    .line 15
    const/4 v9, 0x0

    .line 16
    const/4 v10, 0x1

    .line 17
    if-eqz v8, :cond_0

    .line 18
    .line 19
    array-length v0, v8

    .line 20
    const/4 v1, 0x4

    .line 21
    if-ne v0, v1, :cond_0

    .line 22
    .line 23
    const/4 v11, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v11, 0x0

    .line 26
    :goto_0
    iget-object v0, v6, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->mChart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 27
    .line 28
    move-object v12, v0

    .line 29
    check-cast v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 30
    .line 31
    const/4 v13, 0x0

    .line 32
    :goto_1
    if-ge v13, v7, :cond_c

    .line 33
    .line 34
    invoke-virtual {v6, v13}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->getRange(I)[D

    .line 35
    .line 36
    .line 37
    move-result-object v14

    .line 38
    invoke-virtual {v12, v13}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getCalcRange(I)[D

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    aget-wide v1, v14, v9

    .line 43
    .line 44
    aget-wide v3, v14, v10

    .line 45
    .line 46
    cmpl-double v5, v1, v3

    .line 47
    .line 48
    if-nez v5, :cond_1

    .line 49
    .line 50
    aget-wide v1, v0, v9

    .line 51
    .line 52
    aget-wide v3, v0, v10

    .line 53
    .line 54
    cmpl-double v5, v1, v3

    .line 55
    .line 56
    if-eqz v5, :cond_2

    .line 57
    .line 58
    :cond_1
    const/4 v15, 0x2

    .line 59
    aget-wide v1, v14, v15

    .line 60
    .line 61
    const/16 v16, 0x3

    .line 62
    .line 63
    aget-wide v3, v14, v16

    .line 64
    .line 65
    cmpl-double v5, v1, v3

    .line 66
    .line 67
    if-nez v5, :cond_3

    .line 68
    .line 69
    aget-wide v1, v0, v15

    .line 70
    .line 71
    aget-wide v3, v0, v16

    .line 72
    .line 73
    cmpl-double v0, v1, v3

    .line 74
    .line 75
    if-nez v0, :cond_3

    .line 76
    .line 77
    :cond_2
    return-void

    .line 78
    :cond_3
    invoke-virtual {v6, v14, v13}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->checkRange([DI)V

    .line 79
    .line 80
    .line 81
    move/from16 v5, p1

    .line 82
    .line 83
    move/from16 v3, p2

    .line 84
    .line 85
    invoke-virtual {v12, v5, v3}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->toRealPoint(FF)[D

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    move/from16 v4, p3

    .line 90
    .line 91
    move/from16 v1, p4

    .line 92
    .line 93
    invoke-virtual {v12, v4, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->toRealPoint(FF)[D

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    aget-wide v17, v0, v9

    .line 98
    .line 99
    aget-wide v19, v2, v9

    .line 100
    .line 101
    sub-double v17, v17, v19

    .line 102
    .line 103
    aget-wide v19, v0, v10

    .line 104
    .line 105
    aget-wide v21, v2, v10

    .line 106
    .line 107
    sub-double v19, v19, v21

    .line 108
    .line 109
    iget-object v0, v6, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isPanXEnabled()Z

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    if-eqz v0, :cond_7

    .line 116
    .line 117
    if-eqz v11, :cond_6

    .line 118
    .line 119
    aget-wide v21, v8, v9

    .line 120
    .line 121
    aget-wide v23, v14, v9

    .line 122
    .line 123
    add-double v25, v23, v17

    .line 124
    .line 125
    cmpl-double v0, v21, v25

    .line 126
    .line 127
    if-lez v0, :cond_4

    .line 128
    .line 129
    aget-wide v17, v14, v10

    .line 130
    .line 131
    sub-double v17, v17, v23

    .line 132
    .line 133
    add-double v17, v21, v17

    .line 134
    .line 135
    move-object/from16 v0, p0

    .line 136
    .line 137
    move-wide/from16 v1, v21

    .line 138
    .line 139
    move-wide/from16 v3, v17

    .line 140
    .line 141
    move v5, v13

    .line 142
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setXRange(DDI)V

    .line 143
    .line 144
    .line 145
    goto :goto_2

    .line 146
    :cond_4
    aget-wide v3, v8, v10

    .line 147
    .line 148
    aget-wide v0, v14, v10

    .line 149
    .line 150
    add-double v21, v0, v17

    .line 151
    .line 152
    cmpg-double v2, v3, v21

    .line 153
    .line 154
    if-gez v2, :cond_5

    .line 155
    .line 156
    sub-double v0, v0, v23

    .line 157
    .line 158
    sub-double v1, v3, v0

    .line 159
    .line 160
    move-object/from16 v0, p0

    .line 161
    .line 162
    move v5, v13

    .line 163
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setXRange(DDI)V

    .line 164
    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_5
    add-double v2, v23, v17

    .line 168
    .line 169
    add-double v4, v0, v17

    .line 170
    .line 171
    move-object/from16 v0, p0

    .line 172
    .line 173
    move-wide v1, v2

    .line 174
    move-wide v3, v4

    .line 175
    move v5, v13

    .line 176
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setXRange(DDI)V

    .line 177
    .line 178
    .line 179
    goto :goto_2

    .line 180
    :cond_6
    aget-wide v0, v14, v9

    .line 181
    .line 182
    add-double v1, v0, v17

    .line 183
    .line 184
    aget-wide v3, v14, v10

    .line 185
    .line 186
    add-double v3, v3, v17

    .line 187
    .line 188
    move-object/from16 v0, p0

    .line 189
    .line 190
    move v5, v13

    .line 191
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setXRange(DDI)V

    .line 192
    .line 193
    .line 194
    :cond_7
    :goto_2
    iget-object v0, v6, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->mRenderer:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 195
    .line 196
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isPanYEnabled()Z

    .line 197
    .line 198
    .line 199
    move-result v0

    .line 200
    if-eqz v0, :cond_b

    .line 201
    .line 202
    if-eqz v11, :cond_a

    .line 203
    .line 204
    aget-wide v1, v8, v15

    .line 205
    .line 206
    aget-wide v3, v14, v15

    .line 207
    .line 208
    add-double v17, v3, v19

    .line 209
    .line 210
    cmpl-double v0, v1, v17

    .line 211
    .line 212
    if-lez v0, :cond_8

    .line 213
    .line 214
    aget-wide v15, v14, v16

    .line 215
    .line 216
    sub-double/2addr v15, v3

    .line 217
    add-double v3, v1, v15

    .line 218
    .line 219
    move-object/from16 v0, p0

    .line 220
    .line 221
    move v5, v13

    .line 222
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setYRange(DDI)V

    .line 223
    .line 224
    .line 225
    goto :goto_3

    .line 226
    :cond_8
    aget-wide v17, v8, v16

    .line 227
    .line 228
    aget-wide v0, v14, v16

    .line 229
    .line 230
    add-double v14, v0, v19

    .line 231
    .line 232
    cmpg-double v2, v17, v14

    .line 233
    .line 234
    if-gez v2, :cond_9

    .line 235
    .line 236
    sub-double/2addr v0, v3

    .line 237
    sub-double v1, v17, v0

    .line 238
    .line 239
    move-object/from16 v0, p0

    .line 240
    .line 241
    move-wide/from16 v3, v17

    .line 242
    .line 243
    move v5, v13

    .line 244
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setYRange(DDI)V

    .line 245
    .line 246
    .line 247
    goto :goto_3

    .line 248
    :cond_9
    add-double v2, v3, v19

    .line 249
    .line 250
    add-double v4, v0, v19

    .line 251
    .line 252
    move-object/from16 v0, p0

    .line 253
    .line 254
    move-wide v1, v2

    .line 255
    move-wide v3, v4

    .line 256
    move v5, v13

    .line 257
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setYRange(DDI)V

    .line 258
    .line 259
    .line 260
    goto :goto_3

    .line 261
    :cond_a
    aget-wide v0, v14, v15

    .line 262
    .line 263
    add-double v1, v0, v19

    .line 264
    .line 265
    aget-wide v3, v14, v16

    .line 266
    .line 267
    add-double v3, v3, v19

    .line 268
    .line 269
    move-object/from16 v0, p0

    .line 270
    .line 271
    move v5, v13

    .line 272
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/thirdpart/achartengine/tools/AbstractTool;->setYRange(DDI)V

    .line 273
    .line 274
    .line 275
    :cond_b
    :goto_3
    add-int/lit8 v13, v13, 0x1

    .line 276
    .line 277
    goto/16 :goto_1

    .line 278
    .line 279
    :cond_c
    return-void
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

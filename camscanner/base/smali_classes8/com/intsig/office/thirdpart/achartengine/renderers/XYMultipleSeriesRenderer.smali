.class public Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;
.super Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;
.source "XYMultipleSeriesRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;
    }
.end annotation


# instance fields
.field private formatCode:Ljava/lang/String;

.field private initialRange:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[D>;"
        }
    .end annotation
.end field

.field private mBarSpacing:D

.field private mGridColor:I

.field private mMarginsColor:I

.field private mMaxX:[D

.field private mMaxY:[D

.field private mMinX:[D

.field private mMinY:[D

.field private mOrientation:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

.field private mPanLimits:[D

.field private mPanXEnabled:Z

.field private mPanYEnabled:Z

.field private mPointSize:F

.field private mXLabels:I

.field private mXLabelsAngle:F

.field private mXTextLabels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mXTitle:Ljava/lang/String;

.field private mXTitleTextSize:F

.field private mYLabels:I

.field private mYLabelsAngle:F

.field private mYTextLabels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mYTitle:[Ljava/lang/String;

.field private mYTitleTextSize:F

.field private mZoomLimits:[D

.field private mZoomXEnabled:Z

.field private mZoomYEnabled:Z

.field private maxXTextLabelPos:D

.field private scalesCount:I

.field private seriesFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

.field private seriesFrame:Lcom/intsig/office/common/borders/Line;

.field private xFormatCode:Ljava/lang/String;

.field private xLabelsAlign:Landroid/graphics/Paint$Align;

.field private yAxisAlign:[Landroid/graphics/Paint$Align;

.field private yLabelsAlign:[Landroid/graphics/Paint$Align;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;-><init>()V

    const-string v0, ""

    .line 3
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitle:Ljava/lang/String;

    const/high16 v0, 0x41400000    # 12.0f

    .line 4
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitleTextSize:F

    .line 5
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitleTextSize:F

    const/4 v0, 0x5

    .line 6
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXLabels:I

    const/4 v0, 0x7

    .line 7
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYLabels:I

    .line 8
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->HORIZONTAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mOrientation:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 10
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    const/4 v0, 0x1

    .line 11
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanXEnabled:Z

    .line 12
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanYEnabled:Z

    .line 13
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomXEnabled:Z

    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomYEnabled:Z

    const-wide/16 v0, 0x0

    .line 15
    iput-wide v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mBarSpacing:D

    const/4 v2, 0x0

    .line 16
    iput v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMarginsColor:I

    .line 17
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    const/high16 v2, 0x40a00000    # 5.0f

    .line 18
    iput v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPointSize:F

    const/4 v2, 0x0

    .line 19
    iput-object v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 20
    iput-object v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFrame:Lcom/intsig/office/common/borders/Line;

    const/high16 v2, -0x1000000

    .line 21
    iput v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mGridColor:I

    .line 22
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    iput-object v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->xLabelsAlign:Landroid/graphics/Paint$Align;

    .line 23
    iput-wide v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->maxXTextLabelPos:D

    .line 24
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->scalesCount:I

    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initAxesRange(I)V

    return-void
.end method


# virtual methods
.method public addTextLabel(DLjava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public addXTextLabel(DLjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public addYTextLabel(DLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addYTextLabel(DLjava/lang/String;I)V

    return-void
.end method

.method public addYTextLabel(DLjava/lang/String;I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-interface {p4, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public clearTextLabels()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->clearXTextLabels()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public clearXTextLabels()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public clearYTextLabels()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBarSpacing()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mBarSpacing:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBarsSpacing()D
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getBarSpacing()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFormatCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->formatCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGridColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mGridColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getInitialRange()[D
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getInitialRange(I)[D

    move-result-object v0

    return-object v0
.end method

.method public getInitialRange(I)[D
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [D

    return-object p1
.end method

.method public getMarginsColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMarginsColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMaxXTextLabelPos()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->maxXTextLabelPos:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOrientation()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mOrientation:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPanLimits()[D
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanLimits:[D

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPointSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPointSize:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScalesCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->scalesCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSeriesBackgroundColor()Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSeriesFrame()Lcom/intsig/office/common/borders/Line;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFrame:Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXAxisMax()D
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMax(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getXAxisMax(I)D
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxX:[D

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public getXAxisMin()D
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXAxisMin(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getXAxisMin(I)D
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinX:[D

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public getXFormatCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->xFormatCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXLabels()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXLabels:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXLabelsAlign()Landroid/graphics/Paint$Align;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->xLabelsAlign:Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXLabelsAngle()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXLabelsAngle:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXTextLabel(Ljava/lang/Double;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Ljava/lang/String;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getXTextLabelLocations()[Ljava/lang/Double;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    new-array v1, v1, [Ljava/lang/Double;

    .line 9
    .line 10
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, [Ljava/lang/Double;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXTextLabels()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitle:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXTitleTextSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitleTextSize:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getYAxisAlign(I)Landroid/graphics/Paint$Align;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yAxisAlign:[Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getYAxisMax()D
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMax(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getYAxisMax(I)D
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxY:[D

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public getYAxisMin()D
    .locals 2

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYAxisMin(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getYAxisMin(I)D
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinY:[D

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public getYLabels()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYLabels:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getYLabelsAlign(I)Landroid/graphics/Paint$Align;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yLabelsAlign:[Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getYLabelsAngle()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYLabelsAngle:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getYTextLabel(Ljava/lang/Double;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getYTextLabel(Ljava/lang/Double;I)Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getYTextLabelLocations()[Ljava/lang/Double;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTextLabelLocations(I)[Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getYTextLabelLocations(I)[Ljava/lang/Double;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Double;

    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Double;

    return-object p1
.end method

.method public getYTitle()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getYTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYTitle(I)Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitle:[Ljava/lang/String;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public getYTitleTextSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitleTextSize:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoomLimits()[D
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomLimits:[D

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public initAxesRange(I)V
    .locals 1

    .line 1
    new-array v0, p1, [Ljava/lang/String;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitle:[Ljava/lang/String;

    .line 4
    .line 5
    new-array v0, p1, [Landroid/graphics/Paint$Align;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yLabelsAlign:[Landroid/graphics/Paint$Align;

    .line 8
    .line 9
    new-array v0, p1, [Landroid/graphics/Paint$Align;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yAxisAlign:[Landroid/graphics/Paint$Align;

    .line 12
    .line 13
    new-array v0, p1, [D

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinX:[D

    .line 16
    .line 17
    new-array v0, p1, [D

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxX:[D

    .line 20
    .line 21
    new-array v0, p1, [D

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinY:[D

    .line 24
    .line 25
    new-array v0, p1, [D

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxY:[D

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    :goto_0
    if-ge v0, p1, :cond_0

    .line 31
    .line 32
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initAxesRangeForScale(I)V

    .line 33
    .line 34
    .line 35
    add-int/lit8 v0, v0, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method public initAxesRangeForScale(I)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinX:[D

    .line 2
    .line 3
    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    aput-wide v1, v0, p1

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxX:[D

    .line 11
    .line 12
    const-wide v4, -0x10000000000001L

    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    aput-wide v4, v3, p1

    .line 18
    .line 19
    iget-object v6, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinY:[D

    .line 20
    .line 21
    aput-wide v1, v6, p1

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxY:[D

    .line 24
    .line 25
    aput-wide v4, v1, p1

    .line 26
    .line 27
    const/4 v2, 0x4

    .line 28
    new-array v2, v2, [D

    .line 29
    .line 30
    const/4 v4, 0x0

    .line 31
    aget-wide v7, v0, p1

    .line 32
    .line 33
    aput-wide v7, v2, v4

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    aget-wide v4, v3, p1

    .line 37
    .line 38
    aput-wide v4, v2, v0

    .line 39
    .line 40
    const/4 v0, 0x2

    .line 41
    aget-wide v3, v6, p1

    .line 42
    .line 43
    aput-wide v3, v2, v0

    .line 44
    .line 45
    const/4 v0, 0x3

    .line 46
    aget-wide v3, v1, p1

    .line 47
    .line 48
    aput-wide v3, v2, v0

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    .line 51
    .line 52
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitle:[Ljava/lang/String;

    .line 60
    .line 61
    const-string v1, ""

    .line 62
    .line 63
    aput-object v1, v0, p1

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTextLabels:Ljava/util/Map;

    .line 66
    .line 67
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    new-instance v2, Ljava/util/HashMap;

    .line 72
    .line 73
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yLabelsAlign:[Landroid/graphics/Paint$Align;

    .line 80
    .line 81
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    .line 82
    .line 83
    aput-object v1, v0, p1

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yAxisAlign:[Landroid/graphics/Paint$Align;

    .line 86
    .line 87
    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 88
    .line 89
    aput-object v1, v0, p1

    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public isInitialRangeSet()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isInitialRangeSet(I)Z

    move-result v0

    return v0
.end method

.method public isInitialRangeSet(I)Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isMaxXSet()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v0

    return v0
.end method

.method public isMaxXSet(I)Z
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxX:[D

    aget-wide v1, v0, p1

    const-wide v3, -0x10000000000001L

    cmpl-double p1, v1, v3

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isMaxYSet()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v0

    return v0
.end method

.method public isMaxYSet(I)Z
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxY:[D

    aget-wide v1, v0, p1

    const-wide v3, -0x10000000000001L

    cmpl-double p1, v1, v3

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isMinXSet()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v0

    return v0
.end method

.method public isMinXSet(I)Z
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinX:[D

    aget-wide v1, v0, p1

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double p1, v1, v3

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isMinYSet()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinYSet(I)Z

    move-result v0

    return v0
.end method

.method public isMinYSet(I)Z
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinY:[D

    aget-wide v1, v0, p1

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double p1, v1, v3

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isPanEnabled()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isPanXEnabled()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isPanYEnabled()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPanXEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanXEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPanYEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanYEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isZoomEnabled()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isZoomXEnabled()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isZoomYEnabled()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isZoomXEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomXEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isZoomYEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomYEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setBarSpacing(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mBarSpacing:D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setChartValuesTextSize(F)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRenderers()[Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_0

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setChartValuesTextSize(F)V

    .line 12
    .line 13
    .line 14
    add-int/lit8 v2, v2, 0x1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDisplayChartValues(Z)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRenderers()[Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_0

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setDisplayChartValues(Z)V

    .line 12
    .line 13
    .line 14
    add-int/lit8 v2, v2, 0x1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFormatCode(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->formatCode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGridColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mGridColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setInitialRange([D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setInitialRange([DI)V

    return-void
.end method

.method public setInitialRange([DI)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setMarginsColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMarginsColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setMaxLenXTextLabel(II)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->maxXTextLabelPos:D

    .line 4
    .line 5
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTextLabels:Ljava/util/Map;

    .line 21
    .line 22
    iget-wide v2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->maxXTextLabelPos:D

    .line 23
    .line 24
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    check-cast v1, Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, ""

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-le v0, p2, :cond_0

    .line 51
    .line 52
    return-void

    .line 53
    :cond_0
    int-to-double p1, p1

    .line 54
    iput-wide p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->maxXTextLabelPos:D

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setOrientation(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mOrientation:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPanEnabled(ZZ)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanXEnabled:Z

    .line 2
    .line 3
    iput-boolean p2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanYEnabled:Z

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPanLimits([D)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPanLimits:[D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPointSize(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mPointSize:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRange([D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setRange([DI)V

    return-void
.end method

.method public setRange([DI)V
    .locals 2

    const/4 v0, 0x0

    .line 2
    aget-wide v0, p1, v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(DI)V

    const/4 v0, 0x1

    .line 3
    aget-wide v0, p1, v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(DI)V

    const/4 v0, 0x2

    .line 4
    aget-wide v0, p1, v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMin(DI)V

    const/4 v0, 0x3

    .line 5
    aget-wide v0, p1, v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMax(DI)V

    return-void
.end method

.method public setSeriesBackgroundColor(Lcom/intsig/office/common/bg/BackgroundAndFill;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFill:Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSeriesFrame(Lcom/intsig/office/common/borders/Line;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->seriesFrame:Lcom/intsig/office/common/borders/Line;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXAxisMax(D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(DI)V

    return-void
.end method

.method public setXAxisMax(DI)V
    .locals 2

    .line 2
    invoke-virtual {p0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxXSet(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    const/4 v1, 0x1

    aput-wide p1, v0, v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxX:[D

    aput-wide p1, v0, p3

    return-void
.end method

.method public setXAxisMin(D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(DI)V

    return-void
.end method

.method public setXAxisMin(DI)V
    .locals 2

    .line 2
    invoke-virtual {p0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinXSet(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinX:[D

    aput-wide p1, v0, p3

    return-void
.end method

.method public setXFormatCode(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->xFormatCode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXLabels(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXLabels:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXLabelsAlign(Landroid/graphics/Paint$Align;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->xLabelsAlign:Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXLabelsAngle(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXLabelsAngle:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXTitle(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitle:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXTitleTextSize(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mXTitleTextSize:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setYAxisAlign(Landroid/graphics/Paint$Align;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yAxisAlign:[Landroid/graphics/Paint$Align;

    .line 2
    .line 3
    aput-object p1, v0, p2

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setYAxisMax(D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMax(DI)V

    return-void
.end method

.method public setYAxisMax(DI)V
    .locals 2

    .line 2
    invoke-virtual {p0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMaxYSet(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    const/4 v1, 0x3

    aput-wide p1, v0, v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMaxY:[D

    aput-wide p1, v0, p3

    return-void
.end method

.method public setYAxisMin(D)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMin(DI)V

    return-void
.end method

.method public setYAxisMin(DI)V
    .locals 2

    .line 2
    invoke-virtual {p0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->isMinYSet(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->initialRange:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    const/4 v1, 0x2

    aput-wide p1, v0, v1

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mMinY:[D

    aput-wide p1, v0, p3

    return-void
.end method

.method public setYLabels(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYLabels:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setYLabelsAlign(Landroid/graphics/Paint$Align;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYLabelsAlign(Landroid/graphics/Paint$Align;I)V

    return-void
.end method

.method public setYLabelsAlign(Landroid/graphics/Paint$Align;I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->yLabelsAlign:[Landroid/graphics/Paint$Align;

    aput-object p1, v0, p2

    return-void
.end method

.method public setYLabelsAngle(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYLabelsAngle:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setYTitle(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYTitle(Ljava/lang/String;I)V

    return-void
.end method

.method public setYTitle(Ljava/lang/String;I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitle:[Ljava/lang/String;

    aput-object p1, v0, p2

    return-void
.end method

.method public setYTitleTextSize(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mYTitleTextSize:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZoomEnabled(ZZ)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomXEnabled:Z

    .line 2
    .line 3
    iput-boolean p2, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomYEnabled:Z

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setZoomLimits([D)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->mZoomLimits:[D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

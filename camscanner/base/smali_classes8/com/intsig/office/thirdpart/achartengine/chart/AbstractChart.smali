.class public abstract Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
.super Ljava/lang/Object;
.source "AbstractChart.java"


# static fields
.field public static final CHART_AREA:S = 0x0s

.field public static final CHART_BAR:S = 0x1s

.field public static final CHART_BUBBLE:S = 0x8s

.field public static final CHART_DOUGHNUT:S = 0x7s

.field public static final CHART_LINE:S = 0x2s

.field public static final CHART_PIE:S = 0x3s

.field public static final CHART_RADAR:S = 0x9s

.field public static final CHART_SCATTER:S = 0x4s

.field public static final CHART_STOCK:S = 0x5s

.field public static final CHART_SURFACE:S = 0x6s

.field public static final CHART_UNKOWN:S = 0xas

.field public static final LegendPosition_Bottom:B = 0x3t

.field public static final LegendPosition_Left:B = 0x0t

.field public static final LegendPosition_Right:B = 0x2t

.field public static final LegendPosition_Top:B = 0x1t


# instance fields
.field private categoryAxisTextColor:I

.field private legendArea:Lcom/intsig/office/java/awt/Rectangle;

.field protected legendPos:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, -0x1000000

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    iput-byte v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getFitText(Ljava/lang/String;FLandroid/graphics/Paint;)Ljava/lang/String;
    .locals 6

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    move-object v2, p1

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    invoke-virtual {p3, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 9
    .line 10
    .line 11
    move-result v4

    .line 12
    const-string v5, "..."

    .line 13
    .line 14
    cmpl-float v4, v4, p2

    .line 15
    .line 16
    if-lez v4, :cond_0

    .line 17
    .line 18
    if-ge v3, v0, :cond_0

    .line 19
    .line 20
    add-int/lit8 v3, v3, 0x1

    .line 21
    .line 22
    new-instance v2, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    sub-int v4, v0, v3

    .line 28
    .line 29
    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    if-ne v3, v0, :cond_1

    .line 45
    .line 46
    move-object v2, v5

    .line 47
    :cond_1
    return-object v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getLegendTextOffset(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)F
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendShapeWidth(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    mul-int/lit8 v0, v0, 0x2

    .line 7
    .line 8
    int-to-float v0, v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    mul-float v0, v0, p1

    .line 14
    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public abstract draw(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;IIIILandroid/graphics/Paint;)V
.end method

.method protected drawBackground(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;ZI)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isApplyBackgroundColor()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    if-eqz p8, :cond_2

    .line 8
    .line 9
    :cond_0
    if-eqz p8, :cond_1

    .line 10
    .line 11
    invoke-virtual {p7, p9}, Landroid/graphics/Paint;->setColor(I)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getBackgroundColor()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    invoke-virtual {p7, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    sget-object p1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 23
    .line 24
    invoke-virtual {p7, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 25
    .line 26
    .line 27
    int-to-float v1, p3

    .line 28
    int-to-float v2, p4

    .line 29
    add-int/2addr p3, p5

    .line 30
    int-to-float v3, p3

    .line 31
    add-int/2addr p4, p6

    .line 32
    int-to-float v4, p4

    .line 33
    move-object v0, p2

    .line 34
    move-object v5, p7

    .line 35
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 36
    .line 37
    .line 38
    :cond_2
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method protected drawBackgroundAndFrame(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .locals 13

    .line 1
    move-object/from16 v9, p4

    .line 2
    .line 3
    move-object/from16 v10, p5

    .line 4
    .line 5
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Paint;->getAlpha()I

    .line 6
    .line 7
    .line 8
    move-result v11

    .line 9
    new-instance v12, Landroid/graphics/Path;

    .line 10
    .line 11
    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    .line 12
    .line 13
    .line 14
    iget v0, v9, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    int-to-float v1, v0

    .line 17
    iget v0, v9, Landroid/graphics/Rect;->top:I

    .line 18
    .line 19
    int-to-float v2, v0

    .line 20
    iget v0, v9, Landroid/graphics/Rect;->right:I

    .line 21
    .line 22
    int-to-float v3, v0

    .line 23
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    .line 24
    .line 25
    int-to-float v4, v0

    .line 26
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 27
    .line 28
    move-object v0, v12

    .line 29
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    if-eqz v3, :cond_0

    .line 37
    .line 38
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 39
    .line 40
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    .line 42
    .line 43
    const/4 v2, 0x1

    .line 44
    const/4 v5, 0x0

    .line 45
    const/high16 v6, 0x3f800000    # 1.0f

    .line 46
    .line 47
    move-object v0, p2

    .line 48
    move-object/from16 v1, p3

    .line 49
    .line 50
    move-object/from16 v4, p4

    .line 51
    .line 52
    move-object v7, v12

    .line 53
    move-object/from16 v8, p5

    .line 54
    .line 55
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawPathBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 59
    .line 60
    .line 61
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartFrame()Lcom/intsig/office/common/borders/Line;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    if-eqz v0, :cond_2

    .line 66
    .line 67
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 68
    .line 69
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 70
    .line 71
    .line 72
    const/high16 v1, 0x40000000    # 2.0f

    .line 73
    .line 74
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Line;->isDash()Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_1

    .line 82
    .line 83
    new-instance v1, Landroid/graphics/DashPathEffect;

    .line 84
    .line 85
    const/4 v2, 0x2

    .line 86
    new-array v2, v2, [F

    .line 87
    .line 88
    fill-array-data v2, :array_0

    .line 89
    .line 90
    .line 91
    const/high16 v3, 0x41200000    # 10.0f

    .line 92
    .line 93
    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v10, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 97
    .line 98
    .line 99
    :cond_1
    const/4 v1, 0x0

    .line 100
    const/4 v2, 0x1

    .line 101
    invoke-virtual {v0}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    const/4 v5, 0x0

    .line 106
    const/high16 v6, 0x3f800000    # 1.0f

    .line 107
    .line 108
    move-object v0, p2

    .line 109
    move-object/from16 v4, p4

    .line 110
    .line 111
    move-object v7, v12

    .line 112
    move-object/from16 v8, p5

    .line 113
    .line 114
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/common/BackgroundDrawer;->drawPathBackground(Landroid/graphics/Canvas;Lcom/intsig/office/system/IControl;ILcom/intsig/office/common/bg/BackgroundAndFill;Landroid/graphics/Rect;Lcom/intsig/office/pg/animate/IAnimation;FLandroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 115
    .line 116
    .line 117
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 118
    .line 119
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 123
    .line 124
    .line 125
    :cond_2
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Paint;->reset()V

    .line 126
    .line 127
    .line 128
    const/4 v0, 0x1

    .line 129
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 130
    .line 131
    .line 132
    return-void

    .line 133
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method protected drawLabel(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Ljava/util/List;IIFFFFIILandroid/graphics/Paint;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/lang/String;",
            "Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;IIFFFFII",
            "Landroid/graphics/Paint;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p4

    move-object/from16 v1, p13

    .line 1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLabels()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v2, 0x40000000    # 2.0f

    div-float v3, p10, v2

    add-float v3, p9, v3

    const/high16 v4, 0x42b40000    # 90.0f

    sub-float/2addr v4, v3

    float-to-double v3, v4

    .line 3
    invoke-static {v3, v4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v3

    .line 4
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    .line 5
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    move/from16 v7, p5

    int-to-float v7, v7

    move/from16 v8, p7

    float-to-double v8, v8

    mul-double v10, v8, v5

    double-to-float v10, v10

    add-float/2addr v10, v7

    .line 6
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    move/from16 v11, p6

    int-to-float v11, v11

    mul-double v8, v8, v3

    double-to-float v8, v8

    add-float/2addr v8, v11

    .line 7
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    move/from16 v9, p8

    float-to-double v12, v9

    mul-double v5, v5, v12

    double-to-float v5, v5

    add-float/2addr v7, v5

    .line 8
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    mul-double v12, v12, v3

    double-to-float v3, v12

    add-float/2addr v11, v3

    .line 9
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 10
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLabelsTextSize()F

    move-result v4

    div-float v2, v4, v2

    const/high16 v6, 0x41200000    # 10.0f

    .line 11
    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 12
    sget-object v7, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    if-le v10, v5, :cond_0

    neg-float v6, v6

    .line 13
    sget-object v7, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    :cond_0
    int-to-float v7, v5

    add-float/2addr v6, v7

    int-to-float v3, v3

    move/from16 v9, p12

    int-to-float v9, v9

    sub-float/2addr v9, v6

    if-le v10, v5, :cond_1

    move/from16 v5, p11

    int-to-float v5, v5

    sub-float v9, v6, v5

    :cond_1
    move-object/from16 v5, p0

    move-object/from16 v11, p2

    .line 14
    invoke-direct {v5, v11, v9, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getFitText(Ljava/lang/String;FLandroid/graphics/Paint;)Ljava/lang/String;

    move-result-object v9

    .line 15
    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v11

    const/4 v13, 0x0

    :goto_0
    if-nez v13, :cond_4

    .line 16
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_1
    if-ge v14, v13, :cond_3

    if-nez v15, :cond_3

    .line 17
    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v12, v16

    check-cast v12, Landroid/graphics/RectF;

    add-float v5, v6, v11

    move/from16 p3, v13

    add-float v13, v3, v4

    .line 18
    invoke-virtual {v12, v6, v3, v5, v13}, Landroid/graphics/RectF;->intersects(FFFF)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 19
    iget v5, v12, Landroid/graphics/RectF;->bottom:F

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/4 v15, 0x1

    :cond_2
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v5, p0

    move/from16 v13, p3

    goto :goto_1

    :cond_3
    xor-int/lit8 v13, v15, 0x1

    move-object/from16 v5, p0

    goto :goto_0

    :cond_4
    sub-float v2, v3, v2

    float-to-int v2, v2

    int-to-float v5, v10

    int-to-float v8, v8

    int-to-float v2, v2

    move-object/from16 p5, p1

    move/from16 p6, v5

    move/from16 p7, v8

    move/from16 p8, v7

    move/from16 p9, v2

    move-object/from16 p10, p13

    .line 20
    invoke-virtual/range {p5 .. p10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move/from16 p6, v7

    move/from16 p7, v2

    move/from16 p8, v6

    .line 21
    invoke-virtual/range {p5 .. p10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v2, p1

    .line 22
    invoke-virtual {v2, v9, v6, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 23
    new-instance v1, Landroid/graphics/RectF;

    add-float/2addr v11, v6

    add-float/2addr v4, v3

    invoke-direct {v1, v6, v3, v11, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    return-void
.end method

.method protected drawLegend(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;IIIILandroid/graphics/Paint;Z)I
    .locals 25

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p1

    .line 4
    .line 5
    move-object/from16 v9, p2

    .line 6
    .line 7
    move-object/from16 v10, p3

    .line 8
    .line 9
    move/from16 v0, p4

    .line 10
    .line 11
    move/from16 v11, p6

    .line 12
    .line 13
    move-object/from16 v12, p8

    .line 14
    .line 15
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLegend()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_d

    .line 20
    .line 21
    invoke-virtual {v7, v9, v10, v12, v11}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getSingleAutoLegendSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;Landroid/graphics/Paint;I)Lcom/intsig/office/java/awt/Rectangle;

    .line 22
    .line 23
    .line 24
    move-result-object v13

    .line 25
    int-to-float v14, v0

    .line 26
    move/from16 v1, p5

    .line 27
    .line 28
    int-to-float v1, v1

    .line 29
    add-int/2addr v0, v11

    .line 30
    int-to-float v15, v0

    .line 31
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 32
    .line 33
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendTextSize()F

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    mul-float v0, v0, v2

    .line 45
    .line 46
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 47
    .line 48
    .line 49
    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 50
    .line 51
    .line 52
    move-result-object v6

    .line 53
    array-length v0, v10

    .line 54
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    const/4 v4, 0x0

    .line 63
    move/from16 v16, v1

    .line 64
    .line 65
    move/from16 v17, v14

    .line 66
    .line 67
    const/4 v3, 0x0

    .line 68
    :goto_0
    if-ge v3, v5, :cond_d

    .line 69
    .line 70
    invoke-virtual {v7, v3}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendShapeWidth(I)I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    int-to-float v0, v0

    .line 75
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    mul-float v18, v0, v1

    .line 80
    .line 81
    aget-object v0, v10, v3

    .line 82
    .line 83
    const-string v1, "\n"

    .line 84
    .line 85
    const-string v2, " "

    .line 86
    .line 87
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-virtual {v12, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    invoke-direct {v7, v9}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendTextOffset(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)F

    .line 96
    .line 97
    .line 98
    move-result v19

    .line 99
    add-float v20, v19, v0

    .line 100
    .line 101
    iget-byte v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 102
    .line 103
    const/high16 v21, 0x40000000    # 2.0f

    .line 104
    .line 105
    const/4 v1, 0x1

    .line 106
    if-eqz v0, :cond_0

    .line 107
    .line 108
    if-eq v0, v1, :cond_1

    .line 109
    .line 110
    const/4 v1, 0x2

    .line 111
    if-eq v0, v1, :cond_0

    .line 112
    .line 113
    const/4 v1, 0x3

    .line 114
    if-eq v0, v1, :cond_1

    .line 115
    .line 116
    move/from16 v22, v5

    .line 117
    .line 118
    move-object v5, v6

    .line 119
    move/from16 p9, v14

    .line 120
    .line 121
    move/from16 p7, v15

    .line 122
    .line 123
    move v14, v3

    .line 124
    goto/16 :goto_8

    .line 125
    .line 126
    :cond_0
    move/from16 v22, v5

    .line 127
    .line 128
    move-object v5, v6

    .line 129
    move v6, v11

    .line 130
    move/from16 p9, v14

    .line 131
    .line 132
    move/from16 p7, v15

    .line 133
    .line 134
    move-object v15, v2

    .line 135
    move v14, v3

    .line 136
    goto/16 :goto_5

    .line 137
    .line 138
    :cond_1
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 139
    .line 140
    int-to-float v1, v0

    .line 141
    cmpg-float v1, v20, v1

    .line 142
    .line 143
    if-gtz v1, :cond_5

    .line 144
    .line 145
    int-to-float v0, v0

    .line 146
    add-float v0, v17, v0

    .line 147
    .line 148
    cmpl-float v0, v0, v15

    .line 149
    .line 150
    if-lez v0, :cond_3

    .line 151
    .line 152
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 153
    .line 154
    int-to-float v0, v0

    .line 155
    add-float v16, v16, v0

    .line 156
    .line 157
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    mul-float v17, v14, v0

    .line 162
    .line 163
    array-length v0, v10

    .line 164
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 165
    .line 166
    .line 167
    move-result v1

    .line 168
    if-ne v0, v1, :cond_2

    .line 169
    .line 170
    invoke-virtual {v9, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getColor()I

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    .line 180
    .line 181
    goto :goto_1

    .line 182
    :cond_2
    const v0, -0x333334

    .line 183
    .line 184
    .line 185
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 186
    .line 187
    .line 188
    :goto_1
    invoke-virtual {v9, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 189
    .line 190
    .line 191
    move-result-object v19

    .line 192
    move-object/from16 v0, p0

    .line 193
    .line 194
    move-object/from16 v1, p1

    .line 195
    .line 196
    move/from16 p7, v15

    .line 197
    .line 198
    move-object v15, v2

    .line 199
    move-object/from16 v2, v19

    .line 200
    .line 201
    move/from16 p9, v3

    .line 202
    .line 203
    move/from16 v3, v17

    .line 204
    .line 205
    move/from16 v4, v16

    .line 206
    .line 207
    move/from16 v22, v5

    .line 208
    .line 209
    move/from16 v5, p9

    .line 210
    .line 211
    move-object v11, v6

    .line 212
    move-object/from16 v6, p8

    .line 213
    .line 214
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegendShape(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V

    .line 215
    .line 216
    .line 217
    iget v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 218
    .line 219
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 220
    .line 221
    .line 222
    mul-float v18, v18, v21

    .line 223
    .line 224
    add-float v0, v17, v18

    .line 225
    .line 226
    iget v1, v11, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 227
    .line 228
    add-float v1, v16, v1

    .line 229
    .line 230
    invoke-virtual {v8, v15, v0, v1, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 231
    .line 232
    .line 233
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 234
    .line 235
    int-to-float v0, v0

    .line 236
    add-float v17, v17, v0

    .line 237
    .line 238
    move-object v5, v11

    .line 239
    move/from16 v11, p6

    .line 240
    .line 241
    move/from16 v24, v14

    .line 242
    .line 243
    move/from16 v14, p9

    .line 244
    .line 245
    move/from16 p9, v24

    .line 246
    .line 247
    goto/16 :goto_8

    .line 248
    .line 249
    :cond_3
    move/from16 p9, v3

    .line 250
    .line 251
    move/from16 v22, v5

    .line 252
    .line 253
    move-object v11, v6

    .line 254
    move/from16 p7, v15

    .line 255
    .line 256
    move-object v15, v2

    .line 257
    array-length v0, v10

    .line 258
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 259
    .line 260
    .line 261
    move-result v1

    .line 262
    if-ne v0, v1, :cond_4

    .line 263
    .line 264
    move/from16 v6, p9

    .line 265
    .line 266
    invoke-virtual {v9, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 267
    .line 268
    .line 269
    move-result-object v0

    .line 270
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getColor()I

    .line 271
    .line 272
    .line 273
    move-result v0

    .line 274
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 275
    .line 276
    .line 277
    goto :goto_2

    .line 278
    :cond_4
    move/from16 v6, p9

    .line 279
    .line 280
    const v0, -0x333334

    .line 281
    .line 282
    .line 283
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 284
    .line 285
    .line 286
    :goto_2
    invoke-virtual {v9, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 287
    .line 288
    .line 289
    move-result-object v2

    .line 290
    move-object/from16 v0, p0

    .line 291
    .line 292
    move-object/from16 v1, p1

    .line 293
    .line 294
    move/from16 v3, v17

    .line 295
    .line 296
    move/from16 v4, v16

    .line 297
    .line 298
    move v5, v6

    .line 299
    move/from16 p9, v14

    .line 300
    .line 301
    move v14, v6

    .line 302
    move-object/from16 v6, p8

    .line 303
    .line 304
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegendShape(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V

    .line 305
    .line 306
    .line 307
    iget v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 308
    .line 309
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 310
    .line 311
    .line 312
    mul-float v18, v18, v21

    .line 313
    .line 314
    add-float v0, v17, v18

    .line 315
    .line 316
    iget v1, v11, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 317
    .line 318
    add-float v1, v16, v1

    .line 319
    .line 320
    invoke-virtual {v8, v15, v0, v1, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 321
    .line 322
    .line 323
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 324
    .line 325
    int-to-float v0, v0

    .line 326
    add-float v17, v17, v0

    .line 327
    .line 328
    move-object v5, v11

    .line 329
    move/from16 v11, p6

    .line 330
    .line 331
    goto/16 :goto_8

    .line 332
    .line 333
    :cond_5
    move/from16 v22, v5

    .line 334
    .line 335
    move-object v11, v6

    .line 336
    move/from16 p9, v14

    .line 337
    .line 338
    move/from16 p7, v15

    .line 339
    .line 340
    move-object v15, v2

    .line 341
    move v14, v3

    .line 342
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 343
    .line 344
    int-to-float v0, v0

    .line 345
    add-float v16, v16, v0

    .line 346
    .line 347
    array-length v0, v10

    .line 348
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 349
    .line 350
    .line 351
    move-result v1

    .line 352
    if-ne v0, v1, :cond_6

    .line 353
    .line 354
    invoke-virtual {v9, v14}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 355
    .line 356
    .line 357
    move-result-object v0

    .line 358
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getColor()I

    .line 359
    .line 360
    .line 361
    move-result v0

    .line 362
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 363
    .line 364
    .line 365
    goto :goto_3

    .line 366
    :cond_6
    const v0, -0x333334

    .line 367
    .line 368
    .line 369
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 370
    .line 371
    .line 372
    :goto_3
    invoke-virtual {v9, v14}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 373
    .line 374
    .line 375
    move-result-object v2

    .line 376
    move-object/from16 v0, p0

    .line 377
    .line 378
    const/4 v6, 0x1

    .line 379
    move-object/from16 v1, p1

    .line 380
    .line 381
    move/from16 v3, p9

    .line 382
    .line 383
    move/from16 v4, v16

    .line 384
    .line 385
    move v5, v14

    .line 386
    move-object/from16 v20, v11

    .line 387
    .line 388
    const/4 v11, 0x1

    .line 389
    move-object/from16 v6, p8

    .line 390
    .line 391
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegendShape(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V

    .line 392
    .line 393
    .line 394
    iget v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 395
    .line 396
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 397
    .line 398
    .line 399
    move/from16 v6, p6

    .line 400
    .line 401
    move-object/from16 v5, v20

    .line 402
    .line 403
    int-to-float v0, v6

    .line 404
    sub-float v0, v0, v19

    .line 405
    .line 406
    const/4 v4, 0x0

    .line 407
    new-array v1, v4, [F

    .line 408
    .line 409
    move-object v2, v15

    .line 410
    move/from16 v3, v16

    .line 411
    .line 412
    :goto_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 413
    .line 414
    .line 415
    move-result v15

    .line 416
    if-lez v15, :cond_8

    .line 417
    .line 418
    invoke-virtual {v12, v2, v11, v0, v1}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    .line 419
    .line 420
    .line 421
    move-result v15

    .line 422
    if-nez v15, :cond_7

    .line 423
    .line 424
    const/4 v15, 0x1

    .line 425
    :cond_7
    invoke-virtual {v2, v4, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 426
    .line 427
    .line 428
    move-result-object v11

    .line 429
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 430
    .line 431
    .line 432
    move-result v4

    .line 433
    invoke-virtual {v2, v15, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 434
    .line 435
    .line 436
    move-result-object v2

    .line 437
    mul-float v4, v18, v21

    .line 438
    .line 439
    add-float v4, p9, v4

    .line 440
    .line 441
    iget v15, v5, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 442
    .line 443
    add-float/2addr v15, v3

    .line 444
    invoke-virtual {v8, v11, v4, v15, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 445
    .line 446
    .line 447
    float-to-double v3, v3

    .line 448
    iget v11, v5, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 449
    .line 450
    iget v15, v5, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 451
    .line 452
    sub-float/2addr v11, v15

    .line 453
    move/from16 p4, v0

    .line 454
    .line 455
    move-object/from16 v16, v1

    .line 456
    .line 457
    float-to-double v0, v11

    .line 458
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 459
    .line 460
    .line 461
    move-result-wide v0

    .line 462
    add-double/2addr v3, v0

    .line 463
    double-to-float v3, v3

    .line 464
    move/from16 v0, p4

    .line 465
    .line 466
    move-object/from16 v1, v16

    .line 467
    .line 468
    const/4 v4, 0x0

    .line 469
    const/4 v11, 0x1

    .line 470
    goto :goto_4

    .line 471
    :cond_8
    move/from16 v17, p9

    .line 472
    .line 473
    move/from16 v16, v3

    .line 474
    .line 475
    move v11, v6

    .line 476
    goto/16 :goto_8

    .line 477
    .line 478
    :goto_5
    array-length v0, v10

    .line 479
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 480
    .line 481
    .line 482
    move-result v1

    .line 483
    if-ne v0, v1, :cond_9

    .line 484
    .line 485
    invoke-virtual {v9, v14}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 486
    .line 487
    .line 488
    move-result-object v0

    .line 489
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->getColor()I

    .line 490
    .line 491
    .line 492
    move-result v0

    .line 493
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 494
    .line 495
    .line 496
    goto :goto_6

    .line 497
    :cond_9
    const v0, -0x333334

    .line 498
    .line 499
    .line 500
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 501
    .line 502
    .line 503
    :goto_6
    invoke-virtual {v9, v14}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 504
    .line 505
    .line 506
    move-result-object v2

    .line 507
    move-object/from16 v0, p0

    .line 508
    .line 509
    move-object/from16 v1, p1

    .line 510
    .line 511
    move/from16 v3, v17

    .line 512
    .line 513
    const/4 v11, 0x0

    .line 514
    move/from16 v4, v16

    .line 515
    .line 516
    move-object/from16 v23, v5

    .line 517
    .line 518
    move v5, v14

    .line 519
    move v11, v6

    .line 520
    move-object/from16 v6, p8

    .line 521
    .line 522
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->drawLegendShape(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V

    .line 523
    .line 524
    .line 525
    iget v0, v7, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 526
    .line 527
    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 528
    .line 529
    .line 530
    int-to-float v0, v11

    .line 531
    cmpl-float v1, v20, v0

    .line 532
    .line 533
    if-lez v1, :cond_c

    .line 534
    .line 535
    sub-float v0, v0, v19

    .line 536
    .line 537
    const/4 v1, 0x0

    .line 538
    new-array v2, v1, [F

    .line 539
    .line 540
    move/from16 v3, v16

    .line 541
    .line 542
    :goto_7
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    .line 543
    .line 544
    .line 545
    move-result v4

    .line 546
    if-lez v4, :cond_b

    .line 547
    .line 548
    const/4 v4, 0x1

    .line 549
    invoke-virtual {v12, v15, v4, v0, v2}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    .line 550
    .line 551
    .line 552
    move-result v5

    .line 553
    if-nez v5, :cond_a

    .line 554
    .line 555
    const/4 v5, 0x1

    .line 556
    :cond_a
    invoke-virtual {v15, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 557
    .line 558
    .line 559
    move-result-object v6

    .line 560
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    .line 561
    .line 562
    .line 563
    move-result v1

    .line 564
    invoke-virtual {v15, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 565
    .line 566
    .line 567
    move-result-object v15

    .line 568
    mul-float v1, v18, v21

    .line 569
    .line 570
    add-float v1, v17, v1

    .line 571
    .line 572
    move-object/from16 v5, v23

    .line 573
    .line 574
    iget v4, v5, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 575
    .line 576
    add-float/2addr v4, v3

    .line 577
    invoke-virtual {v8, v6, v1, v4, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 578
    .line 579
    .line 580
    float-to-double v3, v3

    .line 581
    iget v1, v5, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 582
    .line 583
    iget v6, v5, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 584
    .line 585
    sub-float/2addr v1, v6

    .line 586
    move/from16 p4, v0

    .line 587
    .line 588
    float-to-double v0, v1

    .line 589
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 590
    .line 591
    .line 592
    move-result-wide v0

    .line 593
    add-double/2addr v3, v0

    .line 594
    double-to-float v3, v3

    .line 595
    move/from16 v0, p4

    .line 596
    .line 597
    const/4 v1, 0x0

    .line 598
    goto :goto_7

    .line 599
    :cond_b
    move-object/from16 v5, v23

    .line 600
    .line 601
    move/from16 v16, v3

    .line 602
    .line 603
    goto :goto_8

    .line 604
    :cond_c
    move-object/from16 v5, v23

    .line 605
    .line 606
    mul-float v18, v18, v21

    .line 607
    .line 608
    add-float v0, v17, v18

    .line 609
    .line 610
    iget v1, v5, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 611
    .line 612
    add-float v1, v16, v1

    .line 613
    .line 614
    invoke-virtual {v8, v15, v0, v1, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 615
    .line 616
    .line 617
    iget v0, v13, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 618
    .line 619
    int-to-float v0, v0

    .line 620
    add-float v16, v16, v0

    .line 621
    .line 622
    :goto_8
    add-int/lit8 v3, v14, 0x1

    .line 623
    .line 624
    move/from16 v15, p7

    .line 625
    .line 626
    move/from16 v14, p9

    .line 627
    .line 628
    move-object v6, v5

    .line 629
    move/from16 v5, v22

    .line 630
    .line 631
    const/4 v4, 0x0

    .line 632
    goto/16 :goto_0

    .line 633
    .line 634
    :cond_d
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendTextSize()F

    .line 635
    .line 636
    .line 637
    move-result v0

    .line 638
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 639
    .line 640
    .line 641
    move-result v1

    .line 642
    mul-float v0, v0, v1

    .line 643
    .line 644
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 645
    .line 646
    .line 647
    move-result v0

    .line 648
    return v0
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public abstract drawLegendShape(Landroid/graphics/Canvas;Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;FFILandroid/graphics/Paint;)V
.end method

.method protected drawPath(Landroid/graphics/Canvas;[FLandroid/graphics/Paint;Z)V
    .locals 6

    .line 1
    new-instance v0, Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    aget v2, p2, v1

    .line 8
    .line 9
    const/4 v3, 0x1

    .line 10
    aget v4, p2, v3

    .line 11
    .line 12
    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x2

    .line 16
    :goto_0
    array-length v4, p2

    .line 17
    if-ge v2, v4, :cond_0

    .line 18
    .line 19
    aget v4, p2, v2

    .line 20
    .line 21
    add-int/lit8 v5, v2, 0x1

    .line 22
    .line 23
    aget v5, p2, v5

    .line 24
    .line 25
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 26
    .line 27
    .line 28
    add-int/lit8 v2, v2, 0x2

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    if-eqz p4, :cond_1

    .line 32
    .line 33
    aget p4, p2, v1

    .line 34
    .line 35
    aget p2, p2, v3

    .line 36
    .line 37
    invoke-virtual {v0, p4, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 38
    .line 39
    .line 40
    :cond_1
    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method protected drawTitle(Landroid/graphics/Canvas;Ljava/lang/String;FFFFFLandroid/graphics/Paint;F)V
    .locals 17

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p8

    .line 6
    .line 7
    move/from16 v3, p9

    .line 8
    .line 9
    mul-float v4, p4, p3

    .line 10
    .line 11
    mul-float v5, p5, p3

    .line 12
    .line 13
    mul-float v6, p6, p3

    .line 14
    .line 15
    mul-float v7, p7, p3

    .line 16
    .line 17
    const/4 v8, 0x0

    .line 18
    cmpl-float v9, v3, v8

    .line 19
    .line 20
    if-eqz v9, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 23
    .line 24
    .line 25
    :cond_0
    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 26
    .line 27
    .line 28
    move-result-object v10

    .line 29
    iget v11, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 30
    .line 31
    iget v12, v10, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 32
    .line 33
    sub-float/2addr v11, v12

    .line 34
    float-to-double v11, v11

    .line 35
    invoke-static {v11, v12}, Ljava/lang/Math;->ceil(D)D

    .line 36
    .line 37
    .line 38
    move-result-wide v11

    .line 39
    double-to-float v11, v11

    .line 40
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 41
    .line 42
    .line 43
    move-result v12

    .line 44
    cmpg-float v12, v12, v6

    .line 45
    .line 46
    if-gez v12, :cond_1

    .line 47
    .line 48
    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 49
    .line 50
    .line 51
    goto/16 :goto_2

    .line 52
    .line 53
    :cond_1
    const/4 v12, 0x0

    .line 54
    new-array v13, v12, [F

    .line 55
    .line 56
    move v14, v5

    .line 57
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 58
    .line 59
    .line 60
    move-result v15

    .line 61
    if-lez v15, :cond_4

    .line 62
    .line 63
    add-float v15, v8, v11

    .line 64
    .line 65
    cmpg-float v16, v15, v7

    .line 66
    .line 67
    if-gtz v16, :cond_4

    .line 68
    .line 69
    const/4 v12, 0x1

    .line 70
    invoke-virtual {v2, v1, v12, v6, v13}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    .line 71
    .line 72
    .line 73
    move-result v16

    .line 74
    move/from16 p4, v6

    .line 75
    .line 76
    if-nez v16, :cond_2

    .line 77
    .line 78
    move-object/from16 v16, v13

    .line 79
    .line 80
    const/4 v6, 0x0

    .line 81
    goto :goto_1

    .line 82
    :cond_2
    move/from16 v12, v16

    .line 83
    .line 84
    const/4 v6, 0x0

    .line 85
    move-object/from16 v16, v13

    .line 86
    .line 87
    :goto_1
    invoke-virtual {v1, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v13

    .line 91
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 92
    .line 93
    .line 94
    move-result v6

    .line 95
    invoke-virtual {v1, v12, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 100
    .line 101
    .line 102
    move-result v6

    .line 103
    if-lez v6, :cond_3

    .line 104
    .line 105
    const/high16 v6, 0x40000000    # 2.0f

    .line 106
    .line 107
    mul-float v6, v6, v11

    .line 108
    .line 109
    add-float/2addr v8, v6

    .line 110
    cmpl-float v6, v8, v7

    .line 111
    .line 112
    if-lez v6, :cond_3

    .line 113
    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    .line 115
    .line 116
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    .line 120
    .line 121
    .line 122
    move-result v6

    .line 123
    const/4 v7, 0x1

    .line 124
    sub-int/2addr v6, v7

    .line 125
    const/4 v8, 0x0

    .line 126
    invoke-virtual {v13, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v6

    .line 130
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string v6, "..."

    .line 134
    .line 135
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    iget v6, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 143
    .line 144
    add-float/2addr v14, v6

    .line 145
    invoke-virtual {v0, v1, v4, v14, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 146
    .line 147
    .line 148
    goto :goto_2

    .line 149
    :cond_3
    const/4 v8, 0x0

    .line 150
    iget v6, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 151
    .line 152
    add-float/2addr v6, v14

    .line 153
    invoke-virtual {v0, v13, v4, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 154
    .line 155
    .line 156
    add-float/2addr v14, v11

    .line 157
    move/from16 v6, p4

    .line 158
    .line 159
    move v8, v15

    .line 160
    move-object/from16 v13, v16

    .line 161
    .line 162
    const/4 v12, 0x0

    .line 163
    goto :goto_0

    .line 164
    :cond_4
    :goto_2
    if-eqz v9, :cond_5

    .line 165
    .line 166
    neg-float v1, v3

    .line 167
    invoke-virtual {v0, v1, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 168
    .line 169
    .line 170
    :cond_5
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public getCategoryAxisTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getExceed(FLcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;II)Z
    .locals 2

    .line 1
    int-to-float p3, p3

    .line 2
    const/4 v0, 0x1

    .line 3
    const/4 v1, 0x0

    .line 4
    cmpl-float p3, p1, p3

    .line 5
    .line 6
    if-lez p3, :cond_0

    .line 7
    .line 8
    const/4 p3, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p3, 0x0

    .line 11
    :goto_0
    invoke-virtual {p0, p2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->isVertical(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Z

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-eqz p2, :cond_2

    .line 16
    .line 17
    int-to-float p2, p4

    .line 18
    cmpl-float p1, p1, p2

    .line 19
    .line 20
    if-lez p1, :cond_1

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    :goto_1
    move p3, v0

    .line 25
    :cond_2
    return p3
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getLegendAutoSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowLegend()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendTextSize()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    mul-float v0, v0, v2

    .line 18
    .line 19
    invoke-virtual {p5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 20
    .line 21
    .line 22
    array-length v0, p2

    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v2, 0x0

    .line 32
    const/high16 v3, -0x40800000    # -1.0f

    .line 33
    .line 34
    const/high16 v4, -0x40800000    # -1.0f

    .line 35
    .line 36
    const/4 v5, 0x0

    .line 37
    :goto_0
    if-ge v5, v0, :cond_1

    .line 38
    .line 39
    aget-object v6, p2, v5

    .line 40
    .line 41
    const-string v7, "\n"

    .line 42
    .line 43
    const-string v8, " "

    .line 44
    .line 45
    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    invoke-virtual {p5}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 50
    .line 51
    .line 52
    move-result-object v7

    .line 53
    iget v8, v7, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 54
    .line 55
    iget v7, v7, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 56
    .line 57
    sub-float/2addr v8, v7

    .line 58
    float-to-double v7, v8

    .line 59
    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    .line 60
    .line 61
    .line 62
    move-result-wide v7

    .line 63
    double-to-float v7, v7

    .line 64
    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    invoke-virtual {p5, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    invoke-static {v6, v3}, Ljava/lang/Math;->max(FF)F

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    add-int/lit8 v5, v5, 0x1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendShapeWidth(I)I

    .line 80
    .line 81
    .line 82
    move-result p2

    .line 83
    int-to-float p2, p2

    .line 84
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    mul-float p2, p2, p1

    .line 89
    .line 90
    const/high16 p1, 0x40000000    # 2.0f

    .line 91
    .line 92
    mul-float p2, p2, p1

    .line 93
    .line 94
    int-to-float p1, p4

    .line 95
    invoke-virtual {p0, p1}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getMaxLegendHeight(F)I

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    int-to-float p3, p3

    .line 100
    invoke-virtual {p0, p3}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getMaxLegendWidth(F)I

    .line 101
    .line 102
    .line 103
    move-result p3

    .line 104
    int-to-float p4, p3

    .line 105
    sub-float p5, p4, p2

    .line 106
    .line 107
    add-float/2addr p2, v3

    .line 108
    float-to-double v5, p2

    .line 109
    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    .line 110
    .line 111
    .line 112
    move-result-wide v5

    .line 113
    double-to-int p2, v5

    .line 114
    float-to-double v4, v4

    .line 115
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    .line 116
    .line 117
    .line 118
    move-result-wide v4

    .line 119
    double-to-int v2, v4

    .line 120
    cmpl-float v4, v3, p5

    .line 121
    .line 122
    if-lez v4, :cond_2

    .line 123
    .line 124
    div-float/2addr v3, p5

    .line 125
    float-to-double p4, v3

    .line 126
    invoke-static {p4, p5}, Ljava/lang/Math;->ceil(D)D

    .line 127
    .line 128
    .line 129
    move-result-wide p4

    .line 130
    double-to-int p2, p4

    .line 131
    new-instance p4, Lcom/intsig/office/java/awt/Rectangle;

    .line 132
    .line 133
    mul-int v2, v2, p2

    .line 134
    .line 135
    mul-int v2, v2, v0

    .line 136
    .line 137
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    .line 138
    .line 139
    .line 140
    move-result p1

    .line 141
    invoke-direct {p4, p3, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 142
    .line 143
    .line 144
    iput-object p4, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 145
    .line 146
    goto/16 :goto_3

    .line 147
    .line 148
    :cond_2
    iget-byte p5, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 149
    .line 150
    if-eqz p5, :cond_7

    .line 151
    .line 152
    const/4 v3, 0x2

    .line 153
    const/4 v4, 0x1

    .line 154
    if-eq p5, v4, :cond_3

    .line 155
    .line 156
    if-eq p5, v3, :cond_7

    .line 157
    .line 158
    const/4 v4, 0x3

    .line 159
    if-eq p5, v4, :cond_3

    .line 160
    .line 161
    return-object v1

    .line 162
    :cond_3
    int-to-float p5, p2

    .line 163
    div-float/2addr p4, p5

    .line 164
    float-to-int p4, p4

    .line 165
    if-le v0, p4, :cond_6

    .line 166
    .line 167
    int-to-float p4, v0

    .line 168
    int-to-float p5, v3

    .line 169
    div-float p5, p4, p5

    .line 170
    .line 171
    float-to-double v4, p5

    .line 172
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    .line 173
    .line 174
    .line 175
    move-result-wide v4

    .line 176
    :goto_1
    double-to-int p5, v4

    .line 177
    mul-int v1, p5, p2

    .line 178
    .line 179
    if-le v1, p3, :cond_4

    .line 180
    .line 181
    add-int/lit8 v3, v3, 0x1

    .line 182
    .line 183
    int-to-float p5, v3

    .line 184
    div-float p5, p4, p5

    .line 185
    .line 186
    float-to-double v4, p5

    .line 187
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    .line 188
    .line 189
    .line 190
    move-result-wide v4

    .line 191
    goto :goto_1

    .line 192
    :cond_4
    div-int p3, v0, p5

    .line 193
    .line 194
    mul-int p3, p3, p5

    .line 195
    .line 196
    sub-int/2addr v0, p3

    .line 197
    :goto_2
    add-int/lit8 p3, p5, -0x1

    .line 198
    .line 199
    if-ge v0, p3, :cond_5

    .line 200
    .line 201
    int-to-float v0, p3

    .line 202
    div-float v0, p4, v0

    .line 203
    .line 204
    float-to-double v0, v0

    .line 205
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 206
    .line 207
    .line 208
    move-result-wide v0

    .line 209
    double-to-int v0, v0

    .line 210
    if-ne v0, v3, :cond_5

    .line 211
    .line 212
    add-int/lit8 v0, v3, -0x1

    .line 213
    .line 214
    move p5, p3

    .line 215
    goto :goto_2

    .line 216
    :cond_5
    new-instance p3, Lcom/intsig/office/java/awt/Rectangle;

    .line 217
    .line 218
    mul-int p2, p2, p5

    .line 219
    .line 220
    mul-int v2, v2, v3

    .line 221
    .line 222
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    .line 223
    .line 224
    .line 225
    move-result p1

    .line 226
    invoke-direct {p3, p2, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 227
    .line 228
    .line 229
    iput-object p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 230
    .line 231
    goto :goto_3

    .line 232
    :cond_6
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 233
    .line 234
    mul-int p2, p2, v0

    .line 235
    .line 236
    invoke-direct {p1, p2, v2}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 237
    .line 238
    .line 239
    iput-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 240
    .line 241
    goto :goto_3

    .line 242
    :cond_7
    new-instance p3, Lcom/intsig/office/java/awt/Rectangle;

    .line 243
    .line 244
    mul-int v2, v2, v0

    .line 245
    .line 246
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    invoke-direct {p3, p2, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 251
    .line 252
    .line 253
    iput-object p3, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 254
    .line 255
    :goto_3
    iget-object p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendArea:Lcom/intsig/office/java/awt/Rectangle;

    .line 256
    .line 257
    return-object p1
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public getLegendPosition()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getLegendShapeWidth(I)I
.end method

.method public getMaxLegendHeight(F)I
    .locals 2

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v1, 0x2

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const v0, 0x3eb33333    # 0.35f

    .line 10
    .line 11
    .line 12
    mul-float p1, p1, v0

    .line 13
    .line 14
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    return p1

    .line 19
    :cond_1
    :goto_0
    const v0, 0x3f666666    # 0.9f

    .line 20
    .line 21
    .line 22
    mul-float p1, p1, v0

    .line 23
    .line 24
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    return p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getMaxLegendWidth(F)I
    .locals 2

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v1, 0x2

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const v0, 0x3f666666    # 0.9f

    .line 10
    .line 11
    .line 12
    mul-float p1, p1, v0

    .line 13
    .line 14
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    return p1

    .line 19
    :cond_1
    :goto_0
    const v0, 0x3eb33333    # 0.35f

    .line 20
    .line 21
    .line 22
    mul-float p1, p1, v0

    .line 23
    .line 24
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    return p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getMaxTitleAreaSize(II)Lcom/intsig/office/java/awt/Rectangle;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    int-to-float p1, p1

    .line 4
    const v1, 0x3f4ccccd    # 0.8f

    .line 5
    .line 6
    .line 7
    mul-float p1, p1, v1

    .line 8
    .line 9
    float-to-int p1, p1

    .line 10
    div-int/lit8 p2, p2, 0x2

    .line 11
    .line 12
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getSingleAutoLegendSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;[Ljava/lang/String;Landroid/graphics/Paint;I)Lcom/intsig/office/java/awt/Rectangle;
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getLegendTextSize()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 12
    .line 13
    .line 14
    array-length v0, p2

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/high16 v1, -0x40800000    # -1.0f

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    const/high16 v3, -0x40800000    # -1.0f

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    :goto_0
    if-ge v4, v0, :cond_0

    .line 30
    .line 31
    aget-object v5, p2, v4

    .line 32
    .line 33
    const-string v6, "\n"

    .line 34
    .line 35
    const-string v7, " "

    .line 36
    .line 37
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {p3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 42
    .line 43
    .line 44
    move-result-object v6

    .line 45
    iget v7, v6, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 46
    .line 47
    iget v6, v6, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 48
    .line 49
    sub-float/2addr v7, v6

    .line 50
    float-to-double v6, v7

    .line 51
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 52
    .line 53
    .line 54
    move-result-wide v6

    .line 55
    double-to-float v6, v6

    .line 56
    invoke-static {v6, v3}, Ljava/lang/Math;->max(FF)F

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    add-int/lit8 v4, v4, 0x1

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_0
    int-to-float p2, p4

    .line 72
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendShapeWidth(I)I

    .line 73
    .line 74
    .line 75
    move-result p3

    .line 76
    int-to-float p3, p3

    .line 77
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    mul-float p3, p3, v0

    .line 82
    .line 83
    const/high16 v0, 0x40000000    # 2.0f

    .line 84
    .line 85
    mul-float p3, p3, v0

    .line 86
    .line 87
    sub-float/2addr p2, p3

    .line 88
    cmpl-float p3, v1, p2

    .line 89
    .line 90
    if-lez p3, :cond_1

    .line 91
    .line 92
    div-float/2addr v1, p2

    .line 93
    float-to-double p1, v1

    .line 94
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    .line 95
    .line 96
    .line 97
    move-result-wide p1

    .line 98
    double-to-int p1, p1

    .line 99
    new-instance p2, Lcom/intsig/office/java/awt/Rectangle;

    .line 100
    .line 101
    float-to-double v0, v3

    .line 102
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 103
    .line 104
    .line 105
    move-result-wide v0

    .line 106
    double-to-int p3, v0

    .line 107
    mul-int p3, p3, p1

    .line 108
    .line 109
    invoke-direct {p2, p4, p3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 110
    .line 111
    .line 112
    return-object p2

    .line 113
    :cond_1
    new-instance p2, Lcom/intsig/office/java/awt/Rectangle;

    .line 114
    .line 115
    invoke-virtual {p0, v2}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getLegendShapeWidth(I)I

    .line 116
    .line 117
    .line 118
    move-result p3

    .line 119
    int-to-float p3, p3

    .line 120
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 121
    .line 122
    .line 123
    move-result p1

    .line 124
    mul-float p3, p3, p1

    .line 125
    .line 126
    mul-float p3, p3, v0

    .line 127
    .line 128
    add-float/2addr v1, p3

    .line 129
    float-to-double p3, v1

    .line 130
    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    .line 131
    .line 132
    .line 133
    move-result-wide p3

    .line 134
    double-to-int p1, p3

    .line 135
    float-to-double p3, v3

    .line 136
    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    .line 137
    .line 138
    .line 139
    move-result-wide p3

    .line 140
    double-to-int p3, p3

    .line 141
    invoke-direct {p2, p1, p3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 142
    .line 143
    .line 144
    return-object p2
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public getTextSize(Ljava/lang/String;FFFLandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 6

    .line 1
    if-eqz p1, :cond_5

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    goto/16 :goto_3

    .line 10
    .line 11
    :cond_0
    invoke-virtual {p5, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p5}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    iget v0, p2, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 19
    .line 20
    iget p2, p2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 21
    .line 22
    sub-float/2addr v0, p2

    .line 23
    float-to-double v0, v0

    .line 24
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 25
    .line 26
    .line 27
    move-result-wide v0

    .line 28
    double-to-float p2, v0

    .line 29
    invoke-virtual {p5, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    cmpg-float v1, v0, p3

    .line 34
    .line 35
    if-gez v1, :cond_1

    .line 36
    .line 37
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 38
    .line 39
    float-to-double p3, v0

    .line 40
    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    .line 41
    .line 42
    .line 43
    move-result-wide p3

    .line 44
    double-to-int p3, p3

    .line 45
    float-to-double p4, p2

    .line 46
    invoke-static {p4, p5}, Ljava/lang/Math;->ceil(D)D

    .line 47
    .line 48
    .line 49
    move-result-wide p4

    .line 50
    double-to-int p2, p4

    .line 51
    invoke-direct {p1, p3, p2}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 52
    .line 53
    .line 54
    return-object p1

    .line 55
    :cond_1
    const/4 v0, 0x0

    .line 56
    new-array v1, v0, [F

    .line 57
    .line 58
    const/4 v2, 0x0

    .line 59
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    if-lez v3, :cond_4

    .line 64
    .line 65
    add-float v3, v2, p2

    .line 66
    .line 67
    cmpg-float v4, v3, p4

    .line 68
    .line 69
    if-gtz v4, :cond_4

    .line 70
    .line 71
    const/4 v4, 0x1

    .line 72
    invoke-virtual {p5, p1, v4, p3, v1}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    if-nez v5, :cond_2

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_2
    move v4, v5

    .line 80
    :goto_1
    invoke-virtual {p1, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 84
    .line 85
    .line 86
    move-result v5

    .line 87
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    if-lez v4, :cond_3

    .line 96
    .line 97
    const/high16 v4, 0x40000000    # 2.0f

    .line 98
    .line 99
    mul-float v4, v4, p2

    .line 100
    .line 101
    add-float/2addr v2, v4

    .line 102
    cmpl-float v2, v2, p4

    .line 103
    .line 104
    if-lez v2, :cond_3

    .line 105
    .line 106
    move v2, v3

    .line 107
    goto :goto_2

    .line 108
    :cond_3
    move v2, v3

    .line 109
    goto :goto_0

    .line 110
    :cond_4
    :goto_2
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 111
    .line 112
    float-to-double p2, p3

    .line 113
    invoke-static {p2, p3}, Ljava/lang/Math;->ceil(D)D

    .line 114
    .line 115
    .line 116
    move-result-wide p2

    .line 117
    double-to-int p2, p2

    .line 118
    float-to-double p3, v2

    .line 119
    invoke-static {p3, p4}, Ljava/lang/Math;->ceil(D)D

    .line 120
    .line 121
    .line 122
    move-result-wide p3

    .line 123
    double-to-int p3, p3

    .line 124
    invoke-direct {p1, p2, p3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(II)V

    .line 125
    .line 126
    .line 127
    return-object p1

    .line 128
    :cond_5
    :goto_3
    const/4 p1, 0x0

    .line 129
    return-object p1
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public getTitleTextAreaSize(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;IILandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->isShowChartTitle()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    int-to-float p2, p2

    .line 8
    const v0, 0x3f4ccccd    # 0.8f

    .line 9
    .line 10
    .line 11
    mul-float v4, p2, v0

    .line 12
    .line 13
    int-to-float p2, p3

    .line 14
    const/high16 p3, 0x3f000000    # 0.5f

    .line 15
    .line 16
    mul-float v5, p2, p3

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitle()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getChartTitleTextSize()F

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getZoomRate()F

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    mul-float v3, p2, p1

    .line 31
    .line 32
    move-object v1, p0

    .line 33
    move-object v6, p4

    .line 34
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->getTextSize(Ljava/lang/String;FFFLandroid/graphics/Paint;)Lcom/intsig/office/java/awt/Rectangle;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    return-object p1

    .line 39
    :cond_0
    const/4 p1, 0x0

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public abstract getZoomRate()F
.end method

.method protected isVertical(Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getOrientation()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    sget-object v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;->VERTICAL:Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer$Orientation;

    .line 12
    .line 13
    if-ne p1, v0, :cond_0

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCategoryAxisTextColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->categoryAxisTextColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLegendPosition(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->legendPos:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract setZoomRate(F)V
.end method

.class public final Lcom/intsig/resource/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/resource/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_baground_cs:I = 0x7f08004f

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080050

.field public static final abc_action_bar_item_background_material:I = 0x7f080051

.field public static final abc_btn_borderless_material:I = 0x7f080052

.field public static final abc_btn_check_material:I = 0x7f080053

.field public static final abc_btn_check_material_anim:I = 0x7f080054

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080055

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080056

.field public static final abc_btn_colored_material:I = 0x7f080057

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080058

.field public static final abc_btn_radio_material:I = 0x7f080059

.field public static final abc_btn_radio_material_anim:I = 0x7f08005a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08005b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08005c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08005d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08005e

.field public static final abc_cab_background_internal_bg:I = 0x7f08005f

.field public static final abc_cab_background_top_material:I = 0x7f080060

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080061

.field public static final abc_control_background_material:I = 0x7f080062

.field public static final abc_dialog_material_background:I = 0x7f080063

.field public static final abc_edit_text_material:I = 0x7f080064

.field public static final abc_ic_ab_back_material:I = 0x7f080065

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080066

.field public static final abc_ic_clear_material:I = 0x7f080067

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080068

.field public static final abc_ic_go_search_api_material:I = 0x7f080069

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08006a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08006b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08006c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08006d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08006e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08006f

.field public static final abc_ic_search_api_material:I = 0x7f080070

.field public static final abc_ic_voice_search_api_material:I = 0x7f080071

.field public static final abc_item_background_holo_dark:I = 0x7f080072

.field public static final abc_item_background_holo_light:I = 0x7f080073

.field public static final abc_list_divider_material:I = 0x7f080074

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f080075

.field public static final abc_list_focused_holo:I = 0x7f080076

.field public static final abc_list_longpressed_holo:I = 0x7f080077

.field public static final abc_list_pressed_holo_dark:I = 0x7f080078

.field public static final abc_list_pressed_holo_light:I = 0x7f080079

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f08007a

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f08007b

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f08007c

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f08007d

.field public static final abc_list_selector_holo_dark:I = 0x7f08007e

.field public static final abc_list_selector_holo_light:I = 0x7f08007f

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080080

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080081

.field public static final abc_ratingbar_indicator_material:I = 0x7f080082

.field public static final abc_ratingbar_material:I = 0x7f080083

.field public static final abc_ratingbar_small_material:I = 0x7f080084

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f080085

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f080086

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f080087

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f080088

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f080089

.field public static final abc_seekbar_thumb_material:I = 0x7f08008a

.field public static final abc_seekbar_tick_mark_material:I = 0x7f08008b

.field public static final abc_seekbar_track_material:I = 0x7f08008c

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f08008d

.field public static final abc_spinner_textfield_background_material:I = 0x7f08008e

.field public static final abc_star_black_48dp:I = 0x7f08008f

.field public static final abc_star_half_black_48dp:I = 0x7f080090

.field public static final abc_switch_thumb_material:I = 0x7f080091

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080092

.field public static final abc_tab_indicator_material:I = 0x7f080093

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080094

.field public static final abc_text_cursor_material:I = 0x7f080095

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f080096

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f080097

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f080098

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080099

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f08009a

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f08009b

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f08009c

.field public static final abc_textfield_search_material:I = 0x7f08009d

.field public static final abc_vector_test:I = 0x7f08009e

.field public static final app_splash:I = 0x7f0800df

.field public static final avd_hide_password:I = 0x7f080139

.field public static final avd_show_password:I = 0x7f08013a

.field public static final bg_alert_dialog_common:I = 0x7f0801b0

.field public static final bg_back_to_main:I = 0x7f0801b1

.field public static final bg_bg0_border1:I = 0x7f0801b4

.field public static final bg_blue_round_corner_normal:I = 0x7f0801bb

.field public static final bg_btn_green_text:I = 0x7f0801d3

.field public static final bg_capture_number:I = 0x7f0801e9

.field public static final bg_corner_2_190dacd5:I = 0x7f08020e

.field public static final bg_corner_2_1919bc51:I = 0x7f08020f

.field public static final bg_corner_2_194580f2:I = 0x7f080210

.field public static final bg_corner_2_198150f8:I = 0x7f080211

.field public static final bg_corner_2_bg1:I = 0x7f080213

.field public static final bg_corner_2_f1f1f1:I = 0x7f080215

.field public static final bg_gray_round_corner_disable:I = 0x7f080296

.field public static final bg_guide_btn:I = 0x7f0802a3

.field public static final bg_light_black:I = 0x7f0802bd

.field public static final bg_radio_tab_checked:I = 0x7f0802fa

.field public static final bg_radius_commen_dark_btn:I = 0x7f0802fb

.field public static final bg_start_page:I = 0x7f080332

.field public static final bg_tabhost_green:I = 0x7f080337

.field public static final bg_title_image_text_button:I = 0x7f080342

.field public static final bg_white_round_corner_normal:I = 0x7f080369

.field public static final btn_checkbox_checked_mtrl:I = 0x7f08037f

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080380

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080381

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f080382

.field public static final btn_radio_off_mtrl:I = 0x7f080399

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f08039a

.field public static final btn_radio_on_mtrl:I = 0x7f08039b

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f08039c

.field public static final cs_launcher_300_200:I = 0x7f0803fc

.field public static final design_fab_background:I = 0x7f080410

.field public static final design_ic_visibility:I = 0x7f080411

.field public static final design_ic_visibility_off:I = 0x7f080412

.field public static final design_password_eye:I = 0x7f080413

.field public static final design_snackbar_background:I = 0x7f080414

.field public static final dock_btn:I = 0x7f080422

.field public static final edittext_cursor_drawable:I = 0x7f080458

.field public static final gray_common_btn_bg:I = 0x7f08046e

.field public static final holo_checkbox_bg:I = 0x7f080494

.field public static final holo_common_btn_bg:I = 0x7f080495

.field public static final home_nav_search:I = 0x7f08049c

.field public static final home_select_selected:I = 0x7f08049d

.field public static final home_select_unselected:I = 0x7f08049e

.field public static final ic_arrow_back_black_24:I = 0x7f08050d

.field public static final ic_arrow_left:I = 0x7f080516

.field public static final ic_cancel_16_16:I = 0x7f0805a6

.field public static final ic_clear_black_24:I = 0x7f080621

.field public static final ic_clock_black_24dp:I = 0x7f080627

.field public static final ic_cloud_service_auth_left:I = 0x7f080651

.field public static final ic_cloud_service_auth_right:I = 0x7f080652

.field public static final ic_cloud_sync_1gb_44_44:I = 0x7f080654

.field public static final ic_cloud_sync_44_44:I = 0x7f080655

.field public static final ic_cloud_sync_auto_backup_44_44:I = 0x7f080656

.field public static final ic_cloud_sync_close:I = 0x7f080657

.field public static final ic_cloud_sync_cloud_store_44_44:I = 0x7f080658

.field public static final ic_cloud_sync_diff_device_44_44:I = 0x7f080659

.field public static final ic_cloud_sync_exam_erase_44_44:I = 0x7f08065a

.field public static final ic_cloud_sync_photo_recovery_44_44:I = 0x7f08065b

.field public static final ic_cloud_sync_share_dir_44_44:I = 0x7f08065c

.field public static final ic_cloud_sync_smart_erase_44_44:I = 0x7f08065d

.field public static final ic_done_36:I = 0x7f08073e

.field public static final ic_file_square_unchecked_20px:I = 0x7f0807a2

.field public static final ic_home_search_hint:I = 0x7f0808a5

.field public static final ic_keyboard_black_24dp:I = 0x7f08093a

.field public static final ic_m3_chip_check:I = 0x7f080993

.field public static final ic_m3_chip_checked_circle:I = 0x7f080994

.field public static final ic_m3_chip_close:I = 0x7f080995

.field public static final ic_mtrl_checked_circle:I = 0x7f080a13

.field public static final ic_mtrl_chip_checked_black:I = 0x7f080a14

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080a15

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080a16

.field public static final ic_pdf_gallery_back:I = 0x7f080ab0

.field public static final ic_recycle_scroll_thumb:I = 0x7f080b67

.field public static final ic_search_black_24:I = 0x7f080be0

.field public static final ic_search_cancel:I = 0x7f080be1

.field public static final ic_search_edittext_foucs:I = 0x7f080be4

.field public static final ic_search_edittext_normal:I = 0x7f080be5

.field public static final ic_tick_green:I = 0x7f080cd5

.field public static final list_preference_selector_bg:I = 0x7f080faa

.field public static final list_selector_bg_both_design:I = 0x7f080fac

.field public static final list_selector_default:I = 0x7f080faf

.field public static final list_selector_default_bg_color_1:I = 0x7f080fb0

.field public static final list_selector_default_borderless:I = 0x7f080fb1

.field public static final list_selector_default_new:I = 0x7f080fb2

.field public static final list_selector_default_new_light:I = 0x7f080fb3

.field public static final logo_start_page:I = 0x7f080fbb

.field public static final m3_appbar_background:I = 0x7f080fbc

.field public static final m3_avd_hide_password:I = 0x7f080fbd

.field public static final m3_avd_show_password:I = 0x7f080fbe

.field public static final m3_password_eye:I = 0x7f080fbf

.field public static final m3_popupmenu_background_overlay:I = 0x7f080fc0

.field public static final m3_radiobutton_ripple:I = 0x7f080fc1

.field public static final m3_selection_control_ripple:I = 0x7f080fc2

.field public static final m3_tabs_background:I = 0x7f080fc3

.field public static final m3_tabs_line_indicator:I = 0x7f080fc4

.field public static final m3_tabs_rounded_line_indicator:I = 0x7f080fc5

.field public static final m3_tabs_transparent_background:I = 0x7f080fc6

.field public static final material_cursor_drawable:I = 0x7f080fcb

.field public static final material_ic_calendar_black_24dp:I = 0x7f080fcc

.field public static final material_ic_clear_black_24dp:I = 0x7f080fcd

.field public static final material_ic_edit_black_24dp:I = 0x7f080fce

.field public static final material_ic_keyboard_arrow_left_black_24dp:I = 0x7f080fcf

.field public static final material_ic_keyboard_arrow_next_black_24dp:I = 0x7f080fd0

.field public static final material_ic_keyboard_arrow_previous_black_24dp:I = 0x7f080fd1

.field public static final material_ic_keyboard_arrow_right_black_24dp:I = 0x7f080fd2

.field public static final material_ic_menu_arrow_down_black_24dp:I = 0x7f080fd3

.field public static final material_ic_menu_arrow_up_black_24dp:I = 0x7f080fd4

.field public static final mtrl_bottomsheet_drag_handle:I = 0x7f080fd7

.field public static final mtrl_checkbox_button:I = 0x7f080fd8

.field public static final mtrl_checkbox_button_checked_unchecked:I = 0x7f080fd9

.field public static final mtrl_checkbox_button_icon:I = 0x7f080fda

.field public static final mtrl_checkbox_button_icon_checked_indeterminate:I = 0x7f080fdb

.field public static final mtrl_checkbox_button_icon_checked_unchecked:I = 0x7f080fdc

.field public static final mtrl_checkbox_button_icon_indeterminate_checked:I = 0x7f080fdd

.field public static final mtrl_checkbox_button_icon_indeterminate_unchecked:I = 0x7f080fde

.field public static final mtrl_checkbox_button_icon_unchecked_checked:I = 0x7f080fdf

.field public static final mtrl_checkbox_button_icon_unchecked_indeterminate:I = 0x7f080fe0

.field public static final mtrl_checkbox_button_unchecked_checked:I = 0x7f080fe1

.field public static final mtrl_dialog_background:I = 0x7f080fe2

.field public static final mtrl_dropdown_arrow:I = 0x7f080fe3

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080fe4

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080fe5

.field public static final mtrl_ic_cancel:I = 0x7f080fe6

.field public static final mtrl_ic_check_mark:I = 0x7f080fe7

.field public static final mtrl_ic_checkbox_checked:I = 0x7f080fe8

.field public static final mtrl_ic_checkbox_unchecked:I = 0x7f080fe9

.field public static final mtrl_ic_error:I = 0x7f080fea

.field public static final mtrl_ic_indeterminate:I = 0x7f080feb

.field public static final mtrl_navigation_bar_item_background:I = 0x7f080fec

.field public static final mtrl_popupmenu_background:I = 0x7f080fed

.field public static final mtrl_popupmenu_background_overlay:I = 0x7f080fee

.field public static final mtrl_switch_thumb:I = 0x7f080fef

.field public static final mtrl_switch_thumb_checked:I = 0x7f080ff0

.field public static final mtrl_switch_thumb_checked_pressed:I = 0x7f080ff1

.field public static final mtrl_switch_thumb_checked_unchecked:I = 0x7f080ff2

.field public static final mtrl_switch_thumb_pressed:I = 0x7f080ff3

.field public static final mtrl_switch_thumb_pressed_checked:I = 0x7f080ff4

.field public static final mtrl_switch_thumb_pressed_unchecked:I = 0x7f080ff5

.field public static final mtrl_switch_thumb_unchecked:I = 0x7f080ff6

.field public static final mtrl_switch_thumb_unchecked_checked:I = 0x7f080ff7

.field public static final mtrl_switch_thumb_unchecked_pressed:I = 0x7f080ff8

.field public static final mtrl_switch_track:I = 0x7f080ff9

.field public static final mtrl_switch_track_decoration:I = 0x7f080ffa

.field public static final mtrl_tabs_default_indicator:I = 0x7f080ffb

.field public static final navigation_empty_icon:I = 0x7f080ffc

.field public static final notification_action_background:I = 0x7f081002

.field public static final notification_bg:I = 0x7f081003

.field public static final notification_bg_low:I = 0x7f081004

.field public static final notification_bg_low_normal:I = 0x7f081005

.field public static final notification_bg_low_pressed:I = 0x7f081006

.field public static final notification_bg_normal:I = 0x7f081007

.field public static final notification_bg_normal_pressed:I = 0x7f081008

.field public static final notification_icon_background:I = 0x7f081009

.field public static final notification_template_icon_bg:I = 0x7f08100b

.field public static final notification_template_icon_low_bg:I = 0x7f08100c

.field public static final notification_tile_bg:I = 0x7f08100d

.field public static final notify_panel_notification_icon_bg:I = 0x7f08100e

.field public static final search_edittext_bg:I = 0x7f08105d

.field public static final seekbar_control_disabled_holo:I = 0x7f081061

.field public static final seekbar_control_focused_holo:I = 0x7f081062

.field public static final seekbar_control_normal_holo:I = 0x7f081063

.field public static final seekbar_control_pressed_holo:I = 0x7f081064

.field public static final seekbar_control_selector_holo:I = 0x7f081065

.field public static final seekbar_primary_holo:I = 0x7f081066

.field public static final seekbar_progress_horizontal_holo_light:I = 0x7f081067

.field public static final seekbar_secondary_holo:I = 0x7f081068

.field public static final seekbar_track_holo_light:I = 0x7f08106c

.field public static final shape_bg_ffffff_corner_4_border_f1f1f1_1dp:I = 0x7f08112f

.field public static final test_level_drawable:I = 0x7f081211

.field public static final tooltip_frame_dark:I = 0x7f081214

.field public static final tooltip_frame_light:I = 0x7f081215


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

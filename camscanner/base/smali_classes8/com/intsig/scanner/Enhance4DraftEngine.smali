.class public Lcom/intsig/scanner/Enhance4DraftEngine;
.super Ljava/lang/Object;
.source "Enhance4DraftEngine.java"


# static fields
.field public static final COLOR_BGRA_8888:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COLOR_BGR_888:I = 0x0

.field public static final COLOR_FORMAT_YUV420SP12:I = 0x6

.field public static final COLOR_FORMAT_YUV420SP21:I = 0x7

.field public static final COLOR_FORMAT_YUV_Y:I = 0x5

.field public static final COLOR_GRAY:I = 0x4

.field public static final COLOR_RGB:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COLOR_RGBA_8888:I = 0x3

.field public static final COLOR_RGB_888:I = 0x8

.field public static final DECODE_WITHOUT_EXIF_ROTATE:I = 0x400

.field public static final DEFAULT_JPG_QUALITY:I = 0x50

.field public static final ENCODE_WITH_MOZJPG:I = 0x80

.field public static final ENHANCE_MODE_BLACKBOARD:I = 0xc

.field public static final ENHANCE_MODE_BLACKBOARD_REVERSE:I = 0x33

.field public static final ENHANCE_MODE_BLACKWHITE:I = 0x13

.field public static final ENHANCE_MODE_BW:I = 0xb

.field public static final ENHANCE_MODE_BW_2:I = 0x10

.field public static final ENHANCE_MODE_COLOR:I = 0xd

.field public static final ENHANCE_MODE_COLOR_2:I = 0xf

.field public static final ENHANCE_MODE_ECONOMY:I = 0x2

.field public static final ENHANCE_MODE_GRAY:I = 0xa

.field public static final ENHANCE_MODE_GRAY_2:I = 0x12

.field public static final ENHANCE_MODE_LINEAR:I = 0x0

.field public static final ENHANCE_MODE_MAGIC:I = 0x1

.field public static final ENHANCE_MODE_MAGIC_2:I = 0x11

.field public static final ENHANCE_MODE_MAGIC_LITE:I = 0xe

.field public static final ENHANCE_MODE_MAGIC_WEAK:I = 0x14

.field public static final ENHANCE_MODE_NOTE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Enhance4DraftEngine"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    :try_start_0
    const-string v0, "enhance4draft"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static native enhanceImageS(J)I
.end method

.method public static native saveImageAsPNG(JLjava/lang/String;)I
.end method

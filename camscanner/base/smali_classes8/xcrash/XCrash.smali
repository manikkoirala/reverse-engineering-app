.class public final Lxcrash/XCrash;
.super Ljava/lang/Object;
.source "XCrash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxcrash/XCrash$InitParameters;
    }
.end annotation


# static fields
.field private static O8:Ljava/lang/String; = null

.field private static Oo08:Lxcrash/ILogger; = null

.field public static o〇0:Ljava/lang/String; = null

.field private static 〇080:Z = false

.field private static 〇o00〇〇Oo:Ljava/lang/String;

.field private static 〇o〇:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lxcrash/DefaultLogger;

    .line 2
    .line 3
    invoke-direct {v0}, Lxcrash/DefaultLogger;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lxcrash/XCrash;->Oo08:Lxcrash/ILogger;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    sput-object v0, Lxcrash/XCrash;->o〇0:Ljava/lang/String;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static 〇080()Lxcrash/ILogger;
    .locals 1

    .line 1
    sget-object v0, Lxcrash/XCrash;->Oo08:Lxcrash/ILogger;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static declared-synchronized 〇o00〇〇Oo(Landroid/content/Context;Lxcrash/XCrash$InitParameters;)I
    .locals 33

    .line 1
    const-class v1, Lxcrash/XCrash;

    .line 2
    .line 3
    monitor-enter v1

    .line 4
    :try_start_0
    sget-boolean v0, Lxcrash/XCrash;->〇080:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    monitor-exit v1

    .line 10
    return v2

    .line 11
    :cond_0
    const/4 v0, 0x1

    .line 12
    :try_start_1
    sput-boolean v0, Lxcrash/XCrash;->〇080:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13
    .line 14
    if-nez p0, :cond_1

    .line 15
    .line 16
    monitor-exit v1

    .line 17
    const/4 v0, -0x1

    .line 18
    return v0

    .line 19
    :cond_1
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    if-eqz v3, :cond_2

    .line 24
    .line 25
    move-object v5, v3

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    move-object/from16 v5, p0

    .line 28
    .line 29
    :goto_0
    if-nez p1, :cond_3

    .line 30
    .line 31
    new-instance v3, Lxcrash/XCrash$InitParameters;

    .line 32
    .line 33
    invoke-direct {v3}, Lxcrash/XCrash$InitParameters;-><init>()V

    .line 34
    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_3
    move-object/from16 v3, p1

    .line 38
    .line 39
    :goto_1
    iget-object v4, v3, Lxcrash/XCrash$InitParameters;->O8:Lxcrash/ILogger;

    .line 40
    .line 41
    if-eqz v4, :cond_4

    .line 42
    .line 43
    sput-object v4, Lxcrash/XCrash;->Oo08:Lxcrash/ILogger;

    .line 44
    .line 45
    :cond_4
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    sput-object v4, Lxcrash/XCrash;->〇o00〇〇Oo:Ljava/lang/String;

    .line 50
    .line 51
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    if-eqz v6, :cond_5

    .line 56
    .line 57
    const-string v6, "unknown"

    .line 58
    .line 59
    sput-object v6, Lxcrash/XCrash;->〇o00〇〇Oo:Ljava/lang/String;

    .line 60
    .line 61
    :cond_5
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->〇080:Ljava/lang/String;

    .line 62
    .line 63
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    if-eqz v6, :cond_6

    .line 68
    .line 69
    invoke-static {v5}, Lxcrash/Util;->O8(Landroid/content/Context;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v6

    .line 73
    iput-object v6, v3, Lxcrash/XCrash$InitParameters;->〇080:Ljava/lang/String;

    .line 74
    .line 75
    :cond_6
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->〇080:Ljava/lang/String;

    .line 76
    .line 77
    sput-object v6, Lxcrash/XCrash;->〇o〇:Ljava/lang/String;

    .line 78
    .line 79
    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    .line 84
    .line 85
    sput-object v6, Lxcrash/XCrash;->o〇0:Ljava/lang/String;

    .line 86
    .line 87
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 88
    .line 89
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 90
    .line 91
    .line 92
    move-result v6

    .line 93
    if-eqz v6, :cond_7

    .line 94
    .line 95
    new-instance v6, Ljava/lang/StringBuilder;

    .line 96
    .line 97
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    .line 101
    .line 102
    .line 103
    move-result-object v7

    .line 104
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v7, "/tombstones"

    .line 108
    .line 109
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v6

    .line 116
    iput-object v6, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 117
    .line 118
    :cond_7
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 119
    .line 120
    sput-object v6, Lxcrash/XCrash;->O8:Ljava/lang/String;

    .line 121
    .line 122
    invoke-static {}, Landroid/os/Process;->myPid()I

    .line 123
    .line 124
    .line 125
    move-result v8

    .line 126
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->〇〇888:Z

    .line 127
    .line 128
    if-nez v6, :cond_9

    .line 129
    .line 130
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 131
    .line 132
    if-eqz v6, :cond_8

    .line 133
    .line 134
    goto :goto_2

    .line 135
    :cond_8
    const/4 v4, 0x0

    .line 136
    move-object v9, v4

    .line 137
    goto :goto_3

    .line 138
    :cond_9
    :goto_2
    invoke-static {v5, v8}, Lxcrash/Util;->〇O〇(Landroid/content/Context;I)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v6

    .line 142
    iget-boolean v7, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 143
    .line 144
    if-eqz v7, :cond_b

    .line 145
    .line 146
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 147
    .line 148
    .line 149
    move-result v7

    .line 150
    if-nez v7, :cond_a

    .line 151
    .line 152
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 153
    .line 154
    .line 155
    move-result v4

    .line 156
    if-nez v4, :cond_b

    .line 157
    .line 158
    :cond_a
    iput-boolean v2, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 159
    .line 160
    :cond_b
    move-object v9, v6

    .line 161
    :goto_3
    invoke-static {}, Lxcrash/FileManager;->〇O8o08O()Lxcrash/FileManager;

    .line 162
    .line 163
    .line 164
    move-result-object v10

    .line 165
    iget-object v11, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 166
    .line 167
    iget v12, v3, Lxcrash/XCrash$InitParameters;->〇80〇808〇O:I

    .line 168
    .line 169
    iget v13, v3, Lxcrash/XCrash$InitParameters;->o800o8O:I

    .line 170
    .line 171
    iget v14, v3, Lxcrash/XCrash$InitParameters;->〇o:I

    .line 172
    .line 173
    iget v15, v3, Lxcrash/XCrash$InitParameters;->Oo08:I

    .line 174
    .line 175
    iget v4, v3, Lxcrash/XCrash$InitParameters;->o〇0:I

    .line 176
    .line 177
    iget v6, v3, Lxcrash/XCrash$InitParameters;->〇o〇:I

    .line 178
    .line 179
    move/from16 v16, v4

    .line 180
    .line 181
    move/from16 v17, v6

    .line 182
    .line 183
    invoke-virtual/range {v10 .. v17}, Lxcrash/FileManager;->Oooo8o0〇(Ljava/lang/String;IIIIII)V

    .line 184
    .line 185
    .line 186
    iget-boolean v4, v3, Lxcrash/XCrash$InitParameters;->〇〇888:Z

    .line 187
    .line 188
    if-nez v4, :cond_c

    .line 189
    .line 190
    iget-boolean v4, v3, Lxcrash/XCrash$InitParameters;->〇0〇O0088o:Z

    .line 191
    .line 192
    if-nez v4, :cond_c

    .line 193
    .line 194
    iget-boolean v4, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 195
    .line 196
    if-eqz v4, :cond_d

    .line 197
    .line 198
    :cond_c
    instance-of v4, v5, Landroid/app/Application;

    .line 199
    .line 200
    if-eqz v4, :cond_d

    .line 201
    .line 202
    invoke-static {}, Lxcrash/ActivityMonitor;->O8()Lxcrash/ActivityMonitor;

    .line 203
    .line 204
    .line 205
    move-result-object v4

    .line 206
    move-object v6, v5

    .line 207
    check-cast v6, Landroid/app/Application;

    .line 208
    .line 209
    invoke-virtual {v4, v6}, Lxcrash/ActivityMonitor;->Oo08(Landroid/app/Application;)V

    .line 210
    .line 211
    .line 212
    :cond_d
    iget-boolean v4, v3, Lxcrash/XCrash$InitParameters;->〇〇888:Z

    .line 213
    .line 214
    if-eqz v4, :cond_e

    .line 215
    .line 216
    invoke-static {}, Lxcrash/JavaCrashHandler;->〇o〇()Lxcrash/JavaCrashHandler;

    .line 217
    .line 218
    .line 219
    move-result-object v7

    .line 220
    sget-object v10, Lxcrash/XCrash;->〇o00〇〇Oo:Ljava/lang/String;

    .line 221
    .line 222
    iget-object v11, v3, Lxcrash/XCrash$InitParameters;->〇080:Ljava/lang/String;

    .line 223
    .line 224
    iget-object v12, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 225
    .line 226
    iget-boolean v13, v3, Lxcrash/XCrash$InitParameters;->oO80:Z

    .line 227
    .line 228
    iget v14, v3, Lxcrash/XCrash$InitParameters;->OO0o〇〇〇〇0:I

    .line 229
    .line 230
    iget v15, v3, Lxcrash/XCrash$InitParameters;->〇8o8o〇:I

    .line 231
    .line 232
    iget v4, v3, Lxcrash/XCrash$InitParameters;->〇O8o08O:I

    .line 233
    .line 234
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->OO0o〇〇:Z

    .line 235
    .line 236
    iget-boolean v0, v3, Lxcrash/XCrash$InitParameters;->Oooo8o0〇:Z

    .line 237
    .line 238
    iget-boolean v2, v3, Lxcrash/XCrash$InitParameters;->〇〇808〇:Z

    .line 239
    .line 240
    move-object/from16 p0, v5

    .line 241
    .line 242
    iget v5, v3, Lxcrash/XCrash$InitParameters;->〇O〇:I

    .line 243
    .line 244
    move/from16 v20, v5

    .line 245
    .line 246
    iget-object v5, v3, Lxcrash/XCrash$InitParameters;->〇O00:[Ljava/lang/String;

    .line 247
    .line 248
    move-object/from16 v21, v5

    .line 249
    .line 250
    iget-object v5, v3, Lxcrash/XCrash$InitParameters;->〇〇8O0〇8:Lxcrash/ICrashCallback;

    .line 251
    .line 252
    move/from16 v16, v4

    .line 253
    .line 254
    move/from16 v17, v6

    .line 255
    .line 256
    move/from16 v18, v0

    .line 257
    .line 258
    move/from16 v19, v2

    .line 259
    .line 260
    move-object/from16 v22, v5

    .line 261
    .line 262
    invoke-virtual/range {v7 .. v22}, Lxcrash/JavaCrashHandler;->〇〇888(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIIZZZI[Ljava/lang/String;Lxcrash/ICrashCallback;)V

    .line 263
    .line 264
    .line 265
    goto :goto_4

    .line 266
    :cond_e
    move-object/from16 p0, v5

    .line 267
    .line 268
    :goto_4
    iget-boolean v0, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 269
    .line 270
    iget-boolean v2, v3, Lxcrash/XCrash$InitParameters;->〇0〇O0088o:Z

    .line 271
    .line 272
    if-nez v2, :cond_10

    .line 273
    .line 274
    if-eqz v0, :cond_f

    .line 275
    .line 276
    goto :goto_5

    .line 277
    :cond_f
    const/4 v2, 0x0

    .line 278
    goto/16 :goto_7

    .line 279
    .line 280
    :cond_10
    :goto_5
    invoke-static {}, Lxcrash/NativeHandler;->〇080()Lxcrash/NativeHandler;

    .line 281
    .line 282
    .line 283
    move-result-object v4

    .line 284
    sget-object v7, Lxcrash/XCrash;->〇o00〇〇Oo:Ljava/lang/String;

    .line 285
    .line 286
    iget-object v8, v3, Lxcrash/XCrash$InitParameters;->〇080:Ljava/lang/String;

    .line 287
    .line 288
    iget-object v9, v3, Lxcrash/XCrash$InitParameters;->〇o00〇〇Oo:Ljava/lang/String;

    .line 289
    .line 290
    iget-boolean v10, v3, Lxcrash/XCrash$InitParameters;->〇0〇O0088o:Z

    .line 291
    .line 292
    iget-boolean v11, v3, Lxcrash/XCrash$InitParameters;->OoO8:Z

    .line 293
    .line 294
    iget v12, v3, Lxcrash/XCrash$InitParameters;->〇O888o0o:I

    .line 295
    .line 296
    iget v13, v3, Lxcrash/XCrash$InitParameters;->oo88o8O:I

    .line 297
    .line 298
    iget v14, v3, Lxcrash/XCrash$InitParameters;->〇oo〇:I

    .line 299
    .line 300
    iget-boolean v15, v3, Lxcrash/XCrash$InitParameters;->o〇O8〇〇o:Z

    .line 301
    .line 302
    iget-boolean v0, v3, Lxcrash/XCrash$InitParameters;->〇00:Z

    .line 303
    .line 304
    iget-boolean v2, v3, Lxcrash/XCrash$InitParameters;->O〇8O8〇008:Z

    .line 305
    .line 306
    iget-boolean v5, v3, Lxcrash/XCrash$InitParameters;->O8ooOoo〇:Z

    .line 307
    .line 308
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->〇oOO8O8:Z

    .line 309
    .line 310
    move/from16 v16, v6

    .line 311
    .line 312
    iget v6, v3, Lxcrash/XCrash$InitParameters;->〇0000OOO:I

    .line 313
    .line 314
    move/from16 v17, v6

    .line 315
    .line 316
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->o〇〇0〇:[Ljava/lang/String;

    .line 317
    .line 318
    move-object/from16 v18, v6

    .line 319
    .line 320
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->OOO〇O0:Lxcrash/ICrashCallback;

    .line 321
    .line 322
    move/from16 v19, v5

    .line 323
    .line 324
    iget-boolean v5, v3, Lxcrash/XCrash$InitParameters;->oo〇:Z

    .line 325
    .line 326
    if-eqz v5, :cond_11

    .line 327
    .line 328
    const/16 v23, 0x1

    .line 329
    .line 330
    goto :goto_6

    .line 331
    :cond_11
    const/16 v23, 0x0

    .line 332
    .line 333
    :goto_6
    iget-boolean v5, v3, Lxcrash/XCrash$InitParameters;->O8〇o:Z

    .line 334
    .line 335
    move-object/from16 v20, v6

    .line 336
    .line 337
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->〇00〇8:Z

    .line 338
    .line 339
    move/from16 v21, v6

    .line 340
    .line 341
    iget v6, v3, Lxcrash/XCrash$InitParameters;->o0ooO:I

    .line 342
    .line 343
    move/from16 v22, v6

    .line 344
    .line 345
    iget v6, v3, Lxcrash/XCrash$InitParameters;->o〇8:I

    .line 346
    .line 347
    move/from16 v24, v6

    .line 348
    .line 349
    iget v6, v3, Lxcrash/XCrash$InitParameters;->o8:I

    .line 350
    .line 351
    move/from16 v25, v6

    .line 352
    .line 353
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->Oo8Oo00oo:Z

    .line 354
    .line 355
    move/from16 v26, v6

    .line 356
    .line 357
    iget-boolean v6, v3, Lxcrash/XCrash$InitParameters;->〇〇〇0〇〇0:Z

    .line 358
    .line 359
    move/from16 v27, v6

    .line 360
    .line 361
    iget-object v6, v3, Lxcrash/XCrash$InitParameters;->o〇0OOo〇0:Lxcrash/ICrashCallback;

    .line 362
    .line 363
    iget-object v3, v3, Lxcrash/XCrash$InitParameters;->〇〇0o:Lxcrash/ICrashCallback;

    .line 364
    .line 365
    move/from16 v28, v5

    .line 366
    .line 367
    move-object/from16 v5, p0

    .line 368
    .line 369
    move-object/from16 v32, v6

    .line 370
    .line 371
    move/from16 v29, v25

    .line 372
    .line 373
    move/from16 v30, v26

    .line 374
    .line 375
    move/from16 v31, v27

    .line 376
    .line 377
    const/4 v6, 0x0

    .line 378
    move/from16 v25, v21

    .line 379
    .line 380
    move/from16 v26, v22

    .line 381
    .line 382
    move/from16 v27, v24

    .line 383
    .line 384
    move/from16 v21, v17

    .line 385
    .line 386
    move-object/from16 v22, v18

    .line 387
    .line 388
    move-object/from16 v24, v20

    .line 389
    .line 390
    move/from16 v20, v16

    .line 391
    .line 392
    move/from16 v16, v0

    .line 393
    .line 394
    move/from16 v17, v2

    .line 395
    .line 396
    move/from16 v18, v19

    .line 397
    .line 398
    move/from16 v19, v20

    .line 399
    .line 400
    move/from16 v20, v21

    .line 401
    .line 402
    move-object/from16 v21, v22

    .line 403
    .line 404
    move-object/from16 v22, v24

    .line 405
    .line 406
    move/from16 v24, v28

    .line 407
    .line 408
    move/from16 v28, v29

    .line 409
    .line 410
    move/from16 v29, v30

    .line 411
    .line 412
    move/from16 v30, v31

    .line 413
    .line 414
    move-object/from16 v31, v32

    .line 415
    .line 416
    move-object/from16 v32, v3

    .line 417
    .line 418
    invoke-virtual/range {v4 .. v32}, Lxcrash/NativeHandler;->〇o〇(Landroid/content/Context;Lxcrash/ILibLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIIZZZZZI[Ljava/lang/String;Lxcrash/ICrashCallback;ZZZIIIZZLxcrash/ICrashCallback;Lxcrash/ICrashCallback;)I

    .line 419
    .line 420
    .line 421
    move-result v2

    .line 422
    :goto_7
    invoke-static {}, Lxcrash/FileManager;->〇O8o08O()Lxcrash/FileManager;

    .line 423
    .line 424
    .line 425
    move-result-object v0

    .line 426
    invoke-virtual {v0}, Lxcrash/FileManager;->〇〇808〇()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 427
    .line 428
    .line 429
    monitor-exit v1

    .line 430
    return v2

    .line 431
    :catchall_0
    move-exception v0

    .line 432
    monitor-exit v1

    .line 433
    throw v0
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static 〇o〇(Z)V
    .locals 1

    .line 1
    invoke-static {}, Lxcrash/NativeHandler;->〇080()Lxcrash/NativeHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p0}, Lxcrash/NativeHandler;->Oo08(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

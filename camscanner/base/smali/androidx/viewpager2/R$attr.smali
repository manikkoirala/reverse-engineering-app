.class public final Landroidx/viewpager2/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/viewpager2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alpha:I = 0x7f04005b

.field public static final fastScrollEnabled:I = 0x7f040285

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f040286

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040287

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040295

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f040296

.field public static final font:I = 0x7f0402c7

.field public static final fontProviderAuthority:I = 0x7f0402c9

.field public static final fontProviderCerts:I = 0x7f0402ca

.field public static final fontProviderFetchStrategy:I = 0x7f0402cb

.field public static final fontProviderFetchTimeout:I = 0x7f0402cc

.field public static final fontProviderPackage:I = 0x7f0402cd

.field public static final fontProviderQuery:I = 0x7f0402ce

.field public static final fontStyle:I = 0x7f0402d0

.field public static final fontVariationSettings:I = 0x7f0402d1

.field public static final fontWeight:I = 0x7f0402d2

.field public static final layoutManager:I = 0x7f040377

.field public static final recyclerViewStyle:I = 0x7f0404fb

.field public static final reverseLayout:I = 0x7f040504

.field public static final spanCount:I = 0x7f04057a

.field public static final stackFromEnd:I = 0x7f040587

.field public static final ttcIndex:I = 0x7f040697


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

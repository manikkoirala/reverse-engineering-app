.class public abstract Landroidx/camera/core/impl/CamcorderProfileProxy;
.super Ljava/lang/Object;
.source "CamcorderProfileProxy.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# static fields
.field public static CODEC_PROFILE_NONE:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static create(IIIIIIIIIIII)Landroidx/camera/core/impl/CamcorderProfileProxy;
    .locals 14
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v13, Landroidx/camera/core/impl/AutoValue_CamcorderProfileProxy;

    .line 2
    .line 3
    move-object v0, v13

    .line 4
    move v1, p0

    .line 5
    move v2, p1

    .line 6
    move/from16 v3, p2

    .line 7
    .line 8
    move/from16 v4, p3

    .line 9
    .line 10
    move/from16 v5, p4

    .line 11
    .line 12
    move/from16 v6, p5

    .line 13
    .line 14
    move/from16 v7, p6

    .line 15
    .line 16
    move/from16 v8, p7

    .line 17
    .line 18
    move/from16 v9, p8

    .line 19
    .line 20
    move/from16 v10, p9

    .line 21
    .line 22
    move/from16 v11, p10

    .line 23
    .line 24
    move/from16 v12, p11

    .line 25
    .line 26
    invoke-direct/range {v0 .. v12}, Landroidx/camera/core/impl/AutoValue_CamcorderProfileProxy;-><init>(IIIIIIIIIIII)V

    .line 27
    .line 28
    .line 29
    return-object v13
.end method

.method public static fromCamcorderProfile(Landroid/media/CamcorderProfile;)Landroidx/camera/core/impl/CamcorderProfileProxy;
    .locals 14
    .param p0    # Landroid/media/CamcorderProfile;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v13, Landroidx/camera/core/impl/AutoValue_CamcorderProfileProxy;

    .line 2
    .line 3
    iget v1, p0, Landroid/media/CamcorderProfile;->duration:I

    .line 4
    .line 5
    iget v2, p0, Landroid/media/CamcorderProfile;->quality:I

    .line 6
    .line 7
    iget v3, p0, Landroid/media/CamcorderProfile;->fileFormat:I

    .line 8
    .line 9
    iget v4, p0, Landroid/media/CamcorderProfile;->videoCodec:I

    .line 10
    .line 11
    iget v5, p0, Landroid/media/CamcorderProfile;->videoBitRate:I

    .line 12
    .line 13
    iget v6, p0, Landroid/media/CamcorderProfile;->videoFrameRate:I

    .line 14
    .line 15
    iget v7, p0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    .line 16
    .line 17
    iget v8, p0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    .line 18
    .line 19
    iget v9, p0, Landroid/media/CamcorderProfile;->audioCodec:I

    .line 20
    .line 21
    iget v10, p0, Landroid/media/CamcorderProfile;->audioBitRate:I

    .line 22
    .line 23
    iget v11, p0, Landroid/media/CamcorderProfile;->audioSampleRate:I

    .line 24
    .line 25
    iget v12, p0, Landroid/media/CamcorderProfile;->audioChannels:I

    .line 26
    .line 27
    move-object v0, v13

    .line 28
    invoke-direct/range {v0 .. v12}, Landroidx/camera/core/impl/AutoValue_CamcorderProfileProxy;-><init>(IIIIIIIIIIII)V

    .line 29
    .line 30
    .line 31
    return-object v13
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method


# virtual methods
.method public abstract getAudioBitRate()I
.end method

.method public abstract getAudioChannels()I
.end method

.method public abstract getAudioCodec()I
.end method

.method public getAudioCodecMimeType()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/camera/core/impl/CamcorderProfileProxy;->getAudioCodec()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    packed-switch v0, :pswitch_data_0

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    return-object v0

    .line 10
    :pswitch_0
    const-string v0, "audio/opus"

    .line 11
    .line 12
    return-object v0

    .line 13
    :pswitch_1
    const-string v0, "audio/vorbis"

    .line 14
    .line 15
    return-object v0

    .line 16
    :pswitch_2
    const-string v0, "audio/mp4a-latm"

    .line 17
    .line 18
    return-object v0

    .line 19
    :pswitch_3
    const-string v0, "audio/amr-wb"

    .line 20
    .line 21
    return-object v0

    .line 22
    :pswitch_4
    const-string v0, "audio/3gpp"

    .line 23
    .line 24
    return-object v0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public abstract getAudioSampleRate()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getFileFormat()I
.end method

.method public abstract getQuality()I
.end method

.method public getRequiredAudioProfile()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/camera/core/impl/CamcorderProfileProxy;->getAudioCodec()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x3

    .line 6
    if-eq v0, v1, :cond_2

    .line 7
    .line 8
    const/4 v1, 0x4

    .line 9
    const/4 v2, 0x5

    .line 10
    if-eq v0, v1, :cond_1

    .line 11
    .line 12
    if-eq v0, v2, :cond_0

    .line 13
    .line 14
    sget v0, Landroidx/camera/core/impl/CamcorderProfileProxy;->CODEC_PROFILE_NONE:I

    .line 15
    .line 16
    return v0

    .line 17
    :cond_0
    const/16 v0, 0x27

    .line 18
    .line 19
    return v0

    .line 20
    :cond_1
    return v2

    .line 21
    :cond_2
    const/4 v0, 0x2

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public abstract getVideoBitRate()I
.end method

.method public abstract getVideoCodec()I
.end method

.method public getVideoCodecMimeType()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/camera/core/impl/CamcorderProfileProxy;->getVideoCodec()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_4

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-eq v0, v1, :cond_3

    .line 10
    .line 11
    const/4 v1, 0x3

    .line 12
    if-eq v0, v1, :cond_2

    .line 13
    .line 14
    const/4 v1, 0x4

    .line 15
    if-eq v0, v1, :cond_1

    .line 16
    .line 17
    const/4 v1, 0x5

    .line 18
    if-eq v0, v1, :cond_0

    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    return-object v0

    .line 22
    :cond_0
    const-string v0, "video/hevc"

    .line 23
    .line 24
    return-object v0

    .line 25
    :cond_1
    const-string v0, "video/x-vnd.on2.vp8"

    .line 26
    .line 27
    return-object v0

    .line 28
    :cond_2
    const-string v0, "video/mp4v-es"

    .line 29
    .line 30
    return-object v0

    .line 31
    :cond_3
    const-string v0, "video/avc"

    .line 32
    .line 33
    return-object v0

    .line 34
    :cond_4
    const-string v0, "video/3gpp"

    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public abstract getVideoFrameHeight()I
.end method

.method public abstract getVideoFrameRate()I
.end method

.method public abstract getVideoFrameWidth()I
.end method

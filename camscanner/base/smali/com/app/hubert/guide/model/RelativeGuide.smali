.class public Lcom/app/hubert/guide/model/RelativeGuide;
.super Ljava/lang/Object;
.source "RelativeGuide.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;
    }
.end annotation


# instance fields
.field public O8:I

.field public 〇080:Lcom/app/hubert/guide/model/HighLight;

.field public 〇o00〇〇Oo:I
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation
.end field

.field public 〇o〇:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o00〇〇Oo:I

    .line 5
    .line 6
    iput p2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->O8:I

    .line 7
    .line 8
    iput p3, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private 〇o00〇〇Oo(ILandroid/view/ViewGroup;Landroid/view/View;)Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;
    .locals 3

    .line 1
    new-instance p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;

    .line 2
    .line 3
    invoke-direct {p3}, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇080:Lcom/app/hubert/guide/model/HighLight;

    .line 7
    .line 8
    invoke-interface {v0, p2}, Lcom/app/hubert/guide/model/HighLight;->〇080(Landroid/view/View;)Landroid/graphics/RectF;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x3

    .line 13
    const/4 v2, 0x5

    .line 14
    if-eq p1, v1, :cond_3

    .line 15
    .line 16
    if-eq p1, v2, :cond_2

    .line 17
    .line 18
    const/16 v1, 0x30

    .line 19
    .line 20
    const/16 v2, 0x50

    .line 21
    .line 22
    if-eq p1, v1, :cond_1

    .line 23
    .line 24
    if-eq p1, v2, :cond_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 28
    .line 29
    iget p2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇:I

    .line 30
    .line 31
    int-to-float p2, p2

    .line 32
    add-float/2addr p1, p2

    .line 33
    float-to-int p1, p1

    .line 34
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o00〇〇Oo:I

    .line 35
    .line 36
    iget p1, v0, Landroid/graphics/RectF;->left:F

    .line 37
    .line 38
    float-to-int p1, p1

    .line 39
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇080:I

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    iput v2, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->Oo08:I

    .line 43
    .line 44
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    int-to-float p1, p1

    .line 49
    iget p2, v0, Landroid/graphics/RectF;->top:F

    .line 50
    .line 51
    sub-float/2addr p1, p2

    .line 52
    iget p2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇:I

    .line 53
    .line 54
    int-to-float p2, p2

    .line 55
    add-float/2addr p1, p2

    .line 56
    float-to-int p1, p1

    .line 57
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->O8:I

    .line 58
    .line 59
    iget p1, v0, Landroid/graphics/RectF;->left:F

    .line 60
    .line 61
    float-to-int p1, p1

    .line 62
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇080:I

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    iget p1, v0, Landroid/graphics/RectF;->right:F

    .line 66
    .line 67
    iget p2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇:I

    .line 68
    .line 69
    int-to-float p2, p2

    .line 70
    add-float/2addr p1, p2

    .line 71
    float-to-int p1, p1

    .line 72
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇080:I

    .line 73
    .line 74
    iget p1, v0, Landroid/graphics/RectF;->top:F

    .line 75
    .line 76
    float-to-int p1, p1

    .line 77
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o00〇〇Oo:I

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_3
    iput v2, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->Oo08:I

    .line 81
    .line 82
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 83
    .line 84
    .line 85
    move-result p1

    .line 86
    int-to-float p1, p1

    .line 87
    iget p2, v0, Landroid/graphics/RectF;->left:F

    .line 88
    .line 89
    sub-float/2addr p1, p2

    .line 90
    iget p2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇:I

    .line 91
    .line 92
    int-to-float p2, p2

    .line 93
    add-float/2addr p1, p2

    .line 94
    float-to-int p1, p1

    .line 95
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o〇:I

    .line 96
    .line 97
    iget p1, v0, Landroid/graphics/RectF;->top:F

    .line 98
    .line 99
    float-to-int p1, p1

    .line 100
    iput p1, p3, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o00〇〇Oo:I

    .line 101
    .line 102
    :goto_0
    return-object p3
.end method


# virtual methods
.method protected O8(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public final 〇080(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v1, p0, Lcom/app/hubert/guide/model/RelativeGuide;->〇o00〇〇Oo:I

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {p0, v0}, Lcom/app/hubert/guide/model/RelativeGuide;->O8(Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 24
    .line 25
    iget v2, p0, Lcom/app/hubert/guide/model/RelativeGuide;->O8:I

    .line 26
    .line 27
    invoke-direct {p0, v2, p1, v0}, Lcom/app/hubert/guide/model/RelativeGuide;->〇o00〇〇Oo(ILandroid/view/ViewGroup;Landroid/view/View;)Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v2}, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-static {v3}, Lcom/app/hubert/guide/util/LogUtil;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v2, p1, v0}, Lcom/app/hubert/guide/model/RelativeGuide;->〇o〇(Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 39
    .line 40
    .line 41
    iget p1, v2, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->Oo08:I

    .line 42
    .line 43
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 44
    .line 45
    iget p1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 46
    .line 47
    iget v3, v2, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇080:I

    .line 48
    .line 49
    add-int/2addr p1, v3

    .line 50
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 51
    .line 52
    iget p1, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 53
    .line 54
    iget v3, v2, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o00〇〇Oo:I

    .line 55
    .line 56
    add-int/2addr p1, v3

    .line 57
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 58
    .line 59
    iget p1, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 60
    .line 61
    iget v3, v2, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->〇o〇:I

    .line 62
    .line 63
    add-int/2addr p1, v3

    .line 64
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 65
    .line 66
    iget p1, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 67
    .line 68
    iget v2, v2, Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;->O8:I

    .line 69
    .line 70
    add-int/2addr p1, v2

    .line 71
    iput p1, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    .line 75
    .line 76
    return-object v0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method protected 〇o〇(Lcom/app/hubert/guide/model/RelativeGuide$MarginInfo;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

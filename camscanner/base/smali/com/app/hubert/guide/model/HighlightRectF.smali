.class public Lcom/app/hubert/guide/model/HighlightRectF;
.super Ljava/lang/Object;
.source "HighlightRectF.java"

# interfaces
.implements Lcom/app/hubert/guide/model/HighLight;


# instance fields
.field private O8:Lcom/app/hubert/guide/model/HighlightOptions;

.field private 〇080:Landroid/graphics/RectF;

.field private 〇o00〇〇Oo:Lcom/app/hubert/guide/model/HighLight$Shape;

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Landroid/graphics/RectF;Lcom/app/hubert/guide/model/HighLight$Shape;I)V
    .locals 0
    .param p1    # Landroid/graphics/RectF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/app/hubert/guide/model/HighLight$Shape;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇080:Landroid/graphics/RectF;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇o00〇〇Oo:Lcom/app/hubert/guide/model/HighLight$Shape;

    .line 7
    .line 8
    iput p3, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇o〇:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method


# virtual methods
.method public getOptions()Lcom/app/hubert/guide/model/HighlightOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/model/HighlightRectF;->O8:Lcom/app/hubert/guide/model/HighlightOptions;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getRadius()F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇080:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/high16 v1, 0x40000000    # 2.0f

    .line 8
    .line 9
    div-float/2addr v0, v1

    .line 10
    iget-object v2, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇080:Landroid/graphics/RectF;

    .line 11
    .line 12
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    div-float/2addr v2, v1

    .line 17
    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public getShape()Lcom/app/hubert/guide/model/HighLight$Shape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇o00〇〇Oo:Lcom/app/hubert/guide/model/HighLight$Shape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(Landroid/view/View;)Landroid/graphics/RectF;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇080:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/app/hubert/guide/model/HighlightRectF;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o〇(Lcom/app/hubert/guide/model/HighlightOptions;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/app/hubert/guide/model/HighlightRectF;->O8:Lcom/app/hubert/guide/model/HighlightOptions;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

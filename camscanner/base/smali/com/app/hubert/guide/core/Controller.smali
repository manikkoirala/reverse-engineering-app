.class public Lcom/app/hubert/guide/core/Controller;
.super Ljava/lang/Object;
.source "Controller.java"


# instance fields
.field private O8:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

.field private OO0o〇〇:I

.field private OO0o〇〇〇〇0:Lcom/app/hubert/guide/core/GuideLayout;

.field private Oo08:Ljava/lang/String;

.field private oO80:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/app/hubert/guide/model/GuidePage;",
            ">;"
        }
    .end annotation
.end field

.field private o〇0:Z

.field private 〇080:Landroid/app/Activity;

.field private 〇80〇808〇O:I

.field private 〇8o8o〇:Landroid/widget/FrameLayout;

.field private 〇O8o08O:Landroid/content/SharedPreferences;

.field private 〇o00〇〇Oo:Landroid/app/Fragment;

.field private 〇o〇:Landroidx/fragment/app/Fragment;

.field private 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/app/hubert/guide/core/Builder;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇:I

    .line 6
    .line 7
    iget-object v1, p1, Lcom/app/hubert/guide/core/Builder;->〇080:Landroid/app/Activity;

    .line 8
    .line 9
    iput-object v1, p0, Lcom/app/hubert/guide/core/Controller;->〇080:Landroid/app/Activity;

    .line 10
    .line 11
    iget-object v2, p1, Lcom/app/hubert/guide/core/Builder;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 12
    .line 13
    iput-object v2, p0, Lcom/app/hubert/guide/core/Controller;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 14
    .line 15
    iget-object v2, p1, Lcom/app/hubert/guide/core/Builder;->〇o〇:Landroidx/fragment/app/Fragment;

    .line 16
    .line 17
    iput-object v2, p0, Lcom/app/hubert/guide/core/Controller;->〇o〇:Landroidx/fragment/app/Fragment;

    .line 18
    .line 19
    iget-object v2, p1, Lcom/app/hubert/guide/core/Builder;->oO80:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 20
    .line 21
    iput-object v2, p0, Lcom/app/hubert/guide/core/Controller;->O8:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 22
    .line 23
    iget-object v2, p1, Lcom/app/hubert/guide/core/Builder;->O8:Ljava/lang/String;

    .line 24
    .line 25
    iput-object v2, p0, Lcom/app/hubert/guide/core/Controller;->Oo08:Ljava/lang/String;

    .line 26
    .line 27
    iget-boolean v2, p1, Lcom/app/hubert/guide/core/Builder;->Oo08:Z

    .line 28
    .line 29
    iput-boolean v2, p0, Lcom/app/hubert/guide/core/Controller;->o〇0:Z

    .line 30
    .line 31
    iget-object v2, p1, Lcom/app/hubert/guide/core/Builder;->〇80〇808〇O:Ljava/util/List;

    .line 32
    .line 33
    iput-object v2, p0, Lcom/app/hubert/guide/core/Controller;->oO80:Ljava/util/List;

    .line 34
    .line 35
    iget v2, p1, Lcom/app/hubert/guide/core/Builder;->〇〇888:I

    .line 36
    .line 37
    iput v2, p0, Lcom/app/hubert/guide/core/Controller;->〇〇888:I

    .line 38
    .line 39
    iget-object p1, p1, Lcom/app/hubert/guide/core/Builder;->o〇0:Landroid/view/View;

    .line 40
    .line 41
    if-nez p1, :cond_0

    .line 42
    .line 43
    const p1, 0x1020002

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    :cond_0
    instance-of v1, p1, Landroid/widget/FrameLayout;

    .line 51
    .line 52
    if-eqz v1, :cond_1

    .line 53
    .line 54
    check-cast p1, Landroid/widget/FrameLayout;

    .line 55
    .line 56
    iput-object p1, p0, Lcom/app/hubert/guide/core/Controller;->〇8o8o〇:Landroid/widget/FrameLayout;

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    new-instance v1, Landroid/widget/FrameLayout;

    .line 60
    .line 61
    iget-object v2, p0, Lcom/app/hubert/guide/core/Controller;->〇080:Landroid/app/Activity;

    .line 62
    .line 63
    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    check-cast v2, Landroid/view/ViewGroup;

    .line 71
    .line 72
    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    iput v3, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇:I

    .line 77
    .line 78
    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 79
    .line 80
    .line 81
    iget v3, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇:I

    .line 82
    .line 83
    if-ltz v3, :cond_2

    .line 84
    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 86
    .line 87
    .line 88
    move-result-object v4

    .line 89
    invoke-virtual {v2, v1, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 94
    .line 95
    .line 96
    move-result-object v3

    .line 97
    invoke-virtual {v2, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    .line 99
    .line 100
    :goto_0
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    .line 101
    .line 102
    invoke-direct {v2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v1, p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    .line 107
    .line 108
    iput-object v1, p0, Lcom/app/hubert/guide/core/Controller;->〇8o8o〇:Landroid/widget/FrameLayout;

    .line 109
    .line 110
    :goto_1
    iget-object p1, p0, Lcom/app/hubert/guide/core/Controller;->〇080:Landroid/app/Activity;

    .line 111
    .line 112
    const-string v0, "NewbieGuide"

    .line 113
    .line 114
    const/4 v1, 0x0

    .line 115
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    iput-object p1, p0, Lcom/app/hubert/guide/core/Controller;->〇O8o08O:Landroid/content/SharedPreferences;

    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method static synthetic O8(Lcom/app/hubert/guide/core/Controller;)Lcom/app/hubert/guide/listener/OnGuideChangedListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/app/hubert/guide/core/Controller;->O8:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private OO0o〇〇〇〇0(Landroid/app/Fragment;)V
    .locals 2

    .line 1
    :try_start_0
    const-class v0, Landroid/app/Fragment;

    .line 2
    .line 3
    const-string v1, "mChildFragmentManager"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :catch_0
    move-exception p1

    .line 19
    new-instance v0, Ljava/lang/RuntimeException;

    .line 20
    .line 21
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 22
    .line 23
    .line 24
    throw v0

    .line 25
    :catch_1
    move-exception p1

    .line 26
    new-instance v0, Ljava/lang/RuntimeException;

    .line 27
    .line 28
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    throw v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method static synthetic Oo08(Lcom/app/hubert/guide/core/Controller;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private Oooo8o0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->oO80:Ljava/util/List;

    .line 2
    .line 3
    iget v1, p0, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O:I

    .line 4
    .line 5
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/app/hubert/guide/model/GuidePage;

    .line 10
    .line 11
    new-instance v1, Lcom/app/hubert/guide/core/GuideLayout;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/app/hubert/guide/core/Controller;->〇080:Landroid/app/Activity;

    .line 14
    .line 15
    invoke-direct {v1, v2, v0, p0}, Lcom/app/hubert/guide/core/GuideLayout;-><init>(Landroid/content/Context;Lcom/app/hubert/guide/model/GuidePage;Lcom/app/hubert/guide/core/Controller;)V

    .line 16
    .line 17
    .line 18
    new-instance v0, Lcom/app/hubert/guide/core/Controller$3;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/app/hubert/guide/core/Controller$3;-><init>(Lcom/app/hubert/guide/core/Controller;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v0}, Lcom/app/hubert/guide/core/GuideLayout;->setOnGuideLayoutDismissListener(Lcom/app/hubert/guide/core/GuideLayout$OnGuideLayoutDismissListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇8o8o〇:Landroid/widget/FrameLayout;

    .line 27
    .line 28
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 29
    .line 30
    const/4 v3, -0x1

    .line 31
    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇〇〇0:Lcom/app/hubert/guide/core/GuideLayout;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic oO80(Lcom/app/hubert/guide/core/Controller;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Controller;->〇〇808〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic o〇0(Lcom/app/hubert/guide/core/Controller;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/app/hubert/guide/core/Controller;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇080(Lcom/app/hubert/guide/core/Controller;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/app/hubert/guide/core/Controller;->oO80:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇80〇808〇O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 2
    .line 3
    const-string v1, "listener_fragment"

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0, v0}, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇〇〇0(Landroid/app/Fragment;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/app/hubert/guide/lifecycle/ListenerFragment;

    .line 21
    .line 22
    if-nez v2, :cond_0

    .line 23
    .line 24
    new-instance v2, Lcom/app/hubert/guide/lifecycle/ListenerFragment;

    .line 25
    .line 26
    invoke-direct {v2}, Lcom/app/hubert/guide/lifecycle/ListenerFragment;-><init>()V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 38
    .line 39
    .line 40
    :cond_0
    new-instance v0, Lcom/app/hubert/guide/core/Controller$4;

    .line 41
    .line 42
    invoke-direct {v0, p0}, Lcom/app/hubert/guide/core/Controller$4;-><init>(Lcom/app/hubert/guide/core/Controller;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2, v0}, Lcom/app/hubert/guide/lifecycle/ListenerFragment;->〇080(Lcom/app/hubert/guide/lifecycle/FragmentLifecycle;)V

    .line 46
    .line 47
    .line 48
    :cond_1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇o〇:Landroidx/fragment/app/Fragment;

    .line 49
    .line 50
    if-eqz v0, :cond_3

    .line 51
    .line 52
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    check-cast v2, Lcom/app/hubert/guide/lifecycle/V4ListenerFragment;

    .line 61
    .line 62
    if-nez v2, :cond_2

    .line 63
    .line 64
    new-instance v2, Lcom/app/hubert/guide/lifecycle/V4ListenerFragment;

    .line 65
    .line 66
    invoke-direct {v2}, Lcom/app/hubert/guide/lifecycle/V4ListenerFragment;-><init>()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-virtual {v0, v2, v1}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 78
    .line 79
    .line 80
    :cond_2
    new-instance v0, Lcom/app/hubert/guide/core/Controller$5;

    .line 81
    .line 82
    invoke-direct {v0, p0}, Lcom/app/hubert/guide/core/Controller$5;-><init>(Lcom/app/hubert/guide/core/Controller;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v2, v0}, Lcom/app/hubert/guide/lifecycle/V4ListenerFragment;->〇80O8o8O〇(Lcom/app/hubert/guide/lifecycle/FragmentLifecycle;)V

    .line 86
    .line 87
    .line 88
    :cond_3
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method private 〇O8o08O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 2
    .line 3
    const-string v1, "listener_fragment"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    check-cast v2, Lcom/app/hubert/guide/lifecycle/ListenerFragment;

    .line 16
    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 28
    .line 29
    .line 30
    :cond_0
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇o〇:Landroidx/fragment/app/Fragment;

    .line 31
    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    check-cast v1, Lcom/app/hubert/guide/lifecycle/V4ListenerFragment;

    .line 43
    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 55
    .line 56
    .line 57
    :cond_1
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/app/hubert/guide/core/Controller;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O:I

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method static synthetic 〇o〇(Lcom/app/hubert/guide/core/Controller;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Controller;->Oooo8o0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇〇808〇()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/app/hubert/guide/core/Controller;->oO80:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/lit8 v1, v1, -0x1

    .line 10
    .line 11
    if-ge v0, v1, :cond_0

    .line 12
    .line 13
    iget v0, p0, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O:I

    .line 14
    .line 15
    add-int/lit8 v0, v0, 0x1

    .line 16
    .line 17
    iput v0, p0, Lcom/app/hubert/guide/core/Controller;->〇80〇808〇O:I

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Controller;->Oooo8o0〇()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->O8:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-interface {v0, p0}, Lcom/app/hubert/guide/listener/OnGuideChangedListener;->〇o00〇〇Oo(Lcom/app/hubert/guide/core/Controller;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Controller;->〇O8o08O()V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic 〇〇888(Lcom/app/hubert/guide/core/Controller;)Landroid/content/SharedPreferences;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/app/hubert/guide/core/Controller;->〇O8o08O:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method


# virtual methods
.method public OO0o〇〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->〇O8o08O:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/app/hubert/guide/core/Controller;->Oo08:Ljava/lang/String;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget-boolean v1, p0, Lcom/app/hubert/guide/core/Controller;->o〇0:Z

    .line 11
    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    iget v1, p0, Lcom/app/hubert/guide/core/Controller;->〇〇888:I

    .line 15
    .line 16
    if-lt v0, v1, :cond_0

    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    iget-object v1, p0, Lcom/app/hubert/guide/core/Controller;->〇8o8o〇:Landroid/widget/FrameLayout;

    .line 20
    .line 21
    new-instance v2, Lcom/app/hubert/guide/core/Controller$1;

    .line 22
    .line 23
    invoke-direct {v2, p0, v0}, Lcom/app/hubert/guide/core/Controller$1;-><init>(Lcom/app/hubert/guide/core/Controller;I)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public 〇8o8o〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇〇〇0:Lcom/app/hubert/guide/core/GuideLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇〇〇0:Lcom/app/hubert/guide/core/GuideLayout;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Landroid/view/ViewGroup;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇〇〇0:Lcom/app/hubert/guide/core/GuideLayout;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 22
    .line 23
    .line 24
    instance-of v1, v0, Landroid/widget/FrameLayout;

    .line 25
    .line 26
    if-nez v1, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    check-cast v1, Landroid/view/ViewGroup;

    .line 33
    .line 34
    const/4 v2, 0x0

    .line 35
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 40
    .line 41
    .line 42
    if-eqz v2, :cond_1

    .line 43
    .line 44
    iget v3, p0, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇:I

    .line 45
    .line 46
    if-lez v3, :cond_0

    .line 47
    .line 48
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v1, v2, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    .line 62
    .line 63
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/app/hubert/guide/core/Controller;->O8:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 64
    .line 65
    if-eqz v0, :cond_2

    .line 66
    .line 67
    invoke-interface {v0, p0}, Lcom/app/hubert/guide/listener/OnGuideChangedListener;->〇o00〇〇Oo(Lcom/app/hubert/guide/core/Controller;)V

    .line 68
    .line 69
    .line 70
    :cond_2
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

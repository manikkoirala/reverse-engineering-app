.class public Lcom/app/hubert/guide/core/Builder;
.super Ljava/lang/Object;
.source "Builder.java"


# instance fields
.field O8:Ljava/lang/String;

.field Oo08:Z

.field oO80:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

.field o〇0:Landroid/view/View;

.field 〇080:Landroid/app/Activity;

.field 〇80〇808〇O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/app/hubert/guide/model/GuidePage;",
            ">;"
        }
    .end annotation
.end field

.field 〇o00〇〇Oo:Landroid/app/Fragment;

.field 〇o〇:Landroidx/fragment/app/Fragment;

.field 〇〇888:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/app/hubert/guide/core/Builder;->〇〇888:I

    .line 6
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/app/hubert/guide/core/Builder;->〇80〇808〇O:Ljava/util/List;

    .line 13
    .line 14
    iput-object p1, p0, Lcom/app/hubert/guide/core/Builder;->〇080:Landroid/app/Activity;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Builder;->O8:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/app/hubert/guide/core/Builder;->〇080:Landroid/app/Activity;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    iget-object v0, p0, Lcom/app/hubert/guide/core/Builder;->〇o00〇〇Oo:Landroid/app/Fragment;

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/app/hubert/guide/core/Builder;->〇o〇:Landroidx/fragment/app/Fragment;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 23
    .line 24
    const-string v1, "activity is null, please make sure that fragment is showing when call NewbieGuide"

    .line 25
    .line 26
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    throw v0

    .line 30
    :cond_1
    :goto_0
    return-void

    .line 31
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 32
    .line 33
    const-string v1, "the param \'label\' is missing, please call setLabel()"

    .line 34
    .line 35
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    throw v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method


# virtual methods
.method public Oo08(Ljava/lang/String;)Lcom/app/hubert/guide/core/Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/app/hubert/guide/core/Builder;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇0(Lcom/app/hubert/guide/listener/OnGuideChangedListener;)Lcom/app/hubert/guide/core/Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/app/hubert/guide/core/Builder;->oO80:Lcom/app/hubert/guide/listener/OnGuideChangedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇080(Lcom/app/hubert/guide/model/GuidePage;)Lcom/app/hubert/guide/core/Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/app/hubert/guide/core/Builder;->〇80〇808〇O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o00〇〇Oo(Z)Lcom/app/hubert/guide/core/Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/app/hubert/guide/core/Builder;->Oo08:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o〇(Landroid/view/View;)Lcom/app/hubert/guide/core/Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/app/hubert/guide/core/Builder;->o〇0:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇〇888()Lcom/app/hubert/guide/core/Controller;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/app/hubert/guide/core/Builder;->O8()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/app/hubert/guide/core/Controller;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/app/hubert/guide/core/Controller;-><init>(Lcom/app/hubert/guide/core/Builder;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/app/hubert/guide/core/Controller;->OO0o〇〇()V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

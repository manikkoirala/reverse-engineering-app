.class public Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;
.super Landroidx/recyclerview/widget/RecyclerView$LayoutParams;
.source "VirtualLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/alibaba/android/vlayout/VirtualLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field private OO:I

.field public o0:I

.field private 〇08O〇00〇o:I

.field public 〇OOo8〇0:F


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    const/4 p1, 0x0

    .line 7
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->o0:I

    const/high16 p1, 0x7fc00000    # Float.NaN

    .line 8
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    const/high16 p1, -0x80000000

    .line 9
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->OO:I

    .line 10
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇08O〇00〇o:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->o0:I

    const/high16 p1, 0x7fc00000    # Float.NaN

    .line 3
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    const/high16 p1, -0x80000000

    .line 4
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->OO:I

    .line 5
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇08O〇00〇o:I

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x0

    .line 17
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->o0:I

    const/high16 p1, 0x7fc00000    # Float.NaN

    .line 18
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    const/high16 p1, -0x80000000

    .line 19
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->OO:I

    .line 20
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇08O〇00〇o:I

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    const/4 p1, 0x0

    .line 12
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->o0:I

    const/high16 p1, 0x7fc00000    # Float.NaN

    .line 13
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    const/high16 p1, -0x80000000

    .line 14
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->OO:I

    .line 15
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇08O〇00〇o:I

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView$LayoutParams;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(Landroidx/recyclerview/widget/RecyclerView$LayoutParams;)V

    const/4 p1, 0x0

    .line 22
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->o0:I

    const/high16 p1, 0x7fc00000    # Float.NaN

    .line 23
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    const/high16 p1, -0x80000000

    .line 24
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->OO:I

    .line 25
    iput p1, p0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇08O〇00〇o:I

    return-void
.end method

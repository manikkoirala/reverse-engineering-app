.class public Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;
.super Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;
.source "GridLayoutHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;,
        Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$DefaultSpanSizeLookup;
    }
.end annotation


# static fields
.field private static final O8〇o:I

.field private static oo〇:Z = false


# instance fields
.field private O8ooOoo〇:[F

.field private OOO〇O0:Z

.field private OoO8:I

.field private O〇8O8〇008:I

.field private o800o8O:I

.field private oo88o8O:Z

.field private o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private o〇〇0〇:[I

.field private 〇00:I

.field private 〇0000OOO:[I

.field private 〇O888o0o:I

.field private 〇oOO8O8:[Landroid/view/View;

.field private 〇oo〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    sput v0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8〇o:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;-><init>(III)V

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3, p3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 3

    .line 3
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;-><init>()V

    const/4 v0, 0x4

    .line 4
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    .line 6
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    const/4 v1, 0x1

    .line 7
    iput-boolean v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo88o8O:Z

    .line 8
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oo〇:Z

    .line 9
    new-instance v2, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$DefaultSpanSizeLookup;

    invoke-direct {v2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$DefaultSpanSizeLookup;-><init>()V

    iput-object v2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 10
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    .line 11
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    new-array v2, v0, [F

    .line 12
    iput-object v2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8ooOoo〇:[F

    .line 13
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    .line 14
    invoke-virtual {p0, p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O08000(I)V

    .line 15
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    invoke-virtual {p1, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->Oo08(Z)V

    .line 16
    invoke-virtual {p0, p2}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8(I)V

    .line 17
    invoke-virtual {p0, p3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇8〇0〇o〇O(I)V

    .line 18
    invoke-virtual {p0, p4}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇8(I)V

    return-void
.end method

.method private Oo8Oo00oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIZLcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, -0x1

    .line 3
    const/4 v2, 0x1

    .line 4
    if-eqz p5, :cond_0

    .line 5
    .line 6
    move p5, p3

    .line 7
    const/4 p3, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    add-int/lit8 p3, p3, -0x1

    .line 11
    .line 12
    const/4 p5, -0x1

    .line 13
    const/4 v3, -0x1

    .line 14
    :goto_0
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    if-ne v4, v2, :cond_1

    .line 19
    .line 20
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇8O0〇8()Z

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    if-eqz v4, :cond_1

    .line 25
    .line 26
    add-int/lit8 v0, p4, -0x1

    .line 27
    .line 28
    const/4 p4, -0x1

    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 p4, 0x1

    .line 31
    :goto_1
    if-eq p3, p5, :cond_3

    .line 32
    .line 33
    iget-object v4, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    .line 34
    .line 35
    aget-object v4, v4, p3

    .line 36
    .line 37
    invoke-interface {p6, v4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    invoke-direct {p0, p1, p2, v4}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    if-ne p4, v1, :cond_2

    .line 46
    .line 47
    if-le v4, v2, :cond_2

    .line 48
    .line 49
    iget-object v5, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    .line 50
    .line 51
    add-int/lit8 v6, v4, -0x1

    .line 52
    .line 53
    sub-int v6, v0, v6

    .line 54
    .line 55
    aput v6, v5, p3

    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_2
    iget-object v5, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    .line 59
    .line 60
    aput v0, v5, p3

    .line 61
    .line 62
    :goto_2
    mul-int v4, v4, p4

    .line 63
    .line 64
    add-int/2addr v0, v4

    .line 65
    add-int/2addr p3, v3

    .line 66
    goto :goto_1

    .line 67
    :cond_3
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method private o〇0OOo〇0(IIIF)I
    .locals 4

    .line 1
    invoke-static {p4}, Ljava/lang/Float;->isNaN(F)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/high16 v1, 0x3f000000    # 0.5f

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    const/high16 v3, 0x40000000    # 2.0f

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    cmpl-float v0, p4, v2

    .line 13
    .line 14
    if-lez v0, :cond_0

    .line 15
    .line 16
    if-lez p3, :cond_0

    .line 17
    .line 18
    int-to-float p1, p3

    .line 19
    div-float/2addr p1, p4

    .line 20
    add-float/2addr p1, v1

    .line 21
    float-to-int p1, p1

    .line 22
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1

    .line 27
    :cond_0
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 28
    .line 29
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    .line 30
    .line 31
    .line 32
    move-result p3

    .line 33
    if-nez p3, :cond_1

    .line 34
    .line 35
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 36
    .line 37
    cmpl-float p4, p3, v2

    .line 38
    .line 39
    if-lez p4, :cond_1

    .line 40
    .line 41
    int-to-float p1, p2

    .line 42
    div-float/2addr p1, p3

    .line 43
    add-float/2addr p1, v1

    .line 44
    float-to-int p1, p1

    .line 45
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    return p1

    .line 50
    :cond_1
    if-gez p1, :cond_2

    .line 51
    .line 52
    sget p1, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8〇o:I

    .line 53
    .line 54
    return p1

    .line 55
    :cond_2
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    return p1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method private 〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 8
    .line 9
    invoke-virtual {p1, p3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇(I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1

    .line 14
    :cond_0
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    const/4 p2, -0x1

    .line 19
    if-ne p1, p2, :cond_1

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    return p1

    .line 23
    :cond_1
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 24
    .line 25
    invoke-virtual {p2, p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇(I)I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private 〇〇0o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 8
    .line 9
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 10
    .line 11
    invoke-virtual {p1, p3, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1

    .line 16
    :cond_0
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    const/4 p2, -0x1

    .line 21
    if-ne p1, p2, :cond_1

    .line 22
    .line 23
    const/4 p1, 0x0

    .line 24
    return p1

    .line 25
    :cond_1
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 26
    .line 27
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 28
    .line 29
    invoke-virtual {p2, p1, p3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    return p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private 〇〇〇0〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 7
    .line 8
    if-eq v0, v1, :cond_1

    .line 9
    .line 10
    :cond_0
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 11
    .line 12
    new-array v0, v0, [Landroid/view/View;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    .line 15
    .line 16
    :cond_1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    .line 17
    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    array-length v0, v0

    .line 21
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 22
    .line 23
    if-eq v0, v1, :cond_3

    .line 24
    .line 25
    :cond_2
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 26
    .line 27
    new-array v0, v0, [I

    .line 28
    .line 29
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    .line 30
    .line 31
    :cond_3
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    .line 32
    .line 33
    if-eqz v0, :cond_4

    .line 34
    .line 35
    array-length v0, v0

    .line 36
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 37
    .line 38
    if-eq v0, v1, :cond_5

    .line 39
    .line 40
    :cond_4
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 41
    .line 42
    new-array v0, v0, [I

    .line 43
    .line 44
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    .line 45
    .line 46
    :cond_5
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method


# virtual methods
.method public O08000(I)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    if-lt p1, v0, :cond_1

    .line 8
    .line 9
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 10
    .line 11
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->O8()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇〇〇0〇〇0()V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v2, "Span count should be at least 1. Provided "

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public Oo08(IZZLcom/alibaba/android/vlayout/LayoutManagerHelper;)I
    .locals 3

    .line 1
    invoke-interface {p4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-eqz p2, :cond_2

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇888()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    sub-int/2addr v2, v1

    .line 18
    if-ne p1, v2, :cond_4

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 23
    .line 24
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇80〇808〇O:I

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 28
    .line 29
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇〇888:I

    .line 30
    .line 31
    :goto_1
    add-int/2addr p1, p2

    .line 32
    return p1

    .line 33
    :cond_2
    if-nez p1, :cond_4

    .line 34
    .line 35
    if-eqz v0, :cond_3

    .line 36
    .line 37
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 38
    .line 39
    neg-int p1, p1

    .line 40
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_3
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 44
    .line 45
    neg-int p1, p1

    .line 46
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 47
    .line 48
    :goto_2
    sub-int/2addr p1, p2

    .line 49
    return p1

    .line 50
    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->Oo08(IZZLcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    return p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method public oO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo88o8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->O8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇8(I)V
    .locals 0

    .line 1
    if-gez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    :cond_0
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇8〇0〇o〇O(I)V
    .locals 0

    .line 1
    if-gez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    :cond_0
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇O8o08O(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/LayoutHelper;->〇O8o08O(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->O8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 28

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    .line 1
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    .line 3
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->isEnableMarginOverLap()Z

    move-result v13

    .line 4
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->Oo08()I

    move-result v0

    const/4 v14, 0x1

    if-ne v0, v14, :cond_1

    const/16 v16, 0x1

    goto :goto_0

    :cond_1
    const/16 v16, 0x0

    .line 5
    :goto_0
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->Oooo8o0〇()Lcom/alibaba/android/vlayout/OrientationHelperEx;

    move-result-object v6

    .line 6
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v1

    if-ne v1, v14, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    const/high16 v17, 0x3f000000    # 0.5f

    const/high16 v18, 0x3f800000    # 1.0f

    if-eqz v5, :cond_3

    .line 7
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    move-result v1

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OoO8()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o800o8O()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    .line 8
    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    add-int/lit8 v3, v2, -0x1

    iget v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    mul-int v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float v1, v1, v18

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float v1, v1, v17

    float-to-int v1, v1

    iput v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    goto :goto_2

    .line 9
    :cond_3
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇808〇()I

    move-result v1

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇00()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->O〇8O8〇008()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    .line 10
    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    add-int/lit8 v3, v2, -0x1

    iget v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    mul-int v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    mul-float v1, v1, v18

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float v1, v1, v17

    float-to-int v1, v1

    iput v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    .line 11
    :goto_2
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 12
    invoke-direct/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇〇〇0〇〇0()V

    const-string v2, " spans."

    const-string v3, " spans but GridLayoutManager has only "

    const-string v4, " requires "

    const-string v15, "Item at position "

    if-nez v16, :cond_11

    .line 13
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v1

    invoke-direct {v7, v8, v9, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇〇0o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v1

    .line 14
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v14

    invoke-direct {v7, v8, v9, v14}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v14

    add-int/2addr v14, v1

    move/from16 v20, v5

    .line 15
    iget v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    const/16 v19, 0x1

    add-int/lit8 v5, v5, -0x1

    if-eq v1, v5, :cond_10

    .line 16
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v1

    .line 17
    iget v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    sub-int/2addr v5, v14

    move-object/from16 v21, v6

    move/from16 v25, v14

    const/4 v6, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    .line 18
    :goto_3
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    if-ge v6, v14, :cond_e

    if-lez v5, :cond_e

    sub-int/2addr v1, v0

    .line 19
    invoke-virtual {v7, v1}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v14

    if-eqz v14, :cond_4

    goto/16 :goto_8

    .line 20
    :cond_4
    invoke-direct {v7, v8, v9, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v14

    move/from16 v26, v0

    .line 21
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    if-gt v14, v0, :cond_d

    .line 22
    invoke-virtual {v10, v8, v1}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇O8o08O(Landroidx/recyclerview/widget/RecyclerView$Recycler;I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_5

    goto/16 :goto_8

    :cond_5
    if-nez v22, :cond_8

    .line 23
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v1, v11, :cond_7

    goto :goto_4

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v11

    invoke-virtual {v11}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v1, v11, :cond_7

    :goto_4
    const/4 v11, 0x1

    goto :goto_5

    :cond_7
    const/4 v11, 0x0

    :goto_5
    move/from16 v22, v11

    :cond_8
    if-nez v24, :cond_b

    .line 24
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v11

    invoke-virtual {v11}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v1, v11, :cond_a

    goto :goto_6

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v11

    invoke-virtual {v11}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v1, v11, :cond_a

    :goto_6
    const/4 v11, 0x1

    goto :goto_7

    :cond_a
    const/4 v11, 0x0

    :goto_7
    move/from16 v24, v11

    :cond_b
    sub-int/2addr v5, v14

    if-gez v5, :cond_c

    goto :goto_8

    :cond_c
    add-int v23, v23, v14

    .line 25
    iget-object v11, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aput-object v0, v11, v6

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v11, p4

    move/from16 v0, v26

    goto/16 :goto_3

    .line 26
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    :goto_8
    if-lez v6, :cond_f

    add-int/lit8 v0, v6, -0x1

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v0, :cond_f

    .line 27
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aget-object v11, v5, v1

    .line 28
    aget-object v14, v5, v0

    aput-object v14, v5, v1

    .line 29
    aput-object v11, v5, v0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_9

    :cond_f
    move v11, v6

    move/from16 v14, v23

    move/from16 v1, v25

    goto :goto_b

    :cond_10
    move-object/from16 v21, v6

    move/from16 v25, v14

    move/from16 v1, v25

    goto :goto_a

    :cond_11
    move/from16 v20, v5

    move-object/from16 v21, v6

    :goto_a
    const/4 v11, 0x0

    const/4 v14, 0x0

    const/16 v22, 0x0

    const/16 v24, 0x0

    .line 30
    :goto_b
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    if-ge v11, v0, :cond_1c

    invoke-virtual {v10, v9}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->oO80(Landroidx/recyclerview/widget/RecyclerView$State;)Z

    move-result v0

    if-eqz v0, :cond_1c

    if-lez v1, :cond_1c

    .line 31
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v0

    .line 32
    invoke-virtual {v7, v0}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 33
    sget-boolean v2, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo〇:Z

    if-eqz v2, :cond_1c

    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pos ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "] is out of range"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    .line 35
    :cond_12
    invoke-direct {v7, v8, v9, v0}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v5

    .line 36
    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    if-gt v5, v6, :cond_1b

    sub-int/2addr v1, v5

    if-gez v1, :cond_13

    goto :goto_c

    .line 37
    :cond_13
    invoke-virtual {v10, v8}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇8o8o〇(Landroidx/recyclerview/widget/RecyclerView$Recycler;)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_14

    :goto_c
    goto/16 :goto_12

    :cond_14
    if-nez v22, :cond_17

    .line 38
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v22

    if-eqz v22, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    move/from16 v23, v1

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_16

    goto :goto_d

    :cond_15
    move/from16 v23, v1

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v1

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_16

    :goto_d
    const/4 v1, 0x1

    goto :goto_e

    :cond_16
    const/4 v1, 0x0

    :goto_e
    move/from16 v22, v1

    goto :goto_f

    :cond_17
    move/from16 v23, v1

    :goto_f
    if-nez v24, :cond_1a

    .line 39
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v1

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_19

    goto :goto_10

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    move-result-object v1

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_19

    :goto_10
    const/4 v0, 0x1

    goto :goto_11

    :cond_19
    const/4 v0, 0x0

    :goto_11
    move/from16 v24, v0

    :cond_1a
    add-int/2addr v14, v5

    .line 40
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aput-object v6, v0, v11

    add-int/lit8 v11, v11, 0x1

    move/from16 v1, v23

    goto/16 :goto_b

    .line 41
    :cond_1b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1c
    :goto_12
    move v15, v1

    if-nez v11, :cond_1d

    return-void

    :cond_1d
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move v3, v11

    move v4, v14

    move/from16 v6, v20

    move/from16 v5, v16

    move/from16 v20, v13

    move v13, v6

    move-object/from16 v6, p5

    .line 42
    invoke-direct/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->Oo8Oo00oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIZLcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    if-lez v15, :cond_1f

    if-ne v11, v14, :cond_1f

    .line 43
    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo88o8O:Z

    if-eqz v0, :cond_1f

    if-eqz v13, :cond_1e

    .line 44
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v1, v11, -0x1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    div-int/2addr v0, v11

    iput v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    goto :goto_13

    .line 45
    :cond_1e
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v1, v11, -0x1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    div-int/2addr v0, v11

    iput v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    goto :goto_13

    :cond_1f
    if-nez v16, :cond_21

    if-nez v15, :cond_21

    if-ne v11, v14, :cond_21

    .line 46
    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo88o8O:Z

    if-eqz v0, :cond_21

    if-eqz v13, :cond_20

    .line 47
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v1, v11, -0x1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    div-int/2addr v0, v11

    iput v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    goto :goto_13

    .line 48
    :cond_20
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v1, v11, -0x1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    div-int/2addr v0, v11

    iput v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    .line 49
    :cond_21
    :goto_13
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8ooOoo〇:[F

    const/4 v1, -0x1

    if-eqz v0, :cond_28

    array-length v0, v0

    if-lez v0, :cond_28

    if-eqz v13, :cond_22

    .line 50
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v2, v11, -0x1

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    goto :goto_14

    .line 51
    :cond_22
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    add-int/lit8 v2, v11, -0x1

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    :goto_14
    mul-int v2, v2, v3

    sub-int/2addr v0, v2

    if-lez v15, :cond_23

    .line 52
    iget-boolean v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo88o8O:Z

    if-eqz v2, :cond_23

    move v2, v11

    goto :goto_15

    :cond_23
    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    :goto_15
    move v5, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_16
    if-ge v3, v2, :cond_25

    .line 53
    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8ooOoo〇:[F

    array-length v14, v6

    if-ge v3, v14, :cond_24

    aget v6, v6, v3

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_24

    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O8ooOoo〇:[F

    aget v6, v6, v3

    const/4 v14, 0x0

    cmpl-float v14, v6, v14

    if-ltz v14, :cond_24

    .line 54
    iget-object v14, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    mul-float v6, v6, v18

    const/high16 v15, 0x42c80000    # 100.0f

    div-float/2addr v6, v15

    int-to-float v15, v0

    mul-float v6, v6, v15

    add-float v6, v6, v17

    float-to-int v6, v6

    aput v6, v14, v3

    sub-int/2addr v5, v6

    goto :goto_17

    :cond_24
    add-int/lit8 v4, v4, 0x1

    .line 55
    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    aput v1, v6, v3

    :goto_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_16

    :cond_25
    if-lez v4, :cond_27

    .line 56
    div-int/2addr v5, v4

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v2, :cond_27

    .line 57
    iget-object v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    aget v4, v3, v0

    if-gez v4, :cond_26

    .line 58
    aput v5, v3, v0

    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :cond_27
    const/4 v14, 0x1

    goto :goto_19

    :cond_28
    const/4 v14, 0x0

    :goto_19
    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_1a
    if-ge v0, v11, :cond_2f

    .line 59
    iget-object v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aget-object v4, v4, v0

    if-eqz v16, :cond_29

    const/4 v5, -0x1

    goto :goto_1b

    :cond_29
    const/4 v5, 0x0

    .line 60
    :goto_1b
    invoke-interface {v12, v10, v4, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇0〇O0088o(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;I)V

    .line 61
    invoke-interface {v12, v4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    move-result v5

    invoke-direct {v7, v8, v9, v5}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v5

    if-eqz v14, :cond_2b

    .line 62
    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    aget v6, v6, v0

    const/4 v1, 0x0

    const/4 v15, 0x0

    :goto_1c
    if-ge v15, v5, :cond_2a

    .line 63
    iget-object v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    add-int v23, v15, v6

    aget v3, v3, v23

    add-int/2addr v1, v3

    add-int/lit8 v15, v15, 0x1

    goto :goto_1c

    :cond_2a
    const/4 v3, 0x0

    .line 64
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_1e

    :cond_2b
    const/4 v3, 0x0

    .line 65
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    mul-int v1, v1, v5

    add-int/lit8 v5, v5, -0x1

    .line 66
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    if-eqz v13, :cond_2c

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    goto :goto_1d

    :cond_2c
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    :goto_1d
    mul-int v5, v5, v3

    add-int/2addr v1, v5

    const/high16 v3, 0x40000000    # 2.0f

    .line 67
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 68
    :goto_1e
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;

    .line 69
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2d

    .line 70
    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    .line 71
    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    iget v3, v3, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    .line 72
    invoke-direct {v7, v5, v6, v15, v3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇0OOo〇0(IIIF)I

    move-result v3

    invoke-interface {v12, v4, v1, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_1f

    .line 73
    :cond_2d
    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    .line 74
    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    iget v3, v3, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    invoke-direct {v7, v5, v6, v15, v3}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇0OOo〇0(IIIF)I

    move-result v3

    .line 75
    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 76
    invoke-interface {v12, v4, v3, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    :goto_1f
    move-object/from16 v15, v21

    .line 77
    invoke-virtual {v15, v4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    move-result v1

    if-le v1, v2, :cond_2e

    move v2, v1

    :cond_2e
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v21, v15

    const/4 v1, -0x1

    goto/16 :goto_1a

    :cond_2f
    move-object/from16 v15, v21

    .line 78
    iget v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇O888o0o:I

    const/high16 v1, 0x7fc00000    # Float.NaN

    const/4 v3, 0x0

    invoke-direct {v7, v2, v0, v3, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇0OOo〇0(IIIF)I

    move-result v0

    const/4 v3, 0x0

    :goto_20
    if-ge v3, v11, :cond_35

    .line 79
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aget-object v1, v1, v3

    .line 80
    invoke-virtual {v15, v1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    move-result v4

    if-eq v4, v2, :cond_34

    .line 81
    invoke-interface {v12, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    move-result v4

    invoke-direct {v7, v8, v9, v4}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇08O8o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v4

    if-eqz v14, :cond_31

    .line 82
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    aget v5, v5, v3

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_21
    if-ge v6, v4, :cond_30

    .line 83
    iget-object v9, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    add-int v16, v6, v5

    aget v9, v9, v16

    add-int/2addr v8, v9

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v9, p2

    goto :goto_21

    :cond_30
    const/4 v6, 0x0

    .line 84
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move v5, v4

    const/high16 v4, 0x40000000    # 2.0f

    goto :goto_23

    :cond_31
    const/4 v6, 0x0

    .line 85
    iget v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    mul-int v5, v5, v4

    add-int/lit8 v4, v4, -0x1

    .line 86
    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-eqz v13, :cond_32

    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    goto :goto_22

    :cond_32
    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    :goto_22
    mul-int v4, v4, v6

    add-int/2addr v5, v4

    const/high16 v4, 0x40000000    # 2.0f

    .line 87
    invoke-static {v5, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 88
    :goto_23
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_33

    .line 89
    invoke-interface {v12, v1, v5, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_24

    .line 90
    :cond_33
    invoke-interface {v12, v1, v0, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_24

    :cond_34
    const/high16 v4, 0x40000000    # 2.0f

    :goto_24
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    goto :goto_20

    :cond_35
    if-eqz v22, :cond_36

    .line 91
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    move/from16 v3, v20

    invoke-virtual {v7, v12, v13, v0, v3}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇〇0〇(Lcom/alibaba/android/vlayout/LayoutManagerHelper;ZZZ)I

    move-result v0

    goto :goto_25

    :cond_36
    move/from16 v3, v20

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_25
    if-eqz v24, :cond_37

    .line 92
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v4

    xor-int/2addr v4, v1

    invoke-virtual {v7, v12, v13, v4, v3}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇0000OOO(Lcom/alibaba/android/vlayout/LayoutManagerHelper;ZZZ)I

    move-result v3

    goto :goto_26

    :cond_37
    const/4 v3, 0x0

    :goto_26
    add-int v1, v2, v0

    add-int/2addr v1, v3

    move-object/from16 v8, p4

    .line 93
    iput v1, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 94
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_38

    const/4 v1, 0x1

    goto :goto_27

    :cond_38
    const/4 v1, 0x0

    .line 95
    :goto_27
    iget-boolean v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    if-nez v4, :cond_3c

    if-eqz v24, :cond_39

    if-nez v1, :cond_3c

    :cond_39
    if-eqz v22, :cond_3a

    if-eqz v1, :cond_3c

    .line 96
    :cond_3a
    iget v1, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    if-eqz v13, :cond_3b

    iget v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    goto :goto_28

    :cond_3b
    iget v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    :goto_28
    add-int/2addr v1, v4

    iput v1, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    :cond_3c
    if-eqz v13, :cond_42

    .line 97
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_3f

    .line 98
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    sub-int/2addr v0, v3

    iget-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    if-nez v1, :cond_3e

    if-eqz v24, :cond_3d

    goto :goto_29

    :cond_3d
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    goto :goto_2a

    :cond_3e
    :goto_29
    const/4 v3, 0x0

    :goto_2a
    sub-int v3, v0, v3

    sub-int v0, v3, v2

    move v1, v0

    move v2, v3

    goto :goto_2d

    .line 99
    :cond_3f
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v1

    add-int/2addr v1, v0

    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    if-nez v0, :cond_41

    if-eqz v22, :cond_40

    goto :goto_2b

    :cond_40
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    goto :goto_2c

    :cond_41
    :goto_2b
    const/4 v3, 0x0

    :goto_2c
    add-int/2addr v3, v1

    add-int v0, v3, v2

    move v2, v0

    move v1, v3

    :goto_2d
    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_32

    .line 100
    :cond_42
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_45

    .line 101
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    sub-int/2addr v0, v3

    iget-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    if-nez v1, :cond_44

    if-eqz v24, :cond_43

    goto :goto_2e

    :cond_43
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    goto :goto_2f

    :cond_44
    :goto_2e
    const/4 v3, 0x0

    :goto_2f
    sub-int v3, v0, v3

    sub-int v0, v3, v2

    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v27, v3

    move v3, v0

    move/from16 v0, v27

    goto :goto_32

    .line 102
    :cond_45
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v1

    add-int/2addr v1, v0

    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    if-nez v0, :cond_47

    if-eqz v22, :cond_46

    goto :goto_30

    :cond_46
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    goto :goto_31

    :cond_47
    :goto_30
    const/4 v3, 0x0

    :goto_31
    add-int/2addr v3, v1

    add-int v0, v3, v2

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_32
    const/4 v9, 0x0

    :goto_33
    if-ge v9, v11, :cond_50

    .line 103
    iget-object v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    aget-object v10, v4, v9

    .line 104
    iget-object v4, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    aget v4, v4, v9

    .line 105
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    move-object/from16 v16, v5

    check-cast v16, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;

    if-eqz v13, :cond_4a

    if-eqz v14, :cond_48

    .line 106
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v0

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    add-int/2addr v0, v3

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    add-int/2addr v0, v3

    const/4 v3, 0x0

    :goto_34
    if-ge v3, v4, :cond_49

    .line 107
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    aget v5, v5, v3

    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    add-int/2addr v5, v6

    add-int/2addr v0, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_34

    .line 108
    :cond_48
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v0

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    add-int/2addr v0, v3

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    add-int/2addr v0, v3

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    mul-int v3, v3, v4

    add-int/2addr v0, v3

    iget v3, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->O〇8O8〇008:I

    mul-int v3, v3, v4

    add-int/2addr v0, v3

    .line 109
    :cond_49
    invoke-virtual {v15, v10}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    move-result v3

    add-int/2addr v3, v0

    move v5, v1

    move v6, v3

    move v3, v2

    move v2, v0

    goto :goto_36

    :cond_4a
    if-eqz v14, :cond_4b

    .line 110
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    add-int/2addr v1, v2

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    :goto_35
    if-ge v2, v4, :cond_4c

    .line 111
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    aget v5, v5, v2

    iget v6, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    add-int/2addr v5, v6

    add-int/2addr v1, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 112
    :cond_4b
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v1

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    add-int/2addr v1, v2

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    add-int/2addr v1, v2

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o800o8O:I

    mul-int v2, v2, v4

    add-int/2addr v1, v2

    iget v2, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇00:I

    mul-int v2, v2, v4

    add-int/2addr v1, v2

    .line 113
    :cond_4c
    invoke-virtual {v15, v10}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v1

    move v6, v0

    move v5, v1

    move/from16 v27, v3

    move v3, v2

    move/from16 v2, v27

    .line 114
    :goto_36
    sget-boolean v0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->oo〇:Z

    if-eqz v0, :cond_4d

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "layout item in position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->getViewPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " with text "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v1, v10

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " with SpanIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " into ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4d
    move-object/from16 v0, p0

    move-object v1, v10

    move/from16 v17, v2

    move/from16 v18, v3

    move v3, v5

    move v4, v6

    move/from16 v20, v5

    move/from16 v5, v18

    move/from16 v21, v6

    move-object/from16 v6, p5

    .line 116
    invoke-virtual/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->O8〇o(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 117
    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    move-result v0

    if-nez v0, :cond_4f

    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemChanged()Z

    move-result v0

    if-eqz v0, :cond_4e

    goto :goto_37

    :cond_4e
    const/4 v0, 0x1

    goto :goto_38

    :cond_4f
    :goto_37
    const/4 v0, 0x1

    .line 118
    iput-boolean v0, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇o〇:Z

    .line 119
    :goto_38
    iget-boolean v1, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    invoke-virtual {v10}, Landroid/view/View;->isFocusable()Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, v8, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    add-int/lit8 v9, v9, 0x1

    move/from16 v3, v17

    move/from16 v2, v18

    move/from16 v1, v20

    move/from16 v0, v21

    goto/16 :goto_33

    :cond_50
    const/4 v1, 0x0

    .line 120
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    .line 121
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇oOO8O8:[Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 122
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->〇0000OOO:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 123
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇〇0〇:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method

.method public 〇o〇(Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$State;->getItemCount()I

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    if-lez p3, :cond_2

    .line 6
    .line 7
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_2

    .line 12
    .line 13
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 14
    .line 15
    iget p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 16
    .line 17
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 18
    .line 19
    invoke-virtual {p1, p3, v0}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    iget-boolean p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇o〇:Z

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    if-eqz p3, :cond_0

    .line 27
    .line 28
    :goto_0
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 29
    .line 30
    sub-int/2addr p3, v0

    .line 31
    if-ge p1, p3, :cond_1

    .line 32
    .line 33
    iget p1, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    .line 36
    .line 37
    .line 38
    move-result-object p3

    .line 39
    invoke-virtual {p3}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    .line 40
    .line 41
    .line 42
    move-result-object p3

    .line 43
    check-cast p3, Ljava/lang/Integer;

    .line 44
    .line 45
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    .line 46
    .line 47
    .line 48
    move-result p3

    .line 49
    if-ge p1, p3, :cond_1

    .line 50
    .line 51
    iget p1, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 52
    .line 53
    add-int/2addr p1, v0

    .line 54
    iput p1, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 55
    .line 56
    iget-object p3, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 57
    .line 58
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 59
    .line 60
    invoke-virtual {p3, p1, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    goto :goto_0

    .line 65
    :cond_0
    :goto_1
    if-lez p1, :cond_1

    .line 66
    .line 67
    iget p1, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 68
    .line 69
    if-lez p1, :cond_1

    .line 70
    .line 71
    add-int/lit8 p1, p1, -0x1

    .line 72
    .line 73
    iput p1, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 74
    .line 75
    iget-object p3, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 76
    .line 77
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OoO8:I

    .line 78
    .line 79
    invoke-virtual {p3, p1, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    goto :goto_1

    .line 84
    :cond_1
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->OOO〇O0:Z

    .line 85
    .line 86
    :cond_2
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇〇808〇(II)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 2
    .line 3
    invoke-virtual {p2, p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->o〇0(I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;->o〇O8〇〇o:Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->O8()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

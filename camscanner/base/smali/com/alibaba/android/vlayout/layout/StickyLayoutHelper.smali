.class public Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;
.super Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;
.source "StickyLayoutHelper.java"


# instance fields
.field private o800o8O:I

.field private oo88o8O:I

.field private o〇O8〇〇o:Z

.field private 〇00:Z

.field private 〇O888o0o:Z

.field private 〇oo〇:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .line 2
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;-><init>()V

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    const/4 v1, 0x0

    .line 5
    iput-object v1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 6
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 7
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇00:Z

    .line 8
    iput-boolean p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    const/4 p1, 0x1

    .line 9
    invoke-virtual {p0, p1}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇〇8O0〇8(I)V

    return-void
.end method

.method private o〇0OOo〇0(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 2

    .line 1
    sget-boolean p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager;->〇〇o〇:Z

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    new-instance p2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v0, "abnormal pos: "

    .line 11
    .line 12
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 16
    .line 17
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v0, " start: "

    .line 21
    .line 22
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p3, " end: "

    .line 29
    .line 30
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    :cond_0
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 37
    .line 38
    if-eqz p2, :cond_8

    .line 39
    .line 40
    iget-boolean p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 41
    .line 42
    const/4 p3, 0x1

    .line 43
    if-eqz p2, :cond_4

    .line 44
    .line 45
    invoke-interface {p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    sub-int/2addr p2, p3

    .line 50
    :goto_0
    if-ltz p2, :cond_8

    .line 51
    .line 52
    invoke-interface {p5, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object p4

    .line 56
    invoke-interface {p5, p4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 61
    .line 62
    if-ge v0, v1, :cond_3

    .line 63
    .line 64
    invoke-virtual {p1, p4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    invoke-interface {p5, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    instance-of p4, p2, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 73
    .line 74
    if-eqz p4, :cond_1

    .line 75
    .line 76
    check-cast p2, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 77
    .line 78
    invoke-virtual {p2, p5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇〇0〇〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 79
    .line 80
    .line 81
    move-result p2

    .line 82
    :goto_1
    add-int/2addr p1, p2

    .line 83
    goto :goto_2

    .line 84
    :cond_1
    instance-of p4, p2, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 85
    .line 86
    if-eqz p4, :cond_2

    .line 87
    .line 88
    check-cast p2, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 89
    .line 90
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O888o0o()I

    .line 91
    .line 92
    .line 93
    move-result p4

    .line 94
    add-int/2addr p1, p4

    .line 95
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇oo〇()I

    .line 96
    .line 97
    .line 98
    move-result p2

    .line 99
    goto :goto_1

    .line 100
    :cond_2
    :goto_2
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 101
    .line 102
    iget-object p4, p0, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 103
    .line 104
    iget p4, p4, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 105
    .line 106
    add-int/2addr p2, p4

    .line 107
    if-lt p1, p2, :cond_8

    .line 108
    .line 109
    iput-boolean p3, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 110
    .line 111
    goto :goto_6

    .line 112
    :cond_3
    add-int/lit8 p2, p2, -0x1

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_4
    const/4 p2, 0x0

    .line 116
    :goto_3
    invoke-interface {p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 117
    .line 118
    .line 119
    move-result p4

    .line 120
    if-ge p2, p4, :cond_8

    .line 121
    .line 122
    invoke-interface {p5, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 123
    .line 124
    .line 125
    move-result-object p4

    .line 126
    invoke-interface {p5, p4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 131
    .line 132
    if-le v0, v1, :cond_7

    .line 133
    .line 134
    invoke-virtual {p1, p4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    invoke-interface {p5, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 139
    .line 140
    .line 141
    move-result-object p2

    .line 142
    instance-of p4, p2, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 143
    .line 144
    if-eqz p4, :cond_5

    .line 145
    .line 146
    check-cast p2, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 147
    .line 148
    invoke-virtual {p2, p5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o〇0OOo〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 149
    .line 150
    .line 151
    move-result p2

    .line 152
    :goto_4
    sub-int/2addr p1, p2

    .line 153
    goto :goto_5

    .line 154
    :cond_5
    instance-of p4, p2, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 155
    .line 156
    if-eqz p4, :cond_6

    .line 157
    .line 158
    check-cast p2, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 159
    .line 160
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oo88o8O()I

    .line 161
    .line 162
    .line 163
    move-result p4

    .line 164
    sub-int/2addr p1, p4

    .line 165
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇O8〇〇o()I

    .line 166
    .line 167
    .line 168
    move-result p2

    .line 169
    goto :goto_4

    .line 170
    :cond_6
    :goto_5
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 171
    .line 172
    iget-object p4, p0, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 173
    .line 174
    iget p4, p4, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 175
    .line 176
    add-int/2addr p2, p4

    .line 177
    if-lt p1, p2, :cond_8

    .line 178
    .line 179
    iput-boolean p3, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 180
    .line 181
    goto :goto_6

    .line 182
    :cond_7
    add-int/lit8 p2, p2, 0x1

    .line 183
    .line 184
    goto :goto_3

    .line 185
    :cond_8
    :goto_6
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method private 〇08O8o〇0(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 18

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v8, p5

    .line 6
    .line 7
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 12
    .line 13
    invoke-interface {v8, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->findViewByPosition(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    :cond_0
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    const/4 v3, 0x1

    .line 22
    if-ne v2, v3, :cond_1

    .line 23
    .line 24
    const/4 v2, 0x1

    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const/4 v2, 0x0

    .line 27
    :goto_0
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 28
    .line 29
    if-eqz v2, :cond_2

    .line 30
    .line 31
    iget v5, v5, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    iget v5, v5, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇080:I

    .line 35
    .line 36
    :goto_1
    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 37
    .line 38
    if-eqz v2, :cond_3

    .line 39
    .line 40
    iget v6, v6, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_3
    iget v6, v6, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o〇:I

    .line 44
    .line 45
    :goto_2
    iget-boolean v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 46
    .line 47
    if-eqz v9, :cond_4

    .line 48
    .line 49
    iget v10, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 50
    .line 51
    move/from16 v11, p4

    .line 52
    .line 53
    if-ge v11, v10, :cond_5

    .line 54
    .line 55
    :cond_4
    if-nez v9, :cond_b

    .line 56
    .line 57
    iget v10, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 58
    .line 59
    move/from16 v11, p3

    .line 60
    .line 61
    if-gt v11, v10, :cond_b

    .line 62
    .line 63
    :cond_5
    if-nez v1, :cond_8

    .line 64
    .line 65
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 66
    .line 67
    if-eqz v9, :cond_6

    .line 68
    .line 69
    move v9, v5

    .line 70
    goto :goto_3

    .line 71
    :cond_6
    move v9, v6

    .line 72
    :goto_3
    add-int/2addr v1, v9

    .line 73
    if-ltz v1, :cond_7

    .line 74
    .line 75
    const/4 v1, 0x1

    .line 76
    goto :goto_4

    .line 77
    :cond_7
    const/4 v1, 0x0

    .line 78
    :goto_4
    iget v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 79
    .line 80
    move-object/from16 v10, p2

    .line 81
    .line 82
    invoke-virtual {v10, v9}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v9

    .line 86
    iput-object v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 87
    .line 88
    invoke-direct {v7, v9, v8}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇〇〇0〇〇0(Landroid/view/View;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 89
    .line 90
    .line 91
    goto :goto_6

    .line 92
    :cond_8
    if-eqz v9, :cond_9

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 95
    .line 96
    .line 97
    move-result v9

    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 99
    .line 100
    .line 101
    move-result v10

    .line 102
    iget v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 103
    .line 104
    add-int/2addr v10, v11

    .line 105
    add-int/2addr v10, v5

    .line 106
    if-lt v9, v10, :cond_9

    .line 107
    .line 108
    iput-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 109
    .line 110
    :goto_5
    const/4 v1, 0x1

    .line 111
    goto :goto_6

    .line 112
    :cond_9
    iget-boolean v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 113
    .line 114
    if-nez v9, :cond_a

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 117
    .line 118
    .line 119
    move-result v9

    .line 120
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 121
    .line 122
    .line 123
    move-result v10

    .line 124
    iget v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 125
    .line 126
    sub-int/2addr v10, v11

    .line 127
    sub-int/2addr v10, v6

    .line 128
    if-gt v9, v10, :cond_a

    .line 129
    .line 130
    iput-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 131
    .line 132
    goto :goto_5

    .line 133
    :cond_a
    iput-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 134
    .line 135
    :cond_b
    const/4 v1, 0x0

    .line 136
    :goto_6
    iget-object v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 137
    .line 138
    if-eqz v9, :cond_2b

    .line 139
    .line 140
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 141
    .line 142
    .line 143
    move-result-object v9

    .line 144
    check-cast v9, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 145
    .line 146
    invoke-virtual {v9}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    .line 147
    .line 148
    .line 149
    move-result v9

    .line 150
    if-eqz v9, :cond_c

    .line 151
    .line 152
    return-void

    .line 153
    :cond_c
    iget-object v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 154
    .line 155
    invoke-virtual {v0, v9}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    .line 156
    .line 157
    .line 158
    move-result v9

    .line 159
    const/4 v10, 0x0

    .line 160
    const/4 v11, -0x1

    .line 161
    if-eqz v2, :cond_1f

    .line 162
    .line 163
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇8O0〇8()Z

    .line 164
    .line 165
    .line 166
    move-result v2

    .line 167
    if-eqz v2, :cond_d

    .line 168
    .line 169
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 170
    .line 171
    .line 172
    move-result v2

    .line 173
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 174
    .line 175
    .line 176
    move-result v12

    .line 177
    sub-int/2addr v2, v12

    .line 178
    iget-object v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 179
    .line 180
    invoke-virtual {v0, v12}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 181
    .line 182
    .line 183
    move-result v12

    .line 184
    sub-int v12, v2, v12

    .line 185
    .line 186
    goto :goto_7

    .line 187
    :cond_d
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 188
    .line 189
    .line 190
    move-result v12

    .line 191
    iget-object v2, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 192
    .line 193
    invoke-virtual {v0, v2}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 194
    .line 195
    .line 196
    move-result v2

    .line 197
    add-int/2addr v2, v12

    .line 198
    :goto_7
    if-eqz v1, :cond_1b

    .line 199
    .line 200
    iget-boolean v13, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 201
    .line 202
    if-eqz v13, :cond_11

    .line 203
    .line 204
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 205
    .line 206
    .line 207
    move-result v13

    .line 208
    sub-int/2addr v13, v3

    .line 209
    move-object v14, v10

    .line 210
    :goto_8
    if-ltz v13, :cond_15

    .line 211
    .line 212
    invoke-interface {v8, v13}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 213
    .line 214
    .line 215
    move-result-object v14

    .line 216
    invoke-interface {v8, v14}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 217
    .line 218
    .line 219
    move-result v15

    .line 220
    iget v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 221
    .line 222
    if-ge v15, v4, :cond_10

    .line 223
    .line 224
    invoke-virtual {v0, v14}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 225
    .line 226
    .line 227
    move-result v4

    .line 228
    invoke-interface {v8, v15}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 229
    .line 230
    .line 231
    move-result-object v11

    .line 232
    instance-of v15, v11, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 233
    .line 234
    if-eqz v15, :cond_e

    .line 235
    .line 236
    check-cast v11, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 237
    .line 238
    invoke-virtual {v11, v8}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇〇0〇〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 239
    .line 240
    .line 241
    move-result v11

    .line 242
    :goto_9
    add-int/2addr v4, v11

    .line 243
    goto :goto_a

    .line 244
    :cond_e
    instance-of v15, v11, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 245
    .line 246
    if-eqz v15, :cond_f

    .line 247
    .line 248
    check-cast v11, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 249
    .line 250
    invoke-virtual {v11}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O888o0o()I

    .line 251
    .line 252
    .line 253
    move-result v15

    .line 254
    add-int/2addr v4, v15

    .line 255
    invoke-virtual {v11}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇oo〇()I

    .line 256
    .line 257
    .line 258
    move-result v11

    .line 259
    goto :goto_9

    .line 260
    :cond_f
    :goto_a
    add-int v11, v4, v9

    .line 261
    .line 262
    add-int/2addr v13, v3

    .line 263
    move v3, v11

    .line 264
    move v11, v13

    .line 265
    goto :goto_e

    .line 266
    :cond_10
    add-int/lit8 v13, v13, -0x1

    .line 267
    .line 268
    goto :goto_8

    .line 269
    :cond_11
    move-object v14, v10

    .line 270
    const/4 v3, 0x0

    .line 271
    :goto_b
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 272
    .line 273
    .line 274
    move-result v4

    .line 275
    if-ge v3, v4, :cond_15

    .line 276
    .line 277
    invoke-interface {v8, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 278
    .line 279
    .line 280
    move-result-object v14

    .line 281
    invoke-interface {v8, v14}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 282
    .line 283
    .line 284
    move-result v4

    .line 285
    iget v13, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 286
    .line 287
    if-le v4, v13, :cond_14

    .line 288
    .line 289
    invoke-virtual {v0, v14}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 290
    .line 291
    .line 292
    move-result v11

    .line 293
    invoke-interface {v8, v4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 294
    .line 295
    .line 296
    move-result-object v4

    .line 297
    instance-of v13, v4, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 298
    .line 299
    if-eqz v13, :cond_12

    .line 300
    .line 301
    check-cast v4, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 302
    .line 303
    invoke-virtual {v4, v8}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o〇0OOo〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 304
    .line 305
    .line 306
    move-result v4

    .line 307
    :goto_c
    sub-int/2addr v11, v4

    .line 308
    goto :goto_d

    .line 309
    :cond_12
    instance-of v13, v4, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 310
    .line 311
    if-eqz v13, :cond_13

    .line 312
    .line 313
    check-cast v4, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 314
    .line 315
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oo88o8O()I

    .line 316
    .line 317
    .line 318
    move-result v13

    .line 319
    sub-int/2addr v11, v13

    .line 320
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇O8〇〇o()I

    .line 321
    .line 322
    .line 323
    move-result v4

    .line 324
    goto :goto_c

    .line 325
    :cond_13
    :goto_d
    sub-int v4, v11, v9

    .line 326
    .line 327
    move/from16 v17, v11

    .line 328
    .line 329
    move v11, v3

    .line 330
    move/from16 v3, v17

    .line 331
    .line 332
    goto :goto_e

    .line 333
    :cond_14
    add-int/lit8 v3, v3, 0x1

    .line 334
    .line 335
    goto :goto_b

    .line 336
    :cond_15
    const/4 v3, 0x0

    .line 337
    const/4 v4, 0x0

    .line 338
    :goto_e
    if-eqz v14, :cond_16

    .line 339
    .line 340
    if-gez v11, :cond_17

    .line 341
    .line 342
    :cond_16
    const/4 v1, 0x0

    .line 343
    :cond_17
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 344
    .line 345
    .line 346
    move-result v13

    .line 347
    if-nez v13, :cond_19

    .line 348
    .line 349
    iget-boolean v13, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 350
    .line 351
    if-nez v13, :cond_18

    .line 352
    .line 353
    goto :goto_f

    .line 354
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 355
    .line 356
    .line 357
    move-result v13

    .line 358
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 359
    .line 360
    add-int/2addr v13, v14

    .line 361
    add-int/2addr v13, v5

    .line 362
    if-ge v4, v13, :cond_1a

    .line 363
    .line 364
    goto :goto_10

    .line 365
    :cond_19
    :goto_f
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 366
    .line 367
    .line 368
    move-result v13

    .line 369
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 370
    .line 371
    sub-int/2addr v13, v14

    .line 372
    sub-int/2addr v13, v6

    .line 373
    if-le v3, v13, :cond_1a

    .line 374
    .line 375
    :goto_10
    move/from16 v16, v4

    .line 376
    .line 377
    const/4 v4, 0x0

    .line 378
    goto :goto_11

    .line 379
    :cond_1a
    move/from16 v16, v4

    .line 380
    .line 381
    move v4, v1

    .line 382
    goto :goto_11

    .line 383
    :cond_1b
    move v4, v1

    .line 384
    const/4 v3, 0x0

    .line 385
    const/16 v16, 0x0

    .line 386
    .line 387
    :goto_11
    if-nez v4, :cond_1e

    .line 388
    .line 389
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 390
    .line 391
    .line 392
    move-result v1

    .line 393
    if-nez v1, :cond_1d

    .line 394
    .line 395
    iget-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 396
    .line 397
    if-nez v1, :cond_1c

    .line 398
    .line 399
    goto :goto_12

    .line 400
    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 401
    .line 402
    .line 403
    move-result v0

    .line 404
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 405
    .line 406
    add-int/2addr v0, v1

    .line 407
    add-int/2addr v0, v5

    .line 408
    add-int/2addr v9, v0

    .line 409
    move v3, v0

    .line 410
    move v5, v9

    .line 411
    goto :goto_13

    .line 412
    :cond_1d
    :goto_12
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 413
    .line 414
    .line 415
    move-result v0

    .line 416
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 417
    .line 418
    sub-int/2addr v0, v1

    .line 419
    sub-int/2addr v0, v6

    .line 420
    sub-int v1, v0, v9

    .line 421
    .line 422
    move v5, v0

    .line 423
    move v3, v1

    .line 424
    :goto_13
    move v9, v4

    .line 425
    goto :goto_14

    .line 426
    :cond_1e
    move v5, v3

    .line 427
    move v9, v4

    .line 428
    move/from16 v3, v16

    .line 429
    .line 430
    :goto_14
    move v4, v2

    .line 431
    move v2, v12

    .line 432
    goto/16 :goto_1a

    .line 433
    .line 434
    :cond_1f
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 435
    .line 436
    .line 437
    move-result v2

    .line 438
    iget-object v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 439
    .line 440
    invoke-virtual {v0, v4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 441
    .line 442
    .line 443
    move-result v4

    .line 444
    add-int/2addr v4, v2

    .line 445
    if-eqz v1, :cond_25

    .line 446
    .line 447
    iget-boolean v5, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 448
    .line 449
    if-eqz v5, :cond_22

    .line 450
    .line 451
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 452
    .line 453
    .line 454
    move-result v5

    .line 455
    sub-int/2addr v5, v3

    .line 456
    :goto_15
    if-ltz v5, :cond_21

    .line 457
    .line 458
    invoke-interface {v8, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 459
    .line 460
    .line 461
    move-result-object v3

    .line 462
    invoke-interface {v8, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 463
    .line 464
    .line 465
    move-result v6

    .line 466
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 467
    .line 468
    if-ge v6, v12, :cond_20

    .line 469
    .line 470
    invoke-virtual {v0, v3}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 471
    .line 472
    .line 473
    move-result v0

    .line 474
    add-int v3, v0, v9

    .line 475
    .line 476
    move/from16 v16, v0

    .line 477
    .line 478
    goto :goto_16

    .line 479
    :cond_20
    add-int/lit8 v5, v5, -0x1

    .line 480
    .line 481
    goto :goto_15

    .line 482
    :cond_21
    const/4 v3, 0x0

    .line 483
    const/16 v16, 0x0

    .line 484
    .line 485
    :goto_16
    move v0, v3

    .line 486
    goto :goto_18

    .line 487
    :cond_22
    const/4 v3, 0x0

    .line 488
    :goto_17
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 489
    .line 490
    .line 491
    move-result v5

    .line 492
    if-ge v3, v5, :cond_24

    .line 493
    .line 494
    invoke-interface {v8, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 495
    .line 496
    .line 497
    move-result-object v5

    .line 498
    invoke-interface {v8, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 499
    .line 500
    .line 501
    move-result v6

    .line 502
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 503
    .line 504
    if-le v6, v12, :cond_23

    .line 505
    .line 506
    invoke-virtual {v0, v5}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 507
    .line 508
    .line 509
    move-result v0

    .line 510
    sub-int v3, v0, v9

    .line 511
    .line 512
    move/from16 v16, v3

    .line 513
    .line 514
    goto :goto_18

    .line 515
    :cond_23
    add-int/lit8 v3, v3, 0x1

    .line 516
    .line 517
    goto :goto_17

    .line 518
    :cond_24
    const/4 v0, 0x0

    .line 519
    const/16 v16, 0x0

    .line 520
    .line 521
    :goto_18
    move v9, v1

    .line 522
    move v3, v2

    .line 523
    move v5, v4

    .line 524
    move/from16 v2, v16

    .line 525
    .line 526
    move v4, v0

    .line 527
    goto :goto_1a

    .line 528
    :cond_25
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 529
    .line 530
    .line 531
    move-result v3

    .line 532
    if-nez v3, :cond_27

    .line 533
    .line 534
    iget-boolean v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 535
    .line 536
    if-nez v3, :cond_26

    .line 537
    .line 538
    goto :goto_19

    .line 539
    :cond_26
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 540
    .line 541
    .line 542
    move-result v0

    .line 543
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 544
    .line 545
    add-int/2addr v0, v3

    .line 546
    add-int/2addr v0, v5

    .line 547
    add-int/2addr v9, v0

    .line 548
    move v3, v2

    .line 549
    move v5, v4

    .line 550
    move v4, v9

    .line 551
    move v2, v0

    .line 552
    move v9, v1

    .line 553
    goto :goto_1a

    .line 554
    :cond_27
    :goto_19
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 555
    .line 556
    .line 557
    move-result v0

    .line 558
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 559
    .line 560
    sub-int/2addr v0, v3

    .line 561
    sub-int/2addr v0, v6

    .line 562
    sub-int v3, v0, v9

    .line 563
    .line 564
    move v9, v1

    .line 565
    move v5, v4

    .line 566
    move v4, v0

    .line 567
    move/from16 v17, v3

    .line 568
    .line 569
    move v3, v2

    .line 570
    move/from16 v2, v17

    .line 571
    .line 572
    :goto_1a
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 573
    .line 574
    move-object/from16 v0, p0

    .line 575
    .line 576
    move-object/from16 v6, p5

    .line 577
    .line 578
    invoke-virtual/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->O8〇o(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 579
    .line 580
    .line 581
    if-eqz v9, :cond_29

    .line 582
    .line 583
    if-ltz v11, :cond_2a

    .line 584
    .line 585
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 586
    .line 587
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 588
    .line 589
    .line 590
    move-result-object v0

    .line 591
    if-nez v0, :cond_28

    .line 592
    .line 593
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 594
    .line 595
    invoke-interface {v8, v0, v11}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇8o8o〇(Landroid/view/View;I)V

    .line 596
    .line 597
    .line 598
    :cond_28
    iput-object v10, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 599
    .line 600
    goto :goto_1b

    .line 601
    :cond_29
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 602
    .line 603
    invoke-interface {v8, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇888(Landroid/view/View;)V

    .line 604
    .line 605
    .line 606
    :cond_2a
    :goto_1b
    move v1, v9

    .line 607
    :cond_2b
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 608
    .line 609
    return-void
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
.end method

.method private 〇〇0o(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 17

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v8, p5

    .line 6
    .line 7
    iget-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 8
    .line 9
    const/4 v9, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget v2, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 13
    .line 14
    move/from16 v3, p4

    .line 15
    .line 16
    if-ge v3, v2, :cond_1

    .line 17
    .line 18
    :cond_0
    if-nez v1, :cond_20

    .line 19
    .line 20
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 21
    .line 22
    move/from16 v2, p3

    .line 23
    .line 24
    if-gt v2, v1, :cond_20

    .line 25
    .line 26
    :cond_1
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    const/4 v3, 0x1

    .line 37
    const/4 v4, 0x0

    .line 38
    if-ne v2, v3, :cond_2

    .line 39
    .line 40
    const/4 v2, 0x1

    .line 41
    goto :goto_0

    .line 42
    :cond_2
    const/4 v2, 0x0

    .line 43
    :goto_0
    iget-object v5, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 44
    .line 45
    if-eqz v2, :cond_3

    .line 46
    .line 47
    iget v5, v5, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_3
    iget v5, v5, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇080:I

    .line 51
    .line 52
    :goto_1
    iget-object v6, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 53
    .line 54
    if-eqz v2, :cond_4

    .line 55
    .line 56
    iget v6, v6, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_4
    iget v6, v6, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o〇:I

    .line 60
    .line 61
    :goto_2
    const/4 v10, -0x1

    .line 62
    if-eqz v2, :cond_16

    .line 63
    .line 64
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇8O0〇8()Z

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    if-eqz v2, :cond_5

    .line 69
    .line 70
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 75
    .line 76
    .line 77
    move-result v11

    .line 78
    sub-int/2addr v2, v11

    .line 79
    iget-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 80
    .line 81
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 82
    .line 83
    .line 84
    move-result v11

    .line 85
    sub-int v11, v2, v11

    .line 86
    .line 87
    goto :goto_3

    .line 88
    :cond_5
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 89
    .line 90
    .line 91
    move-result v11

    .line 92
    iget-object v2, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 93
    .line 94
    invoke-virtual {v0, v2}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    add-int/2addr v2, v11

    .line 99
    :goto_3
    iget-boolean v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 100
    .line 101
    if-eqz v12, :cond_9

    .line 102
    .line 103
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 104
    .line 105
    .line 106
    move-result v12

    .line 107
    sub-int/2addr v12, v3

    .line 108
    move-object v13, v9

    .line 109
    :goto_4
    if-ltz v12, :cond_d

    .line 110
    .line 111
    invoke-interface {v8, v12}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 112
    .line 113
    .line 114
    move-result-object v13

    .line 115
    invoke-interface {v8, v13}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 116
    .line 117
    .line 118
    move-result v14

    .line 119
    iget v15, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 120
    .line 121
    if-ge v14, v15, :cond_8

    .line 122
    .line 123
    invoke-virtual {v0, v13}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 124
    .line 125
    .line 126
    move-result v10

    .line 127
    invoke-interface {v8, v14}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 128
    .line 129
    .line 130
    move-result-object v14

    .line 131
    instance-of v15, v14, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 132
    .line 133
    if-eqz v15, :cond_6

    .line 134
    .line 135
    check-cast v14, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 136
    .line 137
    invoke-virtual {v14, v8}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇〇0〇〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 138
    .line 139
    .line 140
    move-result v14

    .line 141
    :goto_5
    add-int/2addr v10, v14

    .line 142
    goto :goto_6

    .line 143
    :cond_6
    instance-of v15, v14, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 144
    .line 145
    if-eqz v15, :cond_7

    .line 146
    .line 147
    check-cast v14, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 148
    .line 149
    invoke-virtual {v14}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O888o0o()I

    .line 150
    .line 151
    .line 152
    move-result v15

    .line 153
    add-int/2addr v10, v15

    .line 154
    invoke-virtual {v14}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇oo〇()I

    .line 155
    .line 156
    .line 157
    move-result v14

    .line 158
    goto :goto_5

    .line 159
    :cond_7
    :goto_6
    add-int v14, v10, v1

    .line 160
    .line 161
    iput-boolean v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 162
    .line 163
    goto :goto_a

    .line 164
    :cond_8
    add-int/lit8 v12, v12, -0x1

    .line 165
    .line 166
    goto :goto_4

    .line 167
    :cond_9
    move-object v13, v9

    .line 168
    const/4 v12, 0x0

    .line 169
    :goto_7
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 170
    .line 171
    .line 172
    move-result v14

    .line 173
    if-ge v12, v14, :cond_d

    .line 174
    .line 175
    invoke-interface {v8, v12}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 176
    .line 177
    .line 178
    move-result-object v13

    .line 179
    invoke-interface {v8, v13}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 180
    .line 181
    .line 182
    move-result v14

    .line 183
    iget v15, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 184
    .line 185
    if-le v14, v15, :cond_c

    .line 186
    .line 187
    invoke-virtual {v0, v13}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 188
    .line 189
    .line 190
    move-result v10

    .line 191
    invoke-interface {v8, v14}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 192
    .line 193
    .line 194
    move-result-object v14

    .line 195
    instance-of v15, v14, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 196
    .line 197
    if-eqz v15, :cond_a

    .line 198
    .line 199
    check-cast v14, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;

    .line 200
    .line 201
    invoke-virtual {v14, v8}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o〇0OOo〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 202
    .line 203
    .line 204
    move-result v14

    .line 205
    :goto_8
    sub-int/2addr v10, v14

    .line 206
    goto :goto_9

    .line 207
    :cond_a
    instance-of v15, v14, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 208
    .line 209
    if-eqz v15, :cond_b

    .line 210
    .line 211
    check-cast v14, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 212
    .line 213
    invoke-virtual {v14}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oo88o8O()I

    .line 214
    .line 215
    .line 216
    move-result v15

    .line 217
    sub-int/2addr v10, v15

    .line 218
    invoke-virtual {v14}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇O8〇〇o()I

    .line 219
    .line 220
    .line 221
    move-result v14

    .line 222
    goto :goto_8

    .line 223
    :cond_b
    :goto_9
    move v14, v10

    .line 224
    sub-int v10, v14, v1

    .line 225
    .line 226
    add-int/2addr v12, v3

    .line 227
    iput-boolean v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 228
    .line 229
    :goto_a
    move v3, v10

    .line 230
    move v10, v12

    .line 231
    goto :goto_b

    .line 232
    :cond_c
    add-int/lit8 v12, v12, 0x1

    .line 233
    .line 234
    goto :goto_7

    .line 235
    :cond_d
    const/4 v3, 0x0

    .line 236
    const/4 v14, 0x0

    .line 237
    :goto_b
    if-eqz v13, :cond_e

    .line 238
    .line 239
    if-gez v10, :cond_f

    .line 240
    .line 241
    :cond_e
    iput-boolean v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 242
    .line 243
    :cond_f
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 244
    .line 245
    .line 246
    move-result v12

    .line 247
    if-nez v12, :cond_11

    .line 248
    .line 249
    iget-boolean v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 250
    .line 251
    if-nez v12, :cond_10

    .line 252
    .line 253
    goto :goto_c

    .line 254
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 255
    .line 256
    .line 257
    move-result v12

    .line 258
    iget v13, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 259
    .line 260
    add-int/2addr v12, v13

    .line 261
    add-int/2addr v12, v5

    .line 262
    if-ge v3, v12, :cond_12

    .line 263
    .line 264
    iput-boolean v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 265
    .line 266
    goto :goto_d

    .line 267
    :cond_11
    :goto_c
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 268
    .line 269
    .line 270
    move-result v12

    .line 271
    iget v13, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 272
    .line 273
    sub-int/2addr v12, v13

    .line 274
    sub-int/2addr v12, v6

    .line 275
    if-le v14, v12, :cond_12

    .line 276
    .line 277
    iput-boolean v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 278
    .line 279
    :cond_12
    :goto_d
    iget-boolean v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 280
    .line 281
    if-nez v4, :cond_15

    .line 282
    .line 283
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 284
    .line 285
    .line 286
    move-result v3

    .line 287
    if-nez v3, :cond_14

    .line 288
    .line 289
    iget-boolean v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 290
    .line 291
    if-nez v3, :cond_13

    .line 292
    .line 293
    goto :goto_e

    .line 294
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 295
    .line 296
    .line 297
    move-result v0

    .line 298
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 299
    .line 300
    add-int/2addr v0, v3

    .line 301
    add-int v3, v0, v5

    .line 302
    .line 303
    add-int v14, v3, v1

    .line 304
    .line 305
    goto :goto_f

    .line 306
    :cond_14
    :goto_e
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 307
    .line 308
    .line 309
    move-result v0

    .line 310
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 311
    .line 312
    sub-int/2addr v0, v3

    .line 313
    sub-int v14, v0, v6

    .line 314
    .line 315
    sub-int v3, v14, v1

    .line 316
    .line 317
    :cond_15
    :goto_f
    move v4, v2

    .line 318
    move v2, v11

    .line 319
    move v5, v14

    .line 320
    goto/16 :goto_14

    .line 321
    .line 322
    :cond_16
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 323
    .line 324
    .line 325
    move-result v2

    .line 326
    iget-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 327
    .line 328
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 329
    .line 330
    .line 331
    move-result v11

    .line 332
    add-int/2addr v11, v2

    .line 333
    iget-boolean v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 334
    .line 335
    if-eqz v12, :cond_1b

    .line 336
    .line 337
    iget-boolean v5, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 338
    .line 339
    if-eqz v5, :cond_18

    .line 340
    .line 341
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 342
    .line 343
    .line 344
    move-result v5

    .line 345
    sub-int/2addr v5, v3

    .line 346
    :goto_10
    if-ltz v5, :cond_1a

    .line 347
    .line 348
    invoke-interface {v8, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 349
    .line 350
    .line 351
    move-result-object v3

    .line 352
    invoke-interface {v8, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 353
    .line 354
    .line 355
    move-result v6

    .line 356
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 357
    .line 358
    if-ge v6, v12, :cond_17

    .line 359
    .line 360
    invoke-virtual {v0, v3}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->O8(Landroid/view/View;)I

    .line 361
    .line 362
    .line 363
    move-result v4

    .line 364
    add-int v0, v4, v1

    .line 365
    .line 366
    goto :goto_12

    .line 367
    :cond_17
    add-int/lit8 v5, v5, -0x1

    .line 368
    .line 369
    goto :goto_10

    .line 370
    :cond_18
    const/4 v3, 0x0

    .line 371
    :goto_11
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildCount()I

    .line 372
    .line 373
    .line 374
    move-result v5

    .line 375
    if-ge v3, v5, :cond_1a

    .line 376
    .line 377
    invoke-interface {v8, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getChildAt(I)Landroid/view/View;

    .line 378
    .line 379
    .line 380
    move-result-object v5

    .line 381
    invoke-interface {v8, v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 382
    .line 383
    .line 384
    move-result v6

    .line 385
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 386
    .line 387
    if-le v6, v12, :cond_19

    .line 388
    .line 389
    invoke-virtual {v0, v5}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇〇888(Landroid/view/View;)I

    .line 390
    .line 391
    .line 392
    move-result v4

    .line 393
    sub-int v0, v4, v1

    .line 394
    .line 395
    move/from16 v16, v4

    .line 396
    .line 397
    move v4, v0

    .line 398
    move/from16 v0, v16

    .line 399
    .line 400
    goto :goto_12

    .line 401
    :cond_19
    add-int/lit8 v3, v3, 0x1

    .line 402
    .line 403
    goto :goto_11

    .line 404
    :cond_1a
    const/4 v0, 0x0

    .line 405
    :goto_12
    move v3, v2

    .line 406
    move v2, v4

    .line 407
    move v5, v11

    .line 408
    move v4, v0

    .line 409
    goto :goto_14

    .line 410
    :cond_1b
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 411
    .line 412
    .line 413
    move-result v3

    .line 414
    if-nez v3, :cond_1d

    .line 415
    .line 416
    iget-boolean v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 417
    .line 418
    if-nez v3, :cond_1c

    .line 419
    .line 420
    goto :goto_13

    .line 421
    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 422
    .line 423
    .line 424
    move-result v0

    .line 425
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 426
    .line 427
    add-int/2addr v0, v3

    .line 428
    add-int/2addr v0, v5

    .line 429
    add-int/2addr v1, v0

    .line 430
    move v4, v1

    .line 431
    move v3, v2

    .line 432
    move v5, v11

    .line 433
    move v2, v0

    .line 434
    goto :goto_14

    .line 435
    :cond_1d
    :goto_13
    invoke-virtual/range {p1 .. p1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 436
    .line 437
    .line 438
    move-result v0

    .line 439
    iget v3, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 440
    .line 441
    sub-int/2addr v0, v3

    .line 442
    sub-int/2addr v0, v6

    .line 443
    sub-int v1, v0, v1

    .line 444
    .line 445
    move v4, v0

    .line 446
    move v3, v2

    .line 447
    move v5, v11

    .line 448
    move v2, v1

    .line 449
    :goto_14
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 450
    .line 451
    move-object/from16 v0, p0

    .line 452
    .line 453
    move-object/from16 v6, p5

    .line 454
    .line 455
    invoke-virtual/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->O8〇o(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 456
    .line 457
    .line 458
    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 459
    .line 460
    if-eqz v0, :cond_1f

    .line 461
    .line 462
    if-ltz v10, :cond_21

    .line 463
    .line 464
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 465
    .line 466
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 467
    .line 468
    .line 469
    move-result-object v0

    .line 470
    if-nez v0, :cond_1e

    .line 471
    .line 472
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 473
    .line 474
    invoke-interface {v8, v0, v10}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇8o8o〇(Landroid/view/View;I)V

    .line 475
    .line 476
    .line 477
    :cond_1e
    iput-object v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 478
    .line 479
    goto :goto_15

    .line 480
    :cond_1f
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 481
    .line 482
    invoke-interface {v8, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->showView(Landroid/view/View;)V

    .line 483
    .line 484
    .line 485
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 486
    .line 487
    invoke-interface {v8, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇888(Landroid/view/View;)V

    .line 488
    .line 489
    .line 490
    goto :goto_15

    .line 491
    :cond_20
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 492
    .line 493
    invoke-interface {v8, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 494
    .line 495
    .line 496
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 497
    .line 498
    invoke-interface {v8, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O00(Landroid/view/View;)V

    .line 499
    .line 500
    .line 501
    iput-object v9, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 502
    .line 503
    :cond_21
    :goto_15
    return-void
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
.end method

.method private 〇〇〇0〇〇0(Landroid/view/View;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;

    .line 6
    .line 7
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-ne v1, v3, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v1, 0x0

    .line 18
    :goto_0
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    sub-int/2addr v4, v5

    .line 27
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    sub-int/2addr v4, v5

    .line 32
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OoO8()I

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    sub-int/2addr v4, v5

    .line 37
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇808〇()I

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 42
    .line 43
    .line 44
    move-result v6

    .line 45
    sub-int/2addr v5, v6

    .line 46
    invoke-interface {p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingBottom()I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    sub-int/2addr v5, v6

    .line 51
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇00()I

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    sub-int/2addr v5, v6

    .line 56
    iget v6, v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    .line 57
    .line 58
    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    .line 59
    .line 60
    const/high16 v9, 0x40000000    # 2.0f

    .line 61
    .line 62
    const/4 v10, 0x0

    .line 63
    if-eqz v1, :cond_3

    .line 64
    .line 65
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 66
    .line 67
    invoke-interface {p2, v4, v1, v2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇00(IIZ)I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    if-nez v2, :cond_1

    .line 76
    .line 77
    cmpl-float v2, v6, v10

    .line 78
    .line 79
    if-lez v2, :cond_1

    .line 80
    .line 81
    int-to-float v0, v4

    .line 82
    div-float/2addr v0, v6

    .line 83
    const/high16 v2, 0x3f000000    # 0.5f

    .line 84
    .line 85
    add-float/2addr v0, v2

    .line 86
    float-to-int v0, v0

    .line 87
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    goto :goto_1

    .line 92
    :cond_1
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 93
    .line 94
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    if-nez v2, :cond_2

    .line 99
    .line 100
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 101
    .line 102
    cmpl-float v6, v2, v10

    .line 103
    .line 104
    if-lez v6, :cond_2

    .line 105
    .line 106
    int-to-float v0, v4

    .line 107
    div-float/2addr v0, v2

    .line 108
    float-to-double v2, v0

    .line 109
    add-double/2addr v2, v7

    .line 110
    double-to-int v0, v2

    .line 111
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    goto :goto_1

    .line 116
    :cond_2
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 117
    .line 118
    invoke-interface {p2, v5, v0, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇00(IIZ)I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    :goto_1
    invoke-interface {p2, p1, v1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    .line 123
    .line 124
    .line 125
    goto :goto_3

    .line 126
    :cond_3
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 127
    .line 128
    invoke-interface {p2, v5, v1, v2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇00(IIZ)I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    .line 133
    .line 134
    .line 135
    move-result v2

    .line 136
    if-nez v2, :cond_4

    .line 137
    .line 138
    cmpl-float v2, v6, v10

    .line 139
    .line 140
    if-lez v2, :cond_4

    .line 141
    .line 142
    int-to-float v0, v5

    .line 143
    mul-float v0, v0, v6

    .line 144
    .line 145
    float-to-double v2, v0

    .line 146
    add-double/2addr v2, v7

    .line 147
    double-to-int v0, v2

    .line 148
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    goto :goto_2

    .line 153
    :cond_4
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 154
    .line 155
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    if-nez v2, :cond_5

    .line 160
    .line 161
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 162
    .line 163
    cmpl-float v6, v2, v10

    .line 164
    .line 165
    if-lez v6, :cond_5

    .line 166
    .line 167
    int-to-float v0, v5

    .line 168
    mul-float v0, v0, v2

    .line 169
    .line 170
    float-to-double v2, v0

    .line 171
    add-double/2addr v2, v7

    .line 172
    double-to-int v0, v2

    .line 173
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 174
    .line 175
    .line 176
    move-result v0

    .line 177
    goto :goto_2

    .line 178
    :cond_5
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 179
    .line 180
    invoke-interface {p2, v4, v0, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇00(IIZ)I

    .line 181
    .line 182
    .line 183
    move-result v0

    .line 184
    :goto_2
    invoke-interface {p2, p1, v0, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    .line 185
    .line 186
    .line 187
    :goto_3
    return-void
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method


# virtual methods
.method public o8()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O00(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 14
    .line 15
    .line 16
    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 6

    .line 1
    invoke-super/range {p0 .. p6}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget p5, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 5
    .line 6
    if-gez p5, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->Oooo8o0〇()Lcom/alibaba/android/vlayout/OrientationHelperEx;

    .line 10
    .line 11
    .line 12
    move-result-object p5

    .line 13
    iget-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 18
    .line 19
    if-lt v0, p3, :cond_1

    .line 20
    .line 21
    if-gt v0, p4, :cond_1

    .line 22
    .line 23
    move-object v0, p0

    .line 24
    move-object v1, p5

    .line 25
    move-object v2, p1

    .line 26
    move v3, p3

    .line 27
    move v4, p4

    .line 28
    move-object v5, p6

    .line 29
    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇0OOo〇0(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 30
    .line 31
    .line 32
    :cond_1
    iget-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 33
    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_3

    .line 41
    .line 42
    :cond_2
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 43
    .line 44
    .line 45
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 46
    .line 47
    if-eqz p2, :cond_6

    .line 48
    .line 49
    invoke-interface {p6, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 50
    .line 51
    .line 52
    :cond_3
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 53
    .line 54
    iget-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 55
    .line 56
    if-nez v0, :cond_5

    .line 57
    .line 58
    if-eqz p2, :cond_5

    .line 59
    .line 60
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    if-nez p2, :cond_4

    .line 65
    .line 66
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 67
    .line 68
    invoke-interface {p6, p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇888(Landroid/view/View;)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_4
    move-object v0, p0

    .line 73
    move-object v1, p5

    .line 74
    move-object v2, p1

    .line 75
    move v3, p3

    .line 76
    move v4, p4

    .line 77
    move-object v5, p6

    .line 78
    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇〇0o(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_5
    move-object v0, p0

    .line 83
    move-object v1, p5

    .line 84
    move-object v2, p1

    .line 85
    move v3, p3

    .line 86
    move v4, p4

    .line 87
    move-object v5, p6

    .line 88
    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇08O8o〇0(Lcom/alibaba/android/vlayout/OrientationHelperEx;Landroidx/recyclerview/widget/RecyclerView$Recycler;IILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 89
    .line 90
    .line 91
    :cond_6
    :goto_0
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method public 〇o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 16

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p3

    .line 4
    .line 5
    move-object/from16 v9, p4

    .line 6
    .line 7
    move-object/from16 v10, p5

    .line 8
    .line 9
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {v7, v0}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    move-object/from16 v1, p1

    .line 25
    .line 26
    invoke-virtual {v8, v1}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇8o8o〇(Landroidx/recyclerview/widget/RecyclerView$Recycler;)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->OO0o〇〇()V

    .line 32
    .line 33
    .line 34
    :goto_0
    move-object v11, v0

    .line 35
    const/4 v12, 0x1

    .line 36
    if-nez v11, :cond_2

    .line 37
    .line 38
    iput-boolean v12, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇o00〇〇Oo:Z

    .line 39
    .line 40
    return-void

    .line 41
    :cond_2
    invoke-direct {v7, v11, v10}, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇〇〇0〇〇0(Landroid/view/View;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 42
    .line 43
    .line 44
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    const/4 v1, 0x0

    .line 49
    if-ne v0, v12, :cond_3

    .line 50
    .line 51
    const/4 v13, 0x1

    .line 52
    goto :goto_1

    .line 53
    :cond_3
    const/4 v13, 0x0

    .line 54
    :goto_1
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->Oooo8o0〇()Lcom/alibaba/android/vlayout/OrientationHelperEx;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    iput v2, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 63
    .line 64
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 69
    .line 70
    iput-boolean v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 71
    .line 72
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o00〇〇Oo()I

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    iget v3, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 77
    .line 78
    sub-int/2addr v2, v3

    .line 79
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->O8()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    add-int/2addr v2, v3

    .line 84
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    const/4 v4, -0x1

    .line 89
    if-ne v3, v12, :cond_f

    .line 90
    .line 91
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇8O0〇8()Z

    .line 92
    .line 93
    .line 94
    move-result v3

    .line 95
    if-eqz v3, :cond_4

    .line 96
    .line 97
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 98
    .line 99
    .line 100
    move-result v3

    .line 101
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    sub-int/2addr v3, v5

    .line 106
    iget v5, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 107
    .line 108
    sub-int/2addr v3, v5

    .line 109
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 110
    .line 111
    .line 112
    move-result v5

    .line 113
    sub-int v5, v3, v5

    .line 114
    .line 115
    goto :goto_2

    .line 116
    :cond_4
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    iget v5, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 121
    .line 122
    add-int/2addr v5, v3

    .line 123
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 124
    .line 125
    .line 126
    move-result v3

    .line 127
    add-int/2addr v3, v5

    .line 128
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    .line 129
    .line 130
    .line 131
    move-result v6

    .line 132
    if-ne v6, v4, :cond_5

    .line 133
    .line 134
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 135
    .line 136
    .line 137
    move-result v6

    .line 138
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 139
    .line 140
    sub-int/2addr v6, v14

    .line 141
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 142
    .line 143
    .line 144
    move-result v14

    .line 145
    iget v15, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 146
    .line 147
    sub-int/2addr v14, v15

    .line 148
    goto :goto_3

    .line 149
    :cond_5
    iget-boolean v6, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 150
    .line 151
    if-eqz v6, :cond_6

    .line 152
    .line 153
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 154
    .line 155
    .line 156
    move-result v6

    .line 157
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 158
    .line 159
    add-int/2addr v14, v6

    .line 160
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 161
    .line 162
    .line 163
    move-result v6

    .line 164
    iget v15, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 165
    .line 166
    add-int/2addr v6, v15

    .line 167
    goto :goto_3

    .line 168
    :cond_6
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 169
    .line 170
    .line 171
    move-result v6

    .line 172
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 173
    .line 174
    sub-int/2addr v6, v14

    .line 175
    iget v14, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 176
    .line 177
    sub-int/2addr v6, v14

    .line 178
    iget-object v14, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 179
    .line 180
    iget v14, v14, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 181
    .line 182
    sub-int/2addr v6, v14

    .line 183
    iget v14, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 184
    .line 185
    sub-int v14, v6, v14

    .line 186
    .line 187
    :goto_3
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 188
    .line 189
    .line 190
    move-result v15

    .line 191
    if-nez v15, :cond_b

    .line 192
    .line 193
    iget-boolean v15, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 194
    .line 195
    if-nez v15, :cond_7

    .line 196
    .line 197
    goto :goto_4

    .line 198
    :cond_7
    iget v15, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 199
    .line 200
    iget-object v12, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 201
    .line 202
    iget v12, v12, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 203
    .line 204
    add-int/2addr v15, v12

    .line 205
    if-ge v2, v15, :cond_8

    .line 206
    .line 207
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->Oo08()I

    .line 208
    .line 209
    .line 210
    move-result v12

    .line 211
    if-eq v12, v4, :cond_9

    .line 212
    .line 213
    :cond_8
    iget v4, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 214
    .line 215
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 216
    .line 217
    add-int/2addr v4, v12

    .line 218
    iget-object v12, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 219
    .line 220
    iget v12, v12, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 221
    .line 222
    add-int/2addr v4, v12

    .line 223
    if-ge v14, v4, :cond_a

    .line 224
    .line 225
    :cond_9
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 226
    .line 227
    iput-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 228
    .line 229
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 230
    .line 231
    .line 232
    move-result v0

    .line 233
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 234
    .line 235
    add-int/2addr v0, v1

    .line 236
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 237
    .line 238
    add-int/2addr v0, v1

    .line 239
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 240
    .line 241
    iget v1, v1, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o00〇〇Oo:I

    .line 242
    .line 243
    add-int/2addr v0, v1

    .line 244
    iget v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 245
    .line 246
    add-int/2addr v1, v0

    .line 247
    move v4, v3

    .line 248
    move v2, v5

    .line 249
    move v3, v0

    .line 250
    move v5, v1

    .line 251
    goto/16 :goto_8

    .line 252
    .line 253
    :cond_a
    sget-boolean v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager;->〇〇o〇:Z

    .line 254
    .line 255
    if-eqz v0, :cond_e

    .line 256
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    .line 258
    .line 259
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .line 261
    .line 262
    const-string v1, "remainingSpace: "

    .line 263
    .line 264
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    const-string v1, "    offset: "

    .line 271
    .line 272
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 276
    .line 277
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    goto :goto_5

    .line 281
    :cond_b
    :goto_4
    iget v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 282
    .line 283
    iget-object v12, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 284
    .line 285
    iget v12, v12, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 286
    .line 287
    add-int/2addr v4, v12

    .line 288
    if-ge v2, v4, :cond_c

    .line 289
    .line 290
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->Oo08()I

    .line 291
    .line 292
    .line 293
    move-result v2

    .line 294
    const/4 v4, 0x1

    .line 295
    if-eq v2, v4, :cond_d

    .line 296
    .line 297
    :cond_c
    iget v2, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 298
    .line 299
    iget v4, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 300
    .line 301
    add-int/2addr v2, v4

    .line 302
    iget-object v4, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 303
    .line 304
    iget v4, v4, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 305
    .line 306
    add-int/2addr v2, v4

    .line 307
    if-le v6, v2, :cond_e

    .line 308
    .line 309
    :cond_d
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 310
    .line 311
    iput-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 312
    .line 313
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 314
    .line 315
    .line 316
    move-result v0

    .line 317
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 318
    .line 319
    sub-int/2addr v0, v1

    .line 320
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 321
    .line 322
    sub-int/2addr v0, v1

    .line 323
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 324
    .line 325
    iget v1, v1, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->O8:I

    .line 326
    .line 327
    sub-int/2addr v0, v1

    .line 328
    iget v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 329
    .line 330
    sub-int v1, v0, v1

    .line 331
    .line 332
    move v4, v3

    .line 333
    move v2, v5

    .line 334
    move v5, v0

    .line 335
    move v3, v1

    .line 336
    goto/16 :goto_8

    .line 337
    .line 338
    :cond_e
    :goto_5
    move v4, v3

    .line 339
    move v2, v5

    .line 340
    move v5, v6

    .line 341
    move v3, v14

    .line 342
    goto/16 :goto_8

    .line 343
    .line 344
    :cond_f
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 345
    .line 346
    .line 347
    move-result v3

    .line 348
    invoke-virtual {v0, v11}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    .line 349
    .line 350
    .line 351
    move-result v5

    .line 352
    add-int/2addr v5, v3

    .line 353
    iget v6, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 354
    .line 355
    add-int/2addr v5, v6

    .line 356
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    .line 357
    .line 358
    .line 359
    move-result v6

    .line 360
    if-ne v6, v4, :cond_10

    .line 361
    .line 362
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 363
    .line 364
    .line 365
    move-result v4

    .line 366
    iget v6, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 367
    .line 368
    sub-int/2addr v4, v6

    .line 369
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 370
    .line 371
    .line 372
    move-result v6

    .line 373
    iget v12, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 374
    .line 375
    sub-int/2addr v6, v12

    .line 376
    goto :goto_6

    .line 377
    :cond_10
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 378
    .line 379
    .line 380
    move-result v4

    .line 381
    iget v6, v7, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 382
    .line 383
    add-int/2addr v6, v4

    .line 384
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    .line 385
    .line 386
    .line 387
    move-result v4

    .line 388
    iget v12, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 389
    .line 390
    add-int/2addr v4, v12

    .line 391
    :goto_6
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    .line 392
    .line 393
    .line 394
    move-result v12

    .line 395
    if-nez v12, :cond_12

    .line 396
    .line 397
    iget-boolean v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇O888o0o:Z

    .line 398
    .line 399
    if-nez v12, :cond_11

    .line 400
    .line 401
    goto :goto_7

    .line 402
    :cond_11
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 403
    .line 404
    iget-object v14, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 405
    .line 406
    iget v14, v14, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇080:I

    .line 407
    .line 408
    add-int/2addr v12, v14

    .line 409
    if-ge v2, v12, :cond_13

    .line 410
    .line 411
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 412
    .line 413
    iput-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 414
    .line 415
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇8o8o〇()I

    .line 416
    .line 417
    .line 418
    move-result v0

    .line 419
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 420
    .line 421
    add-int/2addr v0, v1

    .line 422
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 423
    .line 424
    iget v1, v1, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇080:I

    .line 425
    .line 426
    add-int/2addr v0, v1

    .line 427
    iget v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 428
    .line 429
    move v2, v0

    .line 430
    move v4, v1

    .line 431
    goto :goto_8

    .line 432
    :cond_12
    :goto_7
    iget v12, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 433
    .line 434
    iget-object v14, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 435
    .line 436
    iget v14, v14, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o〇:I

    .line 437
    .line 438
    add-int/2addr v12, v14

    .line 439
    if-ge v2, v12, :cond_13

    .line 440
    .line 441
    iput-boolean v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 442
    .line 443
    iput-object v11, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 444
    .line 445
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->〇80〇808〇O()I

    .line 446
    .line 447
    .line 448
    move-result v0

    .line 449
    iget v1, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->oo88o8O:I

    .line 450
    .line 451
    sub-int/2addr v0, v1

    .line 452
    iget-object v1, v7, Lcom/alibaba/android/vlayout/layout/FixAreaLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;

    .line 453
    .line 454
    iget v1, v1, Lcom/alibaba/android/vlayout/layout/FixAreaAdjuster;->〇o〇:I

    .line 455
    .line 456
    sub-int/2addr v0, v1

    .line 457
    iget v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 458
    .line 459
    sub-int v1, v0, v1

    .line 460
    .line 461
    move v4, v0

    .line 462
    move v2, v1

    .line 463
    goto :goto_8

    .line 464
    :cond_13
    move v2, v6

    .line 465
    :goto_8
    move-object/from16 v0, p0

    .line 466
    .line 467
    move-object v1, v11

    .line 468
    move-object/from16 v6, p5

    .line 469
    .line 470
    invoke-virtual/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->O8〇o(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 471
    .line 472
    .line 473
    iget v0, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 474
    .line 475
    if-eqz v13, :cond_14

    .line 476
    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇00()I

    .line 478
    .line 479
    .line 480
    move-result v1

    .line 481
    goto :goto_9

    .line 482
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OoO8()I

    .line 483
    .line 484
    .line 485
    move-result v1

    .line 486
    :goto_9
    add-int/2addr v0, v1

    .line 487
    iput v0, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 488
    .line 489
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 490
    .line 491
    .line 492
    move-result v0

    .line 493
    if-eqz v0, :cond_15

    .line 494
    .line 495
    const/4 v0, 0x1

    .line 496
    iput-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 497
    .line 498
    :cond_15
    iget-boolean v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 499
    .line 500
    if-eqz v0, :cond_16

    .line 501
    .line 502
    invoke-interface {v10, v8, v11}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->oo88o8O(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;)V

    .line 503
    .line 504
    .line 505
    invoke-virtual {v7, v9, v11}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->OOO〇O0(Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Landroid/view/View;)V

    .line 506
    .line 507
    .line 508
    const/4 v0, 0x0

    .line 509
    iput-object v0, v7, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 510
    .line 511
    :cond_16
    return-void
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
.end method

.method public 〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-interface {p3, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇o00〇〇Oo(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 15
    .line 16
    invoke-interface {p3, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 20
    .line 21
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->recycleView(Landroid/view/View;)V

    .line 22
    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->〇oo〇:Landroid/view/View;

    .line 26
    .line 27
    :cond_0
    const/4 p1, 0x0

    .line 28
    iput-boolean p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o〇O8〇〇o:Z

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇〇808〇(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/StickyLayoutHelper;->o800o8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public 〇〇8O0〇8(I)V
    .locals 0

    .line 1
    if-lez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8(I)V

    .line 5
    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 p1, 0x0

    .line 9
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8(I)V

    .line 10
    .line 11
    .line 12
    :goto_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.class public abstract Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;
.super Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;
.source "BaseLayoutHelper.java"


# static fields
.field public static 〇0〇O0088o:Z = false


# instance fields
.field protected Oooo8o0〇:Landroid/graphics/Rect;

.field 〇O00:F

.field 〇O〇:I

.field 〇〇808〇:Landroid/view/View;

.field private 〇〇8O0〇8:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/graphics/Rect;

    .line 5
    .line 6
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 10
    .line 11
    const/high16 v0, 0x7fc00000    # Float.NaN

    .line 12
    .line 13
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O00:F

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8:I

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private 〇oOO8O8(II)I
    .locals 0

    .line 1
    if-ge p1, p2, :cond_0

    .line 2
    .line 3
    sub-int/2addr p2, p1

    .line 4
    return p2

    .line 5
    :cond_0
    const/4 p1, 0x0

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method


# virtual methods
.method public final O8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 10
    .line 11
    :cond_0
    invoke-virtual {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public O8ooOoo〇(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/high16 v1, 0x40000000    # 2.0f

    .line 8
    .line 9
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 27
    .line 28
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 29
    .line 30
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 33
    .line 34
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 35
    .line 36
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 37
    .line 38
    .line 39
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O〇:I

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method protected O8〇o(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 8
    .param p6    # Lcom/alibaba/android/vlayout/LayoutManagerHelper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const/4 v7, 0x0

    .line 2
    move-object v0, p0

    .line 3
    move-object v1, p1

    .line 4
    move v2, p2

    .line 5
    move v3, p3

    .line 6
    move v4, p4

    .line 7
    move v5, p5

    .line 8
    move-object v6, p6

    .line 9
    invoke-virtual/range {v0 .. v7}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇00〇8(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method protected OOO〇O0(Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Landroid/view/View;)V
    .locals 3

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/4 v2, 0x1

    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemChanged()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    :cond_1
    iput-boolean v2, p1, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇o〇:Z

    .line 24
    .line 25
    :cond_2
    iget-boolean v0, p1, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    .line 26
    .line 27
    if-nez v0, :cond_4

    .line 28
    .line 29
    invoke-virtual {p2}, Landroid/view/View;->isFocusable()Z

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    if-eqz p2, :cond_3

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_3
    const/4 v2, 0x0

    .line 37
    :cond_4
    :goto_0
    iput-boolean v2, p1, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
.end method

.method public final o0ooO(Landroidx/recyclerview/widget/RecyclerView$Recycler;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;)Landroid/view/View;
    .locals 0
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p2, p1}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇8o8o〇(Landroidx/recyclerview/widget/RecyclerView$Recycler;)Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-nez p1, :cond_2

    .line 6
    .line 7
    sget-boolean p1, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇0〇O0088o:Z

    .line 8
    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇80〇808〇O()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 19
    .line 20
    const-string p2, "received null view when unexpected"

    .line 21
    .line 22
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw p1

    .line 26
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p4, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇o00〇〇Oo:Z

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    return-object p1

    .line 31
    :cond_2
    invoke-interface {p3, p2, p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->oo88o8O(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;)V

    .line 32
    .line 33
    .line 34
    return-object p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method public o8()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇O〇:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x1

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected oo〇(I)Z
    .locals 1

    .line 1
    const v0, 0x7fffffff

    .line 2
    .line 3
    .line 4
    if-eq p1, v0, :cond_0

    .line 5
    .line 6
    const/high16 v0, -0x80000000

    .line 7
    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    const/4 p1, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇0(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-virtual/range {p0 .. p5}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method protected o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method protected o〇〇0〇(Lcom/alibaba/android/vlayout/LayoutManagerHelper;ZZZ)I
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/alibaba/android/vlayout/VirtualLayoutManager;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    check-cast p1, Lcom/alibaba/android/vlayout/VirtualLayoutManager;

    .line 7
    .line 8
    invoke-virtual {p1, p0, p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager;->〇oOO8O8(Lcom/alibaba/android/vlayout/LayoutHelper;Z)Lcom/alibaba/android/vlayout/LayoutHelper;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v1

    .line 14
    :goto_0
    if-eqz p1, :cond_1

    .line 15
    .line 16
    instance-of v0, p1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    move-object v1, p1

    .line 21
    check-cast v1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;

    .line 22
    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    if-ne p1, p0, :cond_2

    .line 25
    .line 26
    return v0

    .line 27
    :cond_2
    if-nez p4, :cond_4

    .line 28
    .line 29
    if-eqz p2, :cond_3

    .line 30
    .line 31
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 32
    .line 33
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_3
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 37
    .line 38
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 39
    .line 40
    :goto_1
    add-int/2addr p1, p2

    .line 41
    goto :goto_7

    .line 42
    :cond_4
    if-nez v1, :cond_6

    .line 43
    .line 44
    if-eqz p2, :cond_5

    .line 45
    .line 46
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 47
    .line 48
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_5
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 52
    .line 53
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 54
    .line 55
    :goto_2
    add-int/2addr p1, p4

    .line 56
    goto :goto_5

    .line 57
    :cond_6
    if-eqz p2, :cond_8

    .line 58
    .line 59
    if-eqz p3, :cond_7

    .line 60
    .line 61
    iget p1, v1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 62
    .line 63
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_7
    iget p1, v1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 67
    .line 68
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 69
    .line 70
    :goto_3
    invoke-direct {p0, p1, p4}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇oOO8O8(II)I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    goto :goto_5

    .line 75
    :cond_8
    if-eqz p3, :cond_9

    .line 76
    .line 77
    iget p1, v1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 78
    .line 79
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 80
    .line 81
    goto :goto_4

    .line 82
    :cond_9
    iget p1, v1, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 83
    .line 84
    iget p4, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 85
    .line 86
    :goto_4
    invoke-direct {p0, p1, p4}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇oOO8O8(II)I

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    :goto_5
    if-eqz p2, :cond_b

    .line 91
    .line 92
    if-eqz p3, :cond_a

    .line 93
    .line 94
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 95
    .line 96
    goto :goto_6

    .line 97
    :cond_a
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇80〇808〇O:I

    .line 98
    .line 99
    goto :goto_6

    .line 100
    :cond_b
    if-eqz p3, :cond_c

    .line 101
    .line 102
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 103
    .line 104
    goto :goto_6

    .line 105
    :cond_c
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇〇888:I

    .line 106
    .line 107
    :goto_6
    add-int/2addr p2, v0

    .line 108
    add-int/2addr p1, p2

    .line 109
    :goto_7
    return p1
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method protected 〇0000OOO(Lcom/alibaba/android/vlayout/LayoutManagerHelper;ZZZ)I
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 4
    .line 5
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇80〇808〇O:I

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget p1, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 9
    .line 10
    iget p2, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 11
    .line 12
    :goto_0
    add-int/2addr p1, p2

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method protected 〇00〇8(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;Z)V
    .locals 6
    .param p6    # Lcom/alibaba/android/vlayout/LayoutManagerHelper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object v0, p6

    .line 2
    move-object v1, p1

    .line 3
    move v2, p2

    .line 4
    move v3, p3

    .line 5
    move v4, p4

    .line 6
    move v5, p5

    .line 7
    invoke-interface/range {v0 .. v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->OO0o〇〇(Landroid/view/View;IIII)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o8()Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    if-eqz p7, :cond_0

    .line 17
    .line 18
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 19
    .line 20
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 21
    .line 22
    sub-int/2addr p2, p6

    .line 23
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 24
    .line 25
    sub-int/2addr p2, p6

    .line 26
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 27
    .line 28
    sub-int/2addr p3, p6

    .line 29
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 30
    .line 31
    sub-int/2addr p3, p6

    .line 32
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇〇888:I

    .line 33
    .line 34
    add-int/2addr p4, p6

    .line 35
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 36
    .line 37
    add-int/2addr p4, p6

    .line 38
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇80〇808〇O:I

    .line 39
    .line 40
    add-int/2addr p5, p6

    .line 41
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 42
    .line 43
    add-int/2addr p5, p6

    .line 44
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Rect;->union(IIII)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 49
    .line 50
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->o〇0:I

    .line 51
    .line 52
    sub-int/2addr p2, p6

    .line 53
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->oO80:I

    .line 54
    .line 55
    sub-int/2addr p3, p6

    .line 56
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇〇888:I

    .line 57
    .line 58
    add-int/2addr p4, p6

    .line 59
    iget p6, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇80〇808〇O:I

    .line 60
    .line 61
    add-int/2addr p5, p6

    .line 62
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Rect;->union(IIII)V

    .line 63
    .line 64
    .line 65
    :cond_1
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
.end method

.method public 〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 2

    .line 1
    sget-boolean p1, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇0〇O0088o:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string p2, "call afterLayout() on "

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o8()Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-eqz p1, :cond_8

    .line 31
    .line 32
    invoke-virtual {p0, p5}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->oo〇(I)Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 39
    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 43
    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    iget-object p3, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 49
    .line 50
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    .line 51
    .line 52
    .line 53
    move-result p3

    .line 54
    iget-object p4, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 55
    .line 56
    invoke-virtual {p4}, Landroid/view/View;->getRight()I

    .line 57
    .line 58
    .line 59
    move-result p4

    .line 60
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 61
    .line 62
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    invoke-virtual {p2, p1, p3, p4, v0}, Landroid/graphics/Rect;->union(IIII)V

    .line 67
    .line 68
    .line 69
    :cond_1
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    if-nez p1, :cond_8

    .line 76
    .line 77
    invoke-virtual {p0, p5}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->oo〇(I)Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    const/4 p2, 0x1

    .line 82
    const/4 p3, 0x0

    .line 83
    if-eqz p1, :cond_3

    .line 84
    .line 85
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    if-ne p1, p2, :cond_2

    .line 90
    .line 91
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 92
    .line 93
    neg-int p4, p5

    .line 94
    invoke-virtual {p1, p3, p4}, Landroid/graphics/Rect;->offset(II)V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_2
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 99
    .line 100
    neg-int p4, p5

    .line 101
    invoke-virtual {p1, p4, p3}, Landroid/graphics/Rect;->offset(II)V

    .line 102
    .line 103
    .line 104
    :cond_3
    :goto_0
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇808〇()I

    .line 109
    .line 110
    .line 111
    move-result p4

    .line 112
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 113
    .line 114
    .line 115
    move-result p5

    .line 116
    if-ne p5, p2, :cond_4

    .line 117
    .line 118
    iget-object p5, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 119
    .line 120
    neg-int v0, p4

    .line 121
    div-int/lit8 v0, v0, 0x4

    .line 122
    .line 123
    div-int/lit8 v1, p4, 0x4

    .line 124
    .line 125
    add-int/2addr p4, v1

    .line 126
    invoke-virtual {p5, p3, v0, p1, p4}, Landroid/graphics/Rect;->intersects(IIII)Z

    .line 127
    .line 128
    .line 129
    move-result p1

    .line 130
    if-eqz p1, :cond_7

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_4
    iget-object p5, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 134
    .line 135
    neg-int v0, p1

    .line 136
    div-int/lit8 v0, v0, 0x4

    .line 137
    .line 138
    div-int/lit8 v1, p1, 0x4

    .line 139
    .line 140
    add-int/2addr p1, v1

    .line 141
    invoke-virtual {p5, v0, p3, p1, p4}, Landroid/graphics/Rect;->intersects(IIII)Z

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    if-eqz p1, :cond_7

    .line 146
    .line 147
    :goto_1
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 148
    .line 149
    if-nez p1, :cond_5

    .line 150
    .line 151
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->o〇O8〇〇o()Landroid/view/View;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 156
    .line 157
    invoke-interface {p6, p1, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->OoO8(Landroid/view/View;Z)V

    .line 158
    .line 159
    .line 160
    :cond_5
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 161
    .line 162
    .line 163
    move-result p1

    .line 164
    if-ne p1, p2, :cond_6

    .line 165
    .line 166
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 167
    .line 168
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 169
    .line 170
    .line 171
    move-result p2

    .line 172
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇〇〇0:I

    .line 173
    .line 174
    add-int/2addr p2, p3

    .line 175
    iput p2, p1, Landroid/graphics/Rect;->left:I

    .line 176
    .line 177
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 178
    .line 179
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 180
    .line 181
    .line 182
    move-result p2

    .line 183
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 184
    .line 185
    .line 186
    move-result p3

    .line 187
    sub-int/2addr p2, p3

    .line 188
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇8o8o〇:I

    .line 189
    .line 190
    sub-int/2addr p2, p3

    .line 191
    iput p2, p1, Landroid/graphics/Rect;->right:I

    .line 192
    .line 193
    goto :goto_2

    .line 194
    :cond_6
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 195
    .line 196
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 197
    .line 198
    .line 199
    move-result p2

    .line 200
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->〇O8o08O:I

    .line 201
    .line 202
    add-int/2addr p2, p3

    .line 203
    iput p2, p1, Landroid/graphics/Rect;->top:I

    .line 204
    .line 205
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 206
    .line 207
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 208
    .line 209
    .line 210
    move-result p2

    .line 211
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingBottom()I

    .line 212
    .line 213
    .line 214
    move-result p3

    .line 215
    sub-int/2addr p2, p3

    .line 216
    iget p3, p0, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->OO0o〇〇:I

    .line 217
    .line 218
    sub-int/2addr p2, p3

    .line 219
    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    .line 220
    .line 221
    :goto_2
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 222
    .line 223
    invoke-virtual {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->O8ooOoo〇(Landroid/view/View;)V

    .line 224
    .line 225
    .line 226
    return-void

    .line 227
    :cond_7
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 228
    .line 229
    invoke-virtual {p1, p3, p3, p3, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 230
    .line 231
    .line 232
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 233
    .line 234
    if-eqz p1, :cond_8

    .line 235
    .line 236
    invoke-virtual {p1, p3, p3, p3, p3}, Landroid/view/View;->layout(IIII)V

    .line 237
    .line 238
    .line 239
    :cond_8
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 240
    .line 241
    if-eqz p1, :cond_9

    .line 242
    .line 243
    invoke-interface {p6, p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 244
    .line 245
    .line 246
    const/4 p1, 0x0

    .line 247
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 248
    .line 249
    :cond_9
    return-void
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method public 〇80〇808〇O()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract 〇o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
.end method

.method public 〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    sget-boolean p1, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇0〇O0088o:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string p2, "call beforeLayout() on "

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o8()Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    invoke-interface {p3, p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 38
    .line 39
    .line 40
    const/4 p1, 0x0

    .line 41
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇808〇:Landroid/view/View;

    .line 42
    .line 43
    :cond_2
    :goto_0
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇8O0〇8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇8O0〇8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

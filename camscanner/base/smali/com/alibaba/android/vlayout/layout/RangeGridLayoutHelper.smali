.class public Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;
.super Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;
.source "RangeGridLayoutHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;
    }
.end annotation


# static fields
.field private static oo88o8O:Z = false

.field private static final 〇oo〇:I


# instance fields
.field private OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

.field private o800o8O:I

.field private 〇O888o0o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    sput v0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇oo〇:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo8Oo00oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIZLcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, -0x1

    .line 3
    const/4 v2, 0x1

    .line 4
    if-eqz p6, :cond_0

    .line 5
    .line 6
    move p6, p4

    .line 7
    const/4 p4, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    add-int/lit8 p4, p4, -0x1

    .line 11
    .line 12
    const/4 p6, -0x1

    .line 13
    const/4 v3, -0x1

    .line 14
    :goto_0
    invoke-interface {p7}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    if-ne v4, v2, :cond_1

    .line 19
    .line 20
    invoke-interface {p7}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇8O0〇8()Z

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    if-eqz v4, :cond_1

    .line 25
    .line 26
    add-int/lit8 v0, p5, -0x1

    .line 27
    .line 28
    const/4 p5, -0x1

    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 p5, 0x1

    .line 31
    :goto_1
    if-eq p4, p6, :cond_3

    .line 32
    .line 33
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    aget-object v4, v4, p4

    .line 38
    .line 39
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    invoke-interface {p7, v4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    invoke-direct {p0, v5, p2, p3, v4}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-ne p5, v1, :cond_2

    .line 52
    .line 53
    if-le v4, v2, :cond_2

    .line 54
    .line 55
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    add-int/lit8 v6, v4, -0x1

    .line 60
    .line 61
    sub-int v6, v0, v6

    .line 62
    .line 63
    aput v6, v5, p4

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_2
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    .line 67
    .line 68
    .line 69
    move-result-object v5

    .line 70
    aput v0, v5, p4

    .line 71
    .line 72
    :goto_2
    mul-int v4, v4, p5

    .line 73
    .line 74
    add-int/2addr v0, v4

    .line 75
    add-int/2addr p4, v3

    .line 76
    goto :goto_1

    .line 77
    :cond_3
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
.end method

.method private oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I
    .locals 0

    .line 1
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    if-nez p3, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1, p4}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇(I)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1

    .line 12
    :cond_0
    invoke-virtual {p2, p4}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    const/4 p3, -0x1

    .line 17
    if-ne p2, p3, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return p1

    .line 21
    :cond_1
    invoke-virtual {p1, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇(I)I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method private 〇08O8o〇0(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;ILandroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I
    .locals 0

    .line 1
    invoke-virtual {p4}, Landroidx/recyclerview/widget/RecyclerView$State;->isPreLayout()Z

    .line 2
    .line 3
    .line 4
    move-result p4

    .line 5
    if-nez p4, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1, p5, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1

    .line 12
    :cond_0
    invoke-virtual {p3, p5}, Landroidx/recyclerview/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    .line 13
    .line 14
    .line 15
    move-result p3

    .line 16
    const/4 p4, -0x1

    .line 17
    if-ne p3, p4, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return p1

    .line 21
    :cond_1
    invoke-virtual {p1, p3, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method private 〇〇0o(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;IIIF)I
    .locals 4

    .line 1
    invoke-static {p5}, Ljava/lang/Float;->isNaN(F)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/high16 v1, 0x3f000000    # 0.5f

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    const/high16 v3, 0x40000000    # 2.0f

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    cmpl-float v0, p5, v2

    .line 13
    .line 14
    if-lez v0, :cond_0

    .line 15
    .line 16
    if-lez p4, :cond_0

    .line 17
    .line 18
    int-to-float p1, p4

    .line 19
    div-float/2addr p1, p5

    .line 20
    add-float/2addr p1, v1

    .line 21
    float-to-int p1, p1

    .line 22
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1

    .line 27
    :cond_0
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Ooo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)F

    .line 28
    .line 29
    .line 30
    move-result p4

    .line 31
    invoke-static {p4}, Ljava/lang/Float;->isNaN(F)Z

    .line 32
    .line 33
    .line 34
    move-result p4

    .line 35
    if-nez p4, :cond_1

    .line 36
    .line 37
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Ooo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)F

    .line 38
    .line 39
    .line 40
    move-result p4

    .line 41
    cmpl-float p4, p4, v2

    .line 42
    .line 43
    if-lez p4, :cond_1

    .line 44
    .line 45
    int-to-float p2, p3

    .line 46
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Ooo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)F

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    div-float/2addr p2, p1

    .line 51
    add-float/2addr p2, v1

    .line 52
    float-to-int p1, p2

    .line 53
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    return p1

    .line 58
    :cond_1
    if-gez p2, :cond_2

    .line 59
    .line 60
    sget p1, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇oo〇:I

    .line 61
    .line 62
    return p1

    .line 63
    :cond_2
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    return p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method


# virtual methods
.method public Oo08(IZZLcom/alibaba/android/vlayout/LayoutManagerHelper;)I
    .locals 3

    .line 1
    invoke-interface {p4}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-eqz p2, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->〇〇888()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    sub-int/2addr v2, v1

    .line 18
    if-ne p1, v2, :cond_2

    .line 19
    .line 20
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 21
    .line 22
    invoke-static {p1, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O8O〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;Z)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1

    .line 27
    :cond_1
    if-nez p1, :cond_2

    .line 28
    .line 29
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0O8OO088(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;Z)I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    return p1

    .line 36
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Lcom/alibaba/android/vlayout/layout/MarginLayoutHelper;->Oo08(IZZLcom/alibaba/android/vlayout/LayoutManagerHelper;)I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method public o8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇O〇oO()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0OOo〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Ljava/lang/Integer;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    const/4 v1, 0x1

    .line 26
    if-ne p1, v1, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0〇O0088o()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    :goto_0
    add-int/2addr p1, v0

    .line 37
    return p1

    .line 38
    :cond_0
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O00()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    goto :goto_0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o8O〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 2
    .line 3
    move-object v1, p1

    .line 4
    move-object v2, p2

    .line 5
    move v3, p3

    .line 6
    move v4, p4

    .line 7
    move v5, p5

    .line 8
    move-object v6, p6

    .line 9
    invoke-virtual/range {v0 .. v6}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method public 〇O8o08O(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/alibaba/android/vlayout/LayoutHelper;->〇O8o08O(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o8O〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 32

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    .line 1
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v14

    .line 3
    iget-object v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v0, v14}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    move-result-object v15

    .line 4
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->Oo08()I

    move-result v6

    const/4 v7, 0x1

    const/4 v5, 0x0

    if-ne v6, v7, :cond_1

    const/16 v16, 0x1

    goto :goto_0

    :cond_1
    const/16 v16, 0x0

    .line 5
    :goto_0
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->Oooo8o0〇()Lcom/alibaba/android/vlayout/OrientationHelperEx;

    move-result-object v4

    .line 6
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v0

    if-ne v0, v7, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    const/high16 v17, 0x3f000000    # 0.5f

    const/high16 v18, 0x3f800000    # 1.0f

    if-eqz v3, :cond_3

    .line 7
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    move-result v0

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    .line 8
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    .line 9
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    sub-int/2addr v1, v7

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float v0, v0, v18

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float v0, v0, v17

    float-to-int v0, v0

    .line 10
    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    goto :goto_2

    .line 11
    :cond_3
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇808〇()I

    move-result v0

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 12
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OoO8()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o800o8O()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    .line 13
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    sub-int/2addr v1, v7

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float v0, v0, v18

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float v0, v0, v17

    float-to-int v0, v0

    .line 14
    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    .line 15
    :goto_2
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    .line 16
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->ooo〇8oO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)V

    const-string v2, " spans."

    const-string v1, " requires "

    const-string v7, "Item at position "

    if-nez v16, :cond_11

    .line 17
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v20

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v21

    move-object/from16 v0, p0

    move-object v12, v1

    move-object/from16 v1, v20

    move-object v13, v2

    move/from16 v2, v21

    move/from16 v22, v3

    move-object/from16 v3, p1

    move-object/from16 v23, v4

    move-object/from16 v4, p2

    move v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇08O8o〇0(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;ILandroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v0

    .line 18
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v1

    invoke-direct {v8, v1, v9, v10, v14}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v1

    add-int/2addr v1, v0

    .line 19
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    const/16 v19, 0x1

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_10

    .line 20
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v0

    .line 21
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    sub-int/2addr v2, v1

    move/from16 v21, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v20, 0x0

    .line 22
    :goto_3
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    if-ge v5, v1, :cond_e

    if-lez v2, :cond_e

    sub-int/2addr v0, v6

    .line 23
    invoke-virtual {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0OOo〇0(I)Z

    move-result v1

    if-eqz v1, :cond_4

    goto/16 :goto_9

    .line 24
    :cond_4
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v1

    invoke-direct {v8, v1, v9, v10, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v1

    move/from16 v25, v6

    .line 25
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v6

    if-gt v1, v6, :cond_d

    .line 26
    invoke-virtual {v11, v9, v0}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇O8o08O(Landroidx/recyclerview/widget/RecyclerView$Recycler;I)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_5

    goto/16 :goto_9

    :cond_5
    if-nez v3, :cond_8

    .line 27
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v3

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_7

    goto :goto_4

    :cond_6
    iget-object v3, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 28
    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v3

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_7

    :goto_4
    const/4 v3, 0x1

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    :cond_8
    :goto_5
    if-nez v20, :cond_b

    .line 29
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v20

    move/from16 v26, v3

    iget-object v3, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    if-eqz v20, :cond_9

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v3

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_a

    goto :goto_6

    .line 30
    :cond_9
    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v3

    invoke-virtual {v3}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_a

    :goto_6
    const/4 v3, 0x1

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    :goto_7
    move/from16 v20, v3

    goto :goto_8

    :cond_b
    move/from16 v26, v3

    :goto_8
    sub-int/2addr v2, v1

    if-gez v2, :cond_c

    goto :goto_a

    :cond_c
    add-int/2addr v4, v1

    .line 31
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v1

    aput-object v6, v1, v5

    add-int/lit8 v5, v5, 0x1

    move/from16 v6, v25

    move/from16 v3, v26

    goto/16 :goto_3

    .line 32
    :cond_d
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " spans but RangeGridLayoutHelper has only "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_e
    :goto_9
    move/from16 v26, v3

    :goto_a
    if-lez v5, :cond_f

    add-int/lit8 v0, v5, -0x1

    const/4 v1, 0x0

    :goto_b
    if-ge v1, v0, :cond_f

    .line 34
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v2

    aget-object v2, v2, v1

    .line 35
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v3

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v6

    aget-object v6, v6, v0

    aput-object v6, v3, v1

    .line 36
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v3

    aput-object v2, v3, v0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_b

    :cond_f
    move v6, v5

    move/from16 v0, v21

    move/from16 v5, v26

    const/4 v1, 0x0

    const/4 v2, 0x0

    goto :goto_d

    :cond_10
    move/from16 v21, v1

    move/from16 v0, v21

    goto :goto_c

    :cond_11
    move-object v12, v1

    move-object v13, v2

    move/from16 v22, v3

    move-object/from16 v23, v4

    const/16 v19, 0x1

    :goto_c
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v20, 0x0

    .line 37
    :goto_d
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v3

    move/from16 v21, v14

    const-string v14, " pos="

    move-object/from16 v25, v13

    const-string v13, "isSecondEndLineLogic:"

    if-ge v6, v3, :cond_22

    invoke-virtual {v11, v10}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->oO80(Landroidx/recyclerview/widget/RecyclerView$State;)Z

    move-result v3

    if-eqz v3, :cond_22

    if-lez v0, :cond_22

    .line 38
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇o〇()I

    move-result v3

    .line 39
    invoke-virtual {v15, v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0OOo〇0(I)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 40
    sget-boolean v7, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v7, :cond_22

    .line 41
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "pos ["

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] is out of range"

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_17

    :cond_12
    move-object/from16 v26, v12

    .line 42
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v12

    invoke-direct {v8, v12, v9, v10, v3}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v12

    .line 43
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v10

    if-gt v12, v10, :cond_21

    sub-int/2addr v0, v12

    if-gez v0, :cond_13

    goto/16 :goto_17

    :cond_13
    if-nez v5, :cond_16

    .line 44
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v5

    if-eqz v5, :cond_14

    iget-object v5, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v5

    invoke-virtual {v5}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v3, v5, :cond_15

    goto :goto_e

    :cond_14
    iget-object v5, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 45
    invoke-virtual {v5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v5

    invoke-virtual {v5}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v3, v5, :cond_15

    :goto_e
    const/4 v5, 0x1

    goto :goto_f

    :cond_15
    const/4 v5, 0x0

    :cond_16
    :goto_f
    if-nez v1, :cond_19

    .line 46
    iget-object v10, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v15, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_19

    .line 47
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v1

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 48
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v3, v1, :cond_18

    goto :goto_10

    :cond_17
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v1

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v3, v1, :cond_18

    :goto_10
    const/4 v1, 0x1

    goto :goto_11

    :cond_18
    const/4 v1, 0x0

    :cond_19
    :goto_11
    if-nez v20, :cond_1c

    .line 49
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v10

    if-eqz v10, :cond_1a

    iget-object v10, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v10}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v10

    invoke-virtual {v10}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ne v3, v10, :cond_1b

    goto :goto_12

    :cond_1a
    iget-object v10, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 50
    invoke-virtual {v10}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v10

    invoke-virtual {v10}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ne v3, v10, :cond_1b

    :goto_12
    const/4 v10, 0x1

    goto :goto_13

    :cond_1b
    const/4 v10, 0x0

    :goto_13
    move/from16 v20, v10

    :cond_1c
    if-nez v2, :cond_1f

    .line 51
    iget-object v10, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v15, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1f

    .line 52
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v2

    invoke-virtual {v2}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v3, v2, :cond_1e

    goto :goto_14

    :cond_1d
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v2

    invoke-virtual {v2}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v3, v2, :cond_1e

    :goto_14
    const/4 v2, 0x1

    goto :goto_15

    :cond_1e
    const/4 v2, 0x0

    .line 54
    :goto_15
    sget-boolean v10, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v10, :cond_1f

    .line 55
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move/from16 v27, v0

    const-string v0, "  helper.getReverseLayout()="

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getReverseLayout()Z

    move-result v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " rangeStyle.getRange().getLower()="

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v0

    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " rangeStyle.getRange().getUpper()="

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8〇o()Lcom/alibaba/android/vlayout/Range;

    move-result-object v0

    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_16

    :cond_1f
    move/from16 v27, v0

    .line 56
    :goto_16
    invoke-virtual {v11, v9}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇8o8o〇(Landroidx/recyclerview/widget/RecyclerView$Recycler;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_20

    goto :goto_18

    :cond_20
    add-int/2addr v4, v12

    .line 57
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v3

    aput-object v0, v3, v6

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v10, p2

    move/from16 v14, v21

    move-object/from16 v13, v25

    move-object/from16 v12, v26

    move/from16 v0, v27

    goto/16 :goto_d

    .line 58
    :cond_21
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object/from16 v2, v26

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " spans but GridLayoutManager has only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object/from16 v2, v25

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_22
    :goto_17
    move/from16 v27, v0

    :goto_18
    move v10, v1

    move v12, v2

    move/from16 v25, v20

    move/from16 v20, v5

    if-nez v6, :cond_23

    return-void

    :cond_23
    move-object/from16 v0, p0

    move-object v1, v15

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move v7, v4

    move v4, v6

    move v5, v7

    move-object/from16 v26, v14

    move v14, v6

    move/from16 v6, v16

    move-object/from16 v28, v13

    move v13, v7

    move-object/from16 v7, p5

    .line 60
    invoke-direct/range {v0 .. v7}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->Oo8Oo00oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIZLcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    if-lez v27, :cond_25

    if-ne v14, v13, :cond_25

    .line 61
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o0O0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Z

    move-result v0

    if-eqz v0, :cond_25

    move/from16 v7, v22

    if-eqz v7, :cond_24

    .line 62
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v6, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    mul-int v6, v6, v1

    sub-int/2addr v0, v6

    div-int/2addr v0, v14

    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    goto :goto_19

    .line 63
    :cond_24
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v6, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    mul-int v6, v6, v1

    sub-int/2addr v0, v6

    div-int/2addr v0, v14

    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    goto :goto_19

    :cond_25
    move/from16 v7, v22

    if-nez v16, :cond_27

    if-nez v27, :cond_27

    if-ne v14, v13, :cond_27

    .line 64
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o0O0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Z

    move-result v0

    if-eqz v0, :cond_27

    if-eqz v7, :cond_26

    .line 65
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v6, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    mul-int v6, v6, v1

    sub-int/2addr v0, v6

    div-int/2addr v0, v14

    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    goto :goto_19

    .line 66
    :cond_26
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v6, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    mul-int v6, v6, v1

    sub-int/2addr v0, v6

    div-int/2addr v0, v14

    invoke-static {v15, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->oO00OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;I)I

    .line 67
    :cond_27
    :goto_19
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v0

    if-eqz v0, :cond_2e

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2e

    if-eqz v7, :cond_28

    .line 68
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v1, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    goto :goto_1a

    .line 69
    :cond_28
    iget v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    add-int/lit8 v1, v14, -0x1

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    :goto_1a
    mul-int v1, v1, v2

    sub-int/2addr v0, v1

    if-lez v27, :cond_29

    .line 70
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o0O0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Z

    move-result v1

    if-eqz v1, :cond_29

    move v1, v14

    goto :goto_1b

    :cond_29
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    :goto_1b
    move v3, v0

    const/4 v2, 0x0

    const/4 v5, 0x0

    :goto_1c
    if-ge v5, v1, :cond_2b

    .line 71
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v4

    array-length v4, v4

    if-ge v5, v4, :cond_2a

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v4

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_2a

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v4

    aget v4, v4, v5

    const/4 v13, 0x0

    cmpl-float v4, v4, v13

    if-ltz v4, :cond_2a

    .line 72
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇0(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[F

    move-result-object v4

    aget v4, v4, v5

    .line 73
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v13

    mul-float v4, v4, v18

    const/high16 v22, 0x42c80000    # 100.0f

    div-float v4, v4, v22

    int-to-float v6, v0

    mul-float v4, v4, v6

    add-float v4, v4, v17

    float-to-int v4, v4

    aput v4, v13, v5

    .line 74
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v4

    aget v4, v4, v5

    sub-int/2addr v3, v4

    goto :goto_1d

    :cond_2a
    add-int/lit8 v2, v2, 0x1

    .line 75
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v4

    const/4 v6, -0x1

    aput v6, v4, v5

    :goto_1d
    add-int/lit8 v5, v5, 0x1

    goto :goto_1c

    :cond_2b
    if-lez v2, :cond_2d

    .line 76
    div-int/2addr v3, v2

    const/4 v5, 0x0

    :goto_1e
    if-ge v5, v1, :cond_2d

    .line 77
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v0

    aget v0, v0, v5

    if-gez v0, :cond_2c

    .line 78
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v0

    aput v3, v0, v5

    :cond_2c
    add-int/lit8 v5, v5, 0x1

    goto :goto_1e

    :cond_2d
    const/4 v13, 0x1

    goto :goto_1f

    :cond_2e
    const/4 v13, 0x0

    :goto_1f
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_20
    if-ge v6, v14, :cond_35

    .line 79
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v0

    aget-object v3, v0, v6

    move-object/from16 v2, p5

    if-eqz v16, :cond_2f

    const/4 v0, -0x1

    goto :goto_21

    :cond_2f
    const/4 v0, 0x0

    .line 80
    :goto_21
    invoke-interface {v2, v11, v3, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇0〇O0088o(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;I)V

    .line 81
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v0

    invoke-interface {v2, v3}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    move-result v1

    move-object/from16 v4, p2

    invoke-direct {v8, v0, v9, v4, v1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v0

    if-eqz v13, :cond_31

    .line 82
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v1

    aget v1, v1, v6

    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_22
    if-ge v2, v0, :cond_30

    .line 83
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v18

    add-int v27, v2, v1

    aget v18, v18, v27

    add-int v4, v4, v18

    add-int/lit8 v2, v2, 0x1

    goto :goto_22

    :cond_30
    const/4 v2, 0x0

    .line 84
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_24

    :cond_31
    const/4 v2, 0x0

    .line 85
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o〇O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v1

    mul-int v1, v1, v0

    add-int/lit8 v0, v0, -0x1

    .line 86
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v7, :cond_32

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v4

    goto :goto_23

    :cond_32
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v4

    :goto_23
    mul-int v0, v0, v4

    add-int/2addr v1, v0

    const/high16 v4, 0x40000000    # 2.0f

    .line 87
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_24
    move v4, v0

    .line 88
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;

    .line 89
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_33

    .line 90
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v1, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    .line 91
    invoke-static {v4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    iget v0, v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v27, v1

    move-object v1, v15

    move-object/from16 v11, p5

    move/from16 v24, v12

    const/4 v12, 0x0

    move-object v12, v3

    move/from16 v3, v27

    move/from16 v29, v7

    move/from16 v27, v10

    move-object/from16 v10, p2

    move v7, v4

    move/from16 v4, v17

    move/from16 v17, v13

    move v13, v5

    move/from16 v5, v18

    .line 92
    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇0o(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;IIIF)I

    move-result v0

    invoke-interface {v11, v12, v7, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_25

    :cond_33
    move-object/from16 v11, p5

    move/from16 v29, v7

    move/from16 v27, v10

    move/from16 v24, v12

    move/from16 v17, v13

    move-object/from16 v10, p2

    move-object v12, v3

    move v7, v4

    move v13, v5

    .line 93
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iget v3, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    .line 94
    invoke-static {v7}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    iget v5, v0, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;->〇OOo8〇0:F

    move-object/from16 v0, p0

    move-object v1, v15

    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇0o(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;IIIF)I

    move-result v0

    .line 95
    invoke-static {v7}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 96
    invoke-interface {v11, v12, v0, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    :goto_25
    move-object/from16 v7, v23

    .line 97
    invoke-virtual {v7, v12}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    move-result v0

    if-le v0, v13, :cond_34

    move v5, v0

    goto :goto_26

    :cond_34
    move v5, v13

    :goto_26
    add-int/lit8 v6, v6, 0x1

    move-object/from16 v11, p3

    move-object/from16 v23, v7

    move/from16 v13, v17

    move/from16 v12, v24

    move/from16 v10, v27

    move/from16 v7, v29

    goto/16 :goto_20

    :cond_35
    move-object/from16 v11, p5

    move/from16 v29, v7

    move/from16 v27, v10

    move/from16 v24, v12

    move/from16 v17, v13

    move-object/from16 v7, v23

    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v10, p2

    move v13, v5

    .line 98
    iget v3, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->o800o8O:I

    const/4 v5, 0x0

    const/high16 v6, 0x7fc00000    # Float.NaN

    move-object/from16 v0, p0

    move-object v1, v15

    move v2, v13

    const/high16 v12, 0x40000000    # 2.0f

    move v4, v5

    move v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇〇0o(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;IIIF)I

    move-result v0

    const/4 v5, 0x0

    :goto_27
    if-ge v5, v14, :cond_3b

    .line 99
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v1

    aget-object v1, v1, v5

    .line 100
    invoke-virtual {v7, v1}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->Oo08(Landroid/view/View;)I

    move-result v2

    if-eq v2, v13, :cond_3a

    .line 101
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    move-result-object v2

    invoke-interface {v11, v1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPosition(Landroid/view/View;)I

    move-result v3

    invoke-direct {v8, v2, v9, v10, v3}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oO(Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;I)I

    move-result v2

    if-eqz v17, :cond_37

    .line 102
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v3

    aget v3, v3, v5

    const/4 v4, 0x0

    const/4 v6, 0x0

    :goto_28
    if-ge v4, v2, :cond_36

    .line 103
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v16

    add-int v18, v4, v3

    aget v16, v16, v18

    add-int v6, v6, v16

    add-int/lit8 v4, v4, 0x1

    goto :goto_28

    :cond_36
    const/4 v4, 0x0

    .line 104
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_2a

    :cond_37
    const/4 v4, 0x0

    .line 105
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o〇O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v3

    mul-int v3, v3, v2

    add-int/lit8 v2, v2, -0x1

    .line 106
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-eqz v29, :cond_38

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v4

    goto :goto_29

    :cond_38
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v4

    :goto_29
    mul-int v2, v2, v4

    add-int/2addr v3, v2

    .line 107
    invoke-static {v3, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 108
    :goto_2a
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_39

    .line 109
    invoke-interface {v11, v1, v2, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_2b

    .line 110
    :cond_39
    invoke-interface {v11, v1, v0, v2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_2b

    :cond_3a
    const/4 v4, 0x1

    :goto_2b
    add-int/lit8 v5, v5, 0x1

    goto :goto_27

    :cond_3b
    const/4 v4, 0x1

    .line 111
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    move-result v0

    if-ne v0, v4, :cond_3c

    const/4 v0, 0x1

    goto :goto_2c

    :cond_3c
    const/4 v0, 0x0

    .line 112
    :goto_2c
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->isEnableMarginOverLap()Z

    move-result v1

    move/from16 v9, v29

    if-eqz v20, :cond_3d

    .line 113
    invoke-virtual {v8, v11, v9, v0, v1}, Lcom/alibaba/android/vlayout/layout/BaseLayoutHelper;->o〇〇0〇(Lcom/alibaba/android/vlayout/LayoutManagerHelper;ZZZ)I

    move-result v5

    move v10, v5

    goto :goto_2d

    :cond_3d
    const/4 v10, 0x0

    :goto_2d
    if-eqz v27, :cond_3f

    if-eqz v9, :cond_3e

    .line 114
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇8O8〇008()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oo〇()I

    move-result v1

    goto :goto_2e

    .line 115
    :cond_3e
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇O8〇〇o()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇〇0〇()I

    move-result v1

    :goto_2e
    add-int/2addr v0, v1

    move v5, v0

    move v12, v5

    goto :goto_2f

    :cond_3f
    const/4 v12, 0x0

    :goto_2f
    if-eqz v25, :cond_41

    .line 116
    iget-object v0, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    if-eqz v9, :cond_40

    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇oo〇()I

    move-result v0

    iget-object v1, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0000OOO()I

    move-result v1

    goto :goto_30

    .line 117
    :cond_40
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇00()I

    move-result v0

    iget-object v1, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OOO〇O0()I

    move-result v1

    :goto_30
    add-int/2addr v0, v1

    move v5, v0

    goto :goto_31

    :cond_41
    const/4 v5, 0x0

    :goto_31
    if-eqz v24, :cond_44

    if-eqz v9, :cond_42

    .line 118
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇oo〇()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0000OOO()I

    move-result v1

    goto :goto_32

    .line 119
    :cond_42
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇00()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OOO〇O0()I

    move-result v1

    :goto_32
    add-int/2addr v0, v1

    .line 120
    sget-boolean v1, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v1, :cond_43

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v2, v28

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v2, v24

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-object/from16 v3, v26

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v3, v21

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " secondEndSpace="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_33

    :cond_43
    move/from16 v3, v21

    move/from16 v2, v24

    goto :goto_33

    :cond_44
    move/from16 v3, v21

    move/from16 v2, v24

    const/4 v0, 0x0

    :goto_33
    add-int v1, v13, v10

    add-int/2addr v1, v5

    add-int/2addr v1, v12

    add-int/2addr v1, v0

    move-object/from16 v6, p4

    .line 122
    iput v1, v6, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 123
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->o〇0()I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_45

    const/4 v1, 0x1

    goto :goto_34

    :cond_45
    const/4 v1, 0x0

    .line 124
    :goto_34
    iget-boolean v4, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇O888o0o:Z

    const-string v11, " 1 "

    move-object/from16 v23, v7

    const-string v7, " 2 "

    move/from16 v16, v14

    const-string v14, "\u2b06 "

    move/from16 v18, v13

    const-string v13, "\u2b07 "

    if-nez v4, :cond_52

    const-string v4, " gap"

    if-nez v1, :cond_4b

    if-nez v20, :cond_4a

    if-eqz v27, :cond_47

    move/from16 p1, v0

    .line 125
    iget-object v0, v15, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    check-cast v0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    if-eqz v9, :cond_46

    invoke-static {v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    goto :goto_35

    :cond_46
    invoke-static {v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    .line 126
    :goto_35
    sget-boolean v20, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    move/from16 p2, v12

    if-eqz v20, :cond_49

    .line 127
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_37

    :cond_47
    move/from16 p1, v0

    move/from16 p2, v12

    if-eqz v9, :cond_48

    .line 128
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    goto :goto_36

    :cond_48
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    .line 129
    :goto_36
    sget-boolean v12, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v12, :cond_49

    .line 130
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_49
    :goto_37
    move v12, v0

    move/from16 v24, v2

    goto/16 :goto_3d

    :cond_4a
    move/from16 p1, v0

    move/from16 p2, v12

    goto :goto_3b

    :cond_4b
    move/from16 p1, v0

    move/from16 p2, v12

    if-nez v25, :cond_51

    if-eqz v2, :cond_4e

    .line 131
    iget-object v0, v15, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    check-cast v0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    if-eqz v9, :cond_4c

    invoke-static {v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    goto :goto_38

    :cond_4c
    invoke-static {v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    .line 132
    :goto_38
    sget-boolean v12, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v12, :cond_4d

    .line 133
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move/from16 v24, v2

    const-string v2, " 3 "

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3a

    :cond_4d
    move/from16 v24, v2

    goto :goto_3a

    :cond_4e
    move/from16 v24, v2

    if-eqz v9, :cond_4f

    .line 134
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    goto :goto_39

    :cond_4f
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v0

    .line 135
    :goto_39
    sget-boolean v2, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v2, :cond_50

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, " 4 "

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_50
    :goto_3a
    move v12, v0

    goto :goto_3d

    :cond_51
    :goto_3b
    move/from16 v24, v2

    goto :goto_3c

    :cond_52
    move/from16 p1, v0

    move/from16 v24, v2

    move/from16 p2, v12

    :goto_3c
    const/4 v12, 0x0

    .line 137
    :goto_3d
    iget v0, v6, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    add-int/2addr v0, v12

    iput v0, v6, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    if-gtz v0, :cond_53

    const/4 v0, 0x0

    .line 138
    iput v0, v6, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    .line 139
    :cond_53
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->OO0o〇〇〇〇0()Z

    move-result v0

    if-nez v0, :cond_59

    const-string v0, " last"

    if-eqz v1, :cond_57

    add-int/lit8 v2, v3, 0x1

    .line 140
    invoke-virtual {v8, v2}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v4

    if-nez v4, :cond_55

    .line 141
    iget-object v4, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v4, v2}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    move-result-object v4

    .line 142
    invoke-virtual {v4, v2}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo8Oo00oo(I)Z

    move-result v2

    if-eqz v2, :cond_55

    if-eqz v9, :cond_54

    .line 143
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇8O8〇008()I

    move-result v2

    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oo〇()I

    move-result v4

    goto :goto_3e

    .line 144
    :cond_54
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇O8〇〇o()I

    move-result v2

    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇〇0〇()I

    move-result v4

    :goto_3e
    add-int/2addr v2, v4

    .line 145
    sget-boolean v4, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v4, :cond_56

    .line 146
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3f

    :cond_55
    const/4 v2, 0x0

    :cond_56
    :goto_3f
    move v11, v2

    goto :goto_41

    :cond_57
    add-int/lit8 v2, v3, -0x1

    .line 147
    invoke-virtual {v8, v2}, Lcom/alibaba/android/vlayout/LayoutHelper;->OO0o〇〇〇〇0(I)Z

    move-result v4

    if-nez v4, :cond_59

    .line 148
    iget-object v4, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    invoke-virtual {v4, v2}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    move-result-object v4

    .line 149
    invoke-virtual {v4, v2}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇〇0〇〇0(I)Z

    move-result v2

    if-eqz v2, :cond_59

    if-eqz v9, :cond_58

    .line 150
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇oo〇()I

    move-result v2

    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0000OOO()I

    move-result v4

    goto :goto_40

    .line 151
    :cond_58
    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇00()I

    move-result v2

    invoke-virtual {v4}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OOO〇O0()I

    move-result v4

    :goto_40
    add-int/2addr v2, v4

    .line 152
    sget-boolean v4, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v4, :cond_56

    .line 153
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3f

    :cond_59
    const/4 v11, 0x0

    .line 154
    :goto_41
    sget-boolean v0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    if-eqz v0, :cond_5b

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v1, :cond_5a

    goto :goto_42

    :cond_5a
    move-object v14, v13

    :goto_42
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " consumed "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v6, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇080:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " startSpace "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " endSpace "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " secondStartSpace "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v13, p2

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " secondEndSpace "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v2, p1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " lastUnconsumedSpace "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " isSecondEndLine="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v3, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto :goto_43

    :cond_5b
    move/from16 v2, p1

    move/from16 v13, p2

    :goto_43
    if-eqz v9, :cond_5d

    if-eqz v1, :cond_5c

    .line 156
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    sub-int/2addr v0, v5

    sub-int/2addr v0, v2

    sub-int/2addr v0, v12

    sub-int v5, v0, v11

    sub-int v0, v5, v18

    move v1, v5

    goto :goto_44

    .line 157
    :cond_5c
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    add-int/2addr v0, v10

    add-int/2addr v0, v13

    add-int/2addr v0, v12

    add-int v5, v0, v11

    add-int v0, v5, v18

    move v1, v0

    move v0, v5

    :goto_44
    const/4 v2, 0x0

    const/4 v5, 0x0

    goto :goto_45

    :cond_5d
    if-eqz v1, :cond_5e

    .line 158
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    sub-int/2addr v0, v5

    sub-int/2addr v0, v12

    sub-int v5, v0, v11

    sub-int v0, v5, v18

    move v2, v0

    const/4 v0, 0x0

    const/4 v1, 0x0

    goto :goto_45

    .line 159
    :cond_5e
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v0

    add-int/2addr v0, v10

    add-int/2addr v0, v12

    add-int v5, v0, v11

    add-int v0, v5, v18

    move v2, v5

    const/4 v1, 0x0

    move v5, v0

    const/4 v0, 0x0

    :goto_45
    move/from16 v7, v16

    const/4 v14, 0x0

    :goto_46
    if-ge v14, v7, :cond_67

    .line 160
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v3

    aget-object v4, v3, v14

    .line 161
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v3

    aget v3, v3, v14

    .line 162
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutParams;

    if-eqz v9, :cond_61

    if-eqz v17, :cond_5f

    .line 163
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O00()I

    move-result v5

    add-int/2addr v2, v5

    const/4 v5, 0x0

    :goto_47
    if-ge v5, v3, :cond_60

    .line 164
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v18

    aget v18, v18, v5

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v20

    add-int v18, v18, v20

    add-int v2, v2, v18

    add-int/lit8 v5, v5, 0x1

    goto :goto_47

    .line 165
    :cond_5f
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇()I

    move-result v5

    add-int/2addr v2, v5

    .line 166
    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O00()I

    move-result v5

    add-int/2addr v2, v5

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o〇O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v5

    mul-int v5, v5, v3

    add-int/2addr v2, v5

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇O〇80o08O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v5

    mul-int v5, v5, v3

    add-int/2addr v2, v5

    :cond_60
    move-object/from16 v5, v23

    .line 167
    invoke-virtual {v5, v4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    move-result v18

    add-int v18, v2, v18

    move-object/from16 v30, v5

    move v5, v0

    move v0, v2

    move v2, v1

    move-object/from16 v1, v30

    move/from16 v31, v18

    move/from16 v18, v7

    move/from16 v7, v31

    goto :goto_49

    :cond_61
    move-object/from16 v1, v23

    if-eqz v17, :cond_62

    .line 168
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇()I

    move-result v18

    add-int v0, v0, v18

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0〇O0088o()I

    move-result v18

    add-int v0, v0, v18

    move/from16 p1, v2

    const/4 v2, 0x0

    :goto_48
    if-ge v2, v3, :cond_63

    .line 169
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v18

    aget v18, v18, v2

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v20

    add-int v18, v18, v20

    add-int v0, v0, v18

    add-int/lit8 v2, v2, 0x1

    goto :goto_48

    :cond_62
    move/from16 p1, v2

    .line 170
    invoke-interface/range {p5 .. p5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    move-result v0

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v15}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0〇O0088o()I

    move-result v2

    add-int/2addr v0, v2

    .line 171
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o〇O(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    mul-int v2, v2, v3

    add-int/2addr v0, v2

    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OOO(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    move-result v2

    mul-int v2, v2, v3

    add-int/2addr v0, v2

    .line 172
    :cond_63
    invoke-virtual {v1, v4}, Lcom/alibaba/android/vlayout/OrientationHelperEx;->o〇0(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v0

    move/from16 v18, v7

    move v7, v5

    move v5, v0

    move/from16 v0, p1

    .line 173
    :goto_49
    sget-boolean v20, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->oo88o8O:Z

    move-object/from16 v23, v1

    if-eqz v20, :cond_64

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "layout item in position: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->getViewPosition()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " with text with SpanIndex: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " into ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "), topInfo=[layoutState.getOffset()="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual/range {p3 .. p3}, Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;->〇〇888()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " startSpace="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " secondStartSpace="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " consumedGap="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " lastUnconsumedSpace="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_64
    const/16 v20, 0x0

    move/from16 v21, v0

    move-object v0, v15

    move-object/from16 v22, v23

    move-object v1, v4

    move/from16 v23, v2

    move/from16 v2, v21

    move v3, v5

    move-object/from16 v24, v4

    move v4, v7

    move/from16 v25, v5

    move/from16 v5, v23

    move/from16 v29, v9

    move-object/from16 v9, p4

    move-object/from16 v6, p5

    move/from16 v27, v7

    move/from16 v26, v18

    move-object/from16 v18, v22

    move/from16 v22, v29

    move/from16 v7, v20

    .line 176
    invoke-virtual/range {v0 .. v7}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;Z)V

    .line 177
    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    move-result v0

    if-nez v0, :cond_66

    invoke-virtual/range {v16 .. v16}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->isItemChanged()Z

    move-result v0

    if-eqz v0, :cond_65

    goto :goto_4a

    :cond_65
    const/4 v0, 0x1

    goto :goto_4b

    :cond_66
    :goto_4a
    const/4 v0, 0x1

    .line 178
    iput-boolean v0, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->〇o〇:Z

    .line 179
    :goto_4b
    iget-boolean v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->isFocusable()Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, v9, Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;->O8:Z

    add-int/lit8 v14, v14, 0x1

    move-object v6, v9

    move/from16 v2, v21

    move/from16 v9, v22

    move/from16 v1, v23

    move/from16 v0, v25

    move/from16 v7, v26

    move/from16 v5, v27

    move-object/from16 v23, v18

    goto/16 :goto_46

    :cond_67
    const/4 v1, 0x0

    .line 180
    iput-boolean v1, v8, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇O888o0o:Z

    .line 181
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->OO8oO0o〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 182
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->〇80(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 183
    invoke-static {v15}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o88〇OO08〇(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)[I

    move-result-object v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method

.method public 〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇o〇(Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$State;->getItemCount()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-lez p1, :cond_2

    .line 6
    .line 7
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 8
    .line 9
    iget p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 10
    .line 11
    invoke-virtual {p1, p3}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 16
    .line 17
    .line 18
    move-result-object p3

    .line 19
    iget v0, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 20
    .line 21
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p3, v0, v1}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 26
    .line 27
    .line 28
    move-result p3

    .line 29
    iget-boolean v0, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇o〇:Z

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    :goto_0
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    sub-int/2addr v0, v1

    .line 39
    if-ge p3, v0, :cond_1

    .line 40
    .line 41
    iget p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Ljava/lang/Integer;

    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-ge p3, v0, :cond_1

    .line 58
    .line 59
    iget p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 60
    .line 61
    add-int/2addr p3, v1

    .line 62
    iput p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 63
    .line 64
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 65
    .line 66
    .line 67
    move-result-object p3

    .line 68
    iget v0, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 69
    .line 70
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    invoke-virtual {p3, v0, v2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 75
    .line 76
    .line 77
    move-result p3

    .line 78
    goto :goto_0

    .line 79
    :cond_0
    :goto_1
    if-lez p3, :cond_1

    .line 80
    .line 81
    iget p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 82
    .line 83
    if-lez p3, :cond_1

    .line 84
    .line 85
    add-int/lit8 p3, p3, -0x1

    .line 86
    .line 87
    iput p3, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 88
    .line 89
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O0o〇〇Oo(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;

    .line 90
    .line 91
    .line 92
    move-result-object p3

    .line 93
    iget v0, p2, Lcom/alibaba/android/vlayout/VirtualLayoutManager$AnchorInfoWrapper;->〇080:I

    .line 94
    .line 95
    invoke-static {p1}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->O000(Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;)I

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {p3, v0, v2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080(II)I

    .line 100
    .line 101
    .line 102
    move-result p3

    .line 103
    goto :goto_1

    .line 104
    :cond_1
    iput-boolean v1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->〇O888o0o:Z

    .line 105
    .line 106
    :cond_2
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
.end method

.method public 〇〇808〇(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->o8oO〇(II)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public 〇〇〇0〇〇0(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/LayoutHelper;->oO80()Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Ljava/lang/Integer;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper;->OoO8:Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;->Oo〇O(I)Lcom/alibaba/android/vlayout/layout/RangeGridLayoutHelper$GridRangeStyle;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    const/4 v1, 0x1

    .line 26
    if-ne p1, v1, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O〇()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    :goto_0
    add-int/2addr p1, v0

    .line 37
    return p1

    .line 38
    :cond_0
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇8O0〇8()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    goto :goto_0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

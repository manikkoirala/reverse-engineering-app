.class public Lcom/alibaba/android/vlayout/layout/RangeStyle;
.super Ljava/lang/Object;
.source "RangeStyle.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/alibaba/android/vlayout/layout/RangeStyle;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected O8:Lcom/alibaba/android/vlayout/Range;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/alibaba/android/vlayout/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected OO0o〇〇:I

.field protected OO0o〇〇〇〇0:I

.field protected Oo08:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lcom/alibaba/android/vlayout/Range<",
            "Ljava/lang/Integer;",
            ">;TT;>;"
        }
    .end annotation
.end field

.field protected Oooo8o0〇:Landroid/graphics/Rect;

.field protected oO80:I

.field protected o〇0:I

.field protected 〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected 〇80〇808〇O:I

.field protected 〇8o8o〇:I

.field protected 〇O8o08O:I

.field private 〇O〇:I

.field private 〇o00〇〇Oo:I

.field private 〇o〇:I

.field private 〇〇808〇:Landroid/view/View;

.field protected 〇〇888:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o00〇〇Oo:I

    .line 6
    .line 7
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o〇:I

    .line 8
    .line 9
    new-instance v0, Ljava/util/HashMap;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 15
    .line 16
    new-instance v0, Landroid/graphics/Rect;

    .line 17
    .line 18
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private O08000(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/alibaba/android/vlayout/LayoutManagerHelper;",
            "Lcom/alibaba/android/vlayout/layout/RangeStyle<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Ljava/util/Map$Entry;

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 34
    .line 35
    invoke-direct {p0, p1, v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O08000(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 40
    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 44
    .line 45
    .line 46
    const/4 p1, 0x0

    .line 47
    iput-object p1, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 48
    .line 49
    :cond_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private O8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/alibaba/android/vlayout/LayoutManagerHelper;",
            "Lcom/alibaba/android/vlayout/layout/RangeStyle<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    iget-object p2, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 21
    .line 22
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Ljava/util/Map$Entry;

    .line 41
    .line 42
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    check-cast v0, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 47
    .line 48
    invoke-direct {p0, p1, v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private o0ooO(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/alibaba/android/vlayout/LayoutManagerHelper;",
            "Lcom/alibaba/android/vlayout/layout/RangeStyle<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iget-object p2, p2, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Ljava/util/Map$Entry;

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-nez v1, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, p1, v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o0ooO(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    iget-object v0, v0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 39
    .line 40
    if-eqz v0, :cond_0

    .line 41
    .line 42
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->hideView(Landroid/view/View;)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇0o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1, p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o0ooO(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->hideView(Landroid/view/View;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private o〇8oOO88(Lcom/alibaba/android/vlayout/layout/RangeStyle;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/alibaba/android/vlayout/layout/RangeStyle<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Ljava/util/Map$Entry;

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 34
    .line 35
    invoke-direct {p0, v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇8oOO88(Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 36
    .line 37
    .line 38
    iget-object v2, v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 39
    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    iget-object v3, p1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 43
    .line 44
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    iget-object v4, v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 49
    .line 50
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    iget-object v5, v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 55
    .line 56
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    iget-object v1, v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 61
    .line 62
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    invoke-virtual {v3, v2, v4, v5, v1}, Landroid/graphics/Rect;->union(IIII)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private 〇08O8o〇0(I)Z
    .locals 1

    .line 1
    const v0, 0x7fffffff

    .line 2
    .line 3
    .line 4
    if-eq p1, v0, :cond_0

    .line 5
    .line 6
    const/high16 v0, -0x80000000

    .line 7
    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    const/4 p1, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇8〇0〇o〇O(Lcom/alibaba/android/vlayout/layout/RangeStyle;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/alibaba/android/vlayout/layout/RangeStyle<",
            "TT;>;)Z"
        }
    .end annotation

    .line 1
    iget v0, p1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O〇:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x1

    .line 8
    :goto_0
    iget-object p1, p1, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Ljava/util/Map$Entry;

    .line 29
    .line 30
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    if-nez v2, :cond_1

    .line 41
    .line 42
    invoke-direct {p0, v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8〇0〇o〇O(Lcom/alibaba/android/vlayout/layout/RangeStyle;)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    or-int/2addr v0, v1

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇O〇oO()Z

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    return p1

    .line 53
    :cond_2
    return v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method


# virtual methods
.method public O8ooOoo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8〇o()Lcom/alibaba/android/vlayout/Range;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/alibaba/android/vlayout/Range<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8:Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O888o0o()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OOO〇O0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected Oo08(IIIIZ)V
    .locals 6

    .line 1
    if-eqz p5, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 4
    .line 5
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 6
    .line 7
    sub-int v1, p1, v1

    .line 8
    .line 9
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0:I

    .line 10
    .line 11
    sub-int/2addr v1, v2

    .line 12
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 13
    .line 14
    sub-int v2, p2, v2

    .line 15
    .line 16
    iget v3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O:I

    .line 17
    .line 18
    sub-int/2addr v2, v3

    .line 19
    iget v3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 20
    .line 21
    add-int/2addr v3, p3

    .line 22
    iget v4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇:I

    .line 23
    .line 24
    add-int/2addr v3, v4

    .line 25
    iget v4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 26
    .line 27
    add-int/2addr v4, p4

    .line 28
    iget v5, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇:I

    .line 29
    .line 30
    add-int/2addr v4, v5

    .line 31
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 36
    .line 37
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 38
    .line 39
    sub-int v1, p1, v1

    .line 40
    .line 41
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 42
    .line 43
    sub-int v2, p2, v2

    .line 44
    .line 45
    iget v3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 46
    .line 47
    add-int/2addr v3, p3

    .line 48
    iget v4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 49
    .line 50
    add-int/2addr v4, p4

    .line 51
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 55
    .line 56
    if-eqz v0, :cond_1

    .line 57
    .line 58
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 59
    .line 60
    sub-int v1, p1, v1

    .line 61
    .line 62
    iget v2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0:I

    .line 63
    .line 64
    sub-int/2addr v1, v2

    .line 65
    iget v3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 66
    .line 67
    sub-int v3, p2, v3

    .line 68
    .line 69
    sub-int v2, v3, v2

    .line 70
    .line 71
    iget v3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 72
    .line 73
    add-int/2addr v3, p3

    .line 74
    iget v4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇:I

    .line 75
    .line 76
    add-int/2addr v3, v4

    .line 77
    iget v4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 78
    .line 79
    add-int/2addr v4, p4

    .line 80
    iget v5, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇:I

    .line 81
    .line 82
    add-int/2addr v4, v5

    .line 83
    move v5, p5

    .line 84
    invoke-virtual/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08(IIIIZ)V

    .line 85
    .line 86
    .line 87
    :cond_1
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method public Oo8Oo00oo(I)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8:Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->O8()Ljava/lang/Comparable;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Ljava/lang/Integer;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-ne v0, p1, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    return v1
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public OoO8()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OoO8()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇00〇8()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oooo8o0〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇8O8〇008()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇O〇oO()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O〇:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x1

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_1

    .line 13
    .line 14
    invoke-direct {p0, p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8〇0〇o〇O(Lcom/alibaba/android/vlayout/layout/RangeStyle;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    or-int/2addr v0, v1

    .line 19
    :cond_1
    return v0
    .line 20
.end method

.method public o8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o800o8O()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o8oO〇(II)V
    .locals 6

    .line 1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    invoke-static {v0, p2}, Lcom/alibaba/android/vlayout/Range;->〇o〇(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/alibaba/android/vlayout/Range;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    iput-object p2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8:Lcom/alibaba/android/vlayout/Range;

    .line 14
    .line 15
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 16
    .line 17
    invoke-virtual {p2}, Ljava/util/HashMap;->isEmpty()Z

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    if-nez p2, :cond_1

    .line 22
    .line 23
    new-instance p2, Ljava/util/HashMap;

    .line 24
    .line 25
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_0

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Ljava/util/Map$Entry;

    .line 49
    .line 50
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    check-cast v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇oOO8O8()I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    add-int/2addr v2, p1

    .line 61
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8ooOoo〇()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    add-int/2addr v3, p1

    .line 66
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 67
    .line 68
    .line 69
    move-result-object v4

    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    invoke-static {v4, v5}, Lcom/alibaba/android/vlayout/Range;->〇o〇(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/alibaba/android/vlayout/Range;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    invoke-virtual {p2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v2, v3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8oO〇(II)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_0
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 86
    .line 87
    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    .line 88
    .line 89
    .line 90
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 91
    .line 92
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 93
    .line 94
    .line 95
    :cond_1
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method public oO(Landroid/view/View;IIIILcom/alibaba/android/vlayout/LayoutManagerHelper;Z)V
    .locals 6
    .param p6    # Lcom/alibaba/android/vlayout/LayoutManagerHelper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object v0, p6

    .line 2
    move-object v1, p1

    .line 3
    move v2, p2

    .line 4
    move v3, p3

    .line 5
    move v4, p4

    .line 6
    move v5, p5

    .line 7
    invoke-interface/range {v0 .. v5}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->OO0o〇〇(Landroid/view/View;IIII)V

    .line 8
    .line 9
    .line 10
    move-object v0, p0

    .line 11
    move v1, p2

    .line 12
    move v2, p3

    .line 13
    move v3, p4

    .line 14
    move v4, p5

    .line 15
    move v5, p7

    .line 16
    invoke-virtual/range {v0 .. v5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08(IIIIZ)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
.end method

.method public oO80()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OOO〇O0()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
.end method

.method protected oo88o8O()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 2
    .line 3
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0000OOO()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
.end method

.method public o〇0OOo〇0(I)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8:Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Lcom/alibaba/android/vlayout/Range;->〇o00〇〇Oo(Ljava/lang/Comparable;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-nez p1, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v1, 0x0

    .line 18
    :cond_1
    :goto_0
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇O8〇〇o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇〇0〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇00()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0000OOO()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected 〇00〇8()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O:I

    .line 2
    .line 3
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Ljava/util/Map$Entry;

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    move-object v2, v1

    .line 34
    check-cast v2, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 35
    .line 36
    move-object v3, p1

    .line 37
    move-object v4, p2

    .line 38
    move v5, p3

    .line 39
    move v6, p4

    .line 40
    move v7, p5

    .line 41
    move-object v8, p6

    .line 42
    invoke-virtual/range {v2 .. v8}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;IIILcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇O〇oO()Z

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    if-eqz p1, :cond_9

    .line 51
    .line 52
    invoke-direct {p0, p5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇08O8o〇0(I)Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 59
    .line 60
    if-eqz p1, :cond_1

    .line 61
    .line 62
    iget-object p2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 63
    .line 64
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    iget-object p3, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 69
    .line 70
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    .line 71
    .line 72
    .line 73
    move-result p3

    .line 74
    iget-object p4, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 75
    .line 76
    invoke-virtual {p4}, Landroid/view/View;->getRight()I

    .line 77
    .line 78
    .line 79
    move-result p4

    .line 80
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 81
    .line 82
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    invoke-virtual {p2, p1, p3, p4, v0}, Landroid/graphics/Rect;->union(IIII)V

    .line 87
    .line 88
    .line 89
    :cond_1
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-nez p1, :cond_9

    .line 96
    .line 97
    invoke-direct {p0, p5}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇08O8o〇0(I)Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    const/4 p2, 0x1

    .line 102
    const/4 p3, 0x0

    .line 103
    if-eqz p1, :cond_3

    .line 104
    .line 105
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 106
    .line 107
    .line 108
    move-result p1

    .line 109
    if-ne p1, p2, :cond_2

    .line 110
    .line 111
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 112
    .line 113
    neg-int p4, p5

    .line 114
    invoke-virtual {p1, p3, p4}, Landroid/graphics/Rect;->offset(II)V

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_2
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 119
    .line 120
    neg-int p4, p5

    .line 121
    invoke-virtual {p1, p4, p3}, Landroid/graphics/Rect;->offset(II)V

    .line 122
    .line 123
    .line 124
    :cond_3
    :goto_1
    invoke-direct {p0, p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇8oOO88(Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 125
    .line 126
    .line 127
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇〇808〇()I

    .line 132
    .line 133
    .line 134
    move-result p4

    .line 135
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 136
    .line 137
    .line 138
    move-result p5

    .line 139
    if-ne p5, p2, :cond_4

    .line 140
    .line 141
    iget-object p5, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 142
    .line 143
    neg-int v0, p4

    .line 144
    div-int/lit8 v0, v0, 0x4

    .line 145
    .line 146
    div-int/lit8 v1, p4, 0x4

    .line 147
    .line 148
    add-int/2addr p4, v1

    .line 149
    invoke-virtual {p5, p3, v0, p1, p4}, Landroid/graphics/Rect;->intersects(IIII)Z

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    if-eqz p1, :cond_7

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_4
    iget-object p5, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 157
    .line 158
    neg-int v0, p1

    .line 159
    div-int/lit8 v0, v0, 0x4

    .line 160
    .line 161
    div-int/lit8 v1, p1, 0x4

    .line 162
    .line 163
    add-int/2addr p1, v1

    .line 164
    invoke-virtual {p5, v0, p3, p1, p4}, Landroid/graphics/Rect;->intersects(IIII)Z

    .line 165
    .line 166
    .line 167
    move-result p1

    .line 168
    if-eqz p1, :cond_7

    .line 169
    .line 170
    :goto_2
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 171
    .line 172
    if-nez p1, :cond_5

    .line 173
    .line 174
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->o〇O8〇〇o()Landroid/view/View;

    .line 175
    .line 176
    .line 177
    move-result-object p1

    .line 178
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 179
    .line 180
    invoke-interface {p6, p1, p2}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->o800o8O(Landroid/view/View;Z)V

    .line 181
    .line 182
    .line 183
    :cond_5
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getOrientation()I

    .line 184
    .line 185
    .line 186
    move-result p1

    .line 187
    if-ne p1, p2, :cond_6

    .line 188
    .line 189
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 190
    .line 191
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingLeft()I

    .line 192
    .line 193
    .line 194
    move-result p2

    .line 195
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇()I

    .line 196
    .line 197
    .line 198
    move-result p3

    .line 199
    add-int/2addr p2, p3

    .line 200
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888()I

    .line 201
    .line 202
    .line 203
    move-result p3

    .line 204
    add-int/2addr p2, p3

    .line 205
    iput p2, p1, Landroid/graphics/Rect;->left:I

    .line 206
    .line 207
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 208
    .line 209
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 210
    .line 211
    .line 212
    move-result p2

    .line 213
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingRight()I

    .line 214
    .line 215
    .line 216
    move-result p3

    .line 217
    sub-int/2addr p2, p3

    .line 218
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇()I

    .line 219
    .line 220
    .line 221
    move-result p3

    .line 222
    sub-int/2addr p2, p3

    .line 223
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80()I

    .line 224
    .line 225
    .line 226
    move-result p3

    .line 227
    sub-int/2addr p2, p3

    .line 228
    iput p2, p1, Landroid/graphics/Rect;->right:I

    .line 229
    .line 230
    goto :goto_3

    .line 231
    :cond_6
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 232
    .line 233
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingTop()I

    .line 234
    .line 235
    .line 236
    move-result p2

    .line 237
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇()I

    .line 238
    .line 239
    .line 240
    move-result p3

    .line 241
    add-int/2addr p2, p3

    .line 242
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O()I

    .line 243
    .line 244
    .line 245
    move-result p3

    .line 246
    add-int/2addr p2, p3

    .line 247
    iput p2, p1, Landroid/graphics/Rect;->top:I

    .line 248
    .line 249
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 250
    .line 251
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇80〇808〇O()I

    .line 252
    .line 253
    .line 254
    move-result p2

    .line 255
    invoke-interface {p6}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->getPaddingBottom()I

    .line 256
    .line 257
    .line 258
    move-result p3

    .line 259
    sub-int/2addr p2, p3

    .line 260
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O()I

    .line 261
    .line 262
    .line 263
    move-result p3

    .line 264
    sub-int/2addr p2, p3

    .line 265
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0()I

    .line 266
    .line 267
    .line 268
    move-result p3

    .line 269
    sub-int/2addr p2, p3

    .line 270
    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    .line 271
    .line 272
    :goto_3
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 273
    .line 274
    invoke-virtual {p0, p1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o〇(Landroid/view/View;)V

    .line 275
    .line 276
    .line 277
    invoke-direct {p0, p6}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 278
    .line 279
    .line 280
    return-void

    .line 281
    :cond_7
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 282
    .line 283
    invoke-virtual {p1, p3, p3, p3, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 284
    .line 285
    .line 286
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 287
    .line 288
    if-eqz p1, :cond_8

    .line 289
    .line 290
    invoke-virtual {p1, p3, p3, p3, p3}, Landroid/view/View;->layout(IIII)V

    .line 291
    .line 292
    .line 293
    :cond_8
    invoke-direct {p0, p6}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 294
    .line 295
    .line 296
    :cond_9
    invoke-direct {p0, p6}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 297
    .line 298
    .line 299
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇0o()Z

    .line 300
    .line 301
    .line 302
    move-result p1

    .line 303
    if-eqz p1, :cond_a

    .line 304
    .line 305
    invoke-direct {p0, p6, p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O08000(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 306
    .line 307
    .line 308
    :cond_a
    return-void
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
.end method

.method public 〇0〇O0088o()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇0〇O0088o()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8(Lcom/alibaba/android/vlayout/LayoutManagerHelper;Lcom/alibaba/android/vlayout/layout/RangeStyle;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇80〇808〇O()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oo〇()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
.end method

.method public 〇8o8o〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oo88o8O()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O00()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O00()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇0:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected 〇O888o0o()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇〇〇0:I

    .line 2
    .line 3
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇8o8o〇:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8o08O()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected 〇o()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->oO80:I

    .line 2
    .line 3
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇80〇808〇O:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oo08:Ljava/util/HashMap;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Ljava/util/Map$Entry;

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 34
    .line 35
    invoke-virtual {v1, p1, p2, p3}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o00〇〇Oo(Landroidx/recyclerview/widget/RecyclerView$Recycler;Landroidx/recyclerview/widget/RecyclerView$State;Lcom/alibaba/android/vlayout/LayoutManagerHelper;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O〇O〇oO()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 47
    .line 48
    if-eqz p1, :cond_2

    .line 49
    .line 50
    invoke-interface {p3, p1}, Lcom/alibaba/android/vlayout/LayoutManagerHelper;->〇O8o08O(Landroid/view/View;)V

    .line 51
    .line 52
    .line 53
    const/4 p1, 0x0

    .line 54
    iput-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇:Landroid/view/View;

    .line 55
    .line 56
    :cond_2
    :goto_1
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇oOO8O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->OO0o〇〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o〇(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/high16 v1, 0x40000000    # 2.0f

    .line 8
    .line 9
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v2, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 27
    .line 28
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 29
    .line 30
    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 33
    .line 34
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 35
    .line 36
    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 37
    .line 38
    .line 39
    iget v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O〇:I

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->Oooo8o0〇:Landroid/graphics/Rect;

    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public 〇〇0o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇808〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇808〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇O8o08O:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇888()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->o〇〇0〇()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
.end method

.method public 〇〇8O0〇8()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇080:Lcom/alibaba/android/vlayout/layout/RangeStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇8O0〇8()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    iget v1, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->〇〇888:I

    .line 12
    .line 13
    add-int/2addr v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇〇0〇〇0(I)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/RangeStyle;->O8:Lcom/alibaba/android/vlayout/Range;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/alibaba/android/vlayout/Range;->Oo08()Ljava/lang/Comparable;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Ljava/lang/Integer;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-ne v0, p1, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    return v1
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.class public abstract Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;
.super Ljava/lang/Object;
.source "GridLayoutHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/alibaba/android/vlayout/layout/GridLayoutHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SpanSizeLookup"
.end annotation


# instance fields
.field final 〇080:Landroid/util/SparseIntArray;

.field private 〇o00〇〇Oo:Z

.field 〇o〇:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/util/SparseIntArray;

    .line 5
    .line 6
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080:Landroid/util/SparseIntArray;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o00〇〇Oo:Z

    .line 13
    .line 14
    iput v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080:Landroid/util/SparseIntArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo08(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public o〇0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method 〇080(II)I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o00〇〇Oo(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080:Landroid/util/SparseIntArray;

    .line 11
    .line 12
    const/4 v1, -0x1

    .line 13
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eq v0, v1, :cond_1

    .line 18
    .line 19
    return v0

    .line 20
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇o00〇〇Oo(II)I

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    iget-object v0, p0, Lcom/alibaba/android/vlayout/layout/GridLayoutHelper$SpanSizeLookup;->〇080:Landroid/util/SparseIntArray;

    .line 25
    .line 26
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 27
    .line 28
    .line 29
    return p2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public abstract 〇o00〇〇Oo(II)I
.end method

.method public abstract 〇o〇(I)I
.end method

.class public interface abstract Lcom/alibaba/android/vlayout/LayoutManagerHelper;
.super Ljava/lang/Object;
.source "LayoutManagerHelper.java"


# virtual methods
.method public abstract OO0o〇〇(Landroid/view/View;IIII)V
.end method

.method public abstract OoO8(Landroid/view/View;Z)V
.end method

.method public abstract Oooo8o0〇()Lcom/alibaba/android/vlayout/OrientationHelperEx;
.end method

.method public abstract findViewByPosition(I)Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract getChildAt(I)Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract getChildCount()I
.end method

.method public abstract getOrientation()I
.end method

.method public abstract getPaddingBottom()I
.end method

.method public abstract getPaddingLeft()I
.end method

.method public abstract getPaddingRight()I
.end method

.method public abstract getPaddingTop()I
.end method

.method public abstract getPosition(Landroid/view/View;)I
.end method

.method public abstract getReverseLayout()Z
.end method

.method public abstract hideView(Landroid/view/View;)V
.end method

.method public abstract isEnableMarginOverLap()Z
.end method

.method public abstract measureChild(Landroid/view/View;II)V
.end method

.method public abstract measureChildWithMargins(Landroid/view/View;II)V
.end method

.method public abstract o800o8O(Landroid/view/View;Z)V
.end method

.method public abstract oo88o8O(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;)V
.end method

.method public abstract o〇O8〇〇o()Landroid/view/View;
.end method

.method public abstract showView(Landroid/view/View;)V
.end method

.method public abstract 〇00(IIZ)I
.end method

.method public abstract 〇0〇O0088o(Lcom/alibaba/android/vlayout/VirtualLayoutManager$LayoutStateWrapper;Landroid/view/View;I)V
.end method

.method public abstract 〇80〇808〇O()I
.end method

.method public abstract 〇8o8o〇(Landroid/view/View;I)V
.end method

.method public abstract 〇O00(Landroid/view/View;)V
.end method

.method public abstract 〇O8o08O(Landroid/view/View;)V
.end method

.method public abstract 〇o00〇〇Oo(Landroid/view/View;)Z
.end method

.method public abstract 〇o〇(I)Lcom/alibaba/android/vlayout/LayoutHelper;
.end method

.method public abstract 〇〇808〇()I
.end method

.method public abstract 〇〇888(Landroid/view/View;)V
.end method

.method public abstract 〇〇8O0〇8()Z
.end method

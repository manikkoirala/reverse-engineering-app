.class public Lcom/alibaba/android/arouter/routes/ARouter$$Group$$main;
.super Ljava/lang/Object;
.source "ARouter$$Group$$main.java"

# interfaces
.implements Lcom/alibaba/android/arouter/facade/template/IRouteGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public loadInto(Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/alibaba/android/arouter/facade/model/RouteMeta;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    sget-object v8, Lcom/alibaba/android/arouter/facade/enums/RouteType;->PROVIDER:Lcom/alibaba/android/arouter/facade/enums/RouteType;

    .line 4
    .line 5
    const-class v2, Lcom/intsig/camscanner/router/RouterMainServiceImpl;

    .line 6
    .line 7
    const-string v3, "/main/connectmain"

    .line 8
    .line 9
    const-string v4, "main"

    .line 10
    .line 11
    const/4 v5, 0x0

    .line 12
    const/4 v6, -0x1

    .line 13
    const/high16 v7, -0x80000000

    .line 14
    .line 15
    move-object v1, v8

    .line 16
    invoke-static/range {v1 .. v7}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const-string v2, "/main/connectmain"

    .line 21
    .line 22
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    const-class v2, Lcom/intsig/camscanner/tsapp/RouterLoginTaskServiceImpl;

    .line 26
    .line 27
    const-string v3, "/main/login_task"

    .line 28
    .line 29
    const-string v4, "main"

    .line 30
    .line 31
    move-object v1, v8

    .line 32
    invoke-static/range {v1 .. v7}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    const-string v2, "/main/login_task"

    .line 37
    .line 38
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    const-class v2, Lcom/intsig/camscanner/tsapp/RouterLogoutTaskServiceImpl;

    .line 42
    .line 43
    const-string v3, "/main/logout_task"

    .line 44
    .line 45
    const-string v4, "main"

    .line 46
    .line 47
    move-object v1, v8

    .line 48
    invoke-static/range {v1 .. v7}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const-string v2, "/main/logout_task"

    .line 53
    .line 54
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    sget-object v1, Lcom/alibaba/android/arouter/facade/enums/RouteType;->ACTIVITY:Lcom/alibaba/android/arouter/facade/enums/RouteType;

    .line 58
    .line 59
    const-class v10, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 60
    .line 61
    const-string v11, "/main/main_menu_new"

    .line 62
    .line 63
    const-string v12, "main"

    .line 64
    .line 65
    const/4 v13, 0x0

    .line 66
    const/4 v14, -0x1

    .line 67
    const/high16 v15, -0x80000000

    .line 68
    .line 69
    move-object v9, v1

    .line 70
    invoke-static/range {v9 .. v15}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    const-string v3, "/main/main_menu_new"

    .line 75
    .line 76
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    const-class v10, Lcom/intsig/camscanner/purchase/activity/PurchaseIntentActivity;

    .line 80
    .line 81
    const-string v11, "/main/premium"

    .line 82
    .line 83
    const-string v12, "main"

    .line 84
    .line 85
    new-instance v13, Lcom/alibaba/android/arouter/routes/ARouter$$Group$$main$1;

    .line 86
    .line 87
    move-object/from16 v7, p0

    .line 88
    .line 89
    invoke-direct {v13, v7}, Lcom/alibaba/android/arouter/routes/ARouter$$Group$$main$1;-><init>(Lcom/alibaba/android/arouter/routes/ARouter$$Group$$main;)V

    .line 90
    .line 91
    .line 92
    invoke-static/range {v9 .. v15}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    const-string v2, "/main/premium"

    .line 97
    .line 98
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .line 100
    .line 101
    const-class v2, Lcom/intsig/camscanner/purchase/utils/ProductManagerService;

    .line 102
    .line 103
    const-string v3, "/main/productmanager"

    .line 104
    .line 105
    const-string v4, "main"

    .line 106
    .line 107
    const/high16 v9, -0x80000000

    .line 108
    .line 109
    move-object v1, v8

    .line 110
    move v7, v9

    .line 111
    invoke-static/range {v1 .. v7}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    const-string v2, "/main/productmanager"

    .line 116
    .line 117
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    .line 119
    .line 120
    const-class v2, Lcom/intsig/camscanner/tsapp/RouterToolPageServiceImpl;

    .line 121
    .line 122
    const-string v3, "/main/toolpage"

    .line 123
    .line 124
    const-string v4, "main"

    .line 125
    .line 126
    const/high16 v7, -0x80000000

    .line 127
    .line 128
    move-object v1, v8

    .line 129
    invoke-static/range {v1 .. v7}, Lcom/alibaba/android/arouter/facade/model/RouteMeta;->build(Lcom/alibaba/android/arouter/facade/enums/RouteType;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;II)Lcom/alibaba/android/arouter/facade/model/RouteMeta;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    const-string v2, "/main/toolpage"

    .line 134
    .line 135
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.class public Lcom/airbnb/lottie/model/DocumentData;
.super Ljava/lang/Object;
.source "DocumentData.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/airbnb/lottie/model/DocumentData$Justification;
    }
.end annotation


# instance fields
.field public final O8:Lcom/airbnb/lottie/model/DocumentData$Justification;

.field public final OO0o〇〇〇〇0:F

.field public final Oo08:I

.field public final oO80:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public final o〇0:F

.field public final 〇080:Ljava/lang/String;

.field public final 〇80〇808〇O:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public final 〇8o8o〇:Z

.field public final 〇o00〇〇Oo:Ljava/lang/String;

.field public final 〇o〇:F

.field public final 〇〇888:F


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FLcom/airbnb/lottie/model/DocumentData$Justification;IFFIIFZ)V
    .locals 0
    .param p8    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/airbnb/lottie/model/DocumentData;->〇080:Ljava/lang/String;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/airbnb/lottie/model/DocumentData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    iput p3, p0, Lcom/airbnb/lottie/model/DocumentData;->〇o〇:F

    .line 9
    .line 10
    iput-object p4, p0, Lcom/airbnb/lottie/model/DocumentData;->O8:Lcom/airbnb/lottie/model/DocumentData$Justification;

    .line 11
    .line 12
    iput p5, p0, Lcom/airbnb/lottie/model/DocumentData;->Oo08:I

    .line 13
    .line 14
    iput p6, p0, Lcom/airbnb/lottie/model/DocumentData;->o〇0:F

    .line 15
    .line 16
    iput p7, p0, Lcom/airbnb/lottie/model/DocumentData;->〇〇888:F

    .line 17
    .line 18
    iput p8, p0, Lcom/airbnb/lottie/model/DocumentData;->oO80:I

    .line 19
    .line 20
    iput p9, p0, Lcom/airbnb/lottie/model/DocumentData;->〇80〇808〇O:I

    .line 21
    .line 22
    iput p10, p0, Lcom/airbnb/lottie/model/DocumentData;->OO0o〇〇〇〇0:F

    .line 23
    .line 24
    iput-boolean p11, p0, Lcom/airbnb/lottie/model/DocumentData;->〇8o8o〇:Z

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
.end method


# virtual methods
.method public hashCode()I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/airbnb/lottie/model/DocumentData;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x1f

    .line 8
    .line 9
    iget-object v1, p0, Lcom/airbnb/lottie/model/DocumentData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    mul-int/lit8 v0, v0, 0x1f

    .line 17
    .line 18
    int-to-float v0, v0

    .line 19
    iget v1, p0, Lcom/airbnb/lottie/model/DocumentData;->〇o〇:F

    .line 20
    .line 21
    add-float/2addr v0, v1

    .line 22
    float-to-int v0, v0

    .line 23
    mul-int/lit8 v0, v0, 0x1f

    .line 24
    .line 25
    iget-object v1, p0, Lcom/airbnb/lottie/model/DocumentData;->O8:Lcom/airbnb/lottie/model/DocumentData$Justification;

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    add-int/2addr v0, v1

    .line 32
    mul-int/lit8 v0, v0, 0x1f

    .line 33
    .line 34
    iget v1, p0, Lcom/airbnb/lottie/model/DocumentData;->Oo08:I

    .line 35
    .line 36
    add-int/2addr v0, v1

    .line 37
    iget v1, p0, Lcom/airbnb/lottie/model/DocumentData;->o〇0:F

    .line 38
    .line 39
    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    int-to-long v1, v1

    .line 44
    mul-int/lit8 v0, v0, 0x1f

    .line 45
    .line 46
    const/16 v3, 0x20

    .line 47
    .line 48
    ushr-long v3, v1, v3

    .line 49
    .line 50
    xor-long/2addr v1, v3

    .line 51
    long-to-int v2, v1

    .line 52
    add-int/2addr v0, v2

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    .line 54
    .line 55
    iget v1, p0, Lcom/airbnb/lottie/model/DocumentData;->oO80:I

    .line 56
    .line 57
    add-int/2addr v0, v1

    .line 58
    return v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

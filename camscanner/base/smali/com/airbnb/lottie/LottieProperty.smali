.class public interface abstract Lcom/airbnb/lottie/LottieProperty;
.super Ljava/lang/Object;
.source "LottieProperty.java"


# static fields
.field public static final O8:Ljava/lang/Integer;

.field public static final O8ooOoo〇:Ljava/lang/Float;

.field public static final OO0o〇〇:Ljava/lang/Float;

.field public static final OO0o〇〇〇〇0:Landroid/graphics/PointF;

.field public static final Oo08:Landroid/graphics/PointF;

.field public static final OoO8:Ljava/lang/Float;

.field public static final Oooo8o0〇:Ljava/lang/Float;

.field public static final O〇8O8〇008:Ljava/lang/Float;

.field public static final o800o8O:Ljava/lang/Float;

.field public static final oO80:Landroid/graphics/PointF;

.field public static final oo88o8O:Ljava/lang/Float;

.field public static final o〇0:Landroid/graphics/PointF;

.field public static final o〇O8〇〇o:Ljava/lang/Float;

.field public static final 〇00:Ljava/lang/Float;

.field public static final 〇0000OOO:[Ljava/lang/Integer;

.field public static final 〇080:Ljava/lang/Integer;

.field public static final 〇0〇O0088o:Ljava/lang/Float;

.field public static final 〇80〇808〇O:Ljava/lang/Float;

.field public static final 〇8o8o〇:Lcom/airbnb/lottie/value/ScaleXY;

.field public static final 〇O00:Ljava/lang/Float;

.field public static final 〇O888o0o:Ljava/lang/Float;

.field public static final 〇O8o08O:Ljava/lang/Float;

.field public static final 〇O〇:Ljava/lang/Float;

.field public static final 〇o00〇〇Oo:Ljava/lang/Integer;

.field public static final 〇oOO8O8:Landroid/graphics/ColorFilter;

.field public static final 〇oo〇:Ljava/lang/Float;

.field public static final 〇o〇:Ljava/lang/Integer;

.field public static final 〇〇808〇:Ljava/lang/Float;

.field public static final 〇〇888:Landroid/graphics/PointF;

.field public static final 〇〇8O0〇8:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇080:Ljava/lang/Integer;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇o00〇〇Oo:Ljava/lang/Integer;

    .line 14
    .line 15
    const/4 v0, 0x3

    .line 16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇o〇:Ljava/lang/Integer;

    .line 21
    .line 22
    const/4 v0, 0x4

    .line 23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->O8:Ljava/lang/Integer;

    .line 28
    .line 29
    new-instance v0, Landroid/graphics/PointF;

    .line 30
    .line 31
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 32
    .line 33
    .line 34
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->Oo08:Landroid/graphics/PointF;

    .line 35
    .line 36
    new-instance v0, Landroid/graphics/PointF;

    .line 37
    .line 38
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->o〇0:Landroid/graphics/PointF;

    .line 42
    .line 43
    new-instance v0, Landroid/graphics/PointF;

    .line 44
    .line 45
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 46
    .line 47
    .line 48
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇〇888:Landroid/graphics/PointF;

    .line 49
    .line 50
    new-instance v0, Landroid/graphics/PointF;

    .line 51
    .line 52
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 53
    .line 54
    .line 55
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->oO80:Landroid/graphics/PointF;

    .line 56
    .line 57
    const/4 v0, 0x0

    .line 58
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇80〇808〇O:Ljava/lang/Float;

    .line 63
    .line 64
    new-instance v1, Landroid/graphics/PointF;

    .line 65
    .line 66
    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 67
    .line 68
    .line 69
    sput-object v1, Lcom/airbnb/lottie/LottieProperty;->OO0o〇〇〇〇0:Landroid/graphics/PointF;

    .line 70
    .line 71
    new-instance v1, Lcom/airbnb/lottie/value/ScaleXY;

    .line 72
    .line 73
    invoke-direct {v1}, Lcom/airbnb/lottie/value/ScaleXY;-><init>()V

    .line 74
    .line 75
    .line 76
    sput-object v1, Lcom/airbnb/lottie/LottieProperty;->〇8o8o〇:Lcom/airbnb/lottie/value/ScaleXY;

    .line 77
    .line 78
    const/high16 v1, 0x3f800000    # 1.0f

    .line 79
    .line 80
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    sput-object v1, Lcom/airbnb/lottie/LottieProperty;->〇O8o08O:Ljava/lang/Float;

    .line 85
    .line 86
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->OO0o〇〇:Ljava/lang/Float;

    .line 87
    .line 88
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->Oooo8o0〇:Ljava/lang/Float;

    .line 89
    .line 90
    const/high16 v0, 0x40000000    # 2.0f

    .line 91
    .line 92
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇〇808〇:Ljava/lang/Float;

    .line 97
    .line 98
    const/high16 v0, 0x40400000    # 3.0f

    .line 99
    .line 100
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇O〇:Ljava/lang/Float;

    .line 105
    .line 106
    const/high16 v0, 0x40800000    # 4.0f

    .line 107
    .line 108
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇O00:Ljava/lang/Float;

    .line 113
    .line 114
    const/high16 v0, 0x40a00000    # 5.0f

    .line 115
    .line 116
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇〇8O0〇8:Ljava/lang/Float;

    .line 121
    .line 122
    const/high16 v0, 0x40c00000    # 6.0f

    .line 123
    .line 124
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇0〇O0088o:Ljava/lang/Float;

    .line 129
    .line 130
    const/high16 v0, 0x40e00000    # 7.0f

    .line 131
    .line 132
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->OoO8:Ljava/lang/Float;

    .line 137
    .line 138
    const/high16 v0, 0x41000000    # 8.0f

    .line 139
    .line 140
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->o800o8O:Ljava/lang/Float;

    .line 145
    .line 146
    const/high16 v0, 0x41100000    # 9.0f

    .line 147
    .line 148
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇O888o0o:Ljava/lang/Float;

    .line 153
    .line 154
    const/high16 v0, 0x41200000    # 10.0f

    .line 155
    .line 156
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->oo88o8O:Ljava/lang/Float;

    .line 161
    .line 162
    const/high16 v0, 0x41300000    # 11.0f

    .line 163
    .line 164
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇oo〇:Ljava/lang/Float;

    .line 169
    .line 170
    const/high16 v0, 0x41400000    # 12.0f

    .line 171
    .line 172
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 173
    .line 174
    .line 175
    move-result-object v0

    .line 176
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->o〇O8〇〇o:Ljava/lang/Float;

    .line 177
    .line 178
    const v0, 0x4141999a    # 12.1f

    .line 179
    .line 180
    .line 181
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 182
    .line 183
    .line 184
    move-result-object v0

    .line 185
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇00:Ljava/lang/Float;

    .line 186
    .line 187
    const/high16 v0, 0x41500000    # 13.0f

    .line 188
    .line 189
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->O〇8O8〇008:Ljava/lang/Float;

    .line 194
    .line 195
    const/high16 v0, 0x41600000    # 14.0f

    .line 196
    .line 197
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 198
    .line 199
    .line 200
    move-result-object v0

    .line 201
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->O8ooOoo〇:Ljava/lang/Float;

    .line 202
    .line 203
    new-instance v0, Landroid/graphics/ColorFilter;

    .line 204
    .line 205
    invoke-direct {v0}, Landroid/graphics/ColorFilter;-><init>()V

    .line 206
    .line 207
    .line 208
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇oOO8O8:Landroid/graphics/ColorFilter;

    .line 209
    .line 210
    const/4 v0, 0x0

    .line 211
    new-array v0, v0, [Ljava/lang/Integer;

    .line 212
    .line 213
    sput-object v0, Lcom/airbnb/lottie/LottieProperty;->〇0000OOO:[Ljava/lang/Integer;

    .line 214
    .line 215
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

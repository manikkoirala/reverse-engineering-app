.class public final Lcom/android/dx/dex/file/DexFile;
.super Ljava/lang/Object;
.source "DexFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dx/dex/file/DexFile$Storage;
    }
.end annotation


# instance fields
.field private final O8:Lcom/android/dx/dex/file/MixedItemSection;

.field private final OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

.field private final OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

.field private final Oo08:Lcom/android/dx/dex/file/MixedItemSection;

.field private final Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

.field private final oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

.field private final o〇0:Lcom/android/dx/dex/file/StringIdsSection;

.field private final 〇080:Lcom/android/dx/dex/DexOptions;

.field private 〇0〇O0088o:I

.field private final 〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

.field private final 〇8o8o〇:Lcom/android/dx/dex/file/ClassDefsSection;

.field private final 〇O00:[Lcom/android/dx/dex/file/Section;

.field private final 〇O8o08O:Lcom/android/dx/dex/file/MixedItemSection;

.field private final 〇O〇:Lcom/android/dx/dex/file/HeaderSection;

.field private final 〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

.field private final 〇o〇:Lcom/android/dx/dex/file/MixedItemSection;

.field private final 〇〇808〇:Lcom/android/dx/dex/file/MixedItemSection;

.field private final 〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

.field private 〇〇8O0〇8:I


# direct methods
.method public constructor <init>(Lcom/android/dx/dex/DexOptions;)V
    .locals 27

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    iput-object v1, v0, Lcom/android/dx/dex/file/DexFile;->〇080:Lcom/android/dx/dex/DexOptions;

    .line 9
    .line 10
    new-instance v2, Lcom/android/dx/dex/file/HeaderSection;

    .line 11
    .line 12
    invoke-direct {v2, v0}, Lcom/android/dx/dex/file/HeaderSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 13
    .line 14
    .line 15
    iput-object v2, v0, Lcom/android/dx/dex/file/DexFile;->〇O〇:Lcom/android/dx/dex/file/HeaderSection;

    .line 16
    .line 17
    new-instance v3, Lcom/android/dx/dex/file/MixedItemSection;

    .line 18
    .line 19
    sget-object v4, Lcom/android/dx/dex/file/MixedItemSection$SortType;->o0:Lcom/android/dx/dex/file/MixedItemSection$SortType;

    .line 20
    .line 21
    const/4 v5, 0x0

    .line 22
    const/4 v6, 0x4

    .line 23
    invoke-direct {v3, v5, v0, v6, v4}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 24
    .line 25
    .line 26
    iput-object v3, v0, Lcom/android/dx/dex/file/DexFile;->〇o〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 27
    .line 28
    new-instance v7, Lcom/android/dx/dex/file/MixedItemSection;

    .line 29
    .line 30
    sget-object v8, Lcom/android/dx/dex/file/MixedItemSection$SortType;->〇OOo8〇0:Lcom/android/dx/dex/file/MixedItemSection$SortType;

    .line 31
    .line 32
    const-string v9, "word_data"

    .line 33
    .line 34
    invoke-direct {v7, v9, v0, v6, v8}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 35
    .line 36
    .line 37
    iput-object v7, v0, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

    .line 38
    .line 39
    new-instance v9, Lcom/android/dx/dex/file/MixedItemSection;

    .line 40
    .line 41
    sget-object v10, Lcom/android/dx/dex/file/MixedItemSection$SortType;->OO:Lcom/android/dx/dex/file/MixedItemSection$SortType;

    .line 42
    .line 43
    const-string v11, "string_data"

    .line 44
    .line 45
    const/4 v12, 0x1

    .line 46
    invoke-direct {v9, v11, v0, v12, v10}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 47
    .line 48
    .line 49
    iput-object v9, v0, Lcom/android/dx/dex/file/DexFile;->Oo08:Lcom/android/dx/dex/file/MixedItemSection;

    .line 50
    .line 51
    new-instance v10, Lcom/android/dx/dex/file/MixedItemSection;

    .line 52
    .line 53
    invoke-direct {v10, v5, v0, v12, v4}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 54
    .line 55
    .line 56
    iput-object v10, v0, Lcom/android/dx/dex/file/DexFile;->〇O8o08O:Lcom/android/dx/dex/file/MixedItemSection;

    .line 57
    .line 58
    new-instance v11, Lcom/android/dx/dex/file/MixedItemSection;

    .line 59
    .line 60
    const-string v13, "byte_data"

    .line 61
    .line 62
    invoke-direct {v11, v13, v0, v12, v8}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 63
    .line 64
    .line 65
    iput-object v11, v0, Lcom/android/dx/dex/file/DexFile;->〇〇808〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 66
    .line 67
    new-instance v8, Lcom/android/dx/dex/file/StringIdsSection;

    .line 68
    .line 69
    invoke-direct {v8, v0}, Lcom/android/dx/dex/file/StringIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 70
    .line 71
    .line 72
    iput-object v8, v0, Lcom/android/dx/dex/file/DexFile;->o〇0:Lcom/android/dx/dex/file/StringIdsSection;

    .line 73
    .line 74
    new-instance v13, Lcom/android/dx/dex/file/TypeIdsSection;

    .line 75
    .line 76
    invoke-direct {v13, v0}, Lcom/android/dx/dex/file/TypeIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 77
    .line 78
    .line 79
    iput-object v13, v0, Lcom/android/dx/dex/file/DexFile;->〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

    .line 80
    .line 81
    new-instance v14, Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 82
    .line 83
    invoke-direct {v14, v0}, Lcom/android/dx/dex/file/ProtoIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 84
    .line 85
    .line 86
    iput-object v14, v0, Lcom/android/dx/dex/file/DexFile;->oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 87
    .line 88
    new-instance v15, Lcom/android/dx/dex/file/FieldIdsSection;

    .line 89
    .line 90
    invoke-direct {v15, v0}, Lcom/android/dx/dex/file/FieldIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 91
    .line 92
    .line 93
    iput-object v15, v0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 94
    .line 95
    new-instance v5, Lcom/android/dx/dex/file/MethodIdsSection;

    .line 96
    .line 97
    invoke-direct {v5, v0}, Lcom/android/dx/dex/file/MethodIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 98
    .line 99
    .line 100
    iput-object v5, v0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

    .line 101
    .line 102
    new-instance v12, Lcom/android/dx/dex/file/ClassDefsSection;

    .line 103
    .line 104
    invoke-direct {v12, v0}, Lcom/android/dx/dex/file/ClassDefsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 105
    .line 106
    .line 107
    iput-object v12, v0, Lcom/android/dx/dex/file/DexFile;->〇8o8o〇:Lcom/android/dx/dex/file/ClassDefsSection;

    .line 108
    .line 109
    move-object/from16 v16, v10

    .line 110
    .line 111
    new-instance v10, Lcom/android/dx/dex/file/MixedItemSection;

    .line 112
    .line 113
    move-object/from16 v17, v11

    .line 114
    .line 115
    const-string v11, "map"

    .line 116
    .line 117
    invoke-direct {v10, v11, v0, v6, v4}, Lcom/android/dx/dex/file/MixedItemSection;-><init>(Ljava/lang/String;Lcom/android/dx/dex/file/DexFile;ILcom/android/dx/dex/file/MixedItemSection$SortType;)V

    .line 118
    .line 119
    .line 120
    iput-object v10, v0, Lcom/android/dx/dex/file/DexFile;->O8:Lcom/android/dx/dex/file/MixedItemSection;

    .line 121
    .line 122
    const/16 v4, 0x1a

    .line 123
    .line 124
    invoke-virtual {v1, v4}, Lcom/android/dx/dex/DexOptions;->〇080(I)Z

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    const/16 v18, 0xa

    .line 129
    .line 130
    const/16 v19, 0x9

    .line 131
    .line 132
    const/16 v20, 0x8

    .line 133
    .line 134
    const/16 v21, 0x7

    .line 135
    .line 136
    const/16 v22, 0x6

    .line 137
    .line 138
    const/16 v23, 0x5

    .line 139
    .line 140
    const/16 v24, 0x3

    .line 141
    .line 142
    const/16 v25, 0x2

    .line 143
    .line 144
    const/16 v26, 0x0

    .line 145
    .line 146
    if-eqz v1, :cond_0

    .line 147
    .line 148
    new-instance v1, Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 149
    .line 150
    invoke-direct {v1, v0}, Lcom/android/dx/dex/file/CallSiteIdsSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 151
    .line 152
    .line 153
    iput-object v1, v0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 154
    .line 155
    new-instance v4, Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 156
    .line 157
    invoke-direct {v4, v0}, Lcom/android/dx/dex/file/MethodHandlesSection;-><init>(Lcom/android/dx/dex/file/DexFile;)V

    .line 158
    .line 159
    .line 160
    iput-object v4, v0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 161
    .line 162
    const/16 v11, 0xf

    .line 163
    .line 164
    new-array v11, v11, [Lcom/android/dx/dex/file/Section;

    .line 165
    .line 166
    aput-object v2, v11, v26

    .line 167
    .line 168
    const/4 v2, 0x1

    .line 169
    aput-object v8, v11, v2

    .line 170
    .line 171
    aput-object v13, v11, v25

    .line 172
    .line 173
    aput-object v14, v11, v24

    .line 174
    .line 175
    aput-object v15, v11, v6

    .line 176
    .line 177
    aput-object v5, v11, v23

    .line 178
    .line 179
    aput-object v12, v11, v22

    .line 180
    .line 181
    aput-object v1, v11, v21

    .line 182
    .line 183
    aput-object v4, v11, v20

    .line 184
    .line 185
    aput-object v7, v11, v19

    .line 186
    .line 187
    aput-object v3, v11, v18

    .line 188
    .line 189
    const/16 v1, 0xb

    .line 190
    .line 191
    aput-object v9, v11, v1

    .line 192
    .line 193
    const/16 v1, 0xc

    .line 194
    .line 195
    aput-object v17, v11, v1

    .line 196
    .line 197
    const/16 v1, 0xd

    .line 198
    .line 199
    aput-object v16, v11, v1

    .line 200
    .line 201
    const/16 v1, 0xe

    .line 202
    .line 203
    aput-object v10, v11, v1

    .line 204
    .line 205
    iput-object v11, v0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 206
    .line 207
    goto :goto_0

    .line 208
    :cond_0
    const/16 v1, 0xd

    .line 209
    .line 210
    const/4 v4, 0x0

    .line 211
    iput-object v4, v0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 212
    .line 213
    iput-object v4, v0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 214
    .line 215
    new-array v1, v1, [Lcom/android/dx/dex/file/Section;

    .line 216
    .line 217
    aput-object v2, v1, v26

    .line 218
    .line 219
    const/4 v2, 0x1

    .line 220
    aput-object v8, v1, v2

    .line 221
    .line 222
    aput-object v13, v1, v25

    .line 223
    .line 224
    aput-object v14, v1, v24

    .line 225
    .line 226
    aput-object v15, v1, v6

    .line 227
    .line 228
    aput-object v5, v1, v23

    .line 229
    .line 230
    aput-object v12, v1, v22

    .line 231
    .line 232
    aput-object v7, v1, v21

    .line 233
    .line 234
    aput-object v3, v1, v20

    .line 235
    .line 236
    aput-object v9, v1, v19

    .line 237
    .line 238
    aput-object v17, v1, v18

    .line 239
    .line 240
    const/16 v2, 0xb

    .line 241
    .line 242
    aput-object v16, v1, v2

    .line 243
    .line 244
    const/16 v2, 0xc

    .line 245
    .line 246
    aput-object v10, v1, v2

    .line 247
    .line 248
    iput-object v1, v0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 249
    .line 250
    :goto_0
    const/4 v1, -0x1

    .line 251
    iput v1, v0, Lcom/android/dx/dex/file/DexFile;->〇〇8O0〇8:I

    .line 252
    .line 253
    const/16 v1, 0x4f

    .line 254
    .line 255
    iput v1, v0, Lcom/android/dx/dex/file/DexFile;->〇0〇O0088o:I

    .line 256
    .line 257
    return-void
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
.end method

.method private 〇00(ZZLcom/android/dx/dex/file/DexFile$Storage;)Lcom/android/dx/util/ByteArrayAnnotatedOutput;
    .locals 7

    .line 1
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇8o8o〇:Lcom/android/dx/dex/file/ClassDefsSection;

    .line 2
    .line 3
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 4
    .line 5
    .line 6
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇O8o08O:Lcom/android/dx/dex/file/MixedItemSection;

    .line 7
    .line 8
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 9
    .line 10
    .line 11
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

    .line 12
    .line 13
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 14
    .line 15
    .line 16
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇080:Lcom/android/dx/dex/DexOptions;

    .line 17
    .line 18
    const/16 v0, 0x1a

    .line 19
    .line 20
    invoke-virtual {p3, v0}, Lcom/android/dx/dex/DexOptions;->〇080(I)Z

    .line 21
    .line 22
    .line 23
    move-result p3

    .line 24
    if-eqz p3, :cond_0

    .line 25
    .line 26
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 27
    .line 28
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇〇808〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 32
    .line 33
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 34
    .line 35
    .line 36
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇080:Lcom/android/dx/dex/DexOptions;

    .line 37
    .line 38
    invoke-virtual {p3, v0}, Lcom/android/dx/dex/DexOptions;->〇080(I)Z

    .line 39
    .line 40
    .line 41
    move-result p3

    .line 42
    if-eqz p3, :cond_1

    .line 43
    .line 44
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 45
    .line 46
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 47
    .line 48
    .line 49
    :cond_1
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

    .line 50
    .line 51
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 52
    .line 53
    .line 54
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 55
    .line 56
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 57
    .line 58
    .line 59
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 60
    .line 61
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 62
    .line 63
    .line 64
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇o〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 65
    .line 66
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 67
    .line 68
    .line 69
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

    .line 70
    .line 71
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 72
    .line 73
    .line 74
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->o〇0:Lcom/android/dx/dex/file/StringIdsSection;

    .line 75
    .line 76
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 77
    .line 78
    .line 79
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->Oo08:Lcom/android/dx/dex/file/MixedItemSection;

    .line 80
    .line 81
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 82
    .line 83
    .line 84
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇O〇:Lcom/android/dx/dex/file/HeaderSection;

    .line 85
    .line 86
    invoke-virtual {p3}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 87
    .line 88
    .line 89
    iget-object p3, p0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 90
    .line 91
    array-length p3, p3

    .line 92
    const/4 v0, 0x0

    .line 93
    const/4 v1, 0x0

    .line 94
    const/4 v2, 0x0

    .line 95
    :goto_0
    const-string v3, "...while writing section "

    .line 96
    .line 97
    if-ge v1, p3, :cond_7

    .line 98
    .line 99
    iget-object v4, p0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 100
    .line 101
    aget-object v4, v4, v1

    .line 102
    .line 103
    iget-object v5, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 104
    .line 105
    if-eq v4, v5, :cond_2

    .line 106
    .line 107
    iget-object v5, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 108
    .line 109
    if-ne v4, v5, :cond_3

    .line 110
    .line 111
    :cond_2
    invoke-virtual {v4}, Lcom/android/dx/dex/file/Section;->〇〇888()Ljava/util/Collection;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 116
    .line 117
    .line 118
    move-result v5

    .line 119
    if-eqz v5, :cond_3

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_3
    invoke-virtual {v4, v2}, Lcom/android/dx/dex/file/Section;->OO0o〇〇〇〇0(I)I

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    if-lt v5, v2, :cond_6

    .line 127
    .line 128
    :try_start_0
    iget-object v2, p0, Lcom/android/dx/dex/file/DexFile;->O8:Lcom/android/dx/dex/file/MixedItemSection;

    .line 129
    .line 130
    if-ne v4, v2, :cond_4

    .line 131
    .line 132
    iget-object v6, p0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 133
    .line 134
    invoke-static {v6, v2}, Lcom/android/dx/dex/file/MapItem;->〇O00([Lcom/android/dx/dex/file/Section;Lcom/android/dx/dex/file/MixedItemSection;)V

    .line 135
    .line 136
    .line 137
    iget-object v2, p0, Lcom/android/dx/dex/file/DexFile;->O8:Lcom/android/dx/dex/file/MixedItemSection;

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/android/dx/dex/file/Section;->oO80()V

    .line 140
    .line 141
    .line 142
    :cond_4
    instance-of v2, v4, Lcom/android/dx/dex/file/MixedItemSection;

    .line 143
    .line 144
    if-eqz v2, :cond_5

    .line 145
    .line 146
    move-object v2, v4

    .line 147
    check-cast v2, Lcom/android/dx/dex/file/MixedItemSection;

    .line 148
    .line 149
    invoke-virtual {v2}, Lcom/android/dx/dex/file/MixedItemSection;->〇0〇O0088o()V

    .line 150
    .line 151
    .line 152
    :cond_5
    invoke-virtual {v4}, Lcom/android/dx/dex/file/Section;->Oooo8o0〇()I

    .line 153
    .line 154
    .line 155
    move-result v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    add-int/2addr v2, v5

    .line 157
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 158
    .line 159
    goto :goto_0

    .line 160
    :catch_0
    move-exception p1

    .line 161
    new-instance p2, Ljava/lang/StringBuilder;

    .line 162
    .line 163
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object p2

    .line 176
    invoke-static {p1, p2}, Lcom/android/dex/util/ExceptionWithContext;->withContext(Ljava/lang/Throwable;Ljava/lang/String;)Lcom/android/dex/util/ExceptionWithContext;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    throw p1

    .line 181
    :cond_6
    new-instance p1, Ljava/lang/RuntimeException;

    .line 182
    .line 183
    new-instance p2, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string p3, "bogus placement for section "

    .line 189
    .line 190
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object p2

    .line 200
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    throw p1

    .line 204
    :cond_7
    iput v2, p0, Lcom/android/dx/dex/file/DexFile;->〇〇8O0〇8:I

    .line 205
    .line 206
    new-array v1, v2, [B

    .line 207
    .line 208
    new-instance v2, Lcom/android/dx/util/ByteArrayAnnotatedOutput;

    .line 209
    .line 210
    invoke-direct {v2, v1}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;-><init>([B)V

    .line 211
    .line 212
    .line 213
    if-eqz p1, :cond_8

    .line 214
    .line 215
    iget v4, p0, Lcom/android/dx/dex/file/DexFile;->〇0〇O0088o:I

    .line 216
    .line 217
    invoke-virtual {v2, v4, p2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->〇O8o08O(IZ)V

    .line 218
    .line 219
    .line 220
    :cond_8
    :goto_2
    if-ge v0, p3, :cond_d

    .line 221
    .line 222
    :try_start_1
    iget-object p2, p0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 223
    .line 224
    aget-object p2, p2, v0

    .line 225
    .line 226
    iget-object v4, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 227
    .line 228
    if-eq p2, v4, :cond_9

    .line 229
    .line 230
    iget-object v4, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 231
    .line 232
    if-ne p2, v4, :cond_a

    .line 233
    .line 234
    :cond_9
    invoke-virtual {p2}, Lcom/android/dx/dex/file/Section;->〇〇888()Ljava/util/Collection;

    .line 235
    .line 236
    .line 237
    move-result-object v4

    .line 238
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    .line 239
    .line 240
    .line 241
    move-result v4

    .line 242
    if-eqz v4, :cond_a

    .line 243
    .line 244
    goto :goto_3

    .line 245
    :cond_a
    invoke-virtual {p2}, Lcom/android/dx/dex/file/Section;->o〇0()I

    .line 246
    .line 247
    .line 248
    move-result v4

    .line 249
    invoke-virtual {v2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->getCursor()I

    .line 250
    .line 251
    .line 252
    move-result v5

    .line 253
    sub-int/2addr v4, v5

    .line 254
    if-ltz v4, :cond_b

    .line 255
    .line 256
    invoke-virtual {v2, v4}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->〇80〇808〇O(I)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {p2, v2}, Lcom/android/dx/dex/file/Section;->〇〇808〇(Lcom/android/dx/util/AnnotatedOutput;)V

    .line 260
    .line 261
    .line 262
    :goto_3
    add-int/lit8 v0, v0, 0x1

    .line 263
    .line 264
    goto :goto_2

    .line 265
    :cond_b
    new-instance p1, Lcom/android/dex/util/ExceptionWithContext;

    .line 266
    .line 267
    new-instance p2, Ljava/lang/StringBuilder;

    .line 268
    .line 269
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    .line 271
    .line 272
    const-string p3, "excess write of "

    .line 273
    .line 274
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    .line 276
    .line 277
    neg-int p3, v4

    .line 278
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 279
    .line 280
    .line 281
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object p2

    .line 285
    invoke-direct {p1, p2}, Lcom/android/dex/util/ExceptionWithContext;-><init>(Ljava/lang/String;)V

    .line 286
    .line 287
    .line 288
    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 289
    :catch_1
    move-exception p1

    .line 290
    instance-of p2, p1, Lcom/android/dex/util/ExceptionWithContext;

    .line 291
    .line 292
    if-eqz p2, :cond_c

    .line 293
    .line 294
    check-cast p1, Lcom/android/dex/util/ExceptionWithContext;

    .line 295
    .line 296
    goto :goto_4

    .line 297
    :cond_c
    new-instance p2, Lcom/android/dex/util/ExceptionWithContext;

    .line 298
    .line 299
    invoke-direct {p2, p1}, Lcom/android/dex/util/ExceptionWithContext;-><init>(Ljava/lang/Throwable;)V

    .line 300
    .line 301
    .line 302
    move-object p1, p2

    .line 303
    :goto_4
    new-instance p2, Ljava/lang/StringBuilder;

    .line 304
    .line 305
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    .line 307
    .line 308
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object p2

    .line 318
    invoke-virtual {p1, p2}, Lcom/android/dex/util/ExceptionWithContext;->addContext(Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    throw p1

    .line 322
    :cond_d
    invoke-virtual {v2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->getCursor()I

    .line 323
    .line 324
    .line 325
    move-result p2

    .line 326
    iget p3, p0, Lcom/android/dx/dex/file/DexFile;->〇〇8O0〇8:I

    .line 327
    .line 328
    if-ne p2, p3, :cond_f

    .line 329
    .line 330
    invoke-virtual {v2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->getCursor()I

    .line 331
    .line 332
    .line 333
    move-result p2

    .line 334
    invoke-static {v1, p2}, Lcom/android/dx/dex/file/DexFile;->〇o〇([BI)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {v2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->getCursor()I

    .line 338
    .line 339
    .line 340
    move-result p2

    .line 341
    invoke-static {v1, p2}, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo([BI)V

    .line 342
    .line 343
    .line 344
    if-eqz p1, :cond_e

    .line 345
    .line 346
    iget-object p1, p0, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

    .line 347
    .line 348
    sget-object p2, Lcom/android/dx/dex/file/ItemType;->TYPE_CODE_ITEM:Lcom/android/dx/dex/file/ItemType;

    .line 349
    .line 350
    const-string p3, "\nmethod code index:\n\n"

    .line 351
    .line 352
    invoke-virtual {p1, v2, p2, p3}, Lcom/android/dx/dex/file/MixedItemSection;->OoO8(Lcom/android/dx/util/AnnotatedOutput;Lcom/android/dx/dex/file/ItemType;Ljava/lang/String;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {p0}, Lcom/android/dx/dex/file/DexFile;->〇〇8O0〇8()Lcom/android/dx/dex/file/Statistics;

    .line 356
    .line 357
    .line 358
    move-result-object p1

    .line 359
    invoke-virtual {p1, v2}, Lcom/android/dx/dex/file/Statistics;->〇o〇(Lcom/android/dx/util/AnnotatedOutput;)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {v2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->Oooo8o0〇()V

    .line 363
    .line 364
    .line 365
    :cond_e
    return-object v2

    .line 366
    :cond_f
    new-instance p1, Ljava/lang/RuntimeException;

    .line 367
    .line 368
    const-string p2, "foreshortened write"

    .line 369
    .line 370
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    throw p1
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
.end method

.method private static 〇o00〇〇Oo([BI)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/zip/Adler32;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/zip/Adler32;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0xc

    .line 7
    .line 8
    sub-int/2addr p1, v1

    .line 9
    invoke-virtual {v0, p0, v1, p1}, Ljava/util/zip/Adler32;->update([BII)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/util/zip/Adler32;->getValue()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    long-to-int p1, v0

    .line 17
    const/16 v0, 0x8

    .line 18
    .line 19
    int-to-byte v1, p1

    .line 20
    aput-byte v1, p0, v0

    .line 21
    .line 22
    shr-int/lit8 v0, p1, 0x8

    .line 23
    .line 24
    int-to-byte v0, v0

    .line 25
    const/16 v1, 0x9

    .line 26
    .line 27
    aput-byte v0, p0, v1

    .line 28
    .line 29
    shr-int/lit8 v0, p1, 0x10

    .line 30
    .line 31
    int-to-byte v0, v0

    .line 32
    const/16 v1, 0xa

    .line 33
    .line 34
    aput-byte v0, p0, v1

    .line 35
    .line 36
    shr-int/lit8 p1, p1, 0x18

    .line 37
    .line 38
    int-to-byte p1, p1

    .line 39
    const/16 v0, 0xb

    .line 40
    .line 41
    aput-byte p1, p0, v0

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private static 〇o〇([BI)V
    .locals 2

    .line 1
    :try_start_0
    const-string v0, "SHA-1"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    .line 4
    .line 5
    .line 6
    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    .line 7
    const/16 v1, 0x20

    .line 8
    .line 9
    sub-int/2addr p1, v1

    .line 10
    invoke-virtual {v0, p0, v1, p1}, Ljava/security/MessageDigest;->update([BII)V

    .line 11
    .line 12
    .line 13
    const/16 p1, 0xc

    .line 14
    .line 15
    const/16 v1, 0x14

    .line 16
    .line 17
    :try_start_1
    invoke-virtual {v0, p0, p1, v1}, Ljava/security/MessageDigest;->digest([BII)I

    .line 18
    .line 19
    .line 20
    move-result p0

    .line 21
    if-ne p0, v1, :cond_0

    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 25
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "unexpected digest write: "

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string p0, " bytes"

    .line 40
    .line 41
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    throw p1
    :try_end_1
    .catch Ljava/security/DigestException; {:try_start_1 .. :try_end_1} :catch_0

    .line 52
    :catch_0
    move-exception p0

    .line 53
    new-instance p1, Ljava/lang/RuntimeException;

    .line 54
    .line 55
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    throw p1

    .line 59
    :catch_1
    move-exception p0

    .line 60
    new-instance p1, Ljava/lang/RuntimeException;

    .line 61
    .line 62
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 63
    .line 64
    .line 65
    throw p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method


# virtual methods
.method O8(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstString;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->o〇0:Lcom/android/dx/dex/file/StringIdsSection;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/StringIdsSection;->〇〇8O0〇8(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstType;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/TypeIdsSection;->〇〇8O0〇8(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    return-object p1

    .line 23
    :cond_1
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstBaseMethodRef;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

    .line 28
    .line 29
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/MethodIdsSection;->〇0〇O0088o(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1

    .line 34
    :cond_2
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstFieldRef;

    .line 35
    .line 36
    if-eqz v0, :cond_3

    .line 37
    .line 38
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 39
    .line 40
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/FieldIdsSection;->〇0〇O0088o(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    return-object p1

    .line 45
    :cond_3
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstEnumRef;

    .line 46
    .line 47
    if-eqz v0, :cond_4

    .line 48
    .line 49
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 50
    .line 51
    check-cast p1, Lcom/android/dx/rop/cst/CstEnumRef;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/android/dx/rop/cst/CstEnumRef;->oO80()Lcom/android/dx/rop/cst/CstFieldRef;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/FieldIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstFieldRef;)Lcom/android/dx/dex/file/FieldIdItem;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    return-object p1

    .line 62
    :cond_4
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstProtoRef;

    .line 63
    .line 64
    if-eqz v0, :cond_5

    .line 65
    .line 66
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 67
    .line 68
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/ProtoIdsSection;->〇〇8O0〇8(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    return-object p1

    .line 73
    :cond_5
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstMethodHandle;

    .line 74
    .line 75
    if-eqz v0, :cond_6

    .line 76
    .line 77
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 78
    .line 79
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/MethodHandlesSection;->〇〇8O0〇8(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    return-object p1

    .line 84
    :cond_6
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstCallSiteRef;

    .line 85
    .line 86
    if-eqz v0, :cond_7

    .line 87
    .line 88
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 89
    .line 90
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/CallSiteIdsSection;->〇0〇O0088o(Lcom/android/dx/rop/cst/Constant;)Lcom/android/dx/dex/file/IndexedItem;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    return-object p1

    .line 95
    :cond_7
    const/4 p1, 0x0

    .line 96
    return-object p1
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method OO0o〇〇()Lcom/android/dx/dex/file/Section;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->O8:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0()Lcom/android/dx/dex/file/FieldIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method Oo08()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇〇808〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method OoO8()Lcom/android/dx/dex/file/StringIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->o〇0:Lcom/android/dx/dex/file/StringIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method Oooo8o0〇()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->O8:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O()Lcom/android/dx/dex/file/TypeIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()Lcom/android/dx/dex/file/ClassDefsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇8o8o〇:Lcom/android/dx/dex/file/ClassDefsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method oo88o8O()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()Lcom/android/dx/dex/file/CallSiteIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇:Lcom/android/dx/dex/file/CallSiteIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O8〇〇o(Ljava/io/Writer;Z)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    const/4 v0, 0x0

    .line 6
    :goto_0
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0, v0, p2, v1}, Lcom/android/dx/dex/file/DexFile;->〇00(ZZLcom/android/dx/dex/file/DexFile$Storage;)Lcom/android/dx/util/ByteArrayAnnotatedOutput;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p2, p1}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->〇0〇O0088o(Ljava/io/Writer;)V

    .line 14
    .line 15
    .line 16
    :cond_1
    invoke-virtual {p2}, Lcom/android/dx/util/ByteArrayAnnotatedOutput;->〇〇808〇()[B

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public 〇080(Lcom/android/dx/dex/file/ClassDefItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇8o8o〇:Lcom/android/dx/dex/file/ClassDefsSection;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/ClassDefsSection;->〇〇8O0〇8(Lcom/android/dx/dex/file/ClassDefItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method 〇0〇O0088o()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->Oo08:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80〇808〇O()Lcom/android/dx/dex/DexOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇080:Lcom/android/dx/dex/DexOptions;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8o8o〇()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/android/dx/dex/file/DexFile;->〇〇8O0〇8:I

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    return v0

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 7
    .line 8
    const-string v1, "file size not yet known"

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇O00()Lcom/android/dx/dex/file/ProtoIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇O888o0o()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇o〇:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇O8o08O()Lcom/android/dx/dex/file/Section;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇o00〇〇Oo:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇()Lcom/android/dx/dex/file/MethodIdsSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇oo〇(Lcom/android/dx/rop/cst/Constant;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_7

    .line 2
    .line 3
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstString;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->o〇0:Lcom/android/dx/dex/file/StringIdsSection;

    .line 8
    .line 9
    check-cast p1, Lcom/android/dx/rop/cst/CstString;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/StringIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstString;)Lcom/android/dx/dex/file/StringIdItem;

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstType;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇〇888:Lcom/android/dx/dex/file/TypeIdsSection;

    .line 20
    .line 21
    check-cast p1, Lcom/android/dx/rop/cst/CstType;

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/TypeIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstType;)Lcom/android/dx/dex/file/TypeIdItem;

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstBaseMethodRef;

    .line 28
    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->OO0o〇〇〇〇0:Lcom/android/dx/dex/file/MethodIdsSection;

    .line 32
    .line 33
    check-cast p1, Lcom/android/dx/rop/cst/CstBaseMethodRef;

    .line 34
    .line 35
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/MethodIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstBaseMethodRef;)Lcom/android/dx/dex/file/MethodIdItem;

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstFieldRef;

    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 44
    .line 45
    check-cast p1, Lcom/android/dx/rop/cst/CstFieldRef;

    .line 46
    .line 47
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/FieldIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstFieldRef;)Lcom/android/dx/dex/file/FieldIdItem;

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_3
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstEnumRef;

    .line 52
    .line 53
    if-eqz v0, :cond_4

    .line 54
    .line 55
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇80〇808〇O:Lcom/android/dx/dex/file/FieldIdsSection;

    .line 56
    .line 57
    check-cast p1, Lcom/android/dx/rop/cst/CstEnumRef;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/android/dx/rop/cst/CstEnumRef;->oO80()Lcom/android/dx/rop/cst/CstFieldRef;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/FieldIdsSection;->o800o8O(Lcom/android/dx/rop/cst/CstFieldRef;)Lcom/android/dx/dex/file/FieldIdItem;

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_4
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstProtoRef;

    .line 68
    .line 69
    if-eqz v0, :cond_5

    .line 70
    .line 71
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->oO80:Lcom/android/dx/dex/file/ProtoIdsSection;

    .line 72
    .line 73
    check-cast p1, Lcom/android/dx/rop/cst/CstProtoRef;

    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/android/dx/rop/cst/CstProtoRef;->o〇0()Lcom/android/dx/rop/type/Prototype;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/ProtoIdsSection;->OoO8(Lcom/android/dx/rop/type/Prototype;)Lcom/android/dx/dex/file/ProtoIdItem;

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_5
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstMethodHandle;

    .line 84
    .line 85
    if-eqz v0, :cond_6

    .line 86
    .line 87
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 88
    .line 89
    check-cast p1, Lcom/android/dx/rop/cst/CstMethodHandle;

    .line 90
    .line 91
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/file/MethodHandlesSection;->OoO8(Lcom/android/dx/rop/cst/CstMethodHandle;)V

    .line 92
    .line 93
    .line 94
    :cond_6
    :goto_0
    return-void

    .line 95
    :cond_7
    new-instance p1, Ljava/lang/NullPointerException;

    .line 96
    .line 97
    const-string v0, "cst == null"

    .line 98
    .line 99
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    throw p1
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method public 〇〇808〇()Lcom/android/dx/dex/file/MethodHandlesSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->Oooo8o0〇:Lcom/android/dx/dex/file/MethodHandlesSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇〇888()Lcom/android/dx/dex/file/MixedItemSection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/file/DexFile;->〇O8o08O:Lcom/android/dx/dex/file/MixedItemSection;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇8O0〇8()Lcom/android/dx/dex/file/Statistics;
    .locals 5

    .line 1
    new-instance v0, Lcom/android/dx/dex/file/Statistics;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/android/dx/dex/file/Statistics;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/android/dx/dex/file/DexFile;->〇O00:[Lcom/android/dx/dex/file/Section;

    .line 7
    .line 8
    array-length v2, v1

    .line 9
    const/4 v3, 0x0

    .line 10
    :goto_0
    if-ge v3, v2, :cond_0

    .line 11
    .line 12
    aget-object v4, v1, v3

    .line 13
    .line 14
    invoke-virtual {v0, v4}, Lcom/android/dx/dex/file/Statistics;->〇o00〇〇Oo(Lcom/android/dx/dex/file/Section;)V

    .line 15
    .line 16
    .line 17
    add-int/lit8 v3, v3, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    return-object v0
.end method

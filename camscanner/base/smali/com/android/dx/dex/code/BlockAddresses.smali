.class public final Lcom/android/dx/dex/code/BlockAddresses;
.super Ljava/lang/Object;
.source "BlockAddresses.java"


# instance fields
.field private final 〇080:[Lcom/android/dx/dex/code/CodeAddress;

.field private final 〇o00〇〇Oo:[Lcom/android/dx/dex/code/CodeAddress;

.field private final 〇o〇:[Lcom/android/dx/dex/code/CodeAddress;


# direct methods
.method public constructor <init>(Lcom/android/dx/rop/code/RopMethod;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/android/dx/util/LabeledList;->〇O〇()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    new-array v1, v0, [Lcom/android/dx/dex/code/CodeAddress;

    .line 13
    .line 14
    iput-object v1, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇080:[Lcom/android/dx/dex/code/CodeAddress;

    .line 15
    .line 16
    new-array v1, v0, [Lcom/android/dx/dex/code/CodeAddress;

    .line 17
    .line 18
    iput-object v1, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o00〇〇Oo:[Lcom/android/dx/dex/code/CodeAddress;

    .line 19
    .line 20
    new-array v0, v0, [Lcom/android/dx/dex/code/CodeAddress;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o〇:[Lcom/android/dx/dex/code/CodeAddress;

    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/android/dx/dex/code/BlockAddresses;->Oo08(Lcom/android/dx/rop/code/RopMethod;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private Oo08(Lcom/android/dx/rop/code/RopMethod;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/android/dx/util/FixedSizeList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    :goto_0
    if-ge v2, v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1, v2}, Lcom/android/dx/rop/code/BasicBlockList;->o800o8O(I)Lcom/android/dx/rop/code/BasicBlock;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    invoke-virtual {v3}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    invoke-virtual {v3}, Lcom/android/dx/rop/code/BasicBlock;->〇o00〇〇Oo()Lcom/android/dx/rop/code/InsnList;

    .line 22
    .line 23
    .line 24
    move-result-object v5

    .line 25
    invoke-virtual {v5, v1}, Lcom/android/dx/rop/code/InsnList;->〇O〇(I)Lcom/android/dx/rop/code/Insn;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    iget-object v6, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇080:[Lcom/android/dx/dex/code/CodeAddress;

    .line 30
    .line 31
    new-instance v7, Lcom/android/dx/dex/code/CodeAddress;

    .line 32
    .line 33
    invoke-virtual {v5}, Lcom/android/dx/rop/code/Insn;->oO80()Lcom/android/dx/rop/code/SourcePosition;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-direct {v7, v5}, Lcom/android/dx/dex/code/CodeAddress;-><init>(Lcom/android/dx/rop/code/SourcePosition;)V

    .line 38
    .line 39
    .line 40
    aput-object v7, v6, v4

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/android/dx/rop/code/BasicBlock;->〇o〇()Lcom/android/dx/rop/code/Insn;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-virtual {v3}, Lcom/android/dx/rop/code/Insn;->oO80()Lcom/android/dx/rop/code/SourcePosition;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    iget-object v5, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o00〇〇Oo:[Lcom/android/dx/dex/code/CodeAddress;

    .line 51
    .line 52
    new-instance v6, Lcom/android/dx/dex/code/CodeAddress;

    .line 53
    .line 54
    invoke-direct {v6, v3}, Lcom/android/dx/dex/code/CodeAddress;-><init>(Lcom/android/dx/rop/code/SourcePosition;)V

    .line 55
    .line 56
    .line 57
    aput-object v6, v5, v4

    .line 58
    .line 59
    iget-object v5, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o〇:[Lcom/android/dx/dex/code/CodeAddress;

    .line 60
    .line 61
    new-instance v6, Lcom/android/dx/dex/code/CodeAddress;

    .line 62
    .line 63
    invoke-direct {v6, v3}, Lcom/android/dx/dex/code/CodeAddress;-><init>(Lcom/android/dx/rop/code/SourcePosition;)V

    .line 64
    .line 65
    .line 66
    aput-object v6, v5, v4

    .line 67
    .line 68
    add-int/lit8 v2, v2, 0x1

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_0
    return-void
    .line 72
    .line 73
    .line 74
.end method


# virtual methods
.method public O8(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇080:[Lcom/android/dx/dex/code/CodeAddress;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget-object p1, v0, p1

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇080(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o〇:[Lcom/android/dx/dex/code/CodeAddress;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget-object p1, v0, p1

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o00〇〇Oo(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇o00〇〇Oo:[Lcom/android/dx/dex/code/CodeAddress;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget-object p1, v0, p1

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇o〇(I)Lcom/android/dx/dex/code/CodeAddress;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/BlockAddresses;->〇080:[Lcom/android/dx/dex/code/CodeAddress;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

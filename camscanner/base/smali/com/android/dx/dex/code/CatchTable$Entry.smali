.class public Lcom/android/dx/dex/code/CatchTable$Entry;
.super Ljava/lang/Object;
.source "CatchTable.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dx/dex/code/CatchTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/android/dx/dex/code/CatchTable$Entry;",
        ">;"
    }
.end annotation


# instance fields
.field private final OO:Lcom/android/dx/dex/code/CatchHandlerList;

.field private final o0:I

.field private final 〇OOo8〇0:I


# direct methods
.method public constructor <init>(IILcom/android/dx/dex/code/CatchHandlerList;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-ltz p1, :cond_2

    .line 5
    .line 6
    if-le p2, p1, :cond_1

    .line 7
    .line 8
    invoke-virtual {p3}, Lcom/android/dx/util/MutabilityControl;->O8()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iput p1, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->o0:I

    .line 15
    .line 16
    iput p2, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->〇OOo8〇0:I

    .line 17
    .line 18
    iput-object p3, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->OO:Lcom/android/dx/dex/code/CatchHandlerList;

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 22
    .line 23
    const-string p2, "handlers.isMutable()"

    .line 24
    .line 25
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1

    .line 29
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 30
    .line 31
    const-string p2, "end <= start"

    .line 32
    .line 33
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw p1

    .line 37
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 38
    .line 39
    const-string p2, "start < 0"

    .line 40
    .line 41
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    throw p1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->o0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/android/dx/dex/code/CatchTable$Entry;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/android/dx/dex/code/CatchTable$Entry;->〇080(Lcom/android/dx/dex/code/CatchTable$Entry;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/android/dx/dex/code/CatchTable$Entry;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    check-cast p1, Lcom/android/dx/dex/code/CatchTable$Entry;

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/android/dx/dex/code/CatchTable$Entry;->〇080(Lcom/android/dx/dex/code/CatchTable$Entry;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    :cond_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->o0:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x1f

    .line 4
    .line 5
    iget v1, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->〇OOo8〇0:I

    .line 6
    .line 7
    add-int/2addr v0, v1

    .line 8
    mul-int/lit8 v0, v0, 0x1f

    .line 9
    .line 10
    iget-object v1, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->OO:Lcom/android/dx/dex/code/CatchHandlerList;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/android/dx/util/FixedSizeList;->hashCode()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    add-int/2addr v0, v1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(Lcom/android/dx/dex/code/CatchTable$Entry;)I
    .locals 4

    .line 1
    iget v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->o0:I

    .line 2
    .line 3
    iget v1, p1, Lcom/android/dx/dex/code/CatchTable$Entry;->o0:I

    .line 4
    .line 5
    const/4 v2, -0x1

    .line 6
    if-ge v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    const/4 v3, 0x1

    .line 10
    if-le v0, v1, :cond_1

    .line 11
    .line 12
    return v3

    .line 13
    :cond_1
    iget v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->〇OOo8〇0:I

    .line 14
    .line 15
    iget v1, p1, Lcom/android/dx/dex/code/CatchTable$Entry;->〇OOo8〇0:I

    .line 16
    .line 17
    if-ge v0, v1, :cond_2

    .line 18
    .line 19
    return v2

    .line 20
    :cond_2
    if-le v0, v1, :cond_3

    .line 21
    .line 22
    return v3

    .line 23
    :cond_3
    iget-object v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->OO:Lcom/android/dx/dex/code/CatchHandlerList;

    .line 24
    .line 25
    iget-object p1, p1, Lcom/android/dx/dex/code/CatchTable$Entry;->OO:Lcom/android/dx/dex/code/CatchHandlerList;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/code/CatchHandlerList;->〇O〇(Lcom/android/dx/dex/code/CatchHandlerList;)I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->〇OOo8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o〇()Lcom/android/dx/dex/code/CatchHandlerList;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/CatchTable$Entry;->OO:Lcom/android/dx/dex/code/CatchHandlerList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

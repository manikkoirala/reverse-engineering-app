.class public final Lcom/android/dx/dex/code/RopTranslator;
.super Ljava/lang/Object;
.source "RopTranslator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;
    }
.end annotation


# instance fields
.field private final O8:Lcom/android/dx/dex/code/BlockAddresses;

.field private final OO0o〇〇〇〇0:Z

.field private final Oo08:Lcom/android/dx/dex/code/OutputCollector;

.field private oO80:[I

.field private final o〇0:Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;

.field private final 〇080:Lcom/android/dx/dex/DexOptions;

.field private final 〇80〇808〇O:I

.field private final 〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

.field private final 〇o〇:I

.field private final 〇〇888:I


# direct methods
.method private constructor <init>(Lcom/android/dx/rop/code/RopMethod;ILcom/android/dx/rop/code/LocalVariableInfo;ILcom/android/dx/dex/DexOptions;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p5, p0, Lcom/android/dx/dex/code/RopTranslator;->〇080:Lcom/android/dx/dex/DexOptions;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 7
    .line 8
    iput p2, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o〇:I

    .line 9
    .line 10
    new-instance p2, Lcom/android/dx/dex/code/BlockAddresses;

    .line 11
    .line 12
    invoke-direct {p2, p1}, Lcom/android/dx/dex/code/BlockAddresses;-><init>(Lcom/android/dx/rop/code/RopMethod;)V

    .line 13
    .line 14
    .line 15
    iput-object p2, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 16
    .line 17
    iput p4, p0, Lcom/android/dx/dex/code/RopTranslator;->〇80〇808〇O:I

    .line 18
    .line 19
    const/4 p2, 0x0

    .line 20
    iput-object p2, p0, Lcom/android/dx/dex/code/RopTranslator;->oO80:[I

    .line 21
    .line 22
    invoke-static {p1, p4}, Lcom/android/dx/dex/code/RopTranslator;->oO80(Lcom/android/dx/rop/code/RopMethod;I)Z

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    iput-boolean p2, p0, Lcom/android/dx/dex/code/RopTranslator;->OO0o〇〇〇〇0:Z

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/android/dx/util/FixedSizeList;->size()I

    .line 33
    .line 34
    .line 35
    move-result p3

    .line 36
    mul-int/lit8 v3, p3, 0x3

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlockList;->〇O888o0o()I

    .line 39
    .line 40
    .line 41
    move-result p3

    .line 42
    add-int v2, v3, p3

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlockList;->oo88o8O()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p2, :cond_0

    .line 49
    .line 50
    const/4 p2, 0x0

    .line 51
    goto :goto_0

    .line 52
    :cond_0
    move p2, p4

    .line 53
    :goto_0
    add-int v4, p1, p2

    .line 54
    .line 55
    iput v4, p0, Lcom/android/dx/dex/code/RopTranslator;->〇〇888:I

    .line 56
    .line 57
    new-instance p1, Lcom/android/dx/dex/code/OutputCollector;

    .line 58
    .line 59
    move-object v0, p1

    .line 60
    move-object v1, p5

    .line 61
    move v5, p4

    .line 62
    invoke-direct/range {v0 .. v5}, Lcom/android/dx/dex/code/OutputCollector;-><init>(Lcom/android/dx/dex/DexOptions;IIII)V

    .line 63
    .line 64
    .line 65
    iput-object p1, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 66
    .line 67
    new-instance p2, Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;

    .line 68
    .line 69
    invoke-direct {p2, p0, p1}, Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;-><init>(Lcom/android/dx/dex/code/RopTranslator;Lcom/android/dx/dex/code/OutputCollector;)V

    .line 70
    .line 71
    .line 72
    iput-object p2, p0, Lcom/android/dx/dex/code/RopTranslator;->o〇0:Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;

    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method static synthetic O8(Lcom/android/dx/dex/code/RopTranslator;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/android/dx/dex/code/RopTranslator;->〇〇888:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private OO0o〇〇()V
    .locals 14

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/android/dx/util/FixedSizeList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {v0}, Lcom/android/dx/util/LabeledList;->〇O〇()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-static {v2}, Lcom/android/dx/util/Bits;->Oo08(I)[I

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    invoke-static {v2}, Lcom/android/dx/util/Bits;->Oo08(I)[I

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const/4 v4, 0x0

    .line 24
    const/4 v5, 0x0

    .line 25
    :goto_0
    if-ge v5, v1, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0, v5}, Lcom/android/dx/rop/code/BasicBlockList;->o800o8O(I)Lcom/android/dx/rop/code/BasicBlock;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    invoke-virtual {v6}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    invoke-static {v3, v6}, Lcom/android/dx/util/Bits;->o〇0([II)V

    .line 36
    .line 37
    .line 38
    add-int/lit8 v5, v5, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    new-array v5, v1, [I

    .line 42
    .line 43
    iget-object v6, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 44
    .line 45
    invoke-virtual {v6}, Lcom/android/dx/rop/code/RopMethod;->〇o〇()I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    const/4 v7, 0x0

    .line 50
    :goto_1
    const/4 v8, -0x1

    .line 51
    if-eq v6, v8, :cond_b

    .line 52
    .line 53
    :goto_2
    iget-object v9, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 54
    .line 55
    invoke-virtual {v9, v6}, Lcom/android/dx/rop/code/RopMethod;->O8(I)Lcom/android/dx/util/IntList;

    .line 56
    .line 57
    .line 58
    move-result-object v9

    .line 59
    invoke-virtual {v9}, Lcom/android/dx/util/IntList;->size()I

    .line 60
    .line 61
    .line 62
    move-result v10

    .line 63
    const/4 v11, 0x0

    .line 64
    :goto_3
    if-ge v11, v10, :cond_4

    .line 65
    .line 66
    invoke-virtual {v9, v11}, Lcom/android/dx/util/IntList;->〇8o8o〇(I)I

    .line 67
    .line 68
    .line 69
    move-result v12

    .line 70
    invoke-static {v2, v12}, Lcom/android/dx/util/Bits;->O8([II)Z

    .line 71
    .line 72
    .line 73
    move-result v13

    .line 74
    if-eqz v13, :cond_1

    .line 75
    .line 76
    goto :goto_5

    .line 77
    :cond_1
    invoke-static {v3, v12}, Lcom/android/dx/util/Bits;->O8([II)Z

    .line 78
    .line 79
    .line 80
    move-result v13

    .line 81
    if-nez v13, :cond_2

    .line 82
    .line 83
    goto :goto_4

    .line 84
    :cond_2
    invoke-virtual {v0, v12}, Lcom/android/dx/rop/code/BasicBlockList;->〇oo〇(I)Lcom/android/dx/rop/code/BasicBlock;

    .line 85
    .line 86
    .line 87
    move-result-object v13

    .line 88
    invoke-virtual {v13}, Lcom/android/dx/rop/code/BasicBlock;->O8()I

    .line 89
    .line 90
    .line 91
    move-result v13

    .line 92
    if-ne v13, v6, :cond_3

    .line 93
    .line 94
    invoke-static {v2, v12}, Lcom/android/dx/util/Bits;->o〇0([II)V

    .line 95
    .line 96
    .line 97
    move v6, v12

    .line 98
    goto :goto_2

    .line 99
    :cond_3
    :goto_4
    add-int/lit8 v11, v11, 0x1

    .line 100
    .line 101
    goto :goto_3

    .line 102
    :cond_4
    :goto_5
    if-eq v6, v8, :cond_a

    .line 103
    .line 104
    invoke-static {v3, v6}, Lcom/android/dx/util/Bits;->〇080([II)V

    .line 105
    .line 106
    .line 107
    invoke-static {v2, v6}, Lcom/android/dx/util/Bits;->〇080([II)V

    .line 108
    .line 109
    .line 110
    aput v6, v5, v7

    .line 111
    .line 112
    add-int/lit8 v7, v7, 0x1

    .line 113
    .line 114
    invoke-virtual {v0, v6}, Lcom/android/dx/rop/code/BasicBlockList;->〇oo〇(I)Lcom/android/dx/rop/code/BasicBlock;

    .line 115
    .line 116
    .line 117
    move-result-object v6

    .line 118
    invoke-virtual {v0, v6}, Lcom/android/dx/rop/code/BasicBlockList;->o〇O8〇〇o(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/rop/code/BasicBlock;

    .line 119
    .line 120
    .line 121
    move-result-object v9

    .line 122
    if-nez v9, :cond_5

    .line 123
    .line 124
    goto :goto_7

    .line 125
    :cond_5
    invoke-virtual {v9}, Lcom/android/dx/rop/code/BasicBlock;->getLabel()I

    .line 126
    .line 127
    .line 128
    move-result v9

    .line 129
    invoke-virtual {v6}, Lcom/android/dx/rop/code/BasicBlock;->O8()I

    .line 130
    .line 131
    .line 132
    move-result v10

    .line 133
    invoke-static {v3, v9}, Lcom/android/dx/util/Bits;->O8([II)Z

    .line 134
    .line 135
    .line 136
    move-result v11

    .line 137
    if-eqz v11, :cond_6

    .line 138
    .line 139
    move v6, v9

    .line 140
    goto :goto_5

    .line 141
    :cond_6
    if-eq v10, v9, :cond_7

    .line 142
    .line 143
    if-ltz v10, :cond_7

    .line 144
    .line 145
    invoke-static {v3, v10}, Lcom/android/dx/util/Bits;->O8([II)Z

    .line 146
    .line 147
    .line 148
    move-result v9

    .line 149
    if-eqz v9, :cond_7

    .line 150
    .line 151
    move v6, v10

    .line 152
    goto :goto_5

    .line 153
    :cond_7
    invoke-virtual {v6}, Lcom/android/dx/rop/code/BasicBlock;->o〇0()Lcom/android/dx/util/IntList;

    .line 154
    .line 155
    .line 156
    move-result-object v6

    .line 157
    invoke-virtual {v6}, Lcom/android/dx/util/IntList;->size()I

    .line 158
    .line 159
    .line 160
    move-result v9

    .line 161
    const/4 v10, 0x0

    .line 162
    :goto_6
    if-ge v10, v9, :cond_9

    .line 163
    .line 164
    invoke-virtual {v6, v10}, Lcom/android/dx/util/IntList;->〇8o8o〇(I)I

    .line 165
    .line 166
    .line 167
    move-result v11

    .line 168
    invoke-static {v3, v11}, Lcom/android/dx/util/Bits;->O8([II)Z

    .line 169
    .line 170
    .line 171
    move-result v12

    .line 172
    if-eqz v12, :cond_8

    .line 173
    .line 174
    move v6, v11

    .line 175
    goto :goto_5

    .line 176
    :cond_8
    add-int/lit8 v10, v10, 0x1

    .line 177
    .line 178
    goto :goto_6

    .line 179
    :cond_9
    const/4 v6, -0x1

    .line 180
    goto :goto_5

    .line 181
    :cond_a
    :goto_7
    invoke-static {v3, v4}, Lcom/android/dx/util/Bits;->〇o〇([II)I

    .line 182
    .line 183
    .line 184
    move-result v6

    .line 185
    goto/16 :goto_1

    .line 186
    .line 187
    :cond_b
    if-ne v7, v1, :cond_c

    .line 188
    .line 189
    iput-object v5, p0, Lcom/android/dx/dex/code/RopTranslator;->oO80:[I

    .line 190
    .line 191
    return-void

    .line 192
    :cond_c
    new-instance v0, Ljava/lang/RuntimeException;

    .line 193
    .line 194
    const-string v1, "shouldn\'t happen"

    .line 195
    .line 196
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    throw v0
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method private static OO0o〇〇〇〇0(Lcom/android/dx/rop/code/Insn;Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/android/dx/rop/code/Insn;->OO0o〇〇〇〇0()Lcom/android/dx/rop/code/RegisterSpecList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/android/dx/rop/code/Insn;->〇〇888()Lcom/android/dx/rop/code/Rop;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    invoke-virtual {p0}, Lcom/android/dx/rop/code/Rop;->o〇0()Z

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    if-eqz p0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/android/dx/util/FixedSizeList;->size()I

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    const/4 v1, 0x2

    .line 20
    if-ne p0, v1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/android/dx/rop/code/RegisterSpec;->〇8o8o〇()I

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-virtual {v0, v1}, Lcom/android/dx/rop/code/RegisterSpecList;->〇O00(I)Lcom/android/dx/rop/code/RegisterSpec;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v2}, Lcom/android/dx/rop/code/RegisterSpec;->〇8o8o〇()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-ne p0, v2, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/android/dx/rop/code/RegisterSpecList;->〇O00(I)Lcom/android/dx/rop/code/RegisterSpec;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    const/4 v1, 0x0

    .line 42
    invoke-virtual {v0, v1}, Lcom/android/dx/rop/code/RegisterSpecList;->〇O00(I)Lcom/android/dx/rop/code/RegisterSpec;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-static {p0, v0}, Lcom/android/dx/rop/code/RegisterSpecList;->OoO8(Lcom/android/dx/rop/code/RegisterSpec;Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    :cond_0
    if-nez p1, :cond_1

    .line 51
    .line 52
    return-object v0

    .line 53
    :cond_1
    invoke-virtual {v0, p1}, Lcom/android/dx/rop/code/RegisterSpecList;->o〇O8〇〇o(Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    return-object p0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method static synthetic Oo08(Lcom/android/dx/dex/code/RopTranslator;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/android/dx/dex/code/RopTranslator;->〇80〇808〇O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static Oooo8o0〇(Lcom/android/dx/rop/code/RopMethod;ILcom/android/dx/rop/code/LocalVariableInfo;ILcom/android/dx/dex/DexOptions;)Lcom/android/dx/dex/code/DalvCode;
    .locals 7

    .line 1
    new-instance v6, Lcom/android/dx/dex/code/RopTranslator;

    .line 2
    .line 3
    move-object v0, v6

    .line 4
    move-object v1, p0

    .line 5
    move v2, p1

    .line 6
    move-object v3, p2

    .line 7
    move v4, p3

    .line 8
    move-object v5, p4

    .line 9
    invoke-direct/range {v0 .. v5}, Lcom/android/dx/dex/code/RopTranslator;-><init>(Lcom/android/dx/rop/code/RopMethod;ILcom/android/dx/rop/code/LocalVariableInfo;ILcom/android/dx/dex/DexOptions;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {v6}, Lcom/android/dx/dex/code/RopTranslator;->〇〇808〇()Lcom/android/dx/dex/code/DalvCode;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    return-object p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method private static oO80(Lcom/android/dx/rop/code/RopMethod;I)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Z

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    aput-boolean v0, v1, v2

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/android/dx/rop/code/BasicBlockList;->oo88o8O()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p0}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    new-instance v3, Lcom/android/dx/dex/code/RopTranslator$1;

    .line 20
    .line 21
    invoke-direct {v3, v1, v0, p1}, Lcom/android/dx/dex/code/RopTranslator$1;-><init>([ZII)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v3}, Lcom/android/dx/rop/code/BasicBlockList;->OoO8(Lcom/android/dx/rop/code/Insn$Visitor;)V

    .line 25
    .line 26
    .line 27
    aget-boolean p0, v1, v2

    .line 28
    .line 29
    return p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method static synthetic o〇0(Lcom/android/dx/dex/code/RopTranslator;)Lcom/android/dx/rop/code/RopMethod;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇080(Lcom/android/dx/rop/code/Insn;)Lcom/android/dx/rop/code/RegisterSpecList;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/android/dx/dex/code/RopTranslator;->〇80〇808〇O(Lcom/android/dx/rop/code/Insn;)Lcom/android/dx/rop/code/RegisterSpecList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private static 〇80〇808〇O(Lcom/android/dx/rop/code/Insn;)Lcom/android/dx/rop/code/RegisterSpecList;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/android/dx/rop/code/Insn;->〇80〇808〇O()Lcom/android/dx/rop/code/RegisterSpec;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p0, v0}, Lcom/android/dx/dex/code/RopTranslator;->OO0o〇〇〇〇0(Lcom/android/dx/rop/code/Insn;Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇8o8o〇(Lcom/android/dx/rop/code/BasicBlock;I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/android/dx/dex/code/BlockAddresses;->O8(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Lcom/android/dx/dex/code/OutputCollector;->〇080(Lcom/android/dx/dex/code/DalvInsn;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/android/dx/dex/code/RopTranslator;->o〇0:Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 15
    .line 16
    invoke-virtual {v1, p1}, Lcom/android/dx/dex/code/BlockAddresses;->〇o00〇〇Oo(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, p1, v1}, Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;->〇〇888(Lcom/android/dx/rop/code/BasicBlock;Lcom/android/dx/dex/code/CodeAddress;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->〇o00〇〇Oo()Lcom/android/dx/rop/code/InsnList;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->o〇0:Lcom/android/dx/dex/code/RopTranslator$TranslationVisitor;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/android/dx/rop/code/InsnList;->〇〇808〇(Lcom/android/dx/rop/code/Insn$Visitor;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 35
    .line 36
    invoke-virtual {v1, p1}, Lcom/android/dx/dex/code/BlockAddresses;->〇080(Lcom/android/dx/rop/code/BasicBlock;)Lcom/android/dx/dex/code/CodeAddress;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/android/dx/dex/code/OutputCollector;->〇080(Lcom/android/dx/dex/code/DalvInsn;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->O8()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->〇o〇()Lcom/android/dx/rop/code/Insn;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    if-ltz v0, :cond_1

    .line 52
    .line 53
    if-eq v0, p2, :cond_1

    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/android/dx/rop/code/Insn;->〇〇888()Lcom/android/dx/rop/code/Rop;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v2}, Lcom/android/dx/rop/code/Rop;->〇o00〇〇Oo()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    const/4 v3, 0x4

    .line 64
    if-ne v2, v3, :cond_0

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/android/dx/rop/code/BasicBlock;->Oo08()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    if-ne p1, p2, :cond_0

    .line 71
    .line 72
    iget-object p1, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 73
    .line 74
    iget-object p2, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 75
    .line 76
    invoke-virtual {p2, v0}, Lcom/android/dx/dex/code/BlockAddresses;->〇o〇(I)Lcom/android/dx/dex/code/CodeAddress;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    const/4 v0, 0x1

    .line 81
    invoke-virtual {p1, v0, p2}, Lcom/android/dx/dex/code/OutputCollector;->O8(ILcom/android/dx/dex/code/CodeAddress;)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_0
    new-instance p1, Lcom/android/dx/dex/code/TargetInsn;

    .line 86
    .line 87
    sget-object p2, Lcom/android/dx/dex/code/Dops;->〇〇0o:Lcom/android/dx/dex/code/Dop;

    .line 88
    .line 89
    invoke-virtual {v1}, Lcom/android/dx/rop/code/Insn;->oO80()Lcom/android/dx/rop/code/SourcePosition;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    sget-object v2, Lcom/android/dx/rop/code/RegisterSpecList;->OO:Lcom/android/dx/rop/code/RegisterSpecList;

    .line 94
    .line 95
    iget-object v3, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 96
    .line 97
    invoke-virtual {v3, v0}, Lcom/android/dx/dex/code/BlockAddresses;->〇o〇(I)Lcom/android/dx/dex/code/CodeAddress;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-direct {p1, p2, v1, v2, v0}, Lcom/android/dx/dex/code/TargetInsn;-><init>(Lcom/android/dx/dex/code/Dop;Lcom/android/dx/rop/code/SourcePosition;Lcom/android/dx/rop/code/RegisterSpecList;Lcom/android/dx/dex/code/CodeAddress;)V

    .line 102
    .line 103
    .line 104
    iget-object p2, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 105
    .line 106
    invoke-virtual {p2, p1}, Lcom/android/dx/dex/code/OutputCollector;->〇080(Lcom/android/dx/dex/code/DalvInsn;)V

    .line 107
    .line 108
    .line 109
    :cond_1
    :goto_0
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private 〇O8o08O()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/rop/code/RopMethod;->〇o00〇〇Oo()Lcom/android/dx/rop/code/BasicBlockList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->oO80:[I

    .line 8
    .line 9
    array-length v2, v1

    .line 10
    const/4 v3, 0x0

    .line 11
    :goto_0
    if-ge v3, v2, :cond_1

    .line 12
    .line 13
    add-int/lit8 v4, v3, 0x1

    .line 14
    .line 15
    array-length v5, v1

    .line 16
    if-ne v4, v5, :cond_0

    .line 17
    .line 18
    const/4 v5, -0x1

    .line 19
    goto :goto_1

    .line 20
    :cond_0
    aget v5, v1, v4

    .line 21
    .line 22
    :goto_1
    aget v3, v1, v3

    .line 23
    .line 24
    invoke-virtual {v0, v3}, Lcom/android/dx/rop/code/BasicBlockList;->〇oo〇(I)Lcom/android/dx/rop/code/BasicBlock;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-direct {p0, v3, v5}, Lcom/android/dx/dex/code/RopTranslator;->〇8o8o〇(Lcom/android/dx/rop/code/BasicBlock;I)V

    .line 29
    .line 30
    .line 31
    move v3, v4

    .line 32
    goto :goto_0

    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/android/dx/dex/code/RopTranslator;)Lcom/android/dx/dex/code/BlockAddresses;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇o〇(Lcom/android/dx/dex/code/RopTranslator;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/android/dx/dex/code/RopTranslator;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇〇808〇()Lcom/android/dx/dex/code/DalvCode;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/android/dx/dex/code/RopTranslator;->OO0o〇〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/android/dx/dex/code/RopTranslator;->〇O8o08O()V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/android/dx/dex/code/StdCatchBuilder;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o00〇〇Oo:Lcom/android/dx/rop/code/RopMethod;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/android/dx/dex/code/RopTranslator;->oO80:[I

    .line 12
    .line 13
    iget-object v3, p0, Lcom/android/dx/dex/code/RopTranslator;->O8:Lcom/android/dx/dex/code/BlockAddresses;

    .line 14
    .line 15
    invoke-direct {v0, v1, v2, v3}, Lcom/android/dx/dex/code/StdCatchBuilder;-><init>(Lcom/android/dx/rop/code/RopMethod;[ILcom/android/dx/dex/code/BlockAddresses;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/android/dx/dex/code/DalvCode;

    .line 19
    .line 20
    iget v2, p0, Lcom/android/dx/dex/code/RopTranslator;->〇o〇:I

    .line 21
    .line 22
    iget-object v3, p0, Lcom/android/dx/dex/code/RopTranslator;->Oo08:Lcom/android/dx/dex/code/OutputCollector;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/android/dx/dex/code/OutputCollector;->〇o〇()Lcom/android/dx/dex/code/OutputFinisher;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-direct {v1, v2, v3, v0}, Lcom/android/dx/dex/code/DalvCode;-><init>(ILcom/android/dx/dex/code/OutputFinisher;Lcom/android/dx/dex/code/CatchBuilder;)V

    .line 29
    .line 30
    .line 31
    return-object v1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic 〇〇888(Lcom/android/dx/rop/code/Insn;Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/android/dx/dex/code/RopTranslator;->OO0o〇〇〇〇0(Lcom/android/dx/rop/code/Insn;Lcom/android/dx/rop/code/RegisterSpec;)Lcom/android/dx/rop/code/RegisterSpecList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

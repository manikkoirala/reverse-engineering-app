.class public final Lcom/android/dx/rop/cst/Zeroes;
.super Ljava/lang/Object;
.source "Zeroes.java"


# direct methods
.method public static 〇080(Lcom/android/dx/rop/type/Type;)Lcom/android/dx/rop/cst/Constant;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/android/dx/rop/type/Type;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    packed-switch v0, :pswitch_data_0

    .line 6
    .line 7
    .line 8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "no zero for type: "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/android/dx/rop/type/Type;->toHuman()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw v0

    .line 35
    :pswitch_0
    sget-object p0, Lcom/android/dx/rop/cst/CstKnownNull;->o0:Lcom/android/dx/rop/cst/CstKnownNull;

    .line 36
    .line 37
    return-object p0

    .line 38
    :pswitch_1
    sget-object p0, Lcom/android/dx/rop/cst/CstShort;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstShort;

    .line 39
    .line 40
    return-object p0

    .line 41
    :pswitch_2
    sget-object p0, Lcom/android/dx/rop/cst/CstLong;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstLong;

    .line 42
    .line 43
    return-object p0

    .line 44
    :pswitch_3
    sget-object p0, Lcom/android/dx/rop/cst/CstInteger;->〇08O〇00〇o:Lcom/android/dx/rop/cst/CstInteger;

    .line 45
    .line 46
    return-object p0

    .line 47
    :pswitch_4
    sget-object p0, Lcom/android/dx/rop/cst/CstFloat;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstFloat;

    .line 48
    .line 49
    return-object p0

    .line 50
    :pswitch_5
    sget-object p0, Lcom/android/dx/rop/cst/CstDouble;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstDouble;

    .line 51
    .line 52
    return-object p0

    .line 53
    :pswitch_6
    sget-object p0, Lcom/android/dx/rop/cst/CstChar;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstChar;

    .line 54
    .line 55
    return-object p0

    .line 56
    :pswitch_7
    sget-object p0, Lcom/android/dx/rop/cst/CstByte;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstByte;

    .line 57
    .line 58
    return-object p0

    .line 59
    :pswitch_8
    sget-object p0, Lcom/android/dx/rop/cst/CstBoolean;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstBoolean;

    .line 60
    .line 61
    return-object p0

    .line 62
    nop

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

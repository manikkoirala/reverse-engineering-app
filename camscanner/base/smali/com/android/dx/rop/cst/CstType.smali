.class public final Lcom/android/dx/rop/cst/CstType;
.super Lcom/android/dx/rop/cst/TypedConstant;
.source "CstType.java"


# static fields
.field public static final O0O:Lcom/android/dx/rop/cst/CstType;

.field public static final O88O:Lcom/android/dx/rop/cst/CstType;

.field public static final O8o08O8O:Lcom/android/dx/rop/cst/CstType;

.field private static final OO:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Lcom/android/dx/rop/type/Type;",
            "Lcom/android/dx/rop/cst/CstType;",
            ">;"
        }
    .end annotation
.end field

.field public static final OO〇00〇8oO:Lcom/android/dx/rop/cst/CstType;

.field public static final o8o:Lcom/android/dx/rop/cst/CstType;

.field public static final o8oOOo:Lcom/android/dx/rop/cst/CstType;

.field public static final o8〇OO0〇0o:Lcom/android/dx/rop/cst/CstType;

.field public static final oOO〇〇:Lcom/android/dx/rop/cst/CstType;

.field public static final oOo0:Lcom/android/dx/rop/cst/CstType;

.field public static final oOo〇8o008:Lcom/android/dx/rop/cst/CstType;

.field public static final oo8ooo8O:Lcom/android/dx/rop/cst/CstType;

.field public static final ooo0〇〇O:Lcom/android/dx/rop/cst/CstType;

.field public static final o〇00O:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇080OO8〇0:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇08O〇00〇o:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇0O:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇8〇oO〇〇8o:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇O〇〇O8:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇o0O:Lcom/android/dx/rop/cst/CstType;

.field public static final 〇〇08O:Lcom/android/dx/rop/cst/CstType;


# instance fields
.field private final o0:Lcom/android/dx/rop/type/Type;

.field private 〇OOo8〇0:Lcom/android/dx/rop/cst/CstString;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 2
    .line 3
    const/16 v1, 0x3e8

    .line 4
    .line 5
    const/high16 v2, 0x3f400000    # 0.75f

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IF)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->OO:Ljava/util/concurrent/ConcurrentMap;

    .line 11
    .line 12
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 13
    .line 14
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇〇o〇:Lcom/android/dx/rop/type/Type;

    .line 15
    .line 16
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇08O〇00〇o:Lcom/android/dx/rop/cst/CstType;

    .line 20
    .line 21
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 22
    .line 23
    sget-object v1, Lcom/android/dx/rop/type/Type;->O〇08oOOO0:Lcom/android/dx/rop/type/Type;

    .line 24
    .line 25
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 26
    .line 27
    .line 28
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->o〇00O:Lcom/android/dx/rop/cst/CstType;

    .line 29
    .line 30
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 31
    .line 32
    sget-object v1, Lcom/android/dx/rop/type/Type;->o8〇OO:Lcom/android/dx/rop/type/Type;

    .line 33
    .line 34
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 35
    .line 36
    .line 37
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->O8o08O8O:Lcom/android/dx/rop/cst/CstType;

    .line 38
    .line 39
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 40
    .line 41
    sget-object v1, Lcom/android/dx/rop/type/Type;->Ooo08:Lcom/android/dx/rop/type/Type;

    .line 42
    .line 43
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 44
    .line 45
    .line 46
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇080OO8〇0:Lcom/android/dx/rop/cst/CstType;

    .line 47
    .line 48
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 49
    .line 50
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇OO8ooO8〇:Lcom/android/dx/rop/type/Type;

    .line 51
    .line 52
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 53
    .line 54
    .line 55
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇0O:Lcom/android/dx/rop/cst/CstType;

    .line 56
    .line 57
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 58
    .line 59
    sget-object v1, Lcom/android/dx/rop/type/Type;->ooO:Lcom/android/dx/rop/type/Type;

    .line 60
    .line 61
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 62
    .line 63
    .line 64
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->oOo〇8o008:Lcom/android/dx/rop/cst/CstType;

    .line 65
    .line 66
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 67
    .line 68
    sget-object v1, Lcom/android/dx/rop/type/Type;->Oo0〇Ooo:Lcom/android/dx/rop/type/Type;

    .line 69
    .line 70
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 71
    .line 72
    .line 73
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->oOo0:Lcom/android/dx/rop/cst/CstType;

    .line 74
    .line 75
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 76
    .line 77
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇OO〇00〇0O:Lcom/android/dx/rop/type/Type;

    .line 78
    .line 79
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 80
    .line 81
    .line 82
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->OO〇00〇8oO:Lcom/android/dx/rop/cst/CstType;

    .line 83
    .line 84
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 85
    .line 86
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇〇〇0o〇〇0:Lcom/android/dx/rop/type/Type;

    .line 87
    .line 88
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 89
    .line 90
    .line 91
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->o8〇OO0〇0o:Lcom/android/dx/rop/cst/CstType;

    .line 92
    .line 93
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 94
    .line 95
    sget-object v1, Lcom/android/dx/rop/type/Type;->oO〇8O8oOo:Lcom/android/dx/rop/type/Type;

    .line 96
    .line 97
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 98
    .line 99
    .line 100
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇8〇oO〇〇8o:Lcom/android/dx/rop/cst/CstType;

    .line 101
    .line 102
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 103
    .line 104
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇0O〇O00O:Lcom/android/dx/rop/type/Type;

    .line 105
    .line 106
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 107
    .line 108
    .line 109
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->ooo0〇〇O:Lcom/android/dx/rop/cst/CstType;

    .line 110
    .line 111
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 112
    .line 113
    sget-object v1, Lcom/android/dx/rop/type/Type;->o0OoOOo0:Lcom/android/dx/rop/type/Type;

    .line 114
    .line 115
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 116
    .line 117
    .line 118
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇〇08O:Lcom/android/dx/rop/cst/CstType;

    .line 119
    .line 120
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 121
    .line 122
    sget-object v1, Lcom/android/dx/rop/type/Type;->o〇o〇Oo88:Lcom/android/dx/rop/type/Type;

    .line 123
    .line 124
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 125
    .line 126
    .line 127
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->O0O:Lcom/android/dx/rop/cst/CstType;

    .line 128
    .line 129
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 130
    .line 131
    sget-object v1, Lcom/android/dx/rop/type/Type;->oO00〇o:Lcom/android/dx/rop/type/Type;

    .line 132
    .line 133
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 134
    .line 135
    .line 136
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->o8oOOo:Lcom/android/dx/rop/cst/CstType;

    .line 137
    .line 138
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 139
    .line 140
    sget-object v1, Lcom/android/dx/rop/type/Type;->oOO0880O:Lcom/android/dx/rop/type/Type;

    .line 141
    .line 142
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 143
    .line 144
    .line 145
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇O〇〇O8:Lcom/android/dx/rop/cst/CstType;

    .line 146
    .line 147
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 148
    .line 149
    sget-object v1, Lcom/android/dx/rop/type/Type;->Oo0O0o8:Lcom/android/dx/rop/type/Type;

    .line 150
    .line 151
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 152
    .line 153
    .line 154
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->〇o0O:Lcom/android/dx/rop/cst/CstType;

    .line 155
    .line 156
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 157
    .line 158
    sget-object v1, Lcom/android/dx/rop/type/Type;->oOoo80oO:Lcom/android/dx/rop/type/Type;

    .line 159
    .line 160
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 161
    .line 162
    .line 163
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->O88O:Lcom/android/dx/rop/cst/CstType;

    .line 164
    .line 165
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 166
    .line 167
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇800OO〇0O:Lcom/android/dx/rop/type/Type;

    .line 168
    .line 169
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 170
    .line 171
    .line 172
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->oOO〇〇:Lcom/android/dx/rop/cst/CstType;

    .line 173
    .line 174
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 175
    .line 176
    sget-object v1, Lcom/android/dx/rop/type/Type;->oo8ooo8O:Lcom/android/dx/rop/type/Type;

    .line 177
    .line 178
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 179
    .line 180
    .line 181
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->o8o:Lcom/android/dx/rop/cst/CstType;

    .line 182
    .line 183
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 184
    .line 185
    sget-object v1, Lcom/android/dx/rop/type/Type;->〇08〇o0O:Lcom/android/dx/rop/type/Type;

    .line 186
    .line 187
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 188
    .line 189
    .line 190
    sput-object v0, Lcom/android/dx/rop/cst/CstType;->oo8ooo8O:Lcom/android/dx/rop/cst/CstType;

    .line 191
    .line 192
    invoke-static {}, Lcom/android/dx/rop/cst/CstType;->〇80〇808〇O()V

    .line 193
    .line 194
    .line 195
    return-void
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method public constructor <init>(Lcom/android/dx/rop/type/Type;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/android/dx/rop/cst/TypedConstant;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    sget-object v0, Lcom/android/dx/rop/type/Type;->〇O〇〇O8:Lcom/android/dx/rop/type/Type;

    .line 7
    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    iput-object p1, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    iput-object p1, p0, Lcom/android/dx/rop/cst/CstType;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstString;

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 17
    .line 18
    const-string v0, "KNOWN_NULL is not representable"

    .line 19
    .line 20
    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    throw p1

    .line 24
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    .line 25
    .line 26
    const-string v0, "type == null"

    .line 27
    .line 28
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    throw p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public static OO0o〇〇〇〇0(Lcom/android/dx/rop/type/Type;)Lcom/android/dx/rop/cst/CstType;
    .locals 2

    .line 1
    new-instance v0, Lcom/android/dx/rop/cst/CstType;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/android/dx/rop/cst/CstType;-><init>(Lcom/android/dx/rop/type/Type;)V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/android/dx/rop/cst/CstType;->OO:Ljava/util/concurrent/ConcurrentMap;

    .line 7
    .line 8
    invoke-interface {v1, p0, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    check-cast p0, Lcom/android/dx/rop/cst/CstType;

    .line 13
    .line 14
    if-eqz p0, :cond_0

    .line 15
    .line 16
    move-object v0, p0

    .line 17
    :cond_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private static 〇80〇808〇O()V
    .locals 1

    .line 1
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇08O〇00〇o:Lcom/android/dx/rop/cst/CstType;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->o〇00O:Lcom/android/dx/rop/cst/CstType;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 9
    .line 10
    .line 11
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->O8o08O8O:Lcom/android/dx/rop/cst/CstType;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇080OO8〇0:Lcom/android/dx/rop/cst/CstType;

    .line 17
    .line 18
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 19
    .line 20
    .line 21
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇0O:Lcom/android/dx/rop/cst/CstType;

    .line 22
    .line 23
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 24
    .line 25
    .line 26
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->oOo〇8o008:Lcom/android/dx/rop/cst/CstType;

    .line 27
    .line 28
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 29
    .line 30
    .line 31
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->oOo0:Lcom/android/dx/rop/cst/CstType;

    .line 32
    .line 33
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 34
    .line 35
    .line 36
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->OO〇00〇8oO:Lcom/android/dx/rop/cst/CstType;

    .line 37
    .line 38
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 39
    .line 40
    .line 41
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->o8〇OO0〇0o:Lcom/android/dx/rop/cst/CstType;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 44
    .line 45
    .line 46
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇8〇oO〇〇8o:Lcom/android/dx/rop/cst/CstType;

    .line 47
    .line 48
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->ooo0〇〇O:Lcom/android/dx/rop/cst/CstType;

    .line 52
    .line 53
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 54
    .line 55
    .line 56
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇〇08O:Lcom/android/dx/rop/cst/CstType;

    .line 57
    .line 58
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 59
    .line 60
    .line 61
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->O0O:Lcom/android/dx/rop/cst/CstType;

    .line 62
    .line 63
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 64
    .line 65
    .line 66
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->o8oOOo:Lcom/android/dx/rop/cst/CstType;

    .line 67
    .line 68
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 69
    .line 70
    .line 71
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇O〇〇O8:Lcom/android/dx/rop/cst/CstType;

    .line 72
    .line 73
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 74
    .line 75
    .line 76
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->〇o0O:Lcom/android/dx/rop/cst/CstType;

    .line 77
    .line 78
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 79
    .line 80
    .line 81
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->O88O:Lcom/android/dx/rop/cst/CstType;

    .line 82
    .line 83
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 84
    .line 85
    .line 86
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->oOO〇〇:Lcom/android/dx/rop/cst/CstType;

    .line 87
    .line 88
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 89
    .line 90
    .line 91
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->o8o:Lcom/android/dx/rop/cst/CstType;

    .line 92
    .line 93
    invoke-static {v0}, Lcom/android/dx/rop/cst/CstType;->〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method private static 〇8o8o〇(Lcom/android/dx/rop/cst/CstType;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/android/dx/rop/cst/CstType;->OO:Ljava/util/concurrent/ConcurrentMap;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/android/dx/rop/cst/CstType;->o〇0()Lcom/android/dx/rop/type/Type;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1, p0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 15
    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "Attempted re-init of "

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method


# virtual methods
.method protected O8(Lcom/android/dx/rop/cst/Constant;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/rop/type/Type;->oO80()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast p1, Lcom/android/dx/rop/cst/CstType;

    .line 8
    .line 9
    iget-object p1, p1, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/android/dx/rop/type/Type;->oO80()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    return p1
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public Oo08()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstType;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 8
    .line 9
    check-cast p1, Lcom/android/dx/rop/cst/CstType;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 12
    .line 13
    if-ne v0, p1, :cond_1

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_1
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public getType()Lcom/android/dx/rop/type/Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/android/dx/rop/type/Type;->oOO〇〇:Lcom/android/dx/rop/type/Type;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/rop/type/Type;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/android/dx/rop/cst/CstType;->〇〇888()Lcom/android/dx/rop/cst/CstString;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/android/dx/rop/cst/CstString;->getString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/16 v1, 0x2f

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/16 v3, 0x5b

    .line 16
    .line 17
    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    const/4 v4, -0x1

    .line 22
    if-ne v2, v4, :cond_0

    .line 23
    .line 24
    const-string v0, "default"

    .line 25
    .line 26
    return-object v0

    .line 27
    :cond_0
    add-int/lit8 v3, v3, 0x2

    .line 28
    .line 29
    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const/16 v2, 0x2e

    .line 34
    .line 35
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public o〇0()Lcom/android/dx/rop/type/Type;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toHuman()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/rop/type/Type;->toHuman()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "type{"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/android/dx/rop/cst/CstType;->toHuman()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const/16 v1, 0x7d

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public 〇〇888()Lcom/android/dx/rop/cst/CstString;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstString;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/android/dx/rop/cst/CstString;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/android/dx/rop/cst/CstType;->o0:Lcom/android/dx/rop/type/Type;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/android/dx/rop/type/Type;->oO80()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {v0, v1}, Lcom/android/dx/rop/cst/CstString;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/android/dx/rop/cst/CstType;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstString;

    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstType;->〇OOo8〇0:Lcom/android/dx/rop/cst/CstString;

    .line 19
    .line 20
    return-object v0
.end method

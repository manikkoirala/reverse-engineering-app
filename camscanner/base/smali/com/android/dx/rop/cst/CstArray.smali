.class public Lcom/android/dx/rop/cst/CstArray;
.super Lcom/android/dx/rop/cst/Constant;
.source "CstArray.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dx/rop/cst/CstArray$List;
    }
.end annotation


# instance fields
.field private final o0:Lcom/android/dx/rop/cst/CstArray$List;


# direct methods
.method public constructor <init>(Lcom/android/dx/rop/cst/CstArray$List;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/android/dx/rop/cst/Constant;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/android/dx/util/MutabilityControl;->〇〇888()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 13
    .line 14
    const-string v0, "list == null"

    .line 15
    .line 16
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    throw p1
    .line 20
    .line 21
    .line 22
    .line 23
.end method


# virtual methods
.method protected O8(Lcom/android/dx/rop/cst/Constant;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 2
    .line 3
    check-cast p1, Lcom/android/dx/rop/cst/CstArray;

    .line 4
    .line 5
    iget-object p1, p1, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/android/dx/rop/cst/CstArray$List;->〇〇808〇(Lcom/android/dx/rop/cst/CstArray$List;)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public Oo08()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "array"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/android/dx/rop/cst/CstArray;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 8
    .line 9
    check-cast p1, Lcom/android/dx/rop/cst/CstArray;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/android/dx/util/FixedSizeList;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/util/FixedSizeList;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()Lcom/android/dx/rop/cst/CstArray$List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toHuman()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 2
    .line 3
    const-string v1, ", "

    .line 4
    .line 5
    const-string v2, "}"

    .line 6
    .line 7
    const-string v3, "{"

    .line 8
    .line 9
    invoke-virtual {v0, v3, v1, v2}, Lcom/android/dx/util/FixedSizeList;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/android/dx/rop/cst/CstArray;->o0:Lcom/android/dx/rop/cst/CstArray$List;

    .line 2
    .line 3
    const-string v1, ", "

    .line 4
    .line 5
    const-string v2, "}"

    .line 6
    .line 7
    const-string v3, "array{"

    .line 8
    .line 9
    invoke-virtual {v0, v3, v1, v2}, Lcom/android/dx/util/FixedSizeList;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

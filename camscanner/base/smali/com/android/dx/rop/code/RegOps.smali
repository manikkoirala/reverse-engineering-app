.class public final Lcom/android/dx/rop/code/RegOps;
.super Ljava/lang/Object;
.source "RegOps.java"


# direct methods
.method public static 〇080(I)Ljava/lang/String;
    .locals 2

    .line 1
    packed-switch p0, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "unknown-"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-static {p0}, Lcom/android/dx/util/Hex;->O8(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    return-object p0

    .line 26
    :pswitch_1
    const-string p0, "invoke-custom"

    .line 27
    .line 28
    return-object p0

    .line 29
    :pswitch_2
    const-string p0, "invoke-polymorphic"

    .line 30
    .line 31
    return-object p0

    .line 32
    :pswitch_3
    const-string p0, "fill-array-data"

    .line 33
    .line 34
    return-object p0

    .line 35
    :pswitch_4
    const-string p0, "move-result-pseudo"

    .line 36
    .line 37
    return-object p0

    .line 38
    :pswitch_5
    const-string p0, "move-result"

    .line 39
    .line 40
    return-object p0

    .line 41
    :pswitch_6
    const-string p0, "invoke-interface"

    .line 42
    .line 43
    return-object p0

    .line 44
    :pswitch_7
    const-string p0, "invoke-direct"

    .line 45
    .line 46
    return-object p0

    .line 47
    :pswitch_8
    const-string p0, "invoke-super"

    .line 48
    .line 49
    return-object p0

    .line 50
    :pswitch_9
    const-string p0, "invoke-virtual"

    .line 51
    .line 52
    return-object p0

    .line 53
    :pswitch_a
    const-string p0, "invoke-static"

    .line 54
    .line 55
    return-object p0

    .line 56
    :pswitch_b
    const-string p0, "put-static"

    .line 57
    .line 58
    return-object p0

    .line 59
    :pswitch_c
    const-string p0, "put-field"

    .line 60
    .line 61
    return-object p0

    .line 62
    :pswitch_d
    const-string p0, "get-static"

    .line 63
    .line 64
    return-object p0

    .line 65
    :pswitch_e
    const-string p0, "get-field"

    .line 66
    .line 67
    return-object p0

    .line 68
    :pswitch_f
    const-string p0, "instance-of"

    .line 69
    .line 70
    return-object p0

    .line 71
    :pswitch_10
    const-string p0, "check-cast"

    .line 72
    .line 73
    return-object p0

    .line 74
    :pswitch_11
    const-string p0, "filled-new-array"

    .line 75
    .line 76
    return-object p0

    .line 77
    :pswitch_12
    const-string p0, "new-array"

    .line 78
    .line 79
    return-object p0

    .line 80
    :pswitch_13
    const-string p0, "new-instance"

    .line 81
    .line 82
    return-object p0

    .line 83
    :pswitch_14
    const-string p0, "aput"

    .line 84
    .line 85
    return-object p0

    .line 86
    :pswitch_15
    const-string p0, "aget"

    .line 87
    .line 88
    return-object p0

    .line 89
    :pswitch_16
    const-string p0, "monitor-exit"

    .line 90
    .line 91
    return-object p0

    .line 92
    :pswitch_17
    const-string p0, "monitor-enter"

    .line 93
    .line 94
    return-object p0

    .line 95
    :pswitch_18
    const-string p0, "throw"

    .line 96
    .line 97
    return-object p0

    .line 98
    :pswitch_19
    const-string p0, "array-length"

    .line 99
    .line 100
    return-object p0

    .line 101
    :pswitch_1a
    const-string p0, "return"

    .line 102
    .line 103
    return-object p0

    .line 104
    :pswitch_1b
    const-string p0, "to-short"

    .line 105
    .line 106
    return-object p0

    .line 107
    :pswitch_1c
    const-string p0, "to-char"

    .line 108
    .line 109
    return-object p0

    .line 110
    :pswitch_1d
    const-string p0, "to-byte"

    .line 111
    .line 112
    return-object p0

    .line 113
    :pswitch_1e
    const-string p0, "conv"

    .line 114
    .line 115
    return-object p0

    .line 116
    :pswitch_1f
    const-string p0, "cmpg"

    .line 117
    .line 118
    return-object p0

    .line 119
    :pswitch_20
    const-string p0, "cmpl"

    .line 120
    .line 121
    return-object p0

    .line 122
    :pswitch_21
    const-string p0, "not"

    .line 123
    .line 124
    return-object p0

    .line 125
    :pswitch_22
    const-string p0, "ushr"

    .line 126
    .line 127
    return-object p0

    .line 128
    :pswitch_23
    const-string p0, "shr"

    .line 129
    .line 130
    return-object p0

    .line 131
    :pswitch_24
    const-string p0, "shl"

    .line 132
    .line 133
    return-object p0

    .line 134
    :pswitch_25
    const-string p0, "xor"

    .line 135
    .line 136
    return-object p0

    .line 137
    :pswitch_26
    const-string p0, "or"

    .line 138
    .line 139
    return-object p0

    .line 140
    :pswitch_27
    const-string p0, "and"

    .line 141
    .line 142
    return-object p0

    .line 143
    :pswitch_28
    const-string p0, "neg"

    .line 144
    .line 145
    return-object p0

    .line 146
    :pswitch_29
    const-string p0, "rem"

    .line 147
    .line 148
    return-object p0

    .line 149
    :pswitch_2a
    const-string p0, "div"

    .line 150
    .line 151
    return-object p0

    .line 152
    :pswitch_2b
    const-string p0, "mul"

    .line 153
    .line 154
    return-object p0

    .line 155
    :pswitch_2c
    const-string p0, "sub"

    .line 156
    .line 157
    return-object p0

    .line 158
    :pswitch_2d
    const-string p0, "add"

    .line 159
    .line 160
    return-object p0

    .line 161
    :pswitch_2e
    const-string p0, "switch"

    .line 162
    .line 163
    return-object p0

    .line 164
    :pswitch_2f
    const-string p0, "if-gt"

    .line 165
    .line 166
    return-object p0

    .line 167
    :pswitch_30
    const-string p0, "if-le"

    .line 168
    .line 169
    return-object p0

    .line 170
    :pswitch_31
    const-string p0, "if-ge"

    .line 171
    .line 172
    return-object p0

    .line 173
    :pswitch_32
    const-string p0, "if-lt"

    .line 174
    .line 175
    return-object p0

    .line 176
    :pswitch_33
    const-string p0, "if-ne"

    .line 177
    .line 178
    return-object p0

    .line 179
    :pswitch_34
    const-string p0, "if-eq"

    .line 180
    .line 181
    return-object p0

    .line 182
    :pswitch_35
    const-string p0, "goto"

    .line 183
    .line 184
    return-object p0

    .line 185
    :pswitch_36
    const-string p0, "const"

    .line 186
    .line 187
    return-object p0

    .line 188
    :pswitch_37
    const-string p0, "move-exception"

    .line 189
    .line 190
    return-object p0

    .line 191
    :pswitch_38
    const-string p0, "move-param"

    .line 192
    .line 193
    return-object p0

    .line 194
    :pswitch_39
    const-string p0, "move"

    .line 195
    .line 196
    return-object p0

    .line 197
    :pswitch_3a
    const-string p0, "nop"

    .line 198
    .line 199
    return-object p0

    .line 200
    nop

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

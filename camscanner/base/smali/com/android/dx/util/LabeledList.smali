.class public Lcom/android/dx/util/LabeledList;
.super Lcom/android/dx/util/FixedSizeList;
.source "LabeledList.java"


# instance fields
.field private final OO:Lcom/android/dx/util/IntList;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/android/dx/util/FixedSizeList;-><init>(I)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/android/dx/util/IntList;

    .line 5
    .line 6
    invoke-direct {v0, p1}, Lcom/android/dx/util/IntList;-><init>(I)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇〇808〇(II)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/util/IntList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    sub-int v2, p1, v0

    .line 9
    .line 10
    if-gt v1, v2, :cond_0

    .line 11
    .line 12
    iget-object v2, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 13
    .line 14
    const/4 v3, -0x1

    .line 15
    invoke-virtual {v2, v3}, Lcom/android/dx/util/IntList;->oO80(I)V

    .line 16
    .line 17
    .line 18
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 22
    .line 23
    invoke-virtual {v0, p1, p2}, Lcom/android/dx/util/IntList;->Oooo8o0〇(II)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method private 〇〇8O0〇8(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    invoke-virtual {v0, p1, v1}, Lcom/android/dx/util/IntList;->Oooo8o0〇(II)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method


# virtual methods
.method protected 〇0〇O0088o(ILcom/android/dx/util/LabeledItem;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/android/dx/util/FixedSizeList;->〇80〇808〇O(I)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/android/dx/util/LabeledItem;

    .line 6
    .line 7
    invoke-virtual {p0, p1, p2}, Lcom/android/dx/util/FixedSizeList;->OO0o〇〇〇〇0(ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/android/dx/util/LabeledItem;->getLabel()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-direct {p0, v0}, Lcom/android/dx/util/LabeledList;->〇〇8O0〇8(I)V

    .line 17
    .line 18
    .line 19
    :cond_0
    if-eqz p2, :cond_1

    .line 20
    .line 21
    invoke-interface {p2}, Lcom/android/dx/util/LabeledItem;->getLabel()I

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    invoke-direct {p0, p2, p1}, Lcom/android/dx/util/LabeledList;->〇〇808〇(II)V

    .line 26
    .line 27
    .line 28
    :cond_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public final 〇O00(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/util/IntList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lt p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, -0x1

    .line 10
    return p1

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/android/dx/util/IntList;->〇8o8o〇(I)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public final 〇O〇()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/android/dx/util/IntList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, -0x1

    .line 8
    .line 9
    :goto_0
    if-ltz v0, :cond_0

    .line 10
    .line 11
    iget-object v1, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Lcom/android/dx/util/IntList;->〇8o8o〇(I)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-gez v1, :cond_0

    .line 18
    .line 19
    add-int/lit8 v0, v0, -0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 23
    .line 24
    iget-object v1, p0, Lcom/android/dx/util/LabeledList;->OO:Lcom/android/dx/util/IntList;

    .line 25
    .line 26
    invoke-virtual {v1, v0}, Lcom/android/dx/util/IntList;->〇〇808〇(I)V

    .line 27
    .line 28
    .line 29
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.class public Lb/a/a/a/a/a/b/a;
.super Ljava/lang/Object;
.source "MediaConfig.java"


# static fields
.field public static O8:I = 0xa

.field public static Oo08:I = 0xa

.field private static 〇080:Lb/a/a/a/a/a/a/d/b; = null

.field public static 〇o00〇〇Oo:I = 0xa

.field public static 〇o〇:I = 0xa


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static O8(Lorg/json/JSONObject;)V
    .locals 3

    .line 1
    const-string v0, "MediaConfig"

    .line 2
    .line 3
    if-nez p0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    :try_start_0
    const-string v1, "splash"

    .line 7
    .line 8
    const/16 v2, 0xa

    .line 9
    .line 10
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    sput v1, Lb/a/a/a/a/a/b/a;->〇o00〇〇Oo:I

    .line 15
    .line 16
    const-string v1, "reward"

    .line 17
    .line 18
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    sput v1, Lb/a/a/a/a/a/b/a;->〇o〇:I

    .line 23
    .line 24
    const-string v1, "brand"

    .line 25
    .line 26
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    sput v1, Lb/a/a/a/a/a/b/a;->O8:I

    .line 31
    .line 32
    const-string v1, "other"

    .line 33
    .line 34
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    sput p0, Lb/a/a/a/a/a/b/a;->Oo08:I

    .line 39
    .line 40
    sget v1, Lb/a/a/a/a/a/b/a;->〇o00〇〇Oo:I

    .line 41
    .line 42
    if-gez v1, :cond_1

    .line 43
    .line 44
    sput v2, Lb/a/a/a/a/a/b/a;->〇o00〇〇Oo:I

    .line 45
    .line 46
    :cond_1
    sget v1, Lb/a/a/a/a/a/b/a;->〇o〇:I

    .line 47
    .line 48
    if-gez v1, :cond_2

    .line 49
    .line 50
    sput v2, Lb/a/a/a/a/a/b/a;->〇o〇:I

    .line 51
    .line 52
    :cond_2
    sget v1, Lb/a/a/a/a/a/b/a;->O8:I

    .line 53
    .line 54
    if-gez v1, :cond_3

    .line 55
    .line 56
    sput v2, Lb/a/a/a/a/a/b/a;->O8:I

    .line 57
    .line 58
    :cond_3
    if-gez p0, :cond_4

    .line 59
    .line 60
    sput v2, Lb/a/a/a/a/a/b/a;->Oo08:I

    .line 61
    .line 62
    :cond_4
    const/16 p0, 0x8

    .line 63
    .line 64
    new-array p0, p0, [Ljava/lang/Object;

    .line 65
    .line 66
    const-string v1, "splash="

    .line 67
    .line 68
    const/4 v2, 0x0

    .line 69
    aput-object v1, p0, v2

    .line 70
    .line 71
    sget v1, Lb/a/a/a/a/a/b/a;->〇o00〇〇Oo:I

    .line 72
    .line 73
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    const/4 v2, 0x1

    .line 78
    aput-object v1, p0, v2

    .line 79
    .line 80
    const-string v1, ",reward="

    .line 81
    .line 82
    const/4 v2, 0x2

    .line 83
    aput-object v1, p0, v2

    .line 84
    .line 85
    sget v1, Lb/a/a/a/a/a/b/a;->〇o〇:I

    .line 86
    .line 87
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    const/4 v2, 0x3

    .line 92
    aput-object v1, p0, v2

    .line 93
    .line 94
    const-string v1, ",brand="

    .line 95
    .line 96
    const/4 v2, 0x4

    .line 97
    aput-object v1, p0, v2

    .line 98
    .line 99
    sget v1, Lb/a/a/a/a/a/b/a;->O8:I

    .line 100
    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    const/4 v2, 0x5

    .line 106
    aput-object v1, p0, v2

    .line 107
    .line 108
    const-string v1, ",other="

    .line 109
    .line 110
    const/4 v2, 0x6

    .line 111
    aput-object v1, p0, v2

    .line 112
    .line 113
    sget v1, Lb/a/a/a/a/a/b/a;->Oo08:I

    .line 114
    .line 115
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    const/4 v2, 0x7

    .line 120
    aput-object v1, p0, v2

    .line 121
    .line 122
    invoke-static {v0, p0}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :catchall_0
    move-exception p0

    .line 127
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p0

    .line 131
    invoke-static {v0, p0}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    :goto_0
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method public static Oo08()I
    .locals 1

    .line 1
    sget v0, Lb/a/a/a/a/a/b/a;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static oO80()I
    .locals 1

    .line 1
    sget v0, Lb/a/a/a/a/a/b/a;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static o〇0()I
    .locals 1

    .line 1
    sget v0, Lb/a/a/a/a/a/b/a;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇080()V
    .locals 1

    .line 1
    sget-object v0, Lb/a/a/a/a/a/b/a;->〇080:Lb/a/a/a/a/a/a/d/b;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/d/b;->c()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-static {p0}, Lb/a/a/a/a/a/a/i/a;->〇080(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 5
    .line 6
    const/16 v0, 0x17

    .line 7
    .line 8
    if-ge p0, v0, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lb/a/a/a/a/a/b/c/q/a;->〇o〇()Lb/a/a/a/a/a/b/c/q/a;

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇o〇(Lb/a/a/a/a/a/a/d/b;)V
    .locals 0

    .line 1
    sput-object p0, Lb/a/a/a/a/a/b/a;->〇080:Lb/a/a/a/a/a/a/d/b;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇888()I
    .locals 1

    .line 1
    sget v0, Lb/a/a/a/a/a/b/a;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

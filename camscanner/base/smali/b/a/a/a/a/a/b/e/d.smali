.class public Lb/a/a/a/a/a/b/e/d;
.super Ljava/lang/Object;
.source "SSMediaPlayerWrapper.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/y$a;
.implements Lb/a/a/a/a/a/b/e/c$e;
.implements Lb/a/a/a/a/a/b/e/c$b;
.implements Lb/a/a/a/a/a/b/e/c$c;
.implements Lb/a/a/a/a/a/b/e/c$g;
.implements Lb/a/a/a/a/a/b/e/c$a;
.implements Lb/a/a/a/a/a/b/e/c$f;
.implements Lb/a/a/a/a/a/b/e/c$d;
.implements Lb/a/a/a/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/a/a/a/a/a/b/e/d$o;
    }
.end annotation


# static fields
.field private static final J:Landroid/util/SparseIntArray;


# instance fields
.field private volatile A:I

.field private B:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private C:Landroid/view/Surface;

.field private final D:Ljava/lang/Runnable;

.field private final E:Lb/a/a/a/a/a/b/e/d$o;

.field private volatile F:Z

.field private G:J

.field private H:J

.field private I:Z

.field private a:Landroid/graphics/SurfaceTexture;

.field private b:Landroid/view/SurfaceHolder;

.field private c:I

.field private d:I

.field private e:Z

.field private volatile f:Lb/a/a/a/a/a/b/e/c;

.field private g:Z

.field private h:Z

.field private i:Z

.field private volatile j:I

.field private k:J

.field private l:Lcom/bytedance/sdk/component/utils/y;

.field private m:Z

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:Z

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Z

.field private final x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/WeakReference<",
            "Lb/a/a/a/a/a/a/a$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private y:Lb/a/a/a/a/a/a/f/c;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/util/SparseIntArray;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lb/a/a/a/a/a/b/e/d;->J:Landroid/util/SparseIntArray;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    .line 6
    .line 7
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->e:Z

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 11
    .line 12
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->g:Z

    .line 13
    .line 14
    const/16 v2, 0xc9

    .line 15
    .line 16
    iput v2, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 17
    .line 18
    const-wide/16 v2, -0x1

    .line 19
    .line 20
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->k:J

    .line 21
    .line 22
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->m:Z

    .line 23
    .line 24
    const-wide/16 v2, 0x0

    .line 25
    .line 26
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    .line 27
    .line 28
    const-wide/high16 v4, -0x8000000000000000L

    .line 29
    .line 30
    iput-wide v4, p0, Lb/a/a/a/a/a/b/e/d;->o:J

    .line 31
    .line 32
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->p:J

    .line 33
    .line 34
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->q:J

    .line 35
    .line 36
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->r:J

    .line 37
    .line 38
    iput v0, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    .line 39
    .line 40
    new-instance v4, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 41
    .line 42
    invoke-direct {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object v4, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    .line 46
    .line 47
    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    .line 48
    .line 49
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->z:Z

    .line 50
    .line 51
    const/16 v4, 0xc8

    .line 52
    .line 53
    iput v4, p0, Lb/a/a/a/a/a/b/e/d;->A:I

    .line 54
    .line 55
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    .line 57
    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 58
    .line 59
    .line 60
    iput-object v4, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 61
    .line 62
    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->C:Landroid/view/Surface;

    .line 63
    .line 64
    new-instance v1, Lb/a/a/a/a/a/b/e/d$h;

    .line 65
    .line 66
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$h;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 67
    .line 68
    .line 69
    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    .line 70
    .line 71
    new-instance v1, Lb/a/a/a/a/a/b/e/d$o;

    .line 72
    .line 73
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$o;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 74
    .line 75
    .line 76
    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    .line 77
    .line 78
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->G:J

    .line 79
    .line 80
    iput-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->H:J

    .line 81
    .line 82
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    .line 83
    .line 84
    iput v0, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    .line 85
    .line 86
    invoke-static {}, Lb/b/a/a/k/i/a;->〇080()Lb/b/a/a/k/i/a;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    const-string v1, "csj_SSMediaPlayerWrapper"

    .line 91
    .line 92
    invoke-virtual {v0, p0, v1}, Lb/b/a/a/k/i/a;->o〇0(Lcom/bytedance/sdk/component/utils/y$a;Ljava/lang/String;)Lcom/bytedance/sdk/component/utils/y;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    iput-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 97
    .line 98
    const/4 v0, 0x1

    .line 99
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    .line 100
    .line 101
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->t()V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method private B()V
    .locals 3

    .line 1
    const-string v0, "releaseMediaPlayer: "

    .line 2
    .line 3
    const-string v1, "CSJ_VIDEO_MEDIA"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    :try_start_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 14
    .line 15
    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :catchall_0
    move-exception v0

    .line 20
    const-string v2, "releaseMediaplayer error1: "

    .line 21
    .line 22
    invoke-static {v1, v2, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->O8(Lb/a/a/a/a/a/b/e/c$b;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 32
    .line 33
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->Oo08(Lb/a/a/a/a/a/b/e/c$g;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 37
    .line 38
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->〇o〇(Lb/a/a/a/a/a/b/e/c$a;)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 42
    .line 43
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->〇080(Lb/a/a/a/a/a/b/e/c$d;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 47
    .line 48
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->〇o00〇〇Oo(Lb/a/a/a/a/a/b/e/c$c;)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 52
    .line 53
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->〇〇888(Lb/a/a/a/a/a/b/e/c$e;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 57
    .line 58
    invoke-interface {v0, v2}, Lb/a/a/a/a/a/b/e/c;->o〇0(Lb/a/a/a/a/a/b/e/c$f;)V

    .line 59
    .line 60
    .line 61
    :try_start_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 62
    .line 63
    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :catchall_1
    move-exception v0

    .line 68
    const-string v2, "releaseMediaplayer error2: "

    .line 69
    .line 70
    invoke-static {v1, v2, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    .line 72
    .line 73
    :goto_1
    return-void
    .line 74
    .line 75
    .line 76
.end method

.method private C()V
    .locals 3

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    iput-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    iput v2, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    .line 7
    .line 8
    iput-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->p:J

    .line 9
    .line 10
    iput-boolean v2, p0, Lb/a/a/a/a/a/b/e/d;->m:Z

    .line 11
    .line 12
    const-wide/high16 v0, -0x8000000000000000L

    .line 13
    .line 14
    iput-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->o:J

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;I)I
    .locals 0

    .line 6
    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    return p1
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;J)J
    .locals 0

    .line 4
    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->o:J

    return-wide p1
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;)Lb/a/a/a/a/a/b/e/c;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    return-object p0
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;Lb/a/a/a/a/a/b/e/c;)Lb/a/a/a/a/a/b/e/c;
    .locals 0

    .line 2
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    return-object p1
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;Lcom/bytedance/sdk/component/utils/y;)Lcom/bytedance/sdk/component/utils/y;
    .locals 0

    .line 9
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    return-object p1
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 3
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->v:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 1

    .line 210
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(II)V
    .locals 13

    const/16 p2, 0x2bd

    const v0, 0x7fffffff

    const-string v1, "bufferCount = "

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, "CSJ_VIDEO_MEDIA"

    const/4 v5, 0x0

    if-ne p1, p2, :cond_2

    .line 165
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->G:J

    .line 166
    iget p1, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    add-int/2addr p1, v3

    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    .line 167
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_0

    .line 168
    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 169
    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {p2, p0, v0, v5, v5}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;III)V

    goto :goto_0

    :cond_1
    new-array p1, v2, [Ljava/lang/Object;

    aput-object v1, p1, v5

    .line 170
    iget p2, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v3

    invoke-static {v4, p1}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_2
    const/16 p2, 0x2be

    const/4 v6, 0x3

    if-ne p1, p2, :cond_6

    .line 171
    iget-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->G:J

    const-wide/16 v7, 0x0

    cmp-long v9, p1, v7

    if-lez v9, :cond_3

    .line 172
    iget-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->H:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    iget-wide v11, p0, Lb/a/a/a/a/a/b/e/d;->G:J

    sub-long/2addr v9, v11

    add-long/2addr p1, v9

    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->H:J

    .line 173
    iput-wide v7, p0, Lb/a/a/a/a/a/b/e/d;->G:J

    .line 174
    :cond_3
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_4

    .line 175
    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 176
    invoke-virtual {p2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {p2, p0, v0}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;I)V

    goto :goto_1

    :cond_5
    const/4 p1, 0x4

    new-array p1, p1, [Ljava/lang/Object;

    aput-object v1, p1, v5

    .line 177
    iget p2, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v3

    const-string p2, " mBufferTotalTime = "

    aput-object p2, p1, v2

    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->H:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v6

    invoke-static {v4, p1}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 178
    :cond_6
    iget-boolean p2, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    if-eqz p2, :cond_7

    if-ne p1, v6, :cond_7

    .line 179
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "hasPendingPauseCommand:"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p2, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->k()V

    .line 181
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->j()V

    .line 182
    iget-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->z:Z

    invoke-virtual {p0, p1}, Lb/a/a/a/a/a/b/e/d;->a(Z)V

    const-string p1, "onRenderStart"

    .line 183
    invoke-static {v4, p1}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_2
    return-void
.end method

.method private a(J)V
    .locals 1

    .line 27
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    invoke-virtual {v0, p1, p2}, Lb/a/a/a/a/a/b/e/d$o;->〇080(J)V

    .line 28
    iget-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->w:Z

    if-eqz p1, :cond_0

    .line 29
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    invoke-direct {p0, p1}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 30
    :cond_0
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    invoke-direct {p0, p1}, Lb/a/a/a/a/a/b/e/d;->a(Lb/a/a/a/a/a/a/f/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 31
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    invoke-direct {p0, p1}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 32
    :cond_1
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    invoke-direct {p0, p1}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private a(JJ)V
    .locals 8

    .line 10
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 11
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 12
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lb/a/a/a/a/a/a/a$a;

    move-object v3, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-interface/range {v2 .. v7}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;JJ)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;II)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2}, Lb/a/a/a/a/a/b/e/d;->a(II)V

    return-void
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;JJ)V
    .locals 0

    .line 8
    invoke-direct {p0, p1, p2, p3, p4}, Lb/a/a/a/a/a/b/e/d;->a(JJ)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    const-string v0, "CSJ_VIDEO_MEDIA"

    :try_start_0
    const-string v1, "enqueueAction()"

    .line 205
    invoke-static {v0, v1}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    .line 208
    :cond_0
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 209
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 151
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 152
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-interface {p1, v1}, Lb/a/a/a/a/a/b/e/c;->OO0o〇〇〇〇0(Ljava/io/FileDescriptor;)V

    .line 153
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    return-void
.end method

.method private a(Lb/a/a/a/a/a/a/f/c;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 33
    invoke-virtual {p1}, Lb/a/a/a/a/a/a/f/c;->s()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic a(Lb/a/a/a/a/a/b/e/d;Z)Z
    .locals 0

    .line 5
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->g:Z

    return p1
.end method

.method static synthetic b(Lb/a/a/a/a/a/b/e/d;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->o:J

    return-wide v0
.end method

.method static synthetic b(Lb/a/a/a/a/a/b/e/d;J)J
    .locals 0

    .line 2
    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->k:J

    return-wide p1
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    if-eqz p1, :cond_2

    .line 34
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 35
    :cond_0
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->i:Z

    if-nez v0, :cond_1

    .line 36
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 37
    :cond_1
    invoke-direct {p0, p1}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/Runnable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private b(II)Z
    .locals 2

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnError - Error code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " Extra code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CSJ_VIDEO_MEDIA"

    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x3f2

    const/4 v1, 0x1

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ef

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-eq p1, v0, :cond_0

    const/16 v0, -0x6e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eq p2, v1, :cond_1

    const/16 v0, 0x2bc

    if-eq p2, v0, :cond_1

    const/16 v0, 0x320

    if-eq p2, v0, :cond_1

    move v1, p1

    :cond_1
    return v1
.end method

.method static synthetic b(Lb/a/a/a/a/a/b/e/d;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    return p1
.end method

.method static synthetic c(Lb/a/a/a/a/a/b/e/d;J)J
    .locals 0

    .line 3
    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->p:J

    return-wide p1
.end method

.method static synthetic c(Lb/a/a/a/a/a/b/e/d;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    return-object p0
.end method

.method static synthetic c(Lb/a/a/a/a/a/b/e/d;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->z:Z

    return p1
.end method

.method static synthetic d(Lb/a/a/a/a/a/b/e/d;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->k:J

    return-wide v0
.end method

.method static synthetic d(Lb/a/a/a/a/a/b/e/d;J)J
    .locals 0

    .line 3
    iput-wide p1, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    return-wide p1
.end method

.method static synthetic d(Lb/a/a/a/a/a/b/e/d;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->m:Z

    return p1
.end method

.method static synthetic e(Lb/a/a/a/a/a/b/e/d;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->t()V

    return-void
.end method

.method static synthetic f(Lb/a/a/a/a/a/b/e/d;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lb/a/a/a/a/a/b/e/d;->m:Z

    return p0
.end method

.method static synthetic g(Lb/a/a/a/a/a/b/e/d;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->p:J

    return-wide v0
.end method

.method static synthetic h(Lb/a/a/a/a/a/b/e/d;)I
    .locals 0

    .line 1
    iget p0, p0, Lb/a/a/a/a/a/b/e/d;->A:I

    return p0
.end method

.method static synthetic i(Lb/a/a/a/a/a/b/e/d;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    return-wide v0
.end method

.method static synthetic j(Lb/a/a/a/a/a/b/e/d;)I
    .locals 0

    .line 1
    iget p0, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    return p0
.end method

.method private j()V
    .locals 5

    .line 2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->r:J

    sub-long/2addr v0, v2

    .line 3
    iget-object v2, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    if-eqz v3, :cond_0

    .line 4
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 5
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v3, p0, v0, v1}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;J)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->e:Z

    return-void
.end method

.method static synthetic k(Lb/a/a/a/a/a/b/e/d;)Lcom/bytedance/sdk/component/utils/y;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    return-object p0
.end method

.method private k()V
    .locals 3

    .line 2
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 3
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPendingAction:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CSJ_VIDEO_MEDIA"

    invoke-static {v2, v1}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    return-void

    .line 4
    :cond_2
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->l()V

    return-void
.end method

.method private l()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->h:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->h:Z

    .line 8
    .line 9
    new-instance v0, Ljava/util/ArrayList;

    .line 10
    .line 11
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Ljava/lang/Runnable;

    .line 31
    .line 32
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->t:Ljava/util/ArrayList;

    .line 37
    .line 38
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 39
    .line 40
    .line 41
    const/4 v0, 0x0

    .line 42
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->h:Z

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private s()V
    .locals 4

    .line 1
    sget-object v0, Lb/a/a/a/a/a/b/e/d;->J:Landroid/util/SparseIntArray;

    .line 2
    .line 3
    iget v1, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    iget v1, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget v3, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    add-int/2addr v1, v2

    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 38
    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private t()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "initMediaPlayer: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v1, 0x0

    .line 18
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "CSJ_VIDEO_MEDIA"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 31
    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    new-instance v1, Lb/a/a/a/a/a/b/e/d$j;

    .line 35
    .line 36
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$j;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 40
    .line 41
    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private v()V
    .locals 2

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 13
    .line 14
    new-instance v1, Lb/a/a/a/a/a/b/e/d$e;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$e;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 20
    .line 21
    .line 22
    :cond_1
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private x()V
    .locals 2

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Lb/a/a/a/a/a/b/e/d$f;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$f;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private z()V
    .locals 2

    .line 1
    const-string v0, "CSJ_VIDEO_MEDIA"

    .line 2
    .line 3
    const-string v1, "[video] MediaPlayerProxy#start first play prepare invoke !"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lb/a/a/a/a/a/b/e/d$n;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lb/a/a/a/a/a/b/e/d$n;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public A()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->i:Z

    .line 10
    .line 11
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->a()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 15
    .line 16
    if-eqz v0, :cond_2

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 27
    .line 28
    const/16 v1, 0x67

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    .line 32
    .line 33
    :cond_1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->v()V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catchall_0
    move-exception v0

    .line 38
    :try_start_1
    const-string v1, "CSJ_VIDEO_MEDIA"

    .line 39
    .line 40
    const-string v2, "release error: "

    .line 41
    .line 42
    invoke-static {v1, v2, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->v()V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :catchall_1
    move-exception v0

    .line 50
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->v()V

    .line 51
    .line 52
    .line 53
    throw v0

    .line 54
    :cond_2
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public D()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v1, "[video] MediaPlayerProxy#restart:"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    iget v1, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "CSJ_VIDEO_MEDIA"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 33
    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    .line 39
    const/4 v1, 0x1

    .line 40
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 41
    .line 42
    .line 43
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 44
    .line 45
    const/16 v2, 0xce

    .line 46
    .line 47
    if-ne v0, v2, :cond_2

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->C()V

    .line 51
    .line 52
    .line 53
    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    .line 55
    .line 56
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->E:Lb/a/a/a/a/a/b/e/d$o;

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/b/e/d$o;->〇o00〇〇Oo(Z)V

    .line 59
    .line 60
    .line 61
    const-wide/16 v0, 0x0

    .line 62
    .line 63
    invoke-direct {p0, v0, v1}, Lb/a/a/a/a/a/b/e/d;->a(J)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 67
    .line 68
    if-eqz v0, :cond_3

    .line 69
    .line 70
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    .line 71
    .line 72
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 76
    .line 77
    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    .line 78
    .line 79
    iget v2, p0, Lb/a/a/a/a/a/b/e/d;->A:I

    .line 80
    .line 81
    int-to-long v2, v2

    .line 82
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    .line 84
    .line 85
    :cond_3
    :goto_0
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method public a(I)V
    .locals 0

    .line 222
    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->d:I

    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .line 34
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 35
    :cond_0
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->a:Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x1

    .line 36
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Z)V

    .line 37
    new-instance v0, Lb/a/a/a/a/a/b/e/d$b;

    invoke-direct {v0, p0, p1}, Lb/a/a/a/a/a/b/e/d$b;-><init>(Lb/a/a/a/a/a/b/e/d;Landroid/graphics/SurfaceTexture;)V

    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    .line 42
    iget v2, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 43
    iget v3, v0, Landroid/os/Message;->what:I

    .line 44
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[video]  execute , mCurrentState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " handlerMsg="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CSJ_VIDEO_MEDIA"

    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v4, :cond_12

    .line 46
    iget v4, v0, Landroid/os/Message;->what:I

    const/16 v8, 0xcb

    const/16 v11, 0xc9

    const/16 v12, 0xca

    const/16 v13, 0xcd

    const/16 v14, 0xd0

    const/16 v15, 0xd1

    const/16 v9, 0xce

    const/16 v10, 0xcf

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_5

    :pswitch_1
    const-string v4, "OP_SET_SURFACE"

    .line 47
    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :try_start_0
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 49
    new-instance v4, Landroid/view/Surface;

    invoke-direct {v4, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v4, v1, Lb/a/a/a/a/a/b/e/d;->C:Landroid/view/Surface;

    .line 50
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->C:Landroid/view/Surface;

    invoke-interface {v0, v4}, Lb/a/a/a/a/a/b/e/c;->a(Landroid/view/Surface;)V

    .line 51
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0, v6}, Lb/a/a/a/a/a/b/e/c;->a(Z)V

    .line 52
    invoke-direct/range {p0 .. p0}, Lb/a/a/a/a/a/b/e/d;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    const-string v4, "OP_SET_SURFACE error: "

    .line 53
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_2
    const-string v4, "OP_SET_DISPLAY"

    .line 54
    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :try_start_1
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/SurfaceHolder;

    .line 56
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v4, v0}, Lb/a/a/a/a/a/b/e/c;->oO80(Landroid/view/SurfaceHolder;)V

    .line 57
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0, v6}, Lb/a/a/a/a/a/b/e/c;->a(Z)V

    .line 58
    invoke-direct/range {p0 .. p0}, Lb/a/a/a/a/a/b/e/d;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_5

    :catchall_1
    move-exception v0

    const-string v4, "OP_SET_DISPLAY error: "

    .line 59
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_3
    const-string v4, "OP_SET_DATASOURCE"

    .line 60
    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-direct/range {p0 .. p0}, Lb/a/a/a/a/a/b/e/d;->C()V

    .line 62
    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v4, v11, :cond_0

    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v4, v8, :cond_f

    .line 63
    :cond_0
    :try_start_2
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lb/a/a/a/a/a/a/f/c;

    .line 64
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 65
    invoke-static {}, Lb/a/a/a/a/a/a/c;->oO80()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lb/a/a/a/a/a/a/f/c;->c(Ljava/lang/String;)V

    .line 66
    :cond_1
    new-instance v4, Ljava/io/File;

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setDataSource\uff1a try paly local:"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-static {}, Lb/a/a/a/a/a/a/c;->OO0o〇〇〇〇0()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 71
    :cond_2
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lb/a/a/a/a/a/b/e/c;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 72
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setDataSource\uff1a paly net:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget v4, v0, Lb/a/a/a/a/a/a/f/c;->i:I

    const/16 v8, 0x17

    if-ne v4, v6, :cond_4

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v4, v8, :cond_4

    .line 74
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lb/a/a/a/a/a/b/e/c;->a(Ljava/lang/String;)V

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setDataSource\uff1a  url"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_4
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v8, :cond_5

    .line 77
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v4, v0}, Lb/a/a/a/a/a/b/e/c;->a(Lb/a/a/a/a/a/a/f/c;)V

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setDataSource\uff1a MediaDataSource url"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_5
    invoke-static {}, Lb/a/a/a/a/a/b/c/q/a;->〇o〇()Lb/a/a/a/a/a/b/c/q/a;

    move-result-object v4

    invoke-virtual {v4, v0}, Lb/a/a/a/a/a/b/c/q/a;->O8(Lb/a/a/a/a/a/a/f/c;)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v8, "setDataSource\uff1a  local url = "

    aput-object v8, v4, v7

    aput-object v0, v4, v6

    .line 80
    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->〇〇888(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_6

    .line 81
    invoke-static {}, Lb/a/a/a/a/a/a/c;->OO0o〇〇〇〇0()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "file"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 82
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_6
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v4, v0}, Lb/a/a/a/a/a/b/e/c;->a(Ljava/lang/String;)V

    .line 85
    :goto_0
    iput v12, v1, Lb/a/a/a/a/a/b/e/d;->j:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/16 :goto_5

    :catchall_2
    move-exception v0

    const-string v4, "OP_SET_DATASOURCE error: "

    .line 86
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_4
    const-string v4, "OP_SEEKTO"

    .line 87
    invoke-static {v5, v4}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v4, v9, :cond_7

    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v4, v10, :cond_7

    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v4, v15, :cond_f

    .line 89
    :cond_7
    :try_start_3
    iget-object v4, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->d:I

    invoke-interface {v4, v8, v9, v0}, Lb/a/a/a/a/a/b/e/c;->a(JI)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto/16 :goto_5

    :catchall_3
    move-exception v0

    const-string v4, "OP_SEEKTO error: "

    .line 90
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_5
    const-string v0, "OP_STOP"

    .line 91
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v13, :cond_8

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v9, :cond_8

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v14, :cond_8

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v10, :cond_8

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v0, v15, :cond_f

    .line 93
    :cond_8
    :try_start_4
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->e()V

    .line 94
    iput v14, v1, Lb/a/a/a/a/a/b/e/d;->j:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto/16 :goto_5

    :catchall_4
    move-exception v0

    const-string v4, "OP_STOP error: "

    .line 95
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_6
    const-string v0, "OP_PREPARE_ASYNC"

    .line 96
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v12, :cond_9

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v0, v14, :cond_f

    .line 98
    :cond_9
    :try_start_5
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->g()V

    const-string v0, "[video] OP_PREPARE_ASYNC execute , mMediaPlayer real prepareAsync !"

    .line 99
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto/16 :goto_5

    :catchall_5
    move-exception v0

    const-string v4, "OP_PREPARE_ASYNC error: "

    .line 100
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_7
    const-string v0, "OP_RELEASE"

    .line 101
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lb/a/a/a/a/a/b/e/d;->B()V

    const-string v0, "[video] OP_RELEASE execute , releaseMediaplayer !"

    .line 103
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    goto :goto_1

    :catchall_6
    move-exception v0

    const-string v4, "OP_RELEASE error: "

    .line 104
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    :goto_1
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_a

    .line 106
    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 107
    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v4, v1}, Lb/a/a/a/a/a/a/a$a;->b(Lb/a/a/a/a/a/a/a;)V

    goto :goto_2

    .line 108
    :cond_b
    iput v8, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    goto/16 :goto_5

    :pswitch_8
    const-string v0, "OP_RESET"

    .line 109
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :try_start_7
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->i()V

    const-string v0, "[video] OP_RESET execute!"

    .line 111
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iput v11, v1, Lb/a/a/a/a/a/b/e/d;->j:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    goto/16 :goto_5

    :catchall_7
    move-exception v0

    const-string v4, "OP_RESET error: "

    .line 113
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    :pswitch_9
    const-string v0, "OP_PAUSE"

    .line 114
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-boolean v0, v1, Lb/a/a/a/a/a/b/e/d;->m:Z

    if-eqz v0, :cond_c

    .line 116
    iget-wide v11, v1, Lb/a/a/a/a/a/b/e/d;->n:J

    iget-wide v13, v1, Lb/a/a/a/a/a/b/e/d;->p:J

    add-long/2addr v11, v13

    iput-wide v11, v1, Lb/a/a/a/a/a/b/e/d;->n:J

    .line 117
    :cond_c
    iput-boolean v7, v1, Lb/a/a/a/a/a/b/e/d;->m:Z

    const-wide/16 v11, 0x0

    .line 118
    iput-wide v11, v1, Lb/a/a/a/a/a/b/e/d;->p:J

    const-wide/high16 v11, -0x8000000000000000L

    .line 119
    iput-wide v11, v1, Lb/a/a/a/a/a/b/e/d;->o:J

    .line 120
    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v9, :cond_d

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v10, :cond_d

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v0, v15, :cond_f

    :cond_d
    :try_start_8
    const-string v0, "[video] OP_PAUSE execute , mMediaPlayer  OP_PAUSE !"

    .line 121
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->d()V

    .line 123
    iput v10, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 124
    iput-boolean v7, v1, Lb/a/a/a/a/a/b/e/d;->F:Z

    .line 125
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_e
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_e

    .line 126
    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_e

    .line 127
    invoke-virtual {v4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v4, v1}, Lb/a/a/a/a/a/a/a$a;->d(Lb/a/a/a/a/a/a/a;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    goto :goto_3

    :catchall_8
    move-exception v0

    const-string v4, "OP_PAUSE error: "

    .line 128
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    :pswitch_a
    const-string v0, "OP_START"

    .line 129
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v13, :cond_10

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-eq v0, v10, :cond_10

    iget v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    if-ne v0, v15, :cond_f

    goto :goto_4

    :cond_f
    const/4 v7, 0x1

    goto :goto_5

    .line 131
    :cond_10
    :goto_4
    :try_start_9
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->f()V

    .line 132
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iput-wide v10, v1, Lb/a/a/a/a/a/b/e/d;->r:J

    const-string v0, "[video] OP_START execute , mMediaPlayer real start !"

    .line 133
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iput v9, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 135
    iget-wide v8, v1, Lb/a/a/a/a/a/b/e/d;->k:J

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-lez v0, :cond_11

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[video] OP_START, seekTo:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v8, v1, Lb/a/a/a/a/a/b/e/d;->k:J

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    iget-wide v8, v1, Lb/a/a/a/a/a/b/e/d;->k:J

    iget v4, v1, Lb/a/a/a/a/a/b/e/d;->d:I

    invoke-interface {v0, v8, v9, v4}, Lb/a/a/a/a/a/b/e/c;->a(JI)V

    const-wide/16 v8, -0x1

    .line 138
    iput-wide v8, v1, Lb/a/a/a/a/a/b/e/d;->k:J

    .line 139
    :cond_11
    iget-object v0, v1, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    if-eqz v0, :cond_12

    .line 140
    iget-boolean v0, v1, Lb/a/a/a/a/a/b/e/d;->z:Z

    invoke-virtual {v1, v0}, Lb/a/a/a/a/a/b/e/d;->a(Z)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    goto :goto_5

    :catchall_9
    move-exception v0

    const-string v4, "OP_START error: "

    .line 141
    invoke-static {v5, v4, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_12
    :goto_5
    if-eqz v7, :cond_15

    const-string v0, "wrongState"

    .line 142
    invoke-static {v5, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xc8

    .line 143
    iput v0, v1, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 144
    iget-boolean v0, v1, Lb/a/a/a/a/a/b/e/d;->g:Z

    if-nez v0, :cond_15

    .line 145
    new-instance v0, Lb/a/a/a/a/a/a/f/a;

    const/16 v4, 0x134

    invoke-direct {v0, v4, v3}, Lb/a/a/a/a/a/a/f/a;-><init>(II)V

    .line 146
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lb/a/a/a/a/a/a/f/a;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 147
    iget-object v2, v1, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    if-eqz v3, :cond_13

    .line 148
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 149
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v3, v1, v0}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;Lb/a/a/a/a/a/a/f/a;)V

    goto :goto_6

    .line 150
    :cond_14
    iput-boolean v6, v1, Lb/a/a/a/a/a/b/e/d;->g:Z

    :cond_15
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 38
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 39
    :cond_0
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->b:Landroid/view/SurfaceHolder;

    const/4 v0, 0x1

    .line 40
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Z)V

    .line 41
    new-instance v0, Lb/a/a/a/a/a/b/e/d$c;

    invoke-direct {v0, p0, p1}, Lb/a/a/a/a/a/b/e/d$c;-><init>(Lb/a/a/a/a/a/b/e/d;Landroid/view/SurfaceHolder;)V

    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/a$a;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 220
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_1

    return-void

    .line 221
    :cond_2
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lb/a/a/a/a/a/b/e/c;)V
    .locals 3

    const-string p1, "CSJ_VIDEO_MEDIA"

    .line 184
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0xcd

    .line 185
    iput v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 186
    :try_start_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->h()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 188
    new-instance v1, Lb/a/a/a/a/a/a/b;

    invoke-direct {v1}, Lb/a/a/a/a/a/a/b;-><init>()V

    .line 189
    invoke-virtual {v1, v0}, Lb/a/a/a/a/a/a/b;->〇o00〇〇Oo(F)V

    .line 190
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0, v1}, Lb/a/a/a/a/a/b/e/c;->〇80〇808〇O(Lb/a/a/a/a/a/a/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "speed error: "

    .line 191
    invoke-static {p1, v1, v0}, Lb/a/a/a/a/a/a/i/c;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 192
    :cond_1
    :goto_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz v0, :cond_3

    .line 193
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    if-eqz v0, :cond_2

    .line 194
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->x()V

    goto :goto_1

    :cond_2
    const-string v0, "onPrepared op_Start"

    .line 195
    invoke-static {p1, v0}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    const/16 v1, 0x64

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 197
    :cond_3
    :goto_1
    sget-object v0, Lb/a/a/a/a/a/b/e/d;->J:Landroid/util/SparseIntArray;

    iget v1, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPrepared:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lb/a/a/a/a/a/b/e/d;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    if-nez p1, :cond_4

    iget-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->s:Z

    if-nez p1, :cond_4

    .line 200
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->j()V

    const/4 p1, 0x1

    .line 201
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->s:Z

    .line 202
    :cond_4
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    .line 203
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 204
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v0, p0}, Lb/a/a/a/a/a/a/a$a;->c(Lb/a/a/a/a/a/a/a;)V

    goto :goto_2

    :cond_6
    return-void
.end method

.method public a(Lb/a/a/a/a/a/b/e/c;I)V
    .locals 2

    .line 154
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    if-eq v0, p1, :cond_0

    return-void

    .line 155
    :cond_0
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 156
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 157
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v0, p0, p2}, Lb/a/a/a/a/a/a/a$a;->b(Lb/a/a/a/a/a/a/a;I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(Lb/a/a/a/a/a/b/e/c;IIII)V
    .locals 0

    .line 216
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/ref/WeakReference;

    if-eqz p4, :cond_0

    .line 217
    invoke-virtual {p4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p5

    if-eqz p5, :cond_0

    .line 218
    invoke-virtual {p4}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {p4, p0, p2, p3}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 2

    .line 212
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-nez v0, :cond_1

    const-string p1, "CSJ_VIDEO_MEDIA"

    const-string v0, "quietPlay set opHandler is null"

    .line 214
    invoke-static {p1, v0}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 215
    :cond_1
    new-instance v1, Lb/a/a/a/a/a/b/e/d$g;

    invoke-direct {v1, p0, p1}, Lb/a/a/a/a/a/b/e/d$g;-><init>(Lb/a/a/a/a/a/b/e/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(ZJZ)V
    .locals 4

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[video] MediaPlayerProxy#start firstSeekToPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",isFirst :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ",isPauseOtherMusicVolume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CSJ_VIDEO_MEDIA"

    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 15
    :cond_1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->t()V

    .line 16
    iput-boolean p4, p0, Lb/a/a/a/a/a/b/e/d;->z:Z

    .line 17
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 18
    iput-boolean v3, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    .line 19
    invoke-virtual {p0, p4}, Lb/a/a/a/a/a/b/e/d;->a(Z)V

    if-eqz p1, :cond_2

    const-string p1, "[video] first start , SSMediaPlayer  start method !"

    .line 20
    invoke-static {v1, p1}, Lb/a/a/a/a/a/a/i/c;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    iput-wide p2, p0, Lb/a/a/a/a/a/b/e/d;->k:J

    .line 22
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->z()V

    goto :goto_1

    .line 23
    :cond_2
    invoke-direct {p0, p2, p3}, Lb/a/a/a/a/a/b/e/d;->a(J)V

    .line 24
    :goto_1
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz p1, :cond_3

    .line 25
    iget-object p2, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 26
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    iget-object p2, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    iget p3, p0, Lb/a/a/a/a/a/b/e/d;->A:I

    int-to-long p3, p3

    invoke-virtual {p1, p2, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    return-void
.end method

.method public a(Lb/a/a/a/a/a/b/e/c;II)Z
    .locals 4

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "what,extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CSJ_VIDEO_MEDIA"

    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_0

    return v1

    :cond_0
    const/16 p1, -0x3ec

    if-ne p3, p1, :cond_2

    .line 160
    new-instance p1, Lb/a/a/a/a/a/a/f/a;

    invoke-direct {p1, p2, p3}, Lb/a/a/a/a/a/a/f/a;-><init>(II)V

    .line 161
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_1

    .line 162
    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 163
    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v2, p0, p1}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;Lb/a/a/a/a/a/a/f/a;)V

    goto :goto_0

    .line 164
    :cond_2
    invoke-direct {p0, p2, p3}, Lb/a/a/a/a/a/b/e/d;->a(II)V

    return v1
.end method

.method public b()I
    .locals 1

    .line 38
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->b()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)V
    .locals 1

    .line 40
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 41
    :cond_0
    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->A:I

    return-void
.end method

.method public b(J)V
    .locals 2

    .line 10
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 11
    :cond_0
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_1

    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xce

    if-eq v0, v1, :cond_1

    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_2

    .line 12
    :cond_1
    new-instance v0, Lb/a/a/a/a/a/b/e/d$a;

    invoke-direct {v0, p0, p1, p2}, Lb/a/a/a/a/a/b/e/d$a;-><init>(Lb/a/a/a/a/a/b/e/d;J)V

    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method public b(Lb/a/a/a/a/a/a/f/c;)V
    .locals 1

    .line 13
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 14
    :cond_0
    iput-object p1, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    if-eqz p1, :cond_2

    .line 15
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lb/a/a/a/a/a/a/f/c;->s()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    .line 16
    :cond_2
    new-instance v0, Lb/a/a/a/a/a/b/e/d$d;

    invoke-direct {v0, p0, p1}, Lb/a/a/a/a/a/b/e/d$d;-><init>(Lb/a/a/a/a/a/b/e/d;Lb/a/a/a/a/a/a/f/c;)V

    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lb/a/a/a/a/a/b/e/c;)V
    .locals 2

    .line 31
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/a/a/a/a/a/a$a;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 2

    .line 4
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 5
    :cond_0
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/e/d;->w:Z

    .line 6
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0, p1}, Lb/a/a/a/a/a/b/e/c;->d(Z)V

    goto :goto_0

    .line 8
    :cond_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz v0, :cond_2

    .line 9
    new-instance v1, Lb/a/a/a/a/a/b/e/d$i;

    invoke-direct {v1, p0, p1}, Lb/a/a/a/a/a/b/e/d$i;-><init>(Lb/a/a/a/a/a/b/e/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public b(Lb/a/a/a/a/a/b/e/c;II)Z
    .locals 2

    .line 17
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "what="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "extra="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "CSJ_VIDEO_MEDIA"

    invoke-static {v0, p1}, Lb/a/a/a/a/a/a/i/c;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->s()V

    const/16 p1, 0xc8

    .line 19
    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 20
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz p1, :cond_0

    .line 21
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 22
    :cond_0
    invoke-direct {p0, p2, p3}, Lb/a/a/a/a/a/b/e/d;->b(II)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 23
    invoke-direct {p0}, Lb/a/a/a/a/a/b/e/d;->v()V

    .line 24
    :cond_1
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_2

    return v0

    .line 25
    :cond_2
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 26
    new-instance p1, Lb/a/a/a/a/a/a/f/a;

    invoke-direct {p1, p2, p3}, Lb/a/a/a/a/a/a/f/a;-><init>(II)V

    .line 27
    iget-object p2, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/ref/WeakReference;

    if-eqz p3, :cond_3

    .line 28
    invoke-virtual {p3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 29
    invoke-virtual {p3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {p3, p0, p1}, Lb/a/a/a/a/a/a/a$a;->a(Lb/a/a/a/a/a/a/a;Lb/a/a/a/a/a/a/f/a;)V

    goto :goto_0

    :cond_4
    return v0
.end method

.method public c()I
    .locals 1

    .line 11
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->c()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c(Lb/a/a/a/a/a/b/e/c;)V
    .locals 2

    const/16 p1, 0xd1

    .line 4
    iput p1, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 5
    sget-object p1, Lb/a/a/a/a/a/b/e/d;->J:Landroid/util/SparseIntArray;

    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->u:I

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 6
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz p1, :cond_0

    .line 7
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->D:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 8
    :cond_0
    iget-object p1, p0, Lb/a/a/a/a/a/b/e/d;->x:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 10
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/a/a/a/a/a/a/a$a;

    invoke-interface {v0, p0}, Lb/a/a/a/a/a/a/a$a;->e(Lb/a/a/a/a/a/a/a;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public d()Z
    .locals 2

    .line 4
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->e:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .line 2
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->u()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->h()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public g()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->i:Z

    return v0
.end method

.method public h()Z
    .locals 2

    .line 2
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz v0, :cond_1

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 2

    .line 2
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    if-eqz v0, :cond_1

    const/16 v1, 0x64

    .line 3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()I
    .locals 1

    .line 1
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->c:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public n()J
    .locals 4

    .line 1
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-wide v1

    .line 10
    :cond_0
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 11
    .line 12
    const/16 v3, 0xce

    .line 13
    .line 14
    if-eq v0, v3, :cond_1

    .line 15
    .line 16
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 17
    .line 18
    const/16 v3, 0xcf

    .line 19
    .line 20
    if-ne v0, v3, :cond_2

    .line 21
    .line 22
    :cond_1
    :try_start_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 23
    .line 24
    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->h()J

    .line 25
    .line 26
    .line 27
    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    return-wide v0

    .line 29
    :catchall_0
    :cond_2
    return-wide v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public o()Landroid/view/SurfaceHolder;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->b:Landroid/view/SurfaceHolder;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public p()Landroid/graphics/SurfaceTexture;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->a:Landroid/graphics/SurfaceTexture;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public q()J
    .locals 5

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x17

    .line 4
    .line 5
    if-lt v0, v1, :cond_1

    .line 6
    .line 7
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->m:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->p:J

    .line 12
    .line 13
    const-wide/16 v2, 0x0

    .line 14
    .line 15
    cmp-long v4, v0, v2

    .line 16
    .line 17
    if-lez v4, :cond_0

    .line 18
    .line 19
    iget-wide v2, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    .line 20
    .line 21
    add-long/2addr v2, v0

    .line 22
    return-wide v2

    .line 23
    :cond_0
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->n:J

    .line 24
    .line 25
    return-wide v0

    .line 26
    :cond_1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->H:J

    .line 27
    .line 28
    return-wide v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public r()J
    .locals 5

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->q:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-eqz v4, :cond_0

    .line 8
    .line 9
    return-wide v0

    .line 10
    :cond_0
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 11
    .line 12
    const/16 v1, 0xce

    .line 13
    .line 14
    if-eq v0, v1, :cond_1

    .line 15
    .line 16
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 17
    .line 18
    const/16 v1, 0xcf

    .line 19
    .line 20
    if-ne v0, v1, :cond_2

    .line 21
    .line 22
    :cond_1
    :try_start_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->f:Lb/a/a/a/a/a/b/e/c;

    .line 23
    .line 24
    invoke-interface {v0}, Lb/a/a/a/a/a/b/e/c;->a()J

    .line 25
    .line 26
    .line 27
    move-result-wide v0

    .line 28
    iput-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    .line 30
    :catchall_0
    :cond_2
    iget-wide v0, p0, Lb/a/a/a/a/a/b/e/d;->q:J

    .line 31
    .line 32
    return-wide v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public u()Z
    .locals 2

    .line 1
    iget v0, p0, Lb/a/a/a/a/a/b/e/d;->j:I

    .line 2
    .line 3
    const/16 v1, 0xcd

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public w()V
    .locals 2

    .line 1
    const-string v0, "CSJ_VIDEO_MEDIA"

    .line 2
    .line 3
    const-string v1, "pause: from outer"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 16
    .line 17
    if-eqz v0, :cond_6

    .line 18
    .line 19
    const/16 v1, 0x64

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 22
    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->F:Z

    .line 26
    .line 27
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->I:Z

    .line 28
    .line 29
    const/16 v1, 0x65

    .line 30
    .line 31
    if-nez v0, :cond_3

    .line 32
    .line 33
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->s:Z

    .line 34
    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    .line 38
    .line 39
    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->a(Lb/a/a/a/a/a/a/f/c;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    new-instance v0, Lb/a/a/a/a/a/b/e/d$l;

    .line 47
    .line 48
    invoke-direct {v0, p0}, Lb/a/a/a/a/a/b/e/d$l;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/Runnable;)V

    .line 52
    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_2
    :goto_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 56
    .line 57
    if-eqz v0, :cond_6

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 60
    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_3
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/e/d;->e:Z

    .line 64
    .line 65
    if-nez v0, :cond_5

    .line 66
    .line 67
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->y:Lb/a/a/a/a/a/a/f/c;

    .line 68
    .line 69
    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->a(Lb/a/a/a/a/a/a/f/c;)Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-eqz v0, :cond_4

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_4
    new-instance v0, Lb/a/a/a/a/a/b/e/d$m;

    .line 77
    .line 78
    invoke-direct {v0, p0}, Lb/a/a/a/a/a/b/e/d$m;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0, v0}, Lb/a/a/a/a/a/b/e/d;->a(Ljava/lang/Runnable;)V

    .line 82
    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_5
    :goto_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 86
    .line 87
    if-eqz v0, :cond_6

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 90
    .line 91
    .line 92
    :cond_6
    :goto_2
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method public y()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/e/d;->g()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lb/a/a/a/a/a/b/e/d;->l:Lcom/bytedance/sdk/component/utils/y;

    .line 19
    .line 20
    new-instance v1, Lb/a/a/a/a/a/b/e/d$k;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/e/d$k;-><init>(Lb/a/a/a/a/a/b/e/d;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 26
    .line 27
    .line 28
    :cond_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.class public Lb/a/a/a/a/a/b/b/b/b;
.super Ljava/lang/Object;
.source "MediaDownloadPlayCacheImpl.java"

# interfaces
.implements Lb/a/a/a/a/a/b/b/b/c;


# instance fields
.field private O8:Ljava/io/File;

.field private final OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

.field private Oo08:J

.field private volatile oO80:Z

.field private volatile o〇0:J

.field private volatile 〇080:J

.field private 〇80〇808〇O:Ljava/io/RandomAccessFile;

.field private final 〇o00〇〇Oo:Ljava/lang/Object;

.field private 〇o〇:Ljava/io/File;

.field private volatile 〇〇888:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lb/a/a/a/a/a/a/f/c;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/32 v0, -0x80000000

    .line 5
    .line 6
    .line 7
    iput-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 8
    .line 9
    new-instance p1, Ljava/lang/Object;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 15
    .line 16
    const-wide/16 v0, 0x0

    .line 17
    .line 18
    iput-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->Oo08:J

    .line 19
    .line 20
    const-wide/16 v0, -0x1

    .line 21
    .line 22
    iput-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->o〇0:J

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇〇888:Z

    .line 26
    .line 27
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/b/b/b;->oO80:Z

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 31
    .line 32
    iput-object p2, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 33
    .line 34
    :try_start_0
    invoke-virtual {p2}, Lb/a/a/a/a/a/a/f/c;->b()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {p2}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-static {v0, v1}, Lb/a/a/a/a/a/b/f/b;->O8(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iput-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 47
    .line 48
    invoke-virtual {p2}, Lb/a/a/a/a/a/a/f/c;->b()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {p2}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-static {v0, v1}, Lb/a/a/a/a/a/b/f/b;->〇o〇(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 61
    .line 62
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇8o8o〇()Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    .line 68
    new-instance v0, Ljava/io/RandomAccessFile;

    .line 69
    .line 70
    iget-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 71
    .line 72
    const-string v2, "r"

    .line 73
    .line 74
    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    iput-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_0
    new-instance v0, Ljava/io/RandomAccessFile;

    .line 81
    .line 82
    iget-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 83
    .line 84
    const-string v2, "rw"

    .line 85
    .line 86
    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 90
    .line 91
    :goto_0
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇8o8o〇()Z

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    if-nez v0, :cond_1

    .line 96
    .line 97
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 98
    .line 99
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 100
    .line 101
    .line 102
    move-result-wide v0

    .line 103
    iput-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->Oo08:J

    .line 104
    .line 105
    invoke-virtual {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    .line 107
    .line 108
    goto :goto_1

    .line 109
    :catchall_0
    const/4 v0, 0x3

    .line 110
    new-array v0, v0, [Ljava/lang/Object;

    .line 111
    .line 112
    const-string v1, "Error using file "

    .line 113
    .line 114
    aput-object v1, v0, p1

    .line 115
    .line 116
    invoke-virtual {p2}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    const/4 p2, 0x1

    .line 121
    aput-object p1, v0, p2

    .line 122
    .line 123
    const/4 p1, 0x2

    .line 124
    const-string p2, " as disc cache"

    .line 125
    .line 126
    aput-object p2, v0, p1

    .line 127
    .line 128
    const-string p1, "CSJ_MediaDLPlay"

    .line 129
    .line 130
    invoke-static {p1, v0}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    .line 132
    .line 133
    :cond_1
    :goto_1
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method static synthetic O8(Lb/a/a/a/a/a/b/b/b/b;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lb/a/a/a/a/a/b/b/b/b;->oO80:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic OO0o〇〇(Lb/a/a/a/a/a/b/b/b/b;)Ljava/io/RandomAccessFile;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic OO0o〇〇〇〇0(Lb/a/a/a/a/a/b/b/b/b;)Lb/a/a/a/a/a/a/f/c;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic Oo08(Lb/a/a/a/a/a/b/b/b/b;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lb/a/a/a/a/a/b/b/b/b;->oO80:Z

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method static synthetic Oooo8o0〇(Lb/a/a/a/a/a/b/b/b/b;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->o〇0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic oO80(Lb/a/a/a/a/a/b/b/b/b;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->Oo08:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic o〇0(Lb/a/a/a/a/a/b/b/b/b;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇8o8o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇O8o08O(Lb/a/a/a/a/a/b/b/b/b;)Ljava/lang/Object;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇O〇(Lb/a/a/a/a/a/b/b/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇〇888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇o00〇〇Oo()J
    .locals 2

    .line 1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    return-wide v0

    .line 14
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    return-wide v0
.end method

.method static synthetic 〇o〇(Lb/a/a/a/a/a/b/b/b/b;J)J
    .locals 0

    .line 1
    iput-wide p1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 2
    .line 3
    return-wide p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method static synthetic 〇〇808〇(Lb/a/a/a/a/a/b/b/b/b;)Ljava/io/File;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇〇888()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇8o8o〇()Z

    .line 5
    .line 6
    .line 7
    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 8
    const/4 v2, 0x2

    .line 9
    const/4 v3, 0x1

    .line 10
    const/4 v4, 0x0

    .line 11
    const/4 v5, 0x3

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v1, "CSJ_MediaDLPlay"

    .line 15
    .line 16
    :try_start_1
    new-array v5, v5, [Ljava/lang/Object;

    .line 17
    .line 18
    const-string v6, "complete: isCompleted "

    .line 19
    .line 20
    aput-object v6, v5, v4

    .line 21
    .line 22
    iget-object v4, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 23
    .line 24
    invoke-virtual {v4}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    aput-object v4, v5, v3

    .line 29
    .line 30
    iget-object v3, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 31
    .line 32
    invoke-virtual {v3}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    aput-object v3, v5, v2

    .line 37
    .line 38
    invoke-static {v1, v5}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 42
    return-void

    .line 43
    :cond_0
    :try_start_2
    iget-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 44
    .line 45
    iget-object v6, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 46
    .line 47
    invoke-virtual {v1, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_2

    .line 52
    .line 53
    iget-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 54
    .line 55
    if-eqz v1, :cond_1

    .line 56
    .line 57
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 58
    .line 59
    .line 60
    :cond_1
    new-instance v1, Ljava/io/RandomAccessFile;

    .line 61
    .line 62
    iget-object v6, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 63
    .line 64
    const-string v7, "rw"

    .line 65
    .line 66
    invoke-direct {v1, v6, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    iput-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 70
    .line 71
    const-string v1, "CSJ_MediaDLPlay"

    .line 72
    .line 73
    :try_start_3
    new-array v5, v5, [Ljava/lang/Object;

    .line 74
    .line 75
    const-string v6, "complete: rename "

    .line 76
    .line 77
    aput-object v6, v5, v4

    .line 78
    .line 79
    iget-object v4, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 80
    .line 81
    invoke-virtual {v4}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    aput-object v4, v5, v3

    .line 86
    .line 87
    iget-object v3, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 88
    .line 89
    invoke-virtual {v3}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v3

    .line 93
    aput-object v3, v5, v2

    .line 94
    .line 95
    invoke-static {v1, v5}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_2
    new-instance v1, Ljava/io/IOException;

    .line 100
    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v3, "Error renaming file "

    .line 107
    .line 108
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    iget-object v3, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 112
    .line 113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    const-string v3, " to "

    .line 117
    .line 118
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    iget-object v3, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 122
    .line 123
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v3, " for completion!"

    .line 127
    .line 128
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :catchall_0
    move-exception v1

    .line 140
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 141
    .line 142
    .line 143
    const-string v2, "CSJ_MediaDLPlay"

    .line 144
    .line 145
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-static {v2, v1}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 150
    .line 151
    .line 152
    :goto_0
    :try_start_6
    monitor-exit v0

    .line 153
    return-void

    .line 154
    :catchall_1
    move-exception v1

    .line 155
    throw v1

    .line 156
    :catchall_2
    move-exception v1

    .line 157
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 158
    throw v1
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method


# virtual methods
.method public close()V
    .locals 3

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇〇888:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o〇:Ljava/io/File;

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setLastModified(J)Z

    .line 19
    .line 20
    .line 21
    :cond_1
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 22
    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 26
    .line 27
    .line 28
    move-result-wide v1

    .line 29
    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setLastModified(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :catchall_0
    move-exception v0

    .line 34
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 35
    .line 36
    .line 37
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 38
    iput-boolean v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇〇888:Z

    .line 39
    .line 40
    return-void

    .line 41
    :catchall_1
    move-exception v0

    .line 42
    throw v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public length()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lb/a/a/a/a/a/b/b/b/b;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->O8:Ljava/io/File;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    iput-wide v2, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 18
    .line 19
    monitor-enter v0

    .line 20
    const/4 v2, 0x0

    .line 21
    :cond_1
    :try_start_0
    iget-wide v3, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .line 23
    const-wide/32 v5, -0x80000000

    .line 24
    .line 25
    .line 26
    cmp-long v7, v3, v5

    .line 27
    .line 28
    if-nez v7, :cond_2

    .line 29
    .line 30
    :try_start_1
    const-string v3, "CSJ_MediaDLPlay"

    .line 31
    .line 32
    const-string v4, "totalLength: wait"

    .line 33
    .line 34
    invoke-static {v3, v4}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    add-int/lit8 v2, v2, 0xf

    .line 38
    .line 39
    iget-object v3, p0, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 40
    .line 41
    const-wide/16 v4, 0x5

    .line 42
    .line 43
    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 44
    .line 45
    .line 46
    const/16 v3, 0x4e20

    .line 47
    .line 48
    if-le v2, v3, :cond_1

    .line 49
    .line 50
    :try_start_2
    monitor-exit v0

    .line 51
    const-wide/16 v0, -0x1

    .line 52
    .line 53
    return-wide v0

    .line 54
    :catch_0
    move-exception v1

    .line 55
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 56
    .line 57
    .line 58
    new-instance v1, Ljava/io/IOException;

    .line 59
    .line 60
    const-string v2, "total length InterruptException"

    .line 61
    .line 62
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    throw v1

    .line 66
    :cond_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 67
    :goto_0
    const/4 v0, 0x2

    .line 68
    new-array v0, v0, [Ljava/lang/Object;

    .line 69
    .line 70
    const-string v2, "totalLength= "

    .line 71
    .line 72
    aput-object v2, v0, v1

    .line 73
    .line 74
    iget-wide v1, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 75
    .line 76
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    const/4 v2, 0x1

    .line 81
    aput-object v1, v0, v2

    .line 82
    .line 83
    const-string v1, "CSJ_MediaDLPlay"

    .line 84
    .line 85
    invoke-static {v1, v0}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    .line 87
    .line 88
    iget-wide v0, p0, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 89
    .line 90
    return-wide v0

    .line 91
    :catchall_0
    move-exception v1

    .line 92
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    throw v1
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method public 〇080(J[BII)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-wide/from16 v2, p1

    .line 4
    .line 5
    :try_start_0
    iget-wide v4, v1, Lb/a/a/a/a/a/b/b/b/b;->〇080:J

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    cmp-long v6, v2, v4

    .line 9
    .line 10
    if-nez v6, :cond_0

    .line 11
    .line 12
    return v0

    .line 13
    :cond_0
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    const/4 v6, 0x0

    .line 16
    :goto_0
    iget-boolean v7, v1, Lb/a/a/a/a/a/b/b/b/b;->〇〇888:Z

    .line 17
    .line 18
    if-nez v7, :cond_4

    .line 19
    .line 20
    iget-object v7, v1, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 21
    .line 22
    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 23
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo()J

    .line 24
    .line 25
    .line 26
    move-result-wide v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 27
    cmp-long v10, v2, v8

    .line 28
    .line 29
    if-gez v10, :cond_1

    .line 30
    .line 31
    const-string v6, "CSJ_MediaDLPlay"

    .line 32
    .line 33
    :try_start_2
    new-instance v8, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v9, "read:  read "

    .line 39
    .line 40
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v9, " success"

    .line 47
    .line 48
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v8

    .line 55
    invoke-static {v6, v8}, Lb/a/a/a/a/a/a/i/c;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget-object v6, v1, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 59
    .line 60
    invoke-virtual {v6, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 61
    .line 62
    .line 63
    iget-object v6, v1, Lb/a/a/a/a/a/b/b/b/b;->〇80〇808〇O:Ljava/io/RandomAccessFile;

    .line 64
    .line 65
    move-object/from16 v10, p3

    .line 66
    .line 67
    move/from16 v11, p4

    .line 68
    .line 69
    move/from16 v12, p5

    .line 70
    .line 71
    invoke-virtual {v6, v10, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 72
    .line 73
    .line 74
    move-result v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    goto :goto_1

    .line 76
    :cond_1
    move-object/from16 v10, p3

    .line 77
    .line 78
    move/from16 v11, p4

    .line 79
    .line 80
    move/from16 v12, p5

    .line 81
    .line 82
    const-string v13, "CSJ_MediaDLPlay"

    .line 83
    .line 84
    const/4 v14, 0x4

    .line 85
    :try_start_3
    new-array v14, v14, [Ljava/lang/Object;

    .line 86
    .line 87
    const-string v15, "read: wait at "

    .line 88
    .line 89
    aput-object v15, v14, v4

    .line 90
    .line 91
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 92
    .line 93
    .line 94
    move-result-object v15

    .line 95
    const/16 v16, 0x1

    .line 96
    .line 97
    aput-object v15, v14, v16

    .line 98
    .line 99
    const-string v15, "  file size = "

    .line 100
    .line 101
    const/16 v16, 0x2

    .line 102
    .line 103
    aput-object v15, v14, v16

    .line 104
    .line 105
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 106
    .line 107
    .line 108
    move-result-object v8

    .line 109
    const/4 v9, 0x3

    .line 110
    aput-object v8, v14, v9

    .line 111
    .line 112
    invoke-static {v13, v14}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    add-int/lit8 v5, v5, 0x21

    .line 116
    .line 117
    iget-object v8, v1, Lb/a/a/a/a/a/b/b/b/b;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 118
    .line 119
    const-wide/16 v13, 0x21

    .line 120
    .line 121
    invoke-virtual {v8, v13, v14}, Ljava/lang/Object;->wait(J)V

    .line 122
    .line 123
    .line 124
    :goto_1
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 125
    if-lez v6, :cond_2

    .line 126
    .line 127
    return v6

    .line 128
    :cond_2
    const/16 v7, 0x4e20

    .line 129
    .line 130
    if-ge v5, v7, :cond_3

    .line 131
    .line 132
    goto :goto_0

    .line 133
    :cond_3
    :try_start_4
    new-instance v0, Ljava/net/SocketTimeoutException;

    .line 134
    .line 135
    invoke-direct {v0}, Ljava/net/SocketTimeoutException;-><init>()V

    .line 136
    .line 137
    .line 138
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 139
    :catchall_0
    move-exception v0

    .line 140
    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 141
    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 142
    :cond_4
    return v0

    .line 143
    :catchall_1
    move-exception v0

    .line 144
    :try_start_7
    instance-of v2, v0, Ljava/io/IOException;

    .line 145
    .line 146
    if-eqz v2, :cond_5

    .line 147
    .line 148
    check-cast v0, Ljava/io/IOException;

    .line 149
    .line 150
    throw v0

    .line 151
    :cond_5
    new-instance v0, Ljava/io/IOException;

    .line 152
    .line 153
    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    .line 154
    .line 155
    .line 156
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 157
    :catchall_2
    move-exception v0

    .line 158
    throw v0
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method public 〇80〇808〇O()V
    .locals 6

    .line 1
    invoke-static {}, Lb/a/a/a/a/a/a/c;->o〇0()Lb/b/a/a/f/a/j;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lb/a/a/a/a/a/a/c;->o〇0()Lb/b/a/a/f/a/j;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lb/b/a/a/f/a/j;->〇o〇()Lb/b/a/a/f/a/j$a;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    new-instance v0, Lb/b/a/a/f/a/j$a;

    .line 17
    .line 18
    const-string v1, "v_cache"

    .line 19
    .line 20
    invoke-direct {v0, v1}, Lb/b/a/a/f/a/j$a;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :goto_0
    iget-object v1, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 24
    .line 25
    invoke-virtual {v1}, Lb/a/a/a/a/a/a/f/c;->c()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    int-to-long v1, v1

    .line 30
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 31
    .line 32
    invoke-virtual {v0, v1, v2, v3}, Lb/b/a/a/f/a/j$a;->〇080(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iget-object v2, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 37
    .line 38
    invoke-virtual {v2}, Lb/a/a/a/a/a/a/f/c;->k()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    int-to-long v4, v2

    .line 43
    invoke-virtual {v1, v4, v5, v3}, Lb/b/a/a/f/a/j$a;->O8(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    iget-object v2, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 48
    .line 49
    invoke-virtual {v2}, Lb/a/a/a/a/a/a/f/c;->r()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    int-to-long v4, v2

    .line 54
    invoke-virtual {v1, v4, v5, v3}, Lb/b/a/a/f/a/j$a;->Oo08(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lb/b/a/a/f/a/j$a;->〇o〇()Lb/b/a/a/f/a/j;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const/4 v1, 0x4

    .line 62
    new-array v1, v1, [Ljava/lang/Object;

    .line 63
    .line 64
    const/4 v2, 0x0

    .line 65
    const-string v3, "RANGE, bytes="

    .line 66
    .line 67
    aput-object v3, v1, v2

    .line 68
    .line 69
    iget-wide v2, p0, Lb/a/a/a/a/a/b/b/b/b;->Oo08:J

    .line 70
    .line 71
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    const/4 v3, 0x1

    .line 76
    aput-object v2, v1, v3

    .line 77
    .line 78
    const/4 v2, 0x2

    .line 79
    const-string v3, " file hash="

    .line 80
    .line 81
    aput-object v3, v1, v2

    .line 82
    .line 83
    iget-object v2, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 84
    .line 85
    invoke-virtual {v2}, Lb/a/a/a/a/a/a/f/c;->e()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    const/4 v3, 0x3

    .line 90
    aput-object v2, v1, v3

    .line 91
    .line 92
    const-string v2, "CSJ_MediaDLPlay"

    .line 93
    .line 94
    invoke-static {v2, v1}, Lb/a/a/a/a/a/a/i/c;->〇8o8o〇(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    new-instance v1, Lb/b/a/a/f/a/l$a;

    .line 98
    .line 99
    invoke-direct {v1}, Lb/b/a/a/f/a/l$a;-><init>()V

    .line 100
    .line 101
    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    .line 103
    .line 104
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .line 106
    .line 107
    const-string v3, "bytes="

    .line 108
    .line 109
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    iget-wide v3, p0, Lb/a/a/a/a/a/b/b/b/b;->Oo08:J

    .line 113
    .line 114
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v3, "-"

    .line 118
    .line 119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    const-string v3, "RANGE"

    .line 127
    .line 128
    invoke-virtual {v1, v3, v2}, Lb/b/a/a/f/a/l$a;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/f/a/l$a;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    iget-object v2, p0, Lb/a/a/a/a/a/b/b/b/b;->OO0o〇〇〇〇0:Lb/a/a/a/a/a/a/f/c;

    .line 133
    .line 134
    invoke-virtual {v2}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    invoke-virtual {v1, v2}, Lb/b/a/a/f/a/l$a;->Oo08(Ljava/lang/String;)Lb/b/a/a/f/a/l$a;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lb/b/a/a/f/a/l$a;->〇80〇808〇O()Lb/b/a/a/f/a/l$a;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lb/b/a/a/f/a/l$a;->oO80()Lb/b/a/a/f/a/l;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {v0, v1}, Lb/b/a/a/f/a/j;->〇080(Lb/b/a/a/f/a/l;)Lb/b/a/a/f/a/b;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    new-instance v1, Lb/a/a/a/a/a/b/b/b/b$a;

    .line 155
    .line 156
    invoke-direct {v1, p0}, Lb/a/a/a/a/a/b/b/b/b$a;-><init>(Lb/a/a/a/a/a/b/b/b/b;)V

    .line 157
    .line 158
    .line 159
    invoke-interface {v0, v1}, Lb/b/a/a/f/a/b;->〇O00(Lb/b/a/a/f/a/c;)V

    .line 160
    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

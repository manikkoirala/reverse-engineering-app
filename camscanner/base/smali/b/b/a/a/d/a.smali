.class public Lb/b/a/a/d/a;
.super Ljava/lang/Object;
.source "DynamicDefaultPropertyManager.java"


# static fields
.field public static final 〇080:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static O000(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O08000(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"dynamic_creative.developer_name\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O0o〇〇Oo(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":0.5625,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"ad\",\"paddingRight\":2,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":3,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#2a90d7\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"openPrivacy\":false,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"union_logo\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O8ooOoo〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"time_down\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O8〇o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OO0o〇〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":8,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"interactSlideDirection\":\"2\",\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"paddingTop\":4,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":14,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":8,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OO0o〇〇〇〇0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.comment_num\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"score_count\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OO8oO0o〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"flex\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OOO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1.52,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OOO〇O0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":80,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":1,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static Oo08(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":3,\"interactSlideDirection\":\"2\",\"bgColor\":\"rgba(22, 24, 35, 0.2)\",\"paddingTop\":3,\"materialCenterData\":-999,\"openPrivacy\":false,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":2,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static Oo8Oo00oo(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static OoO8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"source|app.app_name\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"source\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static Ooo(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"Please enter the content\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static Oooo8o0〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"text\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"skip\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static O〇8O8〇008(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3

    .line 1
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-nez v1, :cond_3c

    .line 8
    .line 9
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, -0x1

    .line 17
    sparse-switch v1, :sswitch_data_0

    .line 18
    .line 19
    .line 20
    goto/16 :goto_0

    .line 21
    .line 22
    :sswitch_0
    const-string v1, "download-progress-button"

    .line 23
    .line 24
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-nez v1, :cond_0

    .line 29
    .line 30
    goto/16 :goto_0

    .line 31
    .line 32
    :cond_0
    const/16 v2, 0x3b

    .line 33
    .line 34
    goto/16 :goto_0

    .line 35
    .line 36
    :sswitch_1
    const-string v1, "score-count-type-2"

    .line 37
    .line 38
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-nez v1, :cond_1

    .line 43
    .line 44
    goto/16 :goto_0

    .line 45
    .line 46
    :cond_1
    const/16 v2, 0x3a

    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :sswitch_2
    const-string v1, "logo-union"

    .line 51
    .line 52
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-nez v1, :cond_2

    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_2
    const/16 v2, 0x39

    .line 61
    .line 62
    goto/16 :goto_0

    .line 63
    .line 64
    :sswitch_3
    const-string v1, "dislike"

    .line 65
    .line 66
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-nez v1, :cond_3

    .line 71
    .line 72
    goto/16 :goto_0

    .line 73
    .line 74
    :cond_3
    const/16 v2, 0x38

    .line 75
    .line 76
    goto/16 :goto_0

    .line 77
    .line 78
    :sswitch_4
    const-string v1, "arrowButton"

    .line 79
    .line 80
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-nez v1, :cond_4

    .line 85
    .line 86
    goto/16 :goto_0

    .line 87
    .line 88
    :cond_4
    const/16 v2, 0x37

    .line 89
    .line 90
    goto/16 :goto_0

    .line 91
    .line 92
    :sswitch_5
    const-string v1, "skip-with-countdowns-video-countdown"

    .line 93
    .line 94
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    if-nez v1, :cond_5

    .line 99
    .line 100
    goto/16 :goto_0

    .line 101
    .line 102
    :cond_5
    const/16 v2, 0x36

    .line 103
    .line 104
    goto/16 :goto_0

    .line 105
    .line 106
    :sswitch_6
    const-string v1, "skip-with-time-countdown"

    .line 107
    .line 108
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    if-nez v1, :cond_6

    .line 113
    .line 114
    goto/16 :goto_0

    .line 115
    .line 116
    :cond_6
    const/16 v2, 0x35

    .line 117
    .line 118
    goto/16 :goto_0

    .line 119
    .line 120
    :sswitch_7
    const-string v1, "video-vd"

    .line 121
    .line 122
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    if-nez v1, :cond_7

    .line 127
    .line 128
    goto/16 :goto_0

    .line 129
    .line 130
    :cond_7
    const/16 v2, 0x34

    .line 131
    .line 132
    goto/16 :goto_0

    .line 133
    .line 134
    :sswitch_8
    const-string v1, "video-sq"

    .line 135
    .line 136
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-nez v1, :cond_8

    .line 141
    .line 142
    goto/16 :goto_0

    .line 143
    .line 144
    :cond_8
    const/16 v2, 0x33

    .line 145
    .line 146
    goto/16 :goto_0

    .line 147
    .line 148
    :sswitch_9
    const-string v1, "video-hd"

    .line 149
    .line 150
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    if-nez v1, :cond_9

    .line 155
    .line 156
    goto/16 :goto_0

    .line 157
    .line 158
    :cond_9
    const/16 v2, 0x32

    .line 159
    .line 160
    goto/16 :goto_0

    .line 161
    .line 162
    :sswitch_a
    const-string v1, "app-icon"

    .line 163
    .line 164
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 165
    .line 166
    .line 167
    move-result v1

    .line 168
    if-nez v1, :cond_a

    .line 169
    .line 170
    goto/16 :goto_0

    .line 171
    .line 172
    :cond_a
    const/16 v2, 0x31

    .line 173
    .line 174
    goto/16 :goto_0

    .line 175
    .line 176
    :sswitch_b
    const-string v1, "image-cover"

    .line 177
    .line 178
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 179
    .line 180
    .line 181
    move-result v1

    .line 182
    if-nez v1, :cond_b

    .line 183
    .line 184
    goto/16 :goto_0

    .line 185
    .line 186
    :cond_b
    const/16 v2, 0x30

    .line 187
    .line 188
    goto/16 :goto_0

    .line 189
    .line 190
    :sswitch_c
    const-string v1, "webview-close"

    .line 191
    .line 192
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    if-nez v1, :cond_c

    .line 197
    .line 198
    goto/16 :goto_0

    .line 199
    .line 200
    :cond_c
    const/16 v2, 0x2f

    .line 201
    .line 202
    goto/16 :goto_0

    .line 203
    .line 204
    :sswitch_d
    const-string v1, "scoreCountWithIcon"

    .line 205
    .line 206
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 207
    .line 208
    .line 209
    move-result v1

    .line 210
    if-nez v1, :cond_d

    .line 211
    .line 212
    goto/16 :goto_0

    .line 213
    .line 214
    :cond_d
    const/16 v2, 0x2e

    .line 215
    .line 216
    goto/16 :goto_0

    .line 217
    .line 218
    :sswitch_e
    const-string v1, "tag-group"

    .line 219
    .line 220
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 221
    .line 222
    .line 223
    move-result v1

    .line 224
    if-nez v1, :cond_e

    .line 225
    .line 226
    goto/16 :goto_0

    .line 227
    .line 228
    :cond_e
    const/16 v2, 0x2d

    .line 229
    .line 230
    goto/16 :goto_0

    .line 231
    .line 232
    :sswitch_f
    const-string v1, "custom-component-vessel"

    .line 233
    .line 234
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 235
    .line 236
    .line 237
    move-result v1

    .line 238
    if-nez v1, :cond_f

    .line 239
    .line 240
    goto/16 :goto_0

    .line 241
    .line 242
    :cond_f
    const/16 v2, 0x2c

    .line 243
    .line 244
    goto/16 :goto_0

    .line 245
    .line 246
    :sswitch_10
    const-string v1, "score-count"

    .line 247
    .line 248
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 249
    .line 250
    .line 251
    move-result v1

    .line 252
    if-nez v1, :cond_10

    .line 253
    .line 254
    goto/16 :goto_0

    .line 255
    .line 256
    :cond_10
    const/16 v2, 0x2b

    .line 257
    .line 258
    goto/16 :goto_0

    .line 259
    .line 260
    :sswitch_11
    const-string v1, "development-name"

    .line 261
    .line 262
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 263
    .line 264
    .line 265
    move-result v1

    .line 266
    if-nez v1, :cond_11

    .line 267
    .line 268
    goto/16 :goto_0

    .line 269
    .line 270
    :cond_11
    const/16 v2, 0x2a

    .line 271
    .line 272
    goto/16 :goto_0

    .line 273
    .line 274
    :sswitch_12
    const-string v1, "feedback-dislike"

    .line 275
    .line 276
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 277
    .line 278
    .line 279
    move-result v1

    .line 280
    if-nez v1, :cond_12

    .line 281
    .line 282
    goto/16 :goto_0

    .line 283
    .line 284
    :cond_12
    const/16 v2, 0x29

    .line 285
    .line 286
    goto/16 :goto_0

    .line 287
    .line 288
    :sswitch_13
    const-string v1, "cardButton"

    .line 289
    .line 290
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 291
    .line 292
    .line 293
    move-result v1

    .line 294
    if-nez v1, :cond_13

    .line 295
    .line 296
    goto/16 :goto_0

    .line 297
    .line 298
    :cond_13
    const/16 v2, 0x28

    .line 299
    .line 300
    goto/16 :goto_0

    .line 301
    .line 302
    :sswitch_14
    const-string v1, "downloadButton"

    .line 303
    .line 304
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 305
    .line 306
    .line 307
    move-result v1

    .line 308
    if-nez v1, :cond_14

    .line 309
    .line 310
    goto/16 :goto_0

    .line 311
    .line 312
    :cond_14
    const/16 v2, 0x27

    .line 313
    .line 314
    goto/16 :goto_0

    .line 315
    .line 316
    :sswitch_15
    const-string v1, "video"

    .line 317
    .line 318
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 319
    .line 320
    .line 321
    move-result v1

    .line 322
    if-nez v1, :cond_15

    .line 323
    .line 324
    goto/16 :goto_0

    .line 325
    .line 326
    :cond_15
    const/16 v2, 0x26

    .line 327
    .line 328
    goto/16 :goto_0

    .line 329
    .line 330
    :sswitch_16
    const-string v1, "title"

    .line 331
    .line 332
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 333
    .line 334
    .line 335
    move-result v1

    .line 336
    if-nez v1, :cond_16

    .line 337
    .line 338
    goto/16 :goto_0

    .line 339
    .line 340
    :cond_16
    const/16 v2, 0x25

    .line 341
    .line 342
    goto/16 :goto_0

    .line 343
    .line 344
    :sswitch_17
    const-string v1, "muted"

    .line 345
    .line 346
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 347
    .line 348
    .line 349
    move-result v1

    .line 350
    if-nez v1, :cond_17

    .line 351
    .line 352
    goto/16 :goto_0

    .line 353
    .line 354
    :cond_17
    const/16 v2, 0x24

    .line 355
    .line 356
    goto/16 :goto_0

    .line 357
    .line 358
    :sswitch_18
    const-string v1, "image"

    .line 359
    .line 360
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 361
    .line 362
    .line 363
    move-result v1

    .line 364
    if-nez v1, :cond_18

    .line 365
    .line 366
    goto/16 :goto_0

    .line 367
    .line 368
    :cond_18
    const/16 v2, 0x23

    .line 369
    .line 370
    goto/16 :goto_0

    .line 371
    .line 372
    :sswitch_19
    const-string v1, "empty"

    .line 373
    .line 374
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 375
    .line 376
    .line 377
    move-result v1

    .line 378
    if-nez v1, :cond_19

    .line 379
    .line 380
    goto/16 :goto_0

    .line 381
    .line 382
    :cond_19
    const/16 v2, 0x22

    .line 383
    .line 384
    goto/16 :goto_0

    .line 385
    .line 386
    :sswitch_1a
    const-string v1, "close"

    .line 387
    .line 388
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 389
    .line 390
    .line 391
    move-result v1

    .line 392
    if-nez v1, :cond_1a

    .line 393
    .line 394
    goto/16 :goto_0

    .line 395
    .line 396
    :cond_1a
    const/16 v2, 0x21

    .line 397
    .line 398
    goto/16 :goto_0

    .line 399
    .line 400
    :sswitch_1b
    const-string v1, "text"

    .line 401
    .line 402
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 403
    .line 404
    .line 405
    move-result v1

    .line 406
    if-nez v1, :cond_1b

    .line 407
    .line 408
    goto/16 :goto_0

    .line 409
    .line 410
    :cond_1b
    const/16 v2, 0x20

    .line 411
    .line 412
    goto/16 :goto_0

    .line 413
    .line 414
    :sswitch_1c
    const-string v1, "star"

    .line 415
    .line 416
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 417
    .line 418
    .line 419
    move-result v1

    .line 420
    if-nez v1, :cond_1c

    .line 421
    .line 422
    goto/16 :goto_0

    .line 423
    .line 424
    :cond_1c
    const/16 v2, 0x1f

    .line 425
    .line 426
    goto/16 :goto_0

    .line 427
    .line 428
    :sswitch_1d
    const-string v1, "skip"

    .line 429
    .line 430
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 431
    .line 432
    .line 433
    move-result v1

    .line 434
    if-nez v1, :cond_1d

    .line 435
    .line 436
    goto/16 :goto_0

    .line 437
    .line 438
    :cond_1d
    const/16 v2, 0x1e

    .line 439
    .line 440
    goto/16 :goto_0

    .line 441
    .line 442
    :sswitch_1e
    const-string v1, "root"

    .line 443
    .line 444
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 445
    .line 446
    .line 447
    move-result v1

    .line 448
    if-nez v1, :cond_1e

    .line 449
    .line 450
    goto/16 :goto_0

    .line 451
    .line 452
    :cond_1e
    const/16 v2, 0x1d

    .line 453
    .line 454
    goto/16 :goto_0

    .line 455
    .line 456
    :sswitch_1f
    const-string v1, "logo"

    .line 457
    .line 458
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 459
    .line 460
    .line 461
    move-result v1

    .line 462
    if-nez v1, :cond_1f

    .line 463
    .line 464
    goto/16 :goto_0

    .line 465
    .line 466
    :cond_1f
    const/16 v2, 0x1c

    .line 467
    .line 468
    goto/16 :goto_0

    .line 469
    .line 470
    :sswitch_20
    const-string v1, "icon"

    .line 471
    .line 472
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 473
    .line 474
    .line 475
    move-result v1

    .line 476
    if-nez v1, :cond_20

    .line 477
    .line 478
    goto/16 :goto_0

    .line 479
    .line 480
    :cond_20
    const/16 v2, 0x1b

    .line 481
    .line 482
    goto/16 :goto_0

    .line 483
    .line 484
    :sswitch_21
    const-string v1, "colourMixtureButton"

    .line 485
    .line 486
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 487
    .line 488
    .line 489
    move-result v1

    .line 490
    if-nez v1, :cond_21

    .line 491
    .line 492
    goto/16 :goto_0

    .line 493
    .line 494
    :cond_21
    const/16 v2, 0x1a

    .line 495
    .line 496
    goto/16 :goto_0

    .line 497
    .line 498
    :sswitch_22
    const-string v1, "skip-with-countdowns-skip-countdown"

    .line 499
    .line 500
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 501
    .line 502
    .line 503
    move-result v1

    .line 504
    if-nez v1, :cond_22

    .line 505
    .line 506
    goto/16 :goto_0

    .line 507
    .line 508
    :cond_22
    const/16 v2, 0x19

    .line 509
    .line 510
    goto/16 :goto_0

    .line 511
    .line 512
    :sswitch_23
    const-string v1, "split-line"

    .line 513
    .line 514
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 515
    .line 516
    .line 517
    move-result v1

    .line 518
    if-nez v1, :cond_23

    .line 519
    .line 520
    goto/16 :goto_0

    .line 521
    .line 522
    :cond_23
    const/16 v2, 0x18

    .line 523
    .line 524
    goto/16 :goto_0

    .line 525
    .line 526
    :sswitch_24
    const-string v1, "skip-with-countdowns-skip-btn"

    .line 527
    .line 528
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 529
    .line 530
    .line 531
    move-result v1

    .line 532
    if-nez v1, :cond_24

    .line 533
    .line 534
    goto/16 :goto_0

    .line 535
    .line 536
    :cond_24
    const/16 v2, 0x17

    .line 537
    .line 538
    goto/16 :goto_0

    .line 539
    .line 540
    :sswitch_25
    const-string v1, "skip-countdowns"

    .line 541
    .line 542
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 543
    .line 544
    .line 545
    move-result v1

    .line 546
    if-nez v1, :cond_25

    .line 547
    .line 548
    goto/16 :goto_0

    .line 549
    .line 550
    :cond_25
    const/16 v2, 0x16

    .line 551
    .line 552
    goto/16 :goto_0

    .line 553
    .line 554
    :sswitch_26
    const-string v1, "video-image-budget"

    .line 555
    .line 556
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 557
    .line 558
    .line 559
    move-result v1

    .line 560
    if-nez v1, :cond_26

    .line 561
    .line 562
    goto/16 :goto_0

    .line 563
    .line 564
    :cond_26
    const/16 v2, 0x15

    .line 565
    .line 566
    goto/16 :goto_0

    .line 567
    .line 568
    :sswitch_27
    const-string v1, "image-square"

    .line 569
    .line 570
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 571
    .line 572
    .line 573
    move-result v1

    .line 574
    if-nez v1, :cond_27

    .line 575
    .line 576
    goto/16 :goto_0

    .line 577
    .line 578
    :cond_27
    const/16 v2, 0x14

    .line 579
    .line 580
    goto/16 :goto_0

    .line 581
    .line 582
    :sswitch_28
    const-string v1, "image-splash"

    .line 583
    .line 584
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 585
    .line 586
    .line 587
    move-result v1

    .line 588
    if-nez v1, :cond_28

    .line 589
    .line 590
    goto/16 :goto_0

    .line 591
    .line 592
    :cond_28
    const/16 v2, 0x13

    .line 593
    .line 594
    goto/16 :goto_0

    .line 595
    .line 596
    :sswitch_29
    const-string v1, "vessel"

    .line 597
    .line 598
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 599
    .line 600
    .line 601
    move-result v1

    .line 602
    if-nez v1, :cond_29

    .line 603
    .line 604
    goto/16 :goto_0

    .line 605
    .line 606
    :cond_29
    const/16 v2, 0x12

    .line 607
    .line 608
    goto/16 :goto_0

    .line 609
    .line 610
    :sswitch_2a
    const-string v1, "privacy-detail"

    .line 611
    .line 612
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 613
    .line 614
    .line 615
    move-result v1

    .line 616
    if-nez v1, :cond_2a

    .line 617
    .line 618
    goto/16 :goto_0

    .line 619
    .line 620
    :cond_2a
    const/16 v2, 0x11

    .line 621
    .line 622
    goto/16 :goto_0

    .line 623
    .line 624
    :sswitch_2b
    const-string v1, "source"

    .line 625
    .line 626
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 627
    .line 628
    .line 629
    move-result v1

    .line 630
    if-nez v1, :cond_2b

    .line 631
    .line 632
    goto/16 :goto_0

    .line 633
    .line 634
    :cond_2b
    const/16 v2, 0x10

    .line 635
    .line 636
    goto/16 :goto_0

    .line 637
    .line 638
    :sswitch_2c
    const-string v1, "text_star"

    .line 639
    .line 640
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 641
    .line 642
    .line 643
    move-result v1

    .line 644
    if-nez v1, :cond_2c

    .line 645
    .line 646
    goto/16 :goto_0

    .line 647
    .line 648
    :cond_2c
    const/16 v2, 0xf

    .line 649
    .line 650
    goto/16 :goto_0

    .line 651
    .line 652
    :sswitch_2d
    const-string v1, "fillButton"

    .line 653
    .line 654
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 655
    .line 656
    .line 657
    move-result v1

    .line 658
    if-nez v1, :cond_2d

    .line 659
    .line 660
    goto/16 :goto_0

    .line 661
    .line 662
    :cond_2d
    const/16 v2, 0xe

    .line 663
    .line 664
    goto/16 :goto_0

    .line 665
    .line 666
    :sswitch_2e
    const-string v1, "laceButton"

    .line 667
    .line 668
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 669
    .line 670
    .line 671
    move-result v1

    .line 672
    if-nez v1, :cond_2e

    .line 673
    .line 674
    goto/16 :goto_0

    .line 675
    .line 676
    :cond_2e
    const/16 v2, 0xd

    .line 677
    .line 678
    goto/16 :goto_0

    .line 679
    .line 680
    :sswitch_2f
    const-string v1, "icon-download"

    .line 681
    .line 682
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 683
    .line 684
    .line 685
    move-result v1

    .line 686
    if-nez v1, :cond_2f

    .line 687
    .line 688
    goto/16 :goto_0

    .line 689
    .line 690
    :cond_2f
    const/16 v2, 0xc

    .line 691
    .line 692
    goto/16 :goto_0

    .line 693
    .line 694
    :sswitch_30
    const-string v1, "footer"

    .line 695
    .line 696
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 697
    .line 698
    .line 699
    move-result v1

    .line 700
    if-nez v1, :cond_30

    .line 701
    .line 702
    goto/16 :goto_0

    .line 703
    .line 704
    :cond_30
    const/16 v2, 0xb

    .line 705
    .line 706
    goto/16 :goto_0

    .line 707
    .line 708
    :sswitch_31
    const-string v1, "button"

    .line 709
    .line 710
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 711
    .line 712
    .line 713
    move-result v1

    .line 714
    if-nez v1, :cond_31

    .line 715
    .line 716
    goto/16 :goto_0

    .line 717
    .line 718
    :cond_31
    const/16 v2, 0xa

    .line 719
    .line 720
    goto/16 :goto_0

    .line 721
    .line 722
    :sswitch_32
    const-string v1, "skip-with-time-skip-btn"

    .line 723
    .line 724
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 725
    .line 726
    .line 727
    move-result v1

    .line 728
    if-nez v1, :cond_32

    .line 729
    .line 730
    goto/16 :goto_0

    .line 731
    .line 732
    :cond_32
    const/16 v2, 0x9

    .line 733
    .line 734
    goto/16 :goto_0

    .line 735
    .line 736
    :sswitch_33
    const-string v1, "image-wide"

    .line 737
    .line 738
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 739
    .line 740
    .line 741
    move-result v1

    .line 742
    if-nez v1, :cond_33

    .line 743
    .line 744
    goto/16 :goto_0

    .line 745
    .line 746
    :cond_33
    const/16 v2, 0x8

    .line 747
    .line 748
    goto/16 :goto_0

    .line 749
    .line 750
    :sswitch_34
    const-string v1, "image-long"

    .line 751
    .line 752
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 753
    .line 754
    .line 755
    move-result v1

    .line 756
    if-nez v1, :cond_34

    .line 757
    .line 758
    goto :goto_0

    .line 759
    :cond_34
    const/4 v2, 0x7

    .line 760
    goto :goto_0

    .line 761
    :sswitch_35
    const-string v1, "creative-playable-bait"

    .line 762
    .line 763
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 764
    .line 765
    .line 766
    move-result v1

    .line 767
    if-nez v1, :cond_35

    .line 768
    .line 769
    goto :goto_0

    .line 770
    :cond_35
    const/4 v2, 0x6

    .line 771
    goto :goto_0

    .line 772
    :sswitch_36
    const-string v1, "downloadWithIcon"

    .line 773
    .line 774
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 775
    .line 776
    .line 777
    move-result v1

    .line 778
    if-nez v1, :cond_36

    .line 779
    .line 780
    goto :goto_0

    .line 781
    :cond_36
    const/4 v2, 0x5

    .line 782
    goto :goto_0

    .line 783
    :sswitch_37
    const-string v1, "skip-with-time"

    .line 784
    .line 785
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 786
    .line 787
    .line 788
    move-result v1

    .line 789
    if-nez v1, :cond_37

    .line 790
    .line 791
    goto :goto_0

    .line 792
    :cond_37
    const/4 v2, 0x4

    .line 793
    goto :goto_0

    .line 794
    :sswitch_38
    const-string v1, "subtitle"

    .line 795
    .line 796
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 797
    .line 798
    .line 799
    move-result v1

    .line 800
    if-nez v1, :cond_38

    .line 801
    .line 802
    goto :goto_0

    .line 803
    :cond_38
    const/4 v2, 0x3

    .line 804
    goto :goto_0

    .line 805
    :sswitch_39
    const-string v1, "timedown"

    .line 806
    .line 807
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 808
    .line 809
    .line 810
    move-result v1

    .line 811
    if-nez v1, :cond_39

    .line 812
    .line 813
    goto :goto_0

    .line 814
    :cond_39
    const/4 v2, 0x2

    .line 815
    goto :goto_0

    .line 816
    :sswitch_3a
    const-string v1, "close-fill"

    .line 817
    .line 818
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 819
    .line 820
    .line 821
    move-result v1

    .line 822
    if-nez v1, :cond_3a

    .line 823
    .line 824
    goto :goto_0

    .line 825
    :cond_3a
    const/4 v2, 0x1

    .line 826
    goto :goto_0

    .line 827
    :sswitch_3b
    const-string v1, "app-version"

    .line 828
    .line 829
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 830
    .line 831
    .line 832
    move-result v1

    .line 833
    if-nez v1, :cond_3b

    .line 834
    .line 835
    goto :goto_0

    .line 836
    :cond_3b
    const/4 v2, 0x0

    .line 837
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 838
    .line 839
    .line 840
    goto/16 :goto_1

    .line 841
    .line 842
    :pswitch_0
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇8oOO88(Ljava/lang/String;)V

    .line 843
    .line 844
    .line 845
    goto/16 :goto_1

    .line 846
    .line 847
    :pswitch_1
    invoke-static {p0}, Lb/b/a/a/d/a;->〇8o8o〇(Ljava/lang/String;)V

    .line 848
    .line 849
    .line 850
    goto/16 :goto_1

    .line 851
    .line 852
    :pswitch_2
    invoke-static {p0}, Lb/b/a/a/d/a;->Oo08(Ljava/lang/String;)V

    .line 853
    .line 854
    .line 855
    goto/16 :goto_1

    .line 856
    .line 857
    :pswitch_3
    invoke-static {p0}, Lb/b/a/a/d/a;->〇8〇0〇o〇O(Ljava/lang/String;)V

    .line 858
    .line 859
    .line 860
    goto/16 :goto_1

    .line 861
    .line 862
    :pswitch_4
    invoke-static {p0}, Lb/b/a/a/d/a;->oo〇(Ljava/lang/String;)V

    .line 863
    .line 864
    .line 865
    goto/16 :goto_1

    .line 866
    .line 867
    :pswitch_5
    invoke-static {p0}, Lb/b/a/a/d/a;->〇O〇(Ljava/lang/String;)V

    .line 868
    .line 869
    .line 870
    goto/16 :goto_1

    .line 871
    .line 872
    :pswitch_6
    invoke-static {p0}, Lb/b/a/a/d/a;->〇〇8O0〇8(Ljava/lang/String;)V

    .line 873
    .line 874
    .line 875
    goto/16 :goto_1

    .line 876
    .line 877
    :pswitch_7
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇0OOo〇0(Ljava/lang/String;)V

    .line 878
    .line 879
    .line 880
    goto/16 :goto_1

    .line 881
    .line 882
    :pswitch_8
    invoke-static {p0}, Lb/b/a/a/d/a;->Oo8Oo00oo(Ljava/lang/String;)V

    .line 883
    .line 884
    .line 885
    goto/16 :goto_1

    .line 886
    .line 887
    :pswitch_9
    invoke-static {p0}, Lb/b/a/a/d/a;->〇o(Ljava/lang/String;)V

    .line 888
    .line 889
    .line 890
    goto/16 :goto_1

    .line 891
    .line 892
    :pswitch_a
    invoke-static {p0}, Lb/b/a/a/d/a;->〇oOO8O8(Ljava/lang/String;)V

    .line 893
    .line 894
    .line 895
    goto/16 :goto_1

    .line 896
    .line 897
    :pswitch_b
    invoke-static {p0}, Lb/b/a/a/d/a;->ooo〇8oO(Ljava/lang/String;)V

    .line 898
    .line 899
    .line 900
    goto/16 :goto_1

    .line 901
    .line 902
    :pswitch_c
    invoke-static {p0}, Lb/b/a/a/d/a;->〇08O8o〇0(Ljava/lang/String;)V

    .line 903
    .line 904
    .line 905
    goto/16 :goto_1

    .line 906
    .line 907
    :pswitch_d
    invoke-static {p0}, Lb/b/a/a/d/a;->〇80〇808〇O(Ljava/lang/String;)V

    .line 908
    .line 909
    .line 910
    goto/16 :goto_1

    .line 911
    .line 912
    :pswitch_e
    invoke-static {p0}, Lb/b/a/a/d/a;->〇oo〇(Ljava/lang/String;)V

    .line 913
    .line 914
    .line 915
    goto/16 :goto_1

    .line 916
    .line 917
    :pswitch_f
    invoke-static {p0}, Lb/b/a/a/d/a;->〇8(Ljava/lang/String;)V

    .line 918
    .line 919
    .line 920
    goto/16 :goto_1

    .line 921
    .line 922
    :pswitch_10
    invoke-static {p0}, Lb/b/a/a/d/a;->OO0o〇〇〇〇0(Ljava/lang/String;)V

    .line 923
    .line 924
    .line 925
    goto/16 :goto_1

    .line 926
    .line 927
    :pswitch_11
    invoke-static {p0}, Lb/b/a/a/d/a;->O08000(Ljava/lang/String;)V

    .line 928
    .line 929
    .line 930
    goto/16 :goto_1

    .line 931
    .line 932
    :pswitch_12
    invoke-static {p0}, Lb/b/a/a/d/a;->oO00OOO(Ljava/lang/String;)V

    .line 933
    .line 934
    .line 935
    goto/16 :goto_1

    .line 936
    .line 937
    :pswitch_13
    invoke-static {p0}, Lb/b/a/a/d/a;->o0ooO(Ljava/lang/String;)V

    .line 938
    .line 939
    .line 940
    goto/16 :goto_1

    .line 941
    .line 942
    :pswitch_14
    invoke-static {p0}, Lb/b/a/a/d/a;->O〇O〇oO(Ljava/lang/String;)V

    .line 943
    .line 944
    .line 945
    goto/16 :goto_1

    .line 946
    .line 947
    :pswitch_15
    invoke-static {p0}, Lb/b/a/a/d/a;->O8〇o(Ljava/lang/String;)V

    .line 948
    .line 949
    .line 950
    goto/16 :goto_1

    .line 951
    .line 952
    :pswitch_16
    invoke-static {p0}, Lb/b/a/a/d/a;->〇0000OOO(Ljava/lang/String;)V

    .line 953
    .line 954
    .line 955
    goto/16 :goto_1

    .line 956
    .line 957
    :pswitch_17
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇0(Ljava/lang/String;)V

    .line 958
    .line 959
    .line 960
    goto/16 :goto_1

    .line 961
    .line 962
    :pswitch_18
    invoke-static {p0}, Lb/b/a/a/d/a;->OOO(Ljava/lang/String;)V

    .line 963
    .line 964
    .line 965
    goto/16 :goto_1

    .line 966
    .line 967
    :pswitch_19
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇O(Ljava/lang/String;)V

    .line 968
    .line 969
    .line 970
    goto/16 :goto_1

    .line 971
    .line 972
    :pswitch_1a
    invoke-static {p0}, Lb/b/a/a/d/a;->o8(Ljava/lang/String;)V

    .line 973
    .line 974
    .line 975
    goto/16 :goto_1

    .line 976
    .line 977
    :pswitch_1b
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇O8〇〇o(Ljava/lang/String;)V

    .line 978
    .line 979
    .line 980
    goto/16 :goto_1

    .line 981
    .line 982
    :pswitch_1c
    invoke-static {p0}, Lb/b/a/a/d/a;->〇O888o0o(Ljava/lang/String;)V

    .line 983
    .line 984
    .line 985
    goto/16 :goto_1

    .line 986
    .line 987
    :pswitch_1d
    invoke-static {p0}, Lb/b/a/a/d/a;->〇O8o08O(Ljava/lang/String;)V

    .line 988
    .line 989
    .line 990
    goto/16 :goto_1

    .line 991
    .line 992
    :pswitch_1e
    invoke-static {p0}, Lb/b/a/a/d/a;->oO80(Ljava/lang/String;)V

    .line 993
    .line 994
    .line 995
    goto/16 :goto_1

    .line 996
    .line 997
    :pswitch_1f
    invoke-static {p0}, Lb/b/a/a/d/a;->O8(Ljava/lang/String;)V

    .line 998
    .line 999
    .line 1000
    goto/16 :goto_1

    .line 1001
    .line 1002
    :pswitch_20
    invoke-static {p0}, Lb/b/a/a/d/a;->Ooo(Ljava/lang/String;)V

    .line 1003
    .line 1004
    .line 1005
    goto/16 :goto_1

    .line 1006
    .line 1007
    :pswitch_21
    invoke-static {p0}, Lb/b/a/a/d/a;->〇〇0o(Ljava/lang/String;)V

    .line 1008
    .line 1009
    .line 1010
    goto/16 :goto_1

    .line 1011
    .line 1012
    :pswitch_22
    invoke-static {p0}, Lb/b/a/a/d/a;->〇〇808〇(Ljava/lang/String;)V

    .line 1013
    .line 1014
    .line 1015
    goto/16 :goto_1

    .line 1016
    .line 1017
    :pswitch_23
    invoke-static {p0}, Lb/b/a/a/d/a;->o800o8O(Ljava/lang/String;)V

    .line 1018
    .line 1019
    .line 1020
    goto/16 :goto_1

    .line 1021
    .line 1022
    :pswitch_24
    invoke-static {p0}, Lb/b/a/a/d/a;->Oooo8o0〇(Ljava/lang/String;)V

    .line 1023
    .line 1024
    .line 1025
    goto/16 :goto_1

    .line 1026
    .line 1027
    :pswitch_25
    invoke-static {p0}, Lb/b/a/a/d/a;->OO0o〇〇(Ljava/lang/String;)V

    .line 1028
    .line 1029
    .line 1030
    goto/16 :goto_1

    .line 1031
    .line 1032
    :pswitch_26
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇8(Ljava/lang/String;)V

    .line 1033
    .line 1034
    .line 1035
    goto :goto_1

    .line 1036
    :pswitch_27
    invoke-static {p0}, Lb/b/a/a/d/a;->〇080(Ljava/lang/String;)V

    .line 1037
    .line 1038
    .line 1039
    goto :goto_1

    .line 1040
    :pswitch_28
    invoke-static {p0}, Lb/b/a/a/d/a;->OO8oO0o〇(Ljava/lang/String;)V

    .line 1041
    .line 1042
    .line 1043
    goto :goto_1

    .line 1044
    :pswitch_29
    invoke-static {p0}, Lb/b/a/a/d/a;->OOO〇O0(Ljava/lang/String;)V

    .line 1045
    .line 1046
    .line 1047
    goto :goto_1

    .line 1048
    :pswitch_2a
    invoke-static {p0}, Lb/b/a/a/d/a;->〇〇888(Ljava/lang/String;)V

    .line 1049
    .line 1050
    .line 1051
    goto :goto_1

    .line 1052
    :pswitch_2b
    invoke-static {p0}, Lb/b/a/a/d/a;->OoO8(Ljava/lang/String;)V

    .line 1053
    .line 1054
    .line 1055
    goto :goto_1

    .line 1056
    :pswitch_2c
    invoke-static {p0}, Lb/b/a/a/d/a;->〇00(Ljava/lang/String;)V

    .line 1057
    .line 1058
    .line 1059
    goto :goto_1

    .line 1060
    :pswitch_2d
    invoke-static {p0}, Lb/b/a/a/d/a;->O000(Ljava/lang/String;)V

    .line 1061
    .line 1062
    .line 1063
    goto :goto_1

    .line 1064
    :pswitch_2e
    invoke-static {p0}, Lb/b/a/a/d/a;->〇o〇(Ljava/lang/String;)V

    .line 1065
    .line 1066
    .line 1067
    goto :goto_1

    .line 1068
    :pswitch_2f
    invoke-static {p0}, Lb/b/a/a/d/a;->〇O〇80o08O(Ljava/lang/String;)V

    .line 1069
    .line 1070
    .line 1071
    goto :goto_1

    .line 1072
    :pswitch_30
    invoke-static {p0}, Lb/b/a/a/d/a;->〇80(Ljava/lang/String;)V

    .line 1073
    .line 1074
    .line 1075
    goto :goto_1

    .line 1076
    :pswitch_31
    invoke-static {p0}, Lb/b/a/a/d/a;->〇00〇8(Ljava/lang/String;)V

    .line 1077
    .line 1078
    .line 1079
    goto :goto_1

    .line 1080
    :pswitch_32
    invoke-static {p0}, Lb/b/a/a/d/a;->〇0〇O0088o(Ljava/lang/String;)V

    .line 1081
    .line 1082
    .line 1083
    goto :goto_1

    .line 1084
    :pswitch_33
    invoke-static {p0}, Lb/b/a/a/d/a;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 1085
    .line 1086
    .line 1087
    goto :goto_1

    .line 1088
    :pswitch_34
    invoke-static {p0}, Lb/b/a/a/d/a;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 1089
    .line 1090
    .line 1091
    goto :goto_1

    .line 1092
    :pswitch_35
    invoke-static {p0}, Lb/b/a/a/d/a;->oO(Ljava/lang/String;)V

    .line 1093
    .line 1094
    .line 1095
    goto :goto_1

    .line 1096
    :pswitch_36
    invoke-static {p0}, Lb/b/a/a/d/a;->o8oO〇(Ljava/lang/String;)V

    .line 1097
    .line 1098
    .line 1099
    goto :goto_1

    .line 1100
    :pswitch_37
    invoke-static {p0}, Lb/b/a/a/d/a;->〇O00(Ljava/lang/String;)V

    .line 1101
    .line 1102
    .line 1103
    goto :goto_1

    .line 1104
    :pswitch_38
    invoke-static {p0}, Lb/b/a/a/d/a;->oo88o8O(Ljava/lang/String;)V

    .line 1105
    .line 1106
    .line 1107
    goto :goto_1

    .line 1108
    :pswitch_39
    invoke-static {p0}, Lb/b/a/a/d/a;->O8ooOoo〇(Ljava/lang/String;)V

    .line 1109
    .line 1110
    .line 1111
    goto :goto_1

    .line 1112
    :pswitch_3a
    invoke-static {p0}, Lb/b/a/a/d/a;->〇〇〇0〇〇0(Ljava/lang/String;)V

    .line 1113
    .line 1114
    .line 1115
    goto :goto_1

    .line 1116
    :pswitch_3b
    invoke-static {p0}, Lb/b/a/a/d/a;->o〇〇0〇(Ljava/lang/String;)V

    .line 1117
    .line 1118
    .line 1119
    :cond_3c
    :goto_1
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120
    .line 1121
    .line 1122
    move-result-object p0

    .line 1123
    check-cast p0, Lorg/json/JSONObject;

    .line 1124
    .line 1125
    return-object p0

    .line 1126
    nop

    .line 1127
    :sswitch_data_0
    .sparse-switch
        -0x7fbc36d4 -> :sswitch_3b
        -0x7da7cbc8 -> :sswitch_3a
        -0x7bcab711 -> :sswitch_39
        -0x7ad0b3e8 -> :sswitch_38
        -0x68207b5a -> :sswitch_37
        -0x5e0f4cf9 -> :sswitch_36
        -0x594c1415 -> :sswitch_35
        -0x58c32052 -> :sswitch_34
        -0x58be37fb -> :sswitch_33
        -0x573525ab -> :sswitch_32
        -0x521dd8ce -> :sswitch_31
        -0x4ba14a65 -> :sswitch_30
        -0x46b4f8a4 -> :sswitch_2f
        -0x40e56957 -> :sswitch_2e
        -0x3f44656b -> :sswitch_2d
        -0x3dd851dc -> :sswitch_2c
        -0x356f97e5 -> :sswitch_2b
        -0x32bc29ca -> :sswitch_2a
        -0x30deddca -> :sswitch_29
        -0x287d40e7 -> :sswitch_28
        -0x286b1231 -> :sswitch_27
        -0x27944bf7 -> :sswitch_26
        -0x1a811e90 -> :sswitch_25
        -0x18abdce0 -> :sswitch_24
        -0x17056939 -> :sswitch_23
        -0xf59a80b -> :sswitch_22
        -0x27cf24a -> :sswitch_21
        0x313c79 -> :sswitch_20
        0x32c5ab -> :sswitch_1f
        0x3580e2 -> :sswitch_1e
        0x35e57f -> :sswitch_1d
        0x360652 -> :sswitch_1c
        0x36452d -> :sswitch_1b
        0x5a5ddf8 -> :sswitch_1a
        0x5c2854d -> :sswitch_19
        0x5faa95b -> :sswitch_18
        0x636f16b -> :sswitch_17
        0x6942258 -> :sswitch_16
        0x6b0147b -> :sswitch_15
        0x97b55da -> :sswitch_14
        0x9d10262 -> :sswitch_13
        0xf40b6dd -> :sswitch_12
        0x153c0bdd -> :sswitch_11
        0x185bee14 -> :sswitch_10
        0x21cdf3e2 -> :sswitch_f
        0x26a12e8c -> :sswitch_e
        0x2ed9731c -> :sswitch_d
        0x36c28f24 -> :sswitch_c
        0x3fe060c5 -> :sswitch_b
        0x42d610a5 -> :sswitch_a
        0x44a00b8e -> :sswitch_9
        0x44a00cf0 -> :sswitch_8
        0x44a00d40 -> :sswitch_7
        0x4718a5ca -> :sswitch_6
        0x52b0f50d -> :sswitch_5
        0x52c49c9b -> :sswitch_4
        0x63a33d25 -> :sswitch_3
        0x6783042d -> :sswitch_2
        0x69dbec38 -> :sswitch_1
        0x6d892b8d -> :sswitch_0
    .end sparse-switch

    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
.end method

.method public static O〇O〇oO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"Download\",\"paddingRight\":0,\"borderSize\":1,\"useAtomFontFamily\":false,\"idleColor\":\"#f85959\",\"align\":\"0\",\"timingStart\":0,\"marginRight\":20,\"paddingBottom\":12,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"downloadTextFinished\":\"Install now\",\"atomBgStartColorPreview\":1,\"downloadTextColor\":\"#ffffff\",\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"downloadTextInstalled\":\"Open now\",\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"downloadTextPaused\":\"Continue downloading\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":20,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#1A73E8\",\"lineLimit\":false,\"downloadTextFailed\":\"Download again\",\"atomBtnAnimation\":[0],\"reachedColor\":\"#f85959\",\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"atomButtonText\":true,\"bgColor\":\"transparent\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":12,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"idleTextColor\":\"#ffffff\",\"letterSpacing\":0,\"unreachedColor\":\"#1d1d1d\",\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"downloadTextStart\":\"Click download\",\"downloadTextActive\":\"Downloading\",\"width\":1,\"useAtomBgStartColor\":false,\"downloadTextIdle\":\"Click download\",\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o0ooO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\"},\"clickType\":\"dislike\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o800o8O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"widthMode\":\"flex\",\"paddingBottom\":0,\"bgColor\":\"#333333\",\"paddingRight\":0,\"width\":1,\"heightMode\":\"fixed\",\"borderSize\":0,\"paddingTop\":0,\"position\":\"0\",\"paddingLeft\":0,\"height\":1},\"areaType\":1,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o8oO〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static oO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"dynamic_creative.show_playable\",\"paddingRight\":0,\"bgImgUrl\":\"playable-bait\",\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":28,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":94,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"mnpl_guide_comp_click\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static oO00OOO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":5,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":5,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":5,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":5,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.3,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"bgColor\":\"#9B9B9B\"},\"clickType\":\"dislike\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static oO80(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"#ffffff\",\"paddingTop\":0,\"borderStyle\":\"solid\",\"height\":375,\"justifyHorizontal\":\"space-around\",\"bindEventBy\":\"none\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"pageMode\":\"0\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":375,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"paddingLeft\":0},\"areaType\":2,\"themeValues\":{\"bgColor\":\"rgba(255,255,255,0)\"},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static oo88o8O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"description\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":500,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":15,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"subtitle\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static ooo〇8oO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"flex\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static oo〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#ff113b\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"Please enter the content\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":20,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":20,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"none\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"muted\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇0OOo〇0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":0.5625,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"contain\",\"borderColor\":\"#000000\",\"data\":\"image.0\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"showPlayButton\":true,\"timingStart\":0,\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"borderStyle\":\"solid\",\"imageType\":\"static-image\",\"height\":1.52,\"bindEventBy\":\"none\",\"image\":{\"imageObjectFit\":\"contain\",\"clickArea\":\"creative\",\"data\":\"image.0\",\"clickTigger\":\"click\",\"materialCenterData\":-999,\"imageType\":\"static-image\",\"isDataFixed\":false},\"alignItems\":\"flex-start\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"heightMode\":\"scale\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"bulletScreenStart\":0,\"fixBorderStyleType\":0,\"video\":{\"showTimeProgress\":false,\"bulletScreenEnd\":0,\"clickArea\":\"video\",\"data\":\"video\",\"showBulletScreen\":false,\"bulletScreenStart\":0,\"clickTigger\":\"click\",\"materialCenterData\":-999,\"showPlayButton\":true,\"isDataFixed\":false,\"bulletScreenData\":\"dynamic_creative.danmu\"},\"isDataFixed\":false,\"bulletScreenData\":\"dynamic_creative.danmu\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"interactText\":\"\",\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"interactType\":\"\",\"showBulletScreen\":false,\"width\":1,\"interactVisibleTime\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇8oOO88(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":12,\"borderSize\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"bgColor\":\"transparent\",\"paddingTop\":10,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":40,\"bindEventBy\":\"none\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"borderRadius\":0,\"clickTigger\":\"click\",\"width\":1,\"heightMode\":\"fixed\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":12,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇O8〇〇o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"text\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static o〇〇0〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"dynamic_creative.app_version\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇00(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.score\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"star\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇0000OOO(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"title\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"highlightWord\":false,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":500,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":19,\"quoteWord\":false,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#222222\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"title\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇00〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#1A73E8\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"transparent\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇080(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇08O8o〇0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇0〇O0088o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(255, 255, 255, 0.2)\",\"data\":\"text\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"rules\":[],\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"triggerActions\":[],\"bindEventBy\":\"none\",\"textMaterialCenterCalcColorSource\":1,\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":8,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"triggerConditions\":[],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"borderMaterialCenterCalcColorSource\":1,\"atomBgStartColor\":[0],\"textFlowSpeed\":3,\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"textFormat\":\"0\",\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"i18n\":{}},\"clickType\":\"skip\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":80,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇80(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":12,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":10,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":1,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":12,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇80〇808〇O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇8o8o〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.comment_num\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"score_count\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇8〇0〇o〇O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":3,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":8,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":8,\"position\":\"0\",\"paddingLeft\":3,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\"},\"clickType\":\"dislike\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇O00(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"rules\":[],\"interactHiddenTime\":5,\"align\":\"0\",\"interactPressDuration\":1500,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"borderStyle\":\"solid\",\"height\":40,\"triggerActions\":[],\"bindEventBy\":\"none\",\"alignItems\":\"flex-start\",\"textAlign\":\"center\",\"interactI18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":0,\"useAtomBgColor\":false,\"triggerConditions\":[],\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":0,\"borderMaterialCenterCalcColorSource\":1,\"appStoreFullScreen\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"interactWontHide\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactSlideThreshold\":0,\"interactTextFixed\":true,\"showHasDataType\":\"\",\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"interactValidate\":false,\"interactType\":\"\",\"width\":320,\"interactVisibleTime\":0,\"interactBottomDistance\":120,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"interactI18n\":{}},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇O888o0o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.score\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":20,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFC642\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"starIconType\":\"1\",\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"starMargin\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"star\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇O8o08O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"text\",\"paddingRight\":3,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":8,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":8,\"position\":\"0\",\"paddingLeft\":3,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\",\"bgColor\":\"rgba(255, 255, 255, 0)\"},\"clickType\":\"skip\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇O〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"right\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"time_down\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇O〇80o08O(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1.78,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1.78,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇oOO8O8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"icon\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":60,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"icon\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇oo〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":5,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"interactSlideDirection\":\"2\",\"fontFamily\":\"PingFangSC-Regular\",\"borderStyle\":\"solid\",\"height\":80,\"bindEventBy\":\"none\",\"alignItems\":\"flex-start\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"useAtomBgColor\":false,\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"heightMode\":\"auto\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":5,\"tagData\":\"dynamic_creative.creative_tags\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"tagStyleType\":\"backgroundColor\",\"bgColor\":\"transparent\",\"paddingTop\":2,\"materialCenterData\":-999,\"tagMaxCount\":5,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"justifyHorizontal\":\"flex-start\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"interactValidate\":false,\"interactType\":\"\",\"width\":375,\"interactVisibleTime\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"tag\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇o〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#895434\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇0o(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#00e7ff\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇808〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"time_down\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇888(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇8O0〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":8,\"borderSize\":0,\"useAtomFontFamily\":false,\"rules\":[],\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":5,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"triggerActions\":[],\"bindEventBy\":\"none\",\"textMaterialCenterCalcColorSource\":1,\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"triggerConditions\":[],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":8,\"colorCg\":0,\"borderMaterialCenterCalcColorSource\":1,\"atomBgStartColor\":[0],\"textFlowSpeed\":3,\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":5,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"textFormat\":\"0\",\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"lineHeight\":1.3,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"i18n\":{}},\"clickType\":\"time_down\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public static 〇〇〇0〇〇0(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/d/a;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    new-instance v1, Lorg/json/JSONObject;

    .line 4
    .line 5
    const-string v2, "{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":16,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"bgColor\":\"#9B9B9B\"},\"clickType\":\"dislike\"}"

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

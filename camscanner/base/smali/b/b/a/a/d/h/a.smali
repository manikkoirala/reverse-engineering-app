.class public abstract Lb/b/a/a/d/h/a;
.super Ljava/lang/Object;
.source "BaseWebViewRender.java"

# interfaces
.implements Lb/b/a/a/d/f/d;
.implements Lb/b/a/a/d/f/k;
.implements Lcom/bytedance/sdk/component/adexpress/theme/a;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lorg/json/JSONObject;

.field private c:Ljava/lang/String;

.field private volatile d:Lb/b/a/a/d/f/g;

.field private e:Z

.field protected f:Z

.field private g:Lb/b/a/a/d/f/h;

.field private h:Lb/b/a/a/d/f/m;

.field protected i:Lcom/bytedance/sdk/component/widget/SSWebView;

.field private j:Z

.field protected k:I

.field protected l:Lb/b/a/a/d/e/c/b;

.field protected m:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lb/b/a/a/d/f/m;Lcom/bytedance/sdk/component/adexpress/theme/ThemeStatusBroadcastReceiver;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lb/b/a/a/d/h/a;->e:Z

    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    iput v1, p0, Lb/b/a/a/d/h/a;->k:I

    .line 10
    .line 11
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 12
    .line 13
    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 14
    .line 15
    .line 16
    iput-object v1, p0, Lb/b/a/a/d/h/a;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 17
    .line 18
    iput-object p1, p0, Lb/b/a/a/d/h/a;->a:Landroid/content/Context;

    .line 19
    .line 20
    iput-object p2, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    .line 21
    .line 22
    invoke-virtual {p2}, Lb/b/a/a/d/f/m;->〇080()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p3, p0}, Lcom/bytedance/sdk/component/adexpress/theme/ThemeStatusBroadcastReceiver;->a(Lcom/bytedance/sdk/component/adexpress/theme/a;)V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lb/b/a/a/d/h/e;->〇O8o08O()Lb/b/a/a/d/h/e;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lb/b/a/a/d/h/e;->〇080()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iput-object p1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 37
    .line 38
    if-nez p1, :cond_0

    .line 39
    .line 40
    invoke-static {}, Lb/b/a/a/d/c;->〇080()Landroid/content/Context;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    new-instance p1, Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 47
    .line 48
    invoke-static {}, Lb/b/a/a/d/c;->〇080()Landroid/content/Context;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-direct {p1, p2}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;)V

    .line 53
    .line 54
    .line 55
    iput-object p1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    const/4 p1, 0x1

    .line 59
    iput-boolean p1, p0, Lb/b/a/a/d/h/a;->e:Z

    .line 60
    .line 61
    :cond_1
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private a(FF)V
    .locals 1
    .annotation build Lcom/bytedance/component/sdk/annotation/UiThread;
    .end annotation

    .line 47
    iget-object v0, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    invoke-virtual {v0}, Lb/b/a/a/d/f/m;->〇〇808〇()Lb/b/a/a/d/f/i;

    move-result-object v0

    invoke-interface {v0}, Lb/b/a/a/d/f/i;->d()V

    .line 48
    iget-object v0, p0, Lb/b/a/a/d/h/a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    move-result p1

    float-to-int p1, p1

    .line 49
    iget-object v0, p0, Lb/b/a/a/d/h/a;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    move-result p2

    float-to-int p2, p2

    .line 50
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 52
    :cond_0
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 53
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 54
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lb/b/a/a/d/f/n;FF)V
    .locals 1

    .line 38
    iget-boolean v0, p0, Lb/b/a/a/d/h/a;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lb/b/a/a/d/h/a;->j:Z

    if-nez v0, :cond_0

    .line 39
    invoke-direct {p0, p2, p3}, Lb/b/a/a/d/h/a;->a(FF)V

    .line 40
    iget p2, p0, Lb/b/a/a/d/h/a;->k:I

    invoke-virtual {p0, p2}, Lb/b/a/a/d/h/a;->b(I)V

    .line 41
    iget-object p2, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    if-eqz p2, :cond_1

    .line 42
    iget-object p2, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p3

    invoke-interface {p2, p3, p1}, Lb/b/a/a/d/f/g;->a(Landroid/view/View;Lb/b/a/a/d/f/n;)V

    goto :goto_0

    .line 43
    :cond_0
    invoke-static {}, Lb/b/a/a/d/h/e;->〇O8o08O()Lb/b/a/a/d/h/e;

    move-result-object p2

    iget-object p3, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p2, p3}, Lb/b/a/a/d/h/e;->Oo08(Lcom/bytedance/sdk/component/widget/SSWebView;)Z

    .line 44
    invoke-virtual {p1}, Lb/b/a/a/d/f/n;->OO0o〇〇()I

    move-result p1

    invoke-direct {p0, p1}, Lb/b/a/a/d/h/a;->c(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a(Lb/b/a/a/d/h/a;Lb/b/a/a/d/f/n;FF)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lb/b/a/a/d/h/a;->a(Lb/b/a/a/d/f/n;FF)V

    return-void
.end method

.method private c(I)V
    .locals 1

    .line 2
    iget-object v0, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {v0, p1}, Lb/b/a/a/d/f/g;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 1
    return-void
.end method

.method public a(Landroid/view/View;ILb/b/a/a/d/b;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lb/b/a/a/d/h/a;->g:Lb/b/a/a/d/f/h;

    if-eqz v0, :cond_0

    .line 46
    invoke-interface {v0, p1, p2, p3}, Lb/b/a/a/d/f/h;->a(Landroid/view/View;ILb/b/a/a/d/b;)V

    :cond_0
    return-void
.end method

.method public a(Lb/b/a/a/d/f/g;)V
    .locals 2

    .line 5
    iput-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    .line 6
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    const/16 v0, 0x66

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object p1

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 7
    :cond_0
    invoke-static {}, Lb/b/a/a/d/e/b/a;->〇〇808〇()Z

    move-result p1

    if-nez p1, :cond_1

    .line 8
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    return-void

    .line 9
    :cond_1
    iget-object p1, p0, Lb/b/a/a/d/h/a;->c:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 10
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    return-void

    .line 11
    :cond_2
    iget-object p1, p0, Lb/b/a/a/d/h/a;->l:Lb/b/a/a/d/e/c/b;

    if-nez p1, :cond_3

    iget-object p1, p0, Lb/b/a/a/d/h/a;->b:Lorg/json/JSONObject;

    invoke-static {p1}, Lb/b/a/a/d/e/b/a;->o〇0(Lorg/json/JSONObject;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 12
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    const/16 v0, 0x67

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    return-void

    .line 13
    :cond_3
    iget-object p1, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    invoke-virtual {p1}, Lb/b/a/a/d/f/m;->〇〇808〇()Lb/b/a/a/d/f/i;

    move-result-object p1

    iget-boolean v1, p0, Lb/b/a/a/d/h/a;->e:Z

    invoke-interface {p1, v1}, Lb/b/a/a/d/f/i;->a(Z)V

    .line 14
    iget-boolean p1, p0, Lb/b/a/a/d/h/a;->e:Z

    if-eqz p1, :cond_4

    const-string p1, "javascript:window.SDK_RESET_RENDER();window.SDK_TRIGGER_RENDER();"

    .line 15
    :try_start_0
    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->f()V

    .line 16
    iget-object v1, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    invoke-virtual {v1}, Lb/b/a/a/d/f/m;->〇〇808〇()Lb/b/a/a/d/f/i;

    move-result-object v1

    invoke-interface {v1}, Lb/b/a/a/d/f/i;->c()V

    .line 17
    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/l;->a(Landroid/webkit/WebView;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "WebViewRender"

    const-string v1, "reuse webview load fail "

    .line 18
    invoke-static {p1, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    invoke-static {}, Lb/b/a/a/d/h/e;->〇O8o08O()Lb/b/a/a/d/h/e;

    move-result-object p1

    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p1, v1}, Lb/b/a/a/d/h/e;->Oo08(Lcom/bytedance/sdk/component/widget/SSWebView;)Z

    .line 20
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    goto :goto_0

    .line 21
    :cond_4
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->f()V

    .line 23
    iget-object v0, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    invoke-virtual {v0}, Lb/b/a/a/d/f/m;->〇〇808〇()Lb/b/a/a/d/f/i;

    move-result-object v0

    invoke-interface {v0}, Lb/b/a/a/d/f/i;->c()V

    .line 24
    iget-object v0, p0, Lb/b/a/a/d/h/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    .line 25
    :cond_5
    :goto_1
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    return-void
.end method

.method public a(Lb/b/a/a/d/f/h;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lb/b/a/a/d/h/a;->g:Lb/b/a/a/d/f/h;

    return-void
.end method

.method public a(Lb/b/a/a/d/f/n;)V
    .locals 6

    const/16 v0, 0x69

    if-nez p1, :cond_1

    .line 27
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    if-eqz p1, :cond_0

    .line 28
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    :cond_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p1}, Lb/b/a/a/d/f/n;->〇0000OOO()Z

    move-result v1

    .line 30
    invoke-virtual {p1}, Lb/b/a/a/d/f/n;->oO80()D

    move-result-wide v2

    double-to-float v2, v2

    .line 31
    invoke-virtual {p1}, Lb/b/a/a/d/f/n;->〇080()D

    move-result-wide v3

    double-to-float v3, v3

    const/4 v4, 0x0

    cmpg-float v5, v2, v4

    if-lez v5, :cond_4

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_2

    goto :goto_1

    .line 32
    :cond_2
    iput-boolean v1, p0, Lb/b/a/a/d/h/a;->f:Z

    .line 33
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 34
    invoke-direct {p0, p1, v2, v3}, Lb/b/a/a/d/h/a;->a(Lb/b/a/a/d/f/n;FF)V

    goto :goto_0

    .line 35
    :cond_3
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lb/b/a/a/d/h/a$a;

    invoke-direct {v1, p0, p1, v2, v3}, Lb/b/a/a/d/h/a$a;-><init>(Lb/b/a/a/d/h/a;Lb/b/a/a/d/f/n;FF)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    .line 36
    :cond_4
    :goto_1
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    if-eqz p1, :cond_5

    .line 37
    iget-object p1, p0, Lb/b/a/a/d/h/a;->d:Lb/b/a/a/d/f/g;

    invoke-interface {p1, v0}, Lb/b/a/a/d/f/g;->a(I)V

    :cond_5
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lb/b/a/a/d/h/a;->c:Ljava/lang/String;

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lb/b/a/a/d/h/a;->b:Lorg/json/JSONObject;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lb/b/a/a/d/h/a;->j:Z

    return-void
.end method

.method public b()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract b(I)V
.end method

.method public c()V
    .locals 0

    .line 1
    return-void
.end method

.method public d()Lcom/bytedance/sdk/component/widget/SSWebView;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->f()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public bridge synthetic e()Landroid/view/View;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->d()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract f()Lcom/bytedance/sdk/component/widget/SSWebView;
.end method

.method public g()Lb/b/a/a/d/f/m;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/d/h/a;->h:Lb/b/a/a/d/f/m;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public h()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->j()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/b;->a(Landroid/view/View;)Landroid/app/Activity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract i()V
.end method

.method protected j()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public k()V
    .locals 2

    .line 1
    iget-object v0, p0, Lb/b/a/a/d/h/a;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lb/b/a/a/d/h/a;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lb/b/a/a/d/h/a;->i()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroid/view/ViewGroup;

    .line 34
    .line 35
    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    iget-boolean v0, p0, Lb/b/a/a/d/h/a;->f:Z

    .line 41
    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    invoke-static {}, Lb/b/a/a/d/h/e;->〇O8o08O()Lb/b/a/a/d/h/e;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lb/b/a/a/d/h/e;->〇80〇808〇O(Lcom/bytedance/sdk/component/widget/SSWebView;)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-static {}, Lb/b/a/a/d/h/e;->〇O8o08O()Lb/b/a/a/d/h/e;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    iget-object v1, p0, Lb/b/a/a/d/h/a;->i:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Lb/b/a/a/d/h/e;->Oo08(Lcom/bytedance/sdk/component/widget/SSWebView;)Z

    .line 61
    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

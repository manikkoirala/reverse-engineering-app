.class public Lb/b/a/a/k/i/a;
.super Ljava/lang/Object;
.source "HandlerPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/b/a/a/k/i/a$b;
    }
.end annotation


# instance fields
.field private final 〇080:Lb/b/a/a/k/i/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/b/a/a/k/i/d<",
            "Lb/b/a/a/k/i/b;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/bytedance/sdk/component/utils/y;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    .line 3
    invoke-static {v0}, Lb/b/a/a/k/i/d;->〇o00〇〇Oo(I)Lb/b/a/a/k/i/d;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/k/i/a;->〇080:Lb/b/a/a/k/i/d;

    return-void
.end method

.method synthetic constructor <init>(Lb/b/a/a/k/i/a$a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lb/b/a/a/k/i/a;-><init>()V

    return-void
.end method

.method public static 〇080()Lb/b/a/a/k/i/a;
    .locals 1

    .line 1
    invoke-static {}, Lb/b/a/a/k/i/a$b;->〇080()Lb/b/a/a/k/i/a;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o00〇〇Oo(Lcom/bytedance/sdk/component/utils/y$a;Ljava/lang/String;)Lb/b/a/a/k/i/b;
    .locals 1

    .line 1
    new-instance v0, Landroid/os/HandlerThread;

    .line 2
    .line 3
    invoke-direct {v0, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 7
    .line 8
    .line 9
    new-instance p2, Lb/b/a/a/k/i/b;

    .line 10
    .line 11
    invoke-direct {p2, v0, p1}, Lb/b/a/a/k/i/b;-><init>(Landroid/os/HandlerThread;Lcom/bytedance/sdk/component/utils/y$a;)V

    .line 12
    .line 13
    .line 14
    return-object p2
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method


# virtual methods
.method public O8(Lcom/bytedance/sdk/component/utils/y;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lb/b/a/a/k/i/b;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p1, Lb/b/a/a/k/i/b;

    .line 6
    .line 7
    iget-object v0, p0, Lb/b/a/a/k/i/a;->〇080:Lb/b/a/a/k/i/d;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lb/b/a/a/k/i/d;->〇o〇(Lb/b/a/a/k/i/c;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p1}, Lb/b/a/a/k/i/b;->〇o〇()V

    .line 17
    .line 18
    .line 19
    :goto_0
    const/4 p1, 0x1

    .line 20
    return p1

    .line 21
    :cond_1
    const/4 p1, 0x0

    .line 22
    return p1
    .line 23
.end method

.method public Oo08()Lcom/bytedance/sdk/component/utils/y;
    .locals 2

    .line 1
    iget-object v0, p0, Lb/b/a/a/k/i/a;->〇o00〇〇Oo:Lcom/bytedance/sdk/component/utils/y;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    const-class v0, Lb/b/a/a/k/i/a;

    .line 6
    .line 7
    monitor-enter v0

    .line 8
    :try_start_0
    iget-object v1, p0, Lb/b/a/a/k/i/a;->〇o00〇〇Oo:Lcom/bytedance/sdk/component/utils/y;

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    const-string v1, "csj_io_handler"

    .line 13
    .line 14
    invoke-virtual {p0, v1}, Lb/b/a/a/k/i/a;->〇o〇(Ljava/lang/String;)Lcom/bytedance/sdk/component/utils/y;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iput-object v1, p0, Lb/b/a/a/k/i/a;->〇o00〇〇Oo:Lcom/bytedance/sdk/component/utils/y;

    .line 19
    .line 20
    :cond_0
    monitor-exit v0

    .line 21
    goto :goto_0

    .line 22
    :catchall_0
    move-exception v1

    .line 23
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    throw v1

    .line 25
    :cond_1
    :goto_0
    iget-object v0, p0, Lb/b/a/a/k/i/a;->〇o00〇〇Oo:Lcom/bytedance/sdk/component/utils/y;

    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public o〇0(Lcom/bytedance/sdk/component/utils/y$a;Ljava/lang/String;)Lcom/bytedance/sdk/component/utils/y;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/k/i/a;->〇080:Lb/b/a/a/k/i/d;

    .line 2
    .line 3
    invoke-virtual {v0}, Lb/b/a/a/k/i/d;->〇080()Lb/b/a/a/k/i/c;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lb/b/a/a/k/i/b;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lb/b/a/a/k/i/b;->〇080(Lcom/bytedance/sdk/component/utils/y$a;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p2}, Lb/b/a/a/k/i/b;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-direct {p0, p1, p2}, Lb/b/a/a/k/i/a;->〇o00〇〇Oo(Lcom/bytedance/sdk/component/utils/y$a;Ljava/lang/String;)Lb/b/a/a/k/i/b;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    :goto_0
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method public 〇o〇(Ljava/lang/String;)Lcom/bytedance/sdk/component/utils/y;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0, p1}, Lb/b/a/a/k/i/a;->o〇0(Lcom/bytedance/sdk/component/utils/y$a;Ljava/lang/String;)Lcom/bytedance/sdk/component/utils/y;

    .line 3
    .line 4
    .line 5
    move-result-object p1

    .line 6
    return-object p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

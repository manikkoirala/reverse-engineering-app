.class public Lb/b/a/a/i/a/k/g/c;
.super Landroid/os/HandlerThread;
.source "AdPriorityLogThread.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static O0O:I = 0xc8

.field private static 〇〇08O:I = 0xa


# instance fields
.field private volatile O8o08O8O:I

.field protected OO:Lb/b/a/a/i/a/j/d;

.field private volatile OO〇00〇8oO:Landroid/os/Handler;

.field private volatile o0:Z

.field private final o8〇OO0〇0o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private final oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final oOo〇8o008:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final ooo0〇〇O:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lb/b/a/a/i/a/m/a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile 〇080OO8〇0:J

.field private 〇08O〇00〇o:Lb/b/a/a/i/a/k/c;

.field private volatile 〇0O:J

.field private final 〇8〇oO〇〇8o:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final 〇OOo8〇0:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public constructor <init>(Ljava/util/concurrent/PriorityBlockingQueue;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lb/b/a/a/i/a/m/a;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "csj_log"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 8
    .line 9
    new-instance v0, Ljava/lang/Object;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->〇OOo8〇0:Ljava/lang/Object;

    .line 15
    .line 16
    const-wide/16 v0, 0x0

    .line 17
    .line 18
    iput-wide v0, p0, Lb/b/a/a/i/a/k/g/c;->〇080OO8〇0:J

    .line 19
    .line 20
    iput-wide v0, p0, Lb/b/a/a/i/a/k/g/c;->〇0O:J

    .line 21
    .line 22
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 26
    .line 27
    .line 28
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo〇8o008:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 29
    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 31
    .line 32
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 36
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .line 41
    .line 42
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 43
    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 45
    .line 46
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->〇8〇oO〇〇8o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 50
    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 52
    .line 53
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->ooo0〇〇O:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 57
    .line 58
    iput-object p1, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 59
    .line 60
    new-instance p1, Lb/b/a/a/i/a/j/b;

    .line 61
    .line 62
    invoke-direct {p1}, Lb/b/a/a/i/a/j/b;-><init>()V

    .line 63
    .line 64
    .line 65
    iput-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO:Lb/b/a/a/i/a/j/d;

    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private O8〇o()Z
    .locals 2

    .line 1
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 2
    .line 3
    iget-boolean v0, v0, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 8
    .line 9
    const/4 v1, 0x4

    .line 10
    if-eq v0, v1, :cond_0

    .line 11
    .line 12
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 13
    .line 14
    const/4 v1, 0x7

    .line 15
    if-eq v0, v1, :cond_0

    .line 16
    .line 17
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 18
    .line 19
    const/4 v1, 0x6

    .line 20
    if-eq v0, v1, :cond_0

    .line 21
    .line 22
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 23
    .line 24
    const/4 v1, 0x5

    .line 25
    if-eq v0, v1, :cond_0

    .line 26
    .line 27
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 28
    .line 29
    const/4 v1, 0x2

    .line 30
    if-ne v0, v1, :cond_1

    .line 31
    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private OO0o〇〇(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4
    .line 5
    .line 6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v0, "a batch applog generation cur="

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const-string v0, "PADLT"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lb/b/a/a/i/a/i;->〇080()Lb/b/a/a/i/a/f;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    if-eqz p1, :cond_0

    .line 43
    .line 44
    invoke-interface {p1}, Lb/b/a/a/i/a/f;->h()Lb/b/a/a/i/a/h;

    .line 45
    .line 46
    .line 47
    :cond_0
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 48
    .line 49
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    sget v2, Lb/b/a/a/i/a/k/g/c;->〇〇08O:I

    .line 54
    .line 55
    const/4 v3, 0x0

    .line 56
    const/16 v4, 0xb

    .line 57
    .line 58
    if-lt v1, v2, :cond_2

    .line 59
    .line 60
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 61
    .line 62
    invoke-virtual {p1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-eqz p1, :cond_1

    .line 67
    .line 68
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 69
    .line 70
    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 71
    .line 72
    .line 73
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    .line 74
    .line 75
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 76
    .line 77
    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 78
    .line 79
    .line 80
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 81
    .line 82
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 83
    .line 84
    .line 85
    const-string v1, "max_size_dispatch"

    .line 86
    .line 87
    invoke-direct {p0, p1, v3, v1}, Lb/b/a/a/i/a/k/g/c;->〇O00(Ljava/util/List;ZLjava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇o00〇〇Oo()V

    .line 91
    .line 92
    .line 93
    new-instance p1, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v1, "batch applog report ( size ) "

    .line 99
    .line 100
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    sget v1, Lb/b/a/a/i/a/k/g/c;->〇〇08O:I

    .line 104
    .line 105
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-static {v0, p1}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_2
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 117
    .line 118
    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    if-nez v1, :cond_6

    .line 123
    .line 124
    invoke-virtual {p0, v3}, Lb/b/a/a/i/a/k/g/c;->〇〇8O0〇8(Z)V

    .line 125
    .line 126
    .line 127
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 128
    .line 129
    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    if-eqz v1, :cond_3

    .line 134
    .line 135
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 136
    .line 137
    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 138
    .line 139
    .line 140
    :cond_3
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 141
    .line 142
    const/4 v2, 0x1

    .line 143
    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    if-eqz v1, :cond_4

    .line 148
    .line 149
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 150
    .line 151
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 152
    .line 153
    .line 154
    :cond_4
    sget v1, Lb/b/a/a/i/a/k/g/c;->O0O:I

    .line 155
    .line 156
    int-to-long v1, v1

    .line 157
    if-eqz p1, :cond_5

    .line 158
    .line 159
    invoke-interface {p1}, Lb/b/a/a/i/a/f;->h()Lb/b/a/a/i/a/h;

    .line 160
    .line 161
    .line 162
    :cond_5
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 163
    .line 164
    invoke-virtual {p1, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 165
    .line 166
    .line 167
    new-instance p1, Ljava/lang/StringBuilder;

    .line 168
    .line 169
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .line 171
    .line 172
    const-string v3, "batch applog report delay ( time )"

    .line 173
    .line 174
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    invoke-static {v0, p1}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    goto :goto_0

    .line 188
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 189
    .line 190
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .line 192
    .line 193
    const-string v0, "uploadBatchOptimize nothing\uff1a"

    .line 194
    .line 195
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 199
    .line 200
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 201
    .line 202
    .line 203
    move-result v0

    .line 204
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    const-string v0, "  "

    .line 208
    .line 209
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    iget-boolean v0, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 213
    .line 214
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    :goto_0
    return-void
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method private OO0o〇〇〇〇0(Lb/b/a/a/i/a/m/a;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo〇8o008:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 5
    .line 6
    .line 7
    const-string v0, "handleThreadMessage()"

    .line 8
    .line 9
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    if-nez p2, :cond_0

    .line 14
    .line 15
    move-object p2, p1

    .line 16
    check-cast p2, Lb/b/a/a/i/a/m/b;

    .line 17
    .line 18
    invoke-virtual {p2}, Lb/b/a/a/i/a/m/b;->O8()I

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    iput p2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 23
    .line 24
    iget p2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 25
    .line 26
    const/4 v1, 0x6

    .line 27
    if-eq p2, v1, :cond_2

    .line 28
    .line 29
    sget-object p2, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 30
    .line 31
    invoke-virtual {p2}, Lb/b/a/a/i/a/k/e/a;->〇o()Ljava/util/concurrent/atomic/AtomicLong;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-static {p2, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->oo88o8O(Lb/b/a/a/i/a/m/a;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    move-object v1, p1

    .line 43
    check-cast v1, Lb/b/a/a/i/a/m/b;

    .line 44
    .line 45
    invoke-virtual {v1}, Lb/b/a/a/i/a/m/b;->O8()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-ne v2, v0, :cond_1

    .line 50
    .line 51
    iput v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 52
    .line 53
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->oo88o8O(Lb/b/a/a/i/a/m/a;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v1}, Lb/b/a/a/i/a/m/b;->O8()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    const/4 v1, 0x2

    .line 62
    if-ne v0, v1, :cond_2

    .line 63
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v2, "before size:"

    .line 70
    .line 71
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->oo〇()V

    .line 85
    .line 86
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    .line 88
    .line 89
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .line 91
    .line 92
    const-string v2, "after size :"

    .line 93
    .line 94
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    invoke-static {p2}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    iput v1, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 108
    .line 109
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->oo88o8O(Lb/b/a/a/i/a/m/a;)V

    .line 110
    .line 111
    .line 112
    :cond_2
    :goto_0
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method public static OOO〇O0(I)V
    .locals 2

    .line 1
    sput p0, Lb/b/a/a/i/a/k/g/c;->O0O:I

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "applog_interval="

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    const-string v0, "PADLT"

    .line 21
    .line 22
    invoke-static {v0, p0}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private Oo08(ILjava/util/List;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;J)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->〇OOo8〇0:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    if-eqz p2, :cond_10

    .line 5
    .line 6
    :try_start_0
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 7
    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    goto/16 :goto_1

    .line 11
    .line 12
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lb/b/a/a/i/a/l/a;->〇o〇(ILjava/util/List;J)V

    .line 13
    .line 14
    .line 15
    iget-object p3, p0, Lb/b/a/a/i/a/k/g/c;->OO:Lb/b/a/a/i/a/j/d;

    .line 16
    .line 17
    invoke-interface {p3, p1, p2}, Lb/b/a/a/i/a/j/d;->〇o〇(ILjava/util/List;)V

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    invoke-virtual {p2}, Lb/b/a/a/i/a/i;->〇080()Lb/b/a/a/i/a/f;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    if-eqz p2, :cond_1

    .line 29
    .line 30
    invoke-interface {p2}, Lb/b/a/a/i/a/f;->h()Lb/b/a/a/i/a/h;

    .line 31
    .line 32
    .line 33
    :cond_1
    const/4 p2, -0x2

    .line 34
    const/4 p3, 0x1

    .line 35
    const/4 p4, 0x3

    .line 36
    const/4 v1, 0x0

    .line 37
    const/4 v2, 0x2

    .line 38
    if-eq p1, p2, :cond_a

    .line 39
    .line 40
    const/4 p2, -0x1

    .line 41
    if-eq p1, p2, :cond_6

    .line 42
    .line 43
    if-eqz p1, :cond_a

    .line 44
    .line 45
    const/16 p2, 0xc8

    .line 46
    .line 47
    if-eq p1, p2, :cond_6

    .line 48
    .line 49
    const/16 p2, 0x1fd

    .line 50
    .line 51
    if-eq p1, p2, :cond_2

    .line 52
    .line 53
    goto/16 :goto_0

    .line 54
    .line 55
    :cond_2
    sget-object p1, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 56
    .line 57
    iput-boolean p3, p1, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 58
    .line 59
    iput-boolean v1, p1, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 60
    .line 61
    const-string p1, "-----------------  server busy start---------------- "

    .line 62
    .line 63
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 67
    .line 68
    invoke-virtual {p1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    if-eqz p1, :cond_3

    .line 73
    .line 74
    const-string p1, "already server busy message"

    .line 75
    .line 76
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    monitor-exit v0

    .line 80
    return-void

    .line 81
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 82
    .line 83
    .line 84
    move-result-wide p1

    .line 85
    iget-wide v3, p0, Lb/b/a/a/i/a/k/g/c;->〇080OO8〇0:J

    .line 86
    .line 87
    sub-long/2addr p1, v3

    .line 88
    const-wide/16 v3, 0x7530

    .line 89
    .line 90
    cmp-long p3, p1, v3

    .line 91
    .line 92
    if-gez p3, :cond_4

    .line 93
    .line 94
    const-string p1, "already server busy\uff0ctoo short"

    .line 95
    .line 96
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    monitor-exit v0

    .line 100
    return-void

    .line 101
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 102
    .line 103
    .line 104
    move-result-wide p1

    .line 105
    iput-wide p1, p0, Lb/b/a/a/i/a/k/g/c;->〇080OO8〇0:J

    .line 106
    .line 107
    const-string p1, "-----------------  server busy send---------------- "

    .line 108
    .line 109
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 113
    .line 114
    invoke-virtual {p1, p4}, Landroid/os/Handler;->hasMessages(I)Z

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-eqz p1, :cond_5

    .line 119
    .line 120
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 121
    .line 122
    invoke-virtual {p1, p4}, Landroid/os/Handler;->removeMessages(I)V

    .line 123
    .line 124
    .line 125
    :cond_5
    invoke-virtual {p0, v2, v3, v4}, Lb/b/a/a/i/a/k/g/c;->O8(IJ)V

    .line 126
    .line 127
    .line 128
    goto/16 :goto_0

    .line 129
    .line 130
    :cond_6
    sget-object p1, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 131
    .line 132
    iget-boolean p2, p1, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 133
    .line 134
    if-nez p2, :cond_7

    .line 135
    .line 136
    iget-boolean p2, p1, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 137
    .line 138
    if-eqz p2, :cond_e

    .line 139
    .line 140
    :cond_7
    iput-boolean v1, p1, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 141
    .line 142
    iput-boolean v1, p1, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 143
    .line 144
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 145
    .line 146
    invoke-virtual {p1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    if-eqz p1, :cond_8

    .line 151
    .line 152
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 153
    .line 154
    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 155
    .line 156
    .line 157
    :cond_8
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 158
    .line 159
    invoke-virtual {p1, p4}, Landroid/os/Handler;->hasMessages(I)Z

    .line 160
    .line 161
    .line 162
    move-result p1

    .line 163
    if-eqz p1, :cond_9

    .line 164
    .line 165
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 166
    .line 167
    invoke-virtual {p1, p4}, Landroid/os/Handler;->removeMessages(I)V

    .line 168
    .line 169
    .line 170
    :cond_9
    const-wide/16 p1, 0x0

    .line 171
    .line 172
    iput-wide p1, p0, Lb/b/a/a/i/a/k/g/c;->〇0O:J

    .line 173
    .line 174
    iput-wide p1, p0, Lb/b/a/a/i/a/k/g/c;->〇080OO8〇0:J

    .line 175
    .line 176
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->〇8〇oO〇〇8o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 177
    .line 178
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 179
    .line 180
    .line 181
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->ooo0〇〇O:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 182
    .line 183
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 184
    .line 185
    .line 186
    const-string p1, "--dispatchResult flush--"

    .line 187
    .line 188
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    sget-object p1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 192
    .line 193
    invoke-virtual {p1}, Lb/b/a/a/i/a/k/e/a;->O8〇o()Ljava/util/concurrent/atomic/AtomicLong;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    invoke-static {p1, p3}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {p0, v2}, Lb/b/a/a/i/a/k/g/c;->O8ooOoo〇(I)V

    .line 201
    .line 202
    .line 203
    goto :goto_0

    .line 204
    :cond_a
    sget-object p1, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 205
    .line 206
    iput-boolean v1, p1, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 207
    .line 208
    iput-boolean p3, p1, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 209
    .line 210
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 211
    .line 212
    invoke-virtual {p1, p4}, Landroid/os/Handler;->hasMessages(I)Z

    .line 213
    .line 214
    .line 215
    move-result p1

    .line 216
    if-eqz p1, :cond_b

    .line 217
    .line 218
    const-string p1, "already routine error message"

    .line 219
    .line 220
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    monitor-exit v0

    .line 224
    return-void

    .line 225
    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 226
    .line 227
    .line 228
    move-result-wide p1

    .line 229
    iget-wide v3, p0, Lb/b/a/a/i/a/k/g/c;->〇0O:J

    .line 230
    .line 231
    sub-long/2addr p1, v3

    .line 232
    const-wide/16 v3, 0x3a98

    .line 233
    .line 234
    cmp-long p3, p1, v3

    .line 235
    .line 236
    if-gez p3, :cond_c

    .line 237
    .line 238
    const-string p1, "already routine error,too short"

    .line 239
    .line 240
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 241
    .line 242
    .line 243
    monitor-exit v0

    .line 244
    return-void

    .line 245
    :cond_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 246
    .line 247
    .line 248
    move-result-wide p1

    .line 249
    iput-wide p1, p0, Lb/b/a/a/i/a/k/g/c;->〇0O:J

    .line 250
    .line 251
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 252
    .line 253
    invoke-virtual {p1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    .line 254
    .line 255
    .line 256
    move-result p1

    .line 257
    if-eqz p1, :cond_d

    .line 258
    .line 259
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 260
    .line 261
    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 262
    .line 263
    .line 264
    :cond_d
    invoke-virtual {p0, p4, v3, v4}, Lb/b/a/a/i/a/k/g/c;->O8(IJ)V

    .line 265
    .line 266
    .line 267
    :cond_e
    :goto_0
    iget p1, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 268
    .line 269
    if-ne p1, v2, :cond_f

    .line 270
    .line 271
    const-string p1, "send notify"

    .line 272
    .line 273
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->〇OOo8〇0:Ljava/lang/Object;

    .line 277
    .line 278
    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    .line 279
    .line 280
    .line 281
    :cond_f
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    new-instance p1, Ljava/lang/StringBuilder;

    .line 283
    .line 284
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 285
    .line 286
    .line 287
    const-string p2, "upload thread reuse or close: "

    .line 288
    .line 289
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    .line 291
    .line 292
    iget-boolean p2, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 293
    .line 294
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    const-string p2, " queue:"

    .line 298
    .line 299
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    .line 301
    .line 302
    iget-object p2, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 303
    .line 304
    invoke-virtual {p2}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 305
    .line 306
    .line 307
    move-result p2

    .line 308
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    const-string p2, " "

    .line 312
    .line 313
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    iget p2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 317
    .line 318
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 319
    .line 320
    .line 321
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 322
    .line 323
    .line 324
    move-result-object p1

    .line 325
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->Oo08(Ljava/lang/String;)V

    .line 326
    .line 327
    .line 328
    return-void

    .line 329
    :cond_10
    :goto_1
    :try_start_1
    monitor-exit v0

    .line 330
    return-void

    .line 331
    :catchall_0
    move-exception p1

    .line 332
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    throw p1
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
.end method

.method private Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lb/b/a/a/i/a/k/g/c;->〇O8o08O(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, p1, v0, p2}, Lb/b/a/a/i/a/k/g/c;->〇O00(Ljava/util/List;ZLjava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇o00〇〇Oo()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
.end method

.method private O〇8O8〇008()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 10
    .line 11
    const/16 v1, 0xb

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lb/b/a/a/i/a/k/g/c;->〇0000OOO()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    invoke-virtual {p0, v0}, Lb/b/a/a/i/a/k/g/c;->〇〇8O0〇8(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic o800o8O(Lb/b/a/a/i/a/k/g/c;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic oO80(Lb/b/a/a/i/a/k/g/c;ZLb/b/a/a/i/a/k/g/b;Ljava/util/List;J)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lb/b/a/a/i/a/k/g/c;->〇0〇O0088o(ZLb/b/a/a/i/a/k/g/b;Ljava/util/List;J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method

.method private oo88o8O(Lb/b/a/a/i/a/m/a;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->O8〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "upload cancel:"

    .line 15
    .line 16
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    iget v3, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 20
    .line 21
    invoke-static {v3}, Lb/b/a/a/i/a/l/a;->〇o00〇〇Oo(I)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇〇888(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 36
    .line 37
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->oO00OOO()Ljava/util/concurrent/atomic/AtomicLong;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-nez v0, :cond_1

    .line 51
    .line 52
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 53
    .line 54
    const/4 v3, 0x2

    .line 55
    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-nez v0, :cond_0

    .line 60
    .line 61
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 62
    .line 63
    iput-boolean v2, v0, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 64
    .line 65
    const-wide/16 v3, 0x0

    .line 66
    .line 67
    iput-wide v3, p0, Lb/b/a/a/i/a/k/g/c;->〇0O:J

    .line 68
    .line 69
    iput-wide v3, p0, Lb/b/a/a/i/a/k/g/c;->〇080OO8〇0:J

    .line 70
    .line 71
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->〇8〇oO〇〇8o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 72
    .line 73
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->ooo0〇〇O:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    .line 78
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p0, v2}, Lb/b/a/a/i/a/k/g/c;->〇〇8O0〇8(Z)V

    .line 83
    .line 84
    .line 85
    :cond_1
    return-void

    .line 86
    :cond_2
    :goto_0
    iget v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 87
    .line 88
    sget-object v3, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 89
    .line 90
    iget-boolean v3, v3, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 91
    .line 92
    invoke-virtual {p0, v0, v3}, Lb/b/a/a/i/a/k/g/c;->OoO8(IZ)Z

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    iget v3, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 97
    .line 98
    invoke-static {v0, v3, p1}, Lb/b/a/a/i/a/l/a;->OO0o〇〇〇〇0(ZILb/b/a/a/i/a/m/a;)V

    .line 99
    .line 100
    .line 101
    sget-object v3, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 102
    .line 103
    invoke-virtual {v3}, Lb/b/a/a/i/a/k/e/a;->o〇O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    invoke-static {v3, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 108
    .line 109
    .line 110
    if-eqz v0, :cond_4

    .line 111
    .line 112
    iget-object v3, p0, Lb/b/a/a/i/a/k/g/c;->OO:Lb/b/a/a/i/a/j/d;

    .line 113
    .line 114
    iget v4, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 115
    .line 116
    const/4 v5, -0x1

    .line 117
    const/4 v6, 0x0

    .line 118
    invoke-interface {v3, v4, v5, v6}, Lb/b/a/a/i/a/j/d;->〇080(IILjava/util/List;)Ljava/util/List;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    if-eqz v3, :cond_3

    .line 123
    .line 124
    new-instance v4, Ljava/lang/StringBuilder;

    .line 125
    .line 126
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .line 128
    .line 129
    const-string v5, "upload size="

    .line 130
    .line 131
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 135
    .line 136
    .line 137
    move-result v5

    .line 138
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    const-string v5, "  times="

    .line 142
    .line 143
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v4

    .line 153
    invoke-static {v4}, Lb/b/a/a/i/a/l/c;->〇〇888(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    invoke-direct {p0, v3}, Lb/b/a/a/i/a/k/g/c;->〇oo〇(Ljava/util/List;)V

    .line 157
    .line 158
    .line 159
    goto :goto_1

    .line 160
    :cond_3
    const-string v3, "no need upload"

    .line 161
    .line 162
    invoke-static {v3}, Lb/b/a/a/i/a/l/c;->Oo08(Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->O〇8O8〇008()V

    .line 166
    .line 167
    .line 168
    goto :goto_1

    .line 169
    :cond_4
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->O〇8O8〇008()V

    .line 170
    .line 171
    .line 172
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 173
    .line 174
    new-instance v3, Ljava/lang/StringBuilder;

    .line 175
    .line 176
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .line 178
    .line 179
    const-string v4, "times="

    .line 180
    .line 181
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    invoke-static {v3}, Lb/b/a/a/i/a/l/c;->Oo08(Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    if-eqz v0, :cond_5

    .line 195
    .line 196
    const/4 v0, 0x6

    .line 197
    if-le v2, v0, :cond_2

    .line 198
    .line 199
    :cond_5
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method private oo〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x64

    .line 8
    .line 9
    if-lt v0, v1, :cond_2

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    :goto_0
    if-ge v0, v1, :cond_2

    .line 13
    .line 14
    iget-object v2, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lb/b/a/a/i/a/m/a;

    .line 21
    .line 22
    instance-of v3, v2, Lb/b/a/a/i/a/m/b;

    .line 23
    .line 24
    if-eqz v3, :cond_0

    .line 25
    .line 26
    const-string v2, "ignore tm"

    .line 27
    .line 28
    invoke-static {v2}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    if-eqz v2, :cond_1

    .line 33
    .line 34
    invoke-direct {p0, v2}, Lb/b/a/a/i/a/k/g/c;->〇80〇808〇O(Lb/b/a/a/i/a/m/a;)V

    .line 35
    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_1
    const-string v2, "event == null"

    .line 39
    .line 40
    invoke-static {v2}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private o〇0(Lb/b/a/a/i/a/k/g/b;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/b/a/a/i/a/k/g/b;",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    iget-boolean p1, p1, Lb/b/a/a/i/a/k/g/b;->〇080:Z

    .line 4
    .line 5
    if-eqz p1, :cond_2

    .line 6
    .line 7
    invoke-static {}, Lb/b/a/a/i/a/b;->〇080()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-eqz p2, :cond_2

    .line 12
    .line 13
    if-eqz p1, :cond_2

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lb/b/a/a/i/a/m/a;

    .line 36
    .line 37
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->c()B

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const/4 v2, 0x1

    .line 42
    if-ne v1, v2, :cond_0

    .line 43
    .line 44
    invoke-static {v0}, Lb/b/a/a/i/a/l/a;->〇〇808〇(Lb/b/a/a/i/a/m/a;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-static {v0}, Lb/b/a/a/i/a/l/a;->o800o8O(Lb/b/a/a/i/a/m/a;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-eqz v3, :cond_0

    .line 61
    .line 62
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    check-cast v3, Lb/b/a/a/i/a/e;

    .line 67
    .line 68
    if-eqz v3, :cond_1

    .line 69
    .line 70
    invoke-interface {v3, v1, v0}, Lb/b/a/a/i/a/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_2
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method private o〇O8〇〇o(Ljava/util/List;ZJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;ZJ)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 4
    .line 5
    .line 6
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 7
    .line 8
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->〇8o8o〇()Ljava/util/concurrent/atomic/AtomicLong;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 14
    .line 15
    .line 16
    :try_start_0
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->〇08O〇00〇o:Lb/b/a/a/i/a/k/c;

    .line 17
    .line 18
    new-instance v2, Lb/b/a/a/i/a/k/g/c$b;

    .line 19
    .line 20
    invoke-direct {v2, p0, p2, p3, p4}, Lb/b/a/a/i/a/k/g/c$b;-><init>(Lb/b/a/a/i/a/k/g/c;ZJ)V

    .line 21
    .line 22
    .line 23
    invoke-interface {v0, p1, v2}, Lb/b/a/a/i/a/k/c;->a(Ljava/util/List;Lb/b/a/a/i/a/k/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception p1

    .line 28
    new-instance p2, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string p3, "outer exception\uff1a"

    .line 34
    .line 35
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    sget-object p1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 53
    .line 54
    invoke-virtual {p1}, Lb/b/a/a/i/a/k/e/a;->〇80〇808〇O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62
    .line 63
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private o〇〇0〇()V
    .locals 6

    .line 1
    :goto_0
    invoke-virtual {p0}, Lb/b/a/a/i/a/k/g/c;->〇0000OOO()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    :try_start_0
    sget-object v1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 9
    .line 10
    invoke-virtual {v1}, Lb/b/a/a/i/a/k/e/a;->o〇0()Ljava/util/concurrent/atomic/AtomicLong;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    invoke-static {v2, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 15
    .line 16
    .line 17
    iget-object v2, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 18
    .line 19
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 20
    .line 21
    const-wide/32 v4, 0xea60

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/PriorityBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lb/b/a/a/i/a/m/a;

    .line 29
    .line 30
    iget-object v3, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    new-instance v4, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v5, "poll size:"

    .line 42
    .line 43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    invoke-static {v4}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    instance-of v4, v2, Lb/b/a/a/i/a/m/b;

    .line 57
    .line 58
    if-eqz v4, :cond_0

    .line 59
    .line 60
    invoke-direct {p0, v2, v3}, Lb/b/a/a/i/a/k/g/c;->OO0o〇〇〇〇0(Lb/b/a/a/i/a/m/a;I)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    if-nez v2, :cond_3

    .line 65
    .line 66
    iget-object v2, p0, Lb/b/a/a/i/a/k/g/c;->oOo〇8o008:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 67
    .line 68
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    invoke-virtual {v1}, Lb/b/a/a/i/a/k/e/a;->o〇8oOO88()Ljava/util/concurrent/atomic/AtomicLong;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-static {v1, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 77
    .line 78
    .line 79
    invoke-direct {p0, v2}, Lb/b/a/a/i/a/k/g/c;->〇00(I)Z

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    if-eqz v1, :cond_1

    .line 84
    .line 85
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇O888o0o()V

    .line 86
    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_1
    const/4 v1, 0x4

    .line 90
    if-lt v2, v1, :cond_2

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v3, "timeoutCount:"

    .line 99
    .line 100
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    invoke-static {v1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    iput v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 114
    .line 115
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, v1}, Lb/b/a/a/i/a/k/g/c;->oo88o8O(Lb/b/a/a/i/a/m/a;)V

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_3
    invoke-direct {p0, v2}, Lb/b/a/a/i/a/k/g/c;->〇80〇808〇O(Lb/b/a/a/i/a/m/a;)V

    .line 121
    .line 122
    .line 123
    invoke-direct {p0, v2}, Lb/b/a/a/i/a/k/g/c;->oo88o8O(Lb/b/a/a/i/a/m/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :catchall_0
    move-exception v1

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v3, "run exception:"

    .line 134
    .line 135
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-static {v1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    sget-object v1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 153
    .line 154
    invoke-virtual {v1}, Lb/b/a/a/i/a/k/e/a;->〇80〇808〇O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-static {v1, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 159
    .line 160
    .line 161
    goto/16 :goto_0

    .line 162
    .line 163
    :cond_4
    :goto_1
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method private 〇00(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-lt p1, v0, :cond_0

    .line 3
    .line 4
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-nez p1, :cond_0

    .line 11
    .line 12
    sget-object p1, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 13
    .line 14
    iget-boolean v0, p1, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    iget-boolean p1, p1, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 19
    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p1, 0x0

    .line 25
    :goto_0
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private 〇00〇8()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "th dead"

    .line 8
    .line 9
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 13
    .line 14
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/d;->oO80()Z

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0}, Lb/b/a/a/i/a/k/g/c;->〇0000OOO()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    const-string v0, "monitor  mLogThread "

    .line 25
    .line 26
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/4 v0, 0x6

    .line 30
    invoke-virtual {p0, v0}, Lb/b/a/a/i/a/k/g/c;->O8ooOoo〇(I)V

    .line 31
    .line 32
    .line 33
    :cond_1
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method static synthetic 〇080(Lb/b/a/a/i/a/k/g/c;)I
    .locals 0

    .line 1
    iget p0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇0〇O0088o(ZLb/b/a/a/i/a/k/g/b;Ljava/util/List;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lb/b/a/a/i/a/k/g/b;",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;J)V"
        }
    .end annotation

    .line 1
    if-nez p1, :cond_8

    .line 2
    .line 3
    if-eqz p2, :cond_8

    .line 4
    .line 5
    iget p1, p2, Lb/b/a/a/i/a/k/g/b;->〇o00〇〇Oo:I

    .line 6
    .line 7
    iget-boolean v0, p2, Lb/b/a/a/i/a/k/g/b;->O8:Z

    .line 8
    .line 9
    const/4 v1, -0x2

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const/4 p1, -0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    if-gez p1, :cond_1

    .line 15
    .line 16
    const/4 p1, -0x2

    .line 17
    :cond_1
    :goto_0
    const/16 v0, 0x1fe

    .line 18
    .line 19
    if-eq p1, v0, :cond_2

    .line 20
    .line 21
    const/16 v0, 0x1ff

    .line 22
    .line 23
    if-ne p1, v0, :cond_3

    .line 24
    .line 25
    :cond_2
    const/4 p1, -0x2

    .line 26
    :cond_3
    iget-boolean p2, p2, Lb/b/a/a/i/a/k/g/b;->〇080:Z

    .line 27
    .line 28
    if-nez p2, :cond_5

    .line 29
    .line 30
    const/16 p2, 0x1f4

    .line 31
    .line 32
    if-lt p1, p2, :cond_4

    .line 33
    .line 34
    const/16 p2, 0x1fd

    .line 35
    .line 36
    if-lt p1, p2, :cond_6

    .line 37
    .line 38
    :cond_4
    const/16 p2, 0x201

    .line 39
    .line 40
    if-le p1, p2, :cond_5

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_5
    move v1, p1

    .line 44
    :cond_6
    :goto_1
    if-eqz p3, :cond_7

    .line 45
    .line 46
    new-instance p1, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string p2, "preprocessResult code is "

    .line 52
    .line 53
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string p2, " sz:"

    .line 60
    .line 61
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string p2, "  count:"

    .line 72
    .line 73
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    iget-object p2, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    .line 78
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 79
    .line 80
    .line 81
    move-result p2

    .line 82
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    :cond_7
    invoke-direct {p0, v1, p3, p4, p5}, Lb/b/a/a/i/a/k/g/c;->Oo08(ILjava/util/List;J)V

    .line 93
    .line 94
    .line 95
    :cond_8
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method private 〇80〇808〇O(Lb/b/a/a/i/a/m/a;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo〇8o008:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 5
    .line 6
    .line 7
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 8
    .line 9
    iget-boolean v1, v0, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x5

    .line 14
    iput v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-boolean v0, v0, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    const/4 v0, 0x7

    .line 22
    iput v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x4

    .line 26
    iput v0, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 27
    .line 28
    :goto_0
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 29
    .line 30
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->o8oO〇()Ljava/util/concurrent/atomic/AtomicLong;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const/4 v1, 0x1

    .line 35
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO:Lb/b/a/a/i/a/j/d;

    .line 39
    .line 40
    iget v1, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 41
    .line 42
    invoke-interface {v0, p1, v1}, Lb/b/a/a/i/a/j/d;->〇o00〇〇Oo(Lb/b/a/a/i/a/m/a;I)V

    .line 43
    .line 44
    .line 45
    invoke-static {p1}, Lb/b/a/a/i/a/l/a;->〇oOO8O8(Lb/b/a/a/i/a/m/a;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private 〇O00(Ljava/util/List;ZLjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget v2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 6
    .line 7
    invoke-static {p1, v2, p3}, Lb/b/a/a/i/a/l/a;->〇〇888(Ljava/util/List;ILjava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    invoke-virtual {p3}, Lb/b/a/a/i/a/i;->〇O888o0o()Lb/b/a/a/i/a/k/c;

    .line 15
    .line 16
    .line 17
    move-result-object p3

    .line 18
    iput-object p3, p0, Lb/b/a/a/i/a/k/g/c;->〇08O〇00〇o:Lb/b/a/a/i/a/k/c;

    .line 19
    .line 20
    if-eqz p3, :cond_0

    .line 21
    .line 22
    invoke-direct {p0, p1, p2, v0, v1}, Lb/b/a/a/i/a/k/g/c;->o〇O8〇〇o(Ljava/util/List;ZJ)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    invoke-direct {p0, p1, p2, v0, v1}, Lb/b/a/a/i/a/k/g/c;->〇〇808〇(Ljava/util/List;ZJ)V

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method private 〇O888o0o()V
    .locals 2

    .line 1
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 2
    .line 3
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->〇00()Ljava/util/concurrent/atomic/AtomicLong;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    invoke-virtual {p0, v0}, Lb/b/a/a/i/a/k/g/c;->〇〇8O0〇8(Z)V

    .line 13
    .line 14
    .line 15
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 16
    .line 17
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/d;->〇O8o08O()V

    .line 18
    .line 19
    .line 20
    const-string v0, "exit log thread"

    .line 21
    .line 22
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇〇888(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private 〇O8o08O(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0xb

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    new-instance v0, Ljava/util/ArrayList;

    .line 25
    .line 26
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 27
    .line 28
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 34
    .line 35
    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v2, "before_"

    .line 42
    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0, v0, v1, p1}, Lb/b/a/a/i/a/k/g/c;->〇O00(Ljava/util/List;ZLjava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇o00〇〇Oo()V

    .line 58
    .line 59
    .line 60
    new-instance p1, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v1, "applog batch reporting size = "

    .line 66
    .line 67
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    const-string v0, "PADLT"

    .line 82
    .line 83
    invoke-static {v0, p1}, Lb/b/a/a/i/a/l/c;->o〇0(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 88
    .line 89
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .line 91
    .line 92
    const-string v1, "ensureUploadOptBatch empty\uff1a"

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    :goto_0
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method private 〇O〇(Ljava/util/List;ZJI)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;ZJI)V"
        }
    .end annotation

    .line 1
    const/4 p5, 0x0

    .line 2
    const/4 v0, 0x1

    .line 3
    :try_start_0
    invoke-interface {p1, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p5

    .line 7
    check-cast p5, Lb/b/a/a/i/a/m/a;

    .line 8
    .line 9
    sget-object v1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 10
    .line 11
    invoke-virtual {v1}, Lb/b/a/a/i/a/k/e/a;->〇8o8o〇()Ljava/util/concurrent/atomic/AtomicLong;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-static {v1, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 16
    .line 17
    .line 18
    invoke-interface {p5}, Lb/b/a/a/i/a/m/a;->f()B

    .line 19
    .line 20
    .line 21
    move-result p5

    .line 22
    if-nez p5, :cond_1

    .line 23
    .line 24
    invoke-static {}, Lb/b/a/a/i/a/i;->〇0〇O0088o()Lb/b/a/a/i/a/n/a;

    .line 25
    .line 26
    .line 27
    move-result-object p5

    .line 28
    invoke-interface {p5, p1}, Lb/b/a/a/i/a/n/a;->a(Ljava/util/List;)Lb/b/a/a/i/a/k/g/b;

    .line 29
    .line 30
    .line 31
    move-result-object p5

    .line 32
    invoke-direct {p0, p5, p1}, Lb/b/a/a/i/a/k/g/c;->o〇0(Lb/b/a/a/i/a/k/g/b;Ljava/util/List;)V

    .line 33
    .line 34
    .line 35
    if-eqz p5, :cond_0

    .line 36
    .line 37
    iget-object v1, p5, Lb/b/a/a/i/a/k/g/b;->〇o〇:Ljava/lang/String;

    .line 38
    .line 39
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/a;->oO80(Ljava/util/List;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    :goto_0
    move-object v3, p5

    .line 43
    goto :goto_3

    .line 44
    :cond_1
    new-instance p5, Lorg/json/JSONObject;

    .line 45
    .line 46
    invoke-direct {p5}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    .line 48
    .line 49
    :try_start_1
    new-instance v1, Lorg/json/JSONArray;

    .line 50
    .line 51
    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    if-eqz v3, :cond_2

    .line 63
    .line 64
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    check-cast v3, Lb/b/a/a/i/a/m/a;

    .line 69
    .line 70
    invoke-interface {v3}, Lb/b/a/a/i/a/m/a;->b()Lorg/json/JSONObject;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_2
    const-string v2, "stats_list"

    .line 79
    .line 80
    invoke-virtual {p5, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :catch_0
    move-exception v1

    .line 85
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    const-string v3, "json exception:"

    .line 91
    .line 92
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-static {v1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    :goto_2
    invoke-static {}, Lb/b/a/a/i/a/i;->〇0〇O0088o()Lb/b/a/a/i/a/n/a;

    .line 110
    .line 111
    .line 112
    move-result-object v1

    .line 113
    invoke-interface {v1, p5}, Lb/b/a/a/i/a/n/a;->a(Lorg/json/JSONObject;)Lb/b/a/a/i/a/k/g/b;

    .line 114
    .line 115
    .line 116
    move-result-object p5

    .line 117
    goto :goto_0

    .line 118
    :goto_3
    iget-object p5, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 119
    .line 120
    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 121
    .line 122
    .line 123
    move-object v1, p0

    .line 124
    move v2, p2

    .line 125
    move-object v4, p1

    .line 126
    move-wide v5, p3

    .line 127
    invoke-direct/range {v1 .. v6}, Lb/b/a/a/i/a/k/g/c;->〇0〇O0088o(ZLb/b/a/a/i/a/k/g/b;Ljava/util/List;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    .line 129
    .line 130
    goto :goto_4

    .line 131
    :catchall_0
    move-exception p1

    .line 132
    new-instance p2, Ljava/lang/StringBuilder;

    .line 133
    .line 134
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .line 136
    .line 137
    const-string p3, "inner exception:"

    .line 138
    .line 139
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    sget-object p1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 157
    .line 158
    invoke-virtual {p1}, Lb/b/a/a/i/a/k/e/a;->〇80〇808〇O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    invoke-static {p1, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 163
    .line 164
    .line 165
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 166
    .line 167
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 168
    .line 169
    .line 170
    :goto_4
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method private 〇o()V
    .locals 2

    .line 1
    const-string v0, "sendServerBusyOrRoutineErrorRetryMessage"

    .line 2
    .line 3
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇00〇8()V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 10
    .line 11
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->〇〇888()Ljava/util/concurrent/atomic/AtomicLong;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v1}, Lb/b/a/a/i/a/k/g/c;->O8ooOoo〇(I)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private 〇o00〇〇Oo()V
    .locals 10

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0xb

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->O〇8O8〇008()V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0, v1}, Lb/b/a/a/i/a/k/g/c;->〇o〇(I)V

    .line 17
    .line 18
    .line 19
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "afterUpload message:"

    .line 25
    .line 26
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget v2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 42
    .line 43
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->〇〇0o()Ljava/util/concurrent/atomic/AtomicLong;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-static {v2, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 48
    .line 49
    .line 50
    iget v2, p0, Lb/b/a/a/i/a/k/g/c;->O8o08O8O:I

    .line 51
    .line 52
    const/4 v3, 0x2

    .line 53
    if-ne v2, v3, :cond_7

    .line 54
    .line 55
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->o0ooO()Ljava/util/concurrent/atomic/AtomicLong;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-static {v2, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 60
    .line 61
    .line 62
    iget-object v2, p0, Lb/b/a/a/i/a/k/g/c;->〇OOo8〇0:Ljava/lang/Object;

    .line 63
    .line 64
    monitor-enter v2

    .line 65
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 66
    .line 67
    .line 68
    move-result-wide v4

    .line 69
    iget-object v6, p0, Lb/b/a/a/i/a/k/g/c;->〇OOo8〇0:Ljava/lang/Object;

    .line 70
    .line 71
    const-wide/16 v7, 0x1388

    .line 72
    .line 73
    invoke-virtual {v6, v7, v8}, Ljava/lang/Object;->wait(J)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 77
    .line 78
    .line 79
    move-result-wide v6

    .line 80
    sub-long/2addr v6, v4

    .line 81
    new-instance v8, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v9, "afterUpload delta:"

    .line 87
    .line 88
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string v9, " start:"

    .line 95
    .line 96
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string v4, " condition:"

    .line 103
    .line 104
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    sget-object v4, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 108
    .line 109
    iget-boolean v5, v4, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 110
    .line 111
    if-nez v5, :cond_2

    .line 112
    .line 113
    iget-boolean v5, v4, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 114
    .line 115
    if-eqz v5, :cond_1

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_1
    const/4 v5, 0x0

    .line 119
    goto :goto_2

    .line 120
    :cond_2
    :goto_1
    const/4 v5, 0x1

    .line 121
    :goto_2
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v5

    .line 128
    invoke-static {v5}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const-wide v8, 0x12a05f200L

    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    cmp-long v5, v6, v8

    .line 137
    .line 138
    if-gez v5, :cond_6

    .line 139
    .line 140
    sub-long/2addr v8, v6

    .line 141
    const-wide/32 v5, 0x2faf080

    .line 142
    .line 143
    .line 144
    cmp-long v7, v8, v5

    .line 145
    .line 146
    if-gez v7, :cond_3

    .line 147
    .line 148
    goto :goto_4

    .line 149
    :cond_3
    iget-boolean v5, v4, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 150
    .line 151
    if-nez v5, :cond_5

    .line 152
    .line 153
    iget-boolean v4, v4, Lb/b/a/a/i/a/k/d;->〇o〇:Z

    .line 154
    .line 155
    if-eqz v4, :cond_4

    .line 156
    .line 157
    goto :goto_3

    .line 158
    :cond_4
    const-string v4, "afterUpload meet notifyRunOnce again"

    .line 159
    .line 160
    invoke-static {v4}, Lb/b/a/a/i/a/l/c;->〇〇888(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->〇〇〇0〇〇0()Ljava/util/concurrent/atomic/AtomicLong;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {p0, v3}, Lb/b/a/a/i/a/k/g/c;->O8ooOoo〇(I)V

    .line 171
    .line 172
    .line 173
    goto :goto_5

    .line 174
    :cond_5
    :goto_3
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->o800o8O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 179
    .line 180
    .line 181
    const-string v0, "afterUpload wait serverBusy"

    .line 182
    .line 183
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    .line 185
    .line 186
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    return-void

    .line 188
    :cond_6
    :goto_4
    :try_start_2
    const-string v3, "afterUpload wait timeout"

    .line 189
    .line 190
    invoke-static {v3}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->o〇8()Ljava/util/concurrent/atomic/AtomicLong;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    invoke-static {v0, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 198
    .line 199
    .line 200
    :try_start_3
    monitor-exit v2

    .line 201
    return-void

    .line 202
    :catchall_0
    move-exception v0

    .line 203
    goto :goto_6

    .line 204
    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    .line 206
    .line 207
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    .line 209
    .line 210
    const-string v3, "wait exception:"

    .line 211
    .line 212
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v0

    .line 219
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object v0

    .line 226
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    :goto_5
    monitor-exit v2

    .line 230
    goto :goto_7

    .line 231
    :goto_6
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 232
    throw v0

    .line 233
    :cond_7
    :goto_7
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method

.method public static 〇oOO8O8(I)V
    .locals 2

    .line 1
    sput p0, Lb/b/a/a/i/a/k/g/c;->〇〇08O:I

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "config size="

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    const-string v0, "PADLT"

    .line 21
    .line 22
    invoke-static {v0, p0}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method private 〇oo〇(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_9

    .line 6
    .line 7
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-static {p1, v0}, Lb/b/a/a/i/a/l/a;->o〇0(Ljava/util/List;I)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const/4 v1, 0x1

    .line 21
    const-string v2, "PADLT"

    .line 22
    .line 23
    if-gt v0, v1, :cond_8

    .line 24
    .line 25
    invoke-static {}, Lb/b/a/a/i/a/l/a;->OoO8()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    goto/16 :goto_0

    .line 32
    .line 33
    :cond_0
    const/4 v0, 0x0

    .line 34
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, Lb/b/a/a/i/a/m/a;

    .line 39
    .line 40
    if-eqz v0, :cond_7

    .line 41
    .line 42
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->c()B

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-ne v3, v1, :cond_1

    .line 47
    .line 48
    const-string v0, "highPriority"

    .line 49
    .line 50
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const-string p1, "Single high priority \uff08 applog \uff09"

    .line 54
    .line 55
    invoke-static {v2, p1}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_1

    .line 59
    .line 60
    :cond_1
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->f()B

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    const/4 v4, 0x3

    .line 65
    const/4 v5, 0x2

    .line 66
    if-nez v3, :cond_3

    .line 67
    .line 68
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->c()B

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    if-ne v3, v5, :cond_3

    .line 73
    .line 74
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->g()B

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    if-ne v0, v4, :cond_2

    .line 79
    .line 80
    const-string v0, "version_v3"

    .line 81
    .line 82
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_2
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->OO0o〇〇(Ljava/util/List;)V

    .line 87
    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_3
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->f()B

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    if-ne v3, v1, :cond_4

    .line 95
    .line 96
    const-string v0, "Stats batch report \uff08 stats \uff09"

    .line 97
    .line 98
    invoke-static {v2, v0}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    const-string v0, "stats"

    .line 102
    .line 103
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_4
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->f()B

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    if-ne v1, v4, :cond_5

    .line 112
    .line 113
    const-string v0, "adType_v3"

    .line 114
    .line 115
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_5
    invoke-interface {v0}, Lb/b/a/a/i/a/m/a;->f()B

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-ne v0, v5, :cond_6

    .line 124
    .line 125
    const-string v0, "Single high priority \uff08 stats \uff09"

    .line 126
    .line 127
    invoke-static {v2, v0}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    const-string v0, "other"

    .line 131
    .line 132
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_6
    const-string p1, "upload adLogEvent adType error"

    .line 137
    .line 138
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    goto :goto_1

    .line 142
    :cond_7
    const-string p1, "upload adLogEvent is null"

    .line 143
    .line 144
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    goto :goto_1

    .line 148
    :cond_8
    :goto_0
    const-string v0, "Batch report\uff08 local or stats \uff09"

    .line 149
    .line 150
    invoke-static {v2, v0}, Lb/b/a/a/i/a/l/c;->O8(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    const-string v0, "batchRead"

    .line 154
    .line 155
    invoke-direct {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->Oooo8o0〇(Ljava/util/List;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_9
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->O〇8O8〇008()V

    .line 160
    .line 161
    .line 162
    const-string p1, "upload list is empty"

    .line 163
    .line 164
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    :goto_1
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method private 〇o〇(I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lb/b/a/a/i/a/k/g/c;->〇0000OOO()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-nez v0, :cond_4

    .line 7
    .line 8
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    sget-object v0, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 14
    .line 15
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->Oooo8o0〇()Ljava/util/concurrent/atomic/AtomicLong;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-static {v2, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 23
    .line 24
    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_5

    .line 29
    .line 30
    if-ne p1, v1, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->o8()Ljava/util/concurrent/atomic/AtomicLong;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 v2, 0x2

    .line 41
    if-ne p1, v2, :cond_2

    .line 42
    .line 43
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->OOO()Ljava/util/concurrent/atomic/AtomicLong;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    const/4 v2, 0x3

    .line 52
    if-ne p1, v2, :cond_3

    .line 53
    .line 54
    invoke-virtual {v0}, Lb/b/a/a/i/a/k/e/a;->oO80()Ljava/util/concurrent/atomic/AtomicLong;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 59
    .line 60
    .line 61
    :cond_3
    :goto_0
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 62
    .line 63
    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_4
    sget-object p1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 68
    .line 69
    invoke-virtual {p1}, Lb/b/a/a/i/a/k/e/a;->ooo〇8oO()Ljava/util/concurrent/atomic/AtomicLong;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    invoke-static {p1, v1}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 74
    .line 75
    .line 76
    :cond_5
    :goto_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method private 〇〇808〇(Ljava/util/List;ZJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb/b/a/a/i/a/m/a;",
            ">;ZJ)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lb/b/a/a/i/a/i;->〇080()Lb/b/a/a/i/a/f;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-interface {v0}, Lb/b/a/a/i/a/f;->b()Ljava/util/concurrent/Executor;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lb/b/a/a/i/a/m/a;

    .line 21
    .line 22
    invoke-interface {v2}, Lb/b/a/a/i/a/m/a;->c()B

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    const/4 v3, 0x1

    .line 27
    if-ne v2, v3, :cond_0

    .line 28
    .line 29
    invoke-interface {v0}, Lb/b/a/a/i/a/f;->a()Ljava/util/concurrent/Executor;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    :cond_0
    if-nez v1, :cond_1

    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->oOo0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 37
    .line 38
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 39
    .line 40
    .line 41
    new-instance v0, Lb/b/a/a/i/a/k/g/c$a;

    .line 42
    .line 43
    const-string v4, "csj_log_upload"

    .line 44
    .line 45
    move-object v2, v0

    .line 46
    move-object v3, p0

    .line 47
    move-object v5, p1

    .line 48
    move v6, p2

    .line 49
    move-wide v7, p3

    .line 50
    invoke-direct/range {v2 .. v8}, Lb/b/a/a/i/a/k/g/c$a;-><init>(Lb/b/a/a/i/a/k/g/c;Ljava/lang/String;Ljava/util/List;ZJ)V

    .line 51
    .line 52
    .line 53
    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method static synthetic 〇〇888(Lb/b/a/a/i/a/k/g/c;Ljava/util/List;ZJI)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lb/b/a/a/i/a/k/g/c;->〇O〇(Ljava/util/List;ZJI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
.end method


# virtual methods
.method public O8(IJ)V
    .locals 5

    .line 1
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string p1, "mHandler == null"

    .line 6
    .line 7
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput p1, v0, Landroid/os/Message;->what:I

    .line 16
    .line 17
    const/4 v1, 0x2

    .line 18
    const-string v2, "sendMonitorMessage:"

    .line 19
    .line 20
    if-ne p1, v1, :cond_1

    .line 21
    .line 22
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->〇8〇oO〇〇8o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    add-int/lit8 v3, v1, -0x1

    .line 29
    .line 30
    rem-int/lit8 v3, v3, 0x4

    .line 31
    .line 32
    add-int/lit8 v3, v3, 0x1

    .line 33
    .line 34
    int-to-long v3, v3

    .line 35
    mul-long v3, v3, p2

    .line 36
    .line 37
    new-instance p2, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string p1, "  busy:"

    .line 49
    .line 50
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string p1, "  l:"

    .line 57
    .line 58
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {p2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 72
    .line 73
    invoke-virtual {p1, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    const/4 v1, 0x3

    .line 78
    if-ne p1, v1, :cond_2

    .line 79
    .line 80
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->ooo0〇〇O:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 81
    .line 82
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    new-instance v3, Ljava/lang/StringBuilder;

    .line 87
    .line 88
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    const-string p1, "  error:"

    .line 98
    .line 99
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 113
    .line 114
    add-int/lit8 v1, v1, -0x1

    .line 115
    .line 116
    rem-int/lit8 v1, v1, 0x4

    .line 117
    .line 118
    add-int/lit8 v1, v1, 0x1

    .line 119
    .line 120
    int-to-long v1, v1

    .line 121
    mul-long v1, v1, p2

    .line 122
    .line 123
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_2
    const-string p1, "sendMonitorMessage error state"

    .line 128
    .line 129
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    :goto_0
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method public O8ooOoo〇(I)V
    .locals 3

    .line 1
    :try_start_0
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 2
    .line 3
    iget-boolean v0, v0, Lb/b/a/a/i/a/k/d;->〇o00〇〇Oo:Z

    .line 4
    .line 5
    invoke-virtual {p0, p1, v0}, Lb/b/a/a/i/a/k/g/c;->OoO8(IZ)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "notify flush : "

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v2, " "

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {v1}, Lb/b/a/a/i/a/l/c;->〇〇888(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const/4 v1, 0x6

    .line 38
    if-eq p1, v1, :cond_0

    .line 39
    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    :cond_0
    new-instance v0, Lb/b/a/a/i/a/m/b;

    .line 43
    .line 44
    invoke-direct {v0}, Lb/b/a/a/i/a/m/b;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, p1}, Lb/b/a/a/i/a/m/b;->〇o〇(I)V

    .line 48
    .line 49
    .line 50
    iget-object p1, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    const/4 p1, 0x3

    .line 56
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->〇o〇(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :catchall_0
    move-exception p1

    .line 61
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    :cond_1
    :goto_0
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public OoO8(IZ)Z
    .locals 2

    .line 1
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lb/b/a/a/i/a/i;->〇080()Lb/b/a/a/i/a/f;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lb/b/a/a/i/a/i;->〇〇8O0〇8()Lb/b/a/a/i/a/i;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lb/b/a/a/i/a/i;->OO0o〇〇()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-interface {v0, v1}, Lb/b/a/a/i/a/f;->a(Landroid/content/Context;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO:Lb/b/a/a/i/a/j/d;

    .line 27
    .line 28
    invoke-interface {v0, p1, p2}, Lb/b/a/a/i/a/j/d;->a(IZ)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    return p1

    .line 33
    :cond_1
    :goto_0
    const-string p1, "AdThread NET IS NOT AVAILABLE!!!"

    .line 34
    .line 35
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x0

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .line 1
    iget p1, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eq p1, v0, :cond_2

    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    if-eq p1, v1, :cond_1

    .line 8
    .line 9
    const/4 v1, 0x3

    .line 10
    if-eq p1, v1, :cond_1

    .line 11
    .line 12
    const/16 v1, 0xb

    .line 13
    .line 14
    if-eq p1, v1, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    :try_start_0
    const-string p1, "opt upload"

    .line 18
    .line 19
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    new-instance p1, Ljava/util/ArrayList;

    .line 23
    .line 24
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 25
    .line 26
    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->o8〇OO0〇0o:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 32
    .line 33
    .line 34
    const-string v1, "timeout_dispatch"

    .line 35
    .line 36
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, v2, v1}, Lb/b/a/a/i/a/k/g/c;->〇O00(Ljava/util/List;ZLjava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇o00〇〇Oo()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const-string p1, "-----------------server busy handleMessage---------------- "

    .line 45
    .line 46
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->〇o()V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_2
    const-string p1, "HANDLER_MESSAGE_INIT"

    .line 54
    .line 55
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    sget-object p1, Lb/b/a/a/i/a/k/d;->oO80:Lb/b/a/a/i/a/k/e/a;

    .line 59
    .line 60
    invoke-virtual {p1}, Lb/b/a/a/i/a/k/e/a;->〇O〇80o08O()Ljava/util/concurrent/atomic/AtomicLong;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-static {p1, v0}, Lb/b/a/a/i/a/l/b;->〇080(Ljava/util/concurrent/atomic/AtomicLong;I)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, v0}, Lb/b/a/a/i/a/k/g/c;->〇〇8O0〇8(Z)V

    .line 68
    .line 69
    .line 70
    invoke-direct {p0}, Lb/b/a/a/i/a/k/g/c;->o〇〇0〇()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :catchall_0
    move-exception p1

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    .line 76
    .line 77
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .line 79
    .line 80
    const-string v2, "error:"

    .line 81
    .line 82
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    :goto_0
    return v0
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
.end method

.method protected onLooperPrepared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/os/HandlerThread;->onLooperPrepared()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/os/Handler;

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 14
    .line 15
    sget-object v0, Lb/b/a/a/i/a/k/d;->〇〇888:Lb/b/a/a/i/a/k/d;

    .line 16
    .line 17
    iget-object v1, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lb/b/a/a/i/a/k/d;->〇o〇(Landroid/os/Handler;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 26
    .line 27
    .line 28
    const-string v0, "onLooperPrepared"

    .line 29
    .line 30
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public 〇0000OOO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8o8o〇(Lb/b/a/a/i/a/m/a;Z)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "ignore result : "

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, ":"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget-boolean v1, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v1, " adType: "

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-interface {p1}, Lb/b/a/a/i/a/m/a;->f()B

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v0}, Lb/b/a/a/i/a/l/c;->〇080(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    if-eqz p2, :cond_2

    .line 47
    .line 48
    iget-object p2, p0, Lb/b/a/a/i/a/k/g/c;->OO〇00〇8oO:Landroid/os/Handler;

    .line 49
    .line 50
    if-eqz p2, :cond_1

    .line 51
    .line 52
    new-instance p2, Ljava/util/ArrayList;

    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    const-string p1, "ignore_result_dispatch"

    .line 62
    .line 63
    invoke-direct {p0, p2, v0, p1}, Lb/b/a/a/i/a/k/g/c;->〇O00(Ljava/util/List;ZLjava/lang/String;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    const-string p1, "handler is null\uff0cignore is true"

    .line 68
    .line 69
    invoke-static {p1}, Lb/b/a/a/i/a/l/c;->〇o〇(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_2
    iget-object p2, p0, Lb/b/a/a/i/a/k/g/c;->o〇00O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 74
    .line 75
    invoke-virtual {p2, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    const/4 p1, 0x2

    .line 79
    invoke-direct {p0, p1}, Lb/b/a/a/i/a/k/g/c;->〇o〇(I)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method public 〇〇8O0〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lb/b/a/a/i/a/k/g/c;->o0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.class public Lb/b/a/a/j/a;
.super Ljava/lang/Object;
.source "NetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/b/a/a/j/a$b;
    }
.end annotation


# instance fields
.field private 〇080:Lb/b/a/a/f/a/j;

.field private 〇o00〇〇Oo:Lb/b/a/a/j/e/f;

.field private 〇o〇:I


# direct methods
.method private constructor <init>(Lb/b/a/a/j/a$b;)V
    .locals 4

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lb/b/a/a/f/a/j$a;

    invoke-direct {v0}, Lb/b/a/a/f/a/j$a;-><init>()V

    iget v1, p1, Lb/b/a/a/j/a$b;->〇080:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 4
    invoke-virtual {v0, v1, v2, v3}, Lb/b/a/a/f/a/j$a;->〇080(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    move-result-object v0

    iget v1, p1, Lb/b/a/a/j/a$b;->〇o〇:I

    int-to-long v1, v1

    .line 5
    invoke-virtual {v0, v1, v2, v3}, Lb/b/a/a/f/a/j$a;->Oo08(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    move-result-object v0

    iget v1, p1, Lb/b/a/a/j/a$b;->〇o00〇〇Oo:I

    int-to-long v1, v1

    .line 6
    invoke-virtual {v0, v1, v2, v3}, Lb/b/a/a/f/a/j$a;->O8(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/f/a/j$a;

    move-result-object v0

    .line 7
    iget-boolean v1, p1, Lb/b/a/a/j/a$b;->O8:Z

    if-eqz v1, :cond_0

    .line 8
    new-instance v1, Lb/b/a/a/j/e/f;

    invoke-direct {v1}, Lb/b/a/a/j/e/f;-><init>()V

    iput-object v1, p0, Lb/b/a/a/j/a;->〇o00〇〇Oo:Lb/b/a/a/j/e/f;

    .line 9
    invoke-virtual {v0, v1}, Lb/b/a/a/f/a/j$a;->〇o00〇〇Oo(Lb/b/a/a/f/a/h;)Lb/b/a/a/f/a/j$a;

    .line 10
    :cond_0
    iget-object v1, p1, Lb/b/a/a/j/a$b;->Oo08:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 11
    iget-object p1, p1, Lb/b/a/a/j/a$b;->Oo08:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/b/a/a/f/a/h;

    .line 12
    invoke-virtual {v0, v1}, Lb/b/a/a/f/a/j$a;->〇o00〇〇Oo(Lb/b/a/a/f/a/h;)Lb/b/a/a/f/a/j$a;

    goto :goto_0

    .line 13
    :cond_1
    invoke-virtual {v0}, Lb/b/a/a/f/a/j$a;->〇o〇()Lb/b/a/a/f/a/j;

    move-result-object p1

    iput-object p1, p0, Lb/b/a/a/j/a;->〇080:Lb/b/a/a/f/a/j;

    return-void
.end method

.method synthetic constructor <init>(Lb/b/a/a/j/a$b;Lb/b/a/a/j/a$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lb/b/a/a/j/a;-><init>(Lb/b/a/a/j/a$b;)V

    return-void
.end method

.method private static O8(Landroid/content/Context;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/r;->a(Landroid/content/Context;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    if-eqz p0, :cond_1

    .line 6
    .line 7
    const-string v0, ":push"

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, ":pushservice"

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 18
    .line 19
    .line 20
    move-result p0

    .line 21
    if-eqz p0, :cond_1

    .line 22
    .line 23
    :cond_0
    const/4 p0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 p0, 0x0

    .line 26
    :goto_0
    return p0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public static oO80()V
    .locals 1

    .line 1
    sget-object v0, Lb/b/a/a/j/f/b$b;->a:Lb/b/a/a/j/f/b$b;

    .line 2
    .line 3
    invoke-static {v0}, Lb/b/a/a/j/f/b;->〇080(Lb/b/a/a/j/f/b$b;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public Oo08()Lb/b/a/a/j/d/b;
    .locals 2

    .line 1
    new-instance v0, Lb/b/a/a/j/d/b;

    .line 2
    .line 3
    iget-object v1, p0, Lb/b/a/a/j/a;->〇080:Lb/b/a/a/f/a/j;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lb/b/a/a/j/d/b;-><init>(Lb/b/a/a/f/a/j;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()Lb/b/a/a/f/a/j;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/j/a;->〇080:Lb/b/a/a/f/a/j;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080()Lb/b/a/a/j/d/a;
    .locals 2

    .line 1
    new-instance v0, Lb/b/a/a/j/d/a;

    .line 2
    .line 3
    iget-object v1, p0, Lb/b/a/a/j/a;->〇080:Lb/b/a/a/f/a/j;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lb/b/a/a/j/d/a;-><init>(Lb/b/a/a/f/a/j;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo(Landroid/content/Context;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lb/b/a/a/j/e/a;->〇80〇808〇O(Z)V

    .line 3
    .line 4
    .line 5
    invoke-static {p1}, Lb/b/a/a/j/a;->O8(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/r;->c(Landroid/content/Context;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    if-eqz p2, :cond_1

    .line 18
    .line 19
    :cond_0
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    iget v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 24
    .line 25
    invoke-virtual {p2, v0, p1}, Lb/b/a/a/j/e/g;->〇080(ILandroid/content/Context;)Lb/b/a/a/j/e/a;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-virtual {p2}, Lb/b/a/a/j/e/a;->〇oo〇()V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    iget v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 37
    .line 38
    invoke-virtual {p2, v0, p1}, Lb/b/a/a/j/e/g;->〇080(ILandroid/content/Context;)Lb/b/a/a/j/e/a;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    invoke-virtual {p2}, Lb/b/a/a/j/e/a;->〇00()V

    .line 43
    .line 44
    .line 45
    :cond_1
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/r;->c(Landroid/content/Context;)Z

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    if-nez p2, :cond_2

    .line 50
    .line 51
    return-void

    .line 52
    :cond_2
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    iget v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 57
    .line 58
    invoke-virtual {p2, v0, p1}, Lb/b/a/a/j/e/g;->〇080(ILandroid/content/Context;)Lb/b/a/a/j/e/a;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    invoke-virtual {p2}, Lb/b/a/a/j/e/a;->〇oo〇()V

    .line 63
    .line 64
    .line 65
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    iget v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 70
    .line 71
    invoke-virtual {p2, v0, p1}, Lb/b/a/a/j/e/g;->〇080(ILandroid/content/Context;)Lb/b/a/a/j/e/a;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lb/b/a/a/j/e/a;->〇00()V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
.end method

.method public 〇o〇(Landroid/content/Context;ZLb/b/a/a/j/e/b;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    if-eqz p3, :cond_1

    .line 4
    .line 5
    invoke-interface {p3}, Lb/b/a/a/j/e/b;->e()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 10
    .line 11
    iget-object v1, p0, Lb/b/a/a/j/a;->〇o00〇〇Oo:Lb/b/a/a/j/e/f;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Lb/b/a/a/j/e/f;->〇o00〇〇Oo(I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget v1, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lb/b/a/a/j/e/g;->〇o00〇〇Oo(I)Lb/b/a/a/j/e/e;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0, p2}, Lb/b/a/a/j/e/e;->〇〇808〇(Z)V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    iget v0, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 36
    .line 37
    invoke-virtual {p2, v0}, Lb/b/a/a/j/e/g;->〇o00〇〇Oo(I)Lb/b/a/a/j/e/e;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {p2, p3}, Lb/b/a/a/j/e/e;->〇〇888(Lb/b/a/a/j/e/b;)V

    .line 42
    .line 43
    .line 44
    invoke-static {}, Lb/b/a/a/j/e/g;->〇o〇()Lb/b/a/a/j/e/g;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    iget p3, p0, Lb/b/a/a/j/a;->〇o〇:I

    .line 49
    .line 50
    invoke-virtual {p2, p3}, Lb/b/a/a/j/e/g;->〇o00〇〇Oo(I)Lb/b/a/a/j/e/e;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/r;->c(Landroid/content/Context;)Z

    .line 55
    .line 56
    .line 57
    move-result p3

    .line 58
    invoke-virtual {p2, p1, p3}, Lb/b/a/a/j/e/e;->〇o〇(Landroid/content/Context;Z)V

    .line 59
    .line 60
    .line 61
    return-void

    .line 62
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 63
    .line 64
    const-string p2, "tryInitAdTTNet ITTAdNetDepend is null"

    .line 65
    .line 66
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    throw p1

    .line 70
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 71
    .line 72
    const-string p2, "tryInitAdTTNet context is null"

    .line 73
    .line 74
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    throw p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method public 〇〇888()Lb/b/a/a/j/d/d;
    .locals 2

    .line 1
    new-instance v0, Lb/b/a/a/j/d/d;

    .line 2
    .line 3
    iget-object v1, p0, Lb/b/a/a/j/a;->〇080:Lb/b/a/a/f/a/j;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lb/b/a/a/j/d/d;-><init>(Lb/b/a/a/f/a/j;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

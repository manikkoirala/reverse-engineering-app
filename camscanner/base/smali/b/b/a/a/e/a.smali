.class public abstract Lb/b/a/a/e/a;
.super Ljava/lang/Object;
.source "AbstractBridge.java"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lb/b/a/a/e/m;

.field protected c:Landroid/os/Handler;

.field protected d:Ljava/lang/String;

.field protected volatile e:Z

.field f:Lb/b/a/a/e/g;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lb/b/a/a/e/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/os/Handler;

    .line 5
    .line 6
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lb/b/a/a/e/a;->c:Landroid/os/Handler;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lb/b/a/a/e/a;->e:Z

    .line 17
    .line 18
    new-instance v0, Ljava/util/HashMap;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lb/b/a/a/e/a;->g:Ljava/util/Map;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private a(Ljava/lang/String;)Lb/b/a/a/e/g;
    .locals 1

    .line 62
    iget-object v0, p0, Lb/b/a/a/e/a;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lb/b/a/a/e/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb/b/a/a/e/g;

    goto :goto_1

    .line 64
    :cond_1
    :goto_0
    iget-object p1, p0, Lb/b/a/a/e/a;->f:Lb/b/a/a/e/g;

    :goto_1
    return-object p1
.end method

.method static synthetic a(Lb/b/a/a/e/a;Lorg/json/JSONObject;)Lb/b/a/a/e/q;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lb/b/a/a/e/a;->a(Lorg/json/JSONObject;)Lb/b/a/a/e/q;

    move-result-object p0

    return-object p0
.end method

.method private a(Lorg/json/JSONObject;)Lb/b/a/a/e/q;
    .locals 7

    const-string v0, "params"

    .line 36
    iget-boolean v1, p0, Lb/b/a/a/e/a;->e:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    const-string v1, "__callback_id"

    .line 37
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "func"

    .line 38
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 39
    invoke-virtual {p0}, Lb/b/a/a/e/a;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    return-object v2

    :cond_1
    :try_start_0
    const-string v2, "__msg_type"

    .line 40
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :try_start_1
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 42
    instance-of v5, v4, Lorg/json/JSONObject;

    if-eqz v5, :cond_2

    .line 43
    check-cast v4, Lorg/json/JSONObject;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_2
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 45
    check-cast v4, Ljava/lang/String;

    move-object v0, v4

    goto :goto_0

    .line 46
    :cond_3
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_0

    .line 47
    :catchall_0
    :try_start_2
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v4, "JSSDK"

    .line 48
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "namespace"

    .line 49
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "__iframe_url"

    .line 50
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-static {}, Lb/b/a/a/e/q;->〇080()Lb/b/a/a/e/q$b;

    move-result-object v6

    .line 52
    invoke-virtual {v6, v4}, Lb/b/a/a/e/q$b;->Oooo8o0〇(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v4

    .line 53
    invoke-virtual {v4, v2}, Lb/b/a/a/e/q$b;->〇O8o08O(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v2

    .line 54
    invoke-virtual {v2, v3}, Lb/b/a/a/e/q$b;->o〇0(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v2

    .line 55
    invoke-virtual {v2, v0}, Lb/b/a/a/e/q$b;->OO0o〇〇〇〇0(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v1}, Lb/b/a/a/e/q$b;->〇080(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v5}, Lb/b/a/a/e/q$b;->oO80(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p1}, Lb/b/a/a/e/q$b;->O8(Ljava/lang/String;)Lb/b/a/a/e/q$b;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lb/b/a/a/e/q$b;->〇o00〇〇Oo()Lb/b/a/a/e/q;

    move-result-object p1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string v0, "Failed to create call."

    .line 60
    invoke-static {v0, p1}, Lb/b/a/a/e/i;->〇o〇(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, -0x1

    .line 61
    invoke-static {v1, p1}, Lb/b/a/a/e/q;->〇o00〇〇Oo(Ljava/lang/String;I)Lb/b/a/a/e/q;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected abstract a(Lb/b/a/a/e/j;)Landroid/content/Context;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method final a(Lb/b/a/a/e/j;Lb/b/a/a/e/v;)V
    .locals 1

    .line 17
    invoke-virtual {p0, p1}, Lb/b/a/a/e/a;->a(Lb/b/a/a/e/j;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/e/a;->a:Landroid/content/Context;

    .line 18
    iget-object v0, p1, Lb/b/a/a/e/j;->O8:Lb/b/a/a/e/h;

    .line 19
    new-instance v0, Lb/b/a/a/e/g;

    invoke-direct {v0, p1, p0, p2}, Lb/b/a/a/e/g;-><init>(Lb/b/a/a/e/j;Lb/b/a/a/e/a;Lb/b/a/a/e/v;)V

    iput-object v0, p0, Lb/b/a/a/e/a;->f:Lb/b/a/a/e/g;

    .line 20
    iget-object p2, p1, Lb/b/a/a/e/j;->OO0o〇〇〇〇0:Ljava/lang/String;

    iput-object p2, p0, Lb/b/a/a/e/a;->d:Ljava/lang/String;

    .line 21
    invoke-virtual {p0, p1}, Lb/b/a/a/e/a;->b(Lb/b/a/a/e/j;)V

    return-void
.end method

.method protected final a(Lb/b/a/a/e/q;)V
    .locals 3
    .annotation build Lcom/bytedance/component/sdk/annotation/MainThread;
    .end annotation

    .line 2
    iget-boolean v0, p0, Lb/b/a/a/e/a;->e:Z

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0}, Lb/b/a/a/e/a;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 4
    :cond_1
    iget-object v1, p1, Lb/b/a/a/e/q;->〇〇888:Ljava/lang/String;

    invoke-direct {p0, v1}, Lb/b/a/a/e/a;->a(Ljava/lang/String;)Lb/b/a/a/e/g;

    move-result-object v1

    if-nez v1, :cond_2

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received call with unknown namespace, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb/b/a/a/e/i;->Oo08(Ljava/lang/String;)V

    .line 6
    new-instance v0, Lb/b/a/a/e/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Namespace "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lb/b/a/a/e/q;->〇〇888:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " unknown."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x4

    invoke-direct {v0, v2, v1}, Lb/b/a/a/e/s;-><init>(ILjava/lang/String;)V

    invoke-static {v0}, Lb/b/a/a/e/y;->〇o〇(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lb/b/a/a/e/a;->a(Ljava/lang/String;Lb/b/a/a/e/q;)V

    return-void

    .line 7
    :cond_2
    new-instance v2, Lb/b/a/a/e/f;

    invoke-direct {v2}, Lb/b/a/a/e/f;-><init>()V

    .line 8
    iput-object v0, v2, Lb/b/a/a/e/f;->〇o00〇〇Oo:Ljava/lang/String;

    .line 9
    iget-object v0, p0, Lb/b/a/a/e/a;->a:Landroid/content/Context;

    iput-object v0, v2, Lb/b/a/a/e/f;->〇080:Landroid/content/Context;

    .line 10
    :try_start_0
    invoke-virtual {v1, p1, v2}, Lb/b/a/a/e/g;->Oo08(Lb/b/a/a/e/q;Lb/b/a/a/e/f;)Lb/b/a/a/e/g$c;

    move-result-object v0

    if-nez v0, :cond_3

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received call but not registered, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb/b/a/a/e/i;->Oo08(Ljava/lang/String;)V

    .line 12
    new-instance v0, Lb/b/a/a/e/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Function "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lb/b/a/a/e/q;->O8:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is not registered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x2

    invoke-direct {v0, v2, v1}, Lb/b/a/a/e/s;-><init>(ILjava/lang/String;)V

    invoke-static {v0}, Lb/b/a/a/e/y;->〇o〇(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lb/b/a/a/e/a;->a(Ljava/lang/String;Lb/b/a/a/e/q;)V

    return-void

    .line 13
    :cond_3
    iget-boolean v1, v0, Lb/b/a/a/e/g$c;->〇080:Z

    if-eqz v1, :cond_4

    .line 14
    iget-object v0, v0, Lb/b/a/a/e/g$c;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lb/b/a/a/e/a;->a(Ljava/lang/String;Lb/b/a/a/e/q;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call finished with error, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lb/b/a/a/e/i;->o〇0(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    invoke-static {v0}, Lb/b/a/a/e/y;->〇o〇(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lb/b/a/a/e/a;->a(Ljava/lang/String;Lb/b/a/a/e/q;)V

    :cond_4
    :goto_0
    return-void
.end method

.method final a(Ljava/lang/String;Lb/b/a/a/e/q;)V
    .locals 3

    .line 22
    iget-boolean v0, p0, Lb/b/a/a/e/a;->e:Z

    if-eqz v0, :cond_0

    return-void

    .line 23
    :cond_0
    iget-object v0, p2, Lb/b/a/a/e/q;->o〇0:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "By passing js callback due to empty callback: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lb/b/a/a/e/i;->〇o00〇〇Oo(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v0, "{"

    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "}"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 26
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal callback data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lb/b/a/a/e/i;->〇080(Ljava/lang/RuntimeException;)V

    .line 27
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invoking js callback: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lb/b/a/a/e/q;->o〇0:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb/b/a/a/e/i;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 28
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 29
    :catch_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 30
    :goto_0
    invoke-static {}, Lb/b/a/a/e/p;->〇080()Lb/b/a/a/e/p;

    move-result-object p1

    const-string v1, "__msg_type"

    const-string v2, "callback"

    .line 31
    invoke-virtual {p1, v1, v2}, Lb/b/a/a/e/p;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Object;)Lb/b/a/a/e/p;

    move-result-object p1

    iget-object v1, p2, Lb/b/a/a/e/q;->o〇0:Ljava/lang/String;

    const-string v2, "__callback_id"

    .line 32
    invoke-virtual {p1, v2, v1}, Lb/b/a/a/e/p;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Object;)Lb/b/a/a/e/p;

    move-result-object p1

    const-string v1, "__params"

    .line 33
    invoke-virtual {p1, v1, v0}, Lb/b/a/a/e/p;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Object;)Lb/b/a/a/e/p;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lb/b/a/a/e/p;->〇o〇()Ljava/lang/String;

    move-result-object p1

    .line 35
    invoke-virtual {p0, p1, p2}, Lb/b/a/a/e/a;->b(Ljava/lang/String;Lb/b/a/a/e/q;)V

    return-void
.end method

.method protected b()V
    .locals 2

    .line 2
    iget-object v0, p0, Lb/b/a/a/e/a;->f:Lb/b/a/a/e/g;

    invoke-virtual {v0}, Lb/b/a/a/e/g;->〇〇888()V

    .line 3
    iget-object v0, p0, Lb/b/a/a/e/a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb/b/a/a/e/g;

    .line 4
    invoke-virtual {v1}, Lb/b/a/a/e/g;->〇〇888()V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lb/b/a/a/e/a;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lb/b/a/a/e/a;->e:Z

    return-void
.end method

.method protected abstract b(Lb/b/a/a/e/j;)V
.end method

.method protected abstract b(Ljava/lang/String;)V
    .annotation build Lcom/bytedance/component/sdk/annotation/AnyThread;
    .end annotation
.end method

.method protected b(Ljava/lang/String;Lb/b/a/a/e/q;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lb/b/a/a/e/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected invokeMethod(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/e/a;->e:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "Received call: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lb/b/a/a/e/i;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lb/b/a/a/e/a;->c:Landroid/os/Handler;

    .line 27
    .line 28
    new-instance v1, Lb/b/a/a/e/a$a;

    .line 29
    .line 30
    invoke-direct {v1, p0, p1}, Lb/b/a/a/e/a$a;-><init>(Lb/b/a/a/e/a;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

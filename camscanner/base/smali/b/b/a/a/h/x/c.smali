.class public Lb/b/a/a/h/x/c;
.super Ljava/lang/Object;
.source "ImageRequest.java"

# interfaces
.implements Lb/b/a/a/h/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb/b/a/a/h/x/c$b;,
        Lb/b/a/a/h/x/c$c;
    }
.end annotation


# instance fields
.field private O8:Lb/b/a/a/h/o;

.field private OO0o〇〇:Z

.field private OO0o〇〇〇〇0:Lb/b/a/a/h/u;

.field private Oo08:Landroid/widget/ImageView$ScaleType;

.field private OoO8:Lb/b/a/a/h/g;

.field private Oooo8o0〇:Z

.field private o800o8O:I

.field private oO80:I

.field private oo88o8O:Lb/b/a/a/h/x/a;

.field private o〇0:Landroid/graphics/Bitmap$Config;

.field private o〇O8〇〇o:I

.field private 〇00:I

.field private 〇080:Ljava/lang/String;

.field private 〇0〇O0088o:Z

.field private 〇80〇808〇O:Lb/b/a/a/h/h;

.field private 〇8o8o〇:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O00:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lb/b/a/a/h/y/i;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O888o0o:Lb/b/a/a/h/x/f;

.field private volatile 〇O8o08O:Z

.field private 〇O〇:Lb/b/a/a/h/t;

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇oo〇:Lb/b/a/a/h/b;

.field private 〇o〇:Ljava/lang/String;

.field private 〇〇808〇:Lb/b/a/a/h/s;

.field private 〇〇888:I

.field private final 〇〇8O0〇8:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Lb/b/a/a/h/x/c$c;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇O00:Ljava/util/Queue;

    .line 4
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇〇8O0〇8:Landroid/os/Handler;

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lb/b/a/a/h/x/c;->〇0〇O0088o:Z

    .line 6
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇80〇808〇O(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇080:Ljava/lang/String;

    .line 7
    new-instance v0, Lb/b/a/a/h/x/c$b;

    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->OO0o〇〇〇〇0(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/o;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/b/a/a/h/x/c$b;-><init>(Lb/b/a/a/h/x/c;Lb/b/a/a/h/o;)V

    iput-object v0, p0, Lb/b/a/a/h/x/c;->O8:Lb/b/a/a/h/o;

    .line 8
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->o800o8O(Lb/b/a/a/h/x/c$c;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇8o8o〇:Ljava/lang/ref/WeakReference;

    .line 9
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O888o0o(Lb/b/a/a/h/x/c$c;)Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->Oo08:Landroid/widget/ImageView$ScaleType;

    .line 10
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->oo88o8O(Lb/b/a/a/h/x/c$c;)Landroid/graphics/Bitmap$Config;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->o〇0:Landroid/graphics/Bitmap$Config;

    .line 11
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇oo〇(Lb/b/a/a/h/x/c$c;)I

    move-result v0

    iput v0, p0, Lb/b/a/a/h/x/c;->〇〇888:I

    .line 12
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->o〇O8〇〇o(Lb/b/a/a/h/x/c$c;)I

    move-result v0

    iput v0, p0, Lb/b/a/a/h/x/c;->oO80:I

    .line 13
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇00(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/u;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lb/b/a/a/h/u;->a:Lb/b/a/a/h/u;

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇00(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/u;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lb/b/a/a/h/x/c;->OO0o〇〇〇〇0:Lb/b/a/a/h/u;

    .line 14
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->O〇8O8〇008(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/t;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lb/b/a/a/h/t;->b:Lb/b/a/a/h/t;

    goto :goto_1

    .line 15
    :cond_1
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->O〇8O8〇008(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/t;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇O〇:Lb/b/a/a/h/t;

    .line 16
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->O8ooOoo〇(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/s;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇〇808〇:Lb/b/a/a/h/s;

    .line 17
    invoke-direct {p0, p1}, Lb/b/a/a/h/x/c;->〇080(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/b;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇oo〇:Lb/b/a/a/h/b;

    .line 18
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O8o08O(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 19
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O8o08O(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/b/a/a/h/x/c;->〇〇888(Ljava/lang/String;)V

    .line 20
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O8o08O(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/b/a/a/h/x/c;->〇O8o08O(Ljava/lang/String;)V

    .line 21
    :cond_2
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->OO0o〇〇(Lb/b/a/a/h/x/c$c;)Z

    move-result v0

    iput-boolean v0, p0, Lb/b/a/a/h/x/c;->OO0o〇〇:Z

    .line 22
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->Oooo8o0〇(Lb/b/a/a/h/x/c$c;)Z

    move-result v0

    iput-boolean v0, p0, Lb/b/a/a/h/x/c;->Oooo8o0〇:Z

    .line 23
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇〇808〇(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/x/f;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇O888o0o:Lb/b/a/a/h/x/f;

    .line 24
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O〇(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/h;

    move-result-object v0

    iput-object v0, p0, Lb/b/a/a/h/x/c;->〇80〇808〇O:Lb/b/a/a/h/h;

    .line 25
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇O00(Lb/b/a/a/h/x/c$c;)I

    move-result v0

    iput v0, p0, Lb/b/a/a/h/x/c;->〇00:I

    .line 26
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇〇8O0〇8(Lb/b/a/a/h/x/c$c;)I

    move-result p1

    iput p1, p0, Lb/b/a/a/h/x/c;->o〇O8〇〇o:I

    .line 27
    iget-object p1, p0, Lb/b/a/a/h/x/c;->〇O00:Ljava/util/Queue;

    new-instance v0, Lb/b/a/a/h/y/c;

    invoke-direct {v0}, Lb/b/a/a/h/y/c;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method synthetic constructor <init>(Lb/b/a/a/h/x/c$c;Lb/b/a/a/h/x/c$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lb/b/a/a/h/x/c;-><init>(Lb/b/a/a/h/x/c$c;)V

    return-void
.end method

.method static synthetic OO0o〇〇(Lb/b/a/a/h/x/c;)Lb/b/a/a/h/s;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇〇808〇:Lb/b/a/a/h/s;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic OoO8(Lb/b/a/a/h/x/c;)Lb/b/a/a/h/h;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇80〇808〇O:Lb/b/a/a/h/h;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic Oooo8o0〇(Lb/b/a/a/h/x/c;)Lb/b/a/a/h/i;
    .locals 0

    .line 1
    invoke-direct {p0}, Lb/b/a/a/h/x/c;->〇o()Lb/b/a/a/h/i;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic o〇0(Lb/b/a/a/h/x/c;ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lb/b/a/a/h/x/c;->〇o〇(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
.end method

.method static synthetic o〇O8〇〇o(Lb/b/a/a/h/x/c;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇080(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/b;
    .locals 1

    .line 1
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇0〇O0088o(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/b;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->〇0〇O0088o(Lb/b/a/a/h/x/c$c;)Lb/b/a/a/h/b;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->OoO8(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    new-instance v0, Ljava/io/File;

    .line 23
    .line 24
    invoke-static {p1}, Lb/b/a/a/h/x/c$c;->OoO8(Lb/b/a/a/h/x/c$c;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v0}, Lb/b/a/a/h/x/i/a;->〇080(Ljava/io/File;)Lb/b/a/a/h/b;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    return-object p1

    .line 36
    :cond_1
    invoke-static {}, Lb/b/a/a/h/x/i/a;->O8()Lb/b/a/a/h/b;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method static synthetic 〇0〇O0088o(Lb/b/a/a/h/x/c;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇〇8O0〇8:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇80〇808〇O(Lb/b/a/a/h/x/c;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lb/b/a/a/h/x/c;->〇O8o08O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇8o8o〇(Lb/b/a/a/h/x/c;)Ljava/util/Queue;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇O00:Ljava/util/Queue;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇O00(Lb/b/a/a/h/x/c;)Lb/b/a/a/h/u;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->OO0o〇〇〇〇0:Lb/b/a/a/h/u;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method static synthetic 〇O888o0o(Lb/b/a/a/h/x/c;)Lb/b/a/a/h/t;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇O〇:Lb/b/a/a/h/t;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method private 〇o()Lb/b/a/a/h/i;
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇O888o0o:Lb/b/a/a/h/x/f;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lb/b/a/a/h/x/c;->O8:Lb/b/a/a/h/o;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v1, "not init !"

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/16 v3, 0x3ed

    .line 13
    .line 14
    invoke-interface {v0, v3, v1, v2}, Lb/b/a/a/h/o;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-object p0

    .line 18
    :cond_1
    invoke-virtual {v0}, Lb/b/a/a/h/x/f;->〇0〇O0088o()Ljava/util/concurrent/ExecutorService;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_2

    .line 23
    .line 24
    new-instance v1, Lb/b/a/a/h/x/c$a;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lb/b/a/a/h/x/c$a;-><init>(Lb/b/a/a/h/x/c;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    .line 34
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    :cond_2
    :goto_0
    return-object p0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method private 〇o〇(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 1
    new-instance v0, Lb/b/a/a/h/y/h;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lb/b/a/a/h/y/h;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0}, Lb/b/a/a/h/y/h;->〇080(Lb/b/a/a/h/x/c;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lb/b/a/a/h/x/c;->〇O00:Ljava/util/Queue;

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Collection;->clear()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
.end method

.method static synthetic 〇〇808〇(Lb/b/a/a/h/x/c;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 1
    iget-object p0, p0, Lb/b/a/a/h/x/c;->〇8o8o〇:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method


# virtual methods
.method public O8(Lb/b/a/a/h/g;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lb/b/a/a/h/x/c;->OoO8:Lb/b/a/a/h/g;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public O8ooOoo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lb/b/a/a/h/x/c;->〇00:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8〇o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/h/x/c;->Oooo8o0〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0(Lb/b/a/a/h/y/i;)Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/h/x/c;->〇O8o08O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇O00:Ljava/util/Queue;

    .line 8
    .line 9
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public OOO〇O0()Lb/b/a/a/h/u;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->OO0o〇〇〇〇0:Lb/b/a/a/h/u;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo08(Lb/b/a/a/h/x/a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lb/b/a/a/h/x/c;->oo88o8O:Lb/b/a/a/h/x/a;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public O〇8O8〇008()Lb/b/a/a/h/o;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->O8:Lb/b/a/a/h/o;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public b()I
    .locals 1

    .line 1
    iget v0, p0, Lb/b/a/a/h/x/c;->〇〇888:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public c()I
    .locals 1

    .line 1
    iget v0, p0, Lb/b/a/a/h/x/c;->oO80:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public d()Landroid/widget/ImageView$ScaleType;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->Oo08:Landroid/widget/ImageView$ScaleType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O()Lb/b/a/a/h/x/f;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇O888o0o:Lb/b/a/a/h/x/f;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lb/b/a/a/h/x/c;->〇0〇O0088o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public oo88o8O()Lb/b/a/a/h/x/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->oo88o8O:Lb/b/a/a/h/x/a;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/h/x/c;->〇0〇O0088o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇〇0〇()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lb/b/a/a/h/x/c;->e()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lb/b/a/a/h/x/c;->OOO〇O0()Lb/b/a/a/h/u;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
.end method

.method public 〇00()Lb/b/a/a/h/g;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->OoO8:Lb/b/a/a/h/g;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0000OOO()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇00〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lb/b/a/a/h/x/c;->OO0o〇〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8o08O(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lb/b/a/a/h/x/c;->〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇O〇()Lb/b/a/a/h/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇oo〇:Lb/b/a/a/h/b;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lb/b/a/a/h/x/c;->o800o8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
.end method

.method public 〇oOO8O8()I
    .locals 1

    .line 1
    iget v0, p0, Lb/b/a/a/h/x/c;->o〇O8〇〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lb/b/a/a/h/x/c;->o800o8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇888(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇8o8o〇:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lb/b/a/a/h/x/c;->〇8o8o〇:Ljava/lang/ref/WeakReference;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Landroid/widget/ImageView;

    .line 18
    .line 19
    const v1, 0x413c0901

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    iput-object p1, p0, Lb/b/a/a/h/x/c;->〇o00〇〇Oo:Ljava/lang/String;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
.end method

.method public 〇〇8O0〇8()Landroid/graphics/Bitmap$Config;
    .locals 1

    .line 1
    iget-object v0, p0, Lb/b/a/a/h/x/c;->o〇0:Landroid/graphics/Bitmap$Config;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/hciilab/digitalink/core/PenType;
.super Ljava/lang/Object;
.source "PenType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;
    }
.end annotation


# static fields
.field public static final BALL_PEN:I = 0x4

.field public static final BRUSH_ONE:I = 0x6

.field public static final BRUSH_REGULAR:I = 0x8

.field public static final EMBOSS_PEN:I = 0x9

.field public static final FREE_ERASER:I = 0x2

.field public static final HIGHT_LIGHT_PEN:I = 0x3

.field public static final MARKER_PEN:I = 0x1

.field public static final PENCIL:I = 0x7

.field public static final PEN_COUNT:I = 0x9

.field public static final PEN_PREFIX:I = 0x0

.field public static final QUILL_PEN:I = 0x5

.field private static mOnPenRangeChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mRangeMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "[F>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    new-array v2, v1, [F

    .line 10
    .line 11
    fill-array-data v2, :array_0

    .line 12
    .line 13
    .line 14
    const/4 v3, 0x4

    .line 15
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 19
    .line 20
    new-array v2, v1, [F

    .line 21
    .line 22
    fill-array-data v2, :array_1

    .line 23
    .line 24
    .line 25
    const/4 v3, 0x6

    .line 26
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 30
    .line 31
    new-array v2, v1, [F

    .line 32
    .line 33
    fill-array-data v2, :array_2

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x1

    .line 37
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 41
    .line 42
    new-array v2, v1, [F

    .line 43
    .line 44
    fill-array-data v2, :array_3

    .line 45
    .line 46
    .line 47
    const/4 v3, 0x5

    .line 48
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 52
    .line 53
    new-array v2, v1, [F

    .line 54
    .line 55
    fill-array-data v2, :array_4

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 62
    .line 63
    new-array v2, v1, [F

    .line 64
    .line 65
    fill-array-data v2, :array_5

    .line 66
    .line 67
    .line 68
    const/4 v3, 0x7

    .line 69
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 73
    .line 74
    new-array v2, v1, [F

    .line 75
    .line 76
    fill-array-data v2, :array_6

    .line 77
    .line 78
    .line 79
    const/16 v3, 0x8

    .line 80
    .line 81
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 82
    .line 83
    .line 84
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 85
    .line 86
    new-array v2, v1, [F

    .line 87
    .line 88
    fill-array-data v2, :array_7

    .line 89
    .line 90
    .line 91
    const/4 v3, 0x3

    .line 92
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 93
    .line 94
    .line 95
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 96
    .line 97
    new-array v1, v1, [F

    .line 98
    .line 99
    fill-array-data v1, :array_8

    .line 100
    .line 101
    .line 102
    const/16 v2, 0x9

    .line 103
    .line 104
    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    return-void

    .line 108
    nop

    .line 109
    :array_0
    .array-data 4
        0x40000000    # 2.0f
        0x41200000    # 10.0f
    .end array-data

    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    :array_1
    .array-data 4
        0x41700000    # 15.0f
        0x42480000    # 50.0f
    .end array-data

    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    :array_2
    .array-data 4
        0x40c00000    # 6.0f
        0x420c0000    # 35.0f
    .end array-data

    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    :array_3
    .array-data 4
        0x41a00000    # 20.0f
        0x420c0000    # 35.0f
    .end array-data

    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    :array_4
    .array-data 4
        0x40a00000    # 5.0f
        0x42c80000    # 100.0f
    .end array-data

    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    :array_5
    .array-data 4
        0x41200000    # 10.0f
        0x42180000    # 38.0f
    .end array-data

    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    :array_6
    .array-data 4
        0x41000000    # 8.0f
        0x41d80000    # 27.0f
    .end array-data

    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    :array_7
    .array-data 4
        0x41a00000    # 20.0f
        0x42c80000    # 100.0f
    .end array-data

    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    :array_8
    .array-data 4
        0x40800000    # 4.0f
        0x41e00000    # 28.0f
    .end array-data
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static getPenWidthRange(I[F)V
    .locals 3

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, [F

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    array-length v0, p1

    .line 12
    const/4 v1, 0x1

    .line 13
    if-le v0, v1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    aget v2, p0, v0

    .line 17
    .line 18
    aput v2, p1, v0

    .line 19
    .line 20
    aget p0, p0, v1

    .line 21
    .line 22
    aput p0, p1, v1

    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static notifyChanged()V
    .locals 2

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mOnPenRangeChangeListeners:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;

    .line 20
    .line 21
    invoke-interface {v1}, Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;->onPenRangeChanged()V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method static putPenRange(I[F)V
    .locals 1

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static registerOnPenRangeChangeListener(Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mOnPenRangeChangeListeners:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/hciilab/digitalink/core/PenType;->mOnPenRangeChangeListeners:Ljava/util/List;

    .line 12
    .line 13
    :cond_0
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mOnPenRangeChangeListeners:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static resetRangeMap()V
    .locals 1

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mRangeMap:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static unregisterOnPenRangeChangeListener(Lcom/hciilab/digitalink/core/PenType$OnPenRangeChangeListener;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/hciilab/digitalink/core/PenType;->mOnPenRangeChangeListeners:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

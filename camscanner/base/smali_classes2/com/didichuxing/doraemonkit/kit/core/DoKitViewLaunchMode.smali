.class public final enum Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;
.super Ljava/lang/Enum;
.source "DokitIntent.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

.field public static final enum COUNTDOWN:Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

.field public static final enum SINGLE_INSTANCE:Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 3
    .line 4
    new-instance v1, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 5
    .line 6
    const-string v2, "SINGLE_INSTANCE"

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;-><init>(Ljava/lang/String;I)V

    .line 10
    .line 11
    .line 12
    sput-object v1, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;->SINGLE_INSTANCE:Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 13
    .line 14
    aput-object v1, v0, v3

    .line 15
    .line 16
    new-instance v1, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 17
    .line 18
    const-string v2, "COUNTDOWN"

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;-><init>(Ljava/lang/String;I)V

    .line 22
    .line 23
    .line 24
    sput-object v1, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;->COUNTDOWN:Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 25
    .line 26
    aput-object v1, v0, v3

    .line 27
    .line 28
    sput-object v0, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;->$VALUES:[Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;
    .locals 1

    .line 1
    const-class v0, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static values()[Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;->$VALUES:[Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/didichuxing/doraemonkit/kit/core/DoKitViewLaunchMode;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

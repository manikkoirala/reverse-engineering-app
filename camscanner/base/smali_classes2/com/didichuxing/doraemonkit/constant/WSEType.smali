.class public final enum Lcom/didichuxing/doraemonkit/constant/WSEType;
.super Ljava/lang/Enum;
.source "WSEventType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/didichuxing/doraemonkit/constant/WSEType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum ACTIVITY_BACK_PRESSED:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum ACTIVITY_FINISH:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum APP_ON_BACKGROUND:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum APP_ON_FOREGROUND:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_CLOSE:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_COMM_EVENT:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_CONNECTED:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_CUSTOM_EVENT:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_HOST_CLOSE:Lcom/didichuxing/doraemonkit/constant/WSEType;

.field public static final enum WSE_TEST:Lcom/didichuxing/doraemonkit/constant/WSEType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    new-array v0, v0, [Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 4
    .line 5
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 6
    .line 7
    const-string v2, "APP_ON_FOREGROUND"

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 11
    .line 12
    .line 13
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->APP_ON_FOREGROUND:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 14
    .line 15
    aput-object v1, v0, v3

    .line 16
    .line 17
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 18
    .line 19
    const-string v2, "APP_ON_BACKGROUND"

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 23
    .line 24
    .line 25
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->APP_ON_BACKGROUND:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 26
    .line 27
    aput-object v1, v0, v3

    .line 28
    .line 29
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 30
    .line 31
    const-string v2, "WSE_TEST"

    .line 32
    .line 33
    const/4 v3, 0x2

    .line 34
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 35
    .line 36
    .line 37
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_TEST:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 38
    .line 39
    aput-object v1, v0, v3

    .line 40
    .line 41
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 42
    .line 43
    const-string v2, "ACTIVITY_BACK_PRESSED"

    .line 44
    .line 45
    const/4 v3, 0x3

    .line 46
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->ACTIVITY_BACK_PRESSED:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 50
    .line 51
    aput-object v1, v0, v3

    .line 52
    .line 53
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 54
    .line 55
    const-string v2, "ACTIVITY_FINISH"

    .line 56
    .line 57
    const/4 v3, 0x4

    .line 58
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 59
    .line 60
    .line 61
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->ACTIVITY_FINISH:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 62
    .line 63
    aput-object v1, v0, v3

    .line 64
    .line 65
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 66
    .line 67
    const-string v2, "WSE_CONNECTED"

    .line 68
    .line 69
    const/4 v3, 0x5

    .line 70
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 71
    .line 72
    .line 73
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_CONNECTED:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 74
    .line 75
    aput-object v1, v0, v3

    .line 76
    .line 77
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 78
    .line 79
    const-string v2, "WSE_CLOSE"

    .line 80
    .line 81
    const/4 v3, 0x6

    .line 82
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 83
    .line 84
    .line 85
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_CLOSE:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 86
    .line 87
    aput-object v1, v0, v3

    .line 88
    .line 89
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 90
    .line 91
    const-string v2, "WSE_HOST_CLOSE"

    .line 92
    .line 93
    const/4 v3, 0x7

    .line 94
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 95
    .line 96
    .line 97
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_HOST_CLOSE:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 98
    .line 99
    aput-object v1, v0, v3

    .line 100
    .line 101
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 102
    .line 103
    const-string v2, "WSE_COMM_EVENT"

    .line 104
    .line 105
    const/16 v3, 0x8

    .line 106
    .line 107
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 108
    .line 109
    .line 110
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_COMM_EVENT:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 111
    .line 112
    aput-object v1, v0, v3

    .line 113
    .line 114
    new-instance v1, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 115
    .line 116
    const-string v2, "WSE_CUSTOM_EVENT"

    .line 117
    .line 118
    const/16 v3, 0x9

    .line 119
    .line 120
    invoke-direct {v1, v2, v3}, Lcom/didichuxing/doraemonkit/constant/WSEType;-><init>(Ljava/lang/String;I)V

    .line 121
    .line 122
    .line 123
    sput-object v1, Lcom/didichuxing/doraemonkit/constant/WSEType;->WSE_CUSTOM_EVENT:Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 124
    .line 125
    aput-object v1, v0, v3

    .line 126
    .line 127
    sput-object v0, Lcom/didichuxing/doraemonkit/constant/WSEType;->$VALUES:[Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/didichuxing/doraemonkit/constant/WSEType;
    .locals 1

    .line 1
    const-class v0, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static values()[Lcom/didichuxing/doraemonkit/constant/WSEType;
    .locals 1

    .line 1
    sget-object v0, Lcom/didichuxing/doraemonkit/constant/WSEType;->$VALUES:[Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/didichuxing/doraemonkit/constant/WSEType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/didichuxing/doraemonkit/constant/WSEType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

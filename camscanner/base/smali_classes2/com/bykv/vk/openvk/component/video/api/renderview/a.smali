.class public interface abstract Lcom/bykv/vk/openvk/component/video/api/renderview/a;
.super Ljava/lang/Object;
.source "IRenderCallback.java"


# virtual methods
.method public abstract onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
.end method

.method public abstract onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
.end method

.method public abstract onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
.end method

.method public abstract onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
.end method

.method public abstract surfaceChanged(Landroid/view/SurfaceHolder;III)V
.end method

.method public abstract surfaceCreated(Landroid/view/SurfaceHolder;)V
.end method

.method public abstract surfaceDestroyed(Landroid/view/SurfaceHolder;)V
.end method

.class public Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;
.super Ljava/lang/Object;
.source "RecyclingPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/adapter/RecyclingPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecycleBin"
.end annotation


# instance fields
.field private O8:I

.field private Oo08:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080:[Landroid/view/View;

.field private 〇o00〇〇Oo:[I

.field private 〇o〇:[Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    new-array v1, v0, [Landroid/view/View;

    .line 6
    .line 7
    iput-object v1, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇080:[Landroid/view/View;

    .line 8
    .line 9
    new-array v0, v0, [I

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o00〇〇Oo:[I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method static O8(Landroid/util/SparseArray;I)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_2

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    check-cast v3, Landroid/view/View;

    .line 19
    .line 20
    if-ne v2, p1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 23
    .line 24
    .line 25
    return-object v3

    .line 26
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 30
    .line 31
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    check-cast p1, Landroid/view/View;

    .line 36
    .line 37
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->keyAt(I)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 42
    .line 43
    .line 44
    return-object p1

    .line 45
    :cond_2
    const/4 p0, 0x0

    .line 46
    return-object p0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private 〇o〇()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇080:[Landroid/view/View;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    iget v1, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8:I

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇:[Landroid/util/SparseArray;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x0

    .line 10
    :goto_0
    if-ge v4, v1, :cond_1

    .line 11
    .line 12
    aget-object v5, v2, v4

    .line 13
    .line 14
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    .line 15
    .line 16
    .line 17
    move-result v6

    .line 18
    sub-int v7, v6, v0

    .line 19
    .line 20
    add-int/lit8 v6, v6, -0x1

    .line 21
    .line 22
    const/4 v8, 0x0

    .line 23
    :goto_1
    if-ge v8, v7, :cond_0

    .line 24
    .line 25
    add-int/lit8 v9, v6, -0x1

    .line 26
    .line 27
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->keyAt(I)I

    .line 28
    .line 29
    .line 30
    move-result v6

    .line 31
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->remove(I)V

    .line 32
    .line 33
    .line 34
    add-int/lit8 v8, v8, 0x1

    .line 35
    .line 36
    move v6, v9

    .line 37
    goto :goto_1

    .line 38
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method Oo08()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇080:[Landroid/view/View;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o00〇〇Oo:[I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8:I

    .line 6
    .line 7
    const/4 v3, 0x1

    .line 8
    if-le v2, v3, :cond_0

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v2, 0x0

    .line 13
    :goto_0
    iget-object v4, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->Oo08:Landroid/util/SparseArray;

    .line 14
    .line 15
    array-length v5, v0

    .line 16
    sub-int/2addr v5, v3

    .line 17
    :goto_1
    if-ltz v5, :cond_4

    .line 18
    .line 19
    aget-object v3, v0, v5

    .line 20
    .line 21
    if-eqz v3, :cond_3

    .line 22
    .line 23
    aget v6, v1, v5

    .line 24
    .line 25
    const/4 v7, 0x0

    .line 26
    aput-object v7, v0, v5

    .line 27
    .line 28
    const/4 v8, -0x1

    .line 29
    aput v8, v1, v5

    .line 30
    .line 31
    invoke-virtual {p0, v6}, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇〇888(I)Z

    .line 32
    .line 33
    .line 34
    move-result v8

    .line 35
    if-nez v8, :cond_1

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_1
    if-eqz v2, :cond_2

    .line 39
    .line 40
    iget-object v4, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇:[Landroid/util/SparseArray;

    .line 41
    .line 42
    aget-object v4, v4, v6

    .line 43
    .line 44
    :cond_2
    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v3, v7}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 48
    .line 49
    .line 50
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, -0x1

    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_4
    invoke-direct {p0}, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public o〇0(I)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-lt p1, v0, :cond_1

    .line 3
    .line 4
    new-array v0, p1, [Landroid/util/SparseArray;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    if-ge v2, p1, :cond_0

    .line 9
    .line 10
    new-instance v3, Landroid/util/SparseArray;

    .line 11
    .line 12
    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 13
    .line 14
    .line 15
    aput-object v3, v0, v2

    .line 16
    .line 17
    add-int/lit8 v2, v2, 0x1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iput p1, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8:I

    .line 21
    .line 22
    aget-object p1, v0, v1

    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->Oo08:Landroid/util/SparseArray;

    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇:[Landroid/util/SparseArray;

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 30
    .line 31
    const-string v0, "Can\'t have a viewTypeCount < 1"

    .line 32
    .line 33
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method 〇080(Landroid/view/View;II)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object p3, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->Oo08:Landroid/util/SparseArray;

    .line 7
    .line 8
    invoke-virtual {p3, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇:[Landroid/util/SparseArray;

    .line 13
    .line 14
    aget-object p3, v0, p3

    .line 15
    .line 16
    invoke-virtual {p3, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    const/4 p2, 0x0

    .line 20
    invoke-virtual {p1, p2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method 〇o00〇〇Oo(II)Landroid/view/View;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->Oo08:Landroid/util/SparseArray;

    .line 7
    .line 8
    invoke-static {p2, p1}, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8(Landroid/util/SparseArray;I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1

    .line 13
    :cond_0
    if-ltz p2, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->〇o〇:[Landroid/util/SparseArray;

    .line 16
    .line 17
    array-length v1, v0

    .line 18
    if-ge p2, v1, :cond_1

    .line 19
    .line 20
    aget-object p2, v0, p2

    .line 21
    .line 22
    invoke-static {p2, p1}, Lcom/intsig/adapter/RecyclingPagerAdapter$RecycleBin;->O8(Landroid/util/SparseArray;I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    return-object p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected 〇〇888(I)Z
    .locals 0

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    const/4 p1, 0x0

    .line 6
    :goto_0
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

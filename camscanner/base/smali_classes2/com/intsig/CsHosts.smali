.class public final Lcom/intsig/CsHosts;
.super Ljava/lang/Object;
.source "CsHosts.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8:Z

.field private static final Oo08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇080:Lcom/intsig/CsHosts;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static 〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

.field public static 〇o〇:Lcom/intsig/tianshu/ApiCenterInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/CsHosts;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇8O0〇8()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    sput-boolean v0, Lcom/intsig/CsHosts;->O8:Z

    .line 13
    .line 14
    const-string v0, "https://ipv6-test.intsig.net/ping"

    .line 15
    .line 16
    sput-object v0, Lcom/intsig/CsHosts;->Oo08:Ljava/lang/String;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static final O8()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->CDNS_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://s.intsig.net"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final O8ooOoo〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->WEBAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://a.intsig.net"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final OO0o〇〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->OAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://open-bak-sandbox.camscanner.com/sync"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://open-bak.camscanner.com/sync"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->LOGAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://log.intsig.net/sync"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final Oo08()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "https://api-center-sandbox.intsig.net/apis"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "https://api-center.intsig.net/apis"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public static final OoO8()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->TAPI_US:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://cs2-sandbox.intsig.net/team"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://d82.intsig.net/team"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final Oooo8o0〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->PAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-bak-sandbox.intsig.net/purchase/cs"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs-bak.intsig.net/purchase/cs"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final O〇8O8〇008()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->UPPIC_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://d2100.intsig.net/sync"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final o800o8O()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->TMSG_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "tmsg-sandbox.intsig.net:10010"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "cs-tmpmsg.intsig.net:443"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final oO80()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->IPV4_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://ipv4-cs-sandbox.intsig.net/sync/get_ipv4"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://ipv4-cs.intsig.net/sync/get_ipv4"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final oo88o8O()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->UAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-bak-sandbox.intsig.net/user/cs"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs-bak.intsig.net/user/cs"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final o〇0()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->DMAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-sandbox.intsig.net/api/domain"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs.intsig.net/api/domain"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final o〇O8〇〇o()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o〇:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->UAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-bak-sandbox.intsig.net/user/cs"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs-bak.intsig.net/user/cs"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final o〇〇0〇()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->OAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v0, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 21
    :goto_2
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇00()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "https://d2100.intsig.net/badcase"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇0000OOO()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "https://b105-sandbox.camscanner.com"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "https://business105.camscanner.com"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇080()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "https://bcrs1.intsig.net"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇0〇O0088o()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->TAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://c1-sandbox.intsig.net/team"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://cs8.intsig.net/team"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇80〇808〇O()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇8o8o〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->MAPI_OLD:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_6

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_4

    .line 30
    .line 31
    sget-boolean v0, Lcom/intsig/CsHosts;->O8:Z

    .line 32
    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    const-string v0, "https://d2149.intsig.net/msg"

    .line 36
    .line 37
    goto :goto_3

    .line 38
    :cond_3
    const-string v0, "https://cs-msg-us-sandbox.intsig.net/msg"

    .line 39
    .line 40
    goto :goto_3

    .line 41
    :cond_4
    sget-boolean v0, Lcom/intsig/CsHosts;->O8:Z

    .line 42
    .line 43
    if-eqz v0, :cond_5

    .line 44
    .line 45
    const-string v0, "https://cs-msg.intsig.net/msg"

    .line 46
    .line 47
    goto :goto_3

    .line 48
    :cond_5
    const-string v0, "https://cs-msg-us.intsig.net/msg"

    .line 49
    .line 50
    :cond_6
    :goto_3
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇O00()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->QRAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-bak-sandbox.intsig.net/user/cs"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs-bak.intsig.net/user/cs"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇O888o0o()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->TRC_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://download.intsig.net/app"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇O8o08O()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "https://mo-sandbox.camscanner.com"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "https://mo.camscanner.com"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇O〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "https://ipv4-cs-sandbox.intsig.net"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "https://open.camscanner.com"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇o00〇〇Oo()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->BASEAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://oapi.camscanner.com"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇oOO8O8()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "https://b103-sandbox.camscanner.com"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "https://business103.camscanner.com"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public static final 〇oo〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->UAPI_CN:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-cs-sandbox.intsig.net/user/cs"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api-cs.intsig.net/user/cs"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇o〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->CDND_API:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_3

    .line 22
    .line 23
    const-string v0, "https://data.camscanner.com"

    .line 24
    .line 25
    :cond_3
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇〇808〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->PAPI_OLD:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://api-sandbox.intsig.net/purchase"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://api.intsig.net/purchase"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇〇888()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->EDAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://open-sandbox.camscanner.com/edapi"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://open.camscanner.com/edapi"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static final 〇〇8O0〇8()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/CsHosts;->〇o00〇〇Oo:Lcom/intsig/tianshu/ApiCenterInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/tianshu/ApiCenterInfo;->SAPI:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 21
    :goto_2
    if-eqz v1, :cond_4

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/CsHosts;->〇080:Lcom/intsig/CsHosts;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/CsHosts;->OOO〇O0()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const-string v0, "https://cs-bak-sandbox.intsig.net/sync"

    .line 32
    .line 33
    goto :goto_3

    .line 34
    :cond_3
    const-string v0, "https://cs-bak.intsig.net/sync"

    .line 35
    .line 36
    :cond_4
    :goto_3
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public final OOO〇O0()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

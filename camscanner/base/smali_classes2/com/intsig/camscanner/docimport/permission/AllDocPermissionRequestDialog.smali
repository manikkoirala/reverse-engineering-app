.class public final Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;
.super Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;
.source "AllDocPermissionRequestDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic oOo〇8o008:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇0O:Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$needRecheck$2;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$needRecheck$2;-><init>(Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 30
    .line 31
    new-instance v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$isFromHome$2;

    .line 32
    .line 33
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$isFromHome$2;-><init>(Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->o〇00O:Lkotlin/Lazy;

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$isFromBackUp$2;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$isFromBackUp$2;-><init>(Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;)V

    .line 45
    .line 46
    .line 47
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    iput-object v1, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->O8o08O8O:Lkotlin/Lazy;

    .line 52
    .line 53
    new-instance v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$pageId$2;

    .line 54
    .line 55
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$pageId$2;-><init>(Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;)V

    .line 56
    .line 57
    .line 58
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇080OO8〇0:Lkotlin/Lazy;

    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private final O0〇0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final o00〇88〇08()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇〇8O0〇8()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->O0〇0()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v3, "cancel, recheck: "

    .line 17
    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v1, ", show: "

    .line 25
    .line 26
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {p0, v1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->O0〇0()Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-eqz v1, :cond_3

    .line 44
    .line 45
    if-nez v0, :cond_0

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-nez v0, :cond_1

    .line 53
    .line 54
    return-void

    .line 55
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionDenyDialog;->o〇00O:Lcom/intsig/camscanner/docimport/permission/AllDocPermissionDenyDialog$Companion;

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇088O()Z

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionDenyDialog$Companion;->〇080(Z)Lcom/intsig/camscanner/docimport/permission/AllDocPermissionDenyDialog;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;->〇O8oOo0()Lcom/intsig/camscanner/docimport/util/FileManagerPermissionCheckUtil$IPermissionRequestCallback;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    if-eqz v2, :cond_2

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;->o880(Lcom/intsig/camscanner/docimport/util/FileManagerPermissionCheckUtil$IPermissionRequestCallback;)V

    .line 72
    .line 73
    .line 74
    :cond_2
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    const-string v2, "activity.supportFragmentManager"

    .line 79
    .line 80
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    const-string v2, "AllDocPermissionDenyDialog"

    .line 84
    .line 85
    invoke-virtual {v1, v0, v2}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 89
    .line 90
    .line 91
    return-void

    .line 92
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;->〇O8oOo0()Lcom/intsig/camscanner/docimport/util/FileManagerPermissionCheckUtil$IPermissionRequestCallback;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    if-eqz v0, :cond_4

    .line 100
    .line 101
    const/4 v1, 0x0

    .line 102
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/docimport/util/FileManagerPermissionCheckUtil$IPermissionRequestCallback;->〇080(Z)V

    .line 103
    .line 104
    .line 105
    :cond_4
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private final o〇0〇o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final 〇088O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final 〇8〇OOoooo()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇080OO8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final 〇〇o0〇8()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method protected dealClickAction(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    move-object v0, v1

    .line 18
    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_3

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇088O()Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-eqz p1, :cond_2

    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇oo〇()V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇8〇OOoooo()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇O00(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;->oooO888()V

    .line 46
    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    if-eqz v0, :cond_4

    .line 54
    .line 55
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 56
    .line 57
    :cond_4
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    if-eqz p1, :cond_5

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->o00〇88〇08()V

    .line 64
    .line 65
    .line 66
    :cond_5
    :goto_2
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 3
    .line 4
    .line 5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 6
    .line 7
    const/16 v1, 0x1e

    .line 8
    .line 9
    if-ge v0, v1, :cond_0

    .line 10
    .line 11
    const-string p1, "init, SDK < R"

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    const/4 v0, 0x2

    .line 21
    new-array v0, v0, [Landroid/view/View;

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const/4 v2, 0x0

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    move-object v1, v2

    .line 34
    :goto_0
    aput-object v1, v0, p1

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    if-eqz p1, :cond_2

    .line 41
    .line 42
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DialogAllDocPermissionRequestBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 43
    .line 44
    :cond_2
    const/4 p1, 0x1

    .line 45
    aput-object v2, v0, p1

    .line 46
    .line 47
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇088O()Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_3

    .line 55
    .line 56
    sget-object p1, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->o800o8O()V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;

    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇8〇OOoooo()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->o〇0〇o()Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->〇〇8O0〇8(Ljava/lang/String;Z)V

    .line 73
    .line 74
    .line 75
    :goto_1
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method protected logTag()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "AllDocPermissionRequestDialog"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0175

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

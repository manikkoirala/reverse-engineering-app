.class public final Lcom/applovin/exoplayer2/i/a/a;
.super Lcom/applovin/exoplayer2/i/a/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/applovin/exoplayer2/i/a/a$a;
    }
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I

.field private static final h:[Z


# instance fields
.field private final i:Lcom/applovin/exoplayer2/l/y;

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:J

.field private final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/applovin/exoplayer2/i/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/applovin/exoplayer2/i/a/a$a;

.field private p:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/applovin/exoplayer2/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/applovin/exoplayer2/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:B

.field private w:B

.field private x:I

.field private y:Z

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v1, v0, [I

    .line 4
    .line 5
    fill-array-data v1, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v1, Lcom/applovin/exoplayer2/i/a/a;->a:[I

    .line 9
    .line 10
    new-array v0, v0, [I

    .line 11
    .line 12
    fill-array-data v0, :array_1

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->b:[I

    .line 16
    .line 17
    const/4 v0, 0x7

    .line 18
    new-array v0, v0, [I

    .line 19
    .line 20
    fill-array-data v0, :array_2

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->c:[I

    .line 24
    .line 25
    const/16 v0, 0x60

    .line 26
    .line 27
    new-array v0, v0, [I

    .line 28
    .line 29
    fill-array-data v0, :array_3

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->d:[I

    .line 33
    .line 34
    const/16 v0, 0x10

    .line 35
    .line 36
    new-array v0, v0, [I

    .line 37
    .line 38
    fill-array-data v0, :array_4

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->e:[I

    .line 42
    .line 43
    const/16 v0, 0x20

    .line 44
    .line 45
    new-array v1, v0, [I

    .line 46
    .line 47
    fill-array-data v1, :array_5

    .line 48
    .line 49
    .line 50
    sput-object v1, Lcom/applovin/exoplayer2/i/a/a;->f:[I

    .line 51
    .line 52
    new-array v0, v0, [I

    .line 53
    .line 54
    fill-array-data v0, :array_6

    .line 55
    .line 56
    .line 57
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->g:[I

    .line 58
    .line 59
    const/16 v0, 0x100

    .line 60
    .line 61
    new-array v0, v0, [Z

    .line 62
    .line 63
    fill-array-data v0, :array_7

    .line 64
    .line 65
    .line 66
    sput-object v0, Lcom/applovin/exoplayer2/i/a/a;->h:[Z

    .line 67
    .line 68
    return-void

    .line 69
    :array_0
    .array-data 4
        0xb
        0x1
        0x3
        0xc
        0xe
        0x5
        0x7
        0x9
    .end array-data

    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    :array_1
    .array-data 4
        0x0
        0x4
        0x8
        0xc
        0x10
        0x14
        0x18
        0x1c
    .end array-data

    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    :array_2
    .array-data 4
        -0x1
        -0xff0100
        -0xffff01
        -0xff0001
        -0x10000
        -0x100
        -0xff01
    .end array-data

    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    :array_3
    .array-data 4
        0x20
        0x21
        0x22
        0x23
        0x24
        0x25
        0x26
        0x27
        0x28
        0x29
        0xe1
        0x2b
        0x2c
        0x2d
        0x2e
        0x2f
        0x30
        0x31
        0x32
        0x33
        0x34
        0x35
        0x36
        0x37
        0x38
        0x39
        0x3a
        0x3b
        0x3c
        0x3d
        0x3e
        0x3f
        0x40
        0x41
        0x42
        0x43
        0x44
        0x45
        0x46
        0x47
        0x48
        0x49
        0x4a
        0x4b
        0x4c
        0x4d
        0x4e
        0x4f
        0x50
        0x51
        0x52
        0x53
        0x54
        0x55
        0x56
        0x57
        0x58
        0x59
        0x5a
        0x5b
        0xe9
        0x5d
        0xed
        0xf3
        0xfa
        0x61
        0x62
        0x63
        0x64
        0x65
        0x66
        0x67
        0x68
        0x69
        0x6a
        0x6b
        0x6c
        0x6d
        0x6e
        0x6f
        0x70
        0x71
        0x72
        0x73
        0x74
        0x75
        0x76
        0x77
        0x78
        0x79
        0x7a
        0xe7
        0xf7
        0xd1
        0xf1
        0x25a0
    .end array-data

    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    :array_4
    .array-data 4
        0xae
        0xb0
        0xbd
        0xbf
        0x2122
        0xa2
        0xa3
        0x266a
        0xe0
        0x20
        0xe8
        0xe2
        0xea
        0xee
        0xf4
        0xfb
    .end array-data

    :array_5
    .array-data 4
        0xc1
        0xc9
        0xd3
        0xda
        0xdc
        0xfc
        0x2018
        0xa1
        0x2a
        0x27
        0x2014
        0xa9
        0x2120
        0x2022
        0x201c
        0x201d
        0xc0
        0xc2
        0xc7
        0xc8
        0xca
        0xcb
        0xeb
        0xce
        0xcf
        0xef
        0xd4
        0xd9
        0xf9
        0xdb
        0xab
        0xbb
    .end array-data

    :array_6
    .array-data 4
        0xc3
        0xe3
        0xcd
        0xcc
        0xec
        0xd2
        0xf2
        0xd5
        0xf5
        0x7b
        0x7d
        0x5c
        0x5e
        0x5f
        0x7c
        0x7e
        0xc4
        0xe4
        0xd6
        0xf6
        0xdf
        0xa5
        0xa4
        0x2502
        0xc5
        0xe5
        0xd8
        0xf8
        0x250c
        0x2510
        0x2514
        0x2518
    .end array-data

    :array_7
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/c;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/applovin/exoplayer2/l/y;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/applovin/exoplayer2/l/y;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    .line 17
    .line 18
    new-instance v0, Lcom/applovin/exoplayer2/i/a/a$a;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    const/4 v2, 0x4

    .line 22
    invoke-direct {v0, v1, v2}, Lcom/applovin/exoplayer2/i/a/a$a;-><init>(II)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    .line 26
    .line 27
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->x:I

    .line 28
    .line 29
    const-wide/16 v3, 0x0

    .line 30
    .line 31
    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    cmp-long v0, p3, v3

    .line 37
    .line 38
    if-lez v0, :cond_0

    .line 39
    .line 40
    const-wide/16 v3, 0x3e8

    .line 41
    .line 42
    mul-long p3, p3, v3

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    move-wide p3, v5

    .line 46
    :goto_0
    iput-wide p3, p0, Lcom/applovin/exoplayer2/i/a/a;->m:J

    .line 47
    .line 48
    const-string p3, "application/x-mp4-cea-608"

    .line 49
    .line 50
    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    const/4 p3, 0x2

    .line 55
    const/4 p4, 0x3

    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    const/4 p1, 0x2

    .line 59
    goto :goto_1

    .line 60
    :cond_1
    const/4 p1, 0x3

    .line 61
    :goto_1
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->j:I

    .line 62
    .line 63
    const/4 p1, 0x1

    .line 64
    if-eq p2, p1, :cond_5

    .line 65
    .line 66
    if-eq p2, p3, :cond_4

    .line 67
    .line 68
    if-eq p2, p4, :cond_3

    .line 69
    .line 70
    if-eq p2, v2, :cond_2

    .line 71
    .line 72
    const-string p2, "Cea608Decoder"

    .line 73
    .line 74
    const-string p3, "Invalid channel. Defaulting to CC1."

    .line 75
    .line 76
    invoke-static {p2, p3}, Lcom/applovin/exoplayer2/l/q;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    .line 80
    .line 81
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_2
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    .line 85
    .line 86
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_3
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    .line 90
    .line 91
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_4
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    .line 95
    .line 96
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    .line 97
    .line 98
    goto :goto_2

    .line 99
    :cond_5
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    .line 100
    .line 101
    iput v1, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    .line 102
    .line 103
    :goto_2
    invoke-direct {p0, v1}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    .line 104
    .line 105
    .line 106
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    .line 107
    .line 108
    .line 109
    iput-boolean p1, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    .line 110
    .line 111
    iput-wide v5, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private a(BB)V
    .locals 5

    .line 46
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->a:[I

    and-int/lit8 p1, p1, 0x7

    aget p1, v0, p1

    and-int/lit8 v0, p2, 0x20

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v0}, Lcom/applovin/exoplayer2/i/a/a$a;->a(Lcom/applovin/exoplayer2/i/a/a$a;)I

    move-result v0

    if-eq p1, v0, :cond_3

    .line 48
    iget v0, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v0}, Lcom/applovin/exoplayer2/i/a/a$a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    new-instance v0, Lcom/applovin/exoplayer2/i/a/a$a;

    iget v3, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    iget v4, p0, Lcom/applovin/exoplayer2/i/a/a;->s:I

    invoke-direct {v0, v3, v4}, Lcom/applovin/exoplayer2/i/a/a$a;-><init>(II)V

    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    .line 50
    iget-object v3, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v0, p1}, Lcom/applovin/exoplayer2/i/a/a$a;->b(Lcom/applovin/exoplayer2/i/a/a$a;I)I

    :cond_3
    and-int/lit8 p1, p2, 0x10

    const/16 v0, 0x10

    if-ne p1, v0, :cond_4

    const/4 p1, 0x1

    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    :goto_1
    and-int/lit8 v0, p2, 0x1

    if-ne v0, v2, :cond_5

    const/4 v1, 0x1

    :cond_5
    shr-int/2addr p2, v2

    and-int/lit8 p2, p2, 0x7

    .line 52
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    if-eqz p1, :cond_6

    const/16 v2, 0x8

    goto :goto_2

    :cond_6
    move v2, p2

    :goto_2
    invoke-virtual {v0, v2, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(IZ)V

    if-eqz p1, :cond_7

    .line 53
    iget-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->b:[I

    aget p2, v0, p2

    invoke-static {p1, p2}, Lcom/applovin/exoplayer2/i/a/a$a;->c(Lcom/applovin/exoplayer2/i/a/a$a;I)I

    :cond_7
    return-void
.end method

.method private a(I)V
    .locals 2

    .line 54
    iget v0, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    if-ne v0, p1, :cond_0

    return-void

    .line 55
    :cond_0
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    const/4 v0, 0x0

    .line 56
    :goto_0
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v1, p1}, Lcom/applovin/exoplayer2/i/a/a$a;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    .line 58
    :cond_2
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    if-eq v0, v1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    if-nez p1, :cond_4

    .line 59
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    :cond_4
    return-void
.end method

.method private a(B)Z
    .locals 1

    .line 36
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->h(B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->i(B)I

    move-result p1

    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->x:I

    .line 38
    :cond_0
    iget p1, p0, Lcom/applovin/exoplayer2/i/a/a;->x:I

    iget v0, p0, Lcom/applovin/exoplayer2/i/a/a;->l:I

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(ZBB)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 39
    invoke-static {p2}, Lcom/applovin/exoplayer2/i/a/a;->j(B)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 40
    iget-boolean p1, p0, Lcom/applovin/exoplayer2/i/a/a;->u:Z

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget-byte p1, p0, Lcom/applovin/exoplayer2/i/a/a;->v:B

    if-ne p1, p2, :cond_0

    iget-byte p1, p0, Lcom/applovin/exoplayer2/i/a/a;->w:B

    if-ne p1, p3, :cond_0

    .line 41
    iput-boolean v0, p0, Lcom/applovin/exoplayer2/i/a/a;->u:Z

    return v1

    .line 42
    :cond_0
    iput-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->u:Z

    .line 43
    iput-byte p2, p0, Lcom/applovin/exoplayer2/i/a/a;->v:B

    .line 44
    iput-byte p3, p0, Lcom/applovin/exoplayer2/i/a/a;->w:B

    goto :goto_0

    .line 45
    :cond_1
    iput-boolean v0, p0, Lcom/applovin/exoplayer2/i/a/a;->u:Z

    :goto_0
    return v0
.end method

.method private b(B)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(C)V

    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    shr-int/2addr p1, v1

    and-int/lit8 p1, p1, 0x7

    .line 4
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v1, p1, v0}, Lcom/applovin/exoplayer2/i/a/a$a;->a(IZ)V

    return-void
.end method

.method private b(BB)V
    .locals 2

    .line 7
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->k(B)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 8
    iput-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    goto :goto_0

    .line 9
    :cond_0
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->l(B)Z

    move-result p1

    if-eqz p1, :cond_2

    const/16 p1, 0x20

    if-eq p2, p1, :cond_1

    const/16 p1, 0x2f

    if-eq p2, p1, :cond_1

    packed-switch p2, :pswitch_data_0

    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 10
    :pswitch_0
    iput-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    goto :goto_0

    :cond_1
    :pswitch_1
    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x25
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x29
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 1

    .line 5
    iput p1, p0, Lcom/applovin/exoplayer2/i/a/a;->s:I

    .line 6
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v0, p1}, Lcom/applovin/exoplayer2/i/a/a$a;->c(I)V

    return-void
.end method

.method private c(B)V
    .locals 4

    const/16 v0, 0x20

    const/4 v1, 0x2

    if-eq p1, v0, :cond_5

    const/16 v0, 0x29

    const/4 v2, 0x3

    if-eq p1, v0, :cond_4

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    .line 15
    iget v1, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/16 v3, 0x21

    if-eq p1, v3, :cond_2

    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 16
    :pswitch_0
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->l()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    .line 17
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    goto :goto_0

    .line 18
    :pswitch_1
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    goto :goto_0

    :pswitch_2
    if-ne v1, v0, :cond_3

    .line 19
    iget-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {p1}, Lcom/applovin/exoplayer2/i/a/a$a;->a()Z

    move-result p1

    if-nez p1, :cond_3

    .line 20
    iget-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {p1}, Lcom/applovin/exoplayer2/i/a/a$a;->c()V

    goto :goto_0

    .line 21
    :pswitch_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    .line 22
    iget p1, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    if-eq p1, v0, :cond_1

    if-ne p1, v2, :cond_3

    .line 23
    :cond_1
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    goto :goto_0

    .line 24
    :cond_2
    iget-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {p1}, Lcom/applovin/exoplayer2/i/a/a$a;->b()V

    :cond_3
    :goto_0
    return-void

    .line 25
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    const/4 p1, 0x4

    .line 26
    invoke-direct {p0, p1}, Lcom/applovin/exoplayer2/i/a/a;->b(I)V

    return-void

    .line 27
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    .line 28
    invoke-direct {p0, v2}, Lcom/applovin/exoplayer2/i/a/a;->b(I)V

    return-void

    .line 29
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    .line 30
    invoke-direct {p0, v1}, Lcom/applovin/exoplayer2/i/a/a;->b(I)V

    return-void

    .line 31
    :cond_4
    invoke-direct {p0, v2}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    return-void

    .line 32
    :cond_5
    invoke-direct {p0, v1}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x25
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2c
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static c(BB)Z
    .locals 1

    .line 1
    and-int/lit16 p0, p0, 0xf7

    const/16 v0, 0x11

    if-ne p0, v0, :cond_0

    and-int/lit16 p0, p1, 0xf0

    const/16 p1, 0x30

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static d(B)C
    .locals 1

    and-int/lit8 p0, p0, 0x7f

    add-int/lit8 p0, p0, -0x20

    .line 3
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->d:[I

    aget p0, v0, p0

    int-to-char p0, p0

    return p0
.end method

.method private static d(BB)Z
    .locals 1

    .line 2
    and-int/lit16 p0, p0, 0xf6

    const/16 v0, 0x12

    if-ne p0, v0, :cond_0

    and-int/lit16 p0, p1, 0xe0

    const/16 p1, 0x20

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static e(B)C
    .locals 1

    and-int/lit8 p0, p0, 0xf

    .line 8
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->e:[I

    aget p0, v0, p0

    int-to-char p0, p0

    return p0
.end method

.method private static e(BB)C
    .locals 0

    and-int/lit8 p0, p0, 0x1

    if-nez p0, :cond_0

    .line 9
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->f(B)C

    move-result p0

    return p0

    .line 10
    :cond_0
    invoke-static {p1}, Lcom/applovin/exoplayer2/i/a/a;->g(B)C

    move-result p0

    return p0
.end method

.method private static f(B)C
    .locals 1

    and-int/lit8 p0, p0, 0x1f

    .line 3
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->f:[I

    aget p0, v0, p0

    int-to-char p0, p0

    return p0
.end method

.method private static f(BB)Z
    .locals 1

    .line 1
    and-int/lit16 p0, p0, 0xf7

    const/16 v0, 0x11

    if-ne p0, v0, :cond_0

    and-int/lit16 p0, p1, 0xf0

    const/16 p1, 0x20

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static g(B)C
    .locals 1

    and-int/lit8 p0, p0, 0x1f

    .line 4
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->g:[I

    aget p0, v0, p0

    int-to-char p0, p0

    return p0
.end method

.method private static g(BB)Z
    .locals 1

    .line 1
    and-int/lit16 p0, p0, 0xf0

    const/16 v0, 0x10

    if-ne p0, v0, :cond_0

    and-int/lit16 p0, p1, 0xc0

    const/16 p1, 0x40

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static h(B)Z
    .locals 0

    .line 1
    and-int/lit16 p0, p0, 0xe0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static h(BB)Z
    .locals 1

    .line 2
    and-int/lit16 p0, p0, 0xf7

    const/16 v0, 0x17

    if-ne p0, v0, :cond_0

    const/16 p0, 0x21

    if-lt p1, p0, :cond_0

    const/16 p0, 0x23

    if-gt p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static i(B)I
    .locals 0

    .line 1
    shr-int/lit8 p0, p0, 0x3

    and-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private static i(BB)Z
    .locals 1

    .line 2
    and-int/lit16 p0, p0, 0xf6

    const/16 v0, 0x14

    if-ne p0, v0, :cond_0

    and-int/lit16 p0, p1, 0xf0

    const/16 p1, 0x20

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic i()[I
    .locals 1

    .line 3
    sget-object v0, Lcom/applovin/exoplayer2/i/a/a;->c:[I

    return-object v0
.end method

.method private static j(B)Z
    .locals 1

    .line 1
    and-int/lit16 p0, p0, 0xf0

    .line 2
    .line 3
    const/16 v0, 0x10

    .line 4
    .line 5
    if-ne p0, v0, :cond_0

    .line 6
    .line 7
    const/4 p0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 p0, 0x0

    .line 10
    :goto_0
    return p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private static k(B)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-gt v0, p0, :cond_0

    .line 3
    .line 4
    const/16 v1, 0xf

    .line 5
    .line 6
    if-gt p0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private l()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/applovin/exoplayer2/i/a;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_1

    .line 4
    iget-object v5, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/applovin/exoplayer2/i/a/a$a;

    const/high16 v6, -0x80000000

    invoke-virtual {v5, v6}, Lcom/applovin/exoplayer2/i/a/a$a;->d(I)Lcom/applovin/exoplayer2/i/a;

    move-result-object v5

    .line 5
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_0

    .line 6
    iget v5, v5, Lcom/applovin/exoplayer2/i/a;->j:I

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 7
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    if-ge v3, v0, :cond_4

    .line 8
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/applovin/exoplayer2/i/a;

    if-eqz v5, :cond_3

    .line 9
    iget v6, v5, Lcom/applovin/exoplayer2/i/a;->j:I

    if-eq v6, v2, :cond_2

    .line 10
    iget-object v5, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v5, v2}, Lcom/applovin/exoplayer2/i/a/a$a;->d(I)Lcom/applovin/exoplayer2/i/a;

    move-result-object v5

    invoke-static {v5}, Lcom/applovin/exoplayer2/l/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/applovin/exoplayer2/i/a;

    .line 11
    :cond_2
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    return-object v4
.end method

.method private static l(B)Z
    .locals 1

    .line 1
    and-int/lit16 p0, p0, 0xf7

    const/16 v0, 0x14

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    .line 2
    .line 3
    iget v1, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->n:Ljava/util/ArrayList;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private n()Z
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/applovin/exoplayer2/i/a/a;->m:J

    .line 2
    .line 3
    const/4 v2, 0x0

    .line 4
    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    cmp-long v5, v0, v3

    .line 10
    .line 11
    if-eqz v5, :cond_1

    .line 12
    .line 13
    iget-wide v0, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    .line 14
    .line 15
    cmp-long v5, v0, v3

    .line 16
    .line 17
    if-nez v5, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/c;->k()J

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    iget-wide v3, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    .line 25
    .line 26
    sub-long/2addr v0, v3

    .line 27
    iget-wide v3, p0, Lcom/applovin/exoplayer2/i/a/a;->m:J

    .line 28
    .line 29
    cmp-long v5, v0, v3

    .line 30
    .line 31
    if-ltz v5, :cond_1

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    :cond_1
    :goto_0
    return v2
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public bridge synthetic a(J)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/applovin/exoplayer2/i/a/c;->a(J)V

    return-void
.end method

.method protected a(Lcom/applovin/exoplayer2/i/j;)V
    .locals 9

    .line 2
    iget-object p1, p1, Lcom/applovin/exoplayer2/c/g;->b:Ljava/nio/ByteBuffer;

    invoke-static {p1}, Lcom/applovin/exoplayer2/l/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/nio/ByteBuffer;

    .line 3
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/Buffer;->limit()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/applovin/exoplayer2/l/y;->a([BI)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    .line 4
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    invoke-virtual {v1}, Lcom/applovin/exoplayer2/l/y;->a()I

    move-result v1

    iget v2, p0, Lcom/applovin/exoplayer2/i/a/a;->j:I

    const/4 v3, 0x1

    if-lt v1, v2, :cond_11

    const/4 v1, 0x2

    if-ne v2, v1, :cond_1

    const/4 v1, -0x4

    goto :goto_1

    .line 5
    :cond_1
    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    invoke-virtual {v1}, Lcom/applovin/exoplayer2/l/y;->h()I

    move-result v1

    int-to-byte v1, v1

    .line 6
    :goto_1
    iget-object v2, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    invoke-virtual {v2}, Lcom/applovin/exoplayer2/l/y;->h()I

    move-result v2

    .line 7
    iget-object v4, p0, Lcom/applovin/exoplayer2/i/a/a;->i:Lcom/applovin/exoplayer2/l/y;

    invoke-virtual {v4}, Lcom/applovin/exoplayer2/l/y;->h()I

    move-result v4

    and-int/lit8 v5, v1, 0x2

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    and-int/lit8 v5, v1, 0x1

    .line 8
    iget v6, p0, Lcom/applovin/exoplayer2/i/a/a;->k:I

    if-eq v5, v6, :cond_3

    goto :goto_0

    :cond_3
    and-int/lit8 v5, v2, 0x7f

    int-to-byte v5, v5

    and-int/lit8 v6, v4, 0x7f

    int-to-byte v6, v6

    if-nez v5, :cond_4

    if-nez v6, :cond_4

    goto :goto_0

    .line 9
    :cond_4
    iget-boolean v7, p0, Lcom/applovin/exoplayer2/i/a/a;->t:Z

    and-int/lit8 v1, v1, 0x4

    const/4 v8, 0x4

    if-ne v1, v8, :cond_5

    .line 10
    sget-object v1, Lcom/applovin/exoplayer2/i/a/a;->h:[Z

    aget-boolean v2, v1, v2

    if-eqz v2, :cond_5

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    iput-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->t:Z

    .line 11
    invoke-direct {p0, v1, v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->a(ZBB)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_0

    .line 12
    :cond_6
    iget-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->t:Z

    if-nez v1, :cond_8

    if-eqz v7, :cond_0

    .line 13
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    :cond_7
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 14
    :cond_8
    invoke-direct {p0, v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->b(BB)V

    .line 15
    iget-boolean v1, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    if-nez v1, :cond_9

    goto :goto_0

    .line 16
    :cond_9
    invoke-direct {p0, v5}, Lcom/applovin/exoplayer2/i/a/a;->a(B)Z

    move-result v1

    if-nez v1, :cond_a

    goto :goto_0

    .line 17
    :cond_a
    invoke-static {v5}, Lcom/applovin/exoplayer2/i/a/a;->h(B)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 18
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->c(BB)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 19
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v6}, Lcom/applovin/exoplayer2/i/a/a;->e(B)C

    move-result v1

    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(C)V

    goto :goto_3

    .line 20
    :cond_b
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->d(BB)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 21
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-virtual {v0}, Lcom/applovin/exoplayer2/i/a/a$a;->b()V

    .line 22
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->e(BB)C

    move-result v1

    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(C)V

    goto :goto_3

    .line 23
    :cond_c
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->f(BB)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 24
    invoke-direct {p0, v6}, Lcom/applovin/exoplayer2/i/a/a;->b(B)V

    goto :goto_3

    .line 25
    :cond_d
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->g(BB)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 26
    invoke-direct {p0, v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->a(BB)V

    goto :goto_3

    .line 27
    :cond_e
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->h(BB)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 28
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    add-int/lit8 v6, v6, -0x20

    invoke-static {v0, v6}, Lcom/applovin/exoplayer2/i/a/a$a;->a(Lcom/applovin/exoplayer2/i/a/a$a;I)I

    goto :goto_3

    .line 29
    :cond_f
    invoke-static {v5, v6}, Lcom/applovin/exoplayer2/i/a/a;->i(BB)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 30
    invoke-direct {p0, v6}, Lcom/applovin/exoplayer2/i/a/a;->c(B)V

    goto :goto_3

    .line 31
    :cond_10
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v5}, Lcom/applovin/exoplayer2/i/a/a;->d(B)C

    move-result v1

    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(C)V

    and-int/lit16 v0, v6, 0xe0

    if-eqz v0, :cond_7

    .line 32
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->o:Lcom/applovin/exoplayer2/i/a/a$a;

    invoke-static {v6}, Lcom/applovin/exoplayer2/i/a/a;->d(B)C

    move-result v1

    invoke-virtual {v0, v1}, Lcom/applovin/exoplayer2/i/a/a$a;->a(C)V

    goto :goto_3

    :cond_11
    if-eqz v0, :cond_13

    .line 33
    iget p1, p0, Lcom/applovin/exoplayer2/i/a/a;->r:I

    if-eq p1, v3, :cond_12

    const/4 v0, 0x3

    if-ne p1, v0, :cond_13

    .line 34
    :cond_12
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->l()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    .line 35
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/c;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    :cond_13
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/applovin/exoplayer2/c/f;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/a;->e()Lcom/applovin/exoplayer2/i/k;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/applovin/exoplayer2/i/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/applovin/exoplayer2/i/h;
        }
    .end annotation

    .line 2
    invoke-super {p0, p1}, Lcom/applovin/exoplayer2/i/a/c;->b(Lcom/applovin/exoplayer2/i/j;)V

    return-void
.end method

.method public c()V
    .locals 2

    .line 2
    invoke-super {p0}, Lcom/applovin/exoplayer2/i/a/c;->c()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    .line 4
    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->q:Ljava/util/List;

    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, v0}, Lcom/applovin/exoplayer2/i/a/a;->a(I)V

    const/4 v1, 0x4

    .line 6
    invoke-direct {p0, v1}, Lcom/applovin/exoplayer2/i/a/a;->b(I)V

    .line 7
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->m()V

    .line 8
    iput-boolean v0, p0, Lcom/applovin/exoplayer2/i/a/a;->t:Z

    .line 9
    iput-boolean v0, p0, Lcom/applovin/exoplayer2/i/a/a;->u:Z

    .line 10
    iput-byte v0, p0, Lcom/applovin/exoplayer2/i/a/a;->v:B

    .line 11
    iput-byte v0, p0, Lcom/applovin/exoplayer2/i/a/a;->w:B

    .line 12
    iput v0, p0, Lcom/applovin/exoplayer2/i/a/a;->x:I

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/applovin/exoplayer2/i/a/a;->y:Z

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    .line 14
    iput-wide v0, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    return-void
.end method

.method public d()V
    .locals 0

    .line 1
    return-void
.end method

.method public e()Lcom/applovin/exoplayer2/i/k;
    .locals 7
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/applovin/exoplayer2/i/h;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/applovin/exoplayer2/i/a/c;->e()Lcom/applovin/exoplayer2/i/k;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    invoke-direct {p0}, Lcom/applovin/exoplayer2/i/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/c;->j()Lcom/applovin/exoplayer2/i/k;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    .line 5
    iput-wide v1, p0, Lcom/applovin/exoplayer2/i/a/a;->z:J

    .line 6
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/a;->g()Lcom/applovin/exoplayer2/i/f;

    move-result-object v4

    .line 7
    invoke-virtual {p0}, Lcom/applovin/exoplayer2/i/a/c;->k()J

    move-result-wide v2

    const-wide v5, 0x7fffffffffffffffL

    move-object v1, v0

    invoke-virtual/range {v1 .. v6}, Lcom/applovin/exoplayer2/i/k;->a(JLcom/applovin/exoplayer2/i/f;J)V

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected f()Z
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    iget-object v1, p0, Lcom/applovin/exoplayer2/i/a/a;->q:Ljava/util/List;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected g()Lcom/applovin/exoplayer2/i/f;
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->p:Ljava/util/List;

    iput-object v0, p0, Lcom/applovin/exoplayer2/i/a/a;->q:Ljava/util/List;

    .line 3
    new-instance v1, Lcom/applovin/exoplayer2/i/a/d;

    invoke-static {v0}, Lcom/applovin/exoplayer2/l/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/applovin/exoplayer2/i/a/d;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public bridge synthetic h()Lcom/applovin/exoplayer2/i/j;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/applovin/exoplayer2/i/h;
        }
    .end annotation

    .line 3
    invoke-super {p0}, Lcom/applovin/exoplayer2/i/a/c;->h()Lcom/applovin/exoplayer2/i/j;

    move-result-object v0

    return-object v0
.end method

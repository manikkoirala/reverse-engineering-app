.class public final Lcom/applovin/sdk/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/applovin/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final al_exo_controls_cc_disabled_description:I = 0x7f1304b4

.field public static final al_exo_controls_cc_enabled_description:I = 0x7f1304b5

.field public static final al_exo_controls_custom_playback_speed:I = 0x7f1304b6

.field public static final al_exo_controls_fastforward_description:I = 0x7f1304b7

.field public static final al_exo_controls_fullscreen_enter_description:I = 0x7f1304b8

.field public static final al_exo_controls_fullscreen_exit_description:I = 0x7f1304b9

.field public static final al_exo_controls_hide:I = 0x7f1304ba

.field public static final al_exo_controls_next_description:I = 0x7f1304bb

.field public static final al_exo_controls_overflow_hide_description:I = 0x7f1304bc

.field public static final al_exo_controls_overflow_show_description:I = 0x7f1304bd

.field public static final al_exo_controls_pause_description:I = 0x7f1304be

.field public static final al_exo_controls_play_description:I = 0x7f1304bf

.field public static final al_exo_controls_playback_speed:I = 0x7f1304c0

.field public static final al_exo_controls_playback_speed_normal:I = 0x7f1304c1

.field public static final al_exo_controls_previous_description:I = 0x7f1304c2

.field public static final al_exo_controls_repeat_all_description:I = 0x7f1304c3

.field public static final al_exo_controls_repeat_off_description:I = 0x7f1304c4

.field public static final al_exo_controls_repeat_one_description:I = 0x7f1304c5

.field public static final al_exo_controls_rewind_description:I = 0x7f1304c6

.field public static final al_exo_controls_seek_bar_description:I = 0x7f1304c7

.field public static final al_exo_controls_settings_description:I = 0x7f1304c8

.field public static final al_exo_controls_show:I = 0x7f1304c9

.field public static final al_exo_controls_shuffle_off_description:I = 0x7f1304ca

.field public static final al_exo_controls_shuffle_on_description:I = 0x7f1304cb

.field public static final al_exo_controls_stop_description:I = 0x7f1304cc

.field public static final al_exo_controls_time_placeholder:I = 0x7f1304cd

.field public static final al_exo_controls_vr_description:I = 0x7f1304ce

.field public static final al_exo_download_completed:I = 0x7f1304cf

.field public static final al_exo_download_description:I = 0x7f1304d0

.field public static final al_exo_download_downloading:I = 0x7f1304d1

.field public static final al_exo_download_failed:I = 0x7f1304d2

.field public static final al_exo_download_notification_channel_name:I = 0x7f1304d3

.field public static final al_exo_download_paused:I = 0x7f1304d4

.field public static final al_exo_download_paused_for_network:I = 0x7f1304d5

.field public static final al_exo_download_paused_for_wifi:I = 0x7f1304d6

.field public static final al_exo_download_removing:I = 0x7f1304d7

.field public static final al_exo_item_list:I = 0x7f1304d8

.field public static final al_exo_track_bitrate:I = 0x7f1304d9

.field public static final al_exo_track_mono:I = 0x7f1304da

.field public static final al_exo_track_resolution:I = 0x7f1304db

.field public static final al_exo_track_role_alternate:I = 0x7f1304dc

.field public static final al_exo_track_role_closed_captions:I = 0x7f1304dd

.field public static final al_exo_track_role_commentary:I = 0x7f1304de

.field public static final al_exo_track_role_supplementary:I = 0x7f1304df

.field public static final al_exo_track_selection_auto:I = 0x7f1304e0

.field public static final al_exo_track_selection_none:I = 0x7f1304e1

.field public static final al_exo_track_selection_title_audio:I = 0x7f1304e2

.field public static final al_exo_track_selection_title_text:I = 0x7f1304e3

.field public static final al_exo_track_selection_title_video:I = 0x7f1304e4

.field public static final al_exo_track_stereo:I = 0x7f1304e5

.field public static final al_exo_track_surround:I = 0x7f1304e6

.field public static final al_exo_track_surround_5_point_1:I = 0x7f1304e7

.field public static final al_exo_track_surround_7_point_1:I = 0x7f1304e8

.field public static final al_exo_track_unknown:I = 0x7f1304e9

.field public static final applovin_agree_message:I = 0x7f1304f0

.field public static final applovin_alt_privacy_policy_text:I = 0x7f1304f1

.field public static final applovin_continue_button_text:I = 0x7f1304f2

.field public static final applovin_creative_debugger_disabled_text:I = 0x7f1304f3

.field public static final applovin_creative_debugger_no_ads_text:I = 0x7f1304f4

.field public static final applovin_gdpr_advertising_partners_screen_message:I = 0x7f1304f5

.field public static final applovin_gdpr_advertising_partners_screen_title:I = 0x7f1304f6

.field public static final applovin_gdpr_analytics_partners_screen_message:I = 0x7f1304f7

.field public static final applovin_gdpr_analytics_partners_screen_title:I = 0x7f1304f8

.field public static final applovin_gdpr_are_you_sure_screen_message:I = 0x7f1304f9

.field public static final applovin_gdpr_are_you_sure_screen_title:I = 0x7f1304fa

.field public static final applovin_gdpr_back_button_text:I = 0x7f1304fb

.field public static final applovin_gdpr_learn_more_screen_bullet_1:I = 0x7f1304fc

.field public static final applovin_gdpr_learn_more_screen_bullet_2:I = 0x7f1304fd

.field public static final applovin_gdpr_learn_more_screen_bullet_3:I = 0x7f1304fe

.field public static final applovin_gdpr_learn_more_screen_bullet_4:I = 0x7f1304ff

.field public static final applovin_gdpr_learn_more_screen_message_1:I = 0x7f130500

.field public static final applovin_gdpr_learn_more_screen_message_2:I = 0x7f130501

.field public static final applovin_gdpr_main_screen_analytics_purposes_switch_text:I = 0x7f130502

.field public static final applovin_gdpr_main_screen_learn_more_button_text:I = 0x7f130503

.field public static final applovin_gdpr_main_screen_message:I = 0x7f130504

.field public static final applovin_gdpr_main_screen_personalized_advertising_purposes_switch_text:I = 0x7f130505

.field public static final applovin_gdpr_main_screen_pp_and_tos_switch_text:I = 0x7f130506

.field public static final applovin_gdpr_main_screen_privacy_policy_switch_text:I = 0x7f130507

.field public static final applovin_gdpr_main_screen_title:I = 0x7f130508

.field public static final applovin_gdpr_understand_and_continue_button_text:I = 0x7f130509

.field public static final applovin_learn_more_screen_title:I = 0x7f13050a

.field public static final applovin_list_item_image_description:I = 0x7f13050b

.field public static final applovin_pp_and_tos_title:I = 0x7f13050c

.field public static final applovin_pp_title:I = 0x7f13050d

.field public static final applovin_privacy_policy_text:I = 0x7f13050e

.field public static final applovin_terms_of_service_text:I = 0x7f13050f

.field public static final applovin_terms_of_use_text:I = 0x7f130510

.field public static final common_google_play_services_enable_button:I = 0x7f1305a7

.field public static final common_google_play_services_enable_text:I = 0x7f1305a8

.field public static final common_google_play_services_enable_title:I = 0x7f1305a9

.field public static final common_google_play_services_install_button:I = 0x7f1305aa

.field public static final common_google_play_services_install_text:I = 0x7f1305ab

.field public static final common_google_play_services_install_title:I = 0x7f1305ac

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1305ad

.field public static final common_google_play_services_notification_ticker:I = 0x7f1305ae

.field public static final common_google_play_services_unknown_issue:I = 0x7f1305af

.field public static final common_google_play_services_unsupported_text:I = 0x7f1305b0

.field public static final common_google_play_services_update_button:I = 0x7f1305b1

.field public static final common_google_play_services_update_text:I = 0x7f1305b2

.field public static final common_google_play_services_update_title:I = 0x7f1305b3

.field public static final common_google_play_services_updating_text:I = 0x7f1305b4

.field public static final common_google_play_services_wear_update_text:I = 0x7f1305b5

.field public static final common_open_on_phone:I = 0x7f1305b6

.field public static final common_signin_button_text:I = 0x7f1305b7

.field public static final common_signin_button_text_long:I = 0x7f1305b8

.field public static final copy_toast_msg:I = 0x7f1305b9

.field public static final fallback_menu_item_copy_link:I = 0x7f131d25

.field public static final fallback_menu_item_open_in_browser:I = 0x7f131d26

.field public static final fallback_menu_item_share_link:I = 0x7f131d27

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

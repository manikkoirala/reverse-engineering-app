.class public final Lcom/appsflyer/internal/AFb1jSDK;
.super Lcom/appsflyer/internal/AFf1pSDK;
.source ""


# instance fields
.field private final AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

.field private final AFVersionDeclaration:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/appsflyer/internal/AFg1mSDK;",
            ">;"
        }
    .end annotation
.end field

.field private final AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

.field final afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFg1nSDK;

.field afWarnLog:I

.field private final getLevel:Ljava/util/concurrent/CountDownLatch;

.field private final init:Lcom/appsflyer/internal/AFc1tSDK;

.field private final onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1hSDK;

.field private onConversionDataSuccess:I

.field private final onInstallConversionDataLoadedNative:Ljava/util/concurrent/ExecutorService;

.field private final onInstallConversionFailureNative:Lcom/appsflyer/internal/AFb1sSDK;

.field private onResponseErrorNative:Z

.field private onResponseNative:I


# direct methods
.method public constructor <init>(Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 6

    .line 1
    const/4 v1, 0x0

    .line 2
    const-string v2, "https://%sdlsdk.%s/v1.0/android/"

    .line 3
    .line 4
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 5
    .line 6
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    .line 8
    const/4 v5, 0x0

    .line 9
    move-object v0, p0

    .line 10
    invoke-direct/range {v0 .. v5}, Lcom/appsflyer/internal/AFf1pSDK;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFVersionDeclaration:Ljava/util/List;

    .line 19
    .line 20
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->getLevel:Ljava/util/concurrent/CountDownLatch;

    .line 27
    .line 28
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger()Lcom/appsflyer/internal/AFf1tSDK;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 33
    .line 34
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 39
    .line 40
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1hSDK;

    .line 45
    .line 46
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFg1nSDK;

    .line 51
    .line 52
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFKeystoreWrapper()Ljava/util/concurrent/ExecutorService;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onInstallConversionDataLoadedNative:Ljava/util/concurrent/ExecutorService;

    .line 57
    .line 58
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFb1sSDK;

    .line 63
    .line 64
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    iput-object p1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private AFInAppEventParameterName(Lcom/appsflyer/internal/AFa1bSDK;)Ljava/util/Map;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFa1bSDK;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 2
    iget-object v1, p1, Lcom/appsflyer/internal/AFa1bSDK;->AFInAppEventParameterName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3
    iget-object v1, p1, Lcom/appsflyer/internal/AFa1bSDK;->valueOf:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 4
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    .line 5
    new-instance v0, Lcom/appsflyer/internal/AFb1jSDK$1;

    invoke-direct {v0, p0, p1}, Lcom/appsflyer/internal/AFb1jSDK$1;-><init>(Lcom/appsflyer/internal/AFb1jSDK;Lcom/appsflyer/internal/AFa1bSDK;)V

    return-object v0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic AFInAppEventParameterName(Lcom/appsflyer/internal/AFb1jSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFb1jSDK;->afWarnLog()V

    return-void
.end method

.method static synthetic AFInAppEventType(Lcom/appsflyer/internal/AFb1jSDK;)Lcom/appsflyer/deeplink/DeepLinkResult;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 1
    :goto_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 2
    .line 3
    iget v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    const-string v3, "Metrics: Unexpected ddl requestCount = "

    .line 7
    .line 8
    const-string v4, "ddl"

    .line 9
    .line 10
    const-wide/16 v5, 0x0

    .line 11
    .line 12
    if-lez v1, :cond_2

    .line 13
    .line 14
    if-le v1, v2, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 18
    .line 19
    iget-object v7, v0, Lcom/appsflyer/internal/AFf1tSDK;->afInfoLog:[J

    .line 20
    .line 21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 22
    .line 23
    .line 24
    move-result-wide v8

    .line 25
    aput-wide v8, v7, v1

    .line 26
    .line 27
    if-nez v1, :cond_3

    .line 28
    .line 29
    iget-wide v7, v0, Lcom/appsflyer/internal/AFf1tSDK;->afErrorLog:J

    .line 30
    .line 31
    cmp-long v9, v7, v5

    .line 32
    .line 33
    if-eqz v9, :cond_1

    .line 34
    .line 35
    iget-object v9, v0, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 36
    .line 37
    iget-object v10, v0, Lcom/appsflyer/internal/AFf1tSDK;->afInfoLog:[J

    .line 38
    .line 39
    aget-wide v11, v10, v1

    .line 40
    .line 41
    sub-long/2addr v11, v7

    .line 42
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v7, "from_fg"

    .line 47
    .line 48
    invoke-interface {v9, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    iget-object v1, v0, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 52
    .line 53
    new-instance v7, Lorg/json/JSONObject;

    .line 54
    .line 55
    invoke-direct {v7, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 56
    .line 57
    .line 58
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 59
    .line 60
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_1
    const-string v0, "Metrics: fg ts is missing"

    .line 69
    .line 70
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 75
    .line 76
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    const-string v1, "Unexpected ddl requestCount - start"

    .line 88
    .line 89
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 90
    .line 91
    .line 92
    :cond_3
    :goto_2
    new-instance v0, Lcom/appsflyer/internal/AFa1fSDK;

    .line 93
    .line 94
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFb1sSDK;

    .line 95
    .line 96
    invoke-direct {v0, p0, v1}, Lcom/appsflyer/internal/AFa1fSDK;-><init>(Lcom/appsflyer/internal/AFf1pSDK;Lcom/appsflyer/internal/AFb1sSDK;)V

    .line 97
    .line 98
    .line 99
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1hSDK;

    .line 100
    .line 101
    iget-object v1, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFa1fSDK;->valueOf(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 108
    .line 109
    iget v7, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 110
    .line 111
    if-lez v7, :cond_6

    .line 112
    .line 113
    if-le v7, v2, :cond_4

    .line 114
    .line 115
    goto :goto_3

    .line 116
    :cond_4
    add-int/lit8 v7, v7, -0x1

    .line 117
    .line 118
    iget-object v2, v1, Lcom/appsflyer/internal/AFf1tSDK;->afDebugLog:[J

    .line 119
    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 121
    .line 122
    .line 123
    move-result-wide v8

    .line 124
    aput-wide v8, v2, v7

    .line 125
    .line 126
    iget-object v2, v1, Lcom/appsflyer/internal/AFf1tSDK;->afInfoLog:[J

    .line 127
    .line 128
    aget-wide v8, v2, v7

    .line 129
    .line 130
    cmp-long v2, v8, v5

    .line 131
    .line 132
    if-eqz v2, :cond_5

    .line 133
    .line 134
    iget-object v2, v1, Lcom/appsflyer/internal/AFf1tSDK;->afRDLog:[J

    .line 135
    .line 136
    iget-object v3, v1, Lcom/appsflyer/internal/AFf1tSDK;->afDebugLog:[J

    .line 137
    .line 138
    aget-wide v10, v3, v7

    .line 139
    .line 140
    sub-long/2addr v10, v8

    .line 141
    aput-wide v10, v2, v7

    .line 142
    .line 143
    iget-object v3, v1, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 144
    .line 145
    const-string v7, "net"

    .line 146
    .line 147
    invoke-interface {v3, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    .line 149
    .line 150
    iget-object v2, v1, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 151
    .line 152
    new-instance v3, Lorg/json/JSONObject;

    .line 153
    .line 154
    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 155
    .line 156
    .line 157
    iget-object v1, v1, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 158
    .line 159
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v2

    .line 163
    invoke-interface {v1, v4, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    goto :goto_4

    .line 167
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 168
    .line 169
    const-string v2, "Metrics: ddlStart["

    .line 170
    .line 171
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    const-string v2, "] ts is missing"

    .line 178
    .line 179
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    goto :goto_4

    .line 190
    :cond_6
    :goto_3
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 191
    .line 192
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object v2

    .line 196
    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v2

    .line 200
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    const-string v2, "Unexpected ddl requestCount - end"

    .line 204
    .line 205
    invoke-static {v2, v1}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    .line 207
    .line 208
    :goto_4
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 209
    .line 210
    .line 211
    move-result v1

    .line 212
    const/16 v2, 0xc8

    .line 213
    .line 214
    const/4 v3, 0x0

    .line 215
    if-ne v1, v2, :cond_c

    .line 216
    .line 217
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    new-instance v1, Lorg/json/JSONObject;

    .line 222
    .line 223
    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 224
    .line 225
    .line 226
    const-string v0, "is_second_ping"

    .line 227
    .line 228
    const/4 v2, 0x1

    .line 229
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    .line 230
    .line 231
    .line 232
    move-result v0

    .line 233
    iput-boolean v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseErrorNative:Z

    .line 234
    .line 235
    const-string v0, "found"

    .line 236
    .line 237
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    .line 238
    .line 239
    .line 240
    move-result v0

    .line 241
    if-nez v0, :cond_7

    .line 242
    .line 243
    move-object v0, v3

    .line 244
    goto :goto_5

    .line 245
    :cond_7
    const-string v0, "click_event"

    .line 246
    .line 247
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    .line 248
    .line 249
    .line 250
    move-result-object v0

    .line 251
    invoke-static {v0}, Lcom/appsflyer/deeplink/DeepLink;->AFKeystoreWrapper(Lorg/json/JSONObject;)Lcom/appsflyer/deeplink/DeepLink;

    .line 252
    .line 253
    .line 254
    move-result-object v0

    .line 255
    iget-object v1, v0, Lcom/appsflyer/deeplink/DeepLink;->AFKeystoreWrapper:Lorg/json/JSONObject;

    .line 256
    .line 257
    const-string v7, "is_deferred"

    .line 258
    .line 259
    invoke-virtual {v1, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 260
    .line 261
    .line 262
    :goto_5
    if-eqz v0, :cond_8

    .line 263
    .line 264
    new-instance p0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 265
    .line 266
    invoke-direct {p0, v0, v3}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 267
    .line 268
    .line 269
    return-object p0

    .line 270
    :cond_8
    iget v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 271
    .line 272
    if-gt v0, v2, :cond_b

    .line 273
    .line 274
    invoke-direct {p0}, Lcom/appsflyer/internal/AFb1jSDK;->getLevel()Z

    .line 275
    .line 276
    .line 277
    move-result v0

    .line 278
    if-eqz v0, :cond_b

    .line 279
    .line 280
    iget-boolean v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseErrorNative:Z

    .line 281
    .line 282
    if-eqz v0, :cond_b

    .line 283
    .line 284
    const-string v0, "[DDL] Waiting for referrers..."

    .line 285
    .line 286
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 287
    .line 288
    .line 289
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->getLevel:Ljava/util/concurrent/CountDownLatch;

    .line 290
    .line 291
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 292
    .line 293
    .line 294
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 295
    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 297
    .line 298
    .line 299
    move-result-wide v1

    .line 300
    iget-object v7, v0, Lcom/appsflyer/internal/AFf1tSDK;->afDebugLog:[J

    .line 301
    .line 302
    const/4 v8, 0x0

    .line 303
    aget-wide v8, v7, v8

    .line 304
    .line 305
    cmp-long v7, v8, v5

    .line 306
    .line 307
    if-eqz v7, :cond_9

    .line 308
    .line 309
    iget-object v5, v0, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 310
    .line 311
    sub-long/2addr v1, v8

    .line 312
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 313
    .line 314
    .line 315
    move-result-object v1

    .line 316
    const-string v2, "rfr_wait"

    .line 317
    .line 318
    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    .line 320
    .line 321
    iget-object v1, v0, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 322
    .line 323
    new-instance v2, Lorg/json/JSONObject;

    .line 324
    .line 325
    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 326
    .line 327
    .line 328
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 329
    .line 330
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object v1

    .line 334
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    .line 336
    .line 337
    goto :goto_6

    .line 338
    :cond_9
    const-string v0, "Metrics: ddlEnd[0] ts is missing"

    .line 339
    .line 340
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 341
    .line 342
    .line 343
    :goto_6
    iget v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onConversionDataSuccess:I

    .line 344
    .line 345
    iget v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->afWarnLog:I

    .line 346
    .line 347
    if-ne v0, v1, :cond_a

    .line 348
    .line 349
    new-instance p0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 350
    .line 351
    invoke-direct {p0, v3, v3}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 352
    .line 353
    .line 354
    return-object p0

    .line 355
    :cond_a
    invoke-direct {p0}, Lcom/appsflyer/internal/AFb1jSDK;->afWarnLog()V

    .line 356
    .line 357
    .line 358
    goto/16 :goto_0

    .line 359
    .line 360
    :cond_b
    new-instance p0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 361
    .line 362
    invoke-direct {p0, v3, v3}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 363
    .line 364
    .line 365
    return-object p0

    .line 366
    :cond_c
    new-instance p0, Ljava/lang/StringBuilder;

    .line 367
    .line 368
    const-string v1, "[DDL] Error occurred. Server response code = "

    .line 369
    .line 370
    invoke-direct {p0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 374
    .line 375
    .line 376
    move-result v0

    .line 377
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 378
    .line 379
    .line 380
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 381
    .line 382
    .line 383
    move-result-object p0

    .line 384
    invoke-static {p0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 385
    .line 386
    .line 387
    new-instance p0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 388
    .line 389
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->HTTP_STATUS_CODE:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 390
    .line 391
    invoke-direct {p0, v3, v0}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 392
    .line 393
    .line 394
    return-object p0
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private static AFKeystoreWrapper(Lcom/appsflyer/internal/AFg1mSDK;)Z
    .locals 6

    .line 1
    iget-object p0, p0, Lcom/appsflyer/internal/AFg1mSDK;->valueOf:Ljava/util/Map;

    .line 2
    .line 3
    const-string v0, "click_ts"

    .line 4
    .line 5
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    check-cast p0, Ljava/lang/Long;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    if-nez p0, :cond_0

    .line 13
    .line 14
    return v0

    .line 15
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 16
    .line 17
    .line 18
    move-result-wide v1

    .line 19
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 20
    .line 21
    invoke-virtual {p0}, Ljava/lang/Number;->longValue()J

    .line 22
    .line 23
    .line 24
    move-result-wide v4

    .line 25
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    sub-long/2addr v1, v3

    .line 30
    sget-object p0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 31
    .line 32
    const-wide/16 v3, 0x1

    .line 33
    .line 34
    invoke-virtual {p0, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 35
    .line 36
    .line 37
    move-result-wide v3

    .line 38
    cmp-long p0, v1, v3

    .line 39
    .line 40
    if-gez p0, :cond_1

    .line 41
    .line 42
    const/4 p0, 0x1

    .line 43
    return p0

    .line 44
    :cond_1
    return v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private afWarnLog()V
    .locals 6

    .line 1
    iget v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    add-int/2addr v0, v1

    .line 5
    iput v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    const-string v2, "[DDL] Preparing request "

    .line 10
    .line 11
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 15
    .line 16
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 27
    .line 28
    if-ne v0, v1, :cond_3

    .line 29
    .line 30
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iget-object v2, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 35
    .line 36
    iget-object v3, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 37
    .line 38
    iget-object v3, v3, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 39
    .line 40
    const-string v4, "appsFlyerCount"

    .line 41
    .line 42
    const/4 v5, 0x0

    .line 43
    invoke-interface {v3, v4, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    if-nez v3, :cond_0

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/4 v1, 0x0

    .line 51
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const-string v3, "is_first"

    .line 56
    .line 57
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 61
    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v3, "-"

    .line 79
    .line 80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    const-string v3, "lang"

    .line 99
    .line 100
    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 104
    .line 105
    const-string v2, "os"

    .line 106
    .line 107
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 108
    .line 109
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    .line 111
    .line 112
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 113
    .line 114
    const-string v2, "type"

    .line 115
    .line 116
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 117
    .line 118
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 122
    .line 123
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 124
    .line 125
    iget-object v3, v2, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 126
    .line 127
    iget-object v2, v2, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 128
    .line 129
    invoke-static {v3, v2}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    const-string v3, "request_id"

    .line 134
    .line 135
    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    .line 137
    .line 138
    iget-object v0, v0, Lcom/appsflyer/internal/AFa1cSDK;->afDebugLog:Lcom/appsflyer/internal/AFb1aSDK;

    .line 139
    .line 140
    if-eqz v0, :cond_1

    .line 141
    .line 142
    iget-object v0, v0, Lcom/appsflyer/internal/AFb1aSDK;->values:[Ljava/lang/String;

    .line 143
    .line 144
    if-eqz v0, :cond_1

    .line 145
    .line 146
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 147
    .line 148
    const-string v2, "sharing_filter"

    .line 149
    .line 150
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    .line 152
    .line 153
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 154
    .line 155
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 156
    .line 157
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 158
    .line 159
    new-instance v1, Ljava/util/HashMap;

    .line 160
    .line 161
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 162
    .line 163
    .line 164
    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFa1dSDK;->AFKeystoreWrapper(Landroid/content/Context;Ljava/util/Map;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFb1jSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFa1bSDK;)Ljava/util/Map;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 173
    .line 174
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 175
    .line 176
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 177
    .line 178
    invoke-static {v1}, Lcom/appsflyer/internal/AFa1dSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 179
    .line 180
    .line 181
    move-result-object v1

    .line 182
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFb1jSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFa1bSDK;)Ljava/util/Map;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    if-eqz v0, :cond_2

    .line 187
    .line 188
    iget-object v2, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 189
    .line 190
    const-string v3, "gaid"

    .line 191
    .line 192
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    .line 194
    .line 195
    :cond_2
    if-eqz v1, :cond_3

    .line 196
    .line 197
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 198
    .line 199
    const-string v2, "oaid"

    .line 200
    .line 201
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    :cond_3
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 205
    .line 206
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 207
    .line 208
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 209
    .line 210
    const-string v3, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    .line 211
    .line 212
    invoke-direct {v1, v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 213
    .line 214
    .line 215
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 216
    .line 217
    .line 218
    move-result-wide v2

    .line 219
    const-string v4, "UTC"

    .line 220
    .line 221
    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    .line 222
    .line 223
    .line 224
    move-result-object v4

    .line 225
    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 226
    .line 227
    .line 228
    new-instance v4, Ljava/util/Date;

    .line 229
    .line 230
    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 231
    .line 232
    .line 233
    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    const-string v2, "timestamp"

    .line 238
    .line 239
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    .line 241
    .line 242
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 243
    .line 244
    iget v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 245
    .line 246
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 247
    .line 248
    .line 249
    move-result-object v1

    .line 250
    const-string v2, "request_count"

    .line 251
    .line 252
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    .line 254
    .line 255
    new-instance v0, Ljava/util/ArrayList;

    .line 256
    .line 257
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .line 259
    .line 260
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFVersionDeclaration:Ljava/util/List;

    .line 261
    .line 262
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 263
    .line 264
    .line 265
    move-result-object v1

    .line 266
    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 267
    .line 268
    .line 269
    move-result v2

    .line 270
    if-eqz v2, :cond_5

    .line 271
    .line 272
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    check-cast v2, Lcom/appsflyer/internal/AFg1mSDK;

    .line 277
    .line 278
    invoke-static {v2}, Lcom/appsflyer/internal/AFb1jSDK;->values(Lcom/appsflyer/internal/AFg1mSDK;)Ljava/util/Map;

    .line 279
    .line 280
    .line 281
    move-result-object v2

    .line 282
    if-eqz v2, :cond_4

    .line 283
    .line 284
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    .line 286
    .line 287
    goto :goto_1

    .line 288
    :cond_5
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 289
    .line 290
    .line 291
    move-result v1

    .line 292
    if-nez v1, :cond_6

    .line 293
    .line 294
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 295
    .line 296
    const-string v2, "referrers"

    .line 297
    .line 298
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    :cond_6
    invoke-direct {p0}, Lcom/appsflyer/internal/AFb1jSDK;->onInstallConversionDataLoadedNative()Ljava/lang/String;

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    invoke-virtual {p0, v0}, Lcom/appsflyer/internal/AFa1uSDK;->valueOf(Ljava/lang/String;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 306
    .line 307
    .line 308
    return-void
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method private getLevel()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 2
    .line 3
    const-string v1, "referrers"

    .line 4
    .line 5
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Ljava/util/List;

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    iget v3, p0, Lcom/appsflyer/internal/AFb1jSDK;->afWarnLog:I

    .line 21
    .line 22
    if-ge v0, v3, :cond_1

    .line 23
    .line 24
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 25
    .line 26
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_1

    .line 31
    .line 32
    const/4 v0, 0x1

    .line 33
    return v0

    .line 34
    :cond_1
    return v2
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private onInstallConversionDataLoadedNative()Ljava/lang/String;
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFg1kSDK;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->init:Lcom/appsflyer/internal/AFc1tSDK;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/appsflyer/internal/AFg1kSDK;-><init>(Lcom/appsflyer/internal/AFc1tSDK;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1hSDK;

    .line 9
    .line 10
    iget-object v1, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 13
    .line 14
    const-string v3, "timestamp"

    .line 15
    .line 16
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Ljava/lang/String;

    .line 21
    .line 22
    invoke-virtual {v0, v1, v2}, Lcom/appsflyer/internal/AFg1kSDK;->AFInAppEventType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private static values(Lcom/appsflyer/internal/AFg1mSDK;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFg1mSDK;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/appsflyer/internal/AFg1mSDK;->afDebugLog:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 2
    .line 3
    sget-object v1, Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    new-instance v0, Ljava/util/HashMap;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/appsflyer/internal/AFg1mSDK;->valueOf:Ljava/util/Map;

    .line 13
    .line 14
    const-string v2, "referrer"

    .line 15
    .line 16
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Ljava/lang/String;

    .line 21
    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    iget-object p0, p0, Lcom/appsflyer/internal/AFg1mSDK;->valueOf:Ljava/util/Map;

    .line 25
    .line 26
    const-string v2, "source"

    .line 27
    .line 28
    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    check-cast p0, Ljava/lang/String;

    .line 33
    .line 34
    invoke-interface {v0, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    const-string p0, "value"

    .line 38
    .line 39
    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    return-object v0

    .line 43
    :cond_0
    const/4 p0, 0x0

    .line 44
    return-object p0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method


# virtual methods
.method public final AFLogger()Lcom/appsflyer/deeplink/DeepLinkResult;
    .locals 5

    .line 1
    const-string v0, "[DDL] start"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/concurrent/FutureTask;

    .line 7
    .line 8
    new-instance v1, Lcom/appsflyer/internal/AFb1jSDK$4;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/appsflyer/internal/AFb1jSDK$4;-><init>(Lcom/appsflyer/internal/AFb1jSDK;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onInstallConversionDataLoadedNative:Ljava/util/concurrent/ExecutorService;

    .line 17
    .line 18
    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    :try_start_0
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 23
    .line 24
    iget-wide v2, v2, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    .line 25
    .line 26
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 27
    .line 28
    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 35
    .line 36
    iget-object v3, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 37
    .line 38
    iget-wide v3, v3, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    .line 39
    .line 40
    invoke-virtual {v2, v0, v3, v4}, Lcom/appsflyer/internal/AFf1tSDK;->valueOf(Lcom/appsflyer/deeplink/DeepLinkResult;J)V

    .line 41
    .line 42
    .line 43
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 44
    .line 45
    invoke-virtual {v2, v0}, Lcom/appsflyer/internal/AFb1kSDK;->AFKeystoreWrapper(Lcom/appsflyer/deeplink/DeepLinkResult;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    .line 48
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    const-string v2, "[DDL] Timeout"

    .line 51
    .line 52
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    .line 54
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    const-string v2, "[DDL] Timeout, didn\'t manage to find deferred deep link after "

    .line 58
    .line 59
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->onResponseNative:I

    .line 63
    .line 64
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v2, " attempt(s) within "

    .line 68
    .line 69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 73
    .line 74
    iget-wide v2, v2, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    .line 75
    .line 76
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string v2, " milliseconds"

    .line 80
    .line 81
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    new-instance v0, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 92
    .line 93
    sget-object v2, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->TIMEOUT:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 94
    .line 95
    invoke-direct {v0, v1, v2}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 96
    .line 97
    .line 98
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 99
    .line 100
    iget-object v2, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 101
    .line 102
    iget-wide v2, v2, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    .line 103
    .line 104
    invoke-virtual {v1, v0, v2, v3}, Lcom/appsflyer/internal/AFf1tSDK;->valueOf(Lcom/appsflyer/deeplink/DeepLinkResult;J)V

    .line 105
    .line 106
    .line 107
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 108
    .line 109
    invoke-virtual {v1, v0}, Lcom/appsflyer/internal/AFb1kSDK;->AFKeystoreWrapper(Lcom/appsflyer/deeplink/DeepLinkResult;)V

    .line 110
    .line 111
    .line 112
    goto :goto_2

    .line 113
    :catch_1
    move-exception v0

    .line 114
    goto :goto_0

    .line 115
    :catch_2
    move-exception v0

    .line 116
    :goto_0
    const-string v2, "[DDL] Error occurred"

    .line 117
    .line 118
    const/4 v3, 0x1

    .line 119
    invoke-static {v2, v0, v3}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 120
    .line 121
    .line 122
    new-instance v2, Lcom/appsflyer/deeplink/DeepLinkResult;

    .line 123
    .line 124
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    instance-of v0, v0, Ljava/io/IOException;

    .line 129
    .line 130
    if-eqz v0, :cond_0

    .line 131
    .line 132
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->NETWORK:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 133
    .line 134
    goto :goto_1

    .line 135
    :cond_0
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->UNEXPECTED:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 136
    .line 137
    :goto_1
    invoke-direct {v2, v1, v0}, Lcom/appsflyer/deeplink/DeepLinkResult;-><init>(Lcom/appsflyer/deeplink/DeepLink;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 138
    .line 139
    .line 140
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 141
    .line 142
    iget-object v1, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 143
    .line 144
    iget-wide v3, v1, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    .line 145
    .line 146
    invoke-virtual {v0, v2, v3, v4}, Lcom/appsflyer/internal/AFf1tSDK;->valueOf(Lcom/appsflyer/deeplink/DeepLinkResult;J)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AppsFlyer2dXConversionCallback:Lcom/appsflyer/internal/AFb1kSDK;

    .line 150
    .line 151
    invoke-virtual {v0, v2}, Lcom/appsflyer/internal/AFb1kSDK;->AFKeystoreWrapper(Lcom/appsflyer/deeplink/DeepLinkResult;)V

    .line 152
    .line 153
    .line 154
    move-object v0, v2

    .line 155
    :goto_2
    return-object v0
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public final valueOf()Lcom/appsflyer/internal/AFd1eSDK;
    .locals 1

    .line 7
    sget-object v0, Lcom/appsflyer/internal/AFd1eSDK;->afRDLog:Lcom/appsflyer/internal/AFd1eSDK;

    return-object v0
.end method

.method final valueOf(Lcom/appsflyer/internal/AFg1mSDK;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/appsflyer/internal/AFb1jSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFg1mSDK;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->AFVersionDeclaration:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    iget-object v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->getLevel:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[DDL] Added non-organic "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    return-void

    .line 5
    :cond_0
    iget p1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onConversionDataSuccess:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/appsflyer/internal/AFb1jSDK;->onConversionDataSuccess:I

    iget v0, p0, Lcom/appsflyer/internal/AFb1jSDK;->afWarnLog:I

    if-ne p1, v0, :cond_1

    .line 6
    iget-object p1, p0, Lcom/appsflyer/internal/AFb1jSDK;->getLevel:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_1
    return-void
.end method

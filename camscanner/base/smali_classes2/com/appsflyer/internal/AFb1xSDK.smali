.class public interface abstract Lcom/appsflyer/internal/AFb1xSDK;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract AFKeystoreWrapper()V
.end method

.method public abstract valueOf()V
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract valueOf(Ljava/lang/String;)Z
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract values(Lcom/appsflyer/internal/AFb1ySDK;)Ljava/lang/String;
    .param p1    # Lcom/appsflyer/internal/AFb1ySDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation
.end method

.method public abstract values()Ljava/util/List;
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/appsflyer/internal/AFb1ySDK;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.class public final Lcom/appsflyer/internal/AFc1iSDK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/appsflyer/internal/AFc1jSDK;


# static fields
.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field private static AFLogger$LogLevel:I = 0x0

.field private static afDebugLog:J = 0x1c9d48781a868687L

.field private static afErrorLogForExcManagerOnly:I = 0x1


# instance fields
.field private final AFInAppEventParameterName:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFInAppEventType:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFKeystoreWrapper:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFLogger:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afErrorLog:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afInfoLog:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afRDLog:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final valueOf:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private values:Lcom/appsflyer/internal/AFc1qSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>(Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 1
    .param p1    # Lcom/appsflyer/internal/AFc1qSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->values:Lcom/appsflyer/internal/AFc1qSDK;

    .line 10
    .line 11
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$5;

    .line 12
    .line 13
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$5;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventType:Lkotlin/Lazy;

    .line 21
    .line 22
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$1;

    .line 23
    .line 24
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$1;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 25
    .line 26
    .line 27
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->valueOf:Lkotlin/Lazy;

    .line 32
    .line 33
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$4;

    .line 34
    .line 35
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$4;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 36
    .line 37
    .line 38
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper:Lkotlin/Lazy;

    .line 43
    .line 44
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$10;

    .line 45
    .line 46
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$10;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 47
    .line 48
    .line 49
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventParameterName:Lkotlin/Lazy;

    .line 54
    .line 55
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$3;

    .line 56
    .line 57
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$3;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 58
    .line 59
    .line 60
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->afInfoLog:Lkotlin/Lazy;

    .line 65
    .line 66
    const-string p1, "6.12.4"

    .line 67
    .line 68
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog:Ljava/lang/String;

    .line 69
    .line 70
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$2;

    .line 71
    .line 72
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$2;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 73
    .line 74
    .line 75
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLog:Lkotlin/Lazy;

    .line 80
    .line 81
    new-instance p1, Lcom/appsflyer/internal/AFc1iSDK$7;

    .line 82
    .line 83
    invoke-direct {p1, p0}, Lcom/appsflyer/internal/AFc1iSDK$7;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 84
    .line 85
    .line 86
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger:Lkotlin/Lazy;

    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private static final AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 4

    .line 2
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x53

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x38

    if-nez v0, :cond_0

    const/16 v0, 0x38

    goto :goto_0

    :cond_0
    const/16 v0, 0x30

    :goto_0
    const/4 v2, 0x0

    const-string v3, ""

    if-eq v0, v1, :cond_3

    .line 3
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel()V

    sget p0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p0, p0, 0x23

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p0, p0, 0x2

    const/4 v0, 0x1

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    if-eq p0, v0, :cond_2

    return-void

    :cond_2
    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :cond_3
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel()V

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p0

    throw p0
.end method

.method private final AFInAppEventParameterName(Lcom/appsflyer/internal/AFf1jSDK;)Z
    .locals 12

    .line 5
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x31

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 7
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v2

    const-string v3, "af_send_exc_to_server_window"

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    move-result-wide v2

    .line 8
    iget-wide v6, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventType:J

    .line 9
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    const/4 v10, 0x0

    cmp-long v11, v6, v8

    if-gez v11, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    const/4 v6, 0x1

    :goto_0
    if-eqz v6, :cond_5

    const/16 v6, 0x5c

    cmp-long v7, v2, v4

    if-eqz v7, :cond_1

    const/16 v4, 0x5c

    goto :goto_1

    :cond_1
    const/16 v4, 0x59

    :goto_1
    if-eq v4, v6, :cond_2

    goto :goto_2

    .line 10
    :cond_2
    sget v4, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v4, v4, 0x75

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v4, v4, 0x2

    cmp-long v4, v2, v0

    if-gez v4, :cond_3

    :goto_2
    return v10

    .line 11
    :cond_3
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v0

    const-string v1, "af_send_exc_min"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_5

    .line 12
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1mSDK;->AFKeystoreWrapper()I

    move-result v1

    if-ge v1, v0, :cond_4

    goto :goto_3

    .line 13
    :cond_4
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFf1jSDK;)Z

    move-result p1

    return p1

    :cond_5
    :goto_3
    return v10
.end method

.method public static final synthetic AFInAppEventType(Lcom/appsflyer/internal/AFc1iSDK;)Lcom/appsflyer/internal/AFc1qSDK;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x1b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    iget-object p0, p0, Lcom/appsflyer/internal/AFc1iSDK;->values:Lcom/appsflyer/internal/AFc1qSDK;

    add-int/lit8 v1, v1, 0x59

    rem-int/lit16 v0, v1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v1, v1, 0x2

    return-object p0
.end method

.method private final AFInAppEventType(Lcom/appsflyer/internal/AFf1jSDK;)V
    .locals 6

    .line 3
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x23

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    .line 4
    iget v0, p1, Lcom/appsflyer/internal/AFf1jSDK;->valueOf:I

    .line 5
    iget p1, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventParameterName:I

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 7
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object p1

    const-string v3, "af_send_exc_to_server_window"

    .line 8
    invoke-interface {p1, v3, v1, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;J)V

    const-string v1, "af_send_exc_min"

    .line 9
    invoke-interface {p1, v1, v0}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventType(Ljava/lang/String;I)V

    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0x2d

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method private static final AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 2

    .line 11
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x9

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 12
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly()V

    sget p0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 p0, p0, 0x49

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 p0, p0, 0x2

    return-void
.end method

.method private static final AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1iSDK;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .line 5
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x7

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, ""

    if-eqz v0, :cond_0

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    move-result-object v0

    const/16 v3, 0x4a

    :try_start_0
    div-int/2addr v3, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 7
    throw p0

    .line 8
    :cond_0
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    move-result-object v0

    if-eqz v0, :cond_3

    :goto_0
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFc1iSDK;->valueOf(Lcom/appsflyer/internal/AFf1jSDK;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_4

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_5

    .line 10
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x5d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    move-result-object p0

    invoke-interface {p0, p1, p2}, Lcom/appsflyer/internal/AFc1mSDK;->AFKeystoreWrapper(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    :cond_5
    return-void
.end method

.method private final AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 26
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x33

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    .line 27
    sget-object v0, Lkotlin/text/Charsets;->〇o00〇〇Oo:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p1, p2}, Lcom/appsflyer/internal/AFb1wSDK;->valueOf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "Authorization"

    .line 29
    invoke-static {p2, p1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->Oo08(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 30
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afWarnLog()Lcom/appsflyer/internal/AFc1lSDK;

    move-result-object p2

    const/16 v1, 0x7d0

    .line 31
    invoke-interface {p2, v0, p1, v1}, Lcom/appsflyer/internal/AFc1lSDK;->valueOf([BLjava/util/Map;I)V

    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 p1, p1, 0x59

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method private final AFKeystoreWrapper(Lcom/appsflyer/internal/AFf1jSDK;)Z
    .locals 6

    .line 14
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLog()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/appsflyer/internal/AFc1gSDK;->AFInAppEventType(Ljava/lang/String;)I

    move-result v0

    .line 15
    iget-object v1, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    const-string v2, ""

    .line 16
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/appsflyer/internal/AFc1gSDK;->AFInAppEventType(Ljava/lang/String;)I

    move-result v1

    .line 17
    iget-object v3, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 18
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/appsflyer/internal/AFc1gSDK;->AFInAppEventParameterName(Ljava/lang/String;)Lkotlin/Pair;

    move-result-object v3

    .line 19
    iget-object p1, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 20
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/appsflyer/internal/AFc1gSDK;->AFKeystoreWrapper(Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p1

    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_0
    if-eq v2, v4, :cond_3

    if-nez v3, :cond_3

    if-ne v1, v0, :cond_2

    .line 21
    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0x11

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_1

    const/4 v4, 0x0

    :cond_1
    return v4

    :cond_2
    return v5

    :cond_3
    if-eqz p1, :cond_9

    .line 22
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    const/16 v2, 0x32

    if-gt v1, v0, :cond_4

    const/16 v1, 0x32

    goto :goto_1

    :cond_4
    const/16 v1, 0x45

    :goto_1
    if-eq v1, v2, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    if-gt v0, p1, :cond_6

    return v4

    .line 23
    :cond_6
    :goto_2
    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0x1b

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p1, p1, 0x2

    const/16 v0, 0x53

    if-nez p1, :cond_7

    const/16 p1, 0x53

    goto :goto_3

    :cond_7
    const/16 p1, 0x29

    :goto_3
    if-eq p1, v0, :cond_8

    return v5

    :cond_8
    const/16 p1, 0x3c

    :try_start_0
    div-int/2addr p1, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v5

    :catchall_0
    move-exception p1

    throw p1

    :cond_9
    if-eqz v3, :cond_c

    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0x59

    rem-int/lit16 v1, p1, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_b

    .line 24
    invoke-virtual {v3}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    if-gt p1, v0, :cond_a

    invoke-virtual {v3}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    if-gt v0, p1, :cond_a

    return v4

    :cond_a
    return v5

    :cond_b
    invoke-virtual {v3}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    const/4 p1, 0x0

    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    .line 25
    throw p1

    :cond_c
    return v5
.end method

.method private final AFLogger()Ljava/util/concurrent/ExecutorService;
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x2f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->afInfoLog:Lkotlin/Lazy;

    .line 12
    .line 13
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Ljava/util/concurrent/ExecutorService;

    .line 18
    .line 19
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 20
    .line 21
    add-int/lit8 v1, v1, 0x53

    .line 22
    .line 23
    rem-int/lit16 v2, v1, 0x80

    .line 24
    .line 25
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 26
    .line 27
    rem-int/lit8 v1, v1, 0x2

    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private final declared-synchronized AFLogger$LogLevel()V
    .locals 8

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    if-eqz v0, :cond_6

    .line 7
    .line 8
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 9
    .line 10
    add-int/lit8 v2, v1, 0x6f

    .line 11
    .line 12
    rem-int/lit16 v3, v2, 0x80

    .line 13
    .line 14
    sput v3, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 15
    .line 16
    rem-int/lit8 v2, v2, 0x2

    .line 17
    .line 18
    iget v2, v0, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventParameterName:I

    .line 19
    .line 20
    const/4 v3, -0x1

    .line 21
    const/4 v4, 0x0

    .line 22
    const/4 v5, 0x1

    .line 23
    if-ne v2, v3, :cond_0

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v2, 0x1

    .line 28
    :goto_0
    if-eq v2, v5, :cond_3

    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x71

    .line 31
    .line 32
    rem-int/lit16 v0, v1, 0x80

    .line 33
    .line 34
    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 35
    .line 36
    rem-int/lit8 v1, v1, 0x2

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const-string v1, "af_send_exc_to_server_window"

    .line 43
    .line 44
    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 48
    .line 49
    add-int/lit8 v0, v0, 0x35

    .line 50
    .line 51
    rem-int/lit16 v1, v0, 0x80

    .line 52
    .line 53
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 54
    .line 55
    rem-int/lit8 v0, v0, 0x2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    if-nez v0, :cond_1

    .line 60
    .line 61
    const/16 v0, 0x2b

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_1
    const/16 v0, 0xa

    .line 65
    .line 66
    :goto_1
    if-eq v0, v1, :cond_2

    .line 67
    .line 68
    const/16 v0, 0x13

    .line 69
    .line 70
    :try_start_1
    div-int/2addr v0, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    monitor-exit p0

    .line 72
    return-void

    .line 73
    :catchall_0
    move-exception v0

    .line 74
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75
    :cond_2
    monitor-exit p0

    .line 76
    return-void

    .line 77
    :cond_3
    :try_start_3
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    const-string v2, "af_send_exc_to_server_window"

    .line 82
    .line 83
    const-wide/16 v6, -0x1

    .line 84
    .line 85
    invoke-interface {v1, v2, v6, v7}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    .line 86
    .line 87
    .line 88
    move-result-wide v1

    .line 89
    cmp-long v3, v1, v6

    .line 90
    .line 91
    if-nez v3, :cond_4

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_4
    const/4 v4, 0x1

    .line 95
    :goto_2
    if-eqz v4, :cond_5

    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_5
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFf1jSDK;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 99
    .line 100
    .line 101
    :goto_3
    monitor-exit p0

    .line 102
    return-void

    .line 103
    :cond_6
    monitor-exit p0

    .line 104
    return-void

    .line 105
    :catchall_1
    move-exception v0

    .line 106
    monitor-exit p0

    .line 107
    throw v0
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private final declared-synchronized AFVersionDeclaration()V
    .locals 8

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 9
    .line 10
    add-int/lit8 v1, v1, 0x59

    .line 11
    .line 12
    rem-int/lit16 v2, v1, 0x80

    .line 13
    .line 14
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 15
    .line 16
    rem-int/lit8 v1, v1, 0x2

    .line 17
    .line 18
    iget-wide v0, v0, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventType:J

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-wide/16 v0, -0x1

    .line 22
    .line 23
    :goto_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 24
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    .line 30
    .line 31
    .line 32
    move-result-wide v2

    .line 33
    const/4 v4, 0x0

    .line 34
    cmp-long v5, v0, v2

    .line 35
    .line 36
    if-gez v5, :cond_2

    .line 37
    .line 38
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 39
    .line 40
    add-int/lit8 v0, v0, 0xf

    .line 41
    .line 42
    rem-int/lit16 v1, v0, 0x80

    .line 43
    .line 44
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 45
    .line 46
    rem-int/lit8 v0, v0, 0x2

    .line 47
    .line 48
    if-nez v0, :cond_1

    .line 49
    .line 50
    const-string v0, "TTL is already passed"

    .line 51
    .line 52
    const-string v1, ""

    .line 53
    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    const-string v1, "[Exception Manager]: "

    .line 58
    .line 59
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    const-string v1, "af_send_exc_to_server_window"

    .line 71
    .line 72
    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1mSDK;->valueOf()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 80
    .line 81
    .line 82
    monitor-exit p0

    .line 83
    return-void

    .line 84
    :cond_1
    :try_start_1
    const-string v0, "TTL is already passed"

    .line 85
    .line 86
    const-string v1, ""

    .line 87
    .line 88
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    const-string v1, "[Exception Manager]: "

    .line 92
    .line 93
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "af_send_exc_to_server_window"

    .line 105
    .line 106
    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1mSDK;->valueOf()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 114
    .line 115
    .line 116
    :try_start_2
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    .line 118
    :try_start_3
    throw v0

    .line 119
    :cond_2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    const/4 v1, 0x0

    .line 124
    const/4 v2, 0x1

    .line 125
    if-eqz v0, :cond_4

    .line 126
    .line 127
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFf1jSDK;)Z

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    const/16 v3, 0x1d

    .line 132
    .line 133
    if-ne v0, v2, :cond_3

    .line 134
    .line 135
    const/16 v0, 0x3f

    .line 136
    .line 137
    goto :goto_1

    .line 138
    :cond_3
    const/16 v0, 0x1d

    .line 139
    .line 140
    :goto_1
    if-eq v0, v3, :cond_4

    .line 141
    .line 142
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 143
    .line 144
    add-int/lit8 v0, v0, 0x61

    .line 145
    .line 146
    rem-int/lit16 v3, v0, 0x80

    .line 147
    .line 148
    sput v3, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 149
    .line 150
    rem-int/lit8 v0, v0, 0x2

    .line 151
    .line 152
    const/4 v0, 0x1

    .line 153
    goto :goto_2

    .line 154
    :cond_4
    const/4 v0, 0x0

    .line 155
    :goto_2
    if-eqz v0, :cond_14

    .line 156
    .line 157
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 158
    .line 159
    add-int/lit8 v0, v0, 0x6b

    .line 160
    .line 161
    rem-int/lit16 v3, v0, 0x80

    .line 162
    .line 163
    sput v3, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 164
    .line 165
    rem-int/lit8 v0, v0, 0x2

    .line 166
    .line 167
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    if-eqz v0, :cond_7

    .line 172
    .line 173
    sget v3, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 174
    .line 175
    add-int/lit8 v3, v3, 0x29

    .line 176
    .line 177
    rem-int/lit16 v5, v3, 0x80

    .line 178
    .line 179
    sput v5, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 180
    .line 181
    rem-int/lit8 v3, v3, 0x2

    .line 182
    .line 183
    if-nez v3, :cond_5

    .line 184
    .line 185
    const/4 v3, 0x1

    .line 186
    goto :goto_3

    .line 187
    :cond_5
    const/4 v3, 0x0

    .line 188
    :goto_3
    if-eq v3, v2, :cond_6

    .line 189
    .line 190
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 191
    .line 192
    if-eqz v0, :cond_7

    .line 193
    .line 194
    invoke-static {v0}, Lcom/appsflyer/internal/AFc1gSDK;->AFInAppEventType(Ljava/lang/String;)I

    .line 195
    .line 196
    .line 197
    move-result v0

    .line 198
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 199
    .line 200
    .line 201
    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 202
    goto :goto_4

    .line 203
    :cond_6
    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 204
    :catchall_1
    move-exception v0

    .line 205
    :try_start_5
    throw v0

    .line 206
    :cond_7
    move-object v0, v4

    .line 207
    :goto_4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 208
    .line 209
    .line 210
    move-result-object v3

    .line 211
    if-eqz v3, :cond_8

    .line 212
    .line 213
    const/4 v5, 0x1

    .line 214
    goto :goto_5

    .line 215
    :cond_8
    const/4 v5, 0x0

    .line 216
    :goto_5
    if-eqz v5, :cond_b

    .line 217
    .line 218
    iget-object v3, v3, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 219
    .line 220
    const/16 v5, 0x25

    .line 221
    .line 222
    if-eqz v3, :cond_9

    .line 223
    .line 224
    const/16 v6, 0x25

    .line 225
    .line 226
    goto :goto_6

    .line 227
    :cond_9
    const/16 v6, 0x12

    .line 228
    .line 229
    :goto_6
    if-eq v6, v5, :cond_a

    .line 230
    .line 231
    goto :goto_7

    .line 232
    :cond_a
    invoke-static {v3}, Lcom/appsflyer/internal/AFc1gSDK;->AFInAppEventParameterName(Ljava/lang/String;)Lkotlin/Pair;

    .line 233
    .line 234
    .line 235
    move-result-object v3

    .line 236
    goto :goto_8

    .line 237
    :cond_b
    :goto_7
    sget v3, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 238
    .line 239
    add-int/lit8 v3, v3, 0x2f

    .line 240
    .line 241
    rem-int/lit16 v5, v3, 0x80

    .line 242
    .line 243
    sput v5, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 244
    .line 245
    rem-int/lit8 v3, v3, 0x2

    .line 246
    .line 247
    move-object v3, v4

    .line 248
    :goto_8
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 249
    .line 250
    .line 251
    move-result-object v5

    .line 252
    const/16 v6, 0x5f

    .line 253
    .line 254
    if-eqz v5, :cond_c

    .line 255
    .line 256
    const/16 v7, 0x5f

    .line 257
    .line 258
    goto :goto_9

    .line 259
    :cond_c
    const/16 v7, 0x34

    .line 260
    .line 261
    :goto_9
    if-eq v7, v6, :cond_d

    .line 262
    .line 263
    goto :goto_a

    .line 264
    :cond_d
    iget-object v5, v5, Lcom/appsflyer/internal/AFf1jSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 265
    .line 266
    if-eqz v5, :cond_e

    .line 267
    .line 268
    invoke-static {v5}, Lcom/appsflyer/internal/AFc1gSDK;->AFKeystoreWrapper(Ljava/lang/String;)Lkotlin/Pair;

    .line 269
    .line 270
    .line 271
    move-result-object v4

    .line 272
    :cond_e
    :goto_a
    if-nez v0, :cond_f

    .line 273
    .line 274
    goto :goto_b

    .line 275
    :cond_f
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 276
    .line 277
    .line 278
    move-result v0

    .line 279
    const/4 v5, -0x1

    .line 280
    if-eq v0, v5, :cond_10

    .line 281
    .line 282
    :goto_b
    if-nez v3, :cond_10

    .line 283
    .line 284
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 285
    .line 286
    .line 287
    move-result-object v0

    .line 288
    new-array v2, v2, [Ljava/lang/String;

    .line 289
    .line 290
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLog()Ljava/lang/String;

    .line 291
    .line 292
    .line 293
    move-result-object v3

    .line 294
    aput-object v3, v2, v1

    .line 295
    .line 296
    invoke-interface {v0, v2}, Lcom/appsflyer/internal/AFc1mSDK;->valueOf([Ljava/lang/String;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 297
    .line 298
    .line 299
    monitor-exit p0

    .line 300
    return-void

    .line 301
    :cond_10
    if-eqz v3, :cond_11

    .line 302
    .line 303
    :try_start_6
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 304
    .line 305
    .line 306
    move-result-object v0

    .line 307
    invoke-virtual {v3}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 308
    .line 309
    .line 310
    move-result-object v1

    .line 311
    check-cast v1, Ljava/lang/Number;

    .line 312
    .line 313
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 314
    .line 315
    .line 316
    move-result v1

    .line 317
    invoke-virtual {v3}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 318
    .line 319
    .line 320
    move-result-object v2

    .line 321
    check-cast v2, Ljava/lang/Number;

    .line 322
    .line 323
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 324
    .line 325
    .line 326
    move-result v2

    .line 327
    invoke-interface {v0, v1, v2}, Lcom/appsflyer/internal/AFc1mSDK;->AFKeystoreWrapper(II)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 328
    .line 329
    .line 330
    monitor-exit p0

    .line 331
    return-void

    .line 332
    :cond_11
    if-eqz v4, :cond_12

    .line 333
    .line 334
    const/16 v0, 0x5f

    .line 335
    .line 336
    goto :goto_c

    .line 337
    :cond_12
    const/16 v0, 0x38

    .line 338
    .line 339
    :goto_c
    if-eq v0, v6, :cond_13

    .line 340
    .line 341
    :try_start_7
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 342
    .line 343
    .line 344
    move-result-object v0

    .line 345
    const-string v1, "af_send_exc_to_server_window"

    .line 346
    .line 347
    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 348
    .line 349
    .line 350
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 351
    .line 352
    .line 353
    move-result-object v0

    .line 354
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1mSDK;->valueOf()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 355
    .line 356
    .line 357
    monitor-exit p0

    .line 358
    return-void

    .line 359
    :cond_13
    :try_start_8
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 360
    .line 361
    .line 362
    move-result-object v0

    .line 363
    invoke-virtual {v4}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 364
    .line 365
    .line 366
    move-result-object v1

    .line 367
    check-cast v1, Ljava/lang/Number;

    .line 368
    .line 369
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 370
    .line 371
    .line 372
    move-result v1

    .line 373
    invoke-virtual {v4}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 374
    .line 375
    .line 376
    move-result-object v2

    .line 377
    check-cast v2, Ljava/lang/Number;

    .line 378
    .line 379
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 380
    .line 381
    .line 382
    move-result v2

    .line 383
    invoke-interface {v0, v1, v2}, Lcom/appsflyer/internal/AFc1mSDK;->AFKeystoreWrapper(II)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 384
    .line 385
    .line 386
    monitor-exit p0

    .line 387
    return-void

    .line 388
    :cond_14
    :try_start_9
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    .line 389
    .line 390
    .line 391
    move-result-object v0

    .line 392
    const-string v1, "af_send_exc_to_server_window"

    .line 393
    .line 394
    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 395
    .line 396
    .line 397
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 398
    .line 399
    .line 400
    move-result-object v0

    .line 401
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1mSDK;->valueOf()Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 402
    .line 403
    .line 404
    monitor-exit p0

    .line 405
    return-void

    .line 406
    :catchall_2
    move-exception v0

    .line 407
    monitor-exit p0

    .line 408
    throw v0
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method public static synthetic O8(Lcom/appsflyer/internal/AFc1iSDK;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1iSDK;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private static a(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 11

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->$10:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->$11:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v0, 0x40

    .line 12
    .line 13
    if-eqz p0, :cond_0

    .line 14
    .line 15
    const/16 v2, 0x27

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v2, 0x40

    .line 19
    .line 20
    :goto_0
    if-eq v2, v0, :cond_1

    .line 21
    .line 22
    add-int/lit8 v1, v1, 0x3b

    .line 23
    .line 24
    rem-int/lit16 v0, v1, 0x80

    .line 25
    .line 26
    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->$10:I

    .line 27
    .line 28
    rem-int/lit8 v1, v1, 0x2

    .line 29
    .line 30
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->$11:I

    .line 35
    .line 36
    add-int/lit8 v0, v0, 0x3f

    .line 37
    .line 38
    rem-int/lit16 v1, v0, 0x80

    .line 39
    .line 40
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->$10:I

    .line 41
    .line 42
    rem-int/lit8 v0, v0, 0x2

    .line 43
    .line 44
    :cond_1
    check-cast p0, [C

    .line 45
    .line 46
    new-instance v0, Lcom/appsflyer/internal/AFg1aSDK;

    .line 47
    .line 48
    invoke-direct {v0}, Lcom/appsflyer/internal/AFg1aSDK;-><init>()V

    .line 49
    .line 50
    .line 51
    sget-wide v1, Lcom/appsflyer/internal/AFc1iSDK;->afDebugLog:J

    .line 52
    .line 53
    const-wide v3, 0x40a9871c7f8b3f1dL    # 3267.5556605829756

    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    xor-long/2addr v1, v3

    .line 59
    invoke-static {v1, v2, p0, p1}, Lcom/appsflyer/internal/AFg1aSDK;->AFKeystoreWrapper(J[CI)[C

    .line 60
    .line 61
    .line 62
    move-result-object p0

    .line 63
    const/4 p1, 0x4

    .line 64
    iput p1, v0, Lcom/appsflyer/internal/AFg1aSDK;->AFKeystoreWrapper:I

    .line 65
    .line 66
    :goto_1
    iget v1, v0, Lcom/appsflyer/internal/AFg1aSDK;->AFKeystoreWrapper:I

    .line 67
    .line 68
    array-length v2, p0

    .line 69
    const/16 v5, 0x5b

    .line 70
    .line 71
    if-ge v1, v2, :cond_2

    .line 72
    .line 73
    const/16 v2, 0x5b

    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_2
    const/16 v2, 0x25

    .line 77
    .line 78
    :goto_2
    if-eq v2, v5, :cond_3

    .line 79
    .line 80
    new-instance v0, Ljava/lang/String;

    .line 81
    .line 82
    array-length v1, p0

    .line 83
    sub-int/2addr v1, p1

    .line 84
    invoke-direct {v0, p0, p1, v1}, Ljava/lang/String;-><init>([CII)V

    .line 85
    .line 86
    .line 87
    const/4 p0, 0x0

    .line 88
    aput-object v0, p2, p0

    .line 89
    .line 90
    return-void

    .line 91
    :cond_3
    sget v2, Lcom/appsflyer/internal/AFc1iSDK;->$11:I

    .line 92
    .line 93
    add-int/lit8 v2, v2, 0x6f

    .line 94
    .line 95
    rem-int/lit16 v5, v2, 0x80

    .line 96
    .line 97
    sput v5, Lcom/appsflyer/internal/AFc1iSDK;->$10:I

    .line 98
    .line 99
    rem-int/lit8 v2, v2, 0x2

    .line 100
    .line 101
    add-int/lit8 v2, v1, -0x4

    .line 102
    .line 103
    iput v2, v0, Lcom/appsflyer/internal/AFg1aSDK;->AFInAppEventParameterName:I

    .line 104
    .line 105
    aget-char v5, p0, v1

    .line 106
    .line 107
    rem-int/lit8 v6, v1, 0x4

    .line 108
    .line 109
    aget-char v6, p0, v6

    .line 110
    .line 111
    xor-int/2addr v5, v6

    .line 112
    int-to-long v5, v5

    .line 113
    int-to-long v7, v2

    .line 114
    sget-wide v9, Lcom/appsflyer/internal/AFc1iSDK;->afDebugLog:J

    .line 115
    .line 116
    xor-long/2addr v9, v3

    .line 117
    mul-long v7, v7, v9

    .line 118
    .line 119
    xor-long/2addr v5, v7

    .line 120
    long-to-int v2, v5

    .line 121
    int-to-char v2, v2

    .line 122
    aput-char v2, p0, v1

    .line 123
    .line 124
    add-int/lit8 v1, v1, 0x1

    .line 125
    .line 126
    iput v1, v0, Lcom/appsflyer/internal/AFg1aSDK;->AFKeystoreWrapper:I

    .line 127
    .line 128
    goto :goto_1
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private final afDebugLog()Lcom/appsflyer/internal/AFe1hSDK;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x15

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0xd

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x12

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0xd

    .line 19
    .line 20
    :goto_0
    if-ne v0, v1, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventParameterName:Lkotlin/Lazy;

    .line 23
    .line 24
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Lcom/appsflyer/internal/AFe1hSDK;

    .line 29
    .line 30
    return-object v0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventParameterName:Lkotlin/Lazy;

    .line 32
    .line 33
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, Lcom/appsflyer/internal/AFe1hSDK;

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :catchall_0
    move-exception v0

    .line 42
    throw v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private afErrorLog()Ljava/lang/String;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x31

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    const/4 v3, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog:Ljava/lang/String;

    .line 21
    .line 22
    const/16 v4, 0x41

    .line 23
    .line 24
    :try_start_0
    div-int/2addr v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    goto :goto_1

    .line 26
    :catchall_0
    move-exception v0

    .line 27
    throw v0

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog:Ljava/lang/String;

    .line 29
    .line 30
    :goto_1
    add-int/lit8 v1, v1, 0x1d

    .line 31
    .line 32
    rem-int/lit16 v4, v1, 0x80

    .line 33
    .line 34
    sput v4, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 35
    .line 36
    rem-int/lit8 v1, v1, 0x2

    .line 37
    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    const/4 v1, 0x0

    .line 41
    goto :goto_2

    .line 42
    :cond_2
    const/4 v1, 0x1

    .line 43
    :goto_2
    if-eq v1, v2, :cond_3

    .line 44
    .line 45
    const/4 v1, 0x4

    .line 46
    :try_start_1
    div-int/2addr v1, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 47
    return-object v0

    .line 48
    :catchall_1
    move-exception v0

    .line 49
    throw v0

    .line 50
    :cond_3
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private final afErrorLogForExcManagerOnly()V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x21

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    if-eqz v0, :cond_4

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_3

    .line 18
    .line 19
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFf1jSDK;)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/16 v2, 0x49

    .line 24
    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    const/16 v1, 0x49

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/16 v1, 0x12

    .line 31
    .line 32
    :goto_0
    const-string v3, ""

    .line 33
    .line 34
    if-eq v1, v2, :cond_1

    .line 35
    .line 36
    const-string v0, "skipping"

    .line 37
    .line 38
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const-string v1, "[Exception Manager]: "

    .line 42
    .line 43
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_1
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 52
    .line 53
    add-int/lit8 v1, v1, 0x1d

    .line 54
    .line 55
    rem-int/lit16 v2, v1, 0x80

    .line 56
    .line 57
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 58
    .line 59
    rem-int/lit8 v1, v1, 0x2

    .line 60
    .line 61
    if-eqz v1, :cond_2

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afDebugLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    iget-object v1, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 68
    .line 69
    const/16 v2, 0x24

    .line 70
    .line 71
    :try_start_0
    div-int/lit8 v2, v2, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .line 73
    if-eqz v1, :cond_3

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :catchall_0
    move-exception v0

    .line 77
    throw v0

    .line 78
    :cond_2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afDebugLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    iget-object v1, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 83
    .line 84
    if-eqz v1, :cond_3

    .line 85
    .line 86
    :goto_1
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFc1iSDK;->values(Lcom/appsflyer/internal/AFf1jSDK;)Ljava/util/Map;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-interface {v2}, Lcom/appsflyer/internal/AFc1mSDK;->AFInAppEventParameterName()Ljava/util/List;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    invoke-static {v0, v2}, Lcom/appsflyer/internal/AFc1iSDK;->values(Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    new-instance v2, Lorg/json/JSONObject;

    .line 103
    .line 104
    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    invoke-direct {p0, v0, v1}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    :cond_3
    return-void

    .line 121
    :cond_4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->getLevel()Lcom/appsflyer/internal/AFf1jSDK;

    .line 122
    .line 123
    .line 124
    const/4 v0, 0x0

    .line 125
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126
    :catchall_1
    move-exception v0

    .line 127
    throw v0
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private final afInfoLog()Lcom/appsflyer/internal/AFc1tSDK;
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x7

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eq v0, v1, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->valueOf:Lkotlin/Lazy;

    .line 21
    .line 22
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Lcom/appsflyer/internal/AFc1tSDK;

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->valueOf:Lkotlin/Lazy;

    .line 30
    .line 31
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/appsflyer/internal/AFc1tSDK;

    .line 36
    .line 37
    const/4 v1, 0x5

    .line 38
    :try_start_0
    div-int/2addr v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :goto_1
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 40
    .line 41
    add-int/lit8 v1, v1, 0xb

    .line 42
    .line 43
    rem-int/lit16 v2, v1, 0x80

    .line 44
    .line 45
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 46
    .line 47
    rem-int/lit8 v1, v1, 0x2

    .line 48
    .line 49
    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    .line 51
    throw v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private final afRDLog()Lcom/appsflyer/internal/AFc1uSDK;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x51

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x42

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x42

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x4

    .line 19
    :goto_0
    if-eq v0, v1, :cond_1

    .line 20
    .line 21
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper:Lkotlin/Lazy;

    .line 22
    .line 23
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/appsflyer/internal/AFc1uSDK;

    .line 28
    .line 29
    return-object v0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper:Lkotlin/Lazy;

    .line 31
    .line 32
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, Lcom/appsflyer/internal/AFc1uSDK;

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :catchall_0
    move-exception v0

    .line 41
    throw v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private afWarnLog()Lcom/appsflyer/internal/AFc1lSDK;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x6d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger:Lkotlin/Lazy;

    .line 12
    .line 13
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/appsflyer/internal/AFc1lSDK;

    .line 18
    .line 19
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 20
    .line 21
    add-int/lit8 v1, v1, 0x15

    .line 22
    .line 23
    rem-int/lit16 v2, v1, 0x80

    .line 24
    .line 25
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 26
    .line 27
    rem-int/lit8 v1, v1, 0x2

    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private final getLevel()Lcom/appsflyer/internal/AFf1jSDK;
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->values()Lcom/appsflyer/internal/AFe1oSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1oSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFe1iSDK;

    .line 6
    .line 7
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1iSDK;->values:Lcom/appsflyer/internal/AFf1eSDK;

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v2, 0x0

    .line 15
    :goto_0
    if-eq v2, v1, :cond_1

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_1
    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 19
    .line 20
    add-int/lit8 v1, v1, 0x59

    .line 21
    .line 22
    rem-int/lit16 v2, v1, 0x80

    .line 23
    .line 24
    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    .line 25
    .line 26
    rem-int/lit8 v1, v1, 0x2

    .line 27
    .line 28
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1eSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFf1bSDK;

    .line 29
    .line 30
    const/16 v1, 0x4c

    .line 31
    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    const/16 v3, 0x56

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_2
    const/16 v3, 0x4c

    .line 38
    .line 39
    :goto_1
    if-eq v3, v1, :cond_3

    .line 40
    .line 41
    add-int/lit8 v1, v2, 0x2b

    .line 42
    .line 43
    rem-int/lit16 v3, v1, 0x80

    .line 44
    .line 45
    sput v3, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 46
    .line 47
    rem-int/lit8 v1, v1, 0x2

    .line 48
    .line 49
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1bSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFf1jSDK;

    .line 50
    .line 51
    add-int/lit8 v2, v2, 0x3f

    .line 52
    .line 53
    rem-int/lit16 v1, v2, 0x80

    .line 54
    .line 55
    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    .line 56
    .line 57
    rem-int/lit8 v2, v2, 0x2

    .line 58
    .line 59
    return-object v0

    .line 60
    :cond_3
    :goto_2
    const/4 v0, 0x0

    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private final valueOf(Lcom/appsflyer/internal/AFf1jSDK;)Z
    .locals 12

    .line 2
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x3d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->afRDLog()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v2

    const-string v3, "af_send_exc_to_server_window"

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    move-result-wide v2

    .line 5
    iget-wide v6, p1, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventType:J

    .line 6
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    const/16 v10, 0x26

    cmp-long v11, v6, v8

    if-gez v11, :cond_0

    const/16 v6, 0xa

    goto :goto_0

    :cond_0
    const/16 v6, 0x26

    :goto_0
    const/4 v7, 0x0

    if-eq v6, v10, :cond_1

    .line 7
    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 p1, p1, 0x51

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 p1, p1, 0x2

    return v7

    :cond_1
    const/4 v6, 0x1

    cmp-long v8, v2, v4

    if-eqz v8, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eq v4, v6, :cond_3

    goto :goto_3

    :cond_3
    cmp-long v4, v2, v0

    if-gez v4, :cond_4

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_5

    :goto_3
    return v7

    :cond_5
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFf1jSDK;)Z

    move-result p1

    return p1
.end method

.method private final values()Lcom/appsflyer/internal/AFe1oSDK;
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x65

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventType:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/appsflyer/internal/AFe1oSDK;

    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v1, v1, 0x57

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v2, 0x51

    if-eqz v1, :cond_0

    const/16 v1, 0x51

    goto :goto_0

    :cond_0
    const/16 v1, 0x4a

    :goto_0
    if-eq v1, v2, :cond_1

    return-object v0

    :cond_1
    const/16 v1, 0x3c

    :try_start_0
    div-int/lit8 v1, v1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private final values(Lcom/appsflyer/internal/AFf1jSDK;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFf1jSDK;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/Pair;

    .line 5
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v6, "\u6679\u661b\u658f\udc67\ufc90\u8fc5\uf686\uda26\u8075"

    const-string v7, "app_id"

    const/4 v8, 0x3

    const-string v9, "p_ex"

    const/4 v10, 0x4

    const-string v11, "api"

    const/4 v12, 0x5

    const/4 v13, 0x6

    const-string v14, "uid"

    const/4 v15, 0x7

    cmpl-float v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v6, v1, v2}, Lcom/appsflyer/internal/AFc1iSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    aget-object v1, v2, v5

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "model"

    .line 6
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v4

    .line 7
    invoke-direct/range {p0 .. p0}, Lcom/appsflyer/internal/AFc1iSDK;->afInfoLog()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object v1

    .line 8
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 9
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 10
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-static {v7, v1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v3

    .line 12
    new-instance v1, Lcom/appsflyer/internal/AFb1vSDK;

    invoke-direct {v1}, Lcom/appsflyer/internal/AFb1vSDK;-><init>()V

    invoke-virtual {v1}, Lcom/appsflyer/internal/AFb1vSDK;->AFKeystoreWrapper()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v8

    .line 13
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v10

    const-string v1, "sdk"

    .line 14
    invoke-direct/range {p0 .. p0}, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLog()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v12

    .line 15
    invoke-direct/range {p0 .. p0}, Lcom/appsflyer/internal/AFc1iSDK;->afInfoLog()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object v1

    .line 16
    iget-object v2, v1, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    iget-object v1, v1, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-static {v2, v1}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-static {v14, v1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v13

    const-string v1, "exc_config"

    .line 18
    invoke-virtual/range {p1 .. p1}, Lcom/appsflyer/internal/AFf1jSDK;->AFInAppEventParameterName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v15

    .line 19
    invoke-static {v0}, Lkotlin/collections/MapsKt;->〇80〇808〇O([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v1, v1, 0x63

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/2addr v1, v3

    return-object v0
.end method

.method private static values(Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List<",
            "Lcom/appsflyer/internal/AFc1pSDK;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 20
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x2f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    const/4 v1, 0x2

    rem-int/2addr v0, v1

    new-array v0, v1, [Lkotlin/Pair;

    const-string v2, "deviceInfo"

    invoke-static {v2, p0}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p0

    const/4 v2, 0x0

    aput-object p0, v0, v2

    const-string p0, "excs"

    invoke-static {p1}, Lcom/appsflyer/internal/AFc1dSDK;->AFInAppEventParameterName(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p0

    const/4 p1, 0x1

    aput-object p0, v0, p1

    invoke-static {v0}, Lkotlin/collections/MapsKt;->〇80〇808〇O([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p0

    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0xf

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/2addr p1, v1

    const/4 v0, 0x4

    if-nez p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/16 p1, 0x3b

    :goto_0
    if-eq p1, v0, :cond_1

    return-object p0

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private static final values(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 3

    .line 2
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x5

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x3b

    if-nez v0, :cond_0

    const/16 v0, 0x2c

    goto :goto_0

    :cond_0
    const/16 v0, 0x3b

    :goto_0
    const-string v2, ""

    if-eq v0, v1, :cond_1

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFVersionDeclaration()V

    const/16 p0, 0x3a

    :try_start_0
    div-int/lit8 p0, p0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p0

    throw p0

    .line 3
    :cond_1
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFVersionDeclaration()V

    :goto_1
    sget p0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p0, p0, 0x1b

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p0, p0, 0x2

    return-void
.end method

.method public static synthetic 〇080(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/appsflyer/internal/AFc1iSDK;->values(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static synthetic 〇o〇(Lcom/appsflyer/internal/AFc1iSDK;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1iSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public final AFInAppEventParameterName()V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x1d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/appsflyer/internal/OO0o〇〇;

    invoke-direct {v1, p0}, Lcom/appsflyer/internal/OO0o〇〇;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x11

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    return-void
.end method

.method public final AFInAppEventType()V
    .locals 2

    .line 2
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x7b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/appsflyer/internal/〇O8o08O;

    invoke-direct {v1, p0}, Lcom/appsflyer/internal/〇O8o08O;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x3d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-ne v0, v1, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method public final AFKeystoreWrapper()Lcom/appsflyer/internal/AFc1mSDK;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x53

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    iget-object v0, p0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLog:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/appsflyer/internal/AFc1mSDK;

    sget v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v1, v1, 0x1b

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/16 v1, 0x43

    :try_start_0
    div-int/2addr v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    throw v0

    :cond_1
    return-object v0
.end method

.method public final AFKeystoreWrapper(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 2
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x9

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/appsflyer/internal/〇8o8o〇;

    invoke-direct {v1, p0, p1, p2}, Lcom/appsflyer/internal/〇8o8o〇;-><init>(Lcom/appsflyer/internal/AFc1iSDK;Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    sget p1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 p1, p1, 0x13

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 p1, p1, 0x2

    const/4 p2, 0x1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eq p1, p2, :cond_1

    return-void

    :cond_1
    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method public final valueOf()V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    add-int/lit8 v0, v0, 0x57

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    rem-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/appsflyer/internal/Oooo8o0〇;

    invoke-direct {v1, p0}, Lcom/appsflyer/internal/Oooo8o0〇;-><init>(Lcom/appsflyer/internal/AFc1iSDK;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    sget v0, Lcom/appsflyer/internal/AFc1iSDK;->afErrorLogForExcManagerOnly:I

    add-int/lit8 v0, v0, 0x1f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFc1iSDK;->AFLogger$LogLevel:I

    rem-int/lit8 v0, v0, 0x2

    return-void
.end method

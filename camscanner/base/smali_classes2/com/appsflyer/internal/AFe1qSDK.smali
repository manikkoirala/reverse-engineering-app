.class public Lcom/appsflyer/internal/AFe1qSDK;
.super Lcom/appsflyer/internal/AFd1aSDK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/appsflyer/internal/AFd1aSDK<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final onAppOpenAttributionNative:[Lcom/appsflyer/internal/AFd1eSDK;


# instance fields
.field private final AFLogger$LogLevel:Lcom/appsflyer/internal/AFd1rSDK;

.field protected final AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

.field private final afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

.field public final afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

.field private final afWarnLog:Lcom/appsflyer/internal/AFc1tSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final getLevel:Lcom/appsflyer/internal/AFe1oSDK;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [Lcom/appsflyer/internal/AFd1eSDK;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/appsflyer/internal/AFd1eSDK;->afRDLog:Lcom/appsflyer/internal/AFd1eSDK;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/appsflyer/internal/AFd1eSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFd1eSDK;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/appsflyer/internal/AFd1eSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFd1eSDK;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/appsflyer/internal/AFd1eSDK;->AFLogger:Lcom/appsflyer/internal/AFd1eSDK;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    sput-object v0, Lcom/appsflyer/internal/AFe1qSDK;->onAppOpenAttributionNative:[Lcom/appsflyer/internal/AFd1eSDK;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 1
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFc1qSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/appsflyer/internal/AFe1qSDK;-><init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFc1qSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->valueOf()Lcom/appsflyer/internal/AFd1eSDK;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/appsflyer/internal/AFd1eSDK;

    sget-object v2, Lcom/appsflyer/internal/AFd1eSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFd1eSDK;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/appsflyer/internal/AFd1aSDK;-><init>(Lcom/appsflyer/internal/AFd1eSDK;[Lcom/appsflyer/internal/AFd1eSDK;Lcom/appsflyer/internal/AFc1qSDK;Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 4
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionDataLoadedNative()Lcom/appsflyer/internal/AFd1rSDK;

    move-result-object p1

    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFd1rSDK;

    .line 5
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object p1

    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 6
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLog()Lcom/appsflyer/internal/AFe1oSDK;

    move-result-object p1

    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->getLevel:Lcom/appsflyer/internal/AFe1oSDK;

    .line 7
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;

    move-result-object p1

    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

    .line 8
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object p1

    iput-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->afWarnLog:Lcom/appsflyer/internal/AFc1tSDK;

    .line 9
    sget-object p1, Lcom/appsflyer/internal/AFe1qSDK;->onAppOpenAttributionNative:[Lcom/appsflyer/internal/AFd1eSDK;

    array-length p2, p1

    :goto_0
    if-ge v3, p2, :cond_0

    aget-object p3, p1, v3

    .line 10
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFd1eSDK;

    if-eq v0, p3, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 12
    iget p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->afRDLog:I

    .line 13
    iget-object p2, p0, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFd1eSDK;

    if-gtz p1, :cond_1

    .line 14
    sget-object p1, Lcom/appsflyer/internal/AFd1eSDK;->valueOf:Lcom/appsflyer/internal/AFd1eSDK;

    if-eq p2, p1, :cond_2

    .line 15
    iget-object p2, p0, Lcom/appsflyer/internal/AFd1kSDK;->values:Ljava/util/Set;

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void

    .line 16
    :cond_1
    sget-object p1, Lcom/appsflyer/internal/AFd1eSDK;->valueOf:Lcom/appsflyer/internal/AFd1eSDK;

    .line 17
    iget-object p2, p0, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventType:Ljava/util/Set;

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method private static AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;)Ljava/util/Map;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFa1uSDK;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "meta"

    .line 6
    .line 7
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Ljava/util/Map;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Ljava/util/HashMap;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    :cond_0
    return-object v0
    .line 28
.end method


# virtual methods
.method protected afDebugLog()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected final afInfoLog()Lcom/appsflyer/attribution/AppsFlyerRequestListener;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/appsflyer/internal/AFa1uSDK;->values:Lcom/appsflyer/attribution/AppsFlyerRequestListener;

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected final valueOf(Ljava/lang/String;)Lcom/appsflyer/internal/AFc1cSDK;
    .locals 17
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/appsflyer/internal/AFc1cSDK<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v2, "*Non-printing character*"

    .line 4
    .line 5
    const-string v3, "JSON toString of eventParams map returns null"

    .line 6
    .line 7
    const-string v4, "\\p{C}"

    .line 8
    .line 9
    const-string v5, "Unexpected error"

    .line 10
    .line 11
    const-string v6, ""

    .line 12
    .line 13
    iget-object v0, v1, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Lcom/appsflyer/internal/AFe1qSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, v1, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v7, "meta"

    .line 25
    .line 26
    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    :try_start_0
    iget-object v0, v1, Lcom/appsflyer/internal/AFe1qSDK;->getLevel:Lcom/appsflyer/internal/AFe1oSDK;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1oSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFe1iSDK;

    .line 35
    .line 36
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1iSDK;->values:Lcom/appsflyer/internal/AFf1eSDK;

    .line 37
    .line 38
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1eSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFf1bSDK;

    .line 39
    .line 40
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1bSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1dSDK;

    .line 41
    .line 42
    iget-wide v8, v0, Lcom/appsflyer/internal/AFf1dSDK;->AFKeystoreWrapper:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :catch_0
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 46
    .line 47
    :goto_0
    invoke-static {v8, v9}, Lcom/appsflyer/internal/AFa1uSDK;->valueOf(D)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    .line 53
    iget-object v0, v1, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-interface {v0, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    :cond_0
    iget-object v0, v1, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 63
    .line 64
    iget-object v7, v0, Lcom/appsflyer/internal/AFa1uSDK;->afErrorLog:Ljava/lang/String;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 67
    .line 68
    .line 69
    move-result-object v8

    .line 70
    const/4 v10, 0x1

    .line 71
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    .line 72
    .line 73
    invoke-direct {v0, v8}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v11
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 80
    if-eqz v11, :cond_1

    .line 81
    .line 82
    :try_start_2
    invoke-virtual {v11, v4, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    goto/16 :goto_8

    .line 87
    .line 88
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    .line 89
    .line 90
    invoke-direct {v0, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    throw v0
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    .line 95
    move-object v9, v11

    .line 96
    goto :goto_1

    .line 97
    :catch_1
    move-exception v0

    .line 98
    goto :goto_2

    .line 99
    :catchall_1
    move-exception v0

    .line 100
    const/4 v9, 0x0

    .line 101
    :goto_1
    invoke-static {v5, v0, v10}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 102
    .line 103
    .line 104
    move-object v0, v6

    .line 105
    move-object v11, v9

    .line 106
    goto/16 :goto_8

    .line 107
    .line 108
    :catch_2
    move-exception v0

    .line 109
    const/4 v11, 0x0

    .line 110
    :goto_2
    const-string v12, "JSONObject return null String object. Trying to create AFJsonObject."

    .line 111
    .line 112
    invoke-static {v12, v0, v10}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 113
    .line 114
    .line 115
    :try_start_3
    new-array v0, v10, [Ljava/lang/Object;

    .line 116
    .line 117
    const/4 v12, 0x0

    .line 118
    aput-object v8, v0, v12

    .line 119
    .line 120
    sget-object v8, Lcom/appsflyer/internal/AFa1ySDK;->init:Ljava/util/Map;

    .line 121
    .line 122
    const v13, -0x5c6bdcc3

    .line 123
    .line 124
    .line 125
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 126
    .line 127
    .line 128
    move-result-object v14

    .line 129
    invoke-interface {v8, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    .line 131
    .line 132
    move-result-object v14

    .line 133
    if-eqz v14, :cond_2

    .line 134
    .line 135
    goto :goto_3

    .line 136
    :cond_2
    invoke-static {}, Landroid/view/ViewConfiguration;->getEdgeSlop()I

    .line 137
    .line 138
    .line 139
    move-result v14

    .line 140
    shr-int/lit8 v14, v14, 0x10

    .line 141
    .line 142
    rsub-int/lit8 v14, v14, 0x24

    .line 143
    .line 144
    invoke-static {v12, v12, v12}, Landroid/graphics/Color;->rgb(III)I

    .line 145
    .line 146
    .line 147
    move-result v15

    .line 148
    const/high16 v16, -0x1000000

    .line 149
    .line 150
    sub-int v15, v16, v15

    .line 151
    .line 152
    int-to-char v15, v15

    .line 153
    invoke-static {v12, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 154
    .line 155
    .line 156
    move-result v16

    .line 157
    add-int/lit8 v9, v16, 0x4a

    .line 158
    .line 159
    invoke-static {v14, v15, v9}, Lcom/appsflyer/internal/AFa1ySDK;->AFKeystoreWrapper(ICI)Ljava/lang/Object;

    .line 160
    .line 161
    .line 162
    move-result-object v9

    .line 163
    check-cast v9, Ljava/lang/Class;

    .line 164
    .line 165
    const-string v14, "values"

    .line 166
    .line 167
    new-array v15, v10, [Ljava/lang/Class;

    .line 168
    .line 169
    const-class v16, Ljava/util/Map;

    .line 170
    .line 171
    aput-object v16, v15, v12

    .line 172
    .line 173
    invoke-virtual {v9, v14, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 174
    .line 175
    .line 176
    move-result-object v14

    .line 177
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 178
    .line 179
    .line 180
    move-result-object v9

    .line 181
    invoke-interface {v8, v9, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    .line 183
    .line 184
    :goto_3
    check-cast v14, Ljava/lang/reflect/Method;

    .line 185
    .line 186
    const/4 v8, 0x0

    .line 187
    invoke-virtual {v14, v8, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    move-object v8, v0

    .line 192
    check-cast v8, Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 193
    .line 194
    if-eqz v8, :cond_3

    .line 195
    .line 196
    :try_start_4
    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    move-object v11, v8

    .line 201
    goto :goto_8

    .line 202
    :cond_3
    new-instance v0, Ljava/lang/NullPointerException;

    .line 203
    .line 204
    invoke-direct {v0, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    throw v0
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 208
    :catchall_2
    move-exception v0

    .line 209
    move-object v11, v8

    .line 210
    goto :goto_4

    .line 211
    :catch_3
    move-exception v0

    .line 212
    move-object v11, v8

    .line 213
    goto :goto_5

    .line 214
    :catch_4
    move-exception v0

    .line 215
    move-object v11, v8

    .line 216
    goto :goto_6

    .line 217
    :catchall_3
    move-exception v0

    .line 218
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    if-eqz v2, :cond_4

    .line 223
    .line 224
    throw v2

    .line 225
    :cond_4
    throw v0
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 226
    :catchall_4
    move-exception v0

    .line 227
    :goto_4
    invoke-static {v5, v0, v10}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 228
    .line 229
    .line 230
    goto :goto_7

    .line 231
    :catch_5
    move-exception v0

    .line 232
    :goto_5
    const-string v2, "AFFinalizer: reflection init failed"

    .line 233
    .line 234
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235
    .line 236
    .line 237
    goto :goto_7

    .line 238
    :catch_6
    move-exception v0

    .line 239
    :goto_6
    const-string v2, "AFJsonObject return null String object."

    .line 240
    .line 241
    invoke-static {v2, v0, v10}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 242
    .line 243
    .line 244
    :goto_7
    move-object v0, v6

    .line 245
    :goto_8
    if-nez v11, :cond_5

    .line 246
    .line 247
    goto :goto_9

    .line 248
    :cond_5
    move-object v6, v11

    .line 249
    :goto_9
    invoke-virtual {v0, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 250
    .line 251
    .line 252
    move-result v2

    .line 253
    if-nez v2, :cond_6

    .line 254
    .line 255
    const-string v2, "Payload contains non-printing characters"

    .line 256
    .line 257
    invoke-static {v2}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    goto :goto_a

    .line 261
    :cond_6
    move-object v0, v6

    .line 262
    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    .line 263
    .line 264
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .line 266
    .line 267
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    const-string v3, ": preparing data: "

    .line 271
    .line 272
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    .line 277
    .line 278
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object v2

    .line 282
    invoke-static {v2}, Lcom/appsflyer/internal/AFb1rSDK;->valueOf(Ljava/lang/String;)V

    .line 283
    .line 284
    .line 285
    iget-object v2, v1, Lcom/appsflyer/internal/AFd1aSDK;->afErrorLog:Lcom/appsflyer/internal/AFb1sSDK;

    .line 286
    .line 287
    invoke-interface {v2, v7, v0}, Lcom/appsflyer/internal/AFb1sSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    iget-object v0, v1, Lcom/appsflyer/internal/AFd1aSDK;->afDebugLog:Lcom/appsflyer/internal/AFd1zSDK;

    .line 291
    .line 292
    iget-object v2, v1, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 293
    .line 294
    iget-object v3, v1, Lcom/appsflyer/internal/AFe1qSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

    .line 295
    .line 296
    move-object/from16 v4, p1

    .line 297
    .line 298
    invoke-virtual {v0, v2, v4, v3}, Lcom/appsflyer/internal/AFd1zSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Ljava/lang/String;Lcom/appsflyer/internal/AFc1rSDK;)Lcom/appsflyer/internal/AFc1cSDK;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    return-object v0
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method protected values(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 7
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->afErrorLog()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1aSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    new-instance v2, Lcom/appsflyer/internal/AFc1wSDK;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1hSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 16
    .line 17
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 18
    .line 19
    invoke-direct {v2, v1, v0}, Lcom/appsflyer/internal/AFc1wSDK;-><init>(Ljava/util/Map;Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v2}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1aSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFe1hSDK;->AFKeystoreWrapper(Ljava/util/Map;)Ljava/util/Map;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {p1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 36
    .line 37
    .line 38
    :cond_0
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->afDebugLog()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1aSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFe1hSDK;->AFInAppEventParameterName()Ljava/util/Map;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {p1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 51
    .line 52
    .line 53
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1kSDK;->values:Ljava/util/Set;

    .line 54
    .line 55
    sget-object v1, Lcom/appsflyer/internal/AFd1eSDK;->afWarnLog:Lcom/appsflyer/internal/AFd1eSDK;

    .line 56
    .line 57
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    const/4 v2, 0x1

    .line 62
    const/4 v3, 0x0

    .line 63
    if-nez v1, :cond_3

    .line 64
    .line 65
    sget-object v1, Lcom/appsflyer/internal/AFd1eSDK;->valueOf:Lcom/appsflyer/internal/AFd1eSDK;

    .line 66
    .line 67
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    if-eqz v0, :cond_2

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_2
    const/4 v0, 0x0

    .line 75
    goto :goto_1

    .line 76
    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 77
    :goto_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFd1kSDK;->afErrorLog()Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_4

    .line 82
    .line 83
    if-eqz v0, :cond_4

    .line 84
    .line 85
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 86
    .line 87
    const-string v1, "appsFlyerCount"

    .line 88
    .line 89
    invoke-interface {v0, v1, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    invoke-virtual {p1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(I)Lcom/appsflyer/internal/AFa1uSDK;

    .line 94
    .line 95
    .line 96
    :cond_4
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->afRDLog()Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_a

    .line 101
    .line 102
    invoke-static {p1}, Lcom/appsflyer/internal/AFe1qSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;)Ljava/util/Map;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFd1rSDK;

    .line 107
    .line 108
    new-instance v4, Lcom/appsflyer/internal/AFd1jSDK;

    .line 109
    .line 110
    invoke-virtual {v1}, Lcom/appsflyer/internal/AFd1rSDK;->valueOf()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v5

    .line 114
    invoke-virtual {v1}, Lcom/appsflyer/internal/AFd1rSDK;->values()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    invoke-static {}, Lcom/appsflyer/internal/AFd1rSDK;->AFKeystoreWrapper()Z

    .line 119
    .line 120
    .line 121
    move-result v6

    .line 122
    if-eqz v6, :cond_5

    .line 123
    .line 124
    sget-object v6, Lcom/appsflyer/internal/AFd1lSDK;->values:Lcom/appsflyer/internal/AFd1lSDK;

    .line 125
    .line 126
    goto :goto_2

    .line 127
    :cond_5
    sget-object v6, Lcom/appsflyer/internal/AFd1lSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFd1lSDK;

    .line 128
    .line 129
    :goto_2
    invoke-direct {v4, v5, v1, v6}, Lcom/appsflyer/internal/AFd1jSDK;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/appsflyer/internal/AFd1lSDK;)V

    .line 130
    .line 131
    .line 132
    new-instance v1, Lorg/json/JSONObject;

    .line 133
    .line 134
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 135
    .line 136
    .line 137
    const-string v5, "name"

    .line 138
    .line 139
    iget-object v6, v4, Lcom/appsflyer/internal/AFd1jSDK;->values:Ljava/lang/String;

    .line 140
    .line 141
    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    .line 143
    .line 144
    iget-object v5, v4, Lcom/appsflyer/internal/AFd1jSDK;->valueOf:Lcom/appsflyer/internal/AFd1lSDK;

    .line 145
    .line 146
    sget-object v6, Lcom/appsflyer/internal/AFd1lSDK;->values:Lcom/appsflyer/internal/AFd1lSDK;

    .line 147
    .line 148
    if-eq v5, v6, :cond_6

    .line 149
    .line 150
    const-string v6, "method"

    .line 151
    .line 152
    iget-object v5, v5, Lcom/appsflyer/internal/AFd1lSDK;->AFInAppEventType:Ljava/lang/String;

    .line 153
    .line 154
    invoke-virtual {v1, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 155
    .line 156
    .line 157
    :cond_6
    iget-object v5, v4, Lcom/appsflyer/internal/AFd1jSDK;->AFInAppEventType:Ljava/lang/String;

    .line 158
    .line 159
    if-eqz v5, :cond_8

    .line 160
    .line 161
    invoke-static {v5}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 162
    .line 163
    .line 164
    move-result v5

    .line 165
    if-eqz v5, :cond_7

    .line 166
    .line 167
    goto :goto_3

    .line 168
    :cond_7
    const/4 v2, 0x0

    .line 169
    :cond_8
    :goto_3
    if-nez v2, :cond_9

    .line 170
    .line 171
    const-string v2, "prefix"

    .line 172
    .line 173
    iget-object v3, v4, Lcom/appsflyer/internal/AFd1jSDK;->AFInAppEventType:Ljava/lang/String;

    .line 174
    .line 175
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 176
    .line 177
    .line 178
    :cond_9
    const-string v2, "host"

    .line 179
    .line 180
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    :cond_a
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->afWarnLog:Lcom/appsflyer/internal/AFc1tSDK;

    .line 184
    .line 185
    const-string v1, "AF_PREINSTALL_DISABLED"

    .line 186
    .line 187
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    .line 188
    .line 189
    .line 190
    move-result v0

    .line 191
    if-eqz v0, :cond_b

    .line 192
    .line 193
    invoke-static {p1}, Lcom/appsflyer/internal/AFe1qSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;)Ljava/util/Map;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    const-string v0, "preinstall_disabled"

    .line 198
    .line 199
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 200
    .line 201
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    :cond_b
    return-void
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

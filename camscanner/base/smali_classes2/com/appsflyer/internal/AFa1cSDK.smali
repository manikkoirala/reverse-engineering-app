.class public final Lcom/appsflyer/internal/AFa1cSDK;
.super Lcom/appsflyer/AppsFlyerLib;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/appsflyer/internal/AFa1cSDK$AFa1zSDK;,
        Lcom/appsflyer/internal/AFa1cSDK$AFa1vSDK;
    }
.end annotation


# static fields
.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field public static final AFInAppEventType:Ljava/lang/String;

.field static AFKeystoreWrapper:Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener; = null

.field private static afErrorLog:Lcom/appsflyer/internal/AFa1cSDK; = null
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static onConversionDataSuccess:C = '\u0000'

.field private static onDeepLinkingNative:I = 0x1

.field private static onResponseErrorNative:I

.field private static onResponseNative:[C

.field public static final valueOf:Ljava/lang/String;

.field static final values:Ljava/lang/String;


# instance fields
.field public volatile AFInAppEventParameterName:Lcom/appsflyer/AppsFlyerConversionListener;

.field private AFLogger:J

.field private AFLogger$LogLevel:Z

.field private AFVersionDeclaration:J

.field private AppsFlyer2dXConversionCallback:Ljava/lang/String;

.field public afDebugLog:Lcom/appsflyer/internal/AFb1aSDK;

.field private afErrorLogForExcManagerOnly:Z

.field afInfoLog:Ljava/lang/String;

.field private afRDLog:J

.field private afWarnLog:Ljava/lang/String;

.field private getLevel:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private init:Landroid/content/SharedPreferences;

.field private onAppOpenAttributionNative:Landroid/app/Application;

.field private final onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private onInstallConversionFailureNative:Lcom/appsflyer/internal/AFc1xSDK;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper()V

    .line 2
    .line 3
    .line 4
    const-string v0, "269"

    .line 5
    .line 6
    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->values:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "6.12"

    .line 9
    .line 10
    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v0, "/androidevent?buildnumber=6.12.4&app_id="

    .line 21
    .line 22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->valueOf:Ljava/lang/String;

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper:Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener;

    .line 33
    .line 34
    new-instance v0, Lcom/appsflyer/internal/AFa1cSDK;

    .line 35
    .line 36
    invoke-direct {v0}, Lcom/appsflyer/internal/AFa1cSDK;-><init>()V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLog:Lcom/appsflyer/internal/AFa1cSDK;

    .line 40
    .line 41
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 42
    .line 43
    add-int/lit8 v0, v0, 0x51

    .line 44
    .line 45
    rem-int/lit16 v1, v0, 0x80

    .line 46
    .line 47
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 48
    .line 49
    rem-int/lit8 v0, v0, 0x2

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>()V
    .locals 3
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/AppsFlyerLib;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName:Lcom/appsflyer/AppsFlyerConversionListener;

    .line 6
    .line 7
    const-wide/16 v0, -0x1

    .line 8
    .line 9
    iput-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog:J

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger:J

    .line 12
    .line 13
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 14
    .line 15
    const-wide/16 v1, 0x5

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 18
    .line 19
    .line 20
    move-result-wide v0

    .line 21
    iput-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration:J

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLogForExcManagerOnly:Z

    .line 25
    .line 26
    invoke-static {}, Lcom/appsflyer/AFVersionDeclaration;->init()V

    .line 27
    .line 28
    .line 29
    new-instance v1, Lcom/appsflyer/internal/AFc1oSDK;

    .line 30
    .line 31
    invoke-direct {v1}, Lcom/appsflyer/internal/AFc1oSDK;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v1, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLogForExcManagerOnly()Lcom/appsflyer/internal/AFd1fSDK;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    new-instance v2, Lcom/appsflyer/internal/AFa1cSDK$AFa1zSDK;

    .line 45
    .line 46
    invoke-direct {v2, p0, v0}, Lcom/appsflyer/internal/AFa1cSDK$AFa1zSDK;-><init>(Lcom/appsflyer/internal/AFa1cSDK;B)V

    .line 47
    .line 48
    .line 49
    iget-object v0, v1, Lcom/appsflyer/internal/AFd1fSDK;->valueOf:Ljava/util/List;

    .line 50
    .line 51
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;)I
    .locals 3

    .line 76
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x71

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "appsFlyerAdImpressionCount"

    invoke-direct {p0, p1, v2, v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I

    move-result p1

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x59

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    return p1
.end method

.method private AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I
    .locals 3

    const/4 v0, 0x0

    .line 83
    invoke-interface {p1, p2, v0}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eq p3, v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 84
    invoke-interface {p1, p2, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventType(Ljava/lang/String;I)V

    .line 85
    :goto_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFb1sSDK;->AFLogger()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    const/4 p2, 0x0

    if-eqz p1, :cond_5

    .line 86
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x43

    rem-int/lit16 p3, p1, 0x80

    sput p3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_3

    const/4 p1, 0x0

    goto :goto_3

    :cond_3
    const/4 p1, 0x1

    :goto_3
    if-ne p1, v2, :cond_4

    .line 87
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/appsflyer/internal/AFb1sSDK;->AFInAppEventType(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object p1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/appsflyer/internal/AFb1sSDK;->AFInAppEventType(Ljava/lang/String;)V

    :try_start_0
    throw p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 88
    throw p1

    .line 89
    :cond_5
    :goto_4
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x51

    rem-int/lit16 p3, p1, 0x80

    sput p3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_6

    const/4 v0, 0x1

    :cond_6
    if-eq v0, v2, :cond_7

    return v1

    :cond_7
    :try_start_1
    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method private AFInAppEventParameterName(Ljava/util/Map;)Lcom/appsflyer/internal/AFe1wSDK$AFa1vSDK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/appsflyer/internal/AFe1wSDK$AFa1vSDK;"
        }
    .end annotation

    .line 79
    new-instance v0, Lcom/appsflyer/internal/AFa1cSDK$2;

    invoke-direct {v0, p0, p1}, Lcom/appsflyer/internal/AFa1cSDK$2;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Ljava/util/Map;)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x1b

    rem-int/lit16 v1, p1, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    return-object v0
.end method

.method private AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1sSDK;)Ljava/lang/Runnable;
    .locals 3

    .line 42
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x79

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x0

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/appsflyer/internal/〇o〇;

    invoke-direct {v0, p0, p1}, Lcom/appsflyer/internal/〇o〇;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFg1sSDK;)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0xf

    rem-int/lit16 v1, p1, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    const/4 v1, 0x4

    if-nez p1, :cond_1

    const/4 p1, 0x4

    goto :goto_1

    :cond_1
    const/4 p1, 0x3

    :goto_1
    if-eq p1, v1, :cond_2

    return-object v0

    :cond_2
    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :cond_3
    new-instance v0, Lcom/appsflyer/internal/〇o〇;

    invoke-direct {v0, p0, p1}, Lcom/appsflyer/internal/〇o〇;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFg1sSDK;)V

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method public static AFInAppEventParameterName()Ljava/lang/String;
    .locals 3

    .line 43
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x39

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x32

    if-eqz v0, :cond_0

    const/16 v0, 0x32

    goto :goto_0

    :cond_0
    const/16 v0, 0xe

    :goto_0
    const-string v2, "AppUserId"

    if-eq v0, v1, :cond_1

    invoke-static {v2}, Lcom/appsflyer/internal/AFa1cSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x47

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    return-object v0

    :cond_1
    invoke-static {v2}, Lcom/appsflyer/internal/AFa1cSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private static AFInAppEventParameterName(Landroid/app/Activity;)Ljava/lang/String;
    .locals 8

    const-string v0, "af"

    const/16 v1, 0x1e

    if-eqz p0, :cond_0

    const/16 v2, 0x17

    goto :goto_0

    :cond_0
    const/16 v2, 0x1e

    :goto_0
    const/4 v3, 0x0

    if-eq v2, v1, :cond_7

    .line 61
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 62
    sget v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v2, v2, 0x11

    rem-int/lit16 v4, v2, 0x80

    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_6

    .line 63
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 64
    invoke-virtual {v2, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v5, 0x3d

    if-eqz v4, :cond_1

    const/16 v6, 0x41

    goto :goto_1

    :cond_1
    const/16 v6, 0x3d

    :goto_1
    if-eq v6, v5, :cond_4

    .line 65
    sget v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v5, v5, 0x69

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v5, v5, 0x2

    const/16 v6, 0x4c

    if-nez v5, :cond_2

    const/16 v5, 0x4c

    goto :goto_2

    :cond_2
    const/4 v5, 0x1

    :goto_2
    const-string v7, "Push Notification received af payload = "

    if-eq v5, v6, :cond_3

    .line 66
    :try_start_1
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    goto :goto_3

    :catchall_0
    move-exception p0

    move-object v3, v4

    goto :goto_4

    .line 69
    :cond_3
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :goto_3
    move-object v3, v4

    .line 72
    :cond_5
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p0, p0, 0x65

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p0, p0, 0x2

    goto :goto_5

    :catchall_1
    move-exception p0

    goto :goto_4

    .line 73
    :cond_6
    :try_start_2
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 74
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75
    :goto_4
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    :goto_5
    return-object v3
.end method

.method private static AFInAppEventParameterName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/16 v0, 0x3f

    .line 77
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eq v1, v2, :cond_1

    .line 78
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, p0, 0x6f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    add-int/lit8 p0, p0, 0x35

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const-string p0, ""

    return-object p0
.end method

.method public static AFInAppEventParameterName(Ljava/text/SimpleDateFormat;J)Ljava/lang/String;
    .locals 1

    const-string v0, "UTC"

    .line 52
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 53
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x6f

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_1

    return-object p0

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private AFInAppEventParameterName(Landroid/content/Context;Lcom/appsflyer/internal/AFf1sSDK;)V
    .locals 3

    .line 44
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x3f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 45
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 46
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger()Lcom/appsflyer/internal/AFf1tSDK;

    move-result-object v0

    .line 47
    invoke-static {p1}, Lcom/appsflyer/internal/AFa1tSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFf1vSDK;

    move-result-object p1

    .line 48
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFf1tSDK;->valueOf()Z

    move-result v1

    const/16 v2, 0x3a

    if-eqz v1, :cond_0

    const/16 v1, 0x60

    goto :goto_0

    :cond_0
    const/16 v1, 0x3a

    :goto_0
    if-eq v1, v2, :cond_1

    .line 49
    iget-object v1, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFKeystoreWrapper:Ljava/util/Map;

    const-string v2, "api_name"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {v0, p1}, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFf1vSDK;)V

    .line 51
    :cond_1
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName()V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x4b

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method private AFInAppEventParameterName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 54
    new-instance v0, Lcom/appsflyer/internal/AFf1lSDK;

    invoke-direct {v0}, Lcom/appsflyer/internal/AFf1lSDK;-><init>()V

    .line 55
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 56
    iput-object p1, v0, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    .line 57
    iput-object p1, v0, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventParameterName:Ljava/util/Map;

    .line 58
    iput-object p2, v0, Lcom/appsflyer/internal/AFa1uSDK;->afDebugLog:Ljava/lang/String;

    .line 59
    iput-object p1, v0, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType:Ljava/lang/String;

    .line 60
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x39

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    const/16 p2, 0x25

    if-nez p1, :cond_0

    const/16 p1, 0x25

    goto :goto_0

    :cond_0
    const/16 p1, 0x4d

    :goto_0
    if-eq p1, p2, :cond_1

    return-void

    :cond_1
    const/16 p1, 0x60

    :try_start_0
    div-int/lit8 p1, p1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1
.end method

.method static synthetic AFInAppEventParameterName(Lcom/appsflyer/internal/AFa1cSDK;)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x2d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog()V

    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x1f

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    if-eq p0, v1, :cond_1

    const/16 p0, 0x62

    :try_start_0
    div-int/2addr p0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p0

    throw p0

    :cond_1
    return-void
.end method

.method private static AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 41
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x29

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x69

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const/16 p1, 0x8

    if-nez p0, :cond_0

    const/16 p0, 0x63

    goto :goto_0

    :cond_0
    const/16 p0, 0x8

    :goto_0
    if-ne p0, p1, :cond_1

    return-void

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private static AFInAppEventParameterName(Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V
    .locals 2
    .param p0    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 90
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x23

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 91
    :try_start_0
    invoke-interface {p0, p1, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void

    :cond_1
    invoke-interface {p0, p1, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x0

    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    const-string p1, "scheduleJob failed with Exception"

    .line 92
    invoke-static {p1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p0

    const-string p1, "scheduleJob failed with RejectedExecutionException Exception"

    .line 93
    invoke-static {p1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static AFInAppEventParameterName(Lorg/json/JSONObject;)V
    .locals 14

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 21
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 22
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_3

    .line 23
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 24
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v5, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 25
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-ge v2, v6, :cond_0

    const/4 v6, 0x0

    goto :goto_2

    :cond_0
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_1

    goto :goto_0

    .line 26
    :cond_1
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v6, v6, 0x1b

    rem-int/lit16 v7, v6, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v6, v6, 0x2

    if-nez v6, :cond_2

    .line 27
    :try_start_1
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x70

    goto :goto_1

    :cond_2
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "error at timeStampArr"

    .line 28
    invoke-static {v3, v2}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 29
    :cond_3
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 30
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_4
    :goto_3
    move-object v5, v2

    .line 31
    :cond_5
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x0

    goto :goto_5

    :cond_6
    const/4 v6, 0x1

    :goto_5
    if-eq v6, v4, :cond_b

    if-nez v5, :cond_b

    .line 32
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 33
    :try_start_2
    new-instance v7, Lorg/json/JSONArray;

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    .line 34
    :goto_6
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    const/16 v10, 0x13

    if-ge v8, v9, :cond_7

    const/16 v9, 0x51

    goto :goto_7

    :cond_7
    const/16 v9, 0x13

    :goto_7
    if-eq v9, v10, :cond_5

    .line 35
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v9

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Number;->longValue()J

    move-result-wide v11
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    cmp-long v13, v9, v11

    if-eqz v13, :cond_8

    const/4 v9, 0x1

    goto :goto_8

    :cond_8
    const/4 v9, 0x0

    :goto_8
    if-eqz v9, :cond_4

    .line 36
    sget v9, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v9, v9, 0x55

    rem-int/lit16 v10, v9, 0x80

    sput v10, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v9, v9, 0x2

    .line 37
    :try_start_3
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v9

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Number;->longValue()J

    move-result-wide v11

    cmp-long v13, v9, v11

    if-eqz v13, :cond_4

    .line 38
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    sub-int/2addr v11, v4

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Number;->longValue()J

    move-result-wide v11
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    cmp-long v5, v9, v11

    if-nez v5, :cond_9

    const/4 v5, 0x0

    goto :goto_9

    :cond_9
    const/4 v5, 0x1

    :goto_9
    if-eq v5, v4, :cond_a

    goto :goto_3

    :cond_a
    add-int/lit8 v8, v8, 0x1

    move-object v5, v6

    goto :goto_6

    :catch_1
    move-exception v6

    const-string v7, "error at manageExtraReferrers"

    .line 39
    invoke-static {v7, v6}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    :cond_b
    if-eqz v5, :cond_c

    .line 40
    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    :cond_c
    return-void
.end method

.method public static declared-synchronized AFInAppEventType(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 6

    const-class v0, Lcom/appsflyer/internal/AFa1cSDK;

    monitor-enter v0

    .line 45
    :try_start_0
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x5f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v2, 0x5b

    if-eqz v1, :cond_0

    const/16 v1, 0x5b

    goto :goto_0

    :cond_0
    const/16 v1, 0x2b

    :goto_0
    const/4 v3, 0x0

    if-eq v1, v2, :cond_4

    .line 46
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    move-result-object v1

    iget-object v1, v1, Lcom/appsflyer/internal/AFa1cSDK;->init:Landroid/content/SharedPreferences;

    if-nez v1, :cond_1

    .line 47
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 48
    :try_start_1
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v4, "appsflyer-data"

    const/4 v5, 0x0

    .line 49
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    iput-object p0, v2, Lcom/appsflyer/internal/AFa1cSDK;->init:Landroid/content/SharedPreferences;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    :try_start_2
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    :catchall_0
    move-exception p0

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 51
    throw p0

    .line 52
    :cond_1
    :goto_1
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    move-result-object p0

    iget-object p0, p0, Lcom/appsflyer/internal/AFa1cSDK;->init:Landroid/content/SharedPreferences;

    .line 53
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x7b

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    const/16 v2, 0x11

    if-eqz v1, :cond_2

    const/16 v1, 0x11

    goto :goto_2

    :cond_2
    const/16 v1, 0xc

    :goto_2
    if-eq v1, v2, :cond_3

    .line 54
    monitor-exit v0

    return-object p0

    .line 55
    :cond_3
    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p0

    :try_start_4
    throw p0

    :cond_4
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    move-result-object p0

    iget-object p0, p0, Lcom/appsflyer/internal/AFa1cSDK;->init:Landroid/content/SharedPreferences;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p0

    .line 56
    :try_start_6
    throw p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;
    .locals 2

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x5b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x4d

    if-eqz v0, :cond_0

    const/16 v0, 0x52

    goto :goto_0

    :cond_0
    const/16 v0, 0x4d

    :goto_0
    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLog:Lcom/appsflyer/internal/AFa1cSDK;

    const/16 v1, 0x2b

    :try_start_0
    div-int/lit8 v1, v1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    throw v0

    :cond_1
    sget-object v0, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLog:Lcom/appsflyer/internal/AFa1cSDK;

    :goto_1
    return-object v0
.end method

.method private AFInAppEventType(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .line 16
    new-instance v0, Lcom/appsflyer/internal/AFf1oSDK;

    invoke-direct {v0}, Lcom/appsflyer/internal/AFf1oSDK;-><init>()V

    .line 17
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object v1

    .line 18
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v2, "appsFlyerCount"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFa1uSDK;->values(I)Lcom/appsflyer/internal/AFa1uSDK;

    move-result-object v0

    .line 20
    iput-object p2, v0, Lcom/appsflyer/internal/AFa1uSDK;->afDebugLog:Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_3

    .line 21
    :cond_1
    sget v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v2, v2, 0x7b

    rem-int/lit16 v4, v2, 0x80

    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_2

    const/4 v1, 0x0

    .line 22
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    if-le p2, v1, :cond_6

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    const/16 v2, 0x11

    if-le p2, v1, :cond_4

    const/16 p2, 0x11

    goto :goto_1

    :cond_4
    const/16 p2, 0x10

    :goto_1
    if-eq p2, v2, :cond_5

    goto :goto_3

    .line 23
    :cond_5
    :goto_2
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1uSDK;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->valueOf()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    .line 25
    new-instance p2, Lcom/appsflyer/internal/AFa1cSDK$AFa1vSDK;

    invoke-direct {p2, p0, v0, v3}, Lcom/appsflyer/internal/AFa1cSDK$AFa1vSDK;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFa1uSDK;B)V

    const-wide/16 v0, 0x5

    .line 26
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, p2, v0, v1, v2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V

    .line 27
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x71

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    :cond_6
    :goto_3
    return-void
.end method

.method private AFInAppEventType(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 9
    new-instance v0, Lcom/appsflyer/internal/AFf1mSDK;

    invoke-direct {v0}, Lcom/appsflyer/internal/AFf1mSDK;-><init>()V

    .line 10
    iput-object p2, v0, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    .line 11
    iput-object p3, v0, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventParameterName:Ljava/util/Map;

    .line 12
    instance-of p2, p1, Landroid/app/Activity;

    const/4 p3, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    :goto_0
    const/4 v2, 0x0

    if-eqz p2, :cond_1

    .line 13
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x1d

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    goto :goto_1

    :cond_1
    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p2, p2, 0x77

    rem-int/lit16 v3, p2, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_2

    const/4 p3, 0x1

    :cond_2
    if-nez p3, :cond_3

    .line 14
    move-object v2, p1

    check-cast v2, Landroid/app/Activity;

    .line 15
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V

    return-void

    :cond_3
    check-cast p1, Landroid/app/Activity;

    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method static synthetic AFInAppEventType(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x7b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    if-eq v0, v1, :cond_1

    return-void

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 58
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x1d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 59
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onResponseErrorNative()Lcom/appsflyer/internal/AFg1wSDK;

    move-result-object v0

    .line 60
    invoke-interface {v0, p2}, Lcom/appsflyer/internal/AFg1wSDK;->values(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 61
    iput-object v1, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType:Ljava/lang/String;

    .line 62
    invoke-interface {v0, p2}, Lcom/appsflyer/internal/AFg1wSDK;->AFInAppEventParameterName(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object p2

    .line 63
    iput-object p2, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFLogger:Ljava/lang/String;

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0xb

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private synthetic AFInAppEventType(Lcom/appsflyer/internal/AFe1kSDK;)V
    .locals 3

    .line 4
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x11

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 5
    sget-object v0, Lcom/appsflyer/internal/AFe1kSDK;->values:Lcom/appsflyer/internal/AFe1kSDK;

    const/16 v2, 0x3b

    if-ne p1, v0, :cond_0

    const/16 p1, 0x10

    goto :goto_0

    :cond_0
    const/16 p1, 0x3b

    :goto_0
    if-eq p1, v2, :cond_2

    .line 6
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x67

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onResponseNative()Lcom/appsflyer/internal/AFc1jSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1jSDK;->AFInAppEventType()V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x63

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onResponseNative()Lcom/appsflyer/internal/AFc1jSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1jSDK;->AFInAppEventType()V

    :try_start_0
    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :cond_2
    :goto_1
    return-void

    .line 7
    :cond_3
    sget-object p1, Lcom/appsflyer/internal/AFe1kSDK;->values:Lcom/appsflyer/internal/AFe1kSDK;

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    .line 8
    throw p1
.end method

.method private AFInAppEventType(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 28
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x45

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 29
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "collectAndroidIdForceByUser"

    const/4 v2, 0x0

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eq v0, v1, :cond_1

    .line 31
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v3, "collectIMEIForceByUser"

    .line 32
    invoke-virtual {v0, v3, v2}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_6

    const-string v0, "advertiserId"

    .line 33
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 34
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog:Ljava/lang/String;

    invoke-static {v0}, Lcom/appsflyer/internal/AFb1iSDK;->AFInAppEventType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "android_id"

    .line 36
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_3

    .line 37
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x53

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    :try_start_1
    const-string v0, "validateGaidAndIMEI :: removing: android_id"

    .line 38
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 39
    :cond_3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object v0

    .line 40
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1hSDK;->afInfoLog:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Lcom/appsflyer/internal/AFb1iSDK;->AFInAppEventType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "imei"

    .line 42
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/16 v0, 0x38

    if-eqz p1, :cond_4

    const/16 p1, 0x55

    goto :goto_1

    :cond_4
    const/16 p1, 0x38

    :goto_1
    if-eq p1, v0, :cond_5

    const-string p1, "validateGaidAndIMEI :: removing: imei"

    .line 43
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    return-void

    :catch_0
    move-exception p1

    const-string v0, "failed to remove IMEI or AndroidID key from params; "

    .line 44
    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_6
    return-void
.end method

.method private static AFInAppEventType(Ljava/lang/String;)Z
    .locals 3

    .line 3
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    const/4 v1, 0x2

    rem-int/2addr v0, v1

    const/16 v2, 0x43

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x43

    :goto_0
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, p0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :goto_2
    return p0
.end method

.method private static AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_0

    .line 93
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x33

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "fb\\d*?://authorize.*"

    .line 94
    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    const/16 v1, 0xa

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    goto :goto_0

    :cond_1
    const/16 v0, 0x28

    :goto_0
    if-eq v0, v1, :cond_2

    goto/16 :goto_6

    :cond_2
    const-string v0, "access_token"

    .line 95
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 96
    invoke-static {p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x4d

    if-nez v2, :cond_3

    const/16 v2, 0x12

    goto :goto_1

    :cond_3
    const/16 v2, 0x4d

    :goto_1
    const/4 v4, 0x0

    if-eq v2, v3, :cond_5

    .line 98
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0xb

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_4

    const/16 v0, 0x55

    .line 99
    :try_start_0
    div-int/2addr v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p0

    :catchall_0
    move-exception p0

    .line 100
    throw p0

    :cond_4
    return-object p0

    .line 101
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "&"

    .line 102
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 103
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_2

    .line 104
    :cond_6
    invoke-virtual {v2, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 105
    sget v5, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v5, v5, 0x49

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v5, v5, 0x2

    .line 106
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 108
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 109
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v6, v6, 0x57

    rem-int/lit16 v7, v6, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v6, v6, 0x2

    .line 110
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 111
    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 112
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 113
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_9

    .line 114
    sget v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v7, v7, 0x4b

    rem-int/lit16 v8, v7, 0x80

    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_8

    .line 115
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0xa

    :try_start_1
    div-int/2addr v7, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception p0

    .line 116
    throw p0

    .line 117
    :cond_8
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_9
    const-string v7, "?"

    .line 118
    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_a

    const/4 v8, 0x0

    goto :goto_4

    :cond_a
    const/4 v8, 0x1

    :goto_4
    if-eqz v8, :cond_b

    goto :goto_5

    .line 119
    :cond_b
    sget v8, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v8, v8, 0x59

    rem-int/lit16 v9, v8, 0x80

    sput v9, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v8, v8, 0x2

    .line 120
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :goto_5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 122
    :cond_c
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 123
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x1f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    :cond_d
    :goto_6
    return-object p0
.end method

.method public static AFKeystoreWrapper(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 11
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "readServerResponse error"

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 125
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v3

    const/16 v4, 0x3e

    if-nez v3, :cond_0

    const/16 v5, 0x50

    goto :goto_0

    :cond_0
    const/16 v5, 0x3e

    :goto_0
    if-eq v5, v4, :cond_1

    .line 126
    invoke-virtual {p0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 127
    :cond_1
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 128
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 129
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v6, :cond_6

    .line 130
    sget v7, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v8, v7, 0x5f

    rem-int/lit16 v9, v8, 0x80

    sput v9, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v8, v8, 0x2

    const/4 v9, 0x1

    if-nez v8, :cond_3

    const/16 v8, 0xd

    .line 131
    :try_start_3
    div-int/2addr v8, v2

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    if-eq v5, v9, :cond_5

    goto :goto_4

    :cond_3
    const/16 v8, 0x42

    if-eqz v5, :cond_4

    const/16 v5, 0x42

    goto :goto_3

    :cond_4
    const/16 v5, 0x5e

    :goto_3
    if-eq v5, v8, :cond_5

    :goto_4
    const-string v5, ""
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :cond_5
    add-int/lit8 v7, v7, 0x53

    .line 132
    rem-int/lit16 v5, v7, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v7, v7, 0x2

    const/16 v5, 0xa

    .line 133
    :try_start_4
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    :goto_5
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v5, 0x1

    goto :goto_1

    .line 134
    :cond_6
    :try_start_5
    invoke-virtual {v3}, Ljava/io/Reader;->close()V

    .line 135
    invoke-virtual {v4}, Ljava/io/Reader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_a

    :catchall_0
    move-exception v2

    move-object v10, v3

    move-object v3, v2

    move-object v2, v10

    goto :goto_6

    :catchall_1
    move-exception v3

    goto :goto_6

    :catchall_2
    move-exception v3

    move-object v4, v2

    .line 136
    :goto_6
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not read connection response from: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Ljava/net/URLConnection;->getURL()Ljava/net/URL;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 138
    invoke-static {p0, v3}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    if-eqz v2, :cond_7

    .line 139
    :try_start_7
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_7

    :catchall_3
    move-exception p0

    goto :goto_9

    :cond_7
    :goto_7
    const/16 p0, 0x43

    if-eqz v4, :cond_8

    const/16 v2, 0x43

    goto :goto_8

    :cond_8
    const/16 v2, 0x26

    :goto_8
    if-eq v2, p0, :cond_9

    goto :goto_a

    .line 140
    :cond_9
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x5b

    rem-int/lit16 v2, p0, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    .line 141
    :try_start_8
    invoke-virtual {v4}, Ljava/io/Reader;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_a

    .line 142
    :goto_9
    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    :goto_a
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 144
    :try_start_9
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    const-string v1, "error while parsing readServerResponse"

    .line 145
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 146
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_a
    const-string v1, "string_response"

    .line 147
    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 148
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    const-string v0, "RESPONSE_NOT_JSON error"

    .line 149
    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :catchall_4
    move-exception p0

    if-eqz v2, :cond_a

    .line 151
    :try_start_b
    invoke-virtual {v2}, Ljava/io/Reader;->close()V

    goto :goto_b

    :catchall_5
    move-exception v1

    goto :goto_c

    :cond_a
    :goto_b
    if-eqz v4, :cond_b

    .line 152
    invoke-virtual {v4}, Ljava/io/Reader;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    goto :goto_d

    .line 153
    :goto_c
    invoke-static {v0, v1}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    :cond_b
    :goto_d
    throw p0
.end method

.method static AFKeystoreWrapper()V
    .locals 1

    .line 1
    const/16 v0, 0x9

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseNative:[C

    const v0, 0xb52c

    sput-char v0, Lcom/appsflyer/internal/AFa1cSDK;->onConversionDataSuccess:C

    return-void

    nop

    :array_0
    .array-data 2
        -0x7f2cs
        -0x7f24s
        -0x7f30s
        -0x7f2ds
        -0x7f3fs
        -0x7f16s
        -0x7f28s
        -0x7f3as
        -0x7f3bs
    .end array-data
.end method

.method private static AFKeystoreWrapper(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DiscouragedApi"
        }
    .end annotation

    .line 33
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 34
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const/16 v1, 0x4e

    if-eqz v0, :cond_0

    const/16 v0, 0x4e

    goto :goto_0

    :cond_0
    const/16 v0, 0x10

    :goto_0
    if-eq v0, v1, :cond_1

    goto :goto_1

    .line 35
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "appsflyer_backup_rules"

    const-string v3, "xml"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, v3, p0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    if-eqz p0, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    .line 36
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x2f

    rem-int/lit16 v1, p0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    :try_start_1
    const-string p0, "appsflyer_backup_rules.xml detected, using AppsFlyer defined backup rules for AppsFlyer SDK data"

    .line 37
    invoke-static {p0, v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 38
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p0, p0, 0x43

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p0, p0, 0x2

    return-void

    :cond_3
    :try_start_2
    const-string p0, "\'allowBackup\' is set to true; appsflyer_backup_rules.xml not detected.\nAppsFlyer shared preferences should be excluded from auto backup by adding: <exclude domain=\"sharedpref\" path=\"appsflyer-data\"/> to the Application\'s <full-backup-content> rules"

    .line 39
    invoke-static {p0}, Lcom/appsflyer/AFLogger;->valueOf(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception p0

    const-string v0, "checkBackupRules Exception"

    .line 40
    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "checkBackupRules Exception: "

    invoke-virtual {v0, p0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    return-void
.end method

.method private AFKeystoreWrapper(Lcom/appsflyer/AppsFlyerConversionListener;)V
    .locals 2

    .line 42
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0xb

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez p1, :cond_0

    add-int/lit8 v1, v1, 0x5d

    rem-int/lit16 p1, v1, 0x80

    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    return-void

    :cond_0
    iput-object p1, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName:Lcom/appsflyer/AppsFlyerConversionListener;

    return-void
.end method

.method private synthetic AFKeystoreWrapper(Lcom/appsflyer/internal/AFg1sSDK;)V
    .locals 5

    .line 25
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x19

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 26
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    invoke-virtual {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v0

    .line 27
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object v1

    .line 28
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v2, "appsFlyerCount"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "newGPReferrerSent"

    .line 29
    invoke-interface {v0, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    move-result v0

    .line 30
    iget-object p1, p1, Lcom/appsflyer/internal/AFg1mSDK;->afDebugLog:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 31
    sget-object v2, Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    const/4 v4, 0x1

    if-ne p1, v2, :cond_1

    .line 32
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x63

    rem-int/lit16 v2, p1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    if-ne v1, v4, :cond_6

    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x2f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x1

    :goto_2
    if-ne v1, v4, :cond_5

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    const/4 v3, 0x1

    :goto_3
    if-eq v3, v4, :cond_4

    if-eqz v0, :cond_6

    :cond_4
    new-instance p1, Lcom/appsflyer/internal/AFf1rSDK;

    invoke-direct {p1}, Lcom/appsflyer/internal/AFf1rSDK;-><init>()V

    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    goto :goto_4

    :cond_5
    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :cond_6
    :goto_4
    return-void
.end method

.method private static AFKeystoreWrapper(Ljava/lang/String;Z)V
    .locals 2

    .line 24
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x23

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Z)V

    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x45

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    return-void
.end method

.method private AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1uSDK;)Z
    .locals 5

    .line 43
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x73

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x0

    .line 44
    invoke-virtual {p0, p2, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFc1uSDK;Z)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eq v3, v2, :cond_1

    goto :goto_2

    .line 45
    :cond_1
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v3, v3, 0x7

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_2

    instance-of p1, p1, Lcom/appsflyer/internal/AFf1rSDK;

    const/16 v3, 0x1e

    :try_start_0
    div-int/2addr v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_3

    goto :goto_1

    :catchall_0
    move-exception p1

    throw p1

    .line 46
    :cond_2
    instance-of p1, p1, Lcom/appsflyer/internal/AFf1rSDK;

    if-nez p1, :cond_3

    :goto_1
    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p1, 0x0

    :goto_3
    const-string v3, "newGPReferrerSent"

    .line 47
    invoke-interface {p2, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_4

    const/4 p2, 0x0

    goto :goto_4

    :cond_4
    const/4 p2, 0x1

    :goto_4
    if-eq p2, v2, :cond_5

    if-ne v1, v2, :cond_5

    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p2, p2, 0x19

    rem-int/lit16 v1, p2, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p2, p2, 0x2

    const/4 p2, 0x1

    goto :goto_5

    :cond_5
    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p2, p2, 0x3f

    rem-int/lit16 v1, p2, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p2, p2, 0x2

    const/4 p2, 0x0

    :goto_5
    if-nez p2, :cond_8

    if-eqz p1, :cond_6

    const/4 p1, 0x0

    goto :goto_6

    :cond_6
    const/4 p1, 0x1

    :goto_6
    if-eq p1, v2, :cond_7

    goto :goto_7

    :cond_7
    return v0

    :cond_8
    :goto_7
    return v2
.end method

.method private AFLogger()Lcom/appsflyer/internal/AFg1sSDK;
    .locals 3

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFg1sSDK;

    new-instance v1, Lcom/appsflyer/internal/〇〇888;

    invoke-direct {v1, p0}, Lcom/appsflyer/internal/〇〇888;-><init>(Lcom/appsflyer/internal/AFa1cSDK;)V

    .line 2
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v2

    invoke-interface {v2}, Lcom/appsflyer/internal/AFc1qSDK;->AFKeystoreWrapper()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/appsflyer/internal/AFg1sSDK;-><init>(Ljava/lang/Runnable;Ljava/util/concurrent/ExecutorService;)V

    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x23

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    return-object v0
.end method

.method private static AFLogger(Landroid/content/Context;)V
    .locals 3

    .line 3
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x3b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v1, "android.permission.INTERNET"

    if-nez v0, :cond_0

    .line 4
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x4bd4

    invoke-virtual {v0, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    .line 5
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 6
    invoke-interface {p0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/16 v2, 0x1000

    invoke-virtual {v0, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    .line 8
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 9
    invoke-interface {p0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    const-string v0, "Permission android.permission.INTERNET is missing in the AndroidManifest.xml"

    .line 10
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x63

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    :cond_1
    :try_start_1
    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    .line 12
    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/16 v1, 0x5d

    if-nez v0, :cond_2

    const/16 v0, 0x1a

    goto :goto_1

    :cond_2
    const/16 v0, 0x5d

    :goto_1
    if-eq v0, v1, :cond_3

    const-string v0, "Permission android.permission.ACCESS_NETWORK_STATE is missing in the AndroidManifest.xml"

    .line 13
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 14
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x20

    if-le v0, v1, :cond_4

    const-string v0, "com.google.android.gms.permission.AD_ID"

    .line 15
    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez p0, :cond_4

    .line 16
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p0, p0, 0x2d

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p0, p0, 0x2

    :try_start_2
    const-string p0, "Permission com.google.android.gms.permission.AD_ID is missing in the AndroidManifest.xml"

    .line 17
    invoke-static {p0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 18
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x17

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    :cond_4
    return-void

    :catch_0
    move-exception p0

    const-string v0, "Exception while validation permissions. "

    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private synthetic AFLogger$LogLevel()V
    .locals 5

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x53

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-eq v0, v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->valueOf()Ljava/util/concurrent/ScheduledExecutorService;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v1, Lcom/appsflyer/internal/〇080;

    .line 28
    .line 29
    invoke-direct {v1, p0}, Lcom/appsflyer/internal/〇080;-><init>(Lcom/appsflyer/internal/AFa1cSDK;)V

    .line 30
    .line 31
    .line 32
    const-wide/16 v2, 0x0

    .line 33
    .line 34
    :goto_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 35
    .line 36
    invoke-static {v0, v1, v2, v3, v4}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V

    .line 37
    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->valueOf()Ljava/util/concurrent/ScheduledExecutorService;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, Lcom/appsflyer/internal/〇080;

    .line 49
    .line 50
    invoke-direct {v1, p0}, Lcom/appsflyer/internal/〇080;-><init>(Lcom/appsflyer/internal/AFa1cSDK;)V

    .line 51
    .line 52
    .line 53
    const-wide/16 v2, 0x1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :goto_2
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private AFVersionDeclaration()[Lcom/appsflyer/internal/AFg1mSDK;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x75

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x3f

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x3f

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x43

    .line 19
    .line 20
    :goto_0
    if-eq v0, v1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFg1nSDK;->values()[Lcom/appsflyer/internal/AFg1mSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    goto :goto_1

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFg1nSDK;->values()[Lcom/appsflyer/internal/AFg1mSDK;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const/16 v1, 0x39

    .line 48
    .line 49
    :try_start_0
    div-int/lit8 v1, v1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    .line 51
    :goto_1
    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    .line 53
    throw v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static synthetic O8(Lcom/appsflyer/internal/AFa1cSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger$LogLevel()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static synthetic Oo08(Lcom/appsflyer/internal/AFa1cSDK;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private static a(IBLjava/lang/String;[Ljava/lang/Object;)V
    .locals 12

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->$10:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x6f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->$11:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    if-eqz p2, :cond_0

    .line 12
    .line 13
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    :cond_0
    check-cast p2, [C

    .line 18
    .line 19
    new-instance v0, Lcom/appsflyer/internal/AFh1tSDK;

    .line 20
    .line 21
    invoke-direct {v0}, Lcom/appsflyer/internal/AFh1tSDK;-><init>()V

    .line 22
    .line 23
    .line 24
    sget-object v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseNative:[C

    .line 25
    .line 26
    const-wide v2, 0x46dcc64b3eacb52fL    # 2.3344717288991912E33

    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    const/4 v4, 0x0

    .line 32
    if-eqz v1, :cond_3

    .line 33
    .line 34
    array-length v5, v1

    .line 35
    new-array v6, v5, [C

    .line 36
    .line 37
    const/4 v7, 0x0

    .line 38
    :goto_0
    const/16 v8, 0x5a

    .line 39
    .line 40
    if-ge v7, v5, :cond_1

    .line 41
    .line 42
    const/16 v9, 0x5a

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    const/16 v9, 0x4e

    .line 46
    .line 47
    :goto_1
    if-eq v9, v8, :cond_2

    .line 48
    .line 49
    move-object v1, v6

    .line 50
    goto :goto_2

    .line 51
    :cond_2
    aget-char v8, v1, v7

    .line 52
    .line 53
    int-to-long v8, v8

    .line 54
    xor-long/2addr v8, v2

    .line 55
    long-to-int v9, v8

    .line 56
    int-to-char v8, v9

    .line 57
    aput-char v8, v6, v7

    .line 58
    .line 59
    add-int/lit8 v7, v7, 0x1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    :goto_2
    sget-char v5, Lcom/appsflyer/internal/AFa1cSDK;->onConversionDataSuccess:C

    .line 63
    .line 64
    int-to-long v5, v5

    .line 65
    xor-long/2addr v2, v5

    .line 66
    long-to-int v3, v2

    .line 67
    int-to-char v2, v3

    .line 68
    new-array v3, p0, [C

    .line 69
    .line 70
    rem-int/lit8 v5, p0, 0x2

    .line 71
    .line 72
    if-eqz v5, :cond_4

    .line 73
    .line 74
    add-int/lit8 v5, p0, -0x1

    .line 75
    .line 76
    aget-char v6, p2, v5

    .line 77
    .line 78
    sub-int/2addr v6, p1

    .line 79
    int-to-char v6, v6

    .line 80
    aput-char v6, v3, v5

    .line 81
    .line 82
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->$10:I

    .line 83
    .line 84
    add-int/lit8 v6, v6, 0x9

    .line 85
    .line 86
    rem-int/lit16 v7, v6, 0x80

    .line 87
    .line 88
    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->$11:I

    .line 89
    .line 90
    rem-int/lit8 v6, v6, 0x2

    .line 91
    .line 92
    goto :goto_3

    .line 93
    :cond_4
    move v5, p0

    .line 94
    :goto_3
    const/4 v6, 0x1

    .line 95
    if-le v5, v6, :cond_5

    .line 96
    .line 97
    const/4 v7, 0x0

    .line 98
    goto :goto_4

    .line 99
    :cond_5
    const/4 v7, 0x1

    .line 100
    :goto_4
    if-eqz v7, :cond_6

    .line 101
    .line 102
    goto :goto_8

    .line 103
    :cond_6
    sget v7, Lcom/appsflyer/internal/AFa1cSDK;->$11:I

    .line 104
    .line 105
    add-int/lit8 v7, v7, 0xb

    .line 106
    .line 107
    rem-int/lit16 v8, v7, 0x80

    .line 108
    .line 109
    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->$10:I

    .line 110
    .line 111
    rem-int/lit8 v7, v7, 0x2

    .line 112
    .line 113
    if-eqz v7, :cond_7

    .line 114
    .line 115
    const/4 v7, 0x0

    .line 116
    goto :goto_5

    .line 117
    :cond_7
    const/4 v7, 0x1

    .line 118
    :goto_5
    iput v4, v0, Lcom/appsflyer/internal/AFh1tSDK;->AFInAppEventParameterName:I

    .line 119
    .line 120
    :goto_6
    iget v7, v0, Lcom/appsflyer/internal/AFh1tSDK;->AFInAppEventParameterName:I

    .line 121
    .line 122
    if-ge v7, v5, :cond_8

    .line 123
    .line 124
    const/4 v8, 0x1

    .line 125
    goto :goto_7

    .line 126
    :cond_8
    const/4 v8, 0x0

    .line 127
    :goto_7
    if-eq v8, v6, :cond_b

    .line 128
    .line 129
    :goto_8
    const/4 p1, 0x0

    .line 130
    :goto_9
    const/16 p2, 0x2f

    .line 131
    .line 132
    if-ge p1, p0, :cond_9

    .line 133
    .line 134
    const/16 v0, 0x4a

    .line 135
    .line 136
    goto :goto_a

    .line 137
    :cond_9
    const/16 v0, 0x2f

    .line 138
    .line 139
    :goto_a
    if-eq v0, p2, :cond_a

    .line 140
    .line 141
    aget-char p2, v3, p1

    .line 142
    .line 143
    xor-int/lit16 p2, p2, 0x359a

    .line 144
    .line 145
    int-to-char p2, p2

    .line 146
    aput-char p2, v3, p1

    .line 147
    .line 148
    add-int/lit8 p1, p1, 0x1

    .line 149
    .line 150
    goto :goto_9

    .line 151
    :cond_a
    new-instance p0, Ljava/lang/String;

    .line 152
    .line 153
    invoke-direct {p0, v3}, Ljava/lang/String;-><init>([C)V

    .line 154
    .line 155
    .line 156
    aput-object p0, p3, v4

    .line 157
    .line 158
    return-void

    .line 159
    :cond_b
    aget-char v8, p2, v7

    .line 160
    .line 161
    iput-char v8, v0, Lcom/appsflyer/internal/AFh1tSDK;->AFKeystoreWrapper:C

    .line 162
    .line 163
    add-int/lit8 v9, v7, 0x1

    .line 164
    .line 165
    aget-char v9, p2, v9

    .line 166
    .line 167
    iput-char v9, v0, Lcom/appsflyer/internal/AFh1tSDK;->AFInAppEventType:C

    .line 168
    .line 169
    if-ne v8, v9, :cond_c

    .line 170
    .line 171
    sub-int/2addr v8, p1

    .line 172
    int-to-char v8, v8

    .line 173
    aput-char v8, v3, v7

    .line 174
    .line 175
    add-int/lit8 v8, v7, 0x1

    .line 176
    .line 177
    sub-int/2addr v9, p1

    .line 178
    int-to-char v9, v9

    .line 179
    aput-char v9, v3, v8

    .line 180
    .line 181
    goto :goto_b

    .line 182
    :cond_c
    div-int v10, v8, v2

    .line 183
    .line 184
    iput v10, v0, Lcom/appsflyer/internal/AFh1tSDK;->values:I

    .line 185
    .line 186
    rem-int/2addr v8, v2

    .line 187
    iput v8, v0, Lcom/appsflyer/internal/AFh1tSDK;->afRDLog:I

    .line 188
    .line 189
    div-int v11, v9, v2

    .line 190
    .line 191
    iput v11, v0, Lcom/appsflyer/internal/AFh1tSDK;->valueOf:I

    .line 192
    .line 193
    rem-int/2addr v9, v2

    .line 194
    iput v9, v0, Lcom/appsflyer/internal/AFh1tSDK;->afErrorLog:I

    .line 195
    .line 196
    if-ne v8, v9, :cond_d

    .line 197
    .line 198
    add-int/2addr v10, v2

    .line 199
    sub-int/2addr v10, v6

    .line 200
    rem-int/2addr v10, v2

    .line 201
    iput v10, v0, Lcom/appsflyer/internal/AFh1tSDK;->values:I

    .line 202
    .line 203
    add-int/2addr v11, v2

    .line 204
    sub-int/2addr v11, v6

    .line 205
    rem-int/2addr v11, v2

    .line 206
    iput v11, v0, Lcom/appsflyer/internal/AFh1tSDK;->valueOf:I

    .line 207
    .line 208
    mul-int v10, v10, v2

    .line 209
    .line 210
    add-int/2addr v10, v8

    .line 211
    mul-int v11, v11, v2

    .line 212
    .line 213
    add-int/2addr v11, v9

    .line 214
    aget-char v8, v1, v10

    .line 215
    .line 216
    aput-char v8, v3, v7

    .line 217
    .line 218
    add-int/lit8 v8, v7, 0x1

    .line 219
    .line 220
    aget-char v9, v1, v11

    .line 221
    .line 222
    aput-char v9, v3, v8

    .line 223
    .line 224
    goto :goto_b

    .line 225
    :cond_d
    if-ne v10, v11, :cond_e

    .line 226
    .line 227
    add-int/2addr v8, v2

    .line 228
    sub-int/2addr v8, v6

    .line 229
    rem-int/2addr v8, v2

    .line 230
    iput v8, v0, Lcom/appsflyer/internal/AFh1tSDK;->afRDLog:I

    .line 231
    .line 232
    add-int/2addr v9, v2

    .line 233
    sub-int/2addr v9, v6

    .line 234
    rem-int/2addr v9, v2

    .line 235
    iput v9, v0, Lcom/appsflyer/internal/AFh1tSDK;->afErrorLog:I

    .line 236
    .line 237
    mul-int v10, v10, v2

    .line 238
    .line 239
    add-int/2addr v10, v8

    .line 240
    mul-int v11, v11, v2

    .line 241
    .line 242
    add-int/2addr v11, v9

    .line 243
    aget-char v8, v1, v10

    .line 244
    .line 245
    aput-char v8, v3, v7

    .line 246
    .line 247
    add-int/lit8 v8, v7, 0x1

    .line 248
    .line 249
    aget-char v9, v1, v11

    .line 250
    .line 251
    aput-char v9, v3, v8

    .line 252
    .line 253
    goto :goto_b

    .line 254
    :cond_e
    mul-int v10, v10, v2

    .line 255
    .line 256
    add-int/2addr v10, v9

    .line 257
    mul-int v11, v11, v2

    .line 258
    .line 259
    add-int/2addr v11, v8

    .line 260
    aget-char v8, v1, v10

    .line 261
    .line 262
    aput-char v8, v3, v7

    .line 263
    .line 264
    add-int/lit8 v8, v7, 0x1

    .line 265
    .line 266
    aget-char v9, v1, v11

    .line 267
    .line 268
    aput-char v9, v3, v8

    .line 269
    .line 270
    :goto_b
    add-int/lit8 v7, v7, 0x2

    .line 271
    .line 272
    iput v7, v0, Lcom/appsflyer/internal/AFh1tSDK;->AFInAppEventParameterName:I

    .line 273
    .line 274
    goto/16 :goto_6
.end method

.method private afDebugLog()Z
    .locals 11

    .line 1
    iget-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    const/4 v4, 0x0

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-lez v5, :cond_9

    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v0

    .line 14
    iget-wide v2, p0, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog:J

    .line 15
    .line 16
    sub-long/2addr v0, v2

    .line 17
    new-instance v2, Ljava/text/SimpleDateFormat;

    .line 18
    .line 19
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 20
    .line 21
    const-string v5, "yyyy/MM/dd HH:mm:ss.SSS Z"

    .line 22
    .line 23
    invoke-direct {v2, v5, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 24
    .line 25
    .line 26
    iget-wide v5, p0, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog:J

    .line 27
    .line 28
    invoke-static {v2, v5, v6}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/text/SimpleDateFormat;J)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v5

    .line 32
    iget-wide v6, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger:J

    .line 33
    .line 34
    invoke-static {v2, v6, v7}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/text/SimpleDateFormat;J)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    iget-wide v6, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration:J

    .line 39
    .line 40
    const/16 v8, 0x5d

    .line 41
    .line 42
    cmp-long v9, v0, v6

    .line 43
    .line 44
    if-gez v9, :cond_0

    .line 45
    .line 46
    const/16 v6, 0x5d

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/16 v6, 0x49

    .line 50
    .line 51
    :goto_0
    const/4 v7, 0x3

    .line 52
    const/4 v9, 0x1

    .line 53
    const/4 v10, 0x2

    .line 54
    if-eq v6, v8, :cond_1

    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_1
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 58
    .line 59
    add-int/lit8 v6, v6, 0xb

    .line 60
    .line 61
    rem-int/lit16 v8, v6, 0x80

    .line 62
    .line 63
    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 64
    .line 65
    rem-int/2addr v6, v10

    .line 66
    if-nez v6, :cond_3

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    const/16 v8, 0xc

    .line 73
    .line 74
    :try_start_0
    div-int/2addr v8, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    const/16 v8, 0x34

    .line 76
    .line 77
    if-nez v6, :cond_2

    .line 78
    .line 79
    const/16 v6, 0x34

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    const/16 v6, 0x45

    .line 83
    .line 84
    :goto_1
    if-eq v6, v8, :cond_4

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :catchall_0
    move-exception v0

    .line 88
    throw v0

    .line 89
    :cond_3
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    .line 90
    .line 91
    .line 92
    move-result v6

    .line 93
    if-nez v6, :cond_7

    .line 94
    .line 95
    :cond_4
    const/4 v6, 0x4

    .line 96
    new-array v6, v6, [Ljava/lang/Object;

    .line 97
    .line 98
    aput-object v5, v6, v4

    .line 99
    .line 100
    aput-object v2, v6, v9

    .line 101
    .line 102
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    aput-object v0, v6, v10

    .line 107
    .line 108
    iget-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration:J

    .line 109
    .line 110
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    aput-object v0, v6, v7

    .line 115
    .line 116
    const-string v0, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nThis launch is blocked: %s ms < %s ms"

    .line 117
    .line 118
    invoke-static {v3, v0, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 126
    .line 127
    add-int/lit8 v0, v0, 0x41

    .line 128
    .line 129
    rem-int/lit16 v1, v0, 0x80

    .line 130
    .line 131
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 132
    .line 133
    rem-int/2addr v0, v10

    .line 134
    if-eqz v0, :cond_5

    .line 135
    .line 136
    goto :goto_2

    .line 137
    :cond_5
    const/4 v4, 0x1

    .line 138
    :goto_2
    if-ne v4, v9, :cond_6

    .line 139
    .line 140
    return v9

    .line 141
    :cond_6
    const/4 v0, 0x0

    .line 142
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 143
    :catchall_1
    move-exception v0

    .line 144
    throw v0

    .line 145
    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    .line 146
    .line 147
    .line 148
    move-result v6

    .line 149
    const/16 v8, 0x39

    .line 150
    .line 151
    if-nez v6, :cond_8

    .line 152
    .line 153
    const/16 v6, 0x4a

    .line 154
    .line 155
    goto :goto_4

    .line 156
    :cond_8
    const/16 v6, 0x39

    .line 157
    .line 158
    :goto_4
    if-eq v6, v8, :cond_a

    .line 159
    .line 160
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 161
    .line 162
    add-int/lit8 v6, v6, 0x11

    .line 163
    .line 164
    rem-int/lit16 v8, v6, 0x80

    .line 165
    .line 166
    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 167
    .line 168
    rem-int/2addr v6, v10

    .line 169
    new-array v6, v7, [Ljava/lang/Object;

    .line 170
    .line 171
    aput-object v5, v6, v4

    .line 172
    .line 173
    aput-object v2, v6, v9

    .line 174
    .line 175
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    aput-object v0, v6, v10

    .line 180
    .line 181
    const-string v0, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nSending launch (+%s ms)"

    .line 182
    .line 183
    invoke-static {v3, v0, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    goto :goto_5

    .line 191
    :cond_9
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    .line 192
    .line 193
    .line 194
    move-result v0

    .line 195
    if-nez v0, :cond_a

    .line 196
    .line 197
    const-string v0, "Sending first launch for this session!"

    .line 198
    .line 199
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    :cond_a
    :goto_5
    return v4
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method private afErrorLog()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger()Lcom/appsflyer/internal/AFg1sSDK;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1sSDK;)Ljava/lang/Runnable;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFg1nSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1mSDK;)V

    .line 18
    .line 19
    .line 20
    new-instance v1, Lcom/appsflyer/internal/AFg1qSDK;

    .line 21
    .line 22
    invoke-direct {v1, v2}, Lcom/appsflyer/internal/AFg1qSDK;-><init>(Ljava/lang/Runnable;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFg1nSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1mSDK;)V

    .line 26
    .line 27
    .line 28
    new-instance v1, Lcom/appsflyer/internal/AFg1rSDK;

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    invoke-direct {v1, v2, v3}, Lcom/appsflyer/internal/AFg1rSDK;-><init>(Ljava/lang/Runnable;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFg1nSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1mSDK;)V

    .line 38
    .line 39
    .line 40
    new-instance v1, Lcom/appsflyer/internal/AFg1oSDK;

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-direct {v1, v2, v3}, Lcom/appsflyer/internal/AFg1oSDK;-><init>(Ljava/lang/Runnable;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/appsflyer/internal/AFg1nSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1mSDK;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-nez v1, :cond_0

    .line 57
    .line 58
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-virtual {v0, v1, v2, v3}, Lcom/appsflyer/internal/AFg1nSDK;->AFInAppEventParameterName(Landroid/content/Context;Ljava/lang/Runnable;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 65
    .line 66
    .line 67
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 68
    .line 69
    add-int/lit8 v1, v1, 0x31

    .line 70
    .line 71
    rem-int/lit16 v2, v1, 0x80

    .line 72
    .line 73
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 74
    .line 75
    rem-int/lit8 v1, v1, 0x2

    .line 76
    .line 77
    :cond_0
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFg1nSDK;->values()[Lcom/appsflyer/internal/AFg1mSDK;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    array-length v1, v0

    .line 82
    const/4 v2, 0x0

    .line 83
    const/4 v3, 0x0

    .line 84
    :goto_0
    const/16 v4, 0x5a

    .line 85
    .line 86
    if-ge v3, v1, :cond_1

    .line 87
    .line 88
    const/16 v5, 0x50

    .line 89
    .line 90
    goto :goto_1

    .line 91
    :cond_1
    const/16 v5, 0x5a

    .line 92
    .line 93
    :goto_1
    if-eq v5, v4, :cond_2

    .line 94
    .line 95
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 96
    .line 97
    add-int/lit8 v4, v4, 0x9

    .line 98
    .line 99
    rem-int/lit16 v5, v4, 0x80

    .line 100
    .line 101
    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 102
    .line 103
    rem-int/lit8 v4, v4, 0x2

    .line 104
    .line 105
    aget-object v4, v0, v3

    .line 106
    .line 107
    iget-object v5, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    .line 108
    .line 109
    invoke-virtual {v4, v5}, Lcom/appsflyer/internal/AFg1mSDK;->AFInAppEventParameterName(Landroid/content/Context;)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v3, v3, 0x1

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 116
    .line 117
    add-int/lit8 v0, v0, 0x1b

    .line 118
    .line 119
    rem-int/lit16 v1, v0, 0x80

    .line 120
    .line 121
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 122
    .line 123
    rem-int/lit8 v0, v0, 0x2

    .line 124
    .line 125
    if-nez v0, :cond_3

    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_3
    const/4 v2, 0x1

    .line 129
    :goto_2
    if-eqz v2, :cond_4

    .line 130
    .line 131
    return-void

    .line 132
    :cond_4
    const/4 v0, 0x0

    .line 133
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :catchall_0
    move-exception v0

    .line 135
    throw v0
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private synthetic afInfoLog(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x43

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib;->getAttributionId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x4d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x5

    if-eqz v0, :cond_0

    const/16 v0, 0x47

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    :goto_0
    if-ne v0, v1, :cond_1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private afInfoLog()V
    .locals 4

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x79

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 3
    invoke-static {}, Lcom/appsflyer/internal/AFe1ySDK;->afInfoLog()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLogForExcManagerOnly()Lcom/appsflyer/internal/AFd1fSDK;

    move-result-object v1

    new-instance v2, Lcom/appsflyer/internal/AFe1ySDK;

    invoke-direct {v2, v0}, Lcom/appsflyer/internal/AFe1ySDK;-><init>(Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 6
    iget-object v0, v1, Lcom/appsflyer/internal/AFd1fSDK;->values:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/appsflyer/internal/AFd1fSDK$3;

    invoke-direct {v3, v1, v2}, Lcom/appsflyer/internal/AFd1fSDK$3;-><init>(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x6b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x39

    if-eqz v0, :cond_1

    const/16 v0, 0x11

    goto :goto_0

    :cond_1
    const/16 v0, 0x39

    :goto_0
    if-ne v0, v1, :cond_2

    return-void

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private afRDLog()Z
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    const/4 v1, 0x7

    .line 4
    add-int/2addr v0, v1

    .line 5
    rem-int/lit16 v2, v0, 0x80

    .line 6
    .line 7
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v2, "AF_PREINSTALL_DISABLED"

    .line 20
    .line 21
    invoke-virtual {v0, v2}, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    sget v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 26
    .line 27
    add-int/lit8 v2, v2, 0x17

    .line 28
    .line 29
    rem-int/lit16 v3, v2, 0x80

    .line 30
    .line 31
    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 32
    .line 33
    rem-int/lit8 v2, v2, 0x2

    .line 34
    .line 35
    if-eqz v2, :cond_0

    .line 36
    .line 37
    const/4 v2, 0x7

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/16 v2, 0x33

    .line 40
    .line 41
    :goto_0
    if-eq v2, v1, :cond_1

    .line 42
    .line 43
    return v0

    .line 44
    :cond_1
    const/4 v0, 0x0

    .line 45
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :catchall_0
    move-exception v0

    .line 47
    throw v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private synthetic afWarnLog()V
    .locals 4

    .line 1
    :try_start_0
    new-instance v0, Lcom/appsflyer/internal/AFf1rSDK;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/appsflyer/internal/AFf1rSDK;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    .line 7
    .line 8
    invoke-virtual {p0, v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-direct {p0, v0, v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1uSDK;)Z

    .line 13
    .line 14
    .line 15
    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 16
    const/16 v2, 0x4e

    .line 17
    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    const/16 v1, 0x3b

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/16 v1, 0x4e

    .line 24
    .line 25
    :goto_0
    const/4 v3, 0x0

    .line 26
    if-eq v1, v2, :cond_3

    .line 27
    .line 28
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x37

    .line 31
    .line 32
    rem-int/lit16 v2, v1, 0x80

    .line 33
    .line 34
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 35
    .line 36
    rem-int/lit8 v1, v1, 0x2

    .line 37
    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    const/4 v1, 0x0

    .line 41
    goto :goto_1

    .line 42
    :cond_1
    const/4 v1, 0x1

    .line 43
    :goto_1
    if-eqz v1, :cond_2

    .line 44
    .line 45
    :try_start_1
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 46
    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_2
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 50
    .line 51
    .line 52
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 53
    :cond_3
    :goto_2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 54
    .line 55
    add-int/lit8 v0, v0, 0x5d

    .line 56
    .line 57
    rem-int/lit16 v1, v0, 0x80

    .line 58
    .line 59
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 60
    .line 61
    rem-int/lit8 v0, v0, 0x2

    .line 62
    .line 63
    const/16 v1, 0x3a

    .line 64
    .line 65
    if-nez v0, :cond_4

    .line 66
    .line 67
    const/16 v0, 0x8

    .line 68
    .line 69
    goto :goto_3

    .line 70
    :cond_4
    const/16 v0, 0x3a

    .line 71
    .line 72
    :goto_3
    if-ne v0, v1, :cond_5

    .line 73
    .line 74
    return-void

    .line 75
    :cond_5
    :try_start_2
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 76
    :catchall_0
    move-exception v0

    .line 77
    throw v0

    .line 78
    :catchall_1
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public static synthetic o〇0(Lcom/appsflyer/internal/AFa1cSDK;Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog(Landroid/content/Context;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private valueOf(Lcom/appsflyer/internal/AFc1uSDK;)I
    .locals 4

    .line 45
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x61

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "appsFlyerAdRevenueCount"

    invoke-direct {p0, p1, v3, v2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I

    move-result p1

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x4d

    rem-int/lit16 v3, v0, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    const/16 v0, 0x28

    :try_start_0
    div-int/2addr v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return p1

    :catchall_0
    move-exception p1

    throw p1

    :cond_2
    return p1
.end method

.method private valueOf(Lcom/appsflyer/internal/AFc1uSDK;Z)I
    .locals 3

    .line 71
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x71

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "appsFlyerInAppEventCount"

    invoke-direct {p0, p1, v2, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I

    move-result p1

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 p2, 0x2b

    :try_start_0
    div-int/2addr p2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_1
    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p2, p2, 0x49

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p2, p2, 0x2

    const/16 v0, 0x1e

    if-nez p2, :cond_2

    const/16 p2, 0x1e

    goto :goto_2

    :cond_2
    const/16 p2, 0x16

    :goto_2
    if-eq p2, v0, :cond_3

    return p1

    :cond_3
    const/16 p2, 0x5b

    :try_start_1
    div-int/2addr p2, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return p1

    :catchall_0
    move-exception p1

    throw p1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method private valueOf(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 53
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x35

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eq v2, v0, :cond_1

    .line 54
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/appsflyer/internal/AFc1tSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    add-int/lit8 v1, v1, 0x2d

    rem-int/lit16 p1, v1, 0x80

    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 p1, 0x0

    return-object p1
.end method

.method public static valueOf(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 62
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x79

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "CACHED_CHANNEL"

    const/4 v4, 0x0

    if-nez v0, :cond_0

    .line 63
    invoke-interface {p0, v3, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v5, 0x1a

    .line 64
    :try_start_0
    div-int/2addr v5, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_6

    goto :goto_1

    :catchall_0
    move-exception p0

    .line 65
    throw p0

    .line 66
    :cond_0
    invoke-interface {p0, v3, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    :goto_0
    if-eqz v5, :cond_6

    .line 67
    :goto_1
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p0, 0xd

    rem-int/lit16 v3, p1, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x1

    :goto_2
    if-ne v2, v1, :cond_5

    add-int/lit8 p0, p0, 0x41

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const/4 p1, 0x4

    if-nez p0, :cond_3

    const/16 p0, 0x39

    goto :goto_3

    :cond_3
    const/4 p0, 0x4

    :goto_3
    if-ne p0, p1, :cond_4

    return-object v0

    :cond_4
    :try_start_1
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p0

    throw p0

    .line 68
    :cond_5
    :try_start_2
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception p0

    .line 69
    throw p0

    .line 70
    :cond_6
    invoke-interface {p0, v3, p1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public static valueOf(Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "meta"

    .line 40
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0xb

    if-eqz v1, :cond_0

    const/16 v1, 0x4f

    goto :goto_0

    :cond_0
    const/16 v1, 0xb

    :goto_0
    if-eq v1, v2, :cond_1

    .line 41
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x73

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    .line 42
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;

    goto :goto_1

    .line 43
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p0, p0, 0x7

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p0, p0, 0x2

    move-object p0, v1

    :goto_1
    return-object p0
.end method

.method private valueOf(Landroid/content/Context;Ljava/util/Map;Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/internal/AFa1uSDK;",
            ")V"
        }
    .end annotation

    .line 3
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 4
    invoke-virtual {p3, p2}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 5
    instance-of p2, p1, Landroid/app/Activity;

    const/16 v0, 0x38

    const/4 v1, 0x2

    if-eqz p2, :cond_0

    const/16 p2, 0x38

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    :goto_0
    const/4 v2, 0x0

    if-eq p2, v0, :cond_1

    goto :goto_2

    :cond_1
    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p2, p2, 0x63

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/2addr p2, v1

    const/16 v0, 0x3b

    if-nez p2, :cond_2

    const/16 p2, 0x3b

    goto :goto_1

    :cond_2
    const/16 p2, 0x18

    :goto_1
    if-eq p2, v0, :cond_3

    move-object v2, p1

    check-cast v2, Landroid/app/Activity;

    :goto_2
    invoke-virtual {p0, p3, v2}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x5f

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/2addr p1, v1

    return-void

    :cond_3
    check-cast p1, Landroid/app/Activity;

    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private valueOf(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 5

    .line 21
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x61

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    const/16 v4, 0x4b

    :try_start_0
    div-int/2addr v4, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 23
    throw p1

    .line 24
    :cond_0
    iget-object v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    if-nez v0, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x37

    .line 25
    rem-int/lit16 v0, v1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v0, 0x0

    .line 26
    :goto_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->values()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 27
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x61

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    const-string p1, "CustomerUserId not set, reporting is disabled"

    if-eqz v2, :cond_3

    .line 28
    invoke-static {p1, v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    return-void

    :cond_3
    invoke-static {p1, v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    return-void

    :cond_4
    const/16 v1, 0x1d

    if-eqz v0, :cond_5

    const/16 v0, 0x1d

    goto :goto_2

    :cond_5
    const/16 v0, 0x23

    :goto_2
    if-eq v0, v1, :cond_6

    goto :goto_7

    .line 29
    :cond_6
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "launchProtectEnabled"

    .line 30
    invoke-virtual {v0, v1, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x6

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    goto :goto_3

    :cond_7
    const/16 v0, 0x29

    :goto_3
    if-eq v0, v1, :cond_8

    const-string v0, "Allowing multiple launches within a 5 second time window."

    .line 31
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    goto :goto_6

    .line 32
    :cond_8
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afDebugLog()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 33
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x47

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 34
    iget-object p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->values:Lcom/appsflyer/attribution/AppsFlyerRequestListener;

    const/16 v0, 0x5c

    if-eqz p1, :cond_9

    const/16 v1, 0x5c

    goto :goto_4

    :cond_9
    const/16 v1, 0x13

    :goto_4
    if-eq v1, v0, :cond_a

    goto :goto_5

    :cond_a
    const/16 v0, 0xa

    const-string v1, "Event timeout. Check \'minTimeBetweenSessions\' param"

    .line 35
    invoke-interface {p1, v0, v1}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    :goto_5
    return-void

    .line 36
    :cond_b
    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afRDLog:J

    .line 37
    :goto_7
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->valueOf()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/appsflyer/internal/AFa1cSDK$AFa1vSDK;

    invoke-direct {v1, p0, p1, v2}, Lcom/appsflyer/internal/AFa1cSDK$AFa1vSDK;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFa1uSDK;B)V

    const-wide/16 v2, 0x0

    .line 39
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method private static valueOf(Ljava/lang/String;)V
    .locals 2

    .line 56
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "pid"

    .line 57
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    const/16 v1, 0xe

    if-eqz v0, :cond_0

    const/16 v0, 0x56

    goto :goto_0

    :cond_0
    const/16 v0, 0xe

    :goto_0
    if-eq v0, v1, :cond_2

    const-string v0, "preInstallName"

    .line 58
    invoke-static {v0, p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x57

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    if-nez p0, :cond_1

    const/16 p0, 0x5d

    :try_start_1
    div-int/lit8 p0, p0, 0x0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception p0

    throw p0

    :cond_1
    return-void

    :cond_2
    :try_start_2
    const-string p0, "Cannot set preinstall attribution data without a media source"

    .line 60
    invoke-static {p0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 61
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p0, p0, 0x75

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p0, p0, 0x2

    const/16 v0, 0x2b

    if-eqz p0, :cond_3

    const/16 p0, 0x58

    goto :goto_1

    :cond_3
    const/16 p0, 0x2b

    :goto_1
    if-eq p0, v0, :cond_4

    const/16 p0, 0x4f

    :try_start_3
    div-int/lit8 p0, p0, 0x0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void

    :catchall_1
    move-exception p0

    throw p0

    :cond_4
    return-void

    :catch_0
    move-exception p0

    const-string v0, "Error parsing JSON for preinstall"

    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static valueOf(Landroid/content/Context;)Z
    .locals 3

    .line 46
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x65

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x53

    if-nez v0, :cond_0

    const/16 v0, 0x1c

    goto :goto_0

    :cond_0
    const/16 v0, 0x53

    :goto_0
    const/4 v2, 0x1

    if-ne v0, v1, :cond_3

    .line 47
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x33

    if-nez v0, :cond_1

    const/16 v0, 0x54

    goto :goto_1

    :cond_1
    const/16 v0, 0x33

    :goto_1
    if-eq v0, v1, :cond_2

    .line 48
    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x79

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    return v2

    .line 49
    :cond_2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0xd

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    const/4 v0, 0x0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    const-string v1, "WARNING:  Google play services is unavailable. "

    .line 50
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    const/4 v0, 0x0

    .line 51
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v1, "com.google.android.gms"

    invoke-virtual {p0, v1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    return v2

    :catch_0
    move-exception p0

    const-string v1, "WARNING:  Google Play Services is unavailable. "

    .line 52
    invoke-static {v1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method static synthetic valueOf(Lcom/appsflyer/internal/AFa1cSDK;Z)Z
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v0, 0x69

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v2, 0x5d

    if-nez v1, :cond_0

    const/16 v1, 0x3d

    goto :goto_0

    :cond_0
    const/16 v1, 0x5d

    :goto_0
    const/4 v3, 0x0

    iput-boolean p1, p0, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLogForExcManagerOnly:Z

    if-ne v1, v2, :cond_3

    add-int/lit8 v0, v0, 0x1d

    rem-int/lit16 p0, v0, 0x80

    sput p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    const/16 p0, 0x50

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    if-nez p0, :cond_2

    return p1

    :cond_2
    :try_start_0
    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :cond_3
    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p0

    throw p0
.end method

.method static synthetic values(Lcom/appsflyer/internal/AFa1cSDK;J)J
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x33

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v2, 0x59

    if-eqz v0, :cond_0

    const/16 v0, 0x59

    goto :goto_0

    :cond_0
    const/16 v0, 0x52

    :goto_0
    iput-wide p1, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger:J

    if-eq v0, v2, :cond_1

    add-int/lit8 v1, v1, 0x49

    rem-int/lit16 p0, v1, 0x80

    sput p0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    return-wide p1

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method static synthetic values(Lcom/appsflyer/internal/AFa1cSDK;)Landroid/app/Application;
    .locals 2

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x1b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object p0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    if-eqz v0, :cond_1

    const/16 v0, 0x24

    :try_start_0
    div-int/2addr v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p0

    throw p0

    :cond_1
    :goto_1
    return-object p0
.end method

.method private static values(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 18
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x51

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x29

    if-nez v0, :cond_0

    const/16 v0, 0x29

    goto :goto_0

    :cond_0
    const/16 v0, 0x28

    :goto_0
    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x43

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    return-object p0

    :cond_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private synthetic values(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .line 6
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 7
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    move-result-object v0

    .line 8
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v1

    const/16 v2, 0x50

    if-eqz p2, :cond_0

    const/16 v3, 0x62

    goto :goto_0

    :cond_0
    const/16 v3, 0x50

    :goto_0
    const/4 v4, 0x0

    if-eq v3, v2, :cond_1

    .line 9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 10
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 11
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v3, v3, 0x5b

    rem-int/lit16 v5, v3, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v3, v3, 0x2

    goto :goto_1

    :cond_1
    move-object v2, v4

    :goto_1
    if-eqz v2, :cond_2

    .line 12
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    const-string v3, "ddl_sent"

    .line 13
    invoke-interface {v1, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    move-result v1

    const/16 v3, 0x1d

    if-eqz v1, :cond_3

    const/16 v1, 0x1d

    goto :goto_3

    :cond_3
    const/16 v1, 0x27

    :goto_3
    if-eq v1, v3, :cond_4

    goto :goto_5

    .line 14
    :cond_4
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x15

    rem-int/lit16 v3, v1, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v3, 0x52

    if-eqz v1, :cond_5

    const/16 v1, 0x54

    goto :goto_4

    :cond_5
    const/16 v1, 0x52

    :goto_4
    if-ne v1, v3, :cond_7

    if-nez v2, :cond_6

    const-string p1, "No direct deep link"

    .line 15
    invoke-virtual {v0, p1, v4}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    return-void

    .line 16
    :cond_6
    :goto_5
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v1, p2, p1}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/util/Map;Landroid/content/Intent;Landroid/content/Context;)V

    return-void

    :cond_7
    :try_start_0
    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private values(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 13

    .line 20
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x4d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    const/4 v1, 0x2

    rem-int/2addr v0, v1

    .line 21
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;

    move-result-object v0

    .line 22
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 23
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x7b

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr p1, v1

    const-string v0, "sendWithEvent - got null context. skipping event/launch."

    if-nez p1, :cond_0

    .line 24
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 25
    throw p1

    .line 26
    :cond_1
    invoke-virtual {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v2

    .line 27
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/appsflyer/AppsFlyerProperties;->saveProperties(Lcom/appsflyer/internal/AFc1uSDK;)V

    .line 28
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v3

    invoke-interface {v3}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object v3

    invoke-virtual {v3}, Lcom/appsflyer/internal/AFe1hSDK;->values()Z

    move-result v3

    if-nez v3, :cond_2

    .line 29
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendWithEvent from activity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 30
    :cond_2
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result v0

    .line 31
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;)Ljava/util/Map;

    move-result-object v3

    .line 32
    iget-object v4, p1, Lcom/appsflyer/internal/AFa1uSDK;->values:Lcom/appsflyer/attribution/AppsFlyerRequestListener;

    const-string v5, "No dev key"

    const/16 v6, 0x29

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-nez v3, :cond_5

    if-eqz v4, :cond_3

    goto :goto_0

    :cond_3
    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_4

    goto :goto_1

    .line 33
    :cond_4
    invoke-interface {v4, v6, v5}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    :goto_1
    return-void

    :cond_5
    const-string v9, "appsflyerKey"

    .line 34
    invoke-interface {v3, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    if-eqz v9, :cond_1b

    .line 35
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x11

    if-nez v9, :cond_6

    const/16 v9, 0x30

    goto :goto_2

    :cond_6
    const/16 v9, 0x11

    :goto_2
    if-eq v9, v10, :cond_7

    goto/16 :goto_e

    .line 36
    :cond_7
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "AppsFlyerLib.sendWithEvent"

    .line 37
    invoke-static {v4}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 38
    :cond_8
    invoke-virtual {p0, v2, v7}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFc1uSDK;Z)I

    move-result v2

    .line 39
    new-instance v4, Lcom/appsflyer/internal/AFg1kSDK;

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v5

    invoke-interface {v5}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/appsflyer/internal/AFg1kSDK;-><init>(Lcom/appsflyer/internal/AFc1tSDK;)V

    const-string v5, ""

    .line 40
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result v5

    .line 42
    instance-of v9, p1, Lcom/appsflyer/internal/AFf1qSDK;

    .line 43
    instance-of v10, p1, Lcom/appsflyer/internal/AFf1uSDK;

    .line 44
    instance-of v11, p1, Lcom/appsflyer/internal/AFf1oSDK;

    .line 45
    instance-of v12, p1, Lcom/appsflyer/internal/AFf1rSDK;

    if-nez v12, :cond_10

    if-eqz v11, :cond_9

    goto :goto_5

    :cond_9
    if-eqz v10, :cond_a

    .line 46
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->AFInAppEventParameterName:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_a
    if-eqz v9, :cond_b

    .line 47
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->values:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_b
    if-eqz v5, :cond_c

    const/4 v5, 0x0

    goto :goto_3

    :cond_c
    const/4 v5, 0x1

    :goto_3
    if-eq v5, v8, :cond_f

    .line 48
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1tSDK;

    .line 49
    iget-object v5, v5, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v10, "appsFlyerCount"

    invoke-interface {v5, v10, v7}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v5

    if-ge v5, v1, :cond_d

    const/4 v5, 0x0

    goto :goto_4

    :cond_d
    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_e

    .line 50
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->afDebugLog:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 51
    :cond_e
    sget v5, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v5, v5, 0x37

    rem-int/lit16 v10, v5, 0x80

    sput v10, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr v5, v1

    .line 52
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->afErrorLog:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 53
    :cond_f
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->AFLogger:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    .line 54
    :cond_10
    :goto_5
    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->valueOf:Lcom/appsflyer/internal/AFg1eSDK;

    sget-object v10, Lcom/appsflyer/internal/AFg1kSDK;->AFKeystoreWrapper:Ljava/lang/String;

    invoke-interface {v5, v10}, Lcom/appsflyer/internal/AFg1eSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 55
    :goto_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lcom/appsflyer/internal/AFg1kSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1tSDK;

    .line 56
    iget-object v5, v5, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 57
    iget-object v5, v5, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 58
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 59
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 60
    invoke-static {v5, v9}, Lcom/appsflyer/internal/AFg1kSDK;->values(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 61
    invoke-virtual {v4, v5}, Lcom/appsflyer/internal/AFg1kSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-direct {p0, v3}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Ljava/util/Map;)V

    .line 63
    new-instance v5, Lcom/appsflyer/internal/AFb1nSDK;

    .line 64
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v9

    .line 65
    invoke-virtual {p1, v4}, Lcom/appsflyer/internal/AFa1uSDK;->valueOf(Ljava/lang/String;)Lcom/appsflyer/internal/AFa1uSDK;

    move-result-object p1

    .line 66
    invoke-virtual {p1, v3}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    move-result-object p1

    .line 67
    invoke-virtual {p1, v2}, Lcom/appsflyer/internal/AFa1uSDK;->values(I)Lcom/appsflyer/internal/AFa1uSDK;

    move-result-object p1

    .line 68
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v2

    invoke-interface {v2}, Lcom/appsflyer/internal/AFc1qSDK;->init()Lcom/appsflyer/internal/AFb1fSDK;

    move-result-object v2

    invoke-interface {v2}, Lcom/appsflyer/internal/AFb1fSDK;->values()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v5, v9, p1, v2}, Lcom/appsflyer/internal/AFb1nSDK;-><init>(Lcom/appsflyer/internal/AFc1qSDK;Lcom/appsflyer/internal/AFa1uSDK;Ljava/util/Map;)V

    if-eqz v0, :cond_19

    .line 69
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x7b

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr p1, v1

    if-eqz p1, :cond_11

    const/4 p1, 0x0

    goto :goto_7

    :cond_11
    const/4 p1, 0x1

    :goto_7
    if-eq p1, v8, :cond_12

    .line 70
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration()[Lcom/appsflyer/internal/AFg1mSDK;

    move-result-object p1

    array-length v0, p1

    const/4 v2, 0x1

    goto :goto_8

    :cond_12
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration()[Lcom/appsflyer/internal/AFg1mSDK;

    move-result-object p1

    array-length v0, p1

    const/4 v2, 0x0

    :goto_8
    const/4 v3, 0x0

    :goto_9
    if-ge v2, v0, :cond_15

    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v4, v4, 0xf

    rem-int/lit16 v9, v4, 0x80

    sput v9, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr v4, v1

    if-eqz v4, :cond_13

    aget-object v4, p1, v2

    .line 71
    iget-object v9, v4, Lcom/appsflyer/internal/AFg1mSDK;->afDebugLog:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 72
    sget-object v10, Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    const/16 v11, 0x45

    :try_start_1
    div-int/2addr v11, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-ne v9, v10, :cond_14

    goto :goto_a

    :catchall_1
    move-exception p1

    .line 73
    throw p1

    :cond_13
    aget-object v4, p1, v2

    .line 74
    iget-object v9, v4, Lcom/appsflyer/internal/AFg1mSDK;->afDebugLog:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 75
    sget-object v10, Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    if-ne v9, v10, :cond_14

    .line 76
    :goto_a
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v9, "Failed to get "

    invoke-direct {v3, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 77
    iget-object v4, v4, Lcom/appsflyer/internal/AFg1mSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " referrer, wait ..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    const/4 v3, 0x1

    :cond_14
    add-int/lit8 v2, v2, 0x1

    .line 79
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/2addr v4, v6

    rem-int/lit16 v9, v4, 0x80

    sput v9, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/2addr v4, v1

    goto :goto_9

    .line 80
    :cond_15
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->init()Lcom/appsflyer/internal/AFb1fSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFb1fSDK;->AFInAppEventType()Z

    move-result p1

    if-eqz p1, :cond_16

    const/4 v7, 0x1

    :cond_16
    if-eqz v7, :cond_17

    const-string p1, "fetching Facebook deferred AppLink data, wait ..."

    .line 81
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    const/4 v7, 0x1

    goto :goto_b

    :cond_17
    move v7, v3

    .line 82
    :goto_b
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object p1

    invoke-virtual {p1}, Lcom/appsflyer/internal/AFe1hSDK;->AFKeystoreWrapper()Z

    move-result p1

    const/16 v0, 0xa

    if-eqz p1, :cond_18

    const/16 p1, 0x4b

    goto :goto_c

    :cond_18
    const/16 p1, 0xa

    :goto_c
    if-eq p1, v0, :cond_19

    .line 83
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x1b

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr p1, v1

    const/4 v7, 0x1

    .line 84
    :cond_19
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->valueOf()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    if-eqz v7, :cond_1a

    .line 85
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x19

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr v0, v1

    const-wide/16 v0, 0x1f4

    goto :goto_d

    :cond_1a
    const-wide/16 v0, 0x0

    .line 86
    :goto_d
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, v5, v0, v1, v2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V

    return-void

    :cond_1b
    :goto_e
    const-string p1, "Not sending data yet, waiting for dev key"

    .line 87
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    if-eqz v4, :cond_1c

    .line 88
    invoke-interface {v4, v6, v5}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    :cond_1c
    return-void
.end method

.method private static synthetic values(Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 2

    .line 17
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x75

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-interface {p0}, Lcom/appsflyer/internal/AFc1qSDK;->onAppOpenAttributionNative()Lcom/appsflyer/internal/AFb1xSDK;

    move-result-object p0

    invoke-interface {p0}, Lcom/appsflyer/internal/AFb1xSDK;->valueOf()V

    sget p0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p0, p0, 0x29

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p0, p0, 0x2

    const/16 v0, 0x41

    if-nez p0, :cond_0

    const/16 p0, 0x41

    goto :goto_0

    :cond_0
    const/16 p0, 0x1f

    :goto_0
    if-eq p0, v0, :cond_1

    return-void

    :cond_1
    const/16 p0, 0x61

    :try_start_0
    div-int/lit8 p0, p0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p0

    throw p0
.end method

.method public static synthetic 〇080(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFg1sSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFg1sSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/appsflyer/internal/AFa1cSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static synthetic 〇o〇(Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/appsflyer/internal/AFa1cSDK;->values(Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static synthetic 〇〇888(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFe1kSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFe1kSDK;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;
    .locals 2

    .line 80
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x51

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 81
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 82
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object p1

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x75

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    return-object p1
.end method

.method public final AFInAppEventParameterName(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .line 2
    new-instance v0, Lcom/appsflyer/internal/AFg1iSDK;

    invoke-direct {v0, p2}, Lcom/appsflyer/internal/AFg1iSDK;-><init>(Landroid/content/Intent;)V

    const-string p2, "appsflyer_preinstall"

    .line 3
    invoke-virtual {v0, p2}, Lcom/appsflyer/internal/AFg1iSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    .line 4
    :cond_1
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x65

    rem-int/lit16 v4, v1, 0x80

    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    const/4 v2, 0x0

    .line 5
    :cond_2
    invoke-virtual {v0, p2}, Lcom/appsflyer/internal/AFg1iSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Ljava/lang/String;)V

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    const/16 p2, 0x4c

    :try_start_0
    div-int/2addr p2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    const-string p2, "****** onReceive called *******"

    .line 6
    invoke-static {p2}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 7
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    const-string p2, "referrer"

    .line 8
    invoke-virtual {v0, p2}, Lcom/appsflyer/internal/AFg1iSDK;->values(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 9
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Play store referrer: "

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    .line 10
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p2

    const-string v1, "AF_REFERRER"

    .line 12
    invoke-virtual {p2, v1, v0}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    iput-object v0, p2, Lcom/appsflyer/AppsFlyerProperties;->values:Ljava/lang/String;

    .line 14
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p2

    invoke-virtual {p2}, Lcom/appsflyer/AppsFlyerProperties;->valueOf()Z

    move-result p2

    if-eqz p2, :cond_4

    const-string p2, "onReceive: isLaunchCalled"

    .line 15
    invoke-static {p2}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 16
    sget-object p2, Lcom/appsflyer/internal/AFf1sSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1sSDK;

    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;Lcom/appsflyer/internal/AFf1sSDK;)V

    .line 17
    invoke-direct {p0, p1, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Landroid/content/Context;Ljava/lang/String;)V

    .line 18
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x3b

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    :cond_4
    return-void

    :catchall_0
    move-exception p1

    .line 19
    throw p1
.end method

.method public final AFInAppEventType(Lcom/appsflyer/internal/AFc1uSDK;Z)I
    .locals 3

    .line 57
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x13

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x51

    if-nez v0, :cond_0

    const/16 v0, 0x51

    goto :goto_0

    :cond_0
    const/16 v0, 0x59

    :goto_0
    const-string v2, "appsFlyerCount"

    if-eq v0, v1, :cond_1

    invoke-direct {p0, p1, v2, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I

    move-result p1

    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p2, p2, 0x25

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p2, p2, 0x2

    return p1

    :cond_1
    invoke-direct {p0, p1, v2, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;Z)I

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method final AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;)Ljava/util/Map;
    .locals 14
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFa1uSDK;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;

    move-result-object v0

    .line 49
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 50
    invoke-virtual {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v1

    .line 51
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v2

    invoke-interface {v2}, Lcom/appsflyer/internal/AFc1qSDK;->afDebugLog()Lcom/appsflyer/internal/AFe1aSDK;

    move-result-object v2

    .line 52
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result v3

    .line 53
    iget-object v4, p1, Lcom/appsflyer/internal/AFa1uSDK;->valueOf:Ljava/util/Map;

    .line 54
    invoke-static {v0, v4}, Lcom/appsflyer/internal/AFa1dSDK;->AFKeystoreWrapper(Landroid/content/Context;Ljava/util/Map;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 55
    sget-object v5, Lcom/appsflyer/internal/AFa1dSDK;->AFInAppEventParameterName:Ljava/lang/Boolean;

    const/16 v6, 0x9

    if-eqz v5, :cond_0

    const/16 v7, 0x9

    goto :goto_0

    :cond_0
    const/4 v7, 0x3

    :goto_0
    const/4 v8, 0x0

    if-eq v7, v6, :cond_1

    goto :goto_2

    .line 56
    :cond_1
    sget v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v7, v7, 0x5b

    rem-int/lit16 v9, v7, 0x80

    sput v9, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/16 v7, 0x58

    :try_start_0
    div-int/2addr v7, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_3

    goto :goto_1

    :catchall_0
    move-exception p1

    throw p1

    .line 57
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_3

    .line 58
    :goto_1
    invoke-static {v4}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    const-string v7, "ad_ids_disabled"

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v5, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_3
    :goto_2
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    const-string v5, ""

    const/16 v7, 0x30

    .line 60
    invoke-static {v5, v7, v8, v8}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    move-result v5

    rsub-int/lit8 v5, v5, 0xb

    invoke-static {v8}, Landroid/graphics/Color;->alpha(I)I

    move-result v7

    rsub-int/lit8 v7, v7, 0x54

    int-to-byte v7, v7

    const/4 v11, 0x1

    new-array v12, v11, [Ljava/lang/Object;

    const-string v13, "\u0003\u0006\u0003\u0005\u0000\u0007\u0001\u0008\u0003\u0001\u0007\u0006"

    invoke-static {v5, v7, v13, v12}, Lcom/appsflyer/internal/AFa1cSDK;->a(IBLjava/lang/String;[Ljava/lang/Object;)V

    aget-object v5, v12, v8

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :try_start_1
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    move-result v5

    if-nez v5, :cond_5

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "******* sendTrackingWithEvent: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_4

    const-string v7, "Launch"

    goto :goto_3

    .line 63
    :cond_4
    iget-object v7, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    .line 64
    :goto_3
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 65
    sget v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v5, v5, 0x49

    rem-int/lit16 v7, v5, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v5, v5, 0x2

    goto :goto_4

    :cond_5
    :try_start_2
    const-string v5, "Reporting has been stopped"

    .line 66
    invoke-static {v5}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 67
    :goto_4
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v5

    invoke-interface {v5}, Lcom/appsflyer/internal/AFc1qSDK;->onAppOpenAttributionNative()Lcom/appsflyer/internal/AFb1xSDK;

    move-result-object v5

    invoke-interface {v5}, Lcom/appsflyer/internal/AFb1xSDK;->AFKeystoreWrapper()V

    .line 68
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger(Landroid/content/Context;)V

    .line 69
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v5

    .line 70
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v7

    invoke-interface {v7}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object v7

    .line 71
    iget-object v7, v7, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v9, 0x0

    if-eqz v7, :cond_e

    .line 72
    sget v10, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v10, v10, 0x37

    rem-int/lit16 v12, v10, 0x80

    sput v12, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v10, v10, 0x2

    if-eqz v10, :cond_d

    .line 73
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_6

    goto :goto_8

    .line 74
    :cond_6
    iget-object v7, p0, Lcom/appsflyer/internal/AFa1cSDK;->AppsFlyer2dXConversionCallback:Ljava/lang/String;

    iget-object v10, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFc1xSDK;

    invoke-interface {v2, p1, v7, v10}, Lcom/appsflyer/internal/AFe1aSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Ljava/lang/String;Lcom/appsflyer/internal/AFc1xSDK;)V

    .line 75
    iget-object v7, p0, Lcom/appsflyer/internal/AFa1cSDK;->afDebugLog:Lcom/appsflyer/internal/AFb1aSDK;

    invoke-interface {v2, v4, v7}, Lcom/appsflyer/internal/AFe1aSDK;->AFInAppEventParameterName(Ljava/util/Map;Lcom/appsflyer/internal/AFb1aSDK;)V

    .line 76
    invoke-virtual {p0, v0}, Lcom/appsflyer/AppsFlyerLib;->isPreInstalledApp(Landroid/content/Context;)Z

    move-result v7

    new-instance v10, Lcom/appsflyer/internal/Oo08;

    invoke-direct {v10, p0, v0}, Lcom/appsflyer/internal/Oo08;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Landroid/content/Context;)V

    .line 77
    invoke-interface {v2, v4, v7, v10}, Lcom/appsflyer/internal/AFe1aSDK;->AFKeystoreWrapper(Ljava/util/Map;ZLkotlin/jvm/functions/Function0;)V

    if-eqz v3, :cond_7

    .line 78
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, Lcom/appsflyer/internal/AFe1aSDK;->values(Ljava/util/Map;Ljava/lang/String;)V

    .line 79
    iput-object v9, p0, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    .line 80
    invoke-interface {v2, p1}, Lcom/appsflyer/internal/AFe1aSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 81
    :cond_7
    invoke-interface {v2, v4}, Lcom/appsflyer/internal/AFe1aSDK;->valueOf(Ljava/util/Map;)V

    .line 82
    invoke-virtual {p0, v1, v3}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFc1uSDK;Z)I

    move-result v0

    .line 83
    iget-object v7, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    if-eqz v7, :cond_8

    const/4 v8, 0x1

    :cond_8
    invoke-direct {p0, v1, v8}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFc1uSDK;Z)I

    move-result v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/16 v7, 0x63

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    goto :goto_5

    :cond_9
    const/16 v3, 0x63

    :goto_5
    if-eq v3, v7, :cond_c

    .line 84
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/2addr v3, v6

    rem-int/lit16 v6, v3, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v3, v3, 0x2

    const/16 v7, 0x46

    if-eqz v3, :cond_a

    const/16 v3, 0x46

    goto :goto_6

    :cond_a
    const/16 v3, 0x5e

    :goto_6
    if-eq v3, v7, :cond_b

    if-ne v0, v11, :cond_c

    goto :goto_7

    :cond_b
    if-nez v0, :cond_c

    .line 85
    :goto_7
    :try_start_4
    iput-boolean v11, v5, Lcom/appsflyer/AppsFlyerProperties;->valueOf:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    add-int/lit8 v6, v6, 0x6f

    .line 86
    rem-int/lit16 v3, v6, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v6, v6, 0x2

    .line 87
    :cond_c
    :try_start_5
    invoke-interface {v2, v4, v0, v1}, Lcom/appsflyer/internal/AFe1aSDK;->values(Ljava/util/Map;II)V

    .line 88
    iget-object v1, p0, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog:Ljava/lang/String;

    invoke-interface {v2, p1, v0, v1}, Lcom/appsflyer/internal/AFe1aSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;ILjava/lang/String;)V

    goto :goto_9

    .line 89
    :cond_d
    throw v9

    :cond_e
    :goto_8
    const-string p1, "AppsFlyer dev key is missing!!! Please use  AppsFlyerLib.getInstance().setAppsFlyerKey(...) to set it. "

    .line 90
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    const-string p1, "AppsFlyer will not track this event."

    .line 91
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-object v9

    :catchall_1
    move-exception p1

    .line 92
    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, v11}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    :goto_9
    return-object v4
.end method

.method public final AFKeystoreWrapper(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11

    const-string v0, "extraReferrers"

    .line 2
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x4d

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    .line 3
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "received a new (extra) referrer: "

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 4
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 5
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v3

    const/4 v4, 0x0

    .line 6
    invoke-interface {v3, v0, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 7
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 8
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    goto :goto_1

    .line 9
    :cond_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-virtual {v4, p2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 11
    new-instance v3, Lorg/json/JSONArray;

    invoke-virtual {v4, p2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v3, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    :goto_0
    move-object v10, v4

    move-object v4, v3

    move-object v3, v10

    goto :goto_1

    .line 12
    :cond_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    sget v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v5, v5, 0xb

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v5, v5, 0x2

    goto :goto_0

    .line 14
    :goto_1
    :try_start_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    int-to-long v5, v5

    const-wide/16 v7, 0x5

    cmp-long v9, v5, v7

    if-gez v9, :cond_2

    .line 15
    invoke-virtual {v4, v1, v2}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 16
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x4b

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    .line 17
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Lorg/json/JSONObject;->length()I

    move-result v1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    int-to-long v1, v1

    const-wide/16 v5, 0x4

    const/16 v7, 0x26

    cmp-long v8, v1, v5

    if-ltz v8, :cond_3

    const/16 v1, 0x5d

    goto :goto_2

    :cond_3
    const/16 v1, 0x26

    :goto_2
    if-eq v1, v7, :cond_4

    .line 18
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v1, v1, 0x73

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v1, v1, 0x2

    .line 19
    :try_start_3
    invoke-static {v3}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lorg/json/JSONObject;)V

    .line 20
    :cond_4
    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 21
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object p1

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t save referrer - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ": "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    const-string p2, "error at addReferrer"

    .line 23
    invoke-static {p2, p1}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final varargs addPushNotificationDeepLinkPath([Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v0, v0, Lcom/appsflyer/internal/AFb1kSDK;->valueOf:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v1, 0x0

    .line 24
    :goto_0
    if-eqz v1, :cond_1

    .line 25
    .line 26
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 27
    .line 28
    add-int/lit8 v1, v1, 0x79

    .line 29
    .line 30
    rem-int/lit16 v2, v1, 0x80

    .line 31
    .line 32
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 33
    .line 34
    rem-int/lit8 v1, v1, 0x2

    .line 35
    .line 36
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    :cond_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 40
    .line 41
    add-int/lit8 p1, p1, 0x3f

    .line 42
    .line 43
    rem-int/lit16 v0, p1, 0x80

    .line 44
    .line 45
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 46
    .line 47
    rem-int/lit8 p1, p1, 0x2

    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final anonymizeUser(Z)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x5d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    aput-object v3, v1, v2

    .line 28
    .line 29
    const-string v2, "anonymizeUser"

    .line 30
    .line 31
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const-string v1, "deviceTrackingDisabled"

    .line 39
    .line 40
    invoke-virtual {v0, v1, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Z)V

    .line 41
    .line 42
    .line 43
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 44
    .line 45
    add-int/lit8 p1, p1, 0x15

    .line 46
    .line 47
    rem-int/lit16 v0, p1, 0x80

    .line 48
    .line 49
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 50
    .line 51
    rem-int/lit8 p1, p1, 0x2

    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final appendParametersToDeepLinkingURL(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x31

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    rem-int/2addr v0, v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    :goto_0
    if-eq v0, v3, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object p1, v0, Lcom/appsflyer/internal/AFb1kSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 29
    .line 30
    iput-object p2, v0, Lcom/appsflyer/internal/AFb1kSDK;->values:Ljava/util/Map;

    .line 31
    .line 32
    :try_start_0
    div-int/2addr v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    goto :goto_1

    .line 34
    :catchall_0
    move-exception p1

    .line 35
    throw p1

    .line 36
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object p1, v0, Lcom/appsflyer/internal/AFb1kSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 45
    .line 46
    iput-object p2, v0, Lcom/appsflyer/internal/AFb1kSDK;->values:Ljava/util/Map;

    .line 47
    .line 48
    :goto_1
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final enableFacebookDeferredApplinks(Z)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->init()Lcom/appsflyer/internal/AFb1fSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-interface {v0, p1}, Lcom/appsflyer/internal/AFb1fSDK;->AFInAppEventParameterName(Z)V

    .line 20
    .line 21
    .line 22
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 23
    .line 24
    add-int/lit8 p1, p1, 0x3d

    .line 25
    .line 26
    rem-int/lit16 v0, p1, 0x80

    .line 27
    .line 28
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 29
    .line 30
    rem-int/lit8 p1, p1, 0x2

    .line 31
    .line 32
    const/16 v0, 0x50

    .line 33
    .line 34
    if-nez p1, :cond_0

    .line 35
    .line 36
    const/16 p1, 0x50

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/16 p1, 0x20

    .line 40
    .line 41
    :goto_0
    if-eq p1, v0, :cond_1

    .line 42
    .line 43
    return-void

    .line 44
    :cond_1
    const/4 p1, 0x0

    .line 45
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :catchall_0
    move-exception p1

    .line 47
    throw p1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final getAppsFlyerUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    new-array v1, v1, [Ljava/lang/String;

    .line 11
    .line 12
    const-string v2, "getAppsFlyerUID"

    .line 13
    .line 14
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/16 v0, 0x46

    .line 18
    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    const/16 v1, 0x15

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/16 v1, 0x46

    .line 25
    .line 26
    :goto_0
    if-eq v1, v0, :cond_1

    .line 27
    .line 28
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 29
    .line 30
    add-int/lit8 p1, p1, 0xf

    .line 31
    .line 32
    rem-int/lit16 v0, p1, 0x80

    .line 33
    .line 34
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 35
    .line 36
    rem-int/lit8 p1, p1, 0x2

    .line 37
    .line 38
    const/4 p1, 0x0

    .line 39
    return-object p1

    .line 40
    :cond_1
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iget-object v0, p1, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 52
    .line 53
    iget-object p1, p1, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 54
    .line 55
    invoke-static {v0, p1}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 60
    .line 61
    add-int/lit8 v0, v0, 0x7b

    .line 62
    .line 63
    rem-int/lit16 v1, v0, 0x80

    .line 64
    .line 65
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 66
    .line 67
    rem-int/lit8 v0, v0, 0x2

    .line 68
    .line 69
    return-object p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final getAttributionId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    :try_start_0
    new-instance v0, Lcom/appsflyer/internal/AFb1zSDK;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, p1, v1}, Lcom/appsflyer/internal/AFb1zSDK;-><init>(Landroid/content/Context;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFb1zSDK;->AFInAppEventParameterName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 15
    .line 16
    add-int/lit8 v0, v0, 0x67

    .line 17
    .line 18
    rem-int/lit16 v1, v0, 0x80

    .line 19
    .line 20
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 21
    .line 22
    rem-int/lit8 v0, v0, 0x2

    .line 23
    .line 24
    return-object p1

    .line 25
    :catchall_0
    move-exception p1

    .line 26
    const-string v0, "Could not collect facebook attribution id. "

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    const/4 p1, 0x0

    .line 32
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final getHostName()Ljava/lang/String;
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x55

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionDataLoadedNative()Lcom/appsflyer/internal/AFd1rSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFd1rSDK;->valueOf()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    add-int/2addr v1, v2

    .line 27
    rem-int/lit16 v3, v1, 0x80

    .line 28
    .line 29
    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 30
    .line 31
    rem-int/lit8 v1, v1, 0x2

    .line 32
    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    const/4 v1, 0x0

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v1, 0x1

    .line 38
    :goto_0
    if-ne v1, v2, :cond_1

    .line 39
    .line 40
    return-object v0

    .line 41
    :cond_1
    const/4 v0, 0x0

    .line 42
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :catchall_0
    move-exception v0

    .line 44
    throw v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final getHostPrefix()Ljava/lang/String;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x9

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x30

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x2c

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x30

    .line 19
    .line 20
    :goto_0
    if-ne v0, v1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionDataLoadedNative()Lcom/appsflyer/internal/AFd1rSDK;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFd1rSDK;->values()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    return-object v0

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionDataLoadedNative()Lcom/appsflyer/internal/AFd1rSDK;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFd1rSDK;->values()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :catchall_0
    move-exception v0

    .line 49
    throw v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final getOutOfStore(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .line 1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "api_store_value"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x1

    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 17
    .line 18
    add-int/lit8 p1, p1, 0x37

    .line 19
    .line 20
    rem-int/lit16 v4, p1, 0x80

    .line 21
    .line 22
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 23
    .line 24
    rem-int/lit8 p1, p1, 0x2

    .line 25
    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v1, 0x1

    .line 30
    :goto_0
    if-ne v1, v3, :cond_1

    .line 31
    .line 32
    return-object v0

    .line 33
    :cond_1
    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :catchall_0
    move-exception p1

    .line 35
    throw p1

    .line 36
    :cond_2
    const-string v0, "AF_STORE"

    .line 37
    .line 38
    invoke-direct {p0, p1, v0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    const/16 v0, 0x3b

    .line 43
    .line 44
    if-eqz p1, :cond_3

    .line 45
    .line 46
    const/16 v4, 0x3b

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_3
    const/16 v4, 0x19

    .line 50
    .line 51
    :goto_1
    if-eq v4, v0, :cond_4

    .line 52
    .line 53
    const-string p1, "No out-of-store value set"

    .line 54
    .line 55
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    return-object v2

    .line 59
    :cond_4
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 60
    .line 61
    add-int/lit8 v0, v0, 0x45

    .line 62
    .line 63
    rem-int/lit16 v4, v0, 0x80

    .line 64
    .line 65
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 66
    .line 67
    rem-int/lit8 v0, v0, 0x2

    .line 68
    .line 69
    if-eqz v0, :cond_5

    .line 70
    .line 71
    const/4 v1, 0x1

    .line 72
    :cond_5
    if-nez v1, :cond_6

    .line 73
    .line 74
    return-object p1

    .line 75
    :cond_6
    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 76
    :catchall_1
    move-exception p1

    .line 77
    throw p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final getSdkVersion()Ljava/lang/String;
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0xb

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x0

    .line 20
    new-array v2, v2, [Ljava/lang/String;

    .line 21
    .line 22
    const-string v3, "getSdkVersion"

    .line 23
    .line 24
    invoke-interface {v1, v3, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 28
    .line 29
    .line 30
    invoke-static {}, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 35
    .line 36
    add-int/lit8 v1, v1, 0x1

    .line 37
    .line 38
    rem-int/lit16 v2, v1, 0x80

    .line 39
    .line 40
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 41
    .line 42
    rem-int/lit8 v1, v1, 0x2

    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final init(Ljava/lang/String;Lcom/appsflyer/AppsFlyerConversionListener;Landroid/content/Context;)Lcom/appsflyer/AppsFlyerLib;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-boolean v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger$LogLevel:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object p0

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger$LogLevel:Z

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    iput-object p1, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/appsflyer/internal/AFb1rSDK;->AFInAppEventType(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    const/4 v2, 0x2

    .line 24
    if-eqz p3, :cond_6

    .line 25
    .line 26
    invoke-virtual {p0, p3}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    invoke-static {p3}, Lcom/appsflyer/internal/AFa1eSDK;->values(Landroid/content/Context;)Landroid/app/Application;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    const/16 v3, 0x3b

    .line 34
    .line 35
    if-eqz p3, :cond_1

    .line 36
    .line 37
    const/16 v4, 0x3b

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 v4, 0x2

    .line 41
    :goto_0
    if-eq v4, v3, :cond_2

    .line 42
    .line 43
    return-object p0

    .line 44
    :cond_2
    iput-object p3, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 47
    .line 48
    .line 49
    move-result-object p3

    .line 50
    invoke-interface {p3}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger()Lcom/appsflyer/internal/AFf1tSDK;

    .line 51
    .line 52
    .line 53
    move-result-object p3

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 55
    .line 56
    .line 57
    move-result-wide v4

    .line 58
    iput-wide v4, p3, Lcom/appsflyer/internal/AFf1tSDK;->valueOf:J

    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 61
    .line 62
    .line 63
    move-result-object p3

    .line 64
    invoke-interface {p3}, Lcom/appsflyer/internal/AFc1qSDK;->onResponseNative()Lcom/appsflyer/internal/AFc1jSDK;

    .line 65
    .line 66
    .line 67
    move-result-object p3

    .line 68
    invoke-interface {p3}, Lcom/appsflyer/internal/AFc1jSDK;->valueOf()V

    .line 69
    .line 70
    .line 71
    new-instance p3, Lcom/appsflyer/internal/O8;

    .line 72
    .line 73
    invoke-direct {p3, p0}, Lcom/appsflyer/internal/O8;-><init>(Lcom/appsflyer/internal/AFa1cSDK;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    invoke-interface {v4}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLog()Lcom/appsflyer/internal/AFe1oSDK;

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    invoke-virtual {v4, p3}, Lcom/appsflyer/internal/AFe1oSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFe1lSDK;)V

    .line 85
    .line 86
    .line 87
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afErrorLog()V

    .line 88
    .line 89
    .line 90
    iget-object p3, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;

    .line 91
    .line 92
    invoke-virtual {p3}, Lcom/appsflyer/internal/AFc1oSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 93
    .line 94
    .line 95
    move-result-object p3

    .line 96
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    invoke-interface {v4}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 101
    .line 102
    .line 103
    move-result-object v4

    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 105
    .line 106
    .line 107
    move-result-wide v5

    .line 108
    iput-wide v5, p3, Lcom/appsflyer/internal/AFe1hSDK;->valueOf:J

    .line 109
    .line 110
    iget-object v5, p3, Lcom/appsflyer/internal/AFe1hSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFe1jSDK;

    .line 111
    .line 112
    new-instance v6, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    iget-object v7, v4, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 118
    .line 119
    iget-object v4, v4, Lcom/appsflyer/internal/AFc1tSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 120
    .line 121
    invoke-static {v7, v4}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v4

    .line 125
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    iget-wide v7, p3, Lcom/appsflyer/internal/AFe1hSDK;->valueOf:J

    .line 129
    .line 130
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    invoke-static {v4}, Lcom/appsflyer/internal/AFb1wSDK;->AFKeystoreWrapper(Ljava/lang/String;)[B

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    if-eqz v4, :cond_5

    .line 142
    .line 143
    array-length v6, v4

    .line 144
    const/16 v7, 0x3e

    .line 145
    .line 146
    if-lez v6, :cond_3

    .line 147
    .line 148
    goto :goto_1

    .line 149
    :cond_3
    const/16 v3, 0x3e

    .line 150
    .line 151
    :goto_1
    if-eq v3, v7, :cond_5

    .line 152
    .line 153
    array-length v3, v4

    .line 154
    const/16 v6, 0x8

    .line 155
    .line 156
    if-le v3, v6, :cond_4

    .line 157
    .line 158
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 159
    .line 160
    add-int/lit8 v3, v3, 0x75

    .line 161
    .line 162
    rem-int/lit16 v7, v3, 0x80

    .line 163
    .line 164
    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 165
    .line 166
    rem-int/2addr v3, v2

    .line 167
    invoke-static {v4, v1, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    .line 168
    .line 169
    .line 170
    move-result-object v4

    .line 171
    :cond_4
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 172
    .line 173
    .line 174
    move-result-object v3

    .line 175
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getLong()J

    .line 182
    .line 183
    .line 184
    move-result-wide v3

    .line 185
    goto :goto_2

    .line 186
    :cond_5
    const-wide/16 v3, -0x1

    .line 187
    .line 188
    :goto_2
    iget-object v6, p3, Lcom/appsflyer/internal/AFe1hSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 189
    .line 190
    iget-object v6, v6, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 191
    .line 192
    new-instance v7, Lcom/appsflyer/internal/AFe1hSDK$2;

    .line 193
    .line 194
    invoke-direct {v7, p3}, Lcom/appsflyer/internal/AFe1hSDK$2;-><init>(Lcom/appsflyer/internal/AFe1hSDK;)V

    .line 195
    .line 196
    .line 197
    invoke-virtual {v5, v3, v4, v6, v7}, Lcom/appsflyer/internal/AFe1jSDK;->AFInAppEventType(JLandroid/content/Context;Lcom/appsflyer/internal/AFe1jSDK$AFa1ySDK;)Z

    .line 198
    .line 199
    .line 200
    move-result v3

    .line 201
    iput-boolean v3, p3, Lcom/appsflyer/internal/AFe1hSDK;->AFInAppEventParameterName:Z

    .line 202
    .line 203
    goto :goto_3

    .line 204
    :cond_6
    const-string p3, "context is null, Google Install Referrer will be not initialized"

    .line 205
    .line 206
    invoke-static {p3}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 207
    .line 208
    .line 209
    sget p3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 210
    .line 211
    add-int/lit8 p3, p3, 0x51

    .line 212
    .line 213
    rem-int/lit16 v3, p3, 0x80

    .line 214
    .line 215
    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 216
    .line 217
    rem-int/2addr p3, v2

    .line 218
    :goto_3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 219
    .line 220
    .line 221
    move-result-object p3

    .line 222
    invoke-interface {p3}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 223
    .line 224
    .line 225
    move-result-object p3

    .line 226
    new-array v3, v2, [Ljava/lang/String;

    .line 227
    .line 228
    aput-object p1, v3, v1

    .line 229
    .line 230
    if-nez p2, :cond_7

    .line 231
    .line 232
    const/4 p1, 0x1

    .line 233
    goto :goto_4

    .line 234
    :cond_7
    const/4 p1, 0x0

    .line 235
    :goto_4
    if-eq p1, v0, :cond_8

    .line 236
    .line 237
    const-string p1, "conversionDataListener"

    .line 238
    .line 239
    goto :goto_7

    .line 240
    :cond_8
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 241
    .line 242
    add-int/lit8 p1, p1, 0x1d

    .line 243
    .line 244
    rem-int/lit16 v4, p1, 0x80

    .line 245
    .line 246
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 247
    .line 248
    rem-int/2addr p1, v2

    .line 249
    if-eqz p1, :cond_9

    .line 250
    .line 251
    const/4 p1, 0x0

    .line 252
    goto :goto_5

    .line 253
    :cond_9
    const/4 p1, 0x1

    .line 254
    :goto_5
    if-eq p1, v0, :cond_a

    .line 255
    .line 256
    const/16 p1, 0xb

    .line 257
    .line 258
    :try_start_0
    div-int/2addr p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    goto :goto_6

    .line 260
    :catchall_0
    move-exception p1

    .line 261
    throw p1

    .line 262
    :cond_a
    :goto_6
    const-string p1, "null"

    .line 263
    .line 264
    :goto_7
    aput-object p1, v3, v0

    .line 265
    .line 266
    const-string p1, "init"

    .line 267
    .line 268
    invoke-interface {p3, p1, v3}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    new-array p1, v2, [Ljava/lang/Object;

    .line 272
    .line 273
    const-string p3, "6.12.4"

    .line 274
    .line 275
    aput-object p3, p1, v1

    .line 276
    .line 277
    sget-object p3, Lcom/appsflyer/internal/AFa1cSDK;->values:Ljava/lang/String;

    .line 278
    .line 279
    aput-object p3, p1, v0

    .line 280
    .line 281
    const-string p3, "Initializing AppsFlyer SDK: (v%s.%s)"

    .line 282
    .line 283
    invoke-static {p3, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 284
    .line 285
    .line 286
    move-result-object p1

    .line 287
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->values(Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    iput-object p2, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName:Lcom/appsflyer/AppsFlyerConversionListener;

    .line 291
    .line 292
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 293
    .line 294
    add-int/lit8 p1, p1, 0x29

    .line 295
    .line 296
    rem-int/lit16 p2, p1, 0x80

    .line 297
    .line 298
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 299
    .line 300
    rem-int/2addr p1, v2

    .line 301
    return-object p0
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
.end method

.method public final isPreInstalledApp(Landroid/content/Context;)Z
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x5f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iget p1, p1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    and-int/2addr p1, v1

    .line 28
    const/16 v2, 0x1a

    .line 29
    .line 30
    if-eqz p1, :cond_0

    .line 31
    .line 32
    const/16 p1, 0x43

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/16 p1, 0x1a

    .line 36
    .line 37
    :goto_0
    if-eq p1, v2, :cond_1

    .line 38
    .line 39
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 40
    .line 41
    add-int/lit8 p1, p1, 0x41

    .line 42
    .line 43
    rem-int/lit16 v0, p1, 0x80

    .line 44
    .line 45
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 46
    .line 47
    rem-int/lit8 p1, p1, 0x2

    .line 48
    .line 49
    return v1

    .line 50
    :cond_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 51
    .line 52
    add-int/lit8 p1, p1, 0x13

    .line 53
    .line 54
    rem-int/lit16 v1, p1, 0x80

    .line 55
    .line 56
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 57
    .line 58
    rem-int/lit8 p1, p1, 0x2

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :catch_0
    move-exception p1

    .line 62
    const-string v1, "Could not check if app is pre installed"

    .line 63
    .line 64
    invoke-static {v1, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    :goto_1
    return v0
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final isStopped()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFe1hSDK;->values()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1d

    .line 26
    .line 27
    rem-int/lit16 v2, v1, 0x80

    .line 28
    .line 29
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 30
    .line 31
    rem-int/lit8 v1, v1, 0x2

    .line 32
    .line 33
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final logEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x41

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/appsflyer/AppsFlyerLib;->logEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Lcom/appsflyer/attribution/AppsFlyerRequestListener;)V

    if-nez v0, :cond_3

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/2addr p1, v1

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :cond_1
    if-eqz v1, :cond_2

    return-void

    :cond_2
    const/16 p1, 0x58

    :try_start_0
    div-int/2addr p1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1

    :cond_3
    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method public final logEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Lcom/appsflyer/attribution/AppsFlyerRequestListener;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/attribution/AppsFlyerRequestListener;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p3, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3
    :goto_0
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 4
    new-instance p3, Lcom/appsflyer/internal/AFf1mSDK;

    invoke-direct {p3}, Lcom/appsflyer/internal/AFf1mSDK;-><init>()V

    .line 5
    iput-object p2, p3, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    .line 6
    iput-object p4, p3, Lcom/appsflyer/internal/AFa1uSDK;->values:Lcom/appsflyer/attribution/AppsFlyerRequestListener;

    if-eqz v1, :cond_2

    const-string p4, "af_touch_obj"

    .line 7
    invoke-interface {v1, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 8
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 9
    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 10
    instance-of v4, v3, Landroid/view/MotionEvent;

    if-eqz v4, :cond_1

    .line 11
    check-cast v3, Landroid/view/MotionEvent;

    .line 12
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 13
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, "x"

    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-string v6, "y"

    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "loc"

    .line 15
    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPressure()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-string v5, "pf"

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getTouchMajor()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-string v4, "rad"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string v3, "error"

    const-string v4, "Parsing failed due to invalid input in \'af_touch_obj\'."

    .line 18
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    invoke-static {v4}, Lcom/appsflyer/AFLogger;->valueOf(Ljava/lang/String;)V

    :goto_1
    const-string v3, "tch_data"

    .line 20
    invoke-static {v3, v2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    .line 21
    invoke-interface {v1, p4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    invoke-virtual {p3, v2}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper(Ljava/util/Map;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 23
    :cond_2
    iput-object v1, p3, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventParameterName:Ljava/util/Map;

    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p4

    invoke-interface {p4}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object p4

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 25
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p3, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventParameterName:Ljava/util/Map;

    if-nez v3, :cond_3

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    :cond_3
    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "logEvent"

    .line 26
    invoke-interface {p4, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    if-nez p2, :cond_4

    .line 27
    sget-object p2, Lcom/appsflyer/internal/AFf1sSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFf1sSDK;

    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;Lcom/appsflyer/internal/AFf1sSDK;)V

    .line 28
    :cond_4
    instance-of p2, p1, Landroid/app/Activity;

    if-eqz p2, :cond_5

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    :cond_5
    invoke-virtual {p0, p3, v0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V

    return-void
.end method

.method public final logLocation(Landroid/content/Context;DD)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x2

    .line 10
    new-array v2, v1, [Ljava/lang/String;

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    aput-object v4, v2, v3

    .line 18
    .line 19
    const/4 v3, 0x1

    .line 20
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v4

    .line 24
    aput-object v4, v2, v3

    .line 25
    .line 26
    const-string v3, "logLocation"

    .line 27
    .line 28
    invoke-interface {v0, v3, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    new-instance v0, Ljava/util/HashMap;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v2, "af_long"

    .line 37
    .line 38
    invoke-static {p4, p5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p4

    .line 42
    invoke-interface {v0, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    const-string p4, "af_lat"

    .line 46
    .line 47
    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    invoke-interface {v0, p4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    const-string p2, "af_location_coordinates"

    .line 55
    .line 56
    invoke-direct {p0, p1, p2, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    .line 57
    .line 58
    .line 59
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 60
    .line 61
    add-int/lit8 p1, p1, 0x17

    .line 62
    .line 63
    rem-int/lit16 p2, p1, 0x80

    .line 64
    .line 65
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 66
    .line 67
    rem-int/2addr p1, v1

    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method public final logSession(Landroid/content/Context;)V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x2f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const-string v2, "logSession"

    .line 23
    .line 24
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/appsflyer/internal/AFb1sSDK;->AFInAppEventType()V

    .line 36
    .line 37
    .line 38
    sget-object v0, Lcom/appsflyer/internal/AFf1sSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFf1sSDK;

    .line 39
    .line 40
    invoke-direct {p0, p1, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;Lcom/appsflyer/internal/AFf1sSDK;)V

    .line 41
    .line 42
    .line 43
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, v0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    .line 45
    .line 46
    .line 47
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 48
    .line 49
    add-int/lit8 p1, p1, 0x63

    .line 50
    .line 51
    rem-int/lit16 v0, p1, 0x80

    .line 52
    .line 53
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 54
    .line 55
    rem-int/lit8 p1, p1, 0x2

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final onPause(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x65

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-nez v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onConversionDataSuccess()Lcom/appsflyer/internal/AFc1ySDK;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-interface {v0, p1}, Lcom/appsflyer/internal/AFc1ySDK;->AFInAppEventParameterName(Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 30
    .line 31
    add-int/lit8 p1, p1, 0x6f

    .line 32
    .line 33
    rem-int/lit16 v0, p1, 0x80

    .line 34
    .line 35
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 36
    .line 37
    rem-int/lit8 p1, p1, 0x2

    .line 38
    .line 39
    return-void

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onConversionDataSuccess()Lcom/appsflyer/internal/AFc1ySDK;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-interface {v0, p1}, Lcom/appsflyer/internal/AFc1ySDK;->AFInAppEventParameterName(Landroid/content/Context;)V

    .line 49
    .line 50
    .line 51
    const/4 p1, 0x0

    .line 52
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :catchall_0
    move-exception p1

    .line 54
    throw p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final performOnAppAttribution(Landroid/content/Context;Ljava/net/URI;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/net/URI;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    const/4 v0, 0x1

    .line 6
    :goto_0
    const-string v1, "\""

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    goto :goto_2

    .line 11
    :cond_1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 12
    .line 13
    add-int/lit8 v0, v0, 0x15

    .line 14
    .line 15
    rem-int/lit16 v2, v0, 0x80

    .line 16
    .line 17
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 18
    .line 19
    rem-int/lit8 v0, v0, 0x2

    .line 20
    .line 21
    const/4 v2, 0x3

    .line 22
    if-nez v0, :cond_2

    .line 23
    .line 24
    const/16 v0, 0x57

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_2
    const/4 v0, 0x3

    .line 28
    :goto_1
    if-ne v0, v2, :cond_5

    .line 29
    .line 30
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_3

    .line 39
    .line 40
    :goto_2
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    const-string v2, "Link is \""

    .line 51
    .line 52
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->NETWORK:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 66
    .line 67
    invoke-virtual {p1, p2, v0}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 68
    .line 69
    .line 70
    return-void

    .line 71
    :cond_3
    if-nez p1, :cond_4

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    const-string v2, "Context is \""

    .line 84
    .line 85
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->NETWORK:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 99
    .line 100
    invoke-virtual {p2, p1, v0}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 101
    .line 102
    .line 103
    return-void

    .line 104
    :cond_4
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    new-instance v1, Ljava/util/HashMap;

    .line 113
    .line 114
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 122
    .line 123
    .line 124
    move-result-object p2

    .line 125
    invoke-virtual {v0, p1, v1, p2}, Lcom/appsflyer/internal/AFb1kSDK;->AFInAppEventParameterName(Landroid/content/Context;Ljava/util/Map;Landroid/net/Uri;)V

    .line 126
    .line 127
    .line 128
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 129
    .line 130
    add-int/lit8 p1, p1, 0x11

    .line 131
    .line 132
    rem-int/lit16 p2, p1, 0x80

    .line 133
    .line 134
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 135
    .line 136
    rem-int/lit8 p1, p1, 0x2

    .line 137
    .line 138
    return-void

    .line 139
    :cond_5
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    .line 144
    .line 145
    .line 146
    const/4 p1, 0x0

    .line 147
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :catchall_0
    move-exception p1

    .line 149
    throw p1
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final performOnDeepLinking(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x3b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v0, 0x44

    .line 12
    .line 13
    if-nez p1, :cond_0

    .line 14
    .line 15
    const/16 v2, 0x44

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v2, 0xe

    .line 19
    .line 20
    :goto_0
    const/4 v3, 0x0

    .line 21
    const/4 v4, 0x1

    .line 22
    if-eq v2, v0, :cond_4

    .line 23
    .line 24
    if-nez p2, :cond_3

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string p2, "performOnDeepLinking was called with null context"

    .line 35
    .line 36
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->DEVELOPER_ERROR:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 37
    .line 38
    invoke-virtual {p1, p2, v0}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 39
    .line 40
    .line 41
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 42
    .line 43
    add-int/lit8 p1, p1, 0x41

    .line 44
    .line 45
    rem-int/lit16 p2, p1, 0x80

    .line 46
    .line 47
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 48
    .line 49
    rem-int/lit8 p1, p1, 0x2

    .line 50
    .line 51
    if-eqz p1, :cond_1

    .line 52
    .line 53
    const/4 p1, 0x1

    .line 54
    goto :goto_1

    .line 55
    :cond_1
    const/4 p1, 0x0

    .line 56
    :goto_1
    if-eq p1, v4, :cond_2

    .line 57
    .line 58
    return-void

    .line 59
    :cond_2
    :try_start_0
    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :catchall_0
    move-exception p1

    .line 61
    throw p1

    .line 62
    :cond_3
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object p2

    .line 66
    invoke-virtual {p0, p2}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFKeystoreWrapper()Ljava/util/concurrent/ExecutorService;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    new-instance v1, Lcom/appsflyer/internal/〇o00〇〇Oo;

    .line 78
    .line 79
    invoke-direct {v1, p0, p2, p1}, Lcom/appsflyer/internal/〇o00〇〇Oo;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Landroid/content/Context;Landroid/content/Intent;)V

    .line 80
    .line 81
    .line 82
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :cond_4
    add-int/2addr v1, v4

    .line 87
    rem-int/lit16 p1, v1, 0x80

    .line 88
    .line 89
    sput p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 90
    .line 91
    rem-int/lit8 v1, v1, 0x2

    .line 92
    .line 93
    const-string p1, "performOnDeepLinking was called with null intent"

    .line 94
    .line 95
    if-nez v1, :cond_5

    .line 96
    .line 97
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->DEVELOPER_ERROR:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 106
    .line 107
    invoke-virtual {p2, p1, v0}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 108
    .line 109
    .line 110
    return-void

    .line 111
    :cond_5
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 116
    .line 117
    .line 118
    move-result-object p2

    .line 119
    sget-object v0, Lcom/appsflyer/deeplink/DeepLinkResult$Error;->DEVELOPER_ERROR:Lcom/appsflyer/deeplink/DeepLinkResult$Error;

    .line 120
    .line 121
    invoke-virtual {p2, p1, v0}, Lcom/appsflyer/internal/AFb1kSDK;->values(Ljava/lang/String;Lcom/appsflyer/deeplink/DeepLinkResult$Error;)V

    .line 122
    .line 123
    .line 124
    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 125
    :catchall_1
    move-exception p1

    .line 126
    throw p1
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final registerConversionListener(Landroid/content/Context;Lcom/appsflyer/AppsFlyerConversionListener;)V
    .locals 2

    .line 1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 p1, p1, 0x33

    .line 4
    .line 5
    rem-int/lit16 v0, p1, 0x80

    .line 6
    .line 7
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 p1, p1, 0x2

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x1

    .line 17
    :goto_0
    const-string v1, "registerConversionListener"

    .line 18
    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    new-array v0, v0, [Ljava/lang/String;

    .line 30
    .line 31
    invoke-interface {p1, v1, v0}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    new-array v0, v0, [Ljava/lang/String;

    .line 44
    .line 45
    invoke-interface {p1, v1, v0}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :goto_1
    invoke-direct {p0, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Lcom/appsflyer/AppsFlyerConversionListener;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final registerValidatorListener(Landroid/content/Context;Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener;)V
    .locals 3

    .line 1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 p1, p1, 0x7d

    .line 4
    .line 5
    rem-int/lit16 v0, p1, 0x80

    .line 6
    .line 7
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 p1, p1, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const-string v0, "registerValidatorListener"

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    new-array v2, v1, [Ljava/lang/String;

    .line 23
    .line 24
    invoke-interface {p1, v0, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    const-string p1, "registerValidatorListener called"

    .line 28
    .line 29
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/16 p1, 0x27

    .line 33
    .line 34
    if-nez p2, :cond_0

    .line 35
    .line 36
    const/16 v0, 0x4a

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/16 v0, 0x27

    .line 40
    .line 41
    :goto_0
    if-eq v0, p1, :cond_3

    .line 42
    .line 43
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 44
    .line 45
    add-int/lit8 p1, p1, 0x23

    .line 46
    .line 47
    rem-int/lit16 p2, p1, 0x80

    .line 48
    .line 49
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 50
    .line 51
    rem-int/lit8 p1, p1, 0x2

    .line 52
    .line 53
    const-string p1, "registerValidatorListener null listener"

    .line 54
    .line 55
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 59
    .line 60
    add-int/lit8 p1, p1, 0x77

    .line 61
    .line 62
    rem-int/lit16 p2, p1, 0x80

    .line 63
    .line 64
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 65
    .line 66
    rem-int/lit8 p1, p1, 0x2

    .line 67
    .line 68
    if-eqz p1, :cond_1

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_1
    const/4 v1, 0x1

    .line 72
    :goto_1
    if-eqz v1, :cond_2

    .line 73
    .line 74
    return-void

    .line 75
    :cond_2
    const/4 p1, 0x0

    .line 76
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :catchall_0
    move-exception p1

    .line 78
    throw p1

    .line 79
    :cond_3
    sput-object p2, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper:Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener;

    .line 80
    .line 81
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 82
    .line 83
    add-int/lit8 p1, p1, 0x3f

    .line 84
    .line 85
    rem-int/lit16 p2, p1, 0x80

    .line 86
    .line 87
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 88
    .line 89
    rem-int/lit8 p1, p1, 0x2

    .line 90
    .line 91
    if-eqz p1, :cond_4

    .line 92
    .line 93
    const/16 p1, 0x53

    .line 94
    .line 95
    :try_start_1
    div-int/2addr p1, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 96
    return-void

    .line 97
    :catchall_1
    move-exception p1

    .line 98
    throw p1

    .line 99
    :cond_4
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final sendAdImpression(Landroid/content/Context;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFc1uSDK;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-instance v1, Ljava/util/HashMap;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "ad_network"

    .line 15
    .line 16
    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    const-string p2, "adimpression_counter"

    .line 20
    .line 21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    new-instance p2, Lcom/appsflyer/internal/AFf1uSDK;

    .line 29
    .line 30
    invoke-direct {p2}, Lcom/appsflyer/internal/AFf1uSDK;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p1, v1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Landroid/content/Context;Ljava/util/Map;Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 34
    .line 35
    .line 36
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 37
    .line 38
    add-int/lit8 p1, p1, 0x77

    .line 39
    .line 40
    rem-int/lit16 p2, p1, 0x80

    .line 41
    .line 42
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 43
    .line 44
    rem-int/lit8 p1, p1, 0x2

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final sendAdRevenue(Landroid/content/Context;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1uSDK;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFc1uSDK;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-instance v1, Ljava/util/HashMap;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "ad_network"

    .line 15
    .line 16
    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    const-string p2, "adrevenue_counter"

    .line 20
    .line 21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    new-instance p2, Lcom/appsflyer/internal/AFf1qSDK;

    .line 29
    .line 30
    invoke-direct {p2}, Lcom/appsflyer/internal/AFf1qSDK;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p1, v1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Landroid/content/Context;Ljava/util/Map;Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 34
    .line 35
    .line 36
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 37
    .line 38
    add-int/lit8 p1, p1, 0x13

    .line 39
    .line 40
    rem-int/lit16 p2, p1, 0x80

    .line 41
    .line 42
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 43
    .line 44
    rem-int/lit8 p1, p1, 0x2

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final sendInAppPurchaseData(Landroid/content/Context;Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;",
            ")V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x5b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afInfoLog()Lcom/appsflyer/PurchaseHandler;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string v0, "purchases"

    .line 23
    .line 24
    filled-new-array {v0}, [Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1, p2, p3, v0}, Lcom/appsflyer/PurchaseHandler;->AFInAppEventParameterName(Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;[Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    new-instance v0, Lcom/appsflyer/internal/AFe1xSDK;

    .line 35
    .line 36
    iget-object v1, p1, Lcom/appsflyer/PurchaseHandler;->valueOf:Lcom/appsflyer/internal/AFc1qSDK;

    .line 37
    .line 38
    invoke-direct {v0, p2, p3, v1}, Lcom/appsflyer/internal/AFe1xSDK;-><init>(Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p1, Lcom/appsflyer/PurchaseHandler;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 42
    .line 43
    iget-object p2, p1, Lcom/appsflyer/internal/AFd1fSDK;->values:Ljava/util/concurrent/Executor;

    .line 44
    .line 45
    new-instance p3, Lcom/appsflyer/internal/AFd1fSDK$3;

    .line 46
    .line 47
    invoke-direct {p3, p1, v0}, Lcom/appsflyer/internal/AFd1fSDK$3;-><init>(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)V

    .line 48
    .line 49
    .line 50
    invoke-interface {p2, p3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 51
    .line 52
    .line 53
    :cond_0
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x55

    .line 56
    .line 57
    rem-int/lit16 p2, p1, 0x80

    .line 58
    .line 59
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    const/4 p2, 0x1

    .line 64
    if-eqz p1, :cond_1

    .line 65
    .line 66
    const/4 p1, 0x1

    .line 67
    goto :goto_0

    .line 68
    :cond_1
    const/4 p1, 0x0

    .line 69
    :goto_0
    if-eq p1, p2, :cond_2

    .line 70
    .line 71
    return-void

    .line 72
    :cond_2
    const/4 p1, 0x0

    .line 73
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :catchall_0
    move-exception p1

    .line 75
    throw p1
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method public final sendPurchaseData(Landroid/content/Context;Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;",
            ")V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x3b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afInfoLog()Lcom/appsflyer/PurchaseHandler;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string v0, "subscriptions"

    .line 23
    .line 24
    filled-new-array {v0}, [Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1, p2, p3, v0}, Lcom/appsflyer/PurchaseHandler;->AFInAppEventParameterName(Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;[Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    new-instance v0, Lcom/appsflyer/internal/AFd1bSDK;

    .line 35
    .line 36
    iget-object v1, p1, Lcom/appsflyer/PurchaseHandler;->valueOf:Lcom/appsflyer/internal/AFc1qSDK;

    .line 37
    .line 38
    invoke-direct {v0, p2, p3, v1}, Lcom/appsflyer/internal/AFd1bSDK;-><init>(Ljava/util/Map;Lcom/appsflyer/PurchaseHandler$PurchaseValidationCallback;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p1, Lcom/appsflyer/PurchaseHandler;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 42
    .line 43
    iget-object p2, p1, Lcom/appsflyer/internal/AFd1fSDK;->values:Ljava/util/concurrent/Executor;

    .line 44
    .line 45
    new-instance p3, Lcom/appsflyer/internal/AFd1fSDK$3;

    .line 46
    .line 47
    invoke-direct {p3, p1, v0}, Lcom/appsflyer/internal/AFd1fSDK$3;-><init>(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)V

    .line 48
    .line 49
    .line 50
    invoke-interface {p2, p3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 51
    .line 52
    .line 53
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x33

    .line 56
    .line 57
    rem-int/lit16 p2, p1, 0x80

    .line 58
    .line 59
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    :cond_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method public final sendPushNotificationData(Landroid/app/Activity;)V
    .locals 16
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v0, "c"

    .line 4
    .line 5
    const-string v2, "pid"

    .line 6
    .line 7
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    add-int/lit8 v3, v3, 0x11

    .line 10
    .line 11
    rem-int/lit16 v4, v3, 0x80

    .line 12
    .line 13
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 14
    .line 15
    const/4 v4, 0x2

    .line 16
    rem-int/2addr v3, v4

    .line 17
    const/4 v5, 0x1

    .line 18
    const/4 v6, 0x0

    .line 19
    if-eqz v3, :cond_0

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v3, 0x0

    .line 24
    :goto_0
    if-nez v3, :cond_d

    .line 25
    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    const/4 v3, 0x0

    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 v3, 0x1

    .line 31
    :goto_1
    const-string v8, "sendPushNotificationData"

    .line 32
    .line 33
    if-eqz v3, :cond_2

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    if-eqz v3, :cond_3

    .line 41
    .line 42
    invoke-virtual/range {p0 .. p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-interface {v3}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    new-array v9, v4, [Ljava/lang/String;

    .line 51
    .line 52
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v10

    .line 56
    aput-object v10, v9, v6

    .line 57
    .line 58
    new-instance v10, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    const-string v11, "activity_intent_"

    .line 61
    .line 62
    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 66
    .line 67
    .line 68
    move-result-object v11

    .line 69
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v11

    .line 73
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v10

    .line 80
    aput-object v10, v9, v5

    .line 81
    .line 82
    invoke-interface {v3, v8, v9}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 86
    .line 87
    add-int/lit8 v3, v3, 0x19

    .line 88
    .line 89
    rem-int/lit16 v8, v3, 0x80

    .line 90
    .line 91
    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 92
    .line 93
    rem-int/2addr v3, v4

    .line 94
    goto :goto_3

    .line 95
    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    .line 96
    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-interface {v3}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    new-array v9, v4, [Ljava/lang/String;

    .line 106
    .line 107
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v10

    .line 111
    aput-object v10, v9, v6

    .line 112
    .line 113
    const-string v10, "activity_intent_null"

    .line 114
    .line 115
    aput-object v10, v9, v5

    .line 116
    .line 117
    invoke-interface {v3, v8, v9}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    goto :goto_3

    .line 121
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 122
    .line 123
    .line 124
    move-result-object v3

    .line 125
    invoke-interface {v3}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    const-string v9, "activity_null"

    .line 130
    .line 131
    filled-new-array {v9}, [Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v9

    .line 135
    invoke-interface {v3, v8, v9}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    :goto_3
    invoke-static/range {p1 .. p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/app/Activity;)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v3

    .line 142
    iput-object v3, v1, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    .line 143
    .line 144
    if-eqz v3, :cond_c

    .line 145
    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 147
    .line 148
    .line 149
    move-result-wide v8

    .line 150
    iget-object v3, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 151
    .line 152
    const-string v10, ")"

    .line 153
    .line 154
    if-nez v3, :cond_5

    .line 155
    .line 156
    const-string v0, "pushes: initializing pushes history.."

    .line 157
    .line 158
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 162
    .line 163
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 164
    .line 165
    .line 166
    iput-object v0, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 167
    .line 168
    move-wide v13, v8

    .line 169
    goto/16 :goto_7

    .line 170
    .line 171
    :cond_5
    :try_start_0
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 172
    .line 173
    .line 174
    move-result-object v3

    .line 175
    const-string v11, "pushPayloadMaxAging"

    .line 176
    .line 177
    const-wide/32 v12, 0x1b7740

    .line 178
    .line 179
    .line 180
    invoke-virtual {v3, v11, v12, v13}, Lcom/appsflyer/AppsFlyerProperties;->getLong(Ljava/lang/String;J)J

    .line 181
    .line 182
    .line 183
    move-result-wide v11

    .line 184
    iget-object v3, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 185
    .line 186
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 187
    .line 188
    .line 189
    move-result-object v3

    .line 190
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 191
    .line 192
    .line 193
    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 194
    move-wide v13, v8

    .line 195
    :goto_4
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 196
    .line 197
    .line 198
    move-result v15

    .line 199
    if-eqz v15, :cond_a

    .line 200
    .line 201
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    move-result-object v15

    .line 205
    check-cast v15, Ljava/lang/Long;

    .line 206
    .line 207
    new-instance v5, Lorg/json/JSONObject;

    .line 208
    .line 209
    iget-object v6, v1, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    .line 210
    .line 211
    invoke-direct {v5, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 212
    .line 213
    .line 214
    new-instance v6, Lorg/json/JSONObject;

    .line 215
    .line 216
    iget-object v7, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 217
    .line 218
    invoke-interface {v7, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    .line 220
    .line 221
    move-result-object v7

    .line 222
    check-cast v7, Ljava/lang/String;

    .line 223
    .line 224
    invoke-direct {v6, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    .line 228
    .line 229
    .line 230
    move-result-object v7

    .line 231
    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    .line 232
    .line 233
    .line 234
    move-result-object v4

    .line 235
    invoke-virtual {v7, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    move-result v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    if-eqz v4, :cond_6

    .line 240
    .line 241
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 242
    .line 243
    add-int/lit8 v4, v4, 0x67

    .line 244
    .line 245
    rem-int/lit16 v7, v4, 0x80

    .line 246
    .line 247
    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 248
    .line 249
    const/4 v7, 0x2

    .line 250
    rem-int/2addr v4, v7

    .line 251
    :try_start_2
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    .line 252
    .line 253
    .line 254
    move-result-object v4

    .line 255
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    .line 256
    .line 257
    .line 258
    move-result-object v7

    .line 259
    invoke-virtual {v4, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 260
    .line 261
    .line 262
    move-result v4

    .line 263
    if-eqz v4, :cond_6

    .line 264
    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    .line 266
    .line 267
    const-string v2, "PushNotificationMeasurement: A previous payload with same PID and campaign was already acknowledged! (old: "

    .line 268
    .line 269
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    const-string v2, ", new: "

    .line 276
    .line 277
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 291
    .line 292
    .line 293
    const/4 v0, 0x0

    .line 294
    iput-object v0, v1, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    .line 295
    .line 296
    return-void

    .line 297
    :cond_6
    invoke-virtual {v15}, Ljava/lang/Number;->longValue()J

    .line 298
    .line 299
    .line 300
    move-result-wide v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 301
    sub-long v4, v8, v4

    .line 302
    .line 303
    cmp-long v6, v4, v11

    .line 304
    .line 305
    if-lez v6, :cond_7

    .line 306
    .line 307
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 308
    .line 309
    add-int/lit8 v4, v4, 0x55

    .line 310
    .line 311
    rem-int/lit16 v5, v4, 0x80

    .line 312
    .line 313
    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 314
    .line 315
    const/4 v5, 0x2

    .line 316
    rem-int/2addr v4, v5

    .line 317
    :try_start_3
    iget-object v4, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 318
    .line 319
    invoke-interface {v4, v15}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .line 321
    .line 322
    :cond_7
    invoke-virtual {v15}, Ljava/lang/Number;->longValue()J

    .line 323
    .line 324
    .line 325
    move-result-wide v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 326
    cmp-long v6, v4, v13

    .line 327
    .line 328
    if-gtz v6, :cond_8

    .line 329
    .line 330
    const/4 v4, 0x1

    .line 331
    goto :goto_5

    .line 332
    :cond_8
    const/4 v4, 0x0

    .line 333
    :goto_5
    if-eqz v4, :cond_9

    .line 334
    .line 335
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 336
    .line 337
    add-int/lit8 v4, v4, 0x1f

    .line 338
    .line 339
    rem-int/lit16 v5, v4, 0x80

    .line 340
    .line 341
    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 342
    .line 343
    const/4 v5, 0x2

    .line 344
    rem-int/2addr v4, v5

    .line 345
    :try_start_4
    invoke-virtual {v15}, Ljava/lang/Number;->longValue()J

    .line 346
    .line 347
    .line 348
    move-result-wide v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 349
    :cond_9
    const/4 v4, 0x2

    .line 350
    const/4 v5, 0x1

    .line 351
    const/4 v6, 0x0

    .line 352
    goto/16 :goto_4

    .line 353
    .line 354
    :catchall_0
    move-exception v0

    .line 355
    goto :goto_6

    .line 356
    :catchall_1
    move-exception v0

    .line 357
    move-wide v13, v8

    .line 358
    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    .line 359
    .line 360
    const-string v3, "Error while handling push notification measurement: "

    .line 361
    .line 362
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 363
    .line 364
    .line 365
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 366
    .line 367
    .line 368
    move-result-object v3

    .line 369
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 370
    .line 371
    .line 372
    move-result-object v3

    .line 373
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 377
    .line 378
    .line 379
    move-result-object v2

    .line 380
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 381
    .line 382
    .line 383
    :cond_a
    :goto_7
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 384
    .line 385
    .line 386
    move-result-object v0

    .line 387
    const-string v2, "pushPayloadHistorySize"

    .line 388
    .line 389
    const/4 v3, 0x2

    .line 390
    invoke-virtual {v0, v2, v3}, Lcom/appsflyer/AppsFlyerProperties;->getInt(Ljava/lang/String;I)I

    .line 391
    .line 392
    .line 393
    move-result v0

    .line 394
    iget-object v2, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 395
    .line 396
    invoke-interface {v2}, Ljava/util/Map;->size()I

    .line 397
    .line 398
    .line 399
    move-result v2

    .line 400
    if-ne v2, v0, :cond_b

    .line 401
    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    .line 403
    .line 404
    const-string v2, "pushes: removing oldest overflowing push (oldest push:"

    .line 405
    .line 406
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 407
    .line 408
    .line 409
    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 410
    .line 411
    .line 412
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    .line 414
    .line 415
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 416
    .line 417
    .line 418
    move-result-object v0

    .line 419
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 420
    .line 421
    .line 422
    iget-object v0, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 423
    .line 424
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 425
    .line 426
    .line 427
    move-result-object v2

    .line 428
    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    .line 430
    .line 431
    :cond_b
    iget-object v0, v1, Lcom/appsflyer/internal/AFa1cSDK;->getLevel:Ljava/util/Map;

    .line 432
    .line 433
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 434
    .line 435
    .line 436
    move-result-object v2

    .line 437
    iget-object v3, v1, Lcom/appsflyer/internal/AFa1cSDK;->afWarnLog:Ljava/lang/String;

    .line 438
    .line 439
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    .line 441
    .line 442
    invoke-virtual/range {p0 .. p1}, Lcom/appsflyer/AppsFlyerLib;->start(Landroid/content/Context;)V

    .line 443
    .line 444
    .line 445
    :cond_c
    return-void

    .line 446
    :cond_d
    const/4 v0, 0x0

    .line 447
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 448
    :catchall_2
    move-exception v0

    .line 449
    move-object v2, v0

    .line 450
    throw v2
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public final setAdditionalData(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x77

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x5a

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x15

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x5a

    .line 19
    .line 20
    :goto_0
    if-ne v0, v1, :cond_2

    .line 21
    .line 22
    if-eqz p1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const/4 v1, 0x1

    .line 33
    new-array v1, v1, [Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    const/4 v3, 0x0

    .line 40
    aput-object v2, v1, v3

    .line 41
    .line 42
    const-string v2, "setAdditionalData"

    .line 43
    .line 44
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    new-instance v0, Lorg/json/JSONObject;

    .line 48
    .line 49
    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 50
    .line 51
    .line 52
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p1, v0}, Lcom/appsflyer/AppsFlyerProperties;->setCustomData(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 64
    .line 65
    add-int/lit8 p1, p1, 0x3b

    .line 66
    .line 67
    rem-int/lit16 v0, p1, 0x80

    .line 68
    .line 69
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 70
    .line 71
    rem-int/lit8 p1, p1, 0x2

    .line 72
    .line 73
    :cond_1
    return-void

    .line 74
    :cond_2
    const/4 p1, 0x0

    .line 75
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :catchall_0
    move-exception p1

    .line 77
    throw p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setAndroidIdData(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x3b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    aput-object p1, v1, v2

    .line 24
    .line 25
    const-string v2, "setAndroidIdData"

    .line 26
    .line 27
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog:Ljava/lang/String;

    .line 31
    .line 32
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 33
    .line 34
    add-int/lit8 p1, p1, 0x3f

    .line 35
    .line 36
    rem-int/lit16 v0, p1, 0x80

    .line 37
    .line 38
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 39
    .line 40
    rem-int/lit8 p1, p1, 0x2

    .line 41
    .line 42
    const/16 v0, 0x58

    .line 43
    .line 44
    if-nez p1, :cond_0

    .line 45
    .line 46
    const/16 p1, 0x41

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/16 p1, 0x58

    .line 50
    .line 51
    :goto_0
    if-ne p1, v0, :cond_1

    .line 52
    .line 53
    return-void

    .line 54
    :cond_1
    const/4 p1, 0x0

    .line 55
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :catchall_0
    move-exception p1

    .line 57
    throw p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setAppId(Ljava/lang/String;)V
    .locals 5

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x53

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x4d

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x4d

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x2b

    .line 19
    .line 20
    :goto_0
    const-string v2, "appid"

    .line 21
    .line 22
    const-string v3, "setAppId"

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    if-eq v0, v1, :cond_1

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const/4 v1, 0x1

    .line 36
    new-array v1, v1, [Ljava/lang/String;

    .line 37
    .line 38
    aput-object p1, v1, v4

    .line 39
    .line 40
    invoke-interface {v0, v3, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    :goto_1
    invoke-static {v2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-array v1, v4, [Ljava/lang/String;

    .line 56
    .line 57
    aput-object p1, v1, v4

    .line 58
    .line 59
    invoke-interface {v0, v3, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :goto_2
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setAppInviteOneLink(Ljava/lang/String;)V
    .locals 7

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x4d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x46

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x46

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x11

    .line 19
    .line 20
    :goto_0
    const-string v2, "oneLinkSlug"

    .line 21
    .line 22
    const-string v3, "setAppInviteOneLink = "

    .line 23
    .line 24
    const-string v4, "setAppInviteOneLink"

    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    const/4 v6, 0x1

    .line 28
    if-eq v0, v1, :cond_2

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    new-array v1, v6, [Ljava/lang/String;

    .line 39
    .line 40
    aput-object p1, v1, v5

    .line 41
    .line 42
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    const/4 v5, 0x1

    .line 59
    :cond_1
    if-eq v5, v6, :cond_4

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    new-array v1, v6, [Ljava/lang/String;

    .line 71
    .line 72
    aput-object p1, v1, v5

    .line 73
    .line 74
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    if-eqz p1, :cond_3

    .line 89
    .line 90
    const/4 v5, 0x1

    .line 91
    :cond_3
    if-eq v5, v6, :cond_4

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_4
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-virtual {v0, v2}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    if-nez v0, :cond_5

    .line 107
    .line 108
    :goto_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    const-string v1, "onelinkDomain"

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->remove(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    const-string v1, "onelinkVersion"

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->remove(Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    const-string v1, "onelinkScheme"

    .line 131
    .line 132
    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->remove(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    :cond_5
    invoke-static {v2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 139
    .line 140
    add-int/lit8 p1, p1, 0x61

    .line 141
    .line 142
    rem-int/lit16 v0, p1, 0x80

    .line 143
    .line 144
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 145
    .line 146
    rem-int/lit8 p1, p1, 0x2

    .line 147
    .line 148
    if-eqz p1, :cond_6

    .line 149
    .line 150
    return-void

    .line 151
    :cond_6
    const/4 p1, 0x0

    .line 152
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :catchall_0
    move-exception p1

    .line 154
    throw p1
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public final setCollectAndroidID(Z)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x59

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    aput-object v3, v1, v2

    .line 28
    .line 29
    const-string v2, "setCollectAndroidID"

    .line 30
    .line 31
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const-string v0, "collectAndroidId"

    .line 35
    .line 36
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const-string v0, "collectAndroidIdForceByUser"

    .line 44
    .line 45
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-static {v0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 53
    .line 54
    add-int/lit8 p1, p1, 0x11

    .line 55
    .line 56
    rem-int/lit16 v0, p1, 0x80

    .line 57
    .line 58
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 59
    .line 60
    rem-int/lit8 p1, p1, 0x2

    .line 61
    .line 62
    const/16 v0, 0xa

    .line 63
    .line 64
    if-eqz p1, :cond_0

    .line 65
    .line 66
    const/16 p1, 0xa

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_0
    const/16 p1, 0x23

    .line 70
    .line 71
    :goto_0
    if-eq p1, v0, :cond_1

    .line 72
    .line 73
    return-void

    .line 74
    :cond_1
    const/4 p1, 0x0

    .line 75
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :catchall_0
    move-exception p1

    .line 77
    throw p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setCollectIMEI(Z)V
    .locals 7

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x75

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    const-string v3, "collectIMEIForceByUser"

    .line 19
    .line 20
    const-string v4, "collectIMEI"

    .line 21
    .line 22
    const-string v5, "setCollectIMEI"

    .line 23
    .line 24
    if-eq v0, v2, :cond_1

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    new-array v2, v2, [Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    aput-object v6, v2, v1

    .line 41
    .line 42
    invoke-interface {v0, v5, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-static {v4, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-static {v3, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    goto :goto_2

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    new-array v1, v2, [Ljava/lang/String;

    .line 69
    .line 70
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v6

    .line 74
    aput-object v6, v1, v2

    .line 75
    .line 76
    invoke-interface {v0, v5, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :goto_2
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setCollectOaid(Z)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x79

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x49

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x49

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x3

    .line 19
    :goto_0
    const-string v2, "collectOAID"

    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v4, 0x1

    .line 23
    const-string v5, "setCollectOaid"

    .line 24
    .line 25
    if-eq v0, v1, :cond_1

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-array v1, v4, [Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    aput-object v4, v1, v3

    .line 42
    .line 43
    invoke-interface {v0, v5, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-static {v2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    new-array v1, v3, [Ljava/lang/String;

    .line 63
    .line 64
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    aput-object v3, v1, v4

    .line 69
    .line 70
    invoke-interface {v0, v5, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :goto_2
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setCurrencyCode(Ljava/lang/String;)V
    .locals 5

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    :goto_0
    const-string v3, "currencyCode"

    .line 19
    .line 20
    const-string v4, "setCurrencyCode"

    .line 21
    .line 22
    if-eq v0, v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-array v2, v2, [Ljava/lang/String;

    .line 33
    .line 34
    aput-object p1, v2, v1

    .line 35
    .line 36
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :goto_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0, v3, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-array v2, v2, [Ljava/lang/String;

    .line 56
    .line 57
    aput-object p1, v2, v1

    .line 58
    .line 59
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :goto_2
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 64
    .line 65
    add-int/lit8 p1, p1, 0x17

    .line 66
    .line 67
    rem-int/lit16 v0, p1, 0x80

    .line 68
    .line 69
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 70
    .line 71
    rem-int/lit8 p1, p1, 0x2

    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setCustomerIdAndLogSession(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p2    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x9

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    const/16 v0, 0x19

    .line 15
    .line 16
    :try_start_0
    div-int/2addr v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    const/16 v0, 0xc

    .line 18
    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    const/16 v3, 0x24

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/16 v3, 0xc

    .line 25
    .line 26
    :goto_0
    if-eq v3, v0, :cond_7

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :catchall_0
    move-exception p1

    .line 30
    throw p1

    .line 31
    :cond_1
    if-eqz p2, :cond_7

    .line 32
    .line 33
    :goto_1
    add-int/lit8 v1, v1, 0x3

    .line 34
    .line 35
    rem-int/lit16 v0, v1, 0x80

    .line 36
    .line 37
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 38
    .line 39
    rem-int/lit8 v1, v1, 0x2

    .line 40
    .line 41
    if-nez v1, :cond_6

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->values()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    const/4 v1, 0x1

    .line 48
    if-eqz v0, :cond_5

    .line 49
    .line 50
    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib;->setCustomerUserId(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    const-string v3, "CustomerUserId set: "

    .line 56
    .line 57
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string p1, " - Initializing AppsFlyer Tacking"

    .line 64
    .line 65
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-static {p1, v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    .line 73
    .line 74
    .line 75
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-virtual {p1, v0}, Lcom/appsflyer/AppsFlyerProperties;->getReferrer(Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    sget-object v0, Lcom/appsflyer/internal/AFf1sSDK;->values:Lcom/appsflyer/internal/AFf1sSDK;

    .line 92
    .line 93
    invoke-direct {p0, p2, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;Lcom/appsflyer/internal/AFf1sSDK;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 105
    .line 106
    if-nez p1, :cond_2

    .line 107
    .line 108
    goto :goto_2

    .line 109
    :cond_2
    const/4 v2, 0x1

    .line 110
    :goto_2
    if-eq v2, v1, :cond_3

    .line 111
    .line 112
    const-string p1, ""

    .line 113
    .line 114
    :cond_3
    instance-of v0, p2, Landroid/app/Activity;

    .line 115
    .line 116
    if-eqz v0, :cond_4

    .line 117
    .line 118
    move-object v0, p2

    .line 119
    check-cast v0, Landroid/app/Activity;

    .line 120
    .line 121
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 122
    .line 123
    .line 124
    :cond_4
    invoke-direct {p0, p2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Landroid/content/Context;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    return-void

    .line 128
    :cond_5
    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib;->setCustomerUserId(Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    const-string p2, "waitForCustomerUserId is false; setting CustomerUserID: "

    .line 136
    .line 137
    invoke-virtual {p2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-static {p1, v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    .line 142
    .line 143
    .line 144
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 145
    .line 146
    add-int/lit8 p1, p1, 0x1d

    .line 147
    .line 148
    rem-int/lit16 p2, p1, 0x80

    .line 149
    .line 150
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 151
    .line 152
    rem-int/lit8 p1, p1, 0x2

    .line 153
    .line 154
    goto :goto_3

    .line 155
    :cond_6
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->values()Z

    .line 156
    .line 157
    .line 158
    const/4 p1, 0x0

    .line 159
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 160
    :catchall_1
    move-exception p1

    .line 161
    throw p1

    .line 162
    :cond_7
    :goto_3
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final setCustomerUserId(Ljava/lang/String;)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x43

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    aput-object p1, v1, v2

    .line 24
    .line 25
    const-string v3, "setCustomerUserId"

    .line 26
    .line 27
    invoke-interface {v0, v3, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "setCustomerUserId = "

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const-string v0, "AppUserId"

    .line 44
    .line 45
    invoke-static {v0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const-string p1, "waitForCustomerId"

    .line 49
    .line 50
    invoke-static {p1, v2}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/lang/String;Z)V

    .line 51
    .line 52
    .line 53
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x59

    .line 56
    .line 57
    rem-int/lit16 v0, p1, 0x80

    .line 58
    .line 59
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setDebugLog(Z)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x45

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    const/4 v3, 0x0

    .line 19
    if-nez v0, :cond_4

    .line 20
    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    :cond_1
    if-eq v1, v2, :cond_2

    .line 25
    .line 26
    sget-object p1, Lcom/appsflyer/AFLogger$LogLevel;->NONE:Lcom/appsflyer/AFLogger$LogLevel;

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_2
    sget-object p1, Lcom/appsflyer/AFLogger$LogLevel;->DEBUG:Lcom/appsflyer/AFLogger$LogLevel;

    .line 30
    .line 31
    :goto_1
    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib;->setLogLevel(Lcom/appsflyer/AFLogger$LogLevel;)V

    .line 32
    .line 33
    .line 34
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 35
    .line 36
    add-int/lit8 p1, p1, 0x77

    .line 37
    .line 38
    rem-int/lit16 v0, p1, 0x80

    .line 39
    .line 40
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 41
    .line 42
    rem-int/lit8 p1, p1, 0x2

    .line 43
    .line 44
    if-eqz p1, :cond_3

    .line 45
    .line 46
    return-void

    .line 47
    :cond_3
    :try_start_0
    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :catchall_0
    move-exception p1

    .line 49
    throw p1

    .line 50
    :cond_4
    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 51
    :catchall_1
    move-exception p1

    .line 52
    throw p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setDisableAdvertisingIdentifiers(Z)V
    .locals 2

    .line 1
    const-string v0, "setDisableAdvertisingIdentifiers: "

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/16 v0, 0x61

    .line 15
    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    const/16 p1, 0x49

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/16 p1, 0x61

    .line 22
    .line 23
    :goto_0
    if-eq p1, v0, :cond_1

    .line 24
    .line 25
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 26
    .line 27
    add-int/lit8 p1, p1, 0x79

    .line 28
    .line 29
    rem-int/lit16 v0, p1, 0x80

    .line 30
    .line 31
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 32
    .line 33
    rem-int/lit8 p1, p1, 0x2

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 38
    .line 39
    add-int/lit8 p1, p1, 0x25

    .line 40
    .line 41
    rem-int/lit16 v0, p1, 0x80

    .line 42
    .line 43
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 44
    .line 45
    rem-int/lit8 p1, p1, 0x2

    .line 46
    .line 47
    const/4 p1, 0x0

    .line 48
    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    sput-object p1, Lcom/appsflyer/internal/AFa1dSDK;->AFInAppEventParameterName:Ljava/lang/Boolean;

    .line 53
    .line 54
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    const-string v0, "advertiserIdEnabled"

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Lcom/appsflyer/AppsFlyerProperties;->remove(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    const-string v0, "advertiserId"

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Lcom/appsflyer/AppsFlyerProperties;->remove(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setDisableNetworkData(Z)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x9

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x11

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x16

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x11

    .line 19
    .line 20
    :goto_0
    const-string v2, "disableCollectNetworkData"

    .line 21
    .line 22
    const-string v3, "setDisableNetworkData: "

    .line 23
    .line 24
    if-ne v0, v1, :cond_1

    .line 25
    .line 26
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-static {v2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/lang/String;Z)V

    .line 38
    .line 39
    .line 40
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 41
    .line 42
    add-int/lit8 p1, p1, 0x33

    .line 43
    .line 44
    rem-int/lit16 v0, p1, 0x80

    .line 45
    .line 46
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 47
    .line 48
    rem-int/lit8 p1, p1, 0x2

    .line 49
    .line 50
    return-void

    .line 51
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-static {v2, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/lang/String;Z)V

    .line 63
    .line 64
    .line 65
    const/4 p1, 0x0

    .line 66
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :catchall_0
    move-exception p1

    .line 68
    throw p1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setExtension(Ljava/lang/String;)V
    .locals 5

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x27

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    :goto_0
    const-string v3, "sdkExtension"

    .line 19
    .line 20
    const-string v4, "setExtension"

    .line 21
    .line 22
    if-eq v0, v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-array v2, v2, [Ljava/lang/String;

    .line 33
    .line 34
    aput-object p1, v2, v1

    .line 35
    .line 36
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :goto_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0, v3, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-array v2, v2, [Ljava/lang/String;

    .line 56
    .line 57
    aput-object p1, v2, v1

    .line 58
    .line 59
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :goto_2
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 64
    .line 65
    add-int/lit8 p1, p1, 0x75

    .line 66
    .line 67
    rem-int/lit16 v0, p1, 0x80

    .line 68
    .line 69
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 70
    .line 71
    rem-int/lit8 p1, p1, 0x2

    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setHost(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x5b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    rem-int/2addr v0, v1

    .line 11
    invoke-static {p2}, Lcom/appsflyer/internal/AFb1iSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/16 v2, 0x23

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/16 v0, 0x23

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v0, 0x2

    .line 23
    :goto_0
    if-eq v0, v1, :cond_4

    .line 24
    .line 25
    if-eqz p1, :cond_3

    .line 26
    .line 27
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 28
    .line 29
    add-int/2addr v0, v2

    .line 30
    rem-int/lit16 v2, v0, 0x80

    .line 31
    .line 32
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 33
    .line 34
    rem-int/2addr v0, v1

    .line 35
    const/4 v1, 0x1

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    goto :goto_1

    .line 40
    :cond_1
    const/4 v0, 0x1

    .line 41
    :goto_1
    if-ne v0, v1, :cond_2

    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    goto :goto_2

    .line 48
    :cond_2
    const/4 p1, 0x0

    .line 49
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :catchall_0
    move-exception p1

    .line 51
    throw p1

    .line 52
    :cond_3
    const-string p1, ""

    .line 53
    .line 54
    :goto_2
    new-instance v0, Lcom/appsflyer/internal/AFd1oSDK;

    .line 55
    .line 56
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    invoke-direct {v0, p1, p2}, Lcom/appsflyer/internal/AFd1oSDK;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-static {v0}, Lcom/appsflyer/internal/AFd1rSDK;->valueOf(Lcom/appsflyer/internal/AFd1oSDK;)V

    .line 64
    .line 65
    .line 66
    return-void

    .line 67
    :cond_4
    const-string p1, "hostname was empty or null - call for setHost is skipped"

    .line 68
    .line 69
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final setImeiData(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x5

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    aput-object p1, v1, v2

    .line 24
    .line 25
    const-string v2, "setImeiData"

    .line 26
    .line 27
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iput-object p1, v0, Lcom/appsflyer/internal/AFe1hSDK;->afInfoLog:Ljava/lang/String;

    .line 39
    .line 40
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 41
    .line 42
    add-int/lit8 p1, p1, 0x6d

    .line 43
    .line 44
    rem-int/lit16 v0, p1, 0x80

    .line 45
    .line 46
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 47
    .line 48
    rem-int/lit8 p1, p1, 0x2

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setIsUpdate(Z)V
    .locals 6

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x15

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    const-string v3, "IS_UPDATE"

    .line 19
    .line 20
    const-string v4, "setIsUpdate"

    .line 21
    .line 22
    if-eq v0, v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-array v2, v2, [Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v5

    .line 38
    aput-object v5, v2, v1

    .line 39
    .line 40
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    :goto_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0, v3, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Z)V

    .line 48
    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-array v1, v2, [Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    aput-object v5, v1, v2

    .line 66
    .line 67
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :goto_2
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 72
    .line 73
    add-int/lit8 p1, p1, 0x15

    .line 74
    .line 75
    rem-int/lit16 v0, p1, 0x80

    .line 76
    .line 77
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 78
    .line 79
    rem-int/lit8 p1, p1, 0x2

    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setLogLevel(Lcom/appsflyer/AFLogger$LogLevel;)V
    .locals 4
    .param p1    # Lcom/appsflyer/AFLogger$LogLevel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x29

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/appsflyer/AFLogger$LogLevel;->getLevel()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    sget-object v1, Lcom/appsflyer/AFLogger$LogLevel;->NONE:Lcom/appsflyer/AFLogger$LogLevel;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/appsflyer/AFLogger$LogLevel;->getLevel()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/4 v2, 0x0

    .line 22
    const/4 v3, 0x1

    .line 23
    if-le v0, v1, :cond_0

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    if-eq v0, v3, :cond_1

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    goto :goto_1

    .line 32
    :cond_1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 33
    .line 34
    add-int/lit8 v0, v0, 0x79

    .line 35
    .line 36
    rem-int/lit16 v1, v0, 0x80

    .line 37
    .line 38
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 39
    .line 40
    rem-int/lit8 v0, v0, 0x2

    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    :goto_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    new-array v3, v3, [Ljava/lang/String;

    .line 52
    .line 53
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    aput-object v0, v3, v2

    .line 58
    .line 59
    const-string v0, "log"

    .line 60
    .line 61
    invoke-interface {v1, v0, v3}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    const-string v1, "logLevel"

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/appsflyer/AFLogger$LogLevel;->getLevel()I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    invoke-virtual {v0, v1, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;I)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setMinTimeBetweenSessions(I)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x13

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-eqz v0, :cond_1

    .line 18
    .line 19
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 20
    .line 21
    int-to-long v2, p1

    .line 22
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    iput-wide v2, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration:J

    .line 27
    .line 28
    const/4 p1, 0x3

    .line 29
    :try_start_0
    div-int/2addr p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    goto :goto_1

    .line 31
    :catchall_0
    move-exception p1

    .line 32
    throw p1

    .line 33
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 34
    .line 35
    int-to-long v1, p1

    .line 36
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 37
    .line 38
    .line 39
    move-result-wide v0

    .line 40
    iput-wide v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFVersionDeclaration:J

    .line 41
    .line 42
    :goto_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 43
    .line 44
    add-int/lit8 p1, p1, 0x7b

    .line 45
    .line 46
    rem-int/lit16 v0, p1, 0x80

    .line 47
    .line 48
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 49
    .line 50
    rem-int/lit8 p1, p1, 0x2

    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setOaidData(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x69

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x1

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    aput-object p1, v1, v2

    .line 24
    .line 25
    const-string v2, "setOaidData"

    .line 26
    .line 27
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    sput-object p1, Lcom/appsflyer/internal/AFa1dSDK;->valueOf:Ljava/lang/String;

    .line 31
    .line 32
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 33
    .line 34
    add-int/lit8 p1, p1, 0x3

    .line 35
    .line 36
    rem-int/lit16 v0, p1, 0x80

    .line 37
    .line 38
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 39
    .line 40
    rem-int/lit8 p1, p1, 0x2

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final varargs setOneLinkCustomDomain([Ljava/lang/String;)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x3b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v1, 0x3d

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/16 v0, 0x3d

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v0, 0x52

    .line 19
    .line 20
    :goto_0
    const-string v2, "setOneLinkCustomDomain %s"

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    if-eq v0, v1, :cond_1

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    .line 28
    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    aput-object v1, v0, v3

    .line 33
    .line 34
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    .line 40
    .line 41
    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    aput-object v1, v0, v3

    .line 46
    .line 47
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    :goto_1
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object p1, v0, Lcom/appsflyer/internal/AFb1kSDK;->AFLogger:[Ljava/lang/String;

    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setOutOfStore(Ljava/lang/String;)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0xd

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    const/16 v0, 0x2d

    .line 16
    .line 17
    :try_start_0
    div-int/2addr v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    const/16 v0, 0x2b

    .line 19
    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    const/16 v3, 0x2b

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/16 v3, 0x47

    .line 26
    .line 27
    :goto_0
    if-eq v3, v0, :cond_3

    .line 28
    .line 29
    goto :goto_2

    .line 30
    :catchall_0
    move-exception p1

    .line 31
    throw p1

    .line 32
    :cond_1
    if-eqz p1, :cond_2

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    goto :goto_1

    .line 36
    :cond_2
    const/4 v0, 0x1

    .line 37
    :goto_1
    if-eq v0, v1, :cond_4

    .line 38
    .line 39
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const-string v2, "api_store_value"

    .line 52
    .line 53
    invoke-virtual {v0, v2, p1}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    const-string v0, "Store API set with value: "

    .line 61
    .line 62
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-static {p1, v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_4
    :goto_2
    const-string p1, "Cannot set setOutOfStore with null"

    .line 71
    .line 72
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->valueOf(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 76
    .line 77
    add-int/lit8 p1, p1, 0x73

    .line 78
    .line 79
    rem-int/lit16 v0, p1, 0x80

    .line 80
    .line 81
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 82
    .line 83
    rem-int/lit8 p1, p1, 0x2

    .line 84
    .line 85
    const/16 v0, 0x1c

    .line 86
    .line 87
    if-eqz p1, :cond_5

    .line 88
    .line 89
    const/16 p1, 0x1c

    .line 90
    .line 91
    goto :goto_3

    .line 92
    :cond_5
    const/16 p1, 0x5d

    .line 93
    .line 94
    :goto_3
    if-eq p1, v0, :cond_6

    .line 95
    .line 96
    return-void

    .line 97
    :cond_6
    const/16 p1, 0x29

    .line 98
    .line 99
    :try_start_1
    div-int/2addr p1, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 100
    return-void

    .line 101
    :catchall_1
    move-exception p1

    .line 102
    throw p1
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public final setPartnerData(Ljava/lang/String;Ljava/util/Map;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x2d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    rem-int/2addr v0, v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v0, :cond_b

    .line 13
    .line 14
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFc1xSDK;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    new-instance v0, Lcom/appsflyer/internal/AFc1xSDK;

    .line 19
    .line 20
    invoke-direct {v0}, Lcom/appsflyer/internal/AFc1xSDK;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFc1xSDK;

    .line 24
    .line 25
    :cond_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionFailureNative:Lcom/appsflyer/internal/AFc1xSDK;

    .line 26
    .line 27
    if-eqz p1, :cond_a

    .line 28
    .line 29
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 30
    .line 31
    add-int/lit8 v3, v3, 0x5d

    .line 32
    .line 33
    rem-int/lit16 v4, v3, 0x80

    .line 34
    .line 35
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 36
    .line 37
    rem-int/2addr v3, v1

    .line 38
    if-eqz v3, :cond_9

    .line 39
    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    const/16 v4, 0x14

    .line 45
    .line 46
    if-eqz v3, :cond_1

    .line 47
    .line 48
    const/16 v3, 0x14

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const/4 v3, 0x2

    .line 52
    :goto_0
    if-eq v3, v4, :cond_a

    .line 53
    .line 54
    if-eqz p2, :cond_6

    .line 55
    .line 56
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 57
    .line 58
    add-int/lit8 v3, v3, 0x7d

    .line 59
    .line 60
    rem-int/lit16 v4, v3, 0x80

    .line 61
    .line 62
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 63
    .line 64
    rem-int/2addr v3, v1

    .line 65
    if-nez v3, :cond_5

    .line 66
    .line 67
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_2

    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    const-string v4, "Setting partner data for "

    .line 77
    .line 78
    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    const-string v4, ": "

    .line 85
    .line 86
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    new-instance v3, Lorg/json/JSONObject;

    .line 100
    .line 101
    invoke-direct {v3, p2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v3

    .line 108
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 109
    .line 110
    .line 111
    move-result v3

    .line 112
    const/16 v4, 0x3e8

    .line 113
    .line 114
    if-le v3, v4, :cond_3

    .line 115
    .line 116
    const-string p2, "Partner data 1000 characters limit exceeded"

    .line 117
    .line 118
    invoke-static {p2}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    new-instance p2, Ljava/util/HashMap;

    .line 122
    .line 123
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v1, "limit exceeded: "

    .line 127
    .line 128
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    const-string v2, "error"

    .line 137
    .line 138
    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    .line 140
    .line 141
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1xSDK;->values:Ljava/util/Map;

    .line 142
    .line 143
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    .line 145
    .line 146
    return-void

    .line 147
    :cond_3
    iget-object v3, v0, Lcom/appsflyer/internal/AFc1xSDK;->AFInAppEventType:Ljava/util/Map;

    .line 148
    .line 149
    invoke-interface {v3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    .line 151
    .line 152
    iget-object p2, v0, Lcom/appsflyer/internal/AFc1xSDK;->values:Ljava/util/Map;

    .line 153
    .line 154
    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    .line 156
    .line 157
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 158
    .line 159
    add-int/lit8 p1, p1, 0x61

    .line 160
    .line 161
    rem-int/lit16 p2, p1, 0x80

    .line 162
    .line 163
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 164
    .line 165
    rem-int/2addr p1, v1

    .line 166
    if-eqz p1, :cond_4

    .line 167
    .line 168
    return-void

    .line 169
    :cond_4
    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :catchall_0
    move-exception p1

    .line 171
    throw p1

    .line 172
    :cond_5
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    .line 173
    .line 174
    .line 175
    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 176
    :catchall_1
    move-exception p1

    .line 177
    throw p1

    .line 178
    :cond_6
    :goto_1
    iget-object p2, v0, Lcom/appsflyer/internal/AFc1xSDK;->AFInAppEventType:Ljava/util/Map;

    .line 179
    .line 180
    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    move-result-object p2

    .line 184
    const/16 v0, 0x5c

    .line 185
    .line 186
    if-nez p2, :cond_7

    .line 187
    .line 188
    const/16 p2, 0x21

    .line 189
    .line 190
    goto :goto_2

    .line 191
    :cond_7
    const/16 p2, 0x5c

    .line 192
    .line 193
    :goto_2
    if-eq p2, v0, :cond_8

    .line 194
    .line 195
    const-string p1, "Partner data is missing or `null`"

    .line 196
    .line 197
    goto :goto_3

    .line 198
    :cond_8
    const-string p2, "Cleared partner data for "

    .line 199
    .line 200
    invoke-virtual {p2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    :goto_3
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    return-void

    .line 208
    :cond_9
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 209
    :catchall_2
    move-exception p1

    .line 210
    throw p1

    .line 211
    :cond_a
    const-string p1, "Partner ID is missing or `null`"

    .line 212
    .line 213
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    return-void

    .line 217
    :cond_b
    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 218
    :catchall_3
    move-exception p1

    .line 219
    throw p1
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final setPhoneNumber(Ljava/lang/String;)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x7

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    invoke-static {p1}, Lcom/appsflyer/internal/AFb1wSDK;->AFInAppEventParameterName(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iput-object p1, p0, Lcom/appsflyer/internal/AFa1cSDK;->AppsFlyer2dXConversionCallback:Ljava/lang/String;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/16 p1, 0x4a

    .line 27
    .line 28
    :try_start_0
    div-int/2addr p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :goto_1
    return-void

    .line 30
    :catchall_0
    move-exception p1

    .line 31
    throw p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setPluginInfo(Lcom/appsflyer/internal/platform_extension/PluginInfo;)V
    .locals 2
    .param p1    # Lcom/appsflyer/internal/platform_extension/PluginInfo;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x63

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AppsFlyer2dXConversionCallback()Lcom/appsflyer/internal/AFf1aSDK;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-interface {v0, p1}, Lcom/appsflyer/internal/AFf1aSDK;->AFInAppEventType(Lcom/appsflyer/internal/platform_extension/PluginInfo;)V

    .line 31
    .line 32
    .line 33
    const/16 p1, 0x2c

    .line 34
    .line 35
    :try_start_0
    div-int/2addr p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    goto :goto_1

    .line 37
    :catchall_0
    move-exception p1

    .line 38
    throw p1

    .line 39
    :cond_1
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AppsFlyer2dXConversionCallback()Lcom/appsflyer/internal/AFf1aSDK;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-interface {v0, p1}, Lcom/appsflyer/internal/AFf1aSDK;->AFInAppEventType(Lcom/appsflyer/internal/platform_extension/PluginInfo;)V

    .line 51
    .line 52
    .line 53
    :goto_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x2f

    .line 56
    .line 57
    rem-int/lit16 v0, p1, 0x80

    .line 58
    .line 59
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final setPreinstallAttribution(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 1
    const-string v0, "setPreinstallAttribution API called"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lorg/json/JSONObject;

    .line 7
    .line 8
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 9
    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const-string v2, "pid"

    .line 13
    .line 14
    const/4 v3, 0x1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    :try_start_0
    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 21
    .line 22
    add-int/lit8 p1, p1, 0x61

    .line 23
    .line 24
    rem-int/lit16 v4, p1, 0x80

    .line 25
    .line 26
    sput v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 27
    .line 28
    rem-int/lit8 p1, p1, 0x2

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :catch_0
    move-exception p1

    .line 32
    goto :goto_2

    .line 33
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    const/4 p1, 0x0

    .line 38
    :goto_1
    if-eqz p1, :cond_2

    .line 39
    .line 40
    :try_start_1
    const-string p1, "c"

    .line 41
    .line 42
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 43
    .line 44
    .line 45
    :cond_2
    if-eqz p3, :cond_3

    .line 46
    .line 47
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 48
    .line 49
    add-int/lit8 p1, p1, 0x29

    .line 50
    .line 51
    rem-int/lit16 p2, p1, 0x80

    .line 52
    .line 53
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 54
    .line 55
    rem-int/lit8 p1, p1, 0x2

    .line 56
    .line 57
    :try_start_2
    const-string p1, "af_siteid"

    .line 58
    .line 59
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 60
    .line 61
    .line 62
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 63
    .line 64
    add-int/lit8 p1, p1, 0x69

    .line 65
    .line 66
    rem-int/lit16 p2, p1, 0x80

    .line 67
    .line 68
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 69
    .line 70
    rem-int/lit8 p1, p1, 0x2

    .line 71
    .line 72
    goto :goto_3

    .line 73
    :goto_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-static {p2, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    .line 79
    .line 80
    goto :goto_4

    .line 81
    :cond_3
    :goto_3
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 82
    .line 83
    add-int/lit8 p1, p1, 0x7d

    .line 84
    .line 85
    rem-int/lit16 p2, p1, 0x80

    .line 86
    .line 87
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 88
    .line 89
    rem-int/lit8 p1, p1, 0x2

    .line 90
    .line 91
    :goto_4
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-eqz p1, :cond_4

    .line 96
    .line 97
    const/4 v1, 0x1

    .line 98
    :cond_4
    if-eq v1, v3, :cond_5

    .line 99
    .line 100
    const-string p1, "Cannot set preinstall attribution data without a media source"

    .line 101
    .line 102
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    return-void

    .line 106
    :cond_5
    const-string p1, "preInstallName"

    .line 107
    .line 108
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p2

    .line 112
    invoke-static {p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method public final varargs setResolveDeepLinkURLs([Ljava/lang/String;)V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x43

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    new-array v1, v0, [Ljava/lang/Object;

    .line 13
    .line 14
    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    const/4 v3, 0x0

    .line 19
    aput-object v2, v1, v3

    .line 20
    .line 21
    const-string v2, "setResolveDeepLinkURLs %s"

    .line 22
    .line 23
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    iget-object v2, v1, Lcom/appsflyer/internal/AFb1kSDK;->afErrorLog:Ljava/util/List;

    .line 39
    .line 40
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 41
    .line 42
    .line 43
    iget-object v1, v1, Lcom/appsflyer/internal/AFb1kSDK;->afErrorLog:Ljava/util/List;

    .line 44
    .line 45
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 50
    .line 51
    .line 52
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 53
    .line 54
    add-int/lit8 p1, p1, 0x71

    .line 55
    .line 56
    rem-int/lit16 v1, p1, 0x80

    .line 57
    .line 58
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 59
    .line 60
    rem-int/lit8 p1, p1, 0x2

    .line 61
    .line 62
    if-eqz p1, :cond_0

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 66
    :goto_0
    if-eqz v0, :cond_1

    .line 67
    .line 68
    const/16 p1, 0x14

    .line 69
    .line 70
    :try_start_0
    div-int/2addr p1, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    return-void

    .line 72
    :catchall_0
    move-exception p1

    .line 73
    throw p1

    .line 74
    :cond_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public final varargs setSharingFilter([Ljava/lang/String;)V
    .locals 2
    .param p1    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0xb

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/appsflyer/AppsFlyerLib;->setSharingFilterForPartners([Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 15
    .line 16
    add-int/lit8 p1, p1, 0x2d

    .line 17
    .line 18
    rem-int/lit16 v0, p1, 0x80

    .line 19
    .line 20
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 21
    .line 22
    rem-int/lit8 p1, p1, 0x2

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setSharingFilterForAllPartners()V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x3b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    const-string v3, "all"

    .line 19
    .line 20
    if-eq v0, v1, :cond_1

    .line 21
    .line 22
    filled-new-array {v3}, [Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/appsflyer/AppsFlyerLib;->setSharingFilterForPartners([Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    new-array v0, v2, [Ljava/lang/String;

    .line 31
    .line 32
    aput-object v3, v0, v2

    .line 33
    .line 34
    invoke-virtual {p0, v0}, Lcom/appsflyer/AppsFlyerLib;->setSharingFilterForPartners([Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :goto_1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 38
    .line 39
    add-int/lit8 v0, v0, 0x75

    .line 40
    .line 41
    rem-int/lit16 v1, v0, 0x80

    .line 42
    .line 43
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 44
    .line 45
    rem-int/lit8 v0, v0, 0x2

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final varargs setSharingFilterForPartners([Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFb1aSDK;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/appsflyer/internal/AFb1aSDK;-><init>([Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->afDebugLog:Lcom/appsflyer/internal/AFb1aSDK;

    .line 7
    .line 8
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 9
    .line 10
    add-int/lit8 p1, p1, 0x7d

    .line 11
    .line 12
    rem-int/lit16 v0, p1, 0x80

    .line 13
    .line 14
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 15
    .line 16
    rem-int/lit8 p1, p1, 0x2

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final varargs setUserEmails(Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;[Ljava/lang/String;)V
    .locals 10

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p2

    const/4 v2, 0x1

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 7
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 9
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object v1

    array-length v3, p2

    add-int/2addr v3, v2

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const-string v3, "setUserEmails"

    invoke-interface {v1, v3, v0}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 10
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "userEmailsCryptType"

    invoke-virtual {p1}, Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;->getValue()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;I)V

    .line 11
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 12
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 13
    array-length v3, p2

    .line 14
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v4, v4, 0x23

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    const/4 v5, 0x2

    rem-int/2addr v4, v5

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v3, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_2

    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v6, v6, 0x47

    rem-int/lit16 v8, v6, 0x80

    sput v8, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/2addr v6, v5

    .line 15
    aget-object v6, p2, v7

    .line 16
    sget-object v8, Lcom/appsflyer/internal/AFa1cSDK$5;->AFInAppEventParameterName:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v9

    aget v8, v8, v9

    if-eq v8, v5, :cond_1

    .line 17
    invoke-static {v6}, Lcom/appsflyer/internal/AFb1wSDK;->AFInAppEventParameterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    const-string v6, "sha256_el_arr"

    goto :goto_2

    .line 18
    :cond_1
    invoke-virtual {v1, v6}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    const-string v6, "plain_el_arr"

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 19
    :cond_2
    invoke-interface {v0, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 21
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p2

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/appsflyer/AppsFlyerProperties;->setUserEmails(Ljava/lang/String;)V

    return-void
.end method

.method public final varargs setUserEmails([Ljava/lang/String;)V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x49

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x9

    if-nez v0, :cond_0

    const/16 v0, 0x5b

    goto :goto_0

    :cond_0
    const/16 v0, 0x9

    :goto_0
    const-string v2, "setUserEmails"

    if-ne v0, v1, :cond_1

    .line 2
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object v0

    invoke-interface {v0, v2, p1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 3
    sget-object v0, Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;->NONE:Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;

    invoke-virtual {p0, v0, p1}, Lcom/appsflyer/AppsFlyerLib;->setUserEmails(Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;[Ljava/lang/String;)V

    return-void

    .line 4
    :cond_1
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object v0

    invoke-interface {v0, v2, p1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 5
    sget-object v0, Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;->NONE:Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;

    invoke-virtual {p0, v0, p1}, Lcom/appsflyer/AppsFlyerLib;->setUserEmails(Lcom/appsflyer/AppsFlyerProperties$EmailsCryptType;[Ljava/lang/String;)V

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method public final start(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x43

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/appsflyer/AppsFlyerLib;->start(Landroid/content/Context;Ljava/lang/String;)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x65

    rem-int/lit16 v1, p1, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    const/16 v1, 0x60

    if-nez p1, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    const/16 p1, 0x60

    :goto_0
    if-ne p1, v1, :cond_1

    return-void

    :cond_1
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method public final start(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x45

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    const/4 v1, 0x2

    rem-int/2addr v0, v1

    const/16 v2, 0x36

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x36

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/appsflyer/AppsFlyerLib;->start(Landroid/content/Context;Ljava/lang/String;Lcom/appsflyer/attribution/AppsFlyerRequestListener;)V

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method public final start(Landroid/content/Context;Ljava/lang/String;Lcom/appsflyer/attribution/AppsFlyerRequestListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onConversionDataSuccess()Lcom/appsflyer/internal/AFc1ySDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1ySDK;->AFInAppEventType()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFLogger$LogLevel:Z

    const-string v1, "No dev key"

    const/16 v2, 0x29

    if-nez v0, :cond_4

    const-string v0, "ERROR: AppsFlyer SDK is not initialized! The API call \'start()\' must be called after the \'init(String, AppsFlyerConversionListener)\' API method, which should be called on the Application\'s onCreate."

    .line 5
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    const/16 v0, 0x5e

    if-nez p2, :cond_1

    const/16 v3, 0x5e

    goto :goto_0

    :cond_1
    const/16 v3, 0x18

    :goto_0
    if-eq v3, v0, :cond_2

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    .line 6
    invoke-interface {p3, v2, v1}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    :cond_3
    return-void

    .line 7
    :cond_4
    :goto_1
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 8
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger()Lcom/appsflyer/internal/AFf1tSDK;

    move-result-object v0

    .line 9
    invoke-static {p1}, Lcom/appsflyer/internal/AFa1tSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFf1vSDK;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFf1vSDK;)V

    .line 10
    iget-object v3, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v3, :cond_7

    .line 11
    sget v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v3, v3, 0xd

    rem-int/lit16 v7, v3, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr v3, v4

    .line 12
    invoke-static {p1}, Lcom/appsflyer/internal/AFa1eSDK;->values(Landroid/content/Context;)Landroid/app/Application;

    move-result-object v3

    if-eqz v3, :cond_5

    const/4 v7, 0x0

    goto :goto_2

    :cond_5
    const/4 v7, 0x1

    :goto_2
    if-eq v7, v6, :cond_6

    .line 13
    iput-object v3, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    goto :goto_3

    :cond_6
    return-void

    .line 14
    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v3

    invoke-interface {v3}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    move-result-object v3

    new-array v7, v6, [Ljava/lang/String;

    aput-object p2, v7, v5

    const-string v8, "start"

    invoke-interface {v3, v8, v7}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    new-array v3, v4, [Ljava/lang/Object;

    const-string v7, "6.12.4"

    aput-object v7, v3, v5

    .line 15
    sget-object v7, Lcom/appsflyer/internal/AFa1cSDK;->values:Ljava/lang/String;

    aput-object v7, v3, v6

    const-string v6, "Starting AppsFlyer: (v%s.%s)"

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 16
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Build Number: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 17
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v3

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v6

    invoke-interface {v6}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/appsflyer/AppsFlyerProperties;->loadProperties(Lcom/appsflyer/internal/AFc1uSDK;)V

    .line 18
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v6, 0x0

    if-nez v3, :cond_9

    .line 19
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x13

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr v1, v4

    if-eqz v1, :cond_8

    .line 20
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object v1

    .line 21
    iput-object p2, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 22
    invoke-static {p2}, Lcom/appsflyer/internal/AFb1rSDK;->AFInAppEventType(Ljava/lang/String;)V

    const/16 p2, 0x8

    :try_start_0
    div-int/2addr p2, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception p1

    .line 23
    throw p1

    .line 24
    :cond_8
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v1

    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object v1

    .line 25
    iput-object p2, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 26
    invoke-static {p2}, Lcom/appsflyer/internal/AFb1rSDK;->AFInAppEventType(Ljava/lang/String;)V

    goto :goto_5

    .line 27
    :cond_9
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p2

    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object p2

    .line 28
    iget-object p2, p2, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 29
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/16 v3, 0x21

    if-eqz p2, :cond_a

    const/16 p2, 0x21

    goto :goto_4

    :cond_a
    const/16 p2, 0x4f

    :goto_4
    if-eq p2, v3, :cond_b

    .line 30
    :goto_5
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p2

    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLog()Lcom/appsflyer/internal/AFe1oSDK;

    move-result-object p2

    invoke-virtual {p2, v6}, Lcom/appsflyer/internal/AFe1oSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFe1lSDK;)V

    .line 31
    invoke-direct {p0}, Lcom/appsflyer/internal/AFa1cSDK;->afInfoLog()V

    .line 32
    iget-object p2, p0, Lcom/appsflyer/internal/AFa1cSDK;->onAppOpenAttributionNative:Landroid/app/Application;

    invoke-virtual {p2}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Landroid/content/Context;)V

    .line 33
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p2

    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->init()Lcom/appsflyer/internal/AFb1fSDK;

    move-result-object p2

    invoke-interface {p2}, Lcom/appsflyer/internal/AFb1fSDK;->valueOf()V

    .line 34
    iget-object p2, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;

    invoke-virtual {p2}, Lcom/appsflyer/internal/AFc1oSDK;->onConversionDataSuccess()Lcom/appsflyer/internal/AFc1ySDK;

    move-result-object p2

    new-instance v1, Lcom/appsflyer/internal/AFa1cSDK$1;

    invoke-direct {v1, p0, v0, p3}, Lcom/appsflyer/internal/AFa1cSDK$1;-><init>(Lcom/appsflyer/internal/AFa1cSDK;Lcom/appsflyer/internal/AFf1tSDK;Lcom/appsflyer/attribution/AppsFlyerRequestListener;)V

    invoke-interface {p2, p1, v1}, Lcom/appsflyer/internal/AFc1ySDK;->values(Landroid/content/Context;Lcom/appsflyer/internal/AFc1ySDK$AFa1ySDK;)V

    return-void

    :cond_b
    const-string p1, "ERROR: AppsFlyer SDK is not initialized! You must provide AppsFlyer Dev-Key either in the \'init\' API method (should be called on Application\'s onCreate),or in the start() API (should be called on Activity\'s onCreate)."

    .line 35
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    if-eqz p3, :cond_c

    .line 36
    invoke-interface {p3, v2, v1}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    .line 37
    :cond_c
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x7b

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/2addr p1, v4

    if-nez p1, :cond_d

    return-void

    :cond_d
    :try_start_1
    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method public final stop(ZLandroid/content/Context;)V
    .locals 2

    .line 1
    invoke-virtual {p0, p2}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 5
    .line 6
    .line 7
    move-result-object p2

    .line 8
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-boolean p1, v0, Lcom/appsflyer/internal/AFe1hSDK;->afRDLog:Z

    .line 13
    .line 14
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFKeystoreWrapper()Ljava/util/concurrent/ExecutorService;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    new-instance v1, Lcom/appsflyer/internal/o〇0;

    .line 19
    .line 20
    invoke-direct {v1, p2}, Lcom/appsflyer/internal/o〇0;-><init>(Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 21
    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 24
    .line 25
    .line 26
    const/4 v0, 0x1

    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 p1, 0x0

    .line 32
    :goto_0
    if-eqz p1, :cond_1

    .line 33
    .line 34
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 35
    .line 36
    add-int/lit8 p1, p1, 0x25

    .line 37
    .line 38
    rem-int/lit16 v1, p1, 0x80

    .line 39
    .line 40
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 41
    .line 42
    rem-int/lit8 p1, p1, 0x2

    .line 43
    .line 44
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const-string p2, "is_stop_tracking_used"

    .line 49
    .line 50
    invoke-interface {p1, p2, v0}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;Z)V

    .line 51
    .line 52
    .line 53
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x2d

    .line 56
    .line 57
    rem-int/lit16 p2, p1, 0x80

    .line 58
    .line 59
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    :cond_1
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 64
    .line 65
    add-int/lit8 p1, p1, 0x77

    .line 66
    .line 67
    rem-int/lit16 p2, p1, 0x80

    .line 68
    .line 69
    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 70
    .line 71
    rem-int/lit8 p1, p1, 0x2

    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final subscribeForDeepLink(Lcom/appsflyer/deeplink/DeepLinkListener;)V
    .locals 4
    .param p1    # Lcom/appsflyer/deeplink/DeepLinkListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x71

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-wide/16 v2, 0x3

    if-eqz v0, :cond_3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {p0, p1, v2, v3}, Lcom/appsflyer/AppsFlyerLib;->subscribeForDeepLink(Lcom/appsflyer/deeplink/DeepLinkListener;J)V

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x9

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    const/16 v0, 0x3f

    if-eqz p1, :cond_1

    const/16 p1, 0x3f

    goto :goto_1

    :cond_1
    const/16 p1, 0x1c

    :goto_1
    if-eq p1, v0, :cond_2

    return-void

    :cond_2
    const/16 p1, 0x3b

    :try_start_0
    div-int/2addr p1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1

    :cond_3
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/appsflyer/AppsFlyerLib;->subscribeForDeepLink(Lcom/appsflyer/deeplink/DeepLinkListener;J)V

    const/4 p1, 0x0

    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method public final subscribeForDeepLink(Lcom/appsflyer/deeplink/DeepLinkListener;J)V
    .locals 2
    .param p1    # Lcom/appsflyer/deeplink/DeepLinkListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x31

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 3
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    move-result-object v0

    iput-object p1, v0, Lcom/appsflyer/internal/AFb1kSDK;->AFKeystoreWrapper:Lcom/appsflyer/deeplink/DeepLinkListener;

    .line 4
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    move-result-object p1

    .line 5
    iput-wide p2, p1, Lcom/appsflyer/internal/AFb1kSDK;->afDebugLog:J

    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p1, p1, 0x27

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p1, p1, 0x2

    const/16 p2, 0x2f

    if-nez p1, :cond_0

    const/16 p1, 0x3b

    goto :goto_0

    :cond_0
    const/16 p1, 0x2f

    :goto_0
    if-eq p1, p2, :cond_1

    const/16 p1, 0x35

    :try_start_0
    div-int/lit8 p1, p1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1

    :cond_1
    return-void
.end method

.method public final unregisterConversionListener()V
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x33

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    new-array v1, v1, [Ljava/lang/String;

    .line 21
    .line 22
    const-string v2, "unregisterConversionListener"

    .line 23
    .line 24
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName:Lcom/appsflyer/AppsFlyerConversionListener;

    .line 29
    .line 30
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 31
    .line 32
    add-int/lit8 v0, v0, 0xd

    .line 33
    .line 34
    rem-int/lit16 v1, v0, 0x80

    .line 35
    .line 36
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 37
    .line 38
    rem-int/lit8 v0, v0, 0x2

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final updateServerUninstallToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .line 1
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/appsflyer/internal/AFe1bSDK;

    .line 5
    .line 6
    invoke-direct {v0, p1}, Lcom/appsflyer/internal/AFe1bSDK;-><init>(Landroid/content/Context;)V

    .line 7
    .line 8
    .line 9
    if-eqz p2, :cond_5

    .line 10
    .line 11
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_0
    const-string p1, "[register] Firebase Refreshed Token = "

    .line 23
    .line 24
    invoke-virtual {p1, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFe1bSDK;->values()Lcom/appsflyer/internal/AFe1cSDK;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget-object v1, p1, Lcom/appsflyer/internal/AFe1cSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 38
    .line 39
    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-nez v1, :cond_4

    .line 44
    .line 45
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 46
    .line 47
    .line 48
    move-result-wide v1

    .line 49
    if-eqz p1, :cond_3

    .line 50
    .line 51
    iget-wide v3, p1, Lcom/appsflyer/internal/AFe1cSDK;->valueOf:J

    .line 52
    .line 53
    sub-long v3, v1, v3

    .line 54
    .line 55
    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 56
    .line 57
    const-wide/16 v5, 0x2

    .line 58
    .line 59
    invoke-virtual {p1, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    .line 60
    .line 61
    .line 62
    move-result-wide v5

    .line 63
    cmp-long p1, v3, v5

    .line 64
    .line 65
    if-lez p1, :cond_2

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    const/4 p1, 0x0

    .line 69
    goto :goto_1

    .line 70
    :cond_3
    :goto_0
    const/4 p1, 0x1

    .line 71
    :goto_1
    new-instance v3, Lcom/appsflyer/internal/AFe1cSDK;

    .line 72
    .line 73
    xor-int/lit8 v4, p1, 0x1

    .line 74
    .line 75
    invoke-direct {v3, p2, v1, v2, v4}, Lcom/appsflyer/internal/AFe1cSDK;-><init>(Ljava/lang/String;JZ)V

    .line 76
    .line 77
    .line 78
    iget-object v1, v0, Lcom/appsflyer/internal/AFe1bSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 79
    .line 80
    const-string v2, "afUninstallToken"

    .line 81
    .line 82
    iget-object v4, v3, Lcom/appsflyer/internal/AFe1cSDK;->AFKeystoreWrapper:Ljava/lang/String;

    .line 83
    .line 84
    invoke-interface {v1, v2, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    iget-object v1, v0, Lcom/appsflyer/internal/AFe1bSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 88
    .line 89
    const-string v2, "afUninstallToken_received_time"

    .line 90
    .line 91
    iget-wide v4, v3, Lcom/appsflyer/internal/AFe1cSDK;->valueOf:J

    .line 92
    .line 93
    invoke-interface {v1, v2, v4, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;J)V

    .line 94
    .line 95
    .line 96
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1bSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFc1uSDK;

    .line 97
    .line 98
    const-string v1, "afUninstallToken_queued"

    .line 99
    .line 100
    invoke-virtual {v3}, Lcom/appsflyer/internal/AFe1cSDK;->valueOf()Z

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    invoke-interface {v0, v1, v2}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;Z)V

    .line 105
    .line 106
    .line 107
    if-eqz p1, :cond_4

    .line 108
    .line 109
    invoke-static {p2}, Lcom/appsflyer/internal/AFe1bSDK;->valueOf(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    :cond_4
    return-void

    .line 113
    :cond_5
    :goto_2
    const-string p1, "[register] Firebase Token is either empty or null and was not registered."

    .line 114
    .line 115
    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method public final validateAndLogInAppPurchase(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p4

    .line 2
    .line 3
    move-object/from16 v8, p5

    .line 4
    .line 5
    move-object/from16 v9, p6

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->getLevel()Lcom/appsflyer/internal/AFb1sSDK;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x6

    .line 16
    new-array v2, v2, [Ljava/lang/String;

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    aput-object p2, v2, v3

    .line 20
    .line 21
    const/4 v4, 0x1

    .line 22
    aput-object p3, v2, v4

    .line 23
    .line 24
    const/4 v5, 0x2

    .line 25
    aput-object v0, v2, v5

    .line 26
    .line 27
    const/4 v6, 0x3

    .line 28
    aput-object v8, v2, v6

    .line 29
    .line 30
    const/4 v6, 0x4

    .line 31
    aput-object v9, v2, v6

    .line 32
    .line 33
    if-nez p7, :cond_0

    .line 34
    .line 35
    sget v6, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 36
    .line 37
    add-int/lit8 v6, v6, 0x69

    .line 38
    .line 39
    rem-int/lit16 v7, v6, 0x80

    .line 40
    .line 41
    sput v7, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 42
    .line 43
    rem-int/2addr v6, v5

    .line 44
    const-string v6, ""

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    :goto_0
    const/4 v7, 0x5

    .line 52
    aput-object v6, v2, v7

    .line 53
    .line 54
    const-string v6, "validateAndTrackInAppPurchase"

    .line 55
    .line 56
    invoke-interface {v1, v6, v2}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;[Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/appsflyer/AppsFlyerLib;->isStopped()Z

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    if-nez v1, :cond_1

    .line 64
    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    const-string v2, "Validate in app called with parameters: "

    .line 68
    .line 69
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    const-string v2, " "

    .line 76
    .line 77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    :cond_1
    if-eqz p2, :cond_2

    .line 97
    .line 98
    const/4 v1, 0x0

    .line 99
    goto :goto_1

    .line 100
    :cond_2
    const/4 v1, 0x1

    .line 101
    :goto_1
    if-eq v1, v4, :cond_8

    .line 102
    .line 103
    if-eqz v8, :cond_3

    .line 104
    .line 105
    const/4 v3, 0x1

    .line 106
    :cond_3
    if-eqz v3, :cond_8

    .line 107
    .line 108
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 109
    .line 110
    add-int/lit8 v1, v1, 0x6d

    .line 111
    .line 112
    rem-int/lit16 v2, v1, 0x80

    .line 113
    .line 114
    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 115
    .line 116
    rem-int/2addr v1, v5

    .line 117
    if-eqz v1, :cond_7

    .line 118
    .line 119
    if-eqz p3, :cond_8

    .line 120
    .line 121
    const/16 v1, 0x23

    .line 122
    .line 123
    if-eqz v9, :cond_4

    .line 124
    .line 125
    const/16 v2, 0x23

    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_4
    const/16 v2, 0x54

    .line 129
    .line 130
    :goto_2
    if-eq v2, v1, :cond_5

    .line 131
    .line 132
    goto :goto_3

    .line 133
    :cond_5
    if-nez v0, :cond_6

    .line 134
    .line 135
    goto :goto_3

    .line 136
    :cond_6
    new-instance v11, Ljava/lang/Thread;

    .line 137
    .line 138
    new-instance v12, Lcom/appsflyer/internal/AFa1gSDK;

    .line 139
    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    iget-object v3, v1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 153
    .line 154
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-interface {v1}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 159
    .line 160
    .line 161
    move-result-object v4

    .line 162
    move-object v1, v12

    .line 163
    move-object v5, p2

    .line 164
    move-object/from16 v6, p3

    .line 165
    .line 166
    move-object/from16 v7, p4

    .line 167
    .line 168
    move-object/from16 v8, p5

    .line 169
    .line 170
    move-object/from16 v9, p6

    .line 171
    .line 172
    move-object/from16 v10, p7

    .line 173
    .line 174
    invoke-direct/range {v1 .. v10}, Lcom/appsflyer/internal/AFa1gSDK;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/appsflyer/internal/AFc1tSDK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 175
    .line 176
    .line 177
    invoke-direct {v11, v12}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    .line 181
    .line 182
    .line 183
    goto :goto_4

    .line 184
    :cond_7
    const/4 v0, 0x0

    .line 185
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :catchall_0
    move-exception v0

    .line 187
    move-object v1, v0

    .line 188
    throw v1

    .line 189
    :cond_8
    :goto_3
    sget-object v0, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper:Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener;

    .line 190
    .line 191
    if-eqz v0, :cond_9

    .line 192
    .line 193
    const-string v1, "Please provide purchase parameters"

    .line 194
    .line 195
    invoke-interface {v0, v1}, Lcom/appsflyer/AppsFlyerInAppPurchaseValidatorListener;->onValidateInAppFailure(Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    :cond_9
    :goto_4
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
.end method

.method public final valueOf()Lcom/appsflyer/internal/AFc1qSDK;
    .locals 3

    .line 2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method final valueOf(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 6
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x1d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-nez v0, :cond_6

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V

    .line 8
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p2

    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object p2

    .line 9
    iget-object p2, p2, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    if-nez p2, :cond_2

    const-string p2, "[LogEvent/Launch] AppsFlyer\'s SDK cannot send any event without providing DevKey."

    .line 10
    invoke-static {p2}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 11
    iget-object p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->values:Lcom/appsflyer/attribution/AppsFlyerRequestListener;

    const/16 p2, 0x50

    if-eqz p1, :cond_0

    const/16 v0, 0x50

    goto :goto_0

    :cond_0
    const/16 v0, 0x10

    :goto_0
    if-eq v0, p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 p2, 0x29

    const-string v0, "No dev key"

    .line 12
    invoke-interface {p1, p2, v0}, Lcom/appsflyer/attribution/AppsFlyerRequestListener;->onError(ILjava/lang/String;)V

    :goto_1
    return-void

    .line 13
    :cond_2
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p2

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v0

    invoke-interface {v0}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/appsflyer/AppsFlyerProperties;->getReferrer(Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_5

    .line 14
    sget p2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 p2, p2, 0x63

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 p2, p2, 0x2

    const/4 v0, 0x1

    if-nez p2, :cond_3

    const/4 p2, 0x0

    goto :goto_2

    :cond_3
    const/4 p2, 0x1

    :goto_2
    if-ne p2, v0, :cond_4

    const-string p2, ""

    goto :goto_3

    :cond_4
    :try_start_0
    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    .line 15
    :cond_5
    :goto_3
    iput-object p2, p1, Lcom/appsflyer/internal/AFa1uSDK;->afDebugLog:Ljava/lang/String;

    .line 16
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Lcom/appsflyer/internal/AFa1uSDK;)V

    return-void

    .line 17
    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;Landroid/app/Activity;)V

    .line 18
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;

    move-result-object p1

    .line 19
    iget-object p1, p1, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    .line 20
    throw p1
.end method

.method public final values(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 3
    iget-object v0, p0, Lcom/appsflyer/internal/AFa1cSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/internal/AFc1oSDK;

    const/16 v1, 0x2f

    if-eqz p1, :cond_0

    const/16 v2, 0x26

    goto :goto_0

    :cond_0
    const/16 v2, 0x2f

    :goto_0
    if-eq v2, v1, :cond_2

    .line 4
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1oSDK;->valueOf:Lcom/appsflyer/internal/AFc1rSDK;

    const/16 v1, 0x3b

    if-eqz p1, :cond_1

    const/16 v2, 0x43

    goto :goto_1

    :cond_1
    const/16 v2, 0x3b

    :goto_1
    if-eq v2, v1, :cond_2

    .line 5
    sget v1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v1, v1, 0x5f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, v0, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    :cond_2
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 p1, p1, 0x61

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method public final values(Landroid/content/Context;Ljava/util/Map;Landroid/net/Uri;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .line 89
    invoke-virtual {p0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->values(Landroid/content/Context;)V

    const-string v0, "af_deeplink"

    .line 90
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eq v1, v3, :cond_1

    goto/16 :goto_2

    .line 91
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v4

    invoke-interface {v4}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    move-result-object v4

    .line 93
    iget-object v5, v4, Lcom/appsflyer/internal/AFb1kSDK;->AFInAppEventParameterName:Ljava/lang/String;

    if-eqz v5, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_4

    .line 94
    sget v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v2, v2, 0x1d

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v2, v2, 0x2

    .line 95
    iget-object v2, v4, Lcom/appsflyer/internal/AFb1kSDK;->values:Ljava/util/Map;

    if-eqz v2, :cond_4

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 97
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 98
    iget-object v3, v4, Lcom/appsflyer/internal/AFb1kSDK;->values:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 99
    sget v4, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v4, v4, 0x19

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v4, v4, 0x2

    .line 100
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 101
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 102
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v5, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 103
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v2

    const-string v3, "appended_query_params"

    invoke-interface {p2, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    :cond_4
    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x5f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 107
    :goto_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 108
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "link"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    new-instance v1, Lcom/appsflyer/internal/AFe1wSDK;

    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-direct {v1, v2, v3, p3}, Lcom/appsflyer/internal/AFe1wSDK;-><init>(Lcom/appsflyer/internal/AFc1qSDK;Ljava/util/UUID;Landroid/net/Uri;)V

    .line 110
    invoke-virtual {v1}, Lcom/appsflyer/internal/AFe1wSDK;->afWarnLog()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "isBrandedDomain"

    .line 111
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p2, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    :cond_5
    invoke-static {p1, v0, p3}, Lcom/appsflyer/internal/AFa1eSDK;->valueOf(Landroid/content/Context;Ljava/util/Map;Landroid/net/Uri;)Ljava/util/Map;

    .line 113
    invoke-virtual {v1}, Lcom/appsflyer/internal/AFe1wSDK;->AFVersionDeclaration()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 114
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/util/Map;)Lcom/appsflyer/internal/AFe1wSDK$AFa1vSDK;

    move-result-object p1

    .line 115
    iput-object p1, v1, Lcom/appsflyer/internal/AFe1wSDK;->afRDLog:Lcom/appsflyer/internal/AFe1wSDK$AFa1vSDK;

    .line 116
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLogForExcManagerOnly()Lcom/appsflyer/internal/AFd1fSDK;

    move-result-object p1

    .line 117
    iget-object p2, p1, Lcom/appsflyer/internal/AFd1fSDK;->values:Ljava/util/concurrent/Executor;

    new-instance p3, Lcom/appsflyer/internal/AFd1fSDK$3;

    invoke-direct {p3, p1, v1}, Lcom/appsflyer/internal/AFd1fSDK$3;-><init>(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)V

    invoke-interface {p2, p3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 118
    :cond_6
    invoke-virtual {p0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf()Lcom/appsflyer/internal/AFc1qSDK;

    move-result-object p1

    invoke-interface {p1}, Lcom/appsflyer/internal/AFc1qSDK;->onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/appsflyer/internal/AFb1kSDK;->valueOf(Ljava/util/Map;)V

    return-void
.end method

.method public final values()Z
    .locals 4

    const-string v0, "waitForCustomerId"

    .line 19
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eq v0, v2, :cond_3

    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x25

    rem-int/lit16 v3, v0, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    add-int/lit8 v0, v0, 0x39

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    rem-int/lit8 v0, v0, 0x2

    return v2

    :cond_3
    :goto_2
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    add-int/lit8 v0, v0, 0x79

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v2, 0x40

    if-nez v0, :cond_4

    const/16 v0, 0x9

    goto :goto_3

    :cond_4
    const/16 v0, 0x40

    :goto_3
    if-ne v0, v2, :cond_5

    return v1

    :cond_5
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method public final waitForCustomerUserId(Z)V
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x2f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const-string v0, "initAfterCustomerUserID: "

    .line 12
    .line 13
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x1

    .line 22
    invoke-static {v0, v1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    .line 23
    .line 24
    .line 25
    const-string v0, "waitForCustomerId"

    .line 26
    .line 27
    invoke-static {v0, p1}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/lang/String;Z)V

    .line 28
    .line 29
    .line 30
    sget p1, Lcom/appsflyer/internal/AFa1cSDK;->onResponseErrorNative:I

    .line 31
    .line 32
    add-int/lit8 p1, p1, 0x73

    .line 33
    .line 34
    rem-int/lit16 v0, p1, 0x80

    .line 35
    .line 36
    sput v0, Lcom/appsflyer/internal/AFa1cSDK;->onDeepLinkingNative:I

    .line 37
    .line 38
    rem-int/lit8 p1, p1, 0x2

    .line 39
    .line 40
    const/16 v0, 0x42

    .line 41
    .line 42
    if-nez p1, :cond_0

    .line 43
    .line 44
    const/16 p1, 0x8

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/16 p1, 0x42

    .line 48
    .line 49
    :goto_0
    if-eq p1, v0, :cond_1

    .line 50
    .line 51
    const/16 p1, 0x11

    .line 52
    .line 53
    :try_start_0
    div-int/lit8 p1, p1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    .line 55
    return-void

    .line 56
    :catchall_0
    move-exception p1

    .line 57
    throw p1

    .line 58
    :cond_1
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

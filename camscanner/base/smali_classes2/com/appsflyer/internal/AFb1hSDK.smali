.class public Lcom/appsflyer/internal/AFb1hSDK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final $$a:[B = null

.field public static final $$b:I = 0x0

.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field private static onAppOpenAttribution:[B

.field private static onAttributionFailure:I

.field private static onAttributionFailureNative:[B

.field private static onConversionDataFail:I

.field private static onConversionDataSuccess:[B

.field private static onDeepLinking:J

.field private static onDeepLinkingNative:Ljava/lang/Object;

.field public static final onInstallConversionDataLoadedNative:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final onResponseErrorNative:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static onResponseNative:Ljava/lang/Object;


# direct methods
.method private static $$c(SSB)Ljava/lang/String;
    .locals 9

    .line 1
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 2
    .line 3
    xor-int/lit8 v1, v0, 0xf

    .line 4
    .line 5
    and-int/lit8 v2, v0, 0xf

    .line 6
    .line 7
    const/4 v3, 0x1

    .line 8
    shl-int/2addr v2, v3

    .line 9
    add-int/2addr v1, v2

    .line 10
    rem-int/lit16 v2, v1, 0x80

    .line 11
    .line 12
    sput v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 13
    .line 14
    rem-int/lit8 v1, v1, 0x2

    .line 15
    .line 16
    rsub-int/lit8 p0, p0, 0x77

    .line 17
    .line 18
    neg-int p1, p1

    .line 19
    xor-int/lit16 v1, p1, 0x3c1

    .line 20
    .line 21
    and-int/lit16 p1, p1, 0x3c1

    .line 22
    .line 23
    shl-int/2addr p1, v3

    .line 24
    add-int/2addr v1, p1

    .line 25
    sget-object p1, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    .line 26
    .line 27
    rsub-int/lit8 p2, p2, 0x24

    .line 28
    .line 29
    new-array v2, p2, [B

    .line 30
    .line 31
    add-int/lit8 p2, p2, -0x2b

    .line 32
    .line 33
    sub-int/2addr p2, v3

    .line 34
    add-int/lit8 p2, p2, 0x2c

    .line 35
    .line 36
    sub-int/2addr p2, v3

    .line 37
    const/4 v4, 0x0

    .line 38
    if-nez p1, :cond_0

    .line 39
    .line 40
    const/4 v5, 0x0

    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v5, 0x1

    .line 43
    :goto_0
    if-eq v5, v3, :cond_3

    .line 44
    .line 45
    add-int/lit8 v0, v0, 0x52

    .line 46
    .line 47
    sub-int/2addr v0, v3

    .line 48
    rem-int/lit16 v5, v0, 0x80

    .line 49
    .line 50
    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 51
    .line 52
    rem-int/lit8 v0, v0, 0x2

    .line 53
    .line 54
    const/16 v5, 0x3e

    .line 55
    .line 56
    if-eqz v0, :cond_1

    .line 57
    .line 58
    const/16 v0, 0x10

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_1
    const/16 v0, 0x3e

    .line 62
    .line 63
    :goto_1
    if-ne v0, v5, :cond_2

    .line 64
    .line 65
    move-object v5, v2

    .line 66
    const/4 v0, 0x0

    .line 67
    move v2, v1

    .line 68
    goto :goto_3

    .line 69
    :cond_2
    const/4 p0, 0x0

    .line 70
    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :catchall_0
    move-exception p0

    .line 72
    throw p0

    .line 73
    :cond_3
    const/4 v0, 0x0

    .line 74
    :goto_2
    int-to-byte v5, p0

    .line 75
    aput-byte v5, v2, v0

    .line 76
    .line 77
    if-ne v0, p2, :cond_4

    .line 78
    .line 79
    new-instance p0, Ljava/lang/String;

    .line 80
    .line 81
    invoke-direct {p0, v2, v4}, Ljava/lang/String;-><init>([BI)V

    .line 82
    .line 83
    .line 84
    return-object p0

    .line 85
    :cond_4
    xor-int/lit8 v5, v0, 0x1

    .line 86
    .line 87
    and-int/lit8 v0, v0, 0x1

    .line 88
    .line 89
    shl-int/2addr v0, v3

    .line 90
    add-int/2addr v5, v0

    .line 91
    aget-byte v0, p1, v1

    .line 92
    .line 93
    sget v6, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 94
    .line 95
    or-int/lit8 v7, v6, 0x3b

    .line 96
    .line 97
    shl-int/2addr v7, v3

    .line 98
    xor-int/lit8 v6, v6, 0x3b

    .line 99
    .line 100
    sub-int/2addr v7, v6

    .line 101
    rem-int/lit16 v6, v7, 0x80

    .line 102
    .line 103
    sput v6, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 104
    .line 105
    rem-int/lit8 v7, v7, 0x2

    .line 106
    .line 107
    move v8, v1

    .line 108
    move v1, p0

    .line 109
    move p0, v0

    .line 110
    move v0, v5

    .line 111
    move-object v5, v2

    .line 112
    move v2, v8

    .line 113
    :goto_3
    neg-int p0, p0

    .line 114
    neg-int p0, p0

    .line 115
    and-int v6, v1, p0

    .line 116
    .line 117
    or-int/2addr p0, v1

    .line 118
    add-int/2addr v6, p0

    .line 119
    xor-int/lit8 p0, v6, -0x3

    .line 120
    .line 121
    and-int/lit8 v1, v6, -0x3

    .line 122
    .line 123
    shl-int/2addr v1, v3

    .line 124
    add-int/2addr p0, v1

    .line 125
    or-int/lit8 v1, v2, -0xb

    .line 126
    .line 127
    shl-int/2addr v1, v3

    .line 128
    xor-int/lit8 v2, v2, -0xb

    .line 129
    .line 130
    sub-int/2addr v1, v2

    .line 131
    and-int/lit8 v2, v1, 0xc

    .line 132
    .line 133
    or-int/lit8 v1, v1, 0xc

    .line 134
    .line 135
    add-int/2addr v1, v2

    .line 136
    sget v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 137
    .line 138
    add-int/lit8 v2, v2, 0x60

    .line 139
    .line 140
    sub-int/2addr v2, v3

    .line 141
    rem-int/lit16 v6, v2, 0x80

    .line 142
    .line 143
    sput v6, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 144
    .line 145
    rem-int/lit8 v2, v2, 0x2

    .line 146
    .line 147
    move-object v2, v5

    .line 148
    goto :goto_2
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
.end method

.method static constructor <clinit>()V
    .locals 51

    const-class v1, [B

    invoke-static {}, Lcom/appsflyer/internal/AFb1hSDK;->init$0()V

    const-wide v2, 0x6ea713e6a2c84cf6L

    .line 1
    sput-wide v2, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinking:J

    const/4 v2, 0x0

    sput v2, Lcom/appsflyer/internal/AFb1hSDK;->onAttributionFailure:I

    const/4 v3, 0x2

    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->onConversionDataFail:I

    .line 2
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/appsflyer/internal/AFb1hSDK;->onResponseErrorNative:Ljava/util/Map;

    .line 3
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/appsflyer/internal/AFb1hSDK;->onInstallConversionDataLoadedNative:Ljava/util/Map;

    .line 4
    :try_start_0
    sget-object v4, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v5, 0xec

    aget-byte v6, v4, v5

    int-to-byte v6, v6

    xor-int/lit16 v7, v6, 0x3a9

    and-int/lit16 v8, v6, 0x3a9

    or-int/2addr v7, v8

    int-to-short v7, v7

    const/16 v8, 0x9

    aget-byte v8, v4, v8

    int-to-byte v8, v8

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    .line 5
    sget-object v7, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    const/4 v8, 0x0

    if-nez v7, :cond_0

    aget-byte v7, v4, v5

    int-to-byte v7, v7

    const/16 v9, 0xd9

    aget-byte v9, v4, v9

    int-to-short v9, v9

    const/16 v10, 0x44

    aget-byte v10, v4, v10

    int-to-byte v10, v10

    invoke-static {v7, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_10

    goto :goto_0

    :cond_0
    move-object v7, v8

    :goto_0
    const/16 v9, 0x8

    const/16 v10, 0x73

    const/16 v11, 0xd

    const/4 v12, 0x4

    .line 6
    :try_start_1
    aget-byte v13, v4, v10

    int-to-byte v13, v13

    xor-int/lit16 v14, v13, 0xc0

    and-int/lit16 v15, v13, 0xc0

    or-int/2addr v14, v15

    int-to-short v14, v14

    const/16 v15, 0x21

    aget-byte v15, v4, v15

    int-to-byte v15, v15

    invoke-static {v13, v14, v15}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v13

    .line 7
    invoke-static {v13}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    aget-byte v14, v4, v5

    int-to-byte v14, v14

    const/16 v15, 0x35b

    int-to-short v15, v15

    aget-byte v4, v4, v9

    int-to-byte v4, v4

    invoke-static {v14, v15, v4}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    new-array v14, v2, [Ljava/lang/Class;

    .line 8
    invoke-virtual {v13, v4, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 9
    invoke-virtual {v4, v8, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v4, :cond_1

    goto :goto_1

    :catch_0
    move-object v4, v8

    .line 10
    :cond_1
    :try_start_2
    sget-object v13, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v14, v13, v10

    int-to-byte v14, v14

    const/16 v15, 0x1f4

    int-to-short v15, v15

    const/16 v16, 0x18

    aget-byte v5, v13, v16

    int-to-byte v5, v5

    invoke-static {v14, v15, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    .line 11
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    aget-byte v14, v13, v11

    int-to-byte v14, v14

    const/16 v15, 0x80

    int-to-short v15, v15

    aget-byte v13, v13, v12

    int-to-byte v13, v13

    invoke-static {v14, v15, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v13

    new-array v14, v2, [Ljava/lang/Class;

    .line 12
    invoke-virtual {v5, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 13
    invoke-virtual {v5, v8, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    nop

    :goto_1
    if-eqz v4, :cond_2

    const/16 v5, 0x9

    goto :goto_2

    :cond_2
    const/16 v5, 0x27

    :goto_2
    const/16 v13, 0x27

    if-eq v5, v13, :cond_3

    .line 14
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    sget-object v13, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v14, v13, v11

    int-to-byte v14, v14

    const/16 v15, 0x2ca

    int-to-short v15, v15

    const/16 v16, 0x13f

    aget-byte v13, v13, v16

    int-to-byte v13, v13

    invoke-static {v14, v15, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v13

    .line 15
    invoke-virtual {v5, v13, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 16
    invoke-virtual {v5, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    nop

    :cond_3
    move-object v5, v8

    :goto_3
    if-eqz v4, :cond_4

    const/16 v13, 0x16

    goto :goto_4

    :cond_4
    const/16 v13, 0x24

    :goto_4
    const/16 v14, 0x24

    const/4 v15, 0x1

    if-eq v13, v14, :cond_6

    .line 17
    sget v13, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    or-int/lit8 v14, v13, 0x2f

    shl-int/2addr v14, v15

    xor-int/lit8 v13, v13, 0x2f

    sub-int/2addr v14, v13

    rem-int/lit16 v13, v14, 0x80

    sput v13, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    rem-int/2addr v14, v3

    if-eqz v14, :cond_5

    .line 18
    :try_start_4
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    sget-object v14, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v16, 0x43

    aget-byte v12, v14, v16

    int-to-byte v12, v12

    and-int/lit16 v10, v12, 0x5dcb

    not-int v10, v10

    or-int/lit16 v2, v12, 0x5dcb

    and-int/2addr v2, v10

    int-to-short v2, v2

    const/16 v10, 0x50

    aget-byte v10, v14, v10

    int-to-byte v10, v10

    invoke-static {v12, v2, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {v13, v2, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 20
    :goto_5
    invoke-virtual {v2, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_6

    .line 21
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    sget-object v10, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v12, v10, v11

    int-to-byte v12, v12

    xor-int/lit16 v13, v12, 0x243

    and-int/lit16 v14, v12, 0x243

    or-int/2addr v13, v14

    int-to-short v13, v13

    const/16 v14, 0x1a

    aget-byte v10, v10, v14

    int-to-byte v10, v10

    invoke-static {v12, v13, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v10

    .line 22
    invoke-virtual {v2, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_5

    :catch_3
    nop

    :cond_6
    move-object v2, v8

    :goto_6
    if-eqz v4, :cond_7

    const/16 v10, 0x24

    goto :goto_7

    :cond_7
    const/16 v10, 0x8

    :goto_7
    if-eq v10, v9, :cond_8

    .line 23
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    sget-object v12, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v13, v12, v11

    int-to-byte v13, v13

    xor-int/lit16 v14, v13, 0x28c

    and-int/lit16 v9, v13, 0x28c

    or-int/2addr v9, v14

    int-to-short v9, v9

    const/16 v14, 0x13f

    aget-byte v12, v12, v14

    int-to-byte v12, v12

    invoke-static {v13, v9, v12}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v9

    .line 24
    invoke-virtual {v10, v9, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 25
    invoke-virtual {v9, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_8

    :catch_4
    nop

    :cond_8
    move-object v4, v8

    .line 26
    :goto_8
    const-class v9, Ljava/lang/String;

    const/16 v10, 0xbd

    if-eqz v5, :cond_9

    .line 27
    sget v7, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    add-int/lit8 v7, v7, 0x69

    rem-int/lit16 v12, v7, 0x80

    sput v12, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    rem-int/2addr v7, v3

    goto :goto_9

    :cond_9
    if-nez v7, :cond_a

    move-object v5, v8

    goto :goto_9

    .line 28
    :cond_a
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v13, 0xc1

    aget-byte v13, v12, v13

    int-to-byte v13, v13

    xor-int/lit16 v14, v13, 0x111

    and-int/lit16 v3, v13, 0x111

    or-int/2addr v3, v14

    int-to-short v3, v3

    const/16 v14, 0x13f

    aget-byte v14, v12, v14

    int-to-byte v14, v14

    invoke-static {v13, v3, v14}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_10

    :try_start_7
    new-array v5, v15, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v5, v7

    aget-byte v3, v12, v10

    int-to-byte v3, v3

    const/16 v13, 0x6c

    int-to-short v13, v13

    aget-byte v12, v12, v7

    int-to-byte v12, v12

    invoke-static {v3, v13, v12}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    new-array v12, v15, [Ljava/lang/Class;

    aput-object v9, v12, v7

    invoke-virtual {v3, v12}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_52

    :goto_9
    if-eqz v4, :cond_b

    goto :goto_a

    .line 29
    :cond_b
    :try_start_8
    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v4, v3, v10

    int-to-byte v4, v4

    sget v7, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    xor-int/lit16 v12, v7, 0x9a

    and-int/lit16 v7, v7, 0x9a

    or-int/2addr v7, v12

    int-to-short v7, v7

    const/16 v12, 0x73

    aget-byte v13, v3, v12

    int-to-byte v12, v13

    invoke-static {v4, v7, v12}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_10

    :try_start_9
    new-array v7, v15, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v7, v12

    aget-byte v4, v3, v10

    int-to-byte v4, v4

    const/16 v12, 0x36a

    int-to-short v12, v12

    const/16 v13, 0xec

    aget-byte v14, v3, v13

    int-to-byte v13, v14

    invoke-static {v4, v12, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aget-byte v12, v3, v11

    int-to-byte v12, v12

    const/16 v13, 0xee

    int-to-short v13, v13

    const/16 v14, 0x13f

    aget-byte v14, v3, v14

    int-to-byte v14, v14

    invoke-static {v12, v13, v14}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v12

    new-array v13, v15, [Ljava/lang/Class;

    const/4 v14, 0x0

    aput-object v9, v13, v14

    invoke-virtual {v4, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v8, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_51

    :try_start_a
    new-array v7, v15, [Ljava/lang/Object;

    aput-object v4, v7, v14

    aget-byte v4, v3, v10

    int-to-byte v4, v4

    const/16 v12, 0x6c

    int-to-short v12, v12

    aget-byte v3, v3, v14

    int-to-byte v3, v3

    invoke-static {v4, v12, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    new-array v4, v15, [Ljava/lang/Class;

    aput-object v9, v4, v14

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_50

    :goto_a
    if-nez v2, :cond_10

    if-eqz v5, :cond_c

    const/4 v3, 0x1

    goto :goto_b

    :cond_c
    const/4 v3, 0x0

    :goto_b
    if-eqz v3, :cond_10

    .line 30
    sget v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v2, v2, 0x2d

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v3, 0x2

    rem-int/2addr v2, v3

    if-nez v2, :cond_d

    const/4 v2, 0x1

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    :goto_c
    if-eq v2, v15, :cond_e

    .line 31
    :try_start_b
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v3, 0xec

    aget-byte v7, v2, v3

    int-to-byte v3, v7

    sget v7, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    xor-int/lit16 v12, v7, 0x21a

    and-int/lit16 v7, v7, 0x21a

    or-int/2addr v7, v12

    int-to-short v7, v7

    const/16 v12, 0x309

    aget-byte v2, v2, v12

    :goto_d
    int-to-byte v2, v2

    invoke-static {v3, v7, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    goto :goto_e

    :cond_e
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v3, 0x7b44

    aget-byte v3, v2, v3

    int-to-byte v3, v3

    sget v7, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    or-int/lit16 v7, v7, 0x3b0e

    int-to-short v7, v7

    const/16 v12, 0xd57

    aget-byte v2, v2, v12
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_10

    goto :goto_d

    :goto_e
    :try_start_c
    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v15

    const/4 v2, 0x0

    aput-object v5, v7, v2

    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v12, v3, v10

    int-to-byte v12, v12

    const/16 v13, 0x6c

    int-to-short v13, v13

    aget-byte v14, v3, v2

    int-to-byte v2, v14

    invoke-static {v12, v13, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v12, 0x2

    new-array v14, v12, [Ljava/lang/Class;

    aget-byte v12, v3, v10

    int-to-byte v12, v12

    const/16 v19, 0x0

    aget-byte v3, v3, v19

    int-to-byte v3, v3

    invoke-static {v12, v13, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v14, v19

    aput-object v9, v14, v15

    invoke-virtual {v2, v14}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_f

    :catchall_0
    move-exception v0

    move-object v1, v0

    :try_start_d
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_f

    throw v2

    :cond_f
    throw v1

    .line 32
    :cond_10
    :goto_f
    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    aget-byte v7, v3, v10

    int-to-byte v7, v7

    const/16 v12, 0x6c

    int-to-short v12, v12

    const/4 v13, 0x0

    aget-byte v14, v3, v13

    int-to-byte v13, v14

    invoke-static {v7, v12, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v13, 0x7

    invoke-static {v7, v13}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v8, v7, v13

    aput-object v2, v7, v15

    const/4 v13, 0x2

    aput-object v5, v7, v13

    const/4 v13, 0x3

    aput-object v4, v7, v13

    const/4 v14, 0x4

    aput-object v2, v7, v14

    const/4 v2, 0x5

    aput-object v5, v7, v2

    const/4 v5, 0x6

    aput-object v4, v7, v5

    const/4 v4, 0x7

    new-array v4, v4, [Z

    const/4 v5, 0x0

    aput-boolean v5, v4, v5

    aput-boolean v15, v4, v15

    const/4 v5, 0x2

    aput-boolean v15, v4, v5

    aput-boolean v15, v4, v13

    const/4 v5, 0x4

    aput-boolean v15, v4, v5

    aput-boolean v15, v4, v2

    const/4 v5, 0x6

    aput-boolean v15, v4, v5

    const/4 v5, 0x7

    new-array v5, v5, [Z

    const/4 v14, 0x0

    aput-boolean v14, v5, v14

    aput-boolean v14, v5, v15

    const/16 v19, 0x2

    aput-boolean v14, v5, v19

    aput-boolean v14, v5, v13

    const/4 v14, 0x4

    aput-boolean v15, v5, v14

    aput-boolean v15, v5, v2

    const/4 v14, 0x6

    aput-boolean v15, v5, v14

    const/4 v14, 0x7

    new-array v11, v14, [Z

    const/16 v19, 0x0

    aput-boolean v19, v11, v19

    aput-boolean v19, v11, v15

    const/16 v20, 0x2

    aput-boolean v15, v11, v20

    aput-boolean v15, v11, v13

    const/16 v18, 0x4

    aput-boolean v19, v11, v18

    aput-boolean v15, v11, v2

    const/16 v22, 0x6

    aput-boolean v15, v11, v22
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_10

    const/16 v14, 0x52

    const/16 v23, 0x6a

    const/16 v16, 0x73

    .line 33
    :try_start_e
    aget-byte v2, v3, v16

    int-to-byte v2, v2

    const/16 v13, 0x338

    int-to-short v13, v13

    aget-byte v8, v3, v23

    int-to-byte v8, v8

    invoke-static {v2, v13, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 34
    aget-byte v8, v3, v14

    int-to-byte v8, v8

    xor-int/lit16 v13, v8, 0x112

    and-int/lit16 v14, v8, 0x112

    or-int/2addr v13, v14

    int-to-short v13, v13

    aget-byte v3, v3, v15

    int-to-byte v3, v3

    invoke-static {v8, v13, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2
    :try_end_e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_10

    const/16 v3, 0x22

    if-lt v2, v3, :cond_11

    const/4 v3, 0x1

    goto :goto_10

    :cond_11
    const/4 v3, 0x0

    :goto_10
    if-eq v3, v15, :cond_12

    const/4 v3, 0x0

    goto :goto_11

    :cond_12
    const/4 v3, 0x1

    :goto_11
    const/16 v8, 0x1d

    if-ne v2, v8, :cond_13

    goto :goto_12

    :cond_13
    const/16 v8, 0x1a

    if-lt v2, v8, :cond_15

    .line 35
    sget v8, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    or-int/lit8 v13, v8, 0xf

    shl-int/2addr v13, v15

    xor-int/lit8 v8, v8, 0xf

    sub-int/2addr v13, v8

    rem-int/lit16 v8, v13, 0x80

    sput v8, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v8, 0x2

    rem-int/2addr v13, v8

    if-nez v13, :cond_14

    goto :goto_12

    :cond_14
    const/4 v8, 0x0

    const/16 v19, 0x1

    goto :goto_13

    :cond_15
    :goto_12
    const/4 v8, 0x0

    const/16 v19, 0x0

    :goto_13
    :try_start_f
    aput-boolean v19, v11, v8
    :try_end_f
    .catch Ljava/lang/ClassNotFoundException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_10

    const/16 v8, 0x15

    if-lt v2, v8, :cond_16

    .line 36
    sget v8, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    or-int/lit8 v13, v8, 0x27

    shl-int/2addr v13, v15

    xor-int/lit8 v8, v8, 0x27

    sub-int/2addr v13, v8

    rem-int/lit16 v8, v13, 0x80

    sput v8, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v8, 0x2

    rem-int/2addr v13, v8

    const/4 v8, 0x1

    goto :goto_14

    :cond_16
    const/4 v8, 0x0

    :goto_14
    :try_start_10
    aput-boolean v8, v11, v15

    const/16 v8, 0x15

    if-lt v2, v8, :cond_17

    const/4 v2, 0x1

    goto :goto_15

    :cond_17
    const/4 v2, 0x0

    :goto_15
    const/4 v8, 0x4

    aput-boolean v2, v11, v8
    :try_end_10
    .catch Ljava/lang/ClassNotFoundException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_10

    goto :goto_16

    :catch_5
    nop

    goto :goto_16

    :catch_6
    nop

    const/4 v3, 0x0

    :goto_16
    const/4 v2, 0x0

    const/4 v8, 0x0

    :goto_17
    if-nez v2, :cond_18

    const/16 v13, 0x57

    goto :goto_18

    :cond_18
    const/16 v13, 0x27

    :goto_18
    const/16 v14, 0x27

    if-eq v13, v14, :cond_84

    const/16 v13, 0x9

    if-ge v8, v13, :cond_19

    const/4 v13, 0x0

    goto :goto_19

    :cond_19
    const/4 v13, 0x1

    :goto_19
    if-eqz v13, :cond_1a

    goto/16 :goto_64

    .line 37
    :cond_1a
    :try_start_11
    aget-boolean v13, v11, v8
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_10

    if-eqz v13, :cond_1b

    const/4 v13, 0x1

    goto :goto_1a

    :cond_1b
    const/4 v13, 0x0

    :goto_1a
    if-eqz v13, :cond_83

    .line 38
    :try_start_12
    aget-boolean v14, v4, v8

    aget-object v13, v7, v8

    aget-boolean v25, v5, v8
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4d

    if-eqz v14, :cond_1c

    move/from16 v27, v2

    const/4 v10, 0x1

    goto :goto_1b

    :cond_1c
    move/from16 v27, v2

    const/4 v10, 0x0

    :goto_1b
    if-eq v10, v15, :cond_1d

    move-object/from16 v30, v4

    goto :goto_1d

    :cond_1d
    if-eqz v13, :cond_7c

    .line 39
    sget v10, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    or-int/lit8 v28, v10, 0x45

    shl-int/lit8 v28, v28, 0x1

    xor-int/lit8 v10, v10, 0x45

    sub-int v10, v28, v10

    rem-int/lit16 v2, v10, 0x80

    sput v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v2, 0x2

    rem-int/2addr v10, v2

    if-eqz v10, :cond_1e

    const/4 v2, 0x1

    goto :goto_1c

    :cond_1e
    const/4 v2, 0x0

    :goto_1c
    if-nez v2, :cond_7b

    .line 40
    :try_start_13
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v10, 0xbd

    aget-byte v15, v2, v10
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_48

    int-to-byte v10, v15

    move-object/from16 v30, v4

    const/4 v15, 0x0

    :try_start_14
    aget-byte v4, v2, v15

    int-to-byte v4, v4

    invoke-static {v10, v12, v4}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v10, 0xec

    aget-byte v15, v2, v10

    int-to-byte v10, v15

    const/16 v15, 0xa0

    int-to-short v15, v15

    const/16 v29, 0x1

    aget-byte v2, v2, v29

    or-int/lit8 v31, v2, -0x1

    shl-int/lit8 v31, v31, 0x1

    xor-int/lit8 v2, v2, -0x1

    sub-int v2, v31, v2

    int-to-byte v2, v2

    invoke-static {v10, v15, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v10, 0x0

    invoke-virtual {v4, v2, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v13, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_47

    if-nez v2, :cond_1f

    move/from16 v46, v3

    goto/16 :goto_5e

    :cond_1f
    :goto_1d
    if-eqz v14, :cond_38

    .line 41
    :try_start_15
    new-instance v10, Ljava/util/Random;

    invoke-direct {v10}, Ljava/util/Random;-><init>()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_11

    .line 42
    :try_start_16
    sget-object v15, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v26, 0xbd

    aget-byte v2, v15, v26
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_f

    int-to-byte v2, v2

    const/16 v4, 0x36a

    int-to-short v4, v4

    move-object/from16 v33, v5

    const/16 v17, 0xec

    :try_start_17
    aget-byte v5, v15, v17

    int-to-byte v5, v5

    invoke-static {v2, v4, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    aget-byte v4, v15, v17

    int-to-byte v4, v4

    xor-int/lit8 v5, v4, 0x49

    and-int/lit8 v34, v4, 0x49

    or-int v5, v5, v34

    int-to-short v5, v5

    const/16 v32, 0x299

    aget-byte v15, v15, v32

    int-to-byte v15, v15

    invoke-static {v4, v5, v15}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_e

    const-wide/32 v34, -0x5eb50b77

    xor-long v4, v4, v34

    :try_start_18
    invoke-virtual {v10, v4, v5}, Ljava/util/Random;->setSeed(J)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_d

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v15, 0x0

    :goto_1e
    if-nez v2, :cond_36

    if-nez v4, :cond_20

    const/16 v34, 0x1

    goto :goto_1f

    :cond_20
    const/16 v34, 0x0

    :goto_1f
    if-eqz v34, :cond_21

    const/16 v34, 0x6

    move-object/from16 v34, v2

    move-object/from16 v35, v6

    const/4 v2, 0x6

    goto :goto_21

    :cond_21
    if-nez v5, :cond_22

    move-object/from16 v34, v2

    move-object/from16 v35, v6

    const/4 v2, 0x1

    goto :goto_20

    :cond_22
    move-object/from16 v34, v2

    move-object/from16 v35, v6

    const/4 v2, 0x0

    :goto_20
    const/4 v6, 0x1

    if-eq v2, v6, :cond_24

    if-nez v15, :cond_23

    const/4 v2, 0x4

    goto :goto_21

    :cond_23
    const/4 v2, 0x3

    goto :goto_21

    :cond_24
    const/4 v2, 0x5

    .line 43
    :goto_21
    :try_start_19
    new-instance v6, Ljava/lang/StringBuilder;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    xor-int/lit8 v36, v2, 0x1

    and-int/lit8 v37, v2, 0x1

    const/16 v29, 0x1

    shl-int/lit8 v37, v37, 0x1

    move-object/from16 v38, v7

    add-int v7, v36, v37

    :try_start_1a
    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v7, 0x2e

    .line 44
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_b

    const/4 v7, 0x0

    :goto_22
    if-ge v7, v2, :cond_28

    if-eqz v25, :cond_25

    const/16 v36, 0x3d

    move/from16 v37, v2

    move-object/from16 v36, v11

    const/16 v2, 0x3d

    goto :goto_23

    :cond_25
    const/16 v36, 0x1b

    move/from16 v37, v2

    move-object/from16 v36, v11

    const/16 v2, 0x1b

    :goto_23
    const/16 v11, 0x1b

    if-eq v2, v11, :cond_27

    .line 45
    sget v2, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    xor-int/lit8 v11, v2, 0x57

    and-int/lit8 v2, v2, 0x57

    const/16 v29, 0x1

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v11, v2

    rem-int/lit16 v2, v11, 0x80

    sput v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v2, 0x2

    rem-int/2addr v11, v2

    const/16 v2, 0x1a

    .line 46
    :try_start_1b
    invoke-virtual {v10, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 47
    invoke-virtual {v10}, Ljava/util/Random;->nextBoolean()Z

    move-result v11

    if-eqz v11, :cond_26

    neg-int v2, v2

    neg-int v2, v2

    xor-int/lit8 v11, v2, 0x41

    and-int/lit8 v2, v2, 0x41

    const/16 v29, 0x1

    shl-int/lit8 v2, v2, 0x1

    :goto_24
    add-int/2addr v11, v2

    goto :goto_25

    :cond_26
    neg-int v2, v2

    neg-int v2, v2

    and-int/lit8 v11, v2, 0x60

    or-int/lit8 v2, v2, 0x60

    goto :goto_24

    :goto_25
    int-to-char v2, v11

    .line 48
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_26

    :cond_27
    const/16 v2, 0xc

    .line 49
    invoke-virtual {v10, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    and-int/lit16 v11, v2, 0x2000

    or-int/lit16 v2, v2, 0x2000

    add-int/2addr v11, v2

    int-to-char v2, v11

    .line 50
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    :goto_26
    xor-int/lit8 v2, v7, 0x1

    and-int/lit8 v7, v7, 0x1

    const/4 v11, 0x1

    shl-int/2addr v7, v11

    add-int/2addr v7, v2

    move-object/from16 v11, v36

    move/from16 v2, v37

    goto :goto_22

    :catchall_1
    move-exception v0

    move-object v2, v0

    move/from16 v46, v3

    move/from16 v42, v8

    goto/16 :goto_32

    :cond_28
    move-object/from16 v36, v11

    .line 51
    :try_start_1c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_a

    if-nez v4, :cond_29

    const/16 v6, 0x8

    goto :goto_27

    :cond_29
    const/16 v6, 0x18

    :goto_27
    const/16 v7, 0x8

    if-eq v6, v7, :cond_33

    if-nez v5, :cond_2b

    const/4 v5, 0x2

    :try_start_1d
    new-array v6, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v2, v6, v5

    const/4 v2, 0x0

    aput-object v13, v6, v2

    .line 52
    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v7, 0xbd

    aget-byte v11, v5, v7

    int-to-byte v7, v11

    aget-byte v11, v5, v2

    int-to-byte v2, v11

    invoke-static {v7, v12, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v7, 0x2

    new-array v11, v7, [Ljava/lang/Class;

    move-object/from16 v37, v4

    const/16 v7, 0xbd

    aget-byte v4, v5, v7

    int-to-byte v4, v4

    const/4 v7, 0x0

    aget-byte v5, v5, v7

    int-to-byte v5, v5

    invoke-static {v4, v12, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v11, v7

    const/4 v4, 0x1

    aput-object v9, v11, v4

    invoke-virtual {v2, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    move-object v5, v2

    :goto_28
    move/from16 v42, v8

    move-object/from16 v40, v10

    move-object/from16 v2, v34

    move-object/from16 v4, v37

    goto/16 :goto_2c

    :catchall_2
    move-exception v0

    move-object v2, v0

    :try_start_1e
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_2a

    throw v4

    :cond_2a
    throw v2
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    :cond_2b
    move-object/from16 v37, v4

    if-nez v15, :cond_2e

    .line 53
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v4, v4, 0x6d

    rem-int/lit16 v6, v4, 0x80

    sput v6, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v6, 0x2

    rem-int/2addr v4, v6

    if-nez v4, :cond_2c

    const/16 v4, 0x53

    const/4 v7, 0x0

    .line 54
    :try_start_1f
    div-int/2addr v4, v7
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    goto :goto_29

    :cond_2c
    const/4 v7, 0x0

    :goto_29
    :try_start_20
    new-array v4, v6, [Ljava/lang/Object;

    const/4 v6, 0x1

    aput-object v2, v4, v6

    aput-object v13, v4, v7

    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v6, 0xbd

    aget-byte v11, v2, v6

    int-to-byte v6, v11

    aget-byte v11, v2, v7

    int-to-byte v7, v11

    invoke-static {v6, v12, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v7, 0x2

    new-array v11, v7, [Ljava/lang/Class;

    const/16 v7, 0xbd

    aget-byte v15, v2, v7

    int-to-byte v7, v15

    const/4 v15, 0x0

    aget-byte v2, v2, v15

    int-to-byte v2, v2

    invoke-static {v7, v12, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v11, v15

    const/4 v2, 0x1

    aput-object v9, v11, v2

    invoke-virtual {v6, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    move-object v15, v2

    goto :goto_28

    :catchall_3
    move-exception v0

    move-object v2, v0

    :try_start_21
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_2d

    throw v4

    :cond_2d
    throw v2
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    :cond_2e
    const/4 v4, 0x2

    :try_start_22
    new-array v6, v4, [Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v6, v4

    const/4 v2, 0x0

    aput-object v13, v6, v2

    .line 55
    sget-object v4, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v7, 0xbd

    aget-byte v11, v4, v7

    int-to-byte v7, v11

    aget-byte v11, v4, v2

    int-to-byte v2, v11

    invoke-static {v7, v12, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v7, 0x2

    new-array v11, v7, [Ljava/lang/Class;

    move-object/from16 v39, v5

    const/16 v7, 0xbd

    aget-byte v5, v4, v7

    int-to-byte v5, v5

    move-object/from16 v40, v10

    const/4 v7, 0x0

    aget-byte v10, v4, v7

    int-to-byte v10, v10

    invoke-static {v5, v12, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v11, v7

    const/4 v5, 0x1

    aput-object v9, v11, v5

    invoke-virtual {v2, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_8

    :try_start_23
    new-array v6, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v6, v5

    const/16 v5, 0xbd

    .line 56
    aget-byte v7, v4, v5

    int-to-byte v5, v7

    const/16 v7, 0xb7

    int-to-short v7, v7

    aget-byte v10, v4, v23

    int-to-byte v10, v10

    invoke-static {v5, v7, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v10, 0x1

    new-array v11, v10, [Ljava/lang/Class;

    move-object/from16 v41, v15

    const/16 v10, 0xbd

    aget-byte v15, v4, v10
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_6

    int-to-byte v10, v15

    move/from16 v42, v8

    const/4 v15, 0x0

    :try_start_24
    aget-byte v8, v4, v15

    int-to-byte v8, v8

    invoke-static {v10, v12, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    aput-object v8, v11, v15

    invoke-virtual {v5, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_5

    const/16 v6, 0xbd

    :try_start_25
    aget-byte v8, v4, v6

    int-to-byte v6, v8

    aget-byte v8, v4, v23

    int-to-byte v8, v8

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/16 v7, 0xec

    aget-byte v8, v4, v7

    int-to-byte v7, v8

    const/16 v8, 0x118

    int-to-short v10, v8

    const/16 v8, 0x1c

    aget-byte v4, v4, v8

    int-to-byte v4, v4

    invoke-static {v7, v10, v4}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_4

    move-object/from16 v4, v37

    goto/16 :goto_2b

    :catchall_4
    move-exception v0

    move-object v4, v0

    :try_start_26
    invoke-virtual {v4}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_2f

    throw v5

    :cond_2f
    throw v4

    :catchall_5
    move-exception v0

    goto :goto_2a

    :catchall_6
    move-exception v0

    move/from16 v42, v8

    :goto_2a
    move-object v4, v0

    invoke-virtual {v4}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_30

    throw v5

    :cond_30
    throw v4
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_7
    .catchall {:try_start_26 .. :try_end_26} :catchall_10

    :catch_7
    move-exception v0

    move-object v4, v0

    .line 57
    :try_start_27
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v7, 0x6d

    aget-byte v7, v6, v7

    int-to-byte v7, v7

    sget v8, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    int-to-short v8, v8

    const/16 v10, 0x1c

    aget-byte v10, v6, v10

    int-to-byte v10, v10

    invoke-static {v7, v8, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x190

    aget-byte v2, v6, v2

    or-int/lit8 v7, v2, -0x1

    const/4 v8, 0x1

    shl-int/2addr v7, v8

    xor-int/lit8 v2, v2, -0x1

    sub-int/2addr v7, v2

    int-to-byte v2, v7

    const/16 v7, 0x28f

    int-to-short v7, v7

    const/16 v10, 0x52

    aget-byte v11, v6, v10

    sub-int/2addr v11, v8

    int-to-byte v8, v11

    invoke-static {v2, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_10

    const/4 v5, 0x2

    :try_start_28
    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v4, v7, v5

    const/4 v4, 0x0

    aput-object v2, v7, v4

    const/16 v2, 0xbd

    aget-byte v4, v6, v2

    int-to-byte v2, v4

    const/16 v4, 0x99

    int-to-short v4, v4

    const/16 v5, 0x8b

    aget-byte v6, v6, v5

    int-to-byte v5, v6

    invoke-static {v2, v4, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v9, v5, v4

    const-class v4, Ljava/lang/Throwable;

    const/4 v6, 0x1

    aput-object v4, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_7

    :catchall_7
    move-exception v0

    move-object v2, v0

    :try_start_29
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_31

    throw v4

    :cond_31
    throw v2

    :catchall_8
    move-exception v0

    move/from16 v42, v8

    move-object v2, v0

    .line 58
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_32

    throw v4

    :cond_32
    throw v2
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_10

    :cond_33
    move-object/from16 v39, v5

    move/from16 v42, v8

    move-object/from16 v40, v10

    move-object/from16 v41, v15

    .line 59
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    and-int/lit8 v5, v4, 0x69

    or-int/lit8 v4, v4, 0x69

    add-int/2addr v5, v4

    rem-int/lit16 v4, v5, 0x80

    sput v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v4, 0x2

    rem-int/2addr v5, v4

    if-nez v5, :cond_35

    :try_start_2a
    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v5, v4

    const/4 v2, 0x0

    aput-object v13, v5, v2

    .line 60
    sget-object v4, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v6, 0xbd

    aget-byte v7, v4, v6

    int-to-byte v6, v7

    aget-byte v7, v4, v2

    int-to-byte v2, v7

    invoke-static {v6, v12, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/16 v6, 0xbd

    aget-byte v8, v4, v6

    int-to-byte v6, v8

    const/4 v8, 0x0

    aget-byte v4, v4, v8

    int-to-byte v4, v4

    invoke-static {v6, v12, v4}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x1

    aput-object v9, v7, v4

    invoke-virtual {v2, v7}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_9

    move-object v4, v2

    move-object/from16 v2, v34

    :goto_2b
    move-object/from16 v5, v39

    move-object/from16 v15, v41

    :goto_2c
    move-object/from16 v6, v35

    move-object/from16 v11, v36

    move-object/from16 v7, v38

    move-object/from16 v10, v40

    move/from16 v8, v42

    goto/16 :goto_1e

    :catchall_9
    move-exception v0

    move-object v2, v0

    :try_start_2b
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_34

    throw v4

    :cond_34
    throw v2

    :cond_35
    const/4 v2, 0x0

    throw v2

    :catchall_a
    move-exception v0

    move/from16 v42, v8

    goto :goto_31

    :catchall_b
    move-exception v0

    goto :goto_30

    :catchall_c
    move-exception v0

    goto :goto_2f

    :cond_36
    move-object/from16 v34, v2

    move-object/from16 v37, v4

    move-object/from16 v39, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v36, v11

    move-object/from16 v41, v15

    goto :goto_35

    :catchall_d
    move-exception v0

    goto :goto_2e

    :catchall_e
    move-exception v0

    goto :goto_2d

    :catchall_f
    move-exception v0

    move-object/from16 v33, v5

    :goto_2d
    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v36, v11

    move-object v2, v0

    .line 61
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_37

    throw v4

    :cond_37
    throw v2
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_10

    :catchall_10
    move-exception v0

    goto :goto_31

    :catchall_11
    move-exception v0

    move-object/from16 v33, v5

    :goto_2e
    move-object/from16 v35, v6

    :goto_2f
    move-object/from16 v38, v7

    :goto_30
    move/from16 v42, v8

    move-object/from16 v36, v11

    :goto_31
    move-object v2, v0

    move/from16 v46, v3

    :goto_32
    move-object/from16 v50, v9

    :goto_33
    const/16 v6, 0x52

    const/16 v15, 0xd

    :goto_34
    const/16 v18, 0x4

    goto/16 :goto_60

    :cond_38
    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v36, v11

    const/16 v34, 0x0

    const/16 v37, 0x0

    const/16 v39, 0x0

    const/16 v41, 0x0

    :goto_35
    const/16 v2, 0x1bb8

    :try_start_2c
    new-array v2, v2, [B

    .line 62
    const-class v4, Lcom/appsflyer/internal/AFb1hSDK;

    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v6, 0xc1

    aget-byte v6, v5, v6

    int-to-byte v6, v6

    const/16 v7, 0x114

    int-to-short v7, v7

    const/16 v8, 0x21

    aget-byte v8, v5, v8

    int-to-byte v8, v8

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    .line 63
    invoke-virtual {v4, v6}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_46

    const/4 v6, 0x1

    :try_start_2d
    new-array v7, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v7, v6

    const/16 v4, 0xbd

    aget-byte v6, v5, v4

    int-to-byte v4, v6

    const/16 v8, 0x1d5

    int-to-short v8, v8

    int-to-byte v6, v6

    invoke-static {v4, v8, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/4 v6, 0x1

    new-array v10, v6, [Ljava/lang/Class;

    const/16 v6, 0xbd

    aget-byte v11, v5, v6

    int-to-byte v6, v11

    const/16 v11, 0x67

    aget-byte v13, v5, v11

    int-to-short v13, v13

    const/16 v15, 0x8b

    aget-byte v11, v5, v15

    int-to-byte v11, v11

    invoke-static {v6, v13, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v11, 0x0

    aput-object v6, v10, v11

    invoke-virtual {v4, v10}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_45

    .line 64
    sget v6, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    add-int/lit8 v6, v6, 0x12

    const/4 v7, 0x1

    sub-int/2addr v6, v7

    rem-int/lit16 v10, v6, 0x80

    sput v10, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v10, 0x2

    rem-int/2addr v6, v10

    :try_start_2e
    new-array v6, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/16 v7, 0xbd

    .line 65
    aget-byte v10, v5, v7

    int-to-byte v7, v10

    int-to-byte v10, v10

    invoke-static {v7, v8, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/16 v10, 0x4e

    aget-byte v10, v5, v10

    int-to-byte v10, v10

    sget v11, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    xor-int/lit16 v13, v11, 0x282

    and-int/lit16 v11, v11, 0x282

    or-int/2addr v11, v13

    int-to-short v11, v11

    const/16 v13, 0xaf

    aget-byte v13, v5, v13

    int-to-byte v13, v13

    invoke-static {v10, v11, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v13, v11

    invoke-virtual {v7, v10, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    invoke-virtual {v7, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_44

    const/16 v6, 0xbd

    .line 66
    :try_start_2f
    aget-byte v7, v5, v6

    int-to-byte v6, v7

    int-to-byte v7, v7

    invoke-static {v6, v8, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/16 v7, 0xec

    aget-byte v8, v5, v7

    int-to-byte v7, v8

    const/16 v8, 0x118

    int-to-short v10, v8

    const/16 v8, 0x1c

    aget-byte v5, v5, v8

    int-to-byte v5, v5

    invoke-static {v7, v10, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v6, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_43

    .line 67
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    add-int/lit8 v4, v4, 0x33

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v5, 0x2

    rem-int/2addr v4, v5

    const/16 v4, 0x10

    const/16 v5, 0x1b92

    move-object/from16 v7, v35

    const/4 v6, 0x0

    :goto_36
    and-int/lit16 v8, v4, 0xf0

    or-int/lit16 v10, v4, 0xf0

    add-int/2addr v8, v10

    or-int/lit16 v10, v4, 0x1177

    const/4 v11, 0x1

    shl-int/2addr v10, v11

    xor-int/lit16 v13, v4, 0x1177

    sub-int/2addr v10, v13

    .line 68
    :try_start_30
    aget-byte v10, v2, v10

    add-int/lit8 v10, v10, -0x25

    sub-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, v2, v8

    .line 69
    array-length v8, v2
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_46

    neg-int v10, v4

    not-int v10, v10

    sub-int/2addr v8, v10

    sub-int/2addr v8, v11

    const/4 v10, 0x3

    :try_start_31
    new-array v13, v10, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v10, 0x2

    aput-object v8, v13, v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v13, v11

    const/4 v8, 0x0

    aput-object v2, v13, v8

    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v8, 0xbd

    aget-byte v10, v2, v8

    int-to-byte v8, v10

    const/16 v10, 0x321

    int-to-short v10, v10

    const/16 v11, 0x12

    aget-byte v11, v2, v11

    int-to-byte v11, v11

    invoke-static {v8, v10, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/4 v10, 0x3

    new-array v11, v10, [Ljava/lang/Class;

    const/4 v10, 0x0

    aput-object v1, v11, v10

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v15, 0x1

    aput-object v10, v11, v15

    const/4 v15, 0x2

    aput-object v10, v11, v15

    invoke-virtual {v8, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v8

    invoke-virtual {v8, v13}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object/from16 v44, v8

    check-cast v44, Ljava/io/InputStream;
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_42

    .line 70
    :try_start_32
    sget-object v8, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_46

    if-nez v8, :cond_39

    .line 71
    :try_start_33
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v8

    shr-int/lit8 v8, v8, 0x10

    neg-int v8, v8

    neg-int v8, v8

    const v10, -0x340c1aba    # -3.1967884E7f

    and-int v11, v8, v10

    or-int/2addr v8, v10

    add-int/2addr v11, v8

    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    move-result v8

    const/4 v10, 0x0

    cmpl-float v8, v8, v10

    neg-int v8, v8

    or-int/lit8 v10, v8, 0x5

    const/4 v13, 0x1

    shl-int/2addr v10, v13

    const/4 v13, 0x5

    xor-int/2addr v8, v13

    sub-int/2addr v10, v8

    int-to-short v8, v10

    const/4 v10, 0x2

    new-array v13, v10, [I

    move v15, v5

    move-object/from16 v40, v6

    .line 72
    sget-wide v5, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinking:J
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_13

    const/16 v10, 0x20

    move-object/from16 v50, v9

    ushr-long v9, v5, v10

    long-to-int v10, v9

    xor-int v9, v10, v11

    const/4 v10, 0x0

    :try_start_34
    aput v9, v13, v10

    long-to-int v6, v5

    and-int v5, v6, v11

    not-int v5, v5

    or-int/2addr v6, v11

    and-int/2addr v5, v6

    const/4 v6, 0x1

    aput v5, v13, v6

    .line 73
    new-instance v5, Lcom/appsflyer/internal/AFg1cSDK;

    sget v46, Lcom/appsflyer/internal/AFb1hSDK;->onAttributionFailure:I

    sget-object v47, Lcom/appsflyer/internal/AFb1hSDK;->onAppOpenAttribution:[B

    sget v49, Lcom/appsflyer/internal/AFb1hSDK;->onConversionDataFail:I

    move-object/from16 v43, v5

    move-object/from16 v45, v13

    move/from16 v48, v8

    invoke-direct/range {v43 .. v49}, Lcom/appsflyer/internal/AFg1cSDK;-><init>(Ljava/io/InputStream;[II[BII)V
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_12

    move/from16 v44, v4

    move-object/from16 v45, v7

    move/from16 v43, v15

    goto/16 :goto_38

    :catchall_12
    move-exception v0

    goto :goto_37

    :catchall_13
    move-exception v0

    move-object/from16 v50, v9

    :goto_37
    move-object v2, v0

    move/from16 v46, v3

    goto/16 :goto_33

    :cond_39
    move v15, v5

    move-object/from16 v40, v6

    move-object/from16 v50, v9

    const/4 v5, 0x1

    :try_start_35
    new-array v6, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 74
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v5

    const/16 v5, 0x73

    aget-byte v9, v2, v5

    int-to-byte v5, v9

    const/16 v9, 0x1b9

    int-to-short v9, v9

    const/4 v11, 0x4

    aget-byte v13, v2, v11

    int-to-byte v11, v13

    invoke-static {v5, v9, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v9, 0xc

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    xor-int/lit16 v11, v9, 0x2d2

    and-int/lit16 v13, v9, 0x2d2

    or-int/2addr v11, v13

    int-to-short v11, v11

    move/from16 v43, v15

    const/16 v13, 0x8

    aget-byte v15, v2, v13

    int-to-byte v13, v15

    invoke-static {v9, v11, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v10, v13, v11

    invoke-virtual {v5, v9, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v9, 0x0

    invoke-virtual {v5, v9, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_41

    neg-int v5, v5

    not-int v5, v5

    const v6, 0x7b11b208

    sub-int/2addr v6, v5

    const/4 v5, 0x1

    sub-int/2addr v6, v5

    const/4 v9, 0x0

    :try_start_36
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_40

    neg-int v9, v11

    or-int/lit8 v11, v9, 0x8

    shl-int/2addr v11, v5

    const/16 v5, 0x8

    xor-int/2addr v9, v5

    sub-int/2addr v11, v9

    int-to-short v9, v11

    const/4 v11, 0x3

    :try_start_37
    new-array v13, v11, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    const/4 v11, 0x2

    aput-object v9, v13, v11

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v9, 0x1

    aput-object v6, v13, v9

    const/4 v6, 0x0

    aput-object v44, v13, v6

    const/16 v6, 0xec

    aget-byte v9, v2, v6

    int-to-byte v6, v9

    const/16 v9, 0x2c0

    int-to-short v9, v9

    const/16 v11, 0x9

    aget-byte v11, v2, v11

    int-to-byte v11, v11

    invoke-static {v6, v9, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    sget-object v9, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    check-cast v9, Ljava/lang/ClassLoader;

    const/4 v11, 0x1

    invoke-static {v6, v11, v9}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v6

    const/4 v9, 0x5

    aget-byte v11, v2, v9

    int-to-byte v11, v11

    xor-int/lit16 v15, v11, 0xbc

    and-int/lit16 v5, v11, 0xbc

    or-int/2addr v5, v15

    int-to-short v5, v5

    const/4 v15, 0x1

    aget-byte v9, v2, v15

    int-to-byte v9, v9

    invoke-static {v11, v5, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x3

    new-array v11, v9, [Ljava/lang/Class;

    const/16 v9, 0xbd

    aget-byte v15, v2, v9

    int-to-byte v9, v15

    move/from16 v44, v4

    const/16 v15, 0x67

    aget-byte v4, v2, v15

    int-to-short v4, v4

    move-object/from16 v45, v7

    const/16 v15, 0x8b

    aget-byte v7, v2, v15

    int-to-byte v7, v7

    invoke-static {v9, v4, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/4 v7, 0x0

    aput-object v4, v11, v7

    const/4 v4, 0x1

    aput-object v10, v11, v4

    sget-object v4, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x2

    aput-object v4, v11, v7

    invoke-virtual {v6, v5, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v8, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/io/InputStream;
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_3f

    :goto_38
    const/16 v4, 0x10

    int-to-long v6, v4

    const/4 v4, 0x1

    :try_start_38
    new-array v8, v4, [Ljava/lang/Object;

    .line 75
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v6, 0x0

    aput-object v4, v8, v6

    const/16 v4, 0xbd

    aget-byte v6, v2, v4

    int-to-byte v4, v6

    const/16 v6, 0x67

    aget-byte v7, v2, v6

    int-to-short v6, v7

    const/16 v7, 0x8b

    aget-byte v9, v2, v7

    int-to-byte v7, v9

    invoke-static {v4, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v6, 0x63

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0x189

    int-to-short v7, v7

    const/16 v9, 0x1f4

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    invoke-static {v6, v7, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v9, v7, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {v4, v6, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v5, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_3e

    if-eqz v14, :cond_56

    .line 76
    :try_start_39
    sget-object v4, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;
    :try_end_39
    .catchall {:try_start_39 .. :try_end_39} :catchall_28

    if-nez v4, :cond_3a

    move-object/from16 v6, v37

    goto :goto_39

    :cond_3a
    move-object/from16 v6, v39

    :goto_39
    if-nez v4, :cond_3c

    .line 77
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    or-int/lit8 v7, v4, 0x39

    const/4 v8, 0x1

    shl-int/2addr v7, v8

    xor-int/lit8 v4, v4, 0x39

    sub-int/2addr v7, v4

    rem-int/lit16 v4, v7, 0x80

    sput v4, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v4, 0x2

    rem-int/2addr v7, v4

    if-nez v7, :cond_3b

    const/16 v4, 0x21

    const/4 v7, 0x0

    :try_start_3a
    div-int/2addr v4, v7
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_12

    :cond_3b
    move-object/from16 v4, v41

    goto :goto_3a

    :cond_3c
    move-object/from16 v4, v34

    :goto_3a
    const/4 v7, 0x1

    :try_start_3b
    new-array v8, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v8, v7

    const/16 v7, 0xbd

    .line 78
    aget-byte v9, v2, v7

    int-to-byte v7, v9

    const/16 v9, 0xb7

    int-to-short v9, v9

    aget-byte v10, v2, v23

    int-to-byte v10, v10

    invoke-static {v7, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v10, 0x1

    new-array v11, v10, [Ljava/lang/Class;

    const/16 v10, 0xbd

    aget-byte v13, v2, v10

    int-to-byte v10, v13

    const/4 v13, 0x0

    aget-byte v15, v2, v13

    int-to-byte v15, v15

    invoke-static {v10, v12, v15}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    aput-object v10, v11, v13

    invoke-virtual {v7, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7
    :try_end_3b
    .catchall {:try_start_3b .. :try_end_3b} :catchall_22

    if-eqz v3, :cond_3d

    const/16 v8, 0x25

    goto :goto_3b

    :cond_3d
    const/16 v8, 0x52

    :goto_3b
    const/16 v10, 0x52

    if-eq v8, v10, :cond_3f

    const/16 v8, 0xbd

    .line 79
    :try_start_3c
    aget-byte v10, v2, v8

    int-to-byte v8, v10

    const/4 v10, 0x0

    aget-byte v11, v2, v10

    int-to-byte v10, v11

    invoke-static {v8, v12, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/16 v10, 0x63

    aget-byte v10, v2, v10

    int-to-byte v10, v10

    xor-int/lit16 v11, v10, 0x1db

    and-int/lit16 v13, v10, 0x1db

    or-int/2addr v11, v13

    int-to-short v11, v11

    const/16 v13, 0x13f

    aget-byte v2, v2, v13

    int-to-byte v2, v2

    invoke-static {v10, v11, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v10, 0x0

    invoke-virtual {v8, v2, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v6, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_14

    goto :goto_3c

    :catchall_14
    move-exception v0

    move-object v2, v0

    :try_start_3d
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_3e

    throw v5

    :cond_3e
    throw v2
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_3d} :catch_8
    .catchall {:try_start_3d .. :try_end_3d} :catchall_15

    :catchall_15
    move-exception v0

    move-object v2, v0

    move/from16 v46, v3

    goto/16 :goto_45

    :catch_8
    move-exception v0

    move-object v2, v0

    move/from16 v46, v3

    goto/16 :goto_44

    :cond_3f
    :goto_3c
    const/16 v2, 0x400

    :try_start_3e
    new-array v8, v2, [B

    move/from16 v10, v43

    :goto_3d
    if-lez v10, :cond_42

    .line 80
    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v11
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_21

    .line 81
    sget v13, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v13, v13, 0x6f

    rem-int/lit16 v15, v13, 0x80

    sput v15, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v15, 0x2

    rem-int/2addr v13, v15

    const/4 v13, 0x3

    :try_start_3f
    new-array v2, v13, [Ljava/lang/Object;

    .line 82
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v2, v15

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v15, 0x1

    aput-object v13, v2, v15

    aput-object v8, v2, v11

    sget-object v11, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v13, 0xbd

    aget-byte v15, v11, v13
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_18

    int-to-byte v13, v15

    move/from16 v46, v3

    const/16 v15, 0x67

    :try_start_40
    aget-byte v3, v11, v15

    int-to-short v3, v3

    move/from16 v47, v14

    const/16 v15, 0x8b

    aget-byte v14, v11, v15

    int-to-byte v14, v14

    invoke-static {v13, v3, v14}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v13, 0x4e

    aget-byte v13, v11, v13

    int-to-byte v13, v13

    const/16 v14, 0x292

    int-to-short v14, v14

    const/16 v15, 0x1f4

    aget-byte v15, v11, v15

    int-to-byte v15, v15

    invoke-static {v13, v14, v15}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    new-array v15, v14, [Ljava/lang/Class;

    const/4 v14, 0x0

    aput-object v1, v15, v14

    sget-object v14, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v29, 0x1

    aput-object v14, v15, v29

    const/16 v20, 0x2

    aput-object v14, v15, v20

    invoke-virtual {v3, v13, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v5, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_17

    const/4 v3, -0x1

    if-eq v2, v3, :cond_43

    .line 83
    sget v3, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    and-int/lit8 v13, v3, 0xd

    const/16 v15, 0xd

    or-int/2addr v3, v15

    add-int/2addr v13, v3

    rem-int/lit16 v3, v13, 0x80

    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v3, 0x2

    rem-int/2addr v13, v3

    const/4 v13, 0x3

    :try_start_41
    new-array v15, v13, [Ljava/lang/Object;

    .line 84
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v15, v3

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/16 v19, 0x1

    aput-object v13, v15, v19

    aput-object v8, v15, v3

    const/16 v3, 0xbd

    aget-byte v13, v11, v3

    int-to-byte v3, v13

    aget-byte v13, v11, v23

    int-to-byte v13, v13

    invoke-static {v3, v9, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    move-object/from16 v48, v8

    const/16 v13, 0x67

    aget-byte v8, v11, v13

    int-to-byte v8, v8

    xor-int/lit16 v13, v8, 0x2a0

    move-object/from16 v49, v5

    and-int/lit16 v5, v8, 0x2a0

    or-int/2addr v5, v13

    int-to-short v5, v5

    const/16 v13, 0x1c

    aget-byte v11, v11, v13

    int-to-byte v11, v11

    invoke-static {v8, v5, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x3

    new-array v11, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v1, v11, v8

    const/4 v8, 0x1

    aput-object v14, v11, v8

    const/4 v13, 0x2

    aput-object v14, v11, v13

    invoke-virtual {v3, v5, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v7, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_41
    .catchall {:try_start_41 .. :try_end_41} :catchall_16

    neg-int v2, v2

    or-int v3, v10, v2

    shl-int/2addr v3, v8

    xor-int/2addr v2, v10

    sub-int v10, v3, v2

    move/from16 v3, v46

    move/from16 v14, v47

    move-object/from16 v8, v48

    move-object/from16 v5, v49

    const/16 v2, 0x400

    goto/16 :goto_3d

    :catchall_16
    move-exception v0

    move-object v2, v0

    :try_start_42
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_40

    throw v3

    :cond_40
    throw v2

    :catchall_17
    move-exception v0

    goto :goto_3e

    :catchall_18
    move-exception v0

    move/from16 v46, v3

    :goto_3e
    move-object v2, v0

    .line 85
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_41

    throw v3

    :cond_41
    throw v2
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_23

    :cond_42
    move/from16 v46, v3

    move/from16 v47, v14

    .line 86
    :cond_43
    :try_start_43
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v3, 0xbd

    aget-byte v5, v2, v3

    int-to-byte v3, v5

    aget-byte v5, v2, v23

    int-to-byte v5, v5

    invoke-static {v3, v9, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v5, 0xd

    aget-byte v8, v2, v5

    int-to-byte v5, v8

    const/16 v8, 0x14a

    int-to-short v8, v8

    const/16 v10, 0x1c

    aget-byte v11, v2, v10

    int-to-byte v10, v11

    invoke-static {v5, v8, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_20

    const/16 v5, 0xbd

    :try_start_44
    aget-byte v8, v2, v5

    int-to-byte v5, v8

    const/16 v8, 0x267

    aget-byte v8, v2, v8

    int-to-short v8, v8

    const/16 v10, 0x18

    aget-byte v10, v2, v10

    int-to-byte v10, v10

    invoke-static {v5, v8, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v8, 0x63

    aget-byte v8, v2, v8

    int-to-byte v8, v8

    or-int/lit16 v10, v8, 0x241

    int-to-short v10, v10

    const/16 v11, 0x1f4

    aget-byte v11, v2, v11

    int-to-byte v11, v11

    invoke-static {v8, v10, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v5, v8, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v3, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_44
    .catchall {:try_start_44 .. :try_end_44} :catchall_1f

    const/16 v3, 0xbd

    .line 87
    :try_start_45
    aget-byte v5, v2, v3

    int-to-byte v3, v5

    aget-byte v5, v2, v23

    int-to-byte v5, v5

    invoke-static {v3, v9, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v5, 0xec

    aget-byte v8, v2, v5

    int-to-byte v5, v8

    const/16 v8, 0x118

    int-to-short v9, v8

    const/16 v8, 0x1c

    aget-byte v10, v2, v8

    int-to-byte v8, v10

    invoke-static {v5, v9, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_1e

    const/16 v3, 0x299

    .line 88
    :try_start_46
    aget-byte v5, v2, v3

    int-to-byte v3, v5

    const/16 v5, 0x28f

    int-to-short v5, v5

    const/4 v7, 0x4

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    invoke-static {v3, v5, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v5, 0x65

    .line 89
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    or-int/lit16 v7, v5, 0x1b4

    int-to-short v7, v7

    const/4 v8, 0x1

    aget-byte v9, v2, v8

    int-to-byte v9, v9

    invoke-static {v5, v7, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x3

    new-array v9, v7, [Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v50, v9, v7

    aput-object v50, v9, v8

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x2

    aput-object v7, v9, v8

    invoke-virtual {v3, v5, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v5, 0x3

    new-array v7, v5, [Ljava/lang/Object;
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_23

    const/16 v5, 0xbd

    .line 90
    :try_start_47
    aget-byte v8, v2, v5

    int-to-byte v5, v8

    const/4 v8, 0x0

    aget-byte v9, v2, v8

    int-to-byte v8, v9

    invoke-static {v5, v12, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v8, 0xd

    aget-byte v9, v2, v8

    int-to-byte v8, v9

    const/16 v9, 0xe4

    int-to-short v9, v9

    const/16 v10, 0x1a

    aget-byte v10, v2, v10

    int-to-byte v10, v10

    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v5, v8, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v6, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_1d

    const/4 v8, 0x0

    :try_start_48
    aput-object v5, v7, v8
    :try_end_48
    .catchall {:try_start_48 .. :try_end_48} :catchall_23

    .line 91
    sget v5, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    add-int/lit8 v5, v5, 0x2d

    rem-int/lit16 v8, v5, 0x80

    sput v8, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v8, 0x2

    rem-int/2addr v5, v8

    const/16 v5, 0xbd

    .line 92
    :try_start_49
    aget-byte v8, v2, v5

    int-to-byte v5, v8

    const/4 v8, 0x0

    aget-byte v10, v2, v8

    int-to-byte v8, v10

    invoke-static {v5, v12, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v8, 0xd

    aget-byte v10, v2, v8

    int-to-byte v8, v10

    const/16 v10, 0x1a

    aget-byte v10, v2, v10

    int-to-byte v10, v10

    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_49
    .catchall {:try_start_49 .. :try_end_49} :catchall_1c

    const/4 v8, 0x1

    :try_start_4a
    aput-object v5, v7, v8

    const/4 v5, 0x0

    .line 93
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v5, 0x2

    aput-object v8, v7, v5

    .line 94
    invoke-virtual {v3, v9, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_4a
    .catchall {:try_start_4a .. :try_end_4a} :catchall_23

    const/16 v5, 0xbd

    .line 95
    :try_start_4b
    aget-byte v7, v2, v5

    int-to-byte v5, v7

    const/4 v7, 0x0

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    invoke-static {v5, v12, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v7, 0x299

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    const/16 v8, 0x14f

    int-to-short v8, v8

    const/16 v9, 0xc8

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v5, v7, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v6, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_4b
    .catchall {:try_start_4b .. :try_end_4b} :catchall_1b

    const/16 v5, 0xbd

    .line 96
    :try_start_4c
    aget-byte v6, v2, v5

    int-to-byte v5, v6

    const/4 v6, 0x0

    aget-byte v7, v2, v6

    int-to-byte v6, v7

    invoke-static {v5, v12, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0x299

    aget-byte v7, v2, v6

    int-to-byte v6, v7

    const/16 v7, 0xc8

    aget-byte v7, v2, v7

    int-to-byte v7, v7

    invoke-static {v6, v8, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_4c
    .catchall {:try_start_4c .. :try_end_4c} :catchall_1a

    .line 97
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v5, v4, 0x55

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v6, 0x2

    rem-int/2addr v5, v6

    if-nez v5, :cond_44

    const/16 v5, 0x56

    goto :goto_3f

    :cond_44
    const/4 v5, 0x6

    :goto_3f
    const/16 v6, 0x56

    if-eq v5, v6, :cond_4a

    .line 98
    :try_start_4d
    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;
    :try_end_4d
    .catchall {:try_start_4d .. :try_end_4d} :catchall_27

    if-nez v5, :cond_45

    const/16 v5, 0x33

    goto :goto_40

    :cond_45
    const/16 v5, 0x1d

    :goto_40
    const/16 v6, 0x1d

    if-eq v5, v6, :cond_49

    add-int/lit8 v4, v4, 0x5e

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    .line 99
    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v5, 0x2

    rem-int/2addr v4, v5

    if-nez v4, :cond_46

    const/4 v4, 0x0

    goto :goto_41

    :cond_46
    const/4 v4, 0x1

    :goto_41
    if-eqz v4, :cond_48

    .line 100
    :try_start_4e
    const-class v4, Lcom/appsflyer/internal/AFb1hSDK;
    :try_end_4e
    .catchall {:try_start_4e .. :try_end_4e} :catchall_27

    :try_start_4f
    const-class v5, Ljava/lang/Class;

    const/16 v6, 0xd

    aget-byte v7, v2, v6

    int-to-byte v6, v7

    const/16 v7, 0x186

    int-to-short v7, v7

    const/16 v8, 0x73

    aget-byte v2, v2, v8

    int-to-byte v2, v2

    invoke-static {v6, v7, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_4f
    .catchall {:try_start_4f .. :try_end_4f} :catchall_19

    :try_start_50
    sput-object v2, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    goto :goto_42

    :catchall_19
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_47

    throw v3

    :cond_47
    throw v2

    :cond_48
    const/4 v2, 0x0

    throw v2

    :cond_49
    :goto_42
    const/4 v2, 0x0

    const/4 v14, 0x3

    goto/16 :goto_4f

    :cond_4a
    const/4 v2, 0x0

    .line 101
    throw v2

    :catchall_1a
    move-exception v0

    move-object v2, v0

    .line 102
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4b

    throw v3

    :cond_4b
    throw v2

    :catchall_1b
    move-exception v0

    move-object v2, v0

    .line 103
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4c

    throw v3

    :cond_4c
    throw v2
    :try_end_50
    .catchall {:try_start_50 .. :try_end_50} :catchall_27

    :catchall_1c
    move-exception v0

    move-object v2, v0

    .line 104
    :try_start_51
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4d

    throw v3

    :cond_4d
    throw v2

    :catchall_1d
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4e

    throw v3

    :cond_4e
    throw v2

    :catchall_1e
    move-exception v0

    move-object v2, v0

    .line 105
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4f

    throw v3

    :cond_4f
    throw v2

    :catchall_1f
    move-exception v0

    move-object v2, v0

    .line 106
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_50

    throw v3

    :cond_50
    throw v2

    :catchall_20
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_51

    throw v3

    :cond_51
    throw v2
    :try_end_51
    .catchall {:try_start_51 .. :try_end_51} :catchall_23

    :catchall_21
    move-exception v0

    move/from16 v46, v3

    goto :goto_43

    :catchall_22
    move-exception v0

    move/from16 v46, v3

    move-object v2, v0

    .line 107
    :try_start_52
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_52

    throw v3

    :cond_52
    throw v2
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_52} :catch_9
    .catchall {:try_start_52 .. :try_end_52} :catchall_23

    :catchall_23
    move-exception v0

    :goto_43
    move-object v2, v0

    goto/16 :goto_45

    :catch_9
    move-exception v0

    move-object v2, v0

    .line 108
    :goto_44
    :try_start_53
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v7, 0x6d

    aget-byte v7, v5, v7

    int-to-byte v7, v7

    const/16 v8, 0x228

    int-to-short v8, v8

    const/16 v9, 0x1c

    aget-byte v9, v5, v9

    int-to-byte v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v7, 0x190

    aget-byte v7, v5, v7

    and-int/lit8 v8, v7, -0x1

    or-int/lit8 v7, v7, -0x1

    add-int/2addr v8, v7

    int-to-byte v7, v8

    const/16 v8, 0x28f

    int-to-short v8, v8

    const/16 v9, 0x52

    aget-byte v10, v5, v9

    xor-int/lit8 v9, v10, -0x1

    and-int/lit8 v10, v10, -0x1

    const/4 v11, 0x1

    shl-int/2addr v10, v11

    add-int/2addr v9, v10

    int-to-byte v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_53
    .catchall {:try_start_53 .. :try_end_53} :catchall_23

    const/4 v7, 0x2

    :try_start_54
    new-array v8, v7, [Ljava/lang/Object;

    const/4 v7, 0x1

    aput-object v2, v8, v7

    const/4 v2, 0x0

    aput-object v3, v8, v2

    const/16 v2, 0xbd

    aget-byte v3, v5, v2

    int-to-byte v2, v3

    const/16 v3, 0x99

    int-to-short v3, v3

    const/16 v7, 0x8b

    aget-byte v5, v5, v7

    int-to-byte v5, v5

    invoke-static {v2, v3, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v50, v5, v3

    const-class v3, Ljava/lang/Throwable;

    const/4 v7, 0x1

    aput-object v3, v5, v7

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_54
    .catchall {:try_start_54 .. :try_end_54} :catchall_24

    :catchall_24
    move-exception v0

    move-object v2, v0

    :try_start_55
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_53

    throw v3

    :cond_53
    throw v2
    :try_end_55
    .catchall {:try_start_55 .. :try_end_55} :catchall_23

    .line 109
    :goto_45
    :try_start_56
    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v5, 0xbd

    aget-byte v7, v3, v5

    int-to-byte v5, v7

    const/4 v7, 0x0

    aget-byte v8, v3, v7

    int-to-byte v7, v8

    invoke-static {v5, v12, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v7, 0x299

    aget-byte v8, v3, v7

    int-to-byte v7, v8

    const/16 v8, 0x14f

    int-to-short v8, v8

    const/16 v9, 0xc8

    aget-byte v9, v3, v9

    int-to-byte v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v5, v7, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v6, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_56
    .catchall {:try_start_56 .. :try_end_56} :catchall_26

    const/16 v5, 0xbd

    .line 110
    :try_start_57
    aget-byte v6, v3, v5

    int-to-byte v5, v6

    const/4 v6, 0x0

    aget-byte v7, v3, v6

    int-to-byte v6, v7

    invoke-static {v5, v12, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0x299

    aget-byte v6, v3, v6

    int-to-byte v6, v6

    const/16 v7, 0xc8

    aget-byte v3, v3, v7

    int-to-byte v3, v3

    invoke-static {v6, v8, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_57
    .catchall {:try_start_57 .. :try_end_57} :catchall_25

    .line 111
    :try_start_58
    throw v2

    :catchall_25
    move-exception v0

    move-object v2, v0

    .line 112
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_54

    throw v3

    :cond_54
    throw v2

    :catchall_26
    move-exception v0

    move-object v2, v0

    .line 113
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_55

    throw v3

    :cond_55
    throw v2
    :try_end_58
    .catchall {:try_start_58 .. :try_end_58} :catchall_27

    :catchall_27
    move-exception v0

    goto :goto_46

    :catchall_28
    move-exception v0

    move/from16 v46, v3

    :goto_46
    move-object v2, v0

    goto/16 :goto_33

    :cond_56
    move/from16 v46, v3

    move-object/from16 v49, v5

    move/from16 v47, v14

    .line 114
    :try_start_59
    new-instance v3, Ljava/util/zip/ZipInputStream;

    move-object/from16 v5, v49

    invoke-direct {v3, v5}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 115
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4
    :try_end_59
    .catchall {:try_start_59 .. :try_end_59} :catchall_3d

    .line 116
    sget v5, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    xor-int/lit8 v6, v5, 0x37

    and-int/lit8 v5, v5, 0x37

    const/4 v7, 0x1

    shl-int/2addr v5, v7

    add-int/2addr v6, v5

    rem-int/lit16 v5, v6, 0x80

    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v5, 0x2

    rem-int/2addr v6, v5

    :try_start_5a
    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/16 v3, 0xbd

    .line 117
    aget-byte v6, v2, v3

    int-to-byte v3, v6

    const/16 v6, 0x384

    int-to-short v6, v6

    const/16 v7, 0xc

    aget-byte v7, v2, v7

    int-to-byte v7, v7

    invoke-static {v3, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v7, 0x1

    new-array v8, v7, [Ljava/lang/Class;

    const/16 v7, 0xbd

    aget-byte v9, v2, v7

    int-to-byte v7, v9

    const/16 v9, 0x67

    aget-byte v10, v2, v9

    int-to-short v9, v10

    const/16 v10, 0x8b

    aget-byte v11, v2, v10

    int-to-byte v10, v11

    invoke-static {v7, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v9, 0x0

    aput-object v7, v8, v9

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_5a
    .catchall {:try_start_5a .. :try_end_5a} :catchall_3c

    const/16 v5, 0xbd

    :try_start_5b
    aget-byte v7, v2, v5

    int-to-byte v5, v7

    const/16 v7, 0x1a5

    int-to-short v7, v7

    const/16 v8, 0x91

    aget-byte v2, v2, v8

    int-to-byte v2, v2

    invoke-static {v5, v7, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_5b
    .catchall {:try_start_5b .. :try_end_5b} :catchall_3b

    const/16 v5, 0x400

    :try_start_5c
    new-array v5, v5, [B
    :try_end_5c
    .catchall {:try_start_5c .. :try_end_5c} :catchall_3d

    const/4 v8, 0x0

    :goto_47
    const/4 v9, 0x1

    :try_start_5d
    new-array v10, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v10, v9

    .line 118
    sget-object v9, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v11, 0xbd

    aget-byte v13, v9, v11

    int-to-byte v11, v13

    const/16 v13, 0xc

    aget-byte v13, v9, v13

    int-to-byte v13, v13

    invoke-static {v11, v6, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    const/16 v13, 0x4e

    aget-byte v13, v9, v13

    int-to-byte v13, v13

    const/16 v14, 0x292

    int-to-short v14, v14

    const/16 v15, 0x1f4

    aget-byte v15, v9, v15

    int-to-byte v15, v15

    invoke-static {v13, v14, v15}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    new-array v15, v14, [Ljava/lang/Class;

    const/4 v14, 0x0

    aput-object v1, v15, v14

    invoke-virtual {v11, v13, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    invoke-virtual {v11, v3, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10
    :try_end_5d
    .catchall {:try_start_5d .. :try_end_5d} :catchall_3a

    if-lez v10, :cond_57

    const/16 v11, 0x1c

    goto :goto_48

    :cond_57
    const/16 v11, 0x39

    :goto_48
    const/16 v13, 0x1c

    if-eq v11, v13, :cond_59

    :cond_58
    const/4 v14, 0x3

    goto :goto_4a

    :cond_59
    int-to-long v13, v8

    .line 119
    :try_start_5e
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v48
    :try_end_5e
    .catchall {:try_start_5e .. :try_end_5e} :catchall_3d

    cmp-long v11, v13, v48

    if-gez v11, :cond_58

    const/4 v11, 0x3

    :try_start_5f
    new-array v13, v11, [Ljava/lang/Object;

    .line 120
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v14, 0x2

    aput-object v11, v13, v14

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/4 v15, 0x1

    aput-object v14, v13, v15

    aput-object v5, v13, v11

    const/16 v11, 0xbd

    aget-byte v14, v9, v11

    int-to-byte v11, v14

    const/16 v14, 0x91

    aget-byte v14, v9, v14

    int-to-byte v14, v14

    invoke-static {v11, v7, v14}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    const/16 v14, 0x67

    aget-byte v15, v9, v14

    int-to-byte v14, v15

    or-int/lit16 v15, v14, 0x2a0

    int-to-short v15, v15

    const/16 v28, 0x1c

    aget-byte v9, v9, v28

    int-to-byte v9, v9

    invoke-static {v14, v15, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v9
    :try_end_5f
    .catchall {:try_start_5f .. :try_end_5f} :catchall_2a

    const/4 v14, 0x3

    :try_start_60
    new-array v15, v14, [Ljava/lang/Class;

    const/16 v19, 0x0

    aput-object v1, v15, v19

    sget-object v24, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v29, 0x1

    aput-object v24, v15, v29

    const/16 v20, 0x2

    aput-object v24, v15, v20

    invoke-virtual {v11, v9, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    invoke-virtual {v9, v2, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_60
    .catchall {:try_start_60 .. :try_end_60} :catchall_29

    and-int v9, v8, v10

    or-int/2addr v8, v10

    add-int/2addr v8, v9

    goto/16 :goto_47

    :catchall_29
    move-exception v0

    goto :goto_49

    :catchall_2a
    move-exception v0

    const/4 v14, 0x3

    :goto_49
    move-object v2, v0

    :try_start_61
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_5a

    throw v3

    :cond_5a
    throw v2
    :try_end_61
    .catchall {:try_start_61 .. :try_end_61} :catchall_27

    :goto_4a
    const/16 v4, 0xbd

    .line 121
    :try_start_62
    aget-byte v5, v9, v4

    int-to-byte v4, v5

    const/16 v5, 0x91

    aget-byte v5, v9, v5

    int-to-byte v5, v5

    invoke-static {v4, v7, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x9

    aget-byte v5, v9, v5

    int-to-byte v5, v5

    const/16 v8, 0x3b

    aget-byte v8, v9, v8

    int-to-short v8, v8

    const/16 v10, 0x13f

    aget-byte v10, v9, v10

    int-to-byte v10, v10

    invoke-static {v5, v8, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v2, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_62
    .catchall {:try_start_62 .. :try_end_62} :catchall_39

    const/16 v5, 0xbd

    .line 122
    :try_start_63
    aget-byte v8, v9, v5

    int-to-byte v5, v8

    const/16 v8, 0xc

    aget-byte v8, v9, v8

    int-to-byte v8, v8

    invoke-static {v5, v6, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0xec

    aget-byte v8, v9, v6

    int-to-byte v6, v8

    const/16 v8, 0x118

    int-to-short v10, v8

    const/16 v8, 0x1c

    aget-byte v9, v9, v8

    int-to-byte v8, v9

    invoke-static {v6, v10, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v3, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_63
    .catchall {:try_start_63 .. :try_end_63} :catchall_2b

    goto :goto_4b

    :catchall_2b
    move-exception v0

    move-object v3, v0

    :try_start_64
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_5b

    throw v5

    :cond_5b
    throw v3
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_64} :catch_a
    .catchall {:try_start_64 .. :try_end_64} :catchall_27

    .line 123
    :catch_a
    :goto_4b
    :try_start_65
    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v5, 0xbd

    aget-byte v6, v3, v5

    int-to-byte v5, v6

    const/16 v6, 0x91

    aget-byte v6, v3, v6

    int-to-byte v6, v6

    invoke-static {v5, v7, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0xec

    aget-byte v7, v3, v6

    int-to-byte v6, v7

    const/16 v7, 0x118

    int-to-short v8, v7

    const/16 v7, 0x1c

    aget-byte v3, v3, v7

    int-to-byte v3, v3

    invoke-static {v6, v8, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_65
    .catchall {:try_start_65 .. :try_end_65} :catchall_2c

    goto :goto_4c

    :catchall_2c
    move-exception v0

    move-object v2, v0

    :try_start_66
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_5c

    throw v3

    :cond_5c
    throw v2
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_66} :catch_b
    .catchall {:try_start_66 .. :try_end_66} :catchall_27

    .line 124
    :catch_b
    :goto_4c
    :try_start_67
    const-class v2, Lcom/appsflyer/internal/AFb1hSDK;
    :try_end_67
    .catchall {:try_start_67 .. :try_end_67} :catchall_3d

    :try_start_68
    const-class v3, Ljava/lang/Class;

    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v6, 0xd

    aget-byte v7, v5, v6

    int-to-byte v6, v7

    const/16 v7, 0x186

    int-to-short v7, v7

    const/16 v8, 0x73

    aget-byte v9, v5, v8

    int-to-byte v8, v9

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v2, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_68
    .catchall {:try_start_68 .. :try_end_68} :catchall_38

    const/16 v3, 0x299

    .line 125
    :try_start_69
    aget-byte v6, v5, v3

    int-to-byte v3, v6

    const/16 v6, 0x306

    int-to-short v6, v6

    const/16 v7, 0x67

    aget-byte v8, v5, v7

    int-to-byte v7, v8

    invoke-static {v3, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/16 v6, 0xbd

    .line 126
    aget-byte v8, v5, v6

    int-to-byte v6, v8

    const/16 v8, 0x34a

    int-to-short v8, v8

    const/16 v9, 0x8b

    aget-byte v10, v5, v9

    int-to-byte v9, v10

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v9, 0x0

    aput-object v6, v7, v9

    const/16 v6, 0xbd

    aget-byte v9, v5, v6

    int-to-byte v6, v9

    const/16 v9, 0x208

    int-to-short v9, v9

    const/4 v10, 0x4

    aget-byte v11, v5, v10

    int-to-byte v10, v11

    invoke-static {v6, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v9, 0x1

    aput-object v6, v7, v9

    invoke-virtual {v3, v7}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Object;
    :try_end_69
    .catchall {:try_start_69 .. :try_end_69} :catchall_3d

    :try_start_6a
    new-array v6, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v6, v9

    const/16 v4, 0xbd

    .line 127
    aget-byte v9, v5, v4

    int-to-byte v4, v9

    const/16 v9, 0x8b

    aget-byte v10, v5, v9

    int-to-byte v9, v10

    invoke-static {v4, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v8, 0x67

    aget-byte v9, v5, v8

    int-to-byte v8, v9

    const/16 v9, 0x309

    aget-byte v9, v5, v9

    int-to-short v9, v9

    const/16 v10, 0x1f4

    aget-byte v10, v5, v10

    int-to-byte v10, v10

    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-virtual {v4, v8, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v8, 0x0

    invoke-virtual {v4, v8, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_6a
    .catchall {:try_start_6a .. :try_end_6a} :catchall_37

    :try_start_6b
    aput-object v4, v7, v11

    aput-object v2, v7, v9

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_6b
    .catchall {:try_start_6b .. :try_end_6b} :catchall_3d

    const/16 v4, 0x299

    .line 128
    :try_start_6c
    aget-byte v6, v5, v4

    int-to-byte v4, v6

    const/16 v6, 0x272

    int-to-short v6, v6

    const/16 v7, 0x63

    aget-byte v7, v5, v7

    int-to-byte v7, v7

    invoke-static {v4, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v6, 0x91

    .line 129
    aget-byte v6, v5, v6

    int-to-byte v6, v6

    xor-int/lit16 v7, v6, 0x80

    and-int/lit16 v8, v6, 0x80

    or-int/2addr v7, v8

    int-to-short v7, v7

    const/4 v8, 0x1

    aget-byte v9, v5, v8

    const/4 v10, 0x0

    sub-int/2addr v9, v10

    sub-int/2addr v9, v8

    int-to-byte v9, v9

    invoke-static {v6, v7, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 130
    invoke-virtual {v4, v8}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 131
    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 132
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const/16 v8, 0xc

    .line 133
    aget-byte v8, v5, v8

    int-to-byte v8, v8

    const/16 v9, 0x44

    aget-byte v9, v5, v9

    int-to-short v9, v9

    aget-byte v10, v5, v23

    int-to-byte v10, v10

    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    const/4 v9, 0x1

    .line 134
    invoke-virtual {v8, v9}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/16 v9, 0xc

    .line 135
    aget-byte v9, v5, v9

    int-to-byte v9, v9

    const/16 v10, 0x130

    int-to-short v10, v10

    const/16 v11, 0x65

    aget-byte v5, v5, v11

    int-to-byte v5, v5

    invoke-static {v9, v10, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    const/4 v7, 0x1

    .line 136
    invoke-virtual {v5, v7}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 137
    invoke-virtual {v8, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 138
    invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 139
    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 140
    new-instance v9, Ljava/util/ArrayList;

    check-cast v7, Ljava/util/List;

    invoke-direct {v9, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 141
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 142
    invoke-virtual {v7}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v7

    .line 143
    invoke-static {v6}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v10

    .line 144
    invoke-static {v7, v10}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    const/4 v11, 0x0

    :goto_4d
    if-ge v11, v10, :cond_5d

    const/4 v13, 0x4

    goto :goto_4e

    :cond_5d
    const/16 v13, 0xe

    :goto_4e
    const/4 v15, 0x4

    if-eq v13, v15, :cond_6a

    .line 145
    invoke-virtual {v8, v4, v9}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 146
    invoke-virtual {v5, v4, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_6c} :catch_f
    .catchall {:try_start_6c .. :try_end_6c} :catchall_3d

    .line 147
    :try_start_6d
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;
    :try_end_6d
    .catchall {:try_start_6d .. :try_end_6d} :catchall_3d

    if-nez v2, :cond_5e

    .line 148
    :try_start_6e
    sput-object v3, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;
    :try_end_6e
    .catchall {:try_start_6e .. :try_end_6e} :catchall_27

    :cond_5e
    :goto_4f
    if-eqz v47, :cond_62

    .line 149
    :try_start_6f
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v13, 0x299

    aget-byte v4, v2, v13

    int-to-byte v4, v4

    const/16 v5, 0x28f

    int-to-short v5, v5

    const/4 v6, 0x4

    aget-byte v7, v2, v6

    int-to-byte v6, v7

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x65

    .line 150
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x1d

    aget-byte v6, v2, v6

    neg-int v6, v6

    int-to-short v6, v6

    const/16 v7, 0xaf

    aget-byte v7, v2, v7

    int-to-byte v7, v7

    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v50, v7, v6

    const/16 v6, 0xbd

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    const/16 v8, 0x208

    int-to-short v8, v8

    const/4 v9, 0x4

    aget-byte v10, v2, v9

    int-to-byte v9, v10

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v8, 0x1

    aput-object v6, v7, v8

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 151
    invoke-virtual {v5, v8}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v45, v7, v6

    .line 152
    const-class v6, Lcom/appsflyer/internal/AFb1hSDK;
    :try_end_6f
    .catchall {:try_start_6f .. :try_end_6f} :catchall_30

    :try_start_70
    const-class v8, Ljava/lang/Class;
    :try_end_70
    .catchall {:try_start_70 .. :try_end_70} :catchall_2e

    const/16 v15, 0xd

    :try_start_71
    aget-byte v9, v2, v15

    int-to-byte v9, v9

    const/16 v10, 0x186

    int-to-short v10, v10

    const/16 v11, 0x73

    aget-byte v13, v2, v11

    int-to-byte v11, v13

    invoke-static {v9, v10, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    invoke-virtual {v8, v6, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6
    :try_end_71
    .catchall {:try_start_71 .. :try_end_71} :catchall_2d

    const/4 v8, 0x1

    :try_start_72
    aput-object v6, v7, v8

    invoke-virtual {v5, v3, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_5f

    const/16 v6, 0x18

    goto :goto_50

    :cond_5f
    const/16 v6, 0x40

    :goto_50
    const/16 v7, 0x40

    if-eq v6, v7, :cond_60

    const/16 v6, 0xec

    .line 153
    aget-byte v7, v2, v6

    int-to-byte v6, v7

    const/16 v7, 0x118

    int-to-short v8, v7

    const/16 v7, 0x1c

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    invoke-static {v6, v8, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    new-array v7, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v2, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    .line 154
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_60
    move-object v2, v5

    const/16 v18, 0x4

    goto :goto_53

    :catchall_2d
    move-exception v0

    goto :goto_51

    :catchall_2e
    move-exception v0

    const/16 v15, 0xd

    :goto_51
    move-object v2, v0

    .line 155
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_61

    throw v3

    :cond_61
    throw v2
    :try_end_72
    .catchall {:try_start_72 .. :try_end_72} :catchall_2f

    :catchall_2f
    move-exception v0

    goto :goto_52

    :catchall_30
    move-exception v0

    const/16 v15, 0xd

    :goto_52
    move-object v2, v0

    const/16 v6, 0x52

    goto/16 :goto_34

    :cond_62
    const/16 v15, 0xd

    .line 156
    :try_start_73
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v4, 0xbd

    aget-byte v5, v2, v4
    :try_end_73
    .catchall {:try_start_73 .. :try_end_73} :catchall_35

    int-to-byte v4, v5

    const/16 v5, 0x208

    int-to-short v5, v5

    const/16 v18, 0x4

    :try_start_74
    aget-byte v6, v2, v18

    int-to-byte v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x65

    .line 157
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x1d

    aget-byte v6, v2, v6

    neg-int v6, v6

    int-to-short v6, v6

    const/16 v7, 0xaf

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    invoke-static {v5, v6, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v50, v6, v7

    invoke-virtual {v4, v2, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_74
    .catchall {:try_start_74 .. :try_end_74} :catchall_49

    .line 158
    :try_start_75
    invoke-virtual {v2, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v45, v4, v7

    .line 159
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_75
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_75 .. :try_end_75} :catch_c
    .catchall {:try_start_75 .. :try_end_75} :catchall_49

    goto :goto_53

    :catch_c
    move-exception v0

    move-object v2, v0

    .line 160
    :try_start_76
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljava/lang/Exception;

    throw v2
    :try_end_76
    .catch Ljava/lang/ClassNotFoundException; {:try_start_76 .. :try_end_76} :catch_d
    .catchall {:try_start_76 .. :try_end_76} :catchall_49

    :catch_d
    nop

    const/4 v2, 0x0

    :goto_53
    if-eqz v2, :cond_67

    .line 161
    :try_start_77
    move-object v6, v2

    check-cast v6, Ljava/lang/Class;

    .line 162
    sget-object v2, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v4, 0xec

    aget-byte v5, v2, v4

    int-to-byte v4, v5

    sget v5, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    xor-int/lit16 v7, v5, 0x118

    and-int/lit16 v8, v5, 0x118

    or-int/2addr v7, v8

    int-to-short v7, v7

    const/16 v8, 0x9

    aget-byte v8, v2, v8

    int-to-byte v8, v8

    invoke-static {v4, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/Class;

    .line 163
    const-class v4, Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v9, 0x1

    aput-object v4, v8, v9

    .line 164
    invoke-virtual {v6, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 165
    invoke-virtual {v4, v9}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v8, 0x2

    new-array v9, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v9, v8
    :try_end_77
    .catchall {:try_start_77 .. :try_end_77} :catchall_49

    if-nez v47, :cond_63

    .line 166
    sget v3, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    and-int/lit8 v8, v3, 0x2d

    or-int/lit8 v3, v3, 0x2d

    add-int/2addr v8, v3

    rem-int/lit16 v3, v8, 0x80

    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v3, 0x2

    rem-int/2addr v8, v3

    const/4 v3, 0x1

    goto :goto_54

    :cond_63
    const/4 v3, 0x0

    .line 167
    :goto_54
    :try_start_78
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v8, 0x1

    aput-object v3, v9, v8

    invoke-virtual {v4, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    const/16 v3, 0x1188

    new-array v3, v3, [B

    .line 168
    const-class v4, Lcom/appsflyer/internal/AFb1hSDK;

    const/16 v8, 0xc1

    aget-byte v8, v2, v8

    int-to-byte v8, v8

    const/16 v9, 0x39d

    int-to-short v9, v9

    const/16 v10, 0x21

    aget-byte v10, v2, v10

    int-to-byte v10, v10

    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    .line 169
    invoke-virtual {v4, v8}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4
    :try_end_78
    .catchall {:try_start_78 .. :try_end_78} :catchall_49

    const/4 v8, 0x1

    :try_start_79
    new-array v9, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v9, v8

    const/16 v4, 0xbd

    aget-byte v8, v2, v4

    int-to-byte v4, v8

    const/16 v10, 0x1d5

    int-to-short v10, v10

    int-to-byte v8, v8

    invoke-static {v4, v10, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/4 v8, 0x1

    new-array v11, v8, [Ljava/lang/Class;

    const/16 v8, 0xbd

    aget-byte v13, v2, v8

    int-to-byte v8, v13

    const/16 v13, 0x67

    aget-byte v14, v2, v13

    int-to-short v14, v14

    const/16 v21, 0x8b

    aget-byte v13, v2, v21

    int-to-byte v13, v13

    invoke-static {v8, v14, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/4 v13, 0x0

    aput-object v8, v11, v13

    invoke-virtual {v4, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_79
    .catchall {:try_start_79 .. :try_end_79} :catchall_34

    const/4 v8, 0x1

    :try_start_7a
    new-array v9, v8, [Ljava/lang/Object;

    aput-object v3, v9, v13

    const/16 v8, 0xbd

    .line 170
    aget-byte v11, v2, v8

    int-to-byte v8, v11

    int-to-byte v11, v11

    invoke-static {v8, v10, v11}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/16 v11, 0x4e

    aget-byte v11, v2, v11

    int-to-byte v11, v11

    or-int/lit16 v5, v5, 0x282

    int-to-short v5, v5

    const/16 v13, 0xaf

    aget-byte v13, v2, v13

    int-to-byte v13, v13

    invoke-static {v11, v5, v13}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v13, v11

    invoke-virtual {v8, v5, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7a
    .catchall {:try_start_7a .. :try_end_7a} :catchall_33

    const/16 v5, 0xbd

    .line 171
    :try_start_7b
    aget-byte v8, v2, v5

    int-to-byte v5, v8

    int-to-byte v8, v8

    invoke-static {v5, v10, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5
    :try_end_7b
    .catchall {:try_start_7b .. :try_end_7b} :catchall_32

    const/16 v14, 0xec

    :try_start_7c
    aget-byte v8, v2, v14

    int-to-byte v8, v8

    const/16 v13, 0x118

    int-to-short v9, v13

    const/16 v10, 0x1c

    aget-byte v2, v2, v10

    int-to-byte v2, v2

    invoke-static {v8, v9, v2}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {v5, v2, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7c
    .catchall {:try_start_7c .. :try_end_7c} :catchall_31

    .line 172
    :try_start_7d
    invoke-static/range {v44 .. v44}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/16 v5, 0x1167

    move-object v2, v3

    move/from16 v3, v46

    move/from16 v14, v47

    move-object/from16 v9, v50

    goto/16 :goto_36

    :catchall_31
    move-exception v0

    goto :goto_55

    :catchall_32
    move-exception v0

    const/16 v14, 0xec

    :goto_55
    move-object v2, v0

    .line 173
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_64

    throw v3

    :cond_64
    throw v2

    :catchall_33
    move-exception v0

    const/16 v14, 0xec

    move-object v2, v0

    .line 174
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_65

    throw v3

    :cond_65
    throw v2

    :catchall_34
    move-exception v0

    const/16 v14, 0xec

    move-object v2, v0

    .line 175
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_66

    throw v3

    :cond_66
    throw v2

    :cond_67
    const/16 v14, 0xec

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Class;

    .line 176
    const-class v2, Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    move-object/from16 v6, v40

    .line 177
    invoke-virtual {v6, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 178
    invoke-virtual {v2, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v5, v4

    if-nez v47, :cond_68

    const/16 v3, 0x10

    goto :goto_56

    :cond_68
    const/16 v3, 0xf

    :goto_56
    const/16 v4, 0xf

    if-eq v3, v4, :cond_69

    const/4 v3, 0x1

    goto :goto_57

    :cond_69
    const/4 v3, 0x0

    .line 179
    :goto_57
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v5, v4

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sput-object v2, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;
    :try_end_7d
    .catchall {:try_start_7d .. :try_end_7d} :catchall_49

    const/16 v2, 0xbd

    const/16 v3, 0x73

    const/4 v4, 0x7

    const/4 v5, 0x0

    const/16 v6, 0x52

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/16 v27, 0x1

    goto/16 :goto_63

    :catchall_35
    move-exception v0

    goto/16 :goto_5b

    :cond_6a
    move-object/from16 v17, v40

    const/16 v13, 0x118

    const/16 v14, 0xec

    const/16 v15, 0xd

    const/16 v18, 0x4

    .line 180
    sget v21, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    and-int/lit8 v31, v21, 0x4f

    or-int/lit8 v21, v21, 0x4f

    add-int v13, v31, v21

    rem-int/lit16 v14, v13, 0x80

    sput v14, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v14, 0x2

    rem-int/2addr v13, v14

    if-nez v13, :cond_6b

    .line 181
    :try_start_7e
    invoke-static {v6, v11}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v13

    invoke-static {v7, v11, v13}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    and-int/lit16 v13, v11, 0xb1

    or-int/lit16 v11, v11, 0xb1

    add-int/2addr v13, v11

    and-int/lit8 v11, v13, -0x37

    or-int/lit8 v13, v13, -0x37

    add-int/2addr v11, v13

    goto :goto_58

    :cond_6b
    invoke-static {v6, v11}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v13

    invoke-static {v7, v11, v13}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_7e} :catch_e
    .catchall {:try_start_7e .. :try_end_7e} :catchall_49

    add-int/lit8 v11, v11, 0x1

    :goto_58
    move-object/from16 v40, v17

    const/4 v14, 0x3

    goto/16 :goto_4d

    :catch_e
    move-exception v0

    goto :goto_59

    :catch_f
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    :goto_59
    move-object v3, v0

    .line 182
    :try_start_7f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v6, 0x6d

    aget-byte v6, v5, v6

    int-to-byte v6, v6

    const/16 v7, 0x224

    int-to-short v7, v7

    const/16 v8, 0x1c

    aget-byte v8, v5, v8

    int-to-byte v8, v8

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x190

    aget-byte v2, v5, v2

    and-int/lit8 v6, v2, -0x1

    or-int/lit8 v2, v2, -0x1

    add-int/2addr v6, v2

    int-to-byte v2, v6

    const/16 v6, 0x28f

    int-to-short v6, v6

    const/16 v7, 0x52

    aget-byte v8, v5, v7

    const/4 v7, 0x1

    sub-int/2addr v8, v7

    int-to-byte v7, v8

    invoke-static {v2, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_7f
    .catchall {:try_start_7f .. :try_end_7f} :catchall_49

    const/4 v4, 0x2

    :try_start_80
    new-array v6, v4, [Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v3, v6, v4

    const/4 v3, 0x0

    aput-object v2, v6, v3

    const/16 v2, 0xbd

    aget-byte v3, v5, v2

    int-to-byte v2, v3

    const/16 v3, 0x99

    int-to-short v3, v3

    const/16 v4, 0x8b

    aget-byte v5, v5, v4

    int-to-byte v4, v5

    invoke-static {v2, v3, v4}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v50, v4, v3

    const-class v3, Ljava/lang/Throwable;

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_80
    .catchall {:try_start_80 .. :try_end_80} :catchall_36

    :catchall_36
    move-exception v0

    move-object v2, v0

    :try_start_81
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_6c

    throw v3

    :cond_6c
    throw v2

    :catchall_37
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 183
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_6d

    throw v3

    :cond_6d
    throw v2

    :catchall_38
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 184
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_6e

    throw v3

    :cond_6e
    throw v2

    :catchall_39
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 185
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_6f

    throw v3

    :cond_6f
    throw v2

    :catchall_3a
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 186
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_70

    throw v3

    :cond_70
    throw v2

    :catchall_3b
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 187
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_71

    throw v3

    :cond_71
    throw v2

    :catchall_3c
    move-exception v0

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_72

    throw v3

    :cond_72
    throw v2

    :catchall_3d
    move-exception v0

    goto/16 :goto_5a

    :catchall_3e
    move-exception v0

    move/from16 v46, v3

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 188
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_73

    throw v3

    :cond_73
    throw v2

    :catchall_3f
    move-exception v0

    move/from16 v46, v3

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 189
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_74

    throw v3

    :cond_74
    throw v2

    :catchall_40
    move-exception v0

    move/from16 v46, v3

    goto :goto_5a

    :catchall_41
    move-exception v0

    move/from16 v46, v3

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_75

    throw v3

    :cond_75
    throw v2

    :catchall_42
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v50, v9

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 190
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_76

    throw v3

    :cond_76
    throw v2

    :catchall_43
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v50, v9

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 191
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_77

    throw v3

    :cond_77
    throw v2

    :catchall_44
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v50, v9

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 192
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_78

    throw v3

    :cond_78
    throw v2

    :catchall_45
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v50, v9

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 193
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_79

    throw v3

    :cond_79
    throw v2

    :catchall_46
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v50, v9

    :goto_5a
    const/16 v15, 0xd

    :goto_5b
    const/16 v18, 0x4

    :goto_5c
    move-object v2, v0

    const/16 v6, 0x52

    goto/16 :goto_60

    :catchall_47
    move-exception v0

    move/from16 v46, v3

    goto :goto_5d

    :catchall_48
    move-exception v0

    move/from16 v46, v3

    move-object/from16 v30, v4

    :goto_5d
    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v50, v9

    move-object/from16 v36, v11

    const/16 v15, 0xd

    const/16 v18, 0x4

    move-object v2, v0

    .line 194
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_7a

    throw v3

    :cond_7a
    throw v2

    :cond_7b
    move/from16 v46, v3

    move-object/from16 v30, v4

    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v50, v9

    move-object/from16 v36, v11

    const/4 v2, 0x0

    const/16 v15, 0xd

    const/16 v18, 0x4

    .line 195
    throw v2
    :try_end_81
    .catchall {:try_start_81 .. :try_end_81} :catchall_49

    :catchall_49
    move-exception v0

    goto :goto_5c

    :cond_7c
    move/from16 v46, v3

    move-object/from16 v30, v4

    :goto_5e
    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v50, v9

    move-object/from16 v36, v11

    const/16 v15, 0xd

    const/16 v18, 0x4

    .line 196
    :try_start_82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v4, 0x6d

    aget-byte v4, v3, v4

    int-to-byte v4, v4

    const/16 v5, 0x22c

    int-to-short v5, v5

    const/16 v6, 0x1c

    aget-byte v6, v3, v6

    int-to-byte v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0x190

    aget-byte v4, v3, v4
    :try_end_82
    .catchall {:try_start_82 .. :try_end_82} :catchall_4c

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x28f

    int-to-short v5, v5

    const/16 v6, 0x52

    :try_start_83
    aget-byte v7, v3, v6

    and-int/lit8 v8, v7, -0x1

    or-int/lit8 v7, v7, -0x1

    add-int/2addr v8, v7

    int-to-byte v7, v8

    invoke-static {v4, v5, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_83
    .catchall {:try_start_83 .. :try_end_83} :catchall_4b

    .line 197
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v4, v4, 0x37

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v5, 0x2

    rem-int/2addr v4, v5

    const/4 v4, 0x1

    :try_start_84
    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v5, v4

    const/16 v2, 0xbd

    .line 198
    aget-byte v4, v3, v2

    int-to-byte v2, v4

    const/16 v4, 0x99

    int-to-short v4, v4

    const/16 v7, 0x8b

    aget-byte v3, v3, v7

    int-to-byte v3, v3

    invoke-static {v2, v4, v3}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v50, v4, v3

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_84
    .catchall {:try_start_84 .. :try_end_84} :catchall_4a

    :catchall_4a
    move-exception v0

    move-object v2, v0

    :try_start_85
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_7d

    throw v3

    :cond_7d
    throw v2
    :try_end_85
    .catchall {:try_start_85 .. :try_end_85} :catchall_4b

    :catchall_4b
    move-exception v0

    goto :goto_5f

    :catchall_4c
    move-exception v0

    const/16 v6, 0x52

    goto :goto_5f

    :catchall_4d
    move-exception v0

    move/from16 v27, v2

    move/from16 v46, v3

    move-object/from16 v30, v4

    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v50, v9

    move-object/from16 v36, v11

    const/16 v6, 0x52

    const/16 v15, 0xd

    const/16 v18, 0x4

    :goto_5f
    move-object v2, v0

    :goto_60
    add-int/lit8 v8, v42, 0x2

    const/4 v3, 0x1

    sub-int/2addr v8, v3

    :goto_61
    const/4 v4, 0x7

    if-ge v8, v4, :cond_80

    .line 199
    sget v5, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    or-int/lit8 v7, v5, 0x73

    shl-int/2addr v7, v3

    const/16 v3, 0x73

    xor-int/2addr v5, v3

    sub-int/2addr v7, v5

    rem-int/lit16 v5, v7, 0x80

    sput v5, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v5, 0x2

    rem-int/2addr v7, v5

    if-nez v7, :cond_7f

    .line 200
    :try_start_86
    aget-boolean v5, v36, v8

    if-eqz v5, :cond_7e

    const/4 v5, 0x1

    goto :goto_62

    :cond_7e
    add-int/lit8 v8, v8, -0x4d

    const/4 v5, 0x1

    sub-int/2addr v8, v5

    xor-int/lit8 v7, v8, 0x4f

    and-int/lit8 v8, v8, 0x4f

    shl-int/2addr v8, v5

    add-int/2addr v8, v7

    const/4 v3, 0x1

    goto :goto_61

    :cond_7f
    aget-boolean v1, v36, v8
    :try_end_86
    .catch Ljava/lang/Exception; {:try_start_86 .. :try_end_86} :catch_10

    const/4 v1, 0x0

    :try_start_87
    throw v1
    :try_end_87
    .catch Ljava/lang/Exception; {:try_start_87 .. :try_end_87} :catch_10
    .catchall {:try_start_87 .. :try_end_87} :catchall_4e

    :catchall_4e
    move-exception v0

    move-object v1, v0

    .line 201
    throw v1

    :cond_80
    const/16 v3, 0x73

    const/4 v5, 0x0

    :goto_62
    if-nez v5, :cond_82

    .line 202
    sget v1, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    add-int/lit8 v1, v1, 0x6e

    const/4 v3, 0x1

    sub-int/2addr v1, v3

    rem-int/lit16 v3, v1, 0x80

    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    const/4 v3, 0x2

    rem-int/2addr v1, v3

    .line 203
    :try_start_88
    sget-object v1, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v3, 0x6d

    aget-byte v3, v1, v3

    int-to-byte v3, v3

    const/16 v4, 0x242

    int-to-short v4, v4

    const/16 v5, 0xbd

    aget-byte v6, v1, v5

    int-to-byte v5, v6

    invoke-static {v3, v4, v5}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v3
    :try_end_88
    .catch Ljava/lang/Exception; {:try_start_88 .. :try_end_88} :catch_10

    .line 204
    sget v4, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    or-int/lit8 v5, v4, 0x45

    const/4 v6, 0x1

    shl-int/2addr v5, v6

    xor-int/lit8 v4, v4, 0x45

    sub-int/2addr v5, v4

    rem-int/lit16 v4, v5, 0x80

    sput v4, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v4, 0x2

    rem-int/2addr v5, v4

    :try_start_89
    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v5, v4

    const/4 v2, 0x0

    aput-object v3, v5, v2

    const/16 v2, 0xbd

    .line 205
    aget-byte v2, v1, v2

    int-to-byte v2, v2

    const/16 v3, 0x99

    int-to-short v3, v3

    const/16 v4, 0x8b

    aget-byte v1, v1, v4

    int-to-byte v1, v1

    invoke-static {v2, v3, v1}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v7, 0x2

    new-array v2, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v50, v2, v8

    const-class v3, Ljava/lang/Throwable;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_89
    .catchall {:try_start_89 .. :try_end_89} :catchall_4f

    :catchall_4f
    move-exception v0

    move-object v1, v0

    :try_start_8a
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_81

    throw v2

    :cond_81
    throw v1

    :cond_82
    const/16 v2, 0xbd

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 206
    sput-object v5, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    .line 207
    sput-object v5, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    goto :goto_63

    :cond_83
    move/from16 v27, v2

    move/from16 v46, v3

    move-object/from16 v30, v4

    move-object/from16 v33, v5

    move-object/from16 v35, v6

    move-object/from16 v38, v7

    move/from16 v42, v8

    move-object/from16 v50, v9

    move-object/from16 v36, v11

    const/16 v2, 0xbd

    const/16 v3, 0x73

    const/4 v4, 0x7

    const/4 v5, 0x0

    const/16 v6, 0x52

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/16 v15, 0xd

    const/16 v18, 0x4

    :goto_63
    add-int/lit8 v9, v42, 0x2

    const/4 v10, 0x1

    sub-int/2addr v9, v10

    move v8, v9

    move/from16 v2, v27

    move-object/from16 v4, v30

    move-object/from16 v5, v33

    move-object/from16 v6, v35

    move-object/from16 v11, v36

    move-object/from16 v7, v38

    move/from16 v3, v46

    move-object/from16 v9, v50

    const/16 v10, 0xbd

    const/4 v15, 0x1

    goto/16 :goto_17

    :cond_84
    :goto_64
    return-void

    :catchall_50
    move-exception v0

    move-object v1, v0

    .line 208
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_85

    throw v2

    :cond_85
    throw v1

    :catchall_51
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_86

    throw v2

    :cond_86
    throw v1

    :catchall_52
    move-exception v0

    move-object v1, v0

    .line 209
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_87

    throw v2

    :cond_87
    throw v1
    :try_end_8a
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_8a} :catch_10

    :catch_10
    move-exception v0

    move-object v1, v0

    .line 210
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AFInAppEventParameterName(I)I
    .locals 8

    .line 1
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    xor-int/lit8 v1, v0, 0x63

    and-int/lit8 v0, v0, 0x63

    const/4 v2, 0x1

    shl-int/2addr v0, v2

    add-int/2addr v1, v0

    rem-int/lit16 v0, v1, 0x80

    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v3, 0x48

    const/16 v4, 0x45

    if-eqz v1, :cond_0

    const/16 v1, 0x48

    goto :goto_0

    :cond_0
    const/16 v1, 0x45

    :goto_0
    if-eq v1, v3, :cond_2

    sget-object v1, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    or-int/lit8 v3, v0, 0x35

    shl-int/2addr v3, v2

    xor-int/lit8 v0, v0, 0x35

    sub-int/2addr v3, v0

    rem-int/lit16 v0, v3, 0x80

    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    rem-int/lit8 v3, v3, 0x2

    :try_start_0
    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 v3, 0x0

    aput-object p0, v0, v3

    sget-object p0, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 v5, 0xec

    aget-byte v5, p0, v5

    int-to-byte v5, v5

    const/16 v6, 0x2c0

    int-to-short v6, v6

    const/16 v7, 0x9

    aget-byte v7, p0, v7

    int-to-byte v7, v7

    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    check-cast v6, Ljava/lang/ClassLoader;

    invoke-static {v5, v2, v6}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5

    aget-byte v4, p0, v4

    int-to-byte v4, v4

    const/16 v6, 0x146

    int-to-short v6, v6

    const/16 v7, 0x299

    aget-byte p0, p0, v7

    int-to-byte p0, p0

    invoke-static {v4, v6, p0}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object p0

    new-array v4, v2, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v3

    invoke-virtual {v5, p0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p0

    invoke-virtual {p0, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    or-int/lit8 v1, v0, 0x49

    shl-int/2addr v1, v2

    xor-int/lit8 v0, v0, 0x49

    sub-int/2addr v1, v0

    rem-int/lit16 v0, v1, 0x80

    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    rem-int/lit8 v1, v1, 0x2

    return p0

    :catchall_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    throw v0

    :cond_1
    throw p0

    :cond_2
    const/4 p0, 0x0

    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p0

    throw p0
.end method

.method public static AFInAppEventParameterName(IIC)Ljava/lang/Object;
    .locals 9

    .line 2
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    add-int/lit8 v0, v0, 0x22

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    const/4 v2, 0x2

    rem-int/2addr v0, v2

    const/16 v3, 0xd

    const/4 v4, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/16 v0, 0xd

    :goto_0
    if-ne v0, v3, :cond_4

    sget-object v0, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    :try_start_0
    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v3, p1

    sget-object p0, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    const/16 p2, 0xec

    aget-byte p2, p0, p2

    int-to-byte p2, p2

    const/16 v5, 0x2c0

    int-to-short v5, v5

    const/16 v6, 0x9

    aget-byte v7, p0, v6

    int-to-byte v7, v7

    invoke-static {p2, v5, v7}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object p2

    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    check-cast v5, Ljava/lang/ClassLoader;

    invoke-static {p2, v1, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object p2

    const/16 v5, 0x45

    aget-byte v5, p0, v5

    int-to-byte v5, v5

    const/16 v7, 0x220

    int-to-short v7, v7

    const/16 v8, 0x65

    aget-byte p0, p0, v8

    int-to-byte p0, p0

    invoke-static {v5, v7, p0}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    move-result-object p0

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, p1

    aput-object v5, v4, v1

    sget-object v5, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-virtual {p2, p0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p0

    invoke-virtual {p0, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    sget p2, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    xor-int/lit8 v0, p2, 0x9

    and-int/2addr p2, v6

    shl-int/2addr p2, v1

    add-int/2addr v0, p2

    rem-int/lit16 p2, v0, 0x80

    sput p2, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    rem-int/2addr v0, v2

    const/16 p2, 0x18

    if-nez v0, :cond_1

    const/16 v0, 0x2d

    goto :goto_1

    :cond_1
    const/16 v0, 0x18

    :goto_1
    if-eq v0, p2, :cond_2

    const/16 p2, 0x2c

    :try_start_1
    div-int/2addr p2, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p0

    :catchall_0
    move-exception p0

    throw p0

    :cond_2
    return-object p0

    :catchall_1
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_3

    throw p1

    :cond_3
    throw p0

    :cond_4
    const/4 p0, 0x0

    :try_start_2
    throw p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception p0

    throw p0
.end method

.method static init$0()V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x21

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/16 v0, 0x3d3

    .line 12
    .line 13
    new-array v1, v0, [B

    .line 14
    .line 15
    const-string v2, "\u0018\u001d\u008f>\u000f\u0001\u00c55\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00c0=\u0008\t\u00f4\u0010\u00ff\u00f6\u000e\u00c6\u0015\u0008\u001f\u00d29\u00ef\u00f4\n\u00dc\u00037\u000f\u0001\u00c55\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00c0=\u0008\t\u00f4\u0010\u00ff\u00f6\u000e\u00c68\u00cc\u00fa\u0018\u00ee\u00d0>\t\u00c2\u00176\u00f4\u0003\u0002\u0010\u00f6\u0002\u00e8(\u0005\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f\u00fa\u0018\u00ee\u00d0A\u00f8\u0010\u00fc\u00ca()\u00fd\u0004\u00f4\u000b\u0015\u0000\u0003\u00f6\u000c\t\u00d02\u0003\u00ff\u0000\u00fd\u0001\u0016\u00f8\t\u0002\u00fa\u0018\u00ee\u00d0C\u00fe\t\u00c2\u0017:\u00fe\u00f4\u00e06\u00f4\u0003\u0002\u0010\u0010\u00f9\u0011\u0000\u00fd\u00fe\u00cdD\u0007\u00be\u00176\u00f7\u0006\u00fb\u00c35\u00f2\u0010\u0004\u00f9\t\u0002\u00fa\u0018\u00ee\u00d0>\t\u00c2\u0017:\u00fe\u00f4\u00df4\u0003\u00f2\u001b\u00d3(\u0005\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f\u0000\u000e\r\u00f6\u0005\u00c6H\t\u00fd\u0004\u00f4\u000b\u00c4\u001e(\u00e2\u001b\u000b\u0005\u0006\n\u00ce$\u0016\u00ce,\u00f8\u0015\u0003\u00dc&\u00f5\u0006\u0004\u0010\u00f6\u00ff\u0006\u00e52\u00fa\u0003\u0010\u0004\u0006\u00fe\u00f7\u000e\u0000\u0014\u00ee\u00eb\u001b\u0012\u00f0\u00f5$\u00f0\u0016\u00f4\u0001\u0012\u00d5&\u0006\u00fc\u0011\u00d4(\u000c\u000f\u0001\u00c46\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00bf>\u0008\t\u00f4\u0010\u00ff\u00f6\u000e\u00c5\u0016\u0008\u001f\u00d29\u00ef\u00f4\n\u00dc\u0003\u00fe\u00fa\u000e\u00f4\u0001\u0012\u00d2!\u0005\u0008\u0000\u00e2(\u000c\u00f6\u00ff\u0006\u0000\u000e\r\u00f6\u0005\u00c6H\t\u00fd\u0004\u00f4\u000b\u00c4\u0019$\u0016\u00d1&\u0006\u00fc\u000f\u00f8\u0004\u00fd\u0007\u0001\u0005\u0008\u0000\u0000\u000e\r\u00f6\u0005\u00c6H\t\u00fd\u0004\u00f4\u000b\u00c4\u0017\"\u0015\u00f5\u00e2$\u0016\u00ce,\u00f8\u0015\u0003\u00dc&\u00f5\u0006\u0004\u0010\u0001\u0012\u00d2/\u00f8\u0004\u00e1!\u0005\u0008\u0000\u00e2(\u000c\t\u00f8\u00f8\u00ee\n\u00ec\u000bI\u0004\u00b4I\u00fe\u000e\u0003\u00f9\u0002\u0005\u000b\u000b\u00b0O\u00fc\u0004\u0011\u00b8\u00ee\t\u00ed\u000b\u00ee\u0007\u00ef\u000b\u00ee\u000b\u00eb\u000b\u0008\u0006(\u00d62\u0003\u00d84\u00f2\u000c\t\u00df\u0014\u0014\u00f2\u000f\u00fb\u0012\u00f4\u0010\u00df\u0016\u000f\u00fb\u00fa\u0018\u00ee\u00d0A\u00f8\u0010\u00fc\u00ca\u0018,\u00f8\u0015\u0003\u00dc&\u00f5\u0006\u0004\u0010\u0010\u00f9\u0011\u0000\u00fd\u00fe\u00cd6\u0012\u0003\u00c1\u00162\u0003\u00da(\u0006\u00f6\u0002\u000e\n\u00f5\u0012\u00e1\u0016\u00ff\u0006\u00ee\"\u0001\u0010\u00fa\u0018\u00ee\u00d0>\t\u00c2\u0019 \u0016\u00f0\u00eb(\u0005\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f\u0006\u00f5\u0006\u00e3$\u0016\u0010\u00f9\u0011\u0000\u00fd\u00fe\u00cdK\u00f6\u00ff\u0015\u00ba \u001d\u0017\u00cf4\u00f2\u000c\t\u00fa\u0018\u00ee\u00d0>\t\u00c2\u0017:\u00fe\u00f4\u00df4\u0003\u00f2\u001b\u00d9)\u0002\u00ff\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f\u00fb\u0001\n\u0001\u0012\u00d2,\u00f8\u0015\u0003\u00dc&\u00f5\u0006\u0004\u0010\u000f\u0001\u00c55\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00c0=\u0008\t\u00f4\u0010\u00ff\u00f6\u000e\u00c6\u0015\u0008\u001f\u00d2:\u00ee\u00f4\n\u00dcM8\u0000\u0016\u00f0\u00d18\u0000\u0016\u00f0\u00d1\u0004\n\u00fc\u0012\u00f4\u0001\u0012\u00d5\u0001\u0008\u0008\u001d\u0017\u00fd\u0004\u00fe\u0006\u00f6\u00f5\u001e\u00f2\u0012\u0003\u00f8\u0010\u00f4\n\u0017\u00ed\u0008\t\u00f6\u0016\u00f8\u0010\u00f2\u00ea \u00fc\u0013\u00f2\u0014\n\u00da\u0014\u0016\u00f7\u00e0*\u00fc\u000b\u00fb\u000c\t\u0002\u000c\u0006\u0007\u00f57\u000f\u0001\u00c55\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00c0=\u0008\t\u00f4\u0010\u00ff\u00f6\u000e\u00c67\u00cd\u00fa\u0018\u00ee\u00d0>\t\u00c2I\u00fc\u0006\u00f7\u0008\u000c\u0001\u0012\u00df%\u0000\u0004\u00f8\u0010\u0005\u0008\u0001\u0012\u00d0$\u0014\u00ff\u0000\u000c\u0002\u00f4\u00ee\u0014\u0016\u00f7\u0010\u00f9\u0011\u0000\u00fd\u00fe\u00cd6\u0012\u0003\u00c1\u0016%\u0014\u00f8\u0010\u00f6\u000e\u0008\u00de\u0017\r\u00f6\u00ff\u0006\u00ee\u000e\u000c\u00f3\u00ed\u001a\u00fa\u0018\u00ee\u00d0>\t\u00c2\u001b&\u0006\u00fc\u00ed)\u0002\u00ff\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f\u0001\u0010\u00ec\u001e\u00fa\u000e\u00f4\u00fa\u0018\u00ee\u00d0>\t\u00c2\u001e\t\u00f96\u00ee\u0005\u000e\u0007\u00f8\t\u0002\u00f4\u0016\u00f7\u00e7 \r\u0004\u0001\u0012\u00d8(\u00fe\u000e\u00f8\u00fb\u000e\u00d82\u0003\u00ff\u0000\u00fd\u0001\u0016\u00f8\t\u0002\u00fa\u0018\u00ee\u00d0>\t\u00c2\u001b&\u0006\u00fc\u00ee\u0006\u00f0\u000b\u0015\u0000\u0003\u00f6\u000c\t\u00e3\u0018\u0007\u00fb\u00eb\u001f\u0006\u0003\u0000\r\u00fa\u0018\u00ee\u00d0>\t\u00c2\u001b&\u0006\u00fc\u00e2$\u0011\u00f3\u0012\u00fa\n\u0007\u00fe\u0006\u00fe\u00d6:\u00fe\u00f4\u00df4\u0003\u00f2\u001b\u0006\u00f5\u0006\u00e2,\u00f8\u0015\u0003\u000f\u0001\u00c46\u0012\u0003\u0006\u00f6\t\u0010\u00ef\u0010\u00fe\u00f2\u0012\u00f6\u0016\u00f8\u0010\u00f2\u00ea \u00fc\u0013\u00f2\u0014\n\u00ce(\u000c\u00f6\u0001\u0014\u00fe\u0006\u00fa\u00ff\u0011\u00fa\u0018\u00ee\u00d0>\t\u00c2\u001e(\u0005\u0008\u0002\u00e2$\u0001\u00f6\u00ff\u000f"

    .line 16
    .line 17
    const-string v3, "ISO-8859-1"

    .line 18
    .line 19
    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const/4 v3, 0x0

    .line 24
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 25
    .line 26
    .line 27
    sput-object v1, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    .line 28
    .line 29
    const/16 v0, 0x61

    .line 30
    .line 31
    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$$b:I

    .line 32
    .line 33
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 34
    .line 35
    xor-int/lit8 v1, v0, 0x29

    .line 36
    .line 37
    and-int/lit8 v0, v0, 0x29

    .line 38
    .line 39
    shl-int/lit8 v0, v0, 0x1

    .line 40
    .line 41
    add-int/2addr v1, v0

    .line 42
    rem-int/lit16 v0, v1, 0x80

    .line 43
    .line 44
    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 45
    .line 46
    rem-int/lit8 v1, v1, 0x2

    .line 47
    .line 48
    const/16 v0, 0x33

    .line 49
    .line 50
    if-eqz v1, :cond_0

    .line 51
    .line 52
    const/16 v1, 0x33

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    const/16 v1, 0x2b

    .line 56
    .line 57
    :goto_0
    if-eq v1, v0, :cond_1

    .line 58
    .line 59
    return-void

    .line 60
    :cond_1
    const/4 v0, 0x0

    .line 61
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    .line 63
    throw v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public static valueOf(Ljava/lang/Object;)I
    .locals 8

    .line 1
    sget v0, Lcom/appsflyer/internal/AFb1hSDK;->$11:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x10

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    sub-int/2addr v1, v2

    .line 7
    rem-int/lit16 v3, v1, 0x80

    .line 8
    .line 9
    sput v3, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 10
    .line 11
    rem-int/lit8 v1, v1, 0x2

    .line 12
    .line 13
    const/16 v3, 0x49

    .line 14
    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    const/16 v1, 0x59

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/16 v1, 0x49

    .line 21
    .line 22
    :goto_0
    const/4 v4, 0x0

    .line 23
    if-eq v1, v3, :cond_1

    .line 24
    .line 25
    sget-object v1, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    .line 26
    .line 27
    const/16 v3, 0x3e

    .line 28
    .line 29
    :try_start_0
    div-int/2addr v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    goto :goto_1

    .line 31
    :catchall_0
    move-exception p0

    .line 32
    throw p0

    .line 33
    :cond_1
    sget-object v1, Lcom/appsflyer/internal/AFb1hSDK;->onResponseNative:Ljava/lang/Object;

    .line 34
    .line 35
    :goto_1
    or-int/lit8 v3, v0, 0x33

    .line 36
    .line 37
    shl-int/2addr v3, v2

    .line 38
    xor-int/lit8 v0, v0, 0x33

    .line 39
    .line 40
    sub-int/2addr v3, v0

    .line 41
    rem-int/lit16 v0, v3, 0x80

    .line 42
    .line 43
    sput v0, Lcom/appsflyer/internal/AFb1hSDK;->$10:I

    .line 44
    .line 45
    rem-int/lit8 v3, v3, 0x2

    .line 46
    .line 47
    :try_start_1
    new-array v0, v2, [Ljava/lang/Object;

    .line 48
    .line 49
    aput-object p0, v0, v4

    .line 50
    .line 51
    sget-object p0, Lcom/appsflyer/internal/AFb1hSDK;->$$a:[B

    .line 52
    .line 53
    const/16 v3, 0xec

    .line 54
    .line 55
    aget-byte v3, p0, v3

    .line 56
    .line 57
    int-to-byte v3, v3

    .line 58
    const/16 v5, 0x2c0

    .line 59
    .line 60
    int-to-short v5, v5

    .line 61
    const/16 v6, 0x9

    .line 62
    .line 63
    aget-byte v6, p0, v6

    .line 64
    .line 65
    int-to-byte v6, v6

    .line 66
    invoke-static {v3, v5, v6}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    sget-object v5, Lcom/appsflyer/internal/AFb1hSDK;->onDeepLinkingNative:Ljava/lang/Object;

    .line 71
    .line 72
    check-cast v5, Ljava/lang/ClassLoader;

    .line 73
    .line 74
    invoke-static {v3, v2, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    const/4 v5, 0x5

    .line 79
    aget-byte v5, p0, v5

    .line 80
    .line 81
    int-to-byte v5, v5

    .line 82
    xor-int/lit16 v6, v5, 0xbc

    .line 83
    .line 84
    and-int/lit16 v7, v5, 0xbc

    .line 85
    .line 86
    or-int/2addr v6, v7

    .line 87
    int-to-short v6, v6

    .line 88
    aget-byte p0, p0, v2

    .line 89
    .line 90
    int-to-byte p0, p0

    .line 91
    invoke-static {v5, v6, p0}, Lcom/appsflyer/internal/AFb1hSDK;->$$c(SSB)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p0

    .line 95
    new-array v2, v2, [Ljava/lang/Class;

    .line 96
    .line 97
    const-class v5, Ljava/lang/Object;

    .line 98
    .line 99
    aput-object v5, v2, v4

    .line 100
    .line 101
    invoke-virtual {v3, p0, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 102
    .line 103
    .line 104
    move-result-object p0

    .line 105
    invoke-virtual {p0, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object p0

    .line 109
    check-cast p0, Ljava/lang/Integer;

    .line 110
    .line 111
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 112
    .line 113
    .line 114
    move-result p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 115
    return p0

    .line 116
    :catchall_1
    move-exception p0

    .line 117
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    if-eqz v0, :cond_2

    .line 122
    .line 123
    throw v0

    .line 124
    :cond_2
    throw p0
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

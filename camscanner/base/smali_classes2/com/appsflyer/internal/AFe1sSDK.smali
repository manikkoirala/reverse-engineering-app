.class public final Lcom/appsflyer/internal/AFe1sSDK;
.super Lcom/appsflyer/internal/AFe1qSDK;
.source ""


# static fields
.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field private static init:[C = null

.field private static onAppOpenAttributionNative:I = 0x1

.field private static onInstallConversionDataLoadedNative:I

.field private static onInstallConversionFailureNative:J


# instance fields
.field private final AFLogger$LogLevel:Lcom/appsflyer/internal/AFe1aSDK;

.field private final afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

.field private final afWarnLog:Ljava/lang/String;

.field private final getLevel:Lcom/appsflyer/internal/AFc1tSDK;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [C

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/appsflyer/internal/AFe1sSDK;->init:[C

    .line 8
    .line 9
    const-wide v0, -0x6bce7080347784eeL    # -2.086512601148005E-211

    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    sput-wide v0, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionFailureNative:J

    .line 15
    .line 16
    return-void

    .line 17
    :array_0
    .array-data 2
        0x619es
        0x7b60s
        0x5441s
        0x3158s
        0xa20s
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFc1qSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFe1eSDK;

    .line 2
    .line 3
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/appsflyer/internal/AFe1eSDK;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, v0, p2, p1}, Lcom/appsflyer/internal/AFe1qSDK;-><init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/appsflyer/internal/AFe1sSDK;->getLevel:Lcom/appsflyer/internal/AFc1tSDK;

    .line 20
    .line 21
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/appsflyer/internal/AFe1sSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

    .line 26
    .line 27
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1sSDK;->afWarnLog:Ljava/lang/String;

    .line 28
    .line 29
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afDebugLog()Lcom/appsflyer/internal/AFe1aSDK;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1sSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFe1aSDK;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static a(CII[Ljava/lang/Object;)V
    .locals 12

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFh1zSDK;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/appsflyer/internal/AFh1zSDK;-><init>()V

    .line 4
    .line 5
    .line 6
    new-array v1, p1, [J

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    iput v2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 10
    .line 11
    :goto_0
    iget v3, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 12
    .line 13
    const/4 v4, 0x1

    .line 14
    if-ge v3, p1, :cond_0

    .line 15
    .line 16
    const/16 v5, 0x52

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    const/4 v5, 0x1

    .line 20
    :goto_1
    if-eq v5, v4, :cond_1

    .line 21
    .line 22
    sget-object v4, Lcom/appsflyer/internal/AFe1sSDK;->init:[C

    .line 23
    .line 24
    add-int v5, p2, v3

    .line 25
    .line 26
    aget-char v4, v4, v5

    .line 27
    .line 28
    int-to-long v4, v4

    .line 29
    const-wide v6, -0x5c1be10a21129e04L    # -8.650878505717092E-136

    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    xor-long/2addr v4, v6

    .line 35
    long-to-int v5, v4

    .line 36
    int-to-char v4, v5

    .line 37
    int-to-long v4, v4

    .line 38
    int-to-long v8, v3

    .line 39
    sget-wide v10, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionFailureNative:J

    .line 40
    .line 41
    xor-long/2addr v6, v10

    .line 42
    mul-long v8, v8, v6

    .line 43
    .line 44
    xor-long/2addr v4, v8

    .line 45
    int-to-long v6, p0

    .line 46
    xor-long/2addr v4, v6

    .line 47
    aput-wide v4, v1, v3

    .line 48
    .line 49
    add-int/lit8 v3, v3, 0x1

    .line 50
    .line 51
    iput v3, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 52
    .line 53
    sget v3, Lcom/appsflyer/internal/AFe1sSDK;->$10:I

    .line 54
    .line 55
    add-int/lit8 v3, v3, 0x19

    .line 56
    .line 57
    rem-int/lit16 v4, v3, 0x80

    .line 58
    .line 59
    sput v4, Lcom/appsflyer/internal/AFe1sSDK;->$11:I

    .line 60
    .line 61
    rem-int/lit8 v3, v3, 0x2

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    new-array p0, p1, [C

    .line 65
    .line 66
    iput v2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 67
    .line 68
    sget p2, Lcom/appsflyer/internal/AFe1sSDK;->$11:I

    .line 69
    .line 70
    add-int/lit8 p2, p2, 0x57

    .line 71
    .line 72
    rem-int/lit16 v3, p2, 0x80

    .line 73
    .line 74
    sput v3, Lcom/appsflyer/internal/AFe1sSDK;->$10:I

    .line 75
    .line 76
    rem-int/lit8 p2, p2, 0x2

    .line 77
    .line 78
    :goto_2
    iget p2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 79
    .line 80
    if-ge p2, p1, :cond_2

    .line 81
    .line 82
    aget-wide v3, v1, p2

    .line 83
    .line 84
    long-to-int v4, v3

    .line 85
    int-to-char v3, v4

    .line 86
    aput-char v3, p0, p2

    .line 87
    .line 88
    add-int/lit8 p2, p2, 0x1

    .line 89
    .line 90
    iput p2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_2
    new-instance p1, Ljava/lang/String;

    .line 94
    .line 95
    invoke-direct {p1, p0}, Ljava/lang/String;-><init>([C)V

    .line 96
    .line 97
    .line 98
    aput-object p1, p3, v2

    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method private getLevel()V
    .locals 5

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x57

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    :goto_0
    const-string v3, "[register] Successfully registered for Uninstall Tracking"

    .line 19
    .line 20
    const-string v4, "sentRegisterRequestToAF"

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 25
    .line 26
    invoke-interface {v0, v4, v2}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;Z)V

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 31
    .line 32
    invoke-interface {v0, v4, v1}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;Z)V

    .line 33
    .line 34
    .line 35
    :goto_1
    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 39
    .line 40
    add-int/lit8 v0, v0, 0x1b

    .line 41
    .line 42
    rem-int/lit16 v3, v0, 0x80

    .line 43
    .line 44
    sput v3, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 45
    .line 46
    rem-int/lit8 v0, v0, 0x2

    .line 47
    .line 48
    if-nez v0, :cond_2

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    const/4 v1, 0x1

    .line 52
    :goto_2
    if-eqz v1, :cond_3

    .line 53
    .line 54
    return-void

    .line 55
    :cond_3
    const/4 v0, 0x0

    .line 56
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :catchall_0
    move-exception v0

    .line 58
    throw v0
    .line 59
    .line 60
.end method


# virtual methods
.method protected final afDebugLog()Z
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x6f

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    add-int/lit8 v1, v1, 0x53

    .line 12
    .line 13
    rem-int/lit16 v0, v1, 0x80

    .line 14
    .line 15
    sput v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 16
    .line 17
    rem-int/lit8 v1, v1, 0x2

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    const/4 v2, 0x0

    .line 21
    if-nez v1, :cond_0

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v1, 0x0

    .line 26
    :goto_0
    if-eq v1, v0, :cond_1

    .line 27
    .line 28
    return v2

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    :catchall_0
    move-exception v0

    .line 32
    throw v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final valueOf()V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x2b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    invoke-super {p0}, Lcom/appsflyer/internal/AFd1aSDK;->valueOf()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1aSDK;->afInfoLog:Lcom/appsflyer/internal/AFd1pSDK;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    const/4 v2, 0x1

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v3, 0x1

    .line 23
    :goto_0
    if-eq v3, v2, :cond_2

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFd1pSDK;->isSuccessful()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    :cond_1
    if-eqz v1, :cond_2

    .line 33
    .line 34
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 35
    .line 36
    add-int/lit8 v0, v0, 0x59

    .line 37
    .line 38
    rem-int/lit16 v1, v0, 0x80

    .line 39
    .line 40
    sput v1, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 41
    .line 42
    rem-int/lit8 v0, v0, 0x2

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1sSDK;->getLevel()V

    .line 45
    .line 46
    .line 47
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 48
    .line 49
    add-int/lit8 v0, v0, 0x75

    .line 50
    .line 51
    rem-int/lit16 v1, v0, 0x80

    .line 52
    .line 53
    sput v1, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 54
    .line 55
    rem-int/lit8 v0, v0, 0x2

    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
.end method

.method protected final values(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 9

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    invoke-super {p0, p1}, Lcom/appsflyer/internal/AFe1qSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1sSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    if-eqz v1, :cond_7

    .line 15
    .line 16
    invoke-virtual {v2}, Lcom/appsflyer/internal/AFa1cSDK;->values()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    const/4 v3, 0x1

    .line 21
    if-nez v2, :cond_6

    .line 22
    .line 23
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    const/4 v5, 0x0

    .line 32
    :try_start_0
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    const-string v6, "app_version_code"

    .line 37
    .line 38
    iget v7, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 39
    .line 40
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v7

    .line 44
    invoke-virtual {p1, v6, v7}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 45
    .line 46
    .line 47
    const-string v6, "app_version_name"

    .line 48
    .line 49
    iget-object v7, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 50
    .line 51
    invoke-virtual {p1, v6, v7}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 52
    .line 53
    .line 54
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 55
    .line 56
    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    const-string v6, "app_name"

    .line 65
    .line 66
    invoke-virtual {p1, v6, v2}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 67
    .line 68
    .line 69
    iget-wide v6, v4, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    .line 70
    .line 71
    const-string v2, "yyyy-MM-dd_HHmmssZ"

    .line 72
    .line 73
    new-instance v4, Ljava/text/SimpleDateFormat;

    .line 74
    .line 75
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 76
    .line 77
    invoke-direct {v4, v2, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 78
    .line 79
    .line 80
    const-string v2, "installDate"

    .line 81
    .line 82
    invoke-static {v4, v6, v7}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName(Ljava/text/SimpleDateFormat;J)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    invoke-virtual {p1, v2, v4}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .line 88
    .line 89
    goto :goto_0

    .line 90
    :catchall_0
    move-exception v2

    .line 91
    const-string v4, "Exception while collecting application version info."

    .line 92
    .line 93
    invoke-static {v4, v2}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    .line 95
    .line 96
    :goto_0
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1sSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFe1aSDK;

    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 99
    .line 100
    .line 101
    move-result-object v4

    .line 102
    invoke-interface {v2, v4}, Lcom/appsflyer/internal/AFe1aSDK;->AFInAppEventType(Ljava/util/Map;)V

    .line 103
    .line 104
    .line 105
    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventParameterName()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    if-eqz v2, :cond_0

    .line 110
    .line 111
    const-string v4, "appUserId"

    .line 112
    .line 113
    invoke-virtual {p1, v4, v2}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 114
    .line 115
    .line 116
    sget v2, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 117
    .line 118
    add-int/lit8 v2, v2, 0x27

    .line 119
    .line 120
    rem-int/lit16 v4, v2, 0x80

    .line 121
    .line 122
    sput v4, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 123
    .line 124
    rem-int/lit8 v2, v2, 0x2

    .line 125
    .line 126
    :cond_0
    :try_start_1
    const-string v2, "model"

    .line 127
    .line 128
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 129
    .line 130
    invoke-virtual {p1, v2, v4}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 131
    .line 132
    .line 133
    invoke-static {}, Landroid/os/Process;->myPid()I

    .line 134
    .line 135
    .line 136
    move-result v2

    .line 137
    shr-int/lit8 v2, v2, 0x16

    .line 138
    .line 139
    int-to-char v2, v2

    .line 140
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    .line 141
    .line 142
    .line 143
    move-result v4

    .line 144
    shr-int/lit8 v4, v4, 0x10

    .line 145
    .line 146
    add-int/lit8 v4, v4, 0x5

    .line 147
    .line 148
    invoke-static {v0, v0, v5, v5}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;II)I

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    new-array v3, v3, [Ljava/lang/Object;

    .line 153
    .line 154
    invoke-static {v2, v4, v0, v3}, Lcom/appsflyer/internal/AFe1sSDK;->a(CII[Ljava/lang/Object;)V

    .line 155
    .line 156
    .line 157
    aget-object v0, v3, v5

    .line 158
    .line 159
    check-cast v0, Ljava/lang/String;

    .line 160
    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 166
    .line 167
    invoke-virtual {p1, v0, v2}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 168
    .line 169
    .line 170
    goto :goto_1

    .line 171
    :catchall_1
    move-exception v0

    .line 172
    const-string v2, "Exception while collecting device brand and model."

    .line 173
    .line 174
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 175
    .line 176
    .line 177
    :goto_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    const-string v2, "deviceTrackingDisabled"

    .line 182
    .line 183
    invoke-virtual {v0, v2, v5}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    if-eqz v0, :cond_1

    .line 188
    .line 189
    sget v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 190
    .line 191
    add-int/lit8 v0, v0, 0x25

    .line 192
    .line 193
    rem-int/lit16 v3, v0, 0x80

    .line 194
    .line 195
    sput v3, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 196
    .line 197
    rem-int/lit8 v0, v0, 0x2

    .line 198
    .line 199
    const-string v0, "true"

    .line 200
    .line 201
    invoke-virtual {p1, v2, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 202
    .line 203
    .line 204
    :cond_1
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 205
    .line 206
    .line 207
    move-result-object v0

    .line 208
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1dSDK;->AFKeystoreWrapper(Landroid/content/ContentResolver;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    if-eqz v0, :cond_2

    .line 213
    .line 214
    const-string v1, "amazon_aid"

    .line 215
    .line 216
    iget-object v2, v0, Lcom/appsflyer/internal/AFa1bSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 217
    .line 218
    invoke-virtual {p1, v1, v2}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 219
    .line 220
    .line 221
    iget-object v0, v0, Lcom/appsflyer/internal/AFa1bSDK;->valueOf:Ljava/lang/Boolean;

    .line 222
    .line 223
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v0

    .line 227
    const-string v1, "amazon_aid_limit"

    .line 228
    .line 229
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 230
    .line 231
    .line 232
    :cond_2
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 233
    .line 234
    .line 235
    move-result-object v0

    .line 236
    const-string v1, "advertiserId"

    .line 237
    .line 238
    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    if-eqz v0, :cond_3

    .line 243
    .line 244
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 245
    .line 246
    .line 247
    :cond_3
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1aSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 248
    .line 249
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 250
    .line 251
    const-string v1, "devkey"

    .line 252
    .line 253
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 254
    .line 255
    .line 256
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1sSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFc1rSDK;

    .line 257
    .line 258
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 259
    .line 260
    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    const-string v1, "uid"

    .line 265
    .line 266
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 267
    .line 268
    .line 269
    const-string v0, "af_gcm_token"

    .line 270
    .line 271
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1sSDK;->afWarnLog:Ljava/lang/String;

    .line 272
    .line 273
    invoke-virtual {p1, v0, v1}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 274
    .line 275
    .line 276
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1qSDK;->AFVersionDeclaration:Lcom/appsflyer/internal/AFc1uSDK;

    .line 277
    .line 278
    const-string v1, "appsFlyerCount"

    .line 279
    .line 280
    invoke-interface {v0, v1, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    .line 281
    .line 282
    .line 283
    move-result v0

    .line 284
    const-string v1, "launch_counter"

    .line 285
    .line 286
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 291
    .line 292
    .line 293
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 294
    .line 295
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 296
    .line 297
    .line 298
    move-result-object v0

    .line 299
    const-string v1, "sdk"

    .line 300
    .line 301
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 302
    .line 303
    .line 304
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1sSDK;->getLevel:Lcom/appsflyer/internal/AFc1tSDK;

    .line 305
    .line 306
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFc1tSDK;->valueOf()Ljava/lang/String;

    .line 307
    .line 308
    .line 309
    move-result-object v0

    .line 310
    const/16 v1, 0x2e

    .line 311
    .line 312
    if-eqz v0, :cond_4

    .line 313
    .line 314
    const/16 v2, 0x2e

    .line 315
    .line 316
    goto :goto_2

    .line 317
    :cond_4
    const/16 v2, 0x5a

    .line 318
    .line 319
    :goto_2
    if-eq v2, v1, :cond_5

    .line 320
    .line 321
    goto :goto_3

    .line 322
    :cond_5
    const-string v1, "channel"

    .line 323
    .line 324
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 325
    .line 326
    .line 327
    :goto_3
    sget p1, Lcom/appsflyer/internal/AFe1sSDK;->onInstallConversionDataLoadedNative:I

    .line 328
    .line 329
    add-int/lit8 p1, p1, 0x7d

    .line 330
    .line 331
    rem-int/lit16 v0, p1, 0x80

    .line 332
    .line 333
    sput v0, Lcom/appsflyer/internal/AFe1sSDK;->onAppOpenAttributionNative:I

    .line 334
    .line 335
    rem-int/lit8 p1, p1, 0x2

    .line 336
    .line 337
    return-void

    .line 338
    :cond_6
    const-string p1, "CustomerUserId not set, Tracking is disabled"

    .line 339
    .line 340
    invoke-static {p1, v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;Z)V

    .line 341
    .line 342
    .line 343
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 344
    .line 345
    const-string v0, "CustomerUserId not set, register is not sent"

    .line 346
    .line 347
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 348
    .line 349
    .line 350
    throw p1

    .line 351
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 352
    .line 353
    const-string v0, "Context is not provided, can\'t send register request"

    .line 354
    .line 355
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 356
    .line 357
    .line 358
    throw p1
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

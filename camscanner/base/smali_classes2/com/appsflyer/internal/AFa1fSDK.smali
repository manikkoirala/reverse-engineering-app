.class public final Lcom/appsflyer/internal/AFa1fSDK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

.field private final values:Lcom/appsflyer/internal/AFb1sSDK;


# direct methods
.method public constructor <init>(Lcom/appsflyer/internal/AFf1pSDK;Lcom/appsflyer/internal/AFb1sSDK;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/appsflyer/internal/AFa1fSDK;->values:Lcom/appsflyer/internal/AFb1sSDK;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final valueOf(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 19

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    iget-object v0, v1, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 4
    .line 5
    iget-object v2, v0, Lcom/appsflyer/internal/AFa1uSDK;->afErrorLog:Ljava/lang/String;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1tSDK;->AFInAppEventType(Ljava/util/Map;)Lorg/json/JSONObject;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v3, v1, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/appsflyer/internal/AFf1pSDK;->afErrorLogForExcManagerOnly()Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    iget-object v4, v1, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 26
    .line 27
    invoke-virtual {v4}, Lcom/appsflyer/internal/AFf1pSDK;->AFVersionDeclaration()Z

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    iget-object v5, v1, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 32
    .line 33
    invoke-virtual {v5}, Lcom/appsflyer/internal/AFf1pSDK;->AFLogger$LogLevel()Z

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    iget-object v6, v1, Lcom/appsflyer/internal/AFa1fSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFf1pSDK;

    .line 38
    .line 39
    invoke-virtual {v6}, Lcom/appsflyer/internal/AFa1uSDK;->values()Z

    .line 40
    .line 41
    .line 42
    move-result v6

    .line 43
    const-string v7, ""

    .line 44
    .line 45
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    .line 46
    .line 47
    .line 48
    move-result-object v8

    .line 49
    invoke-virtual {v0, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    .line 50
    .line 51
    .line 52
    move-result-object v8

    .line 53
    const/4 v9, 0x0

    .line 54
    if-eqz v3, :cond_0

    .line 55
    .line 56
    return-object v9

    .line 57
    :cond_0
    const/4 v3, 0x1

    .line 58
    :try_start_0
    new-instance v10, Ljava/net/URL;

    .line 59
    .line 60
    invoke-direct {v10, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    if-eqz v5, :cond_2

    .line 64
    .line 65
    iget-object v11, v1, Lcom/appsflyer/internal/AFa1fSDK;->values:Lcom/appsflyer/internal/AFb1sSDK;

    .line 66
    .line 67
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v12

    .line 71
    invoke-interface {v11, v12, v0}, Lcom/appsflyer/internal/AFb1sSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    .line 75
    .line 76
    .line 77
    move-result-object v11

    .line 78
    invoke-virtual {v0, v11}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    .line 79
    .line 80
    .line 81
    move-result-object v11

    .line 82
    array-length v11, v11

    .line 83
    new-instance v12, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    const-string v13, "call = "

    .line 86
    .line 87
    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v13, "; size = "

    .line 94
    .line 95
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v13, " byte"

    .line 102
    .line 103
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    if-le v11, v3, :cond_1

    .line 107
    .line 108
    const-string v11, "s"

    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_1
    move-object v11, v7

    .line 112
    :goto_0
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    const-string v11, "; body = "

    .line 116
    .line 117
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/appsflyer/internal/AFb1rSDK;->valueOf(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    :cond_2
    const-string v0, "AppsFlyer"

    .line 131
    .line 132
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    move-object v11, v0

    .line 144
    check-cast v11, Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    .line 145
    .line 146
    const/16 v0, 0x7530

    .line 147
    .line 148
    :try_start_1
    invoke-virtual {v11, v0}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v11, v0}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 152
    .line 153
    .line 154
    const-string v0, "POST"

    .line 155
    .line 156
    invoke-virtual {v11, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v11, v3}, Ljava/net/URLConnection;->setDoInput(Z)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v11, v3}, Ljava/net/URLConnection;->setDoOutput(Z)V

    .line 163
    .line 164
    .line 165
    const-string v0, "Content-Type"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 166
    .line 167
    if-eqz v6, :cond_3

    .line 168
    .line 169
    :try_start_2
    const-string v12, "application/octet-stream"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170
    .line 171
    goto :goto_1

    .line 172
    :catchall_0
    move-exception v0

    .line 173
    move-object v9, v11

    .line 174
    goto/16 :goto_8

    .line 175
    .line 176
    :cond_3
    :try_start_3
    const-string v12, "application/json"

    .line 177
    .line 178
    :goto_1
    invoke-virtual {v11, v0, v12}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v11}, Ljava/net/URLConnection;->getOutputStream()Ljava/io/OutputStream;

    .line 182
    .line 183
    .line 184
    move-result-object v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 185
    const/4 v13, 0x0

    .line 186
    if-eqz v6, :cond_8

    .line 187
    .line 188
    :try_start_4
    new-array v0, v3, [Ljava/lang/Object;

    .line 189
    .line 190
    aput-object p1, v0, v13

    .line 191
    .line 192
    sget-object v6, Lcom/appsflyer/internal/AFa1ySDK;->init:Ljava/util/Map;

    .line 193
    .line 194
    const v14, 0x3fc5770f

    .line 195
    .line 196
    .line 197
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 198
    .line 199
    .line 200
    move-result-object v15

    .line 201
    invoke-interface {v6, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    move-result-object v15

    .line 205
    const v16, 0xdeeb

    .line 206
    .line 207
    .line 208
    if-eqz v15, :cond_4

    .line 209
    .line 210
    goto :goto_2

    .line 211
    :cond_4
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollDefaultDelay()I

    .line 212
    .line 213
    .line 214
    move-result v15

    .line 215
    shr-int/lit8 v15, v15, 0x10

    .line 216
    .line 217
    add-int/lit8 v15, v15, 0x25

    .line 218
    .line 219
    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatTimeout()I

    .line 220
    .line 221
    .line 222
    move-result v17

    .line 223
    shr-int/lit8 v17, v17, 0x10

    .line 224
    .line 225
    add-int v9, v17, v16

    .line 226
    .line 227
    int-to-char v9, v9

    .line 228
    invoke-static {}, Landroid/view/ViewConfiguration;->getEdgeSlop()I

    .line 229
    .line 230
    .line 231
    move-result v17

    .line 232
    shr-int/lit8 v17, v17, 0x10

    .line 233
    .line 234
    rsub-int/lit8 v14, v17, 0x25

    .line 235
    .line 236
    invoke-static {v15, v9, v14}, Lcom/appsflyer/internal/AFa1ySDK;->AFKeystoreWrapper(ICI)Ljava/lang/Object;

    .line 237
    .line 238
    .line 239
    move-result-object v9

    .line 240
    check-cast v9, Ljava/lang/Class;

    .line 241
    .line 242
    const-string v14, "AFInAppEventParameterName"

    .line 243
    .line 244
    new-array v15, v3, [Ljava/lang/Class;

    .line 245
    .line 246
    const-class v17, Ljava/lang/String;

    .line 247
    .line 248
    aput-object v17, v15, v13

    .line 249
    .line 250
    invoke-virtual {v9, v14, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 251
    .line 252
    .line 253
    move-result-object v15

    .line 254
    const v9, 0x3fc5770f

    .line 255
    .line 256
    .line 257
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 258
    .line 259
    .line 260
    move-result-object v9

    .line 261
    invoke-interface {v6, v9, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    .line 263
    .line 264
    :goto_2
    check-cast v15, Ljava/lang/reflect/Method;

    .line 265
    .line 266
    const/4 v9, 0x0

    .line 267
    invoke-virtual {v15, v9, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    .line 269
    .line 270
    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 271
    :try_start_5
    new-array v9, v3, [Ljava/lang/Object;

    .line 272
    .line 273
    aput-object v8, v9, v13

    .line 274
    .line 275
    const v8, -0x37d43649

    .line 276
    .line 277
    .line 278
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 279
    .line 280
    .line 281
    move-result-object v14

    .line 282
    invoke-interface {v6, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    .line 284
    .line 285
    move-result-object v14

    .line 286
    if-eqz v14, :cond_5

    .line 287
    .line 288
    move-object v3, v14

    .line 289
    const/4 v14, 0x1

    .line 290
    goto :goto_3

    .line 291
    :cond_5
    const-wide/16 v14, 0x0

    .line 292
    .line 293
    invoke-static {v14, v15}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    .line 294
    .line 295
    .line 296
    move-result v17

    .line 297
    rsub-int/lit8 v8, v17, 0x25

    .line 298
    .line 299
    invoke-static {}, Landroid/view/KeyEvent;->getMaxKeyCode()I

    .line 300
    .line 301
    .line 302
    move-result v17

    .line 303
    shr-int/lit8 v17, v17, 0x10

    .line 304
    .line 305
    sub-int v3, v16, v17

    .line 306
    .line 307
    int-to-char v3, v3

    .line 308
    invoke-static {v13}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    .line 309
    .line 310
    .line 311
    move-result-wide v16

    .line 312
    cmp-long v18, v16, v14

    .line 313
    .line 314
    add-int/lit8 v14, v18, 0x25

    .line 315
    .line 316
    invoke-static {v8, v3, v14}, Lcom/appsflyer/internal/AFa1ySDK;->AFKeystoreWrapper(ICI)Ljava/lang/Object;

    .line 317
    .line 318
    .line 319
    move-result-object v3

    .line 320
    check-cast v3, Ljava/lang/Class;

    .line 321
    .line 322
    const-string v8, "values"
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 323
    .line 324
    const/4 v14, 0x1

    .line 325
    :try_start_6
    new-array v15, v14, [Ljava/lang/Class;

    .line 326
    .line 327
    const-class v16, [B

    .line 328
    .line 329
    aput-object v16, v15, v13

    .line 330
    .line 331
    invoke-virtual {v3, v8, v15}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 332
    .line 333
    .line 334
    move-result-object v3

    .line 335
    const v8, -0x37d43649

    .line 336
    .line 337
    .line 338
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 339
    .line 340
    .line 341
    move-result-object v8

    .line 342
    invoke-interface {v6, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    .line 344
    .line 345
    :goto_3
    check-cast v3, Ljava/lang/reflect/Method;

    .line 346
    .line 347
    invoke-virtual {v3, v0, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    .line 349
    .line 350
    move-result-object v0

    .line 351
    move-object v8, v0

    .line 352
    check-cast v8, [B
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 353
    .line 354
    goto :goto_5

    .line 355
    :catchall_1
    move-exception v0

    .line 356
    goto :goto_4

    .line 357
    :catchall_2
    move-exception v0

    .line 358
    const/4 v14, 0x1

    .line 359
    :goto_4
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 360
    .line 361
    .line 362
    move-result-object v3

    .line 363
    if-eqz v3, :cond_6

    .line 364
    .line 365
    throw v3

    .line 366
    :cond_6
    throw v0

    .line 367
    :catchall_3
    move-exception v0

    .line 368
    const/4 v14, 0x1

    .line 369
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 370
    .line 371
    .line 372
    move-result-object v3

    .line 373
    if-eqz v3, :cond_7

    .line 374
    .line 375
    throw v3

    .line 376
    :cond_7
    throw v0

    .line 377
    :cond_8
    const/4 v14, 0x1

    .line 378
    :goto_5
    invoke-virtual {v12, v8}, Ljava/io/OutputStream;->write([B)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 379
    .line 380
    .line 381
    goto :goto_6

    .line 382
    :catchall_4
    move-exception v0

    .line 383
    goto :goto_7

    .line 384
    :catch_0
    move-exception v0

    .line 385
    :try_start_8
    const-string v3, "AFCrypto: reflection init failed"

    .line 386
    .line 387
    invoke-static {v3, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 388
    .line 389
    .line 390
    :goto_6
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V

    .line 391
    .line 392
    .line 393
    invoke-virtual {v11}, Ljava/net/URLConnection;->connect()V

    .line 394
    .line 395
    .line 396
    invoke-virtual {v11}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 397
    .line 398
    .line 399
    move-result v0

    .line 400
    if-eqz v4, :cond_9

    .line 401
    .line 402
    invoke-static {v11}, Lcom/appsflyer/internal/AFa1cSDK;->AFKeystoreWrapper(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object v3

    .line 406
    move-object v7, v3

    .line 407
    :cond_9
    if-eqz v5, :cond_a

    .line 408
    .line 409
    iget-object v3, v1, Lcom/appsflyer/internal/AFa1fSDK;->values:Lcom/appsflyer/internal/AFb1sSDK;

    .line 410
    .line 411
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 412
    .line 413
    .line 414
    move-result-object v4

    .line 415
    invoke-interface {v3, v4, v0, v7}, Lcom/appsflyer/internal/AFb1sSDK;->valueOf(Ljava/lang/String;ILjava/lang/String;)V

    .line 416
    .line 417
    .line 418
    :cond_a
    const/16 v3, 0xc8

    .line 419
    .line 420
    if-ne v0, v3, :cond_b

    .line 421
    .line 422
    const-string v0, "Status 200 ok"

    .line 423
    .line 424
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 425
    .line 426
    .line 427
    const/4 v3, 0x0

    .line 428
    goto :goto_a

    .line 429
    :catchall_5
    move-exception v0

    .line 430
    const/4 v14, 0x1

    .line 431
    :goto_7
    move-object v9, v11

    .line 432
    goto :goto_9

    .line 433
    :catchall_6
    move-exception v0

    .line 434
    :goto_8
    const/4 v14, 0x1

    .line 435
    :goto_9
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 436
    .line 437
    .line 438
    move-result-object v2

    .line 439
    const-string v3, "Error while calling "

    .line 440
    .line 441
    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 442
    .line 443
    .line 444
    move-result-object v2

    .line 445
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446
    .line 447
    .line 448
    move-object v11, v9

    .line 449
    :cond_b
    const/4 v3, 0x1

    .line 450
    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    .line 451
    .line 452
    const-string v2, "Connection "

    .line 453
    .line 454
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    if-eqz v3, :cond_c

    .line 458
    .line 459
    const-string v2, "error"

    .line 460
    .line 461
    goto :goto_b

    .line 462
    :cond_c
    const-string v2, "call succeeded"

    .line 463
    .line 464
    :goto_b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    .line 466
    .line 467
    const-string v2, ": "

    .line 468
    .line 469
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    .line 471
    .line 472
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    .line 474
    .line 475
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 476
    .line 477
    .line 478
    move-result-object v0

    .line 479
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 480
    .line 481
    .line 482
    return-object v11
.end method

.class public final Lcom/appsflyer/internal/AFe1pSDK;
.super Lcom/appsflyer/internal/AFe1qSDK;
.source ""


# instance fields
.field private final AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public afErrorLogForExcManagerOnly:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final afWarnLog:Lcom/appsflyer/internal/AFc1uSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final getLevel:Lcom/appsflyer/internal/AFg1nSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1oSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final onInstallConversionDataLoadedNative:Lcom/appsflyer/AppsFlyerProperties;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;)V
    .locals 0
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFc1qSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFe1qSDK;-><init>(Lcom/appsflyer/internal/AFa1uSDK;Lcom/appsflyer/internal/AFc1qSDK;)V

    .line 2
    .line 3
    .line 4
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1pSDK;->getLevel:Lcom/appsflyer/internal/AFg1nSDK;

    .line 9
    .line 10
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->values()Lcom/appsflyer/internal/AFc1uSDK;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1pSDK;->afWarnLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 15
    .line 16
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->AFLogger()Lcom/appsflyer/internal/AFf1tSDK;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 21
    .line 22
    invoke-interface {p2}, Lcom/appsflyer/internal/AFc1qSDK;->afErrorLog()Lcom/appsflyer/internal/AFe1oSDK;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1pSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1oSDK;

    .line 27
    .line 28
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1pSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/AppsFlyerProperties;

    .line 33
    .line 34
    sget-object p1, Lcom/appsflyer/internal/AFd1eSDK;->afInfoLog:Lcom/appsflyer/internal/AFd1eSDK;

    .line 35
    .line 36
    iget-object p2, p0, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventType:Ljava/util/Set;

    .line 37
    .line 38
    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    sget-object p1, Lcom/appsflyer/internal/AFd1eSDK;->afRDLog:Lcom/appsflyer/internal/AFd1eSDK;

    .line 42
    .line 43
    iget-object p2, p0, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventType:Ljava/util/Set;

    .line 44
    .line 45
    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final valueOf()V
    .locals 8

    .line 1
    invoke-super {p0}, Lcom/appsflyer/internal/AFd1aSDK;->valueOf()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1qSDK;->afRDLog:Lcom/appsflyer/internal/AFa1uSDK;

    .line 7
    .line 8
    iget v1, v1, Lcom/appsflyer/internal/AFa1uSDK;->afRDLog:I

    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    const/4 v4, 0x1

    .line 15
    if-ne v1, v4, :cond_1

    .line 16
    .line 17
    iget-wide v4, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFLogger:J

    .line 18
    .line 19
    const-wide/16 v6, 0x0

    .line 20
    .line 21
    cmp-long v1, v4, v6

    .line 22
    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    iget-object v1, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFKeystoreWrapper:Ljava/util/Map;

    .line 26
    .line 27
    sub-long/2addr v2, v4

    .line 28
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const-string v3, "net"

    .line 33
    .line 34
    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    iget-object v1, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFKeystoreWrapper:Ljava/util/Map;

    .line 38
    .line 39
    new-instance v2, Lorg/json/JSONObject;

    .line 40
    .line 41
    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, v0, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 45
    .line 46
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v2, "first_launch"

    .line 51
    .line 52
    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    return-void

    .line 56
    :cond_0
    const-string v0, "Metrics: launch start ts is missing"

    .line 57
    .line 58
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :cond_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method protected final values(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 11

    .line 1
    invoke-super {p0, p1}, Lcom/appsflyer/internal/AFe1qSDK;->values(Lcom/appsflyer/internal/AFa1uSDK;)V

    .line 2
    .line 3
    .line 4
    iget v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->afRDLog:I

    .line 5
    .line 6
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 7
    .line 8
    invoke-virtual {v1, v0}, Lcom/appsflyer/internal/AFf1tSDK;->values(I)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const-string v2, "meta"

    .line 16
    .line 17
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Ljava/util/Map;

    .line 22
    .line 23
    if-nez v1, :cond_0

    .line 24
    .line 25
    new-instance v1, Ljava/util/HashMap;

    .line 26
    .line 27
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1pSDK;->onAppOpenAttributionNative:Lcom/appsflyer/internal/AFe1oSDK;

    .line 38
    .line 39
    invoke-virtual {v3}, Lcom/appsflyer/internal/AFe1oSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFf1cSDK;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    if-eqz v3, :cond_8

    .line 44
    .line 45
    new-instance v4, Ljava/util/HashMap;

    .line 46
    .line 47
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v5, "cdn_token"

    .line 51
    .line 52
    iget-object v6, v3, Lcom/appsflyer/internal/AFf1cSDK;->valueOf:Ljava/lang/String;

    .line 53
    .line 54
    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    iget-object v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->AFInAppEventType:Ljava/lang/String;

    .line 58
    .line 59
    if-eqz v5, :cond_1

    .line 60
    .line 61
    const-string v6, "c_ver"

    .line 62
    .line 63
    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    :cond_1
    iget-wide v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->AFKeystoreWrapper:J

    .line 67
    .line 68
    const-wide/16 v7, 0x0

    .line 69
    .line 70
    cmp-long v9, v5, v7

    .line 71
    .line 72
    if-lez v9, :cond_2

    .line 73
    .line 74
    const-string v9, "latency"

    .line 75
    .line 76
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    invoke-interface {v4, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    :cond_2
    iget-wide v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->values:J

    .line 84
    .line 85
    cmp-long v9, v5, v7

    .line 86
    .line 87
    if-lez v9, :cond_3

    .line 88
    .line 89
    const-string v7, "delay"

    .line 90
    .line 91
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 92
    .line 93
    .line 94
    move-result-object v5

    .line 95
    invoke-interface {v4, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    .line 97
    .line 98
    :cond_3
    iget v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->AFInAppEventParameterName:I

    .line 99
    .line 100
    if-lez v5, :cond_4

    .line 101
    .line 102
    const-string v6, "res_code"

    .line 103
    .line 104
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    :cond_4
    iget-object v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->afRDLog:Ljava/lang/Throwable;

    .line 112
    .line 113
    if-eqz v5, :cond_5

    .line 114
    .line 115
    new-instance v5, Ljava/lang/StringBuilder;

    .line 116
    .line 117
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .line 119
    .line 120
    iget-object v6, v3, Lcom/appsflyer/internal/AFf1cSDK;->afRDLog:Ljava/lang/Throwable;

    .line 121
    .line 122
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 123
    .line 124
    .line 125
    move-result-object v6

    .line 126
    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v6

    .line 130
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string v6, ": "

    .line 134
    .line 135
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    iget-object v6, v3, Lcom/appsflyer/internal/AFf1cSDK;->afRDLog:Ljava/lang/Throwable;

    .line 139
    .line 140
    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v6

    .line 144
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v5

    .line 151
    const-string v6, "error"

    .line 152
    .line 153
    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    :cond_5
    iget-object v5, v3, Lcom/appsflyer/internal/AFf1cSDK;->afDebugLog:Lcom/appsflyer/internal/AFf1fSDK;

    .line 157
    .line 158
    if-eqz v5, :cond_6

    .line 159
    .line 160
    const-string v6, "sig"

    .line 161
    .line 162
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v5

    .line 166
    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    :cond_6
    iget-object v3, v3, Lcom/appsflyer/internal/AFf1cSDK;->AFLogger:Ljava/lang/String;

    .line 170
    .line 171
    if-eqz v3, :cond_7

    .line 172
    .line 173
    const-string v5, "cdn_cache_status"

    .line 174
    .line 175
    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    .line 177
    .line 178
    :cond_7
    const-string v3, "rc"

    .line 179
    .line 180
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    :cond_8
    const/4 v3, 0x0

    .line 184
    const-string v4, "first_launch"

    .line 185
    .line 186
    const/4 v5, 0x2

    .line 187
    const/4 v6, 0x1

    .line 188
    if-eq v0, v6, :cond_b

    .line 189
    .line 190
    if-eq v0, v5, :cond_9

    .line 191
    .line 192
    goto :goto_0

    .line 193
    :cond_9
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 194
    .line 195
    new-instance v8, Ljava/util/HashMap;

    .line 196
    .line 197
    iget-object v7, v7, Lcom/appsflyer/internal/AFf1tSDK;->AFKeystoreWrapper:Ljava/util/Map;

    .line 198
    .line 199
    invoke-direct {v8, v7}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 200
    .line 201
    .line 202
    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    .line 203
    .line 204
    .line 205
    move-result v7

    .line 206
    if-nez v7, :cond_a

    .line 207
    .line 208
    invoke-interface {v1, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    .line 210
    .line 211
    :cond_a
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 212
    .line 213
    iget-object v7, v7, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 214
    .line 215
    invoke-interface {v7, v4}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    goto :goto_0

    .line 219
    :cond_b
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->onInstallConversionDataLoadedNative:Lcom/appsflyer/AppsFlyerProperties;

    .line 220
    .line 221
    const-string v8, "waitForCustomerId"

    .line 222
    .line 223
    invoke-virtual {v7, v8, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    .line 224
    .line 225
    .line 226
    move-result v7

    .line 227
    if-eqz v7, :cond_c

    .line 228
    .line 229
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 230
    .line 231
    .line 232
    move-result-object v7

    .line 233
    const-string v8, "wait_cid"

    .line 234
    .line 235
    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object v9

    .line 239
    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    .line 241
    .line 242
    :cond_c
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 243
    .line 244
    new-instance v8, Ljava/util/HashMap;

    .line 245
    .line 246
    iget-object v7, v7, Lcom/appsflyer/internal/AFf1tSDK;->values:Ljava/util/Map;

    .line 247
    .line 248
    invoke-direct {v8, v7}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 249
    .line 250
    .line 251
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 252
    .line 253
    iget-object v7, v7, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    .line 254
    .line 255
    const-string v9, "ddl"

    .line 256
    .line 257
    invoke-interface {v7, v9}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    .line 261
    .line 262
    .line 263
    move-result v7

    .line 264
    if-nez v7, :cond_d

    .line 265
    .line 266
    invoke-interface {v1, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    .line 268
    .line 269
    :cond_d
    iget-object v7, p0, Lcom/appsflyer/internal/AFe1pSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFf1tSDK;

    .line 270
    .line 271
    new-instance v8, Ljava/util/HashMap;

    .line 272
    .line 273
    iget-object v7, v7, Lcom/appsflyer/internal/AFf1tSDK;->AFKeystoreWrapper:Ljava/util/Map;

    .line 274
    .line 275
    invoke-direct {v8, v7}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 276
    .line 277
    .line 278
    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    .line 279
    .line 280
    .line 281
    move-result v7

    .line 282
    if-nez v7, :cond_e

    .line 283
    .line 284
    invoke-interface {v1, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    .line 286
    .line 287
    :cond_e
    :goto_0
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    .line 288
    .line 289
    .line 290
    move-result v1

    .line 291
    if-eqz v1, :cond_f

    .line 292
    .line 293
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    .line 294
    .line 295
    .line 296
    move-result-object v1

    .line 297
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    .line 299
    .line 300
    :cond_f
    if-gt v0, v5, :cond_16

    .line 301
    .line 302
    new-instance v1, Ljava/util/ArrayList;

    .line 303
    .line 304
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .line 306
    .line 307
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1pSDK;->getLevel:Lcom/appsflyer/internal/AFg1nSDK;

    .line 308
    .line 309
    invoke-virtual {v2}, Lcom/appsflyer/internal/AFg1nSDK;->values()[Lcom/appsflyer/internal/AFg1mSDK;

    .line 310
    .line 311
    .line 312
    move-result-object v2

    .line 313
    array-length v4, v2

    .line 314
    :goto_1
    if-ge v3, v4, :cond_14

    .line 315
    .line 316
    aget-object v7, v2, v3

    .line 317
    .line 318
    instance-of v8, v7, Lcom/appsflyer/internal/AFg1sSDK;

    .line 319
    .line 320
    sget-object v9, Lcom/appsflyer/internal/AFe1pSDK$2;->AFInAppEventParameterName:[I

    .line 321
    .line 322
    iget-object v10, v7, Lcom/appsflyer/internal/AFg1mSDK;->afDebugLog:Lcom/appsflyer/internal/AFg1mSDK$AFa1vSDK;

    .line 323
    .line 324
    invoke-virtual {v10}, Ljava/lang/Enum;->ordinal()I

    .line 325
    .line 326
    .line 327
    move-result v10

    .line 328
    aget v9, v9, v10

    .line 329
    .line 330
    if-eq v9, v6, :cond_11

    .line 331
    .line 332
    if-eq v9, v5, :cond_10

    .line 333
    .line 334
    goto :goto_2

    .line 335
    :cond_10
    if-ne v0, v5, :cond_13

    .line 336
    .line 337
    if-nez v8, :cond_13

    .line 338
    .line 339
    new-instance v8, Ljava/util/HashMap;

    .line 340
    .line 341
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 342
    .line 343
    .line 344
    const-string v9, "source"

    .line 345
    .line 346
    iget-object v10, v7, Lcom/appsflyer/internal/AFg1mSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 347
    .line 348
    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    .line 350
    .line 351
    const-string v9, "response"

    .line 352
    .line 353
    const-string v10, "TIMEOUT"

    .line 354
    .line 355
    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    const-string v9, "type"

    .line 359
    .line 360
    iget-object v7, v7, Lcom/appsflyer/internal/AFg1mSDK;->AFLogger:Ljava/lang/String;

    .line 361
    .line 362
    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    .line 364
    .line 365
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    .line 367
    .line 368
    goto :goto_2

    .line 369
    :cond_11
    if-eqz v8, :cond_12

    .line 370
    .line 371
    move-object v8, v7

    .line 372
    check-cast v8, Lcom/appsflyer/internal/AFg1sSDK;

    .line 373
    .line 374
    iget-object v8, v8, Lcom/appsflyer/internal/AFg1sSDK;->values:Ljava/util/Map;

    .line 375
    .line 376
    const-string v9, "rfr"

    .line 377
    .line 378
    invoke-virtual {p1, v9, v8}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 379
    .line 380
    .line 381
    iget-object v8, p0, Lcom/appsflyer/internal/AFe1pSDK;->afWarnLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 382
    .line 383
    const-string v9, "newGPReferrerSent"

    .line 384
    .line 385
    invoke-interface {v8, v9, v6}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;Z)V

    .line 386
    .line 387
    .line 388
    :cond_12
    iget-object v7, v7, Lcom/appsflyer/internal/AFg1mSDK;->valueOf:Ljava/util/Map;

    .line 389
    .line 390
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    .line 392
    .line 393
    :cond_13
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 394
    .line 395
    goto :goto_1

    .line 396
    :cond_14
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    .line 397
    .line 398
    .line 399
    move-result v0

    .line 400
    if-nez v0, :cond_15

    .line 401
    .line 402
    const-string v0, "referrers"

    .line 403
    .line 404
    invoke-virtual {p1, v0, v1}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 405
    .line 406
    .line 407
    :cond_15
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1pSDK;->afErrorLogForExcManagerOnly:Ljava/util/Map;

    .line 408
    .line 409
    if-eqz v0, :cond_16

    .line 410
    .line 411
    const-string v1, "fb_ddl"

    .line 412
    .line 413
    invoke-virtual {p1, v1, v0}, Lcom/appsflyer/internal/AFa1uSDK;->values(Ljava/lang/String;Ljava/lang/Object;)Lcom/appsflyer/internal/AFa1uSDK;

    .line 414
    .line 415
    .line 416
    :cond_16
    return-void
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

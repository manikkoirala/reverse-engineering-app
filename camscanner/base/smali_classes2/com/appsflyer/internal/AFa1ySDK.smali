.class public Lcom/appsflyer/internal/AFa1ySDK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final $$a:[B = null

.field public static final $$b:I = 0x0

.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field public static final init:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static onAppOpenAttribution:J

.field private static onAttributionFailure:[B

.field private static onAttributionFailureNative:[B

.field private static onConversionDataFail:I

.field private static onConversionDataSuccess:[B

.field private static final onDeepLinkingNative:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static onResponse:I

.field private static onResponseErrorNative:Ljava/lang/Object;

.field private static onResponseNative:Ljava/lang/Object;


# direct methods
.method private static $$c(BIS)Ljava/lang/String;
    .locals 9

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x36

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    sub-int/2addr v1, v2

    .line 7
    rem-int/lit16 v3, v1, 0x80

    .line 8
    .line 9
    sput v3, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 10
    .line 11
    rem-int/lit8 v1, v1, 0x2

    .line 12
    .line 13
    sget-object v1, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    .line 14
    .line 15
    xor-int/lit8 v3, p1, -0x54

    .line 16
    .line 17
    and-int/lit8 p1, p1, -0x54

    .line 18
    .line 19
    shl-int/2addr p1, v2

    .line 20
    add-int/2addr v3, p1

    .line 21
    add-int/lit8 v3, v3, 0x7e

    .line 22
    .line 23
    sub-int/2addr v3, v2

    .line 24
    neg-int p1, p2

    .line 25
    or-int/lit16 p2, p1, 0x3da

    .line 26
    .line 27
    shl-int/2addr p2, v2

    .line 28
    xor-int/lit16 p1, p1, 0x3da

    .line 29
    .line 30
    sub-int/2addr p2, p1

    .line 31
    add-int/2addr p0, v2

    .line 32
    new-array p1, p0, [B

    .line 33
    .line 34
    const/4 v4, 0x0

    .line 35
    if-nez v1, :cond_0

    .line 36
    .line 37
    const/4 v5, 0x0

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v5, 0x1

    .line 40
    :goto_0
    if-eq v5, v2, :cond_1

    .line 41
    .line 42
    and-int/lit8 v5, v0, 0x21

    .line 43
    .line 44
    or-int/lit8 v0, v0, 0x21

    .line 45
    .line 46
    add-int/2addr v5, v0

    .line 47
    rem-int/lit16 v0, v5, 0x80

    .line 48
    .line 49
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 50
    .line 51
    rem-int/lit8 v5, v5, 0x2

    .line 52
    .line 53
    move v0, p2

    .line 54
    const/4 v7, 0x0

    .line 55
    goto :goto_2

    .line 56
    :cond_1
    const/4 v0, 0x0

    .line 57
    :goto_1
    add-int/lit8 p2, p2, 0x2

    .line 58
    .line 59
    sub-int/2addr p2, v2

    .line 60
    int-to-byte v5, v3

    .line 61
    xor-int/lit8 v6, v0, 0x10

    .line 62
    .line 63
    and-int/lit8 v7, v0, 0x10

    .line 64
    .line 65
    shl-int/2addr v7, v2

    .line 66
    add-int/2addr v6, v7

    .line 67
    xor-int/lit8 v7, v6, -0xf

    .line 68
    .line 69
    and-int/lit8 v6, v6, -0xf

    .line 70
    .line 71
    shl-int/2addr v6, v2

    .line 72
    add-int/2addr v7, v6

    .line 73
    aput-byte v5, p1, v0

    .line 74
    .line 75
    if-ne v7, p0, :cond_2

    .line 76
    .line 77
    new-instance p0, Ljava/lang/String;

    .line 78
    .line 79
    invoke-direct {p0, p1, v4}, Ljava/lang/String;-><init>([BI)V

    .line 80
    .line 81
    .line 82
    return-object p0

    .line 83
    :cond_2
    aget-byte v0, v1, p2

    .line 84
    .line 85
    sget v5, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 86
    .line 87
    or-int/lit8 v6, v5, 0x15

    .line 88
    .line 89
    shl-int/2addr v6, v2

    .line 90
    xor-int/lit8 v5, v5, 0x15

    .line 91
    .line 92
    sub-int/2addr v6, v5

    .line 93
    rem-int/lit16 v5, v6, 0x80

    .line 94
    .line 95
    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 96
    .line 97
    rem-int/lit8 v6, v6, 0x2

    .line 98
    .line 99
    move v8, v0

    .line 100
    move v0, p2

    .line 101
    move p2, v8

    .line 102
    :goto_2
    neg-int p2, p2

    .line 103
    not-int p2, p2

    .line 104
    sub-int/2addr v3, p2

    .line 105
    sub-int/2addr v3, v2

    .line 106
    sget p2, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 107
    .line 108
    xor-int/lit8 v5, p2, 0x79

    .line 109
    .line 110
    and-int/lit8 p2, p2, 0x79

    .line 111
    .line 112
    shl-int/2addr p2, v2

    .line 113
    add-int/2addr v5, p2

    .line 114
    rem-int/lit16 p2, v5, 0x80

    .line 115
    .line 116
    sput p2, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 117
    .line 118
    rem-int/lit8 v5, v5, 0x2

    .line 119
    .line 120
    move p2, v0

    .line 121
    move v0, v7

    .line 122
    goto :goto_1
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method static constructor <clinit>()V
    .locals 47

    const-class v1, [B

    invoke-static {}, Lcom/appsflyer/internal/AFa1ySDK;->init$0()V

    const-wide v2, -0x1c9d90eed26e183fL    # -5.565494350506325E170

    .line 1
    sput-wide v2, Lcom/appsflyer/internal/AFa1ySDK;->onAppOpenAttribution:J

    const/4 v2, 0x0

    sput v2, Lcom/appsflyer/internal/AFa1ySDK;->onConversionDataFail:I

    const/4 v3, 0x2

    sput v3, Lcom/appsflyer/internal/AFa1ySDK;->onResponse:I

    .line 2
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/appsflyer/internal/AFa1ySDK;->onDeepLinkingNative:Ljava/util/Map;

    .line 3
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/appsflyer/internal/AFa1ySDK;->init:Ljava/util/Map;

    .line 4
    :try_start_0
    sget-object v4, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0xa

    aget-byte v5, v4, v5

    int-to-byte v5, v5

    const/16 v6, 0x151

    aget-byte v7, v4, v6

    neg-int v7, v7

    int-to-byte v7, v7

    const/16 v8, 0x297

    int-to-short v8, v8

    invoke-static {v5, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    .line 5
    sget-object v7, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_10

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-nez v7, :cond_2

    .line 6
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    or-int/lit8 v10, v7, 0x6b

    shl-int/2addr v10, v9

    xor-int/lit8 v7, v7, 0x6b

    sub-int/2addr v10, v7

    rem-int/lit16 v7, v10, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    rem-int/2addr v10, v3

    if-nez v10, :cond_0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_1

    const/16 v7, 0x72

    .line 7
    :try_start_1
    aget-byte v7, v4, v7

    int-to-byte v7, v7

    aget-byte v10, v4, v6

    neg-int v10, v10

    int-to-byte v10, v10

    const/16 v11, 0x292

    aget-byte v11, v4, v11

    int-to-short v11, v11

    invoke-static {v7, v10, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_1
    const/16 v7, 0x2d

    .line 8
    aget-byte v7, v4, v7

    int-to-byte v7, v7

    const/16 v10, 0x468a

    aget-byte v10, v4, v10

    neg-int v10, v10

    int-to-byte v10, v10

    const/16 v11, 0x7429

    aget-byte v11, v4, v11

    int-to-short v11, v11

    invoke-static {v7, v10, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_10

    goto :goto_1

    :cond_2
    move-object v7, v8

    :goto_1
    const/16 v10, 0x2b6

    const/4 v11, -0x1

    const/16 v12, 0xd8

    .line 9
    :try_start_2
    aget-byte v10, v4, v10

    int-to-byte v10, v10

    const/16 v13, 0x3c

    aget-byte v13, v4, v13

    sub-int/2addr v13, v9

    int-to-byte v13, v13

    const/16 v14, 0xd0

    int-to-short v14, v14

    invoke-static {v10, v13, v14}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v10

    .line 10
    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    const/16 v13, 0x77

    aget-byte v13, v4, v13

    int-to-byte v13, v13

    aget-byte v4, v4, v6

    neg-int v4, v4

    int-to-byte v4, v4

    const/16 v14, 0x398

    int-to-short v14, v14

    invoke-static {v13, v4, v14}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    new-array v13, v2, [Ljava/lang/Class;

    .line 11
    invoke-virtual {v10, v4, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 12
    invoke-virtual {v4, v8, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v4, :cond_3

    goto :goto_2

    :catch_0
    move-object v4, v8

    .line 13
    :cond_3
    :try_start_3
    sget-object v10, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v13, 0x1c

    aget-byte v13, v10, v13

    int-to-byte v13, v13

    const/16 v14, 0x3c

    aget-byte v14, v10, v14

    and-int/lit8 v15, v14, -0x1

    or-int/2addr v14, v11

    add-int/2addr v15, v14

    int-to-byte v14, v15

    const/16 v15, 0x221

    int-to-short v15, v15

    invoke-static {v13, v14, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v13

    .line 14
    invoke-static {v13}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    const/16 v14, 0xcd

    aget-byte v14, v10, v14

    int-to-byte v14, v14

    aget-byte v10, v10, v12

    int-to-byte v10, v10

    const/16 v15, 0x80

    int-to-short v15, v15

    invoke-static {v14, v10, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v10

    new-array v14, v2, [Ljava/lang/Class;

    .line 15
    invoke-virtual {v13, v10, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 16
    invoke-virtual {v10, v8, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    nop

    :goto_2
    const/4 v10, 0x7

    if-eqz v4, :cond_6

    .line 17
    sget v13, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    or-int/lit8 v14, v13, 0x7

    shl-int/2addr v14, v9

    xor-int/2addr v13, v10

    sub-int/2addr v14, v13

    rem-int/lit16 v13, v14, 0x80

    sput v13, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    rem-int/2addr v14, v3

    if-nez v14, :cond_4

    const/4 v13, 0x0

    goto :goto_3

    :cond_4
    const/4 v13, 0x1

    :goto_3
    if-eq v13, v9, :cond_5

    .line 18
    :try_start_4
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    sget-object v14, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v15, 0x71

    aget-byte v15, v14, v15

    int-to-byte v15, v15

    const/16 v16, 0x1130

    aget-byte v14, v14, v16

    int-to-byte v14, v14

    const/16 v11, 0x21ed

    int-to-short v11, v11

    invoke-static {v15, v14, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v11

    .line 19
    invoke-virtual {v13, v11, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 20
    :goto_4
    invoke-virtual {v11, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    goto :goto_6

    :catch_2
    nop

    goto :goto_5

    .line 21
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    sget-object v13, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v14, 0x67

    aget-byte v14, v13, v14

    int-to-byte v14, v14

    aget-byte v13, v13, v12

    int-to-byte v13, v13

    const/16 v15, 0x2f8

    int-to-short v15, v15

    invoke-static {v14, v13, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v13

    .line 22
    invoke-virtual {v11, v13, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    :cond_6
    :goto_5
    move-object v11, v8

    :goto_6
    if-eqz v4, :cond_7

    .line 23
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    sget-object v14, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v15, 0xb7

    aget-byte v15, v14, v15

    int-to-byte v15, v15

    aget-byte v14, v14, v12

    int-to-byte v14, v14

    sget v6, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    xor-int/lit16 v10, v6, 0x215

    and-int/lit16 v6, v6, 0x215

    or-int/2addr v6, v10

    int-to-short v6, v6

    invoke-static {v15, v14, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    .line 24
    invoke-virtual {v13, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 25
    invoke-virtual {v6, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_7

    :catch_3
    nop

    :cond_7
    move-object v6, v8

    :goto_7
    const/16 v13, 0xe

    if-eqz v4, :cond_a

    .line 26
    sget v14, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    add-int/lit8 v14, v14, 0x71

    rem-int/lit16 v15, v14, 0x80

    sput v15, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    rem-int/2addr v14, v3

    if-eqz v14, :cond_8

    const/4 v14, 0x5

    goto :goto_8

    :cond_8
    const/16 v14, 0x31

    :goto_8
    const/16 v15, 0x31

    if-eq v14, v15, :cond_9

    .line 27
    :try_start_6
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    sget-object v15, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    aget-byte v10, v15, v13

    int-to-byte v10, v10

    const/16 v17, 0x2141

    aget-byte v15, v15, v17

    int-to-byte v15, v15

    const/16 v13, 0x62ce

    int-to-short v13, v13

    invoke-static {v10, v15, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v10

    .line 28
    invoke-virtual {v14, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 29
    :goto_9
    invoke-virtual {v10, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_b

    :catch_4
    nop

    goto :goto_a

    .line 30
    :cond_9
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    sget-object v13, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v14, 0x67

    aget-byte v14, v13, v14

    int-to-byte v14, v14

    aget-byte v13, v13, v12

    int-to-byte v13, v13

    const/16 v15, 0x2e0

    int-to-short v15, v15

    invoke-static {v14, v13, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v13

    .line 31
    invoke-virtual {v10, v13, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_9

    :cond_a
    :goto_a
    move-object v4, v8

    .line 32
    :goto_b
    const-class v10, Ljava/lang/String;

    const/16 v13, 0x39

    const/16 v14, 0x20

    if-eqz v11, :cond_b

    .line 33
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    or-int/lit8 v15, v7, 0xd

    shl-int/2addr v15, v9

    xor-int/lit8 v7, v7, 0xd

    sub-int/2addr v15, v7

    rem-int/lit16 v7, v15, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    rem-int/2addr v15, v3

    goto :goto_c

    :cond_b
    if-nez v7, :cond_c

    move-object v11, v8

    goto :goto_c

    .line 34
    :cond_c
    :try_start_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v18, 0x67

    aget-byte v8, v15, v18

    int-to-byte v8, v8

    const/16 v18, 0x3f

    aget-byte v12, v15, v18

    int-to-byte v12, v12

    sget v3, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    or-int/lit16 v3, v3, 0x111

    int-to-short v3, v3

    invoke-static {v8, v12, v3}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_10

    :try_start_8
    new-array v7, v9, [Ljava/lang/Object;

    aput-object v3, v7, v2

    aget-byte v3, v15, v13

    int-to-byte v3, v3

    aget-byte v8, v15, v14

    int-to-byte v8, v8

    const/16 v11, 0x6c

    int-to-short v11, v11

    invoke-static {v3, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    new-array v8, v9, [Ljava/lang/Class;

    aput-object v10, v8, v2

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_61

    :goto_c
    if-eqz v4, :cond_d

    goto/16 :goto_d

    .line 35
    :cond_d
    :try_start_9
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/4 v4, 0x7

    aget-byte v7, v3, v4

    int-to-byte v4, v7

    aget-byte v7, v3, v14

    int-to-byte v7, v7

    xor-int/lit16 v8, v7, 0xb4

    and-int/lit16 v12, v7, 0xb4

    or-int/2addr v8, v12

    int-to-short v8, v8

    invoke-static {v4, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_10

    .line 36
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    and-int/lit8 v8, v7, 0x7b

    or-int/lit8 v12, v7, 0x7b

    add-int/2addr v8, v12

    rem-int/lit16 v12, v8, 0x80

    sput v12, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v12, 0x2

    rem-int/2addr v8, v12

    and-int/lit8 v8, v7, 0x9

    or-int/lit8 v7, v7, 0x9

    add-int/2addr v8, v7

    .line 37
    rem-int/lit16 v7, v8, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v7, 0x2

    rem-int/2addr v8, v7

    :try_start_a
    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v2

    const/16 v4, 0x23

    .line 38
    aget-byte v4, v3, v4

    int-to-byte v4, v4

    aget-byte v8, v3, v14

    int-to-byte v8, v8

    const/16 v12, 0x3a7

    int-to-short v12, v12

    invoke-static {v4, v8, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v8, 0x67

    aget-byte v8, v3, v8

    int-to-byte v8, v8

    const/16 v12, 0xd8

    aget-byte v15, v3, v12

    int-to-byte v12, v15

    const/16 v15, 0xe8

    int-to-short v15, v15

    invoke-static {v8, v12, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    new-array v12, v9, [Ljava/lang/Class;

    aput-object v10, v12, v2

    invoke-virtual {v4, v8, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v8, 0x0

    invoke-virtual {v4, v8, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_60

    .line 39
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    and-int/lit8 v8, v7, 0x11

    or-int/lit8 v7, v7, 0x11

    add-int/2addr v8, v7

    rem-int/lit16 v7, v8, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v7, 0x2

    rem-int/2addr v8, v7

    :try_start_b
    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v2

    .line 40
    aget-byte v4, v3, v13

    int-to-byte v4, v4

    aget-byte v3, v3, v14

    int-to-byte v3, v3

    const/16 v8, 0x6c

    int-to-short v8, v8

    invoke-static {v4, v3, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Class;

    aput-object v10, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5f

    :goto_d
    if-nez v6, :cond_f

    if-eqz v11, :cond_f

    .line 41
    :try_start_c
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v6, 0x1a

    aget-byte v6, v3, v6

    int-to-byte v6, v6

    const/16 v7, 0x151

    aget-byte v8, v3, v7

    neg-int v7, v8

    int-to-byte v7, v7

    xor-int/lit16 v8, v7, 0x285

    and-int/lit16 v12, v7, 0x285

    or-int/2addr v8, v12

    int-to-short v8, v8

    invoke-static {v6, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_10

    const/4 v7, 0x2

    :try_start_d
    new-array v8, v7, [Ljava/lang/Object;

    aput-object v6, v8, v9

    aput-object v11, v8, v2

    aget-byte v6, v3, v13

    int-to-byte v6, v6

    aget-byte v7, v3, v14

    int-to-byte v7, v7

    const/16 v12, 0x6c

    int-to-short v12, v12

    invoke-static {v6, v7, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v7, 0x2

    new-array v15, v7, [Ljava/lang/Class;

    aget-byte v7, v3, v13

    int-to-byte v7, v7

    aget-byte v3, v3, v14

    int-to-byte v3, v3

    invoke-static {v7, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v15, v2

    aput-object v10, v15, v9

    invoke-virtual {v6, v15}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_e

    :catchall_0
    move-exception v0

    move-object v1, v0

    :try_start_e
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_e

    throw v2

    :cond_e
    throw v1

    .line 42
    :cond_f
    :goto_e
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    aget-byte v7, v3, v13

    int-to-byte v7, v7

    aget-byte v8, v3, v14

    int-to-byte v8, v8

    const/16 v12, 0x6c

    int-to-short v12, v12

    invoke-static {v7, v8, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v8, 0x7

    invoke-static {v7, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v8, v7, v2

    aput-object v6, v7, v9

    const/4 v8, 0x2

    aput-object v11, v7, v8

    const/4 v8, 0x3

    aput-object v4, v7, v8

    const/4 v15, 0x4

    aput-object v6, v7, v15

    const/4 v6, 0x5

    aput-object v11, v7, v6

    const/4 v6, 0x6

    aput-object v4, v7, v6

    const/4 v4, 0x7

    new-array v11, v4, [Z

    aput-boolean v2, v11, v2

    aput-boolean v9, v11, v9

    const/4 v4, 0x2

    aput-boolean v9, v11, v4

    aput-boolean v9, v11, v8

    aput-boolean v9, v11, v15

    const/4 v4, 0x5

    aput-boolean v9, v11, v4

    aput-boolean v9, v11, v6

    const/4 v4, 0x7

    new-array v14, v4, [Z

    aput-boolean v2, v14, v2

    aput-boolean v2, v14, v9

    const/4 v4, 0x2

    aput-boolean v2, v14, v4

    aput-boolean v2, v14, v8

    aput-boolean v9, v14, v15

    const/4 v4, 0x5

    aput-boolean v9, v14, v4

    aput-boolean v9, v14, v6

    const/4 v4, 0x7

    new-array v13, v4, [Z

    aput-boolean v2, v13, v2

    aput-boolean v2, v13, v9

    const/4 v4, 0x2

    aput-boolean v9, v13, v4

    aput-boolean v9, v13, v8

    aput-boolean v2, v13, v15

    const/4 v4, 0x5

    aput-boolean v9, v13, v4

    aput-boolean v9, v13, v6
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_10

    const/16 v4, 0x186

    .line 43
    :try_start_f
    aget-byte v8, v3, v4

    int-to-byte v8, v8

    const/16 v21, 0x3c

    aget-byte v21, v3, v21

    xor-int/lit8 v22, v21, -0x1

    const/16 v16, -0x1

    and-int/lit8 v21, v21, -0x1

    shl-int/lit8 v21, v21, 0x1

    add-int v4, v22, v21

    int-to-byte v4, v4

    const/16 v6, 0x375

    int-to-short v6, v6

    invoke-static {v8, v4, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v6, 0x3f

    .line 44
    aget-byte v6, v3, v6

    int-to-byte v6, v6

    const/16 v8, 0x232

    aget-byte v3, v3, v8

    int-to-byte v3, v3

    const/16 v8, 0x130

    int-to-short v8, v8

    invoke-static {v6, v3, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3
    :try_end_f
    .catch Ljava/lang/ClassNotFoundException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_10

    const/16 v4, 0x22

    if-lt v3, v4, :cond_10

    const/16 v4, 0x5a

    goto :goto_f

    :cond_10
    const/16 v4, 0x2c

    :goto_f
    const/16 v6, 0x2c

    if-eq v4, v6, :cond_11

    const/4 v4, 0x1

    goto :goto_10

    :cond_11
    const/4 v4, 0x0

    :goto_10
    const/16 v6, 0x1d

    if-ne v3, v6, :cond_12

    const/4 v6, 0x0

    goto :goto_11

    :cond_12
    const/4 v6, 0x1

    :goto_11
    if-eq v6, v9, :cond_13

    goto :goto_12

    :cond_13
    const/16 v6, 0x1a

    if-lt v3, v6, :cond_14

    const/4 v6, 0x1

    goto :goto_13

    :cond_14
    :goto_12
    const/4 v6, 0x0

    :goto_13
    :try_start_10
    aput-boolean v6, v13, v2
    :try_end_10
    .catch Ljava/lang/ClassNotFoundException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_10

    const/16 v6, 0x15

    if-lt v3, v6, :cond_15

    .line 45
    sget v6, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    and-int/lit8 v8, v6, 0x1d

    or-int/lit8 v6, v6, 0x1d

    add-int/2addr v8, v6

    rem-int/lit16 v6, v8, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v6, 0x2

    rem-int/2addr v8, v6

    const/4 v6, 0x1

    goto :goto_14

    :cond_15
    const/4 v6, 0x0

    :goto_14
    :try_start_11
    aput-boolean v6, v13, v9

    const/16 v6, 0x15

    if-lt v3, v6, :cond_16

    const/4 v3, 0x1

    goto :goto_15

    :cond_16
    const/4 v3, 0x0

    :goto_15
    aput-boolean v3, v13, v15
    :try_end_11
    .catch Ljava/lang/ClassNotFoundException; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_10

    goto :goto_16

    :catch_5
    nop

    goto :goto_16

    :catch_6
    nop

    const/4 v4, 0x0

    :goto_16
    const/4 v3, 0x0

    const/4 v6, 0x0

    :goto_17
    if-nez v3, :cond_17

    const/16 v8, 0x34

    goto :goto_18

    :cond_17
    const/16 v8, 0x4e

    :goto_18
    const/16 v15, 0x34

    if-eq v8, v15, :cond_18

    goto/16 :goto_68

    :cond_18
    const/16 v8, 0x9

    if-ge v6, v8, :cond_80

    .line 46
    :try_start_12
    aget-boolean v8, v13, v6
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_10

    if-eqz v8, :cond_7f

    .line 47
    :try_start_13
    aget-boolean v23, v11, v6

    aget-object v8, v7, v6

    aget-boolean v24, v14, v6
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5d

    if-eqz v23, :cond_19

    const/16 v25, 0xc

    const/16 v2, 0xc

    goto :goto_19

    :cond_19
    const/16 v25, 0x5b

    const/16 v2, 0x5b

    :goto_19
    const/16 v9, 0x5b

    if-eq v2, v9, :cond_1d

    if-eqz v8, :cond_1b

    .line 48
    :try_start_14
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v9, 0x39

    aget-byte v15, v2, v9
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    int-to-byte v9, v15

    move/from16 v27, v3

    const/16 v15, 0x20

    :try_start_15
    aget-byte v3, v2, v15

    int-to-byte v3, v3

    invoke-static {v9, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v9, 0x3b

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    const/16 v15, 0x151

    aget-byte v2, v2, v15

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v15, 0xa0

    int-to-short v15, v15

    invoke-static {v9, v2, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x0

    invoke-virtual {v3, v2, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    if-nez v2, :cond_1e

    goto :goto_1b

    :catchall_1
    move-exception v0

    goto :goto_1a

    :catchall_2
    move-exception v0

    move/from16 v27, v3

    :goto_1a
    move-object v2, v0

    :try_start_16
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_1a

    throw v3

    :cond_1a
    throw v2
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    :catchall_3
    move-exception v0

    move-object v3, v0

    move/from16 v39, v4

    move-object/from16 v28, v5

    goto/16 :goto_1d

    :cond_1b
    move/from16 v27, v3

    .line 49
    :goto_1b
    :try_start_17
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v9, 0xe

    aget-byte v15, v3, v9
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    int-to-byte v9, v15

    move-object/from16 v28, v5

    const/16 v15, 0x357

    :try_start_18
    aget-byte v5, v3, v15

    int-to-byte v5, v5

    const/16 v15, 0x241

    int-to-short v15, v15

    invoke-static {v9, v5, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v5, 0x6

    aget-byte v8, v3, v5

    int-to-byte v5, v8

    int-to-byte v8, v5

    xor-int/lit16 v9, v8, 0x2d3

    and-int/lit16 v15, v8, 0x2d3

    or-int/2addr v9, v15

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    .line 50
    sget v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    or-int/lit8 v8, v5, 0x3b

    const/4 v9, 0x1

    shl-int/2addr v8, v9

    xor-int/lit8 v5, v5, 0x3b

    sub-int/2addr v8, v5

    rem-int/lit16 v5, v8, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v5, 0x2

    rem-int/2addr v8, v5

    const/4 v5, 0x1

    :try_start_19
    new-array v8, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v8, v5

    const/4 v2, 0x4

    .line 51
    aget-byte v5, v3, v2

    neg-int v2, v5

    int-to-byte v2, v2

    const/16 v5, 0x20

    aget-byte v3, v3, v5

    int-to-byte v3, v3

    const/16 v5, 0x99

    int-to-short v5, v5

    invoke-static {v2, v3, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v10, v5, v3

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_4

    :catchall_4
    move-exception v0

    move-object v2, v0

    :try_start_1a
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_1c

    throw v3

    :cond_1c
    throw v2
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    :catchall_5
    move-exception v0

    goto :goto_1c

    :catchall_6
    move-exception v0

    move-object/from16 v28, v5

    :goto_1c
    move-object v3, v0

    move/from16 v39, v4

    :goto_1d
    move/from16 v44, v6

    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    goto/16 :goto_30

    :cond_1d
    move/from16 v27, v3

    :cond_1e
    move-object/from16 v28, v5

    if-eqz v23, :cond_37

    .line 52
    :try_start_1b
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_15

    .line 53
    :try_start_1c
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0x23

    aget-byte v5, v3, v5

    int-to-byte v5, v5

    const/16 v9, 0x20

    aget-byte v15, v3, v9

    int-to-byte v9, v15

    const/16 v15, 0x3a7

    int-to-short v15, v15

    invoke-static {v5, v9, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v9, 0x10

    aget-byte v9, v3, v9

    int-to-byte v9, v9

    const/16 v15, 0x151

    aget-byte v3, v3, v15

    neg-int v3, v3

    int-to-byte v3, v3

    sget v15, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    or-int/lit8 v29, v15, -0x5

    const/16 v25, 0x1

    shl-int/lit8 v29, v29, 0x1

    xor-int/lit8 v15, v15, -0x5

    sub-int v15, v29, v15

    int-to-short v15, v15

    invoke-static {v9, v3, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v5, v3, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v9, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v29
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_13

    const-wide/32 v31, -0x606229c7

    move-object v3, v14

    xor-long v14, v29, v31

    :try_start_1d
    invoke-virtual {v2, v14, v15}, Ljava/util/Random;->setSeed(J)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_12

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    :goto_1e
    if-nez v5, :cond_1f

    move-object/from16 v29, v3

    move-object/from16 v30, v5

    const/4 v3, 0x1

    goto :goto_1f

    :cond_1f
    move-object/from16 v29, v3

    move-object/from16 v30, v5

    const/4 v3, 0x0

    :goto_1f
    const/4 v5, 0x1

    if-eq v3, v5, :cond_20

    move-object/from16 v32, v7

    move-object v8, v9

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    goto/16 :goto_32

    :cond_20
    if-nez v9, :cond_21

    const/4 v3, 0x6

    goto :goto_20

    :cond_21
    if-nez v14, :cond_22

    .line 54
    sget v3, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    add-int/lit8 v3, v3, 0x1b

    rem-int/lit16 v5, v3, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v5, 0x2

    rem-int/2addr v3, v5

    const/4 v3, 0x5

    goto :goto_20

    :cond_22
    if-nez v15, :cond_23

    const/4 v3, 0x4

    goto :goto_20

    :cond_23
    const/4 v3, 0x3

    .line 55
    :goto_20
    :try_start_1e
    new-instance v5, Ljava/lang/StringBuilder;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_11

    add-int/lit8 v31, v3, 0x2

    move-object/from16 v32, v7

    const/16 v25, 0x1

    add-int/lit8 v7, v31, -0x1

    :try_start_1f
    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v7, 0x2e

    .line 56
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    :goto_21
    if-ge v7, v3, :cond_24

    const/16 v31, 0x0

    goto :goto_22

    :cond_24
    const/16 v31, 0x1

    :goto_22
    if-eqz v31, :cond_32

    .line 57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_10

    if-nez v9, :cond_25

    const/16 v5, 0x32

    goto :goto_23

    :cond_25
    const/16 v5, 0x22

    :goto_23
    const/16 v7, 0x22

    if-eq v5, v7, :cond_27

    const/4 v5, 0x2

    :try_start_20
    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v3, v7, v5

    const/4 v3, 0x0

    aput-object v8, v7, v3

    .line 58
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0x39

    aget-byte v9, v3, v5
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_9

    int-to-byte v5, v9

    move-object/from16 v31, v11

    const/16 v9, 0x20

    :try_start_21
    aget-byte v11, v3, v9

    int-to-byte v9, v11

    invoke-static {v5, v9, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v9, 0x2

    new-array v11, v9, [Ljava/lang/Class;
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_8

    move-object/from16 v33, v13

    const/16 v9, 0x39

    :try_start_22
    aget-byte v13, v3, v9

    int-to-byte v9, v13

    const/16 v13, 0x20

    aget-byte v3, v3, v13

    int-to-byte v3, v3

    invoke-static {v9, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v9, 0x0

    aput-object v3, v11, v9

    const/4 v3, 0x1

    aput-object v10, v11, v3

    invoke-virtual {v5, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_7

    move-object v9, v3

    :goto_24
    move-object/from16 v34, v8

    move-object/from16 v5, v30

    goto/16 :goto_29

    :catchall_7
    move-exception v0

    goto :goto_26

    :catchall_8
    move-exception v0

    goto :goto_25

    :catchall_9
    move-exception v0

    move-object/from16 v31, v11

    :goto_25
    move-object/from16 v33, v13

    :goto_26
    move-object v2, v0

    :try_start_23
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_26

    throw v3

    :cond_26
    throw v2
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_14

    :cond_27
    move-object/from16 v31, v11

    move-object/from16 v33, v13

    if-nez v14, :cond_28

    const/16 v5, 0x29

    goto :goto_27

    :cond_28
    const/16 v5, 0x54

    :goto_27
    const/16 v7, 0x54

    if-eq v5, v7, :cond_2a

    .line 59
    sget v5, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    xor-int/lit8 v7, v5, 0x2f

    and-int/lit8 v5, v5, 0x2f

    const/4 v11, 0x1

    shl-int/2addr v5, v11

    add-int/2addr v7, v5

    rem-int/lit16 v5, v7, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v5, 0x2

    rem-int/2addr v7, v5

    :try_start_24
    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v3, v7, v5

    const/4 v3, 0x0

    aput-object v8, v7, v3

    .line 60
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0x39

    aget-byte v11, v3, v5

    int-to-byte v5, v11

    const/16 v11, 0x20

    aget-byte v13, v3, v11

    int-to-byte v11, v13

    invoke-static {v5, v11, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v11, 0x2

    new-array v13, v11, [Ljava/lang/Class;

    const/16 v11, 0x39

    aget-byte v14, v3, v11

    int-to-byte v11, v14

    const/16 v14, 0x20

    aget-byte v3, v3, v14

    int-to-byte v3, v3

    invoke-static {v11, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v11, 0x0

    aput-object v3, v13, v11

    const/4 v3, 0x1

    aput-object v10, v13, v3

    invoke-virtual {v5, v13}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_a

    move-object v14, v3

    goto :goto_24

    :catchall_a
    move-exception v0

    move-object v2, v0

    :try_start_25
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_29

    throw v3

    :cond_29
    throw v2
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_14

    :cond_2a
    if-nez v15, :cond_2b

    const/16 v5, 0x4b

    goto :goto_28

    :cond_2b
    const/16 v5, 0x35

    :goto_28
    const/16 v7, 0x35

    if-eq v5, v7, :cond_2d

    .line 61
    sget v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    xor-int/lit8 v7, v5, 0x23

    and-int/lit8 v5, v5, 0x23

    const/4 v11, 0x1

    shl-int/2addr v5, v11

    add-int/2addr v7, v5

    rem-int/lit16 v5, v7, 0x80

    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v5, 0x2

    rem-int/2addr v7, v5

    :try_start_26
    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v3, v7, v5

    const/4 v3, 0x0

    aput-object v8, v7, v3

    .line 62
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0x39

    aget-byte v11, v3, v5

    int-to-byte v5, v11

    const/16 v11, 0x20

    aget-byte v13, v3, v11

    int-to-byte v11, v13

    invoke-static {v5, v11, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v11, 0x2

    new-array v13, v11, [Ljava/lang/Class;

    const/16 v11, 0x39

    aget-byte v15, v3, v11

    int-to-byte v11, v15

    const/16 v15, 0x20

    aget-byte v3, v3, v15

    int-to-byte v3, v3

    invoke-static {v11, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v11, 0x0

    aput-object v3, v13, v11

    const/4 v3, 0x1

    aput-object v10, v13, v3

    invoke-virtual {v5, v13}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_b

    move-object v15, v3

    goto/16 :goto_24

    :catchall_b
    move-exception v0

    move-object v2, v0

    :try_start_27
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_2c

    throw v3

    :cond_2c
    throw v2
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_14

    :cond_2d
    const/4 v5, 0x2

    :try_start_28
    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v3, v7, v5

    const/4 v3, 0x0

    aput-object v8, v7, v3

    .line 63
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0x39

    aget-byte v11, v3, v5

    int-to-byte v5, v11

    const/16 v11, 0x20

    aget-byte v13, v3, v11

    int-to-byte v11, v13

    invoke-static {v5, v11, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v11, 0x2

    new-array v13, v11, [Ljava/lang/Class;

    move-object/from16 v34, v8

    const/16 v11, 0x39

    aget-byte v8, v3, v11

    int-to-byte v8, v8

    move-object/from16 v35, v9

    const/16 v11, 0x20

    aget-byte v9, v3, v11

    int-to-byte v9, v9

    invoke-static {v8, v9, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/4 v9, 0x0

    aput-object v8, v13, v9

    const/4 v8, 0x1

    aput-object v10, v13, v8

    invoke-virtual {v5, v13}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_f

    :try_start_29
    new-array v7, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    const/16 v8, 0x186

    .line 64
    aget-byte v9, v3, v8

    int-to-byte v8, v9

    const/16 v9, 0x20

    aget-byte v11, v3, v9

    int-to-byte v9, v11

    const/16 v11, 0xb7

    int-to-short v11, v11

    invoke-static {v8, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/4 v9, 0x1

    new-array v13, v9, [Ljava/lang/Class;

    move-object/from16 v36, v14

    const/16 v9, 0x39

    aget-byte v14, v3, v9

    int-to-byte v9, v14

    move-object/from16 v37, v15

    const/16 v14, 0x20

    aget-byte v15, v3, v14

    int-to-byte v14, v15

    invoke-static {v9, v14, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    const/4 v14, 0x0

    aput-object v9, v13, v14

    invoke-virtual {v8, v13}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_d

    const/16 v8, 0x186

    :try_start_2a
    aget-byte v9, v3, v8

    int-to-byte v8, v9

    const/16 v9, 0x20

    aget-byte v13, v3, v9

    int-to-byte v9, v13

    invoke-static {v8, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/16 v9, 0xe

    aget-byte v11, v3, v9

    int-to-byte v9, v11

    const/16 v11, 0x151

    aget-byte v3, v3, v11

    neg-int v3, v3

    int-to-byte v3, v3

    const/16 v11, 0x112

    int-to-short v11, v11

    invoke-static {v9, v3, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v7, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_c

    move-object/from16 v9, v35

    move-object/from16 v14, v36

    move-object/from16 v15, v37

    :goto_29
    move-object/from16 v3, v29

    move-object/from16 v11, v31

    move-object/from16 v7, v32

    move-object/from16 v13, v33

    move-object/from16 v8, v34

    goto/16 :goto_1e

    :catchall_c
    move-exception v0

    move-object v2, v0

    :try_start_2b
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_2e

    throw v3

    :cond_2e
    throw v2

    :catchall_d
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_2f

    throw v3

    :cond_2f
    throw v2
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_7
    .catchall {:try_start_2b .. :try_end_2b} :catchall_14

    :catch_7
    move-exception v0

    move-object v2, v0

    .line 65
    :try_start_2c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v8, 0xe

    aget-byte v9, v7, v8

    int-to-byte v8, v9

    const/16 v9, 0x357

    aget-byte v11, v7, v9

    int-to-byte v9, v11

    const/4 v11, 0x2

    aget-byte v13, v7, v11

    int-to-short v11, v13

    invoke-static {v8, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v5, 0x6

    aget-byte v8, v7, v5

    int-to-byte v5, v8

    int-to-byte v8, v5

    or-int/lit16 v9, v8, 0x2d3

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_14

    const/4 v5, 0x2

    :try_start_2d
    new-array v8, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v2, v8, v5

    const/4 v2, 0x0

    aput-object v3, v8, v2

    const/4 v2, 0x4

    aget-byte v3, v7, v2

    neg-int v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x20

    aget-byte v5, v7, v3

    int-to-byte v3, v5

    const/16 v5, 0x99

    int-to-short v5, v5

    invoke-static {v2, v3, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v10, v5, v3

    const-class v3, Ljava/lang/Throwable;

    const/4 v7, 0x1

    aput-object v3, v5, v7

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_e

    :catchall_e
    move-exception v0

    move-object v2, v0

    :try_start_2e
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_30

    throw v3

    :cond_30
    throw v2

    :catchall_f
    move-exception v0

    move-object v2, v0

    .line 66
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_31

    throw v3

    :cond_31
    throw v2
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_14

    :cond_32
    move-object/from16 v34, v8

    move-object/from16 v35, v9

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v36, v14

    move-object/from16 v37, v15

    .line 67
    sget v8, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    and-int/lit8 v9, v8, 0x43

    or-int/lit8 v8, v8, 0x43

    add-int/2addr v9, v8

    rem-int/lit16 v8, v9, 0x80

    sput v8, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v8, 0x2

    rem-int/2addr v9, v8

    if-eqz v24, :cond_33

    const/16 v8, 0x8

    goto :goto_2a

    :cond_33
    const/16 v8, 0x19

    :goto_2a
    const/16 v9, 0x8

    if-eq v8, v9, :cond_34

    const/16 v8, 0xc

    .line 68
    :try_start_2f
    invoke-virtual {v2, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    or-int/lit16 v9, v8, 0x2000

    const/4 v11, 0x1

    shl-int/2addr v9, v11

    xor-int/lit16 v8, v8, 0x2000

    sub-int/2addr v9, v8

    int-to-char v8, v9

    .line 69
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2c

    :cond_34
    const/16 v8, 0x1a

    .line 70
    invoke-virtual {v2, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    .line 71
    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v9

    if-eqz v9, :cond_35

    add-int/lit8 v8, v8, 0x41

    goto :goto_2b

    :cond_35
    neg-int v8, v8

    neg-int v8, v8

    xor-int/lit8 v9, v8, 0x60

    and-int/lit8 v8, v8, 0x60

    const/4 v11, 0x1

    shl-int/2addr v8, v11

    add-int/2addr v8, v9

    :goto_2b
    int-to-char v8, v8

    .line 72
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2c
    and-int/lit8 v8, v7, 0x1d

    or-int/lit8 v7, v7, 0x1d

    add-int/2addr v8, v7

    and-int/lit8 v7, v8, -0x1c

    or-int/lit8 v8, v8, -0x1c

    add-int/2addr v7, v8

    move-object/from16 v11, v31

    move-object/from16 v13, v33

    move-object/from16 v8, v34

    move-object/from16 v9, v35

    move-object/from16 v14, v36

    move-object/from16 v15, v37

    goto/16 :goto_21

    :catchall_10
    move-exception v0

    goto :goto_2e

    :catchall_11
    move-exception v0

    goto :goto_2d

    :catchall_12
    move-exception v0

    move-object/from16 v29, v3

    :goto_2d
    move-object/from16 v32, v7

    :goto_2e
    move-object/from16 v31, v11

    move-object/from16 v33, v13

    goto :goto_2f

    :catchall_13
    move-exception v0

    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    move-object v2, v0

    .line 73
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_36

    throw v3

    :cond_36
    throw v2
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_14

    :catchall_14
    move-exception v0

    goto :goto_2f

    :catchall_15
    move-exception v0

    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    :goto_2f
    move-object v3, v0

    move/from16 v39, v4

    move/from16 v44, v6

    :goto_30
    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    :goto_31
    const/16 v16, -0x1

    goto/16 :goto_62

    :cond_37
    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    const/4 v8, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v30, 0x0

    :goto_32
    const/16 v2, 0x1e20

    :try_start_30
    new-array v2, v2, [B

    .line 74
    const-class v3, Lcom/appsflyer/internal/AFa1ySDK;

    sget-object v5, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v7, 0x2b6

    aget-byte v7, v5, v7

    int-to-byte v7, v7

    const/16 v9, 0x3f

    aget-byte v9, v5, v9

    int-to-byte v9, v9

    or-int/lit16 v11, v9, 0x108

    int-to-short v11, v11

    invoke-static {v7, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    .line 75
    invoke-virtual {v3, v7}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_5c

    .line 76
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    and-int/lit8 v9, v7, 0x75

    or-int/lit8 v7, v7, 0x75

    add-int/2addr v9, v7

    rem-int/lit16 v7, v9, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v7, 0x2

    rem-int/2addr v9, v7

    const/4 v7, 0x1

    :try_start_31
    new-array v9, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v9, v7

    const/16 v3, 0x19c

    .line 77
    aget-byte v7, v5, v3

    int-to-byte v3, v7

    const/16 v7, 0x20

    aget-byte v11, v5, v7

    int-to-byte v7, v11

    xor-int/lit16 v11, v7, 0x1a8

    and-int/lit16 v13, v7, 0x1a8

    or-int/2addr v11, v13

    int-to-short v11, v11

    invoke-static {v3, v7, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v7, 0x1

    new-array v11, v7, [Ljava/lang/Class;

    const/4 v7, 0x4

    aget-byte v13, v5, v7

    neg-int v7, v13

    int-to-byte v7, v7

    move-object/from16 v24, v8

    const/16 v13, 0x20

    aget-byte v8, v5, v13

    int-to-byte v8, v8

    move-object/from16 v34, v14

    const/4 v13, 0x6

    aget-byte v14, v5, v13

    int-to-short v13, v14

    invoke-static {v7, v8, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v11, v8

    invoke-virtual {v3, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_5b

    const/4 v7, 0x1

    :try_start_32
    new-array v9, v7, [Ljava/lang/Object;

    aput-object v2, v9, v8

    const/16 v7, 0x19c

    .line 78
    aget-byte v8, v5, v7

    int-to-byte v7, v8

    const/16 v8, 0x20

    aget-byte v11, v5, v8

    int-to-byte v8, v11

    xor-int/lit16 v11, v8, 0x1a8

    and-int/lit16 v13, v8, 0x1a8

    or-int/2addr v11, v13

    int-to-short v11, v11

    invoke-static {v7, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/16 v8, 0x13

    aget-byte v8, v5, v8

    int-to-byte v8, v8

    const/16 v11, 0x219

    aget-byte v11, v5, v11

    int-to-byte v11, v11

    const/16 v13, 0x320

    int-to-short v13, v13

    invoke-static {v8, v11, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v13, v11

    invoke-virtual {v7, v8, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    invoke-virtual {v7, v3, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_5a

    const/16 v7, 0x19c

    .line 79
    :try_start_33
    aget-byte v8, v5, v7

    int-to-byte v7, v8

    const/16 v8, 0x20

    aget-byte v9, v5, v8

    int-to-byte v8, v9

    xor-int/lit16 v9, v8, 0x1a8

    and-int/lit16 v11, v8, 0x1a8

    or-int/2addr v9, v11

    int-to-short v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/16 v8, 0xe

    aget-byte v9, v5, v8
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_59

    int-to-byte v8, v9

    const/16 v9, 0x151

    :try_start_34
    aget-byte v5, v5, v9
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_58

    neg-int v5, v5

    int-to-byte v5, v5

    const/16 v9, 0x112

    int-to-short v9, v9

    :try_start_35
    invoke-static {v8, v5, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v3, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_59

    const/16 v3, 0x10

    const/16 v5, 0x1dfa

    move-object/from16 v7, v28

    const/4 v8, 0x0

    :goto_33
    add-int/lit16 v9, v3, 0xf9

    and-int/lit16 v11, v3, 0x1e0f

    or-int/lit16 v13, v3, 0x1e0f

    add-int/2addr v11, v13

    .line 80
    :try_start_36
    aget-byte v11, v2, v11

    add-int/lit8 v11, v11, 0x1d

    int-to-byte v11, v11

    aput-byte v11, v2, v9

    .line 81
    array-length v9, v2
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_5c

    neg-int v11, v3

    and-int v13, v9, v11

    or-int/2addr v9, v11

    add-int/2addr v13, v9

    const/4 v9, 0x3

    :try_start_37
    new-array v11, v9, [Ljava/lang/Object;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v13, 0x2

    aput-object v9, v11, v13

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v13, 0x1

    aput-object v9, v11, v13

    const/4 v9, 0x0

    aput-object v2, v11, v9

    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v9, 0x29

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    const/16 v13, 0x20

    aget-byte v14, v2, v13

    int-to-byte v13, v14

    const/16 v14, 0x35e

    int-to-short v14, v14

    invoke-static {v9, v13, v14}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    const/4 v13, 0x3

    new-array v14, v13, [Ljava/lang/Class;

    const/4 v13, 0x0

    aput-object v1, v14, v13

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x1

    aput-object v13, v14, v25

    const/16 v18, 0x2

    aput-object v13, v14, v18

    invoke-virtual {v9, v14}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v36, v9

    check-cast v36, Ljava/io/InputStream;
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_56

    .line 82
    :try_start_38
    sget-object v9, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_5c

    if-nez v9, :cond_39

    const/4 v9, 0x0

    .line 83
    :try_start_39
    invoke-static {v9}, Landroid/graphics/Color;->green(I)I

    move-result v11
    :try_end_39
    .catchall {:try_start_39 .. :try_end_39} :catchall_19

    neg-int v11, v11

    neg-int v11, v11

    const v14, -0x8fffd3e

    and-int v26, v11, v14

    or-int/2addr v11, v14

    add-int v11, v26, v11

    move/from16 v42, v5

    const/4 v14, 0x2

    :try_start_3a
    new-array v5, v14, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/16 v25, 0x1

    aput-object v14, v5, v25

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v5, v9

    const/16 v9, 0xcd

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    const/16 v14, 0x3c

    aget-byte v14, v2, v14

    and-int/lit8 v35, v14, -0x1

    const/16 v16, -0x1

    or-int/lit8 v14, v14, -0x1

    add-int v14, v35, v14

    int-to-byte v14, v14

    move-object/from16 v43, v15

    const/16 v15, 0x1cd

    int-to-short v15, v15

    invoke-static {v9, v14, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    const/16 v14, 0x67

    aget-byte v14, v2, v14
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_17

    int-to-byte v14, v14

    move/from16 v44, v6

    const/16 v15, 0xd8

    :try_start_3b
    aget-byte v6, v2, v15

    int-to-byte v6, v6

    sget v15, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    or-int/lit16 v15, v15, 0x28c

    int-to-short v15, v15

    invoke-static {v14, v6, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v14, 0x2

    new-array v15, v14, [Ljava/lang/Class;

    const/4 v14, 0x0

    aput-object v13, v15, v14

    const/4 v14, 0x1

    aput-object v13, v15, v14

    invoke-virtual {v9, v6, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5
    :try_end_3b
    .catchall {:try_start_3b .. :try_end_3b} :catchall_16

    or-int/lit8 v6, v5, 0x5

    const/4 v9, 0x1

    shl-int/2addr v6, v9

    const/4 v9, 0x5

    xor-int/2addr v5, v9

    sub-int/2addr v6, v5

    int-to-short v5, v6

    const/4 v6, 0x2

    :try_start_3c
    new-array v9, v6, [I

    .line 84
    sget-wide v13, Lcom/appsflyer/internal/AFa1ySDK;->onAppOpenAttribution:J

    move-object/from16 v45, v7

    move-object v15, v8

    const/16 v6, 0x20

    ushr-long v7, v13, v6

    long-to-int v6, v7

    and-int v7, v6, v11

    not-int v7, v7

    or-int/2addr v6, v11

    and-int/2addr v6, v7

    const/4 v7, 0x0

    aput v6, v9, v7

    long-to-int v6, v13

    xor-int/2addr v6, v11

    const/4 v7, 0x1

    aput v6, v9, v7

    .line 85
    new-instance v6, Lcom/appsflyer/internal/AFg1cSDK;

    sget v38, Lcom/appsflyer/internal/AFa1ySDK;->onConversionDataFail:I

    sget-object v39, Lcom/appsflyer/internal/AFa1ySDK;->onAttributionFailure:[B

    sget v41, Lcom/appsflyer/internal/AFa1ySDK;->onResponse:I

    move-object/from16 v35, v6

    move-object/from16 v37, v9

    move/from16 v40, v5

    invoke-direct/range {v35 .. v41}, Lcom/appsflyer/internal/AFg1cSDK;-><init>(Ljava/io/InputStream;[II[BII)V

    move/from16 v36, v3

    move-object/from16 v35, v15

    goto/16 :goto_36

    :catchall_16
    move-exception v0

    goto :goto_34

    :catchall_17
    move-exception v0

    move/from16 v44, v6

    :goto_34
    move-object v2, v0

    .line 86
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_38

    throw v3

    :cond_38
    throw v2
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_18

    :catchall_18
    move-exception v0

    goto :goto_35

    :catchall_19
    move-exception v0

    move/from16 v44, v6

    :goto_35
    move-object v3, v0

    move/from16 v39, v4

    goto/16 :goto_30

    :cond_39
    move/from16 v42, v5

    move/from16 v44, v6

    move-object/from16 v45, v7

    move-object/from16 v43, v15

    move-object v15, v8

    .line 87
    :try_start_3d
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollBarSize()I

    move-result v5
    :try_end_3d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_55

    shr-int/lit8 v5, v5, 0x8

    neg-int v5, v5

    not-int v5, v5

    const v6, 0xb6e6697

    sub-int/2addr v6, v5

    const/4 v5, 0x1

    sub-int/2addr v6, v5

    const/16 v5, 0x1c

    :try_start_3e
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v7, 0x3c

    aget-byte v7, v2, v7

    xor-int/lit8 v8, v7, -0x1

    const/4 v11, -0x1

    and-int/2addr v7, v11

    const/4 v11, 0x1

    shl-int/2addr v7, v11

    add-int/2addr v8, v7

    int-to-byte v7, v8

    const/16 v8, 0x198

    int-to-short v8, v8

    invoke-static {v5, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v7, 0x19c

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    const/16 v8, 0x151

    aget-byte v11, v2, v8

    neg-int v8, v11

    int-to-byte v8, v8

    const/16 v11, 0x3d7

    int-to-short v11, v11

    invoke-static {v7, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v8, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_54

    const-wide/16 v37, -0x1

    cmp-long v5, v7, v37

    neg-int v5, v5

    or-int/lit8 v7, v5, 0x8

    const/4 v8, 0x1

    shl-int/2addr v7, v8

    xor-int/lit8 v5, v5, 0x8

    sub-int/2addr v7, v5

    int-to-short v5, v7

    const/4 v7, 0x3

    :try_start_3f
    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    const/4 v7, 0x2

    aput-object v5, v8, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v8, v6

    const/4 v5, 0x0

    aput-object v36, v8, v5

    const/16 v5, 0xa

    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x151

    aget-byte v7, v2, v6

    neg-int v6, v7

    int-to-byte v6, v6

    const/16 v7, 0x318

    int-to-short v7, v7

    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    check-cast v6, Ljava/lang/ClassLoader;

    const/4 v7, 0x1

    invoke-static {v5, v7, v6}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0x59

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    xor-int/lit8 v7, v6, 0x48

    and-int/lit8 v11, v6, 0x48

    or-int/2addr v7, v11

    int-to-byte v7, v7

    const/16 v11, 0x19d

    int-to-short v11, v11

    invoke-static {v6, v7, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    new-array v11, v7, [Ljava/lang/Class;

    const/4 v7, 0x4

    aget-byte v14, v2, v7

    neg-int v7, v14

    int-to-byte v7, v7

    move-object/from16 v35, v15

    const/16 v14, 0x20

    aget-byte v15, v2, v14

    int-to-byte v14, v15

    move/from16 v36, v3

    const/4 v15, 0x6

    aget-byte v3, v2, v15

    int-to-short v3, v3

    invoke-static {v7, v14, v3}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v7, 0x0

    aput-object v3, v11, v7

    const/4 v3, 0x1

    aput-object v13, v11, v3

    sget-object v3, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x2

    aput-object v3, v11, v7

    invoke-virtual {v5, v6, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v9, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/io/InputStream;
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_53

    :goto_36
    const/16 v3, 0x14

    int-to-long v7, v3

    const/4 v3, 0x1

    :try_start_40
    new-array v5, v3, [Ljava/lang/Object;

    .line 88
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v7, 0x0

    aput-object v3, v5, v7

    const/4 v3, 0x4

    aget-byte v7, v2, v3

    neg-int v3, v7

    int-to-byte v3, v3

    const/16 v7, 0x20

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    const/4 v8, 0x6

    aget-byte v9, v2, v8

    int-to-short v8, v9

    invoke-static {v3, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v7, 0x5

    aget-byte v8, v2, v7

    int-to-byte v7, v8

    const/16 v8, 0x199

    aget-byte v8, v2, v8

    const/4 v9, 0x1

    sub-int/2addr v8, v9

    int-to-byte v8, v8

    const/16 v11, 0x183

    int-to-short v11, v11

    invoke-static {v7, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    new-array v8, v9, [Ljava/lang/Class;

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v9, v8, v11

    invoke-virtual {v3, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_52

    if-eqz v23, :cond_56

    .line 89
    :try_start_41
    sget-object v5, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;
    :try_end_41
    .catchall {:try_start_41 .. :try_end_41} :catchall_34

    if-nez v5, :cond_3a

    const/4 v7, 0x0

    goto :goto_37

    :cond_3a
    const/4 v7, 0x1

    :goto_37
    if-eqz v7, :cond_3b

    move-object/from16 v7, v34

    goto :goto_38

    :cond_3b
    move-object/from16 v7, v24

    :goto_38
    if-nez v5, :cond_3c

    move-object/from16 v5, v43

    goto :goto_39

    :cond_3c
    move-object/from16 v5, v30

    :goto_39
    const/4 v8, 0x1

    :try_start_42
    new-array v9, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v7, v9, v8

    const/16 v8, 0x186

    .line 90
    aget-byte v11, v2, v8

    int-to-byte v8, v11

    const/16 v11, 0x20

    aget-byte v13, v2, v11

    int-to-byte v11, v13

    const/16 v13, 0xb7

    int-to-short v13, v13

    invoke-static {v8, v11, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/4 v11, 0x1

    new-array v14, v11, [Ljava/lang/Class;

    const/16 v11, 0x39

    aget-byte v15, v2, v11

    int-to-byte v11, v15

    const/16 v15, 0x20

    aget-byte v3, v2, v15

    int-to-byte v3, v3

    invoke-static {v11, v3, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v11, 0x0

    aput-object v3, v14, v11

    invoke-virtual {v8, v14}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_2d

    if-eqz v4, :cond_3f

    .line 91
    sget v8, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    or-int/lit8 v9, v8, 0x75

    const/4 v11, 0x1

    shl-int/2addr v9, v11

    xor-int/lit8 v8, v8, 0x75

    sub-int/2addr v9, v8

    rem-int/lit16 v8, v9, 0x80

    sput v8, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v8, 0x2

    rem-int/2addr v9, v8

    if-eqz v9, :cond_3e

    const/16 v8, 0x39

    .line 92
    :try_start_43
    aget-byte v9, v2, v8

    int-to-byte v8, v9

    const/16 v9, 0x20

    aget-byte v11, v2, v9

    int-to-byte v9, v11

    invoke-static {v8, v9, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const/16 v9, 0x67

    aget-byte v2, v2, v9

    int-to-byte v2, v2

    or-int/lit8 v9, v2, 0x40

    int-to-byte v9, v9

    const/16 v11, 0x20c

    int-to-short v11, v11

    invoke-static {v2, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x0

    invoke-virtual {v8, v2, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v7, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_1a

    goto :goto_3b

    :catchall_1a
    move-exception v0

    move-object v2, v0

    :try_start_44
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_3d

    throw v3

    :cond_3d
    throw v2

    :cond_3e
    const/4 v2, 0x0

    throw v2
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_44} :catch_8
    .catchall {:try_start_44 .. :try_end_44} :catchall_1b

    :catchall_1b
    move-exception v0

    move-object v2, v0

    move/from16 v39, v4

    :goto_3a
    move-object v6, v5

    goto/16 :goto_46

    :catch_8
    move-exception v0

    move-object v2, v0

    move/from16 v39, v4

    move-object v6, v5

    goto/16 :goto_45

    .line 93
    :cond_3f
    :goto_3b
    sget v2, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    xor-int/lit8 v8, v2, 0x73

    and-int/lit8 v2, v2, 0x73

    const/4 v9, 0x1

    shl-int/2addr v2, v9

    add-int/2addr v8, v2

    rem-int/lit16 v2, v8, 0x80

    sput v2, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v2, 0x2

    rem-int/2addr v8, v2

    const/16 v2, 0x400

    :try_start_45
    new-array v8, v2, [B
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_2c

    move/from16 v9, v42

    :goto_3c
    if-lez v9, :cond_42

    .line 94
    :try_start_46
    invoke-static {v2, v9}, Ljava/lang/Math;->min(II)I

    move-result v11
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_21

    const/4 v14, 0x3

    :try_start_47
    new-array v15, v14, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v14, 0x2

    aput-object v11, v15, v14

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/16 v25, 0x1

    aput-object v14, v15, v25

    aput-object v8, v15, v11

    sget-object v11, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/4 v14, 0x4

    aget-byte v2, v11, v14
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_1f

    neg-int v2, v2

    int-to-byte v2, v2

    move/from16 v39, v4

    const/16 v14, 0x20

    :try_start_48
    aget-byte v4, v11, v14
    :try_end_48
    .catchall {:try_start_48 .. :try_end_48} :catchall_1e

    int-to-byte v4, v4

    move-object/from16 v40, v5

    const/4 v14, 0x6

    :try_start_49
    aget-byte v5, v11, v14

    int-to-short v5, v5

    invoke-static {v2, v4, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v4, 0x5

    aget-byte v5, v11, v4

    int-to-byte v4, v5

    const/16 v5, 0x219

    aget-byte v5, v11, v5

    int-to-byte v5, v5

    const/16 v14, 0x2d6

    int-to-short v14, v14

    invoke-static {v4, v5, v14}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v14, v5, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v1, v14, v5

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x1

    aput-object v5, v14, v25

    const/16 v18, 0x2

    aput-object v5, v14, v18

    invoke-virtual {v2, v4, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v6, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2
    :try_end_49
    .catchall {:try_start_49 .. :try_end_49} :catchall_1d

    const/4 v4, -0x1

    if-eq v2, v4, :cond_43

    const/4 v4, 0x3

    :try_start_4a
    new-array v14, v4, [Ljava/lang/Object;

    .line 95
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v15, 0x2

    aput-object v4, v14, v15

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    const/16 v25, 0x1

    aput-object v15, v14, v25

    aput-object v8, v14, v4

    const/16 v4, 0x186

    aget-byte v15, v11, v4

    int-to-byte v4, v15

    move-object/from16 v41, v8

    const/16 v15, 0x20

    aget-byte v8, v11, v15

    int-to-byte v8, v8

    invoke-static {v4, v8, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v8, 0xe

    aget-byte v11, v11, v8

    int-to-byte v8, v11

    or-int/lit8 v11, v8, 0x4a

    int-to-byte v11, v11

    const/16 v15, 0x2e4

    int-to-short v15, v15

    invoke-static {v8, v11, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x3

    new-array v15, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v15, v11

    const/4 v11, 0x1

    aput-object v5, v15, v11

    const/4 v11, 0x2

    aput-object v5, v15, v11

    invoke-virtual {v4, v8, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v3, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4a
    .catchall {:try_start_4a .. :try_end_4a} :catchall_1c

    sub-int/2addr v9, v2

    move/from16 v4, v39

    move-object/from16 v5, v40

    move-object/from16 v8, v41

    const/16 v2, 0x400

    goto/16 :goto_3c

    :catchall_1c
    move-exception v0

    move-object v2, v0

    :try_start_4b
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_40

    throw v3

    :cond_40
    throw v2

    :catchall_1d
    move-exception v0

    goto :goto_3e

    :catchall_1e
    move-exception v0

    goto :goto_3d

    :catchall_1f
    move-exception v0

    move/from16 v39, v4

    :goto_3d
    move-object/from16 v40, v5

    :goto_3e
    move-object v2, v0

    .line 96
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_41

    throw v3

    :cond_41
    throw v2
    :try_end_4b
    .catchall {:try_start_4b .. :try_end_4b} :catchall_20

    :catchall_20
    move-exception v0

    move-object v2, v0

    move-object/from16 v6, v40

    goto/16 :goto_46

    :catchall_21
    move-exception v0

    move/from16 v39, v4

    move-object v2, v0

    goto/16 :goto_3a

    :cond_42
    move/from16 v39, v4

    move-object/from16 v40, v5

    .line 97
    :cond_43
    :try_start_4c
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v4, 0x186

    aget-byte v5, v2, v4

    int-to-byte v4, v5

    const/16 v5, 0x20

    aget-byte v6, v2, v5

    int-to-byte v5, v6

    invoke-static {v4, v5, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0xe

    aget-byte v6, v2, v5

    int-to-byte v5, v6

    const/16 v6, 0xd8

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    const/16 v8, 0x144

    int-to-short v8, v8

    invoke-static {v5, v6, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v3, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_4c
    .catchall {:try_start_4c .. :try_end_4c} :catchall_2b

    const/16 v5, 0x1c

    :try_start_4d
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x20

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    or-int/lit8 v8, v6, 0xc

    int-to-short v8, v8

    invoke-static {v5, v6, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x5

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    const/16 v8, 0x199

    aget-byte v8, v2, v8

    or-int/lit8 v9, v8, -0x1

    const/4 v11, 0x1

    shl-int/2addr v9, v11

    const/4 v11, -0x1

    xor-int/2addr v8, v11

    sub-int/2addr v9, v8

    int-to-byte v8, v9

    const/16 v9, 0x269

    int-to-short v9, v9

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4d
    .catchall {:try_start_4d .. :try_end_4d} :catchall_2a

    const/16 v4, 0x186

    .line 98
    :try_start_4e
    aget-byte v5, v2, v4

    int-to-byte v4, v5

    const/16 v5, 0x20

    aget-byte v6, v2, v5

    int-to-byte v5, v6

    invoke-static {v4, v5, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0xe

    aget-byte v6, v2, v5

    int-to-byte v5, v6

    const/16 v6, 0x151

    aget-byte v8, v2, v6

    neg-int v6, v8

    int-to-byte v6, v6

    const/16 v8, 0x112

    int-to-short v8, v8

    invoke-static {v5, v6, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v3, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4e
    .catchall {:try_start_4e .. :try_end_4e} :catchall_29

    const/16 v3, 0xcd

    .line 99
    :try_start_4f
    aget-byte v3, v2, v3

    int-to-byte v3, v3

    const/16 v4, 0x1e

    aget-byte v5, v2, v4

    neg-int v4, v5

    int-to-byte v4, v4

    const/16 v5, 0x2d3

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/16 v4, 0x3f

    .line 100
    aget-byte v4, v2, v4

    int-to-byte v4, v4

    const/16 v5, 0x150

    aget-byte v5, v2, v5

    int-to-byte v5, v5

    or-int/lit16 v6, v5, 0x190

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v6, v5, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v10, v6, v5

    const/4 v5, 0x1

    aput-object v10, v6, v5

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x2

    aput-object v5, v6, v8

    invoke-virtual {v3, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x3

    new-array v5, v4, [Ljava/lang/Object;
    :try_end_4f
    .catchall {:try_start_4f .. :try_end_4f} :catchall_28

    .line 101
    sget v4, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    add-int/lit8 v4, v4, 0x52

    const/4 v6, 0x1

    sub-int/2addr v4, v6

    rem-int/lit16 v6, v4, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v6, 0x2

    rem-int/2addr v4, v6

    const/16 v4, 0x39

    .line 102
    :try_start_50
    aget-byte v6, v2, v4

    int-to-byte v4, v6

    const/16 v6, 0x20

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    invoke-static {v4, v6, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v6, 0xb7

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v8, 0xd8

    aget-byte v9, v2, v8

    int-to-byte v8, v9

    const/16 v9, 0xde

    int-to-short v9, v9

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_50
    .catchall {:try_start_50 .. :try_end_50} :catchall_27

    const/4 v6, 0x0

    :try_start_51
    aput-object v4, v5, v6
    :try_end_51
    .catchall {:try_start_51 .. :try_end_51} :catchall_28

    const/16 v4, 0x39

    :try_start_52
    aget-byte v6, v2, v4

    int-to-byte v4, v6

    const/16 v6, 0x20

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    invoke-static {v4, v6, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v6, 0xb7

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v8, 0xd8

    aget-byte v11, v2, v8

    int-to-byte v8, v11

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4
    :try_end_52
    .catchall {:try_start_52 .. :try_end_52} :catchall_26

    move-object/from16 v6, v40

    :try_start_53
    invoke-virtual {v4, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_53
    .catchall {:try_start_53 .. :try_end_53} :catchall_25

    const/4 v9, 0x1

    :try_start_54
    aput-object v4, v5, v9

    const/4 v4, 0x0

    .line 103
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v4, 0x2

    aput-object v9, v5, v4

    .line 104
    invoke-virtual {v3, v8, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_54
    .catchall {:try_start_54 .. :try_end_54} :catchall_2e

    const/16 v4, 0x39

    .line 105
    :try_start_55
    aget-byte v5, v2, v4

    int-to-byte v4, v5

    const/16 v5, 0x20

    aget-byte v8, v2, v5

    int-to-byte v5, v8

    invoke-static {v4, v5, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x59

    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v8, 0x1e

    aget-byte v9, v2, v8

    neg-int v8, v9

    int-to-byte v8, v8

    const/16 v9, 0x169

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_55
    .catchall {:try_start_55 .. :try_end_55} :catchall_24

    const/16 v4, 0x39

    .line 106
    :try_start_56
    aget-byte v5, v2, v4

    int-to-byte v4, v5

    const/16 v5, 0x20

    aget-byte v7, v2, v5

    int-to-byte v5, v7

    invoke-static {v4, v5, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x59

    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v7, 0x1e

    aget-byte v8, v2, v7

    neg-int v7, v8

    int-to-byte v7, v7

    invoke-static {v5, v7, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_56
    .catchall {:try_start_56 .. :try_end_56} :catchall_23

    .line 107
    :try_start_57
    sget-object v4, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;
    :try_end_57
    .catchall {:try_start_57 .. :try_end_57} :catchall_33

    if-nez v4, :cond_44

    const/4 v4, 0x0

    goto :goto_3f

    :cond_44
    const/4 v4, 0x1

    :goto_3f
    const/4 v5, 0x1

    if-eq v4, v5, :cond_4a

    .line 108
    sget v4, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    add-int/lit8 v5, v4, 0x63

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v6, 0x2

    rem-int/2addr v5, v6

    if-nez v5, :cond_45

    const/16 v5, 0x55

    goto :goto_40

    :cond_45
    const/16 v5, 0x24

    :goto_40
    const/16 v6, 0x24

    if-ne v5, v6, :cond_49

    .line 109
    :try_start_58
    const-class v5, Lcom/appsflyer/internal/AFa1ySDK;
    :try_end_58
    .catchall {:try_start_58 .. :try_end_58} :catchall_33

    add-int/lit8 v4, v4, 0x64

    const/4 v6, 0x1

    sub-int/2addr v4, v6

    .line 110
    rem-int/lit16 v6, v4, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v6, 0x2

    rem-int/2addr v4, v6

    if-nez v4, :cond_46

    const/4 v4, 0x1

    goto :goto_41

    :cond_46
    const/4 v4, 0x0

    :goto_41
    if-eqz v4, :cond_47

    :try_start_59
    const-class v4, Ljava/lang/Class;

    const/16 v6, 0x4c

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0x7180

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    const/16 v7, 0x538b

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_59
    .catchall {:try_start_59 .. :try_end_59} :catchall_22

    :try_start_5a
    sput-object v2, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;
    :try_end_5a
    .catchall {:try_start_5a .. :try_end_5a} :catchall_33

    goto :goto_42

    .line 111
    :cond_47
    :try_start_5b
    const-class v4, Ljava/lang/Class;

    const/4 v6, 0x7

    aget-byte v7, v2, v6

    int-to-byte v6, v7

    const/16 v7, 0xd8

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    const/16 v7, 0x180

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_5b
    .catchall {:try_start_5b .. :try_end_5b} :catchall_22

    :try_start_5c
    sput-object v2, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    goto :goto_42

    :catchall_22
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_48

    throw v3

    :cond_48
    throw v2

    :cond_49
    const/4 v2, 0x0

    throw v2

    :cond_4a
    :goto_42
    const/4 v14, 0x3

    const/16 v16, -0x1

    goto/16 :goto_51

    :catchall_23
    move-exception v0

    move-object v2, v0

    .line 112
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4b

    throw v3

    :cond_4b
    throw v2

    :catchall_24
    move-exception v0

    move-object v2, v0

    .line 113
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4c

    throw v3

    :cond_4c
    throw v2
    :try_end_5c
    .catchall {:try_start_5c .. :try_end_5c} :catchall_33

    :catchall_25
    move-exception v0

    goto :goto_43

    :catchall_26
    move-exception v0

    move-object/from16 v6, v40

    :goto_43
    move-object v2, v0

    .line 114
    :try_start_5d
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4d

    throw v3

    :cond_4d
    throw v2

    :catchall_27
    move-exception v0

    move-object/from16 v6, v40

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4e

    throw v3

    :cond_4e
    throw v2

    :catchall_28
    move-exception v0

    move-object/from16 v6, v40

    goto :goto_44

    :catchall_29
    move-exception v0

    move-object/from16 v6, v40

    move-object v2, v0

    .line 115
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4f

    throw v3

    :cond_4f
    throw v2

    :catchall_2a
    move-exception v0

    move-object/from16 v6, v40

    move-object v2, v0

    .line 116
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_50

    throw v3

    :cond_50
    throw v2

    :catchall_2b
    move-exception v0

    move-object/from16 v6, v40

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_51

    throw v3

    :cond_51
    throw v2
    :try_end_5d
    .catchall {:try_start_5d .. :try_end_5d} :catchall_2e

    :catchall_2c
    move-exception v0

    move/from16 v39, v4

    move-object v6, v5

    goto :goto_44

    :catchall_2d
    move-exception v0

    move/from16 v39, v4

    move-object v6, v5

    move-object v2, v0

    .line 117
    :try_start_5e
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_52

    throw v3

    :cond_52
    throw v2
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_5e} :catch_9
    .catchall {:try_start_5e .. :try_end_5e} :catchall_2e

    :catchall_2e
    move-exception v0

    :goto_44
    move-object v2, v0

    goto :goto_46

    :catch_9
    move-exception v0

    move-object v2, v0

    .line 118
    :goto_45
    :try_start_5f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v5, 0xe

    aget-byte v8, v4, v5

    int-to-byte v5, v8

    const/16 v8, 0x357

    aget-byte v9, v4, v8

    int-to-byte v8, v9

    xor-int/lit16 v9, v8, 0x221

    and-int/lit16 v11, v8, 0x221

    or-int/2addr v9, v11

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v5, 0x6

    aget-byte v8, v4, v5

    int-to-byte v5, v8

    int-to-byte v8, v5

    xor-int/lit16 v9, v8, 0x2d3

    and-int/lit16 v11, v8, 0x2d3

    or-int/2addr v9, v11

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_5f
    .catchall {:try_start_5f .. :try_end_5f} :catchall_2e

    const/4 v5, 0x2

    :try_start_60
    new-array v8, v5, [Ljava/lang/Object;

    const/4 v5, 0x1

    aput-object v2, v8, v5

    const/4 v2, 0x0

    aput-object v3, v8, v2

    const/4 v2, 0x4

    aget-byte v3, v4, v2

    neg-int v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x20

    aget-byte v4, v4, v3

    int-to-byte v3, v4

    const/16 v4, 0x99

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v10, v4, v3

    const-class v3, Ljava/lang/Throwable;

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    throw v2
    :try_end_60
    .catchall {:try_start_60 .. :try_end_60} :catchall_2f

    :catchall_2f
    move-exception v0

    move-object v2, v0

    :try_start_61
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_53

    throw v3

    :cond_53
    throw v2
    :try_end_61
    .catchall {:try_start_61 .. :try_end_61} :catchall_2e

    .line 119
    :goto_46
    :try_start_62
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B
    :try_end_62
    .catchall {:try_start_62 .. :try_end_62} :catchall_32

    const/16 v4, 0x39

    :try_start_63
    aget-byte v5, v3, v4
    :try_end_63
    .catchall {:try_start_63 .. :try_end_63} :catchall_31

    int-to-byte v4, v5

    const/16 v5, 0x20

    :try_start_64
    aget-byte v8, v3, v5

    int-to-byte v5, v8

    invoke-static {v4, v5, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x59

    aget-byte v5, v3, v5

    int-to-byte v5, v5

    const/16 v8, 0x1e

    aget-byte v9, v3, v8

    neg-int v8, v9

    int-to-byte v8, v8

    const/16 v9, 0x169

    int-to-short v9, v9

    invoke-static {v5, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_64
    .catchall {:try_start_64 .. :try_end_64} :catchall_32

    const/16 v4, 0x39

    .line 120
    :try_start_65
    aget-byte v5, v3, v4

    int-to-byte v5, v5

    const/16 v7, 0x20

    aget-byte v8, v3, v7

    int-to-byte v7, v8

    invoke-static {v5, v7, v12}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v7, 0x59

    aget-byte v7, v3, v7

    int-to-byte v7, v7

    const/16 v8, 0x1e

    aget-byte v3, v3, v8

    neg-int v3, v3

    int-to-byte v3, v3

    invoke-static {v7, v3, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_65
    .catchall {:try_start_65 .. :try_end_65} :catchall_30

    .line 121
    :try_start_66
    throw v2

    :catchall_30
    move-exception v0

    move-object v2, v0

    .line 122
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_54

    throw v3

    :cond_54
    throw v2

    :catchall_31
    move-exception v0

    goto :goto_47

    :catchall_32
    move-exception v0

    const/16 v4, 0x39

    :goto_47
    move-object v2, v0

    .line 123
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_55

    throw v3

    :cond_55
    throw v2
    :try_end_66
    .catchall {:try_start_66 .. :try_end_66} :catchall_33

    :catchall_33
    move-exception v0

    goto :goto_48

    :catchall_34
    move-exception v0

    move/from16 v39, v4

    :goto_48
    move-object v3, v0

    goto/16 :goto_30

    :cond_56
    move/from16 v39, v4

    const/16 v4, 0x39

    .line 124
    :try_start_67
    new-instance v3, Ljava/util/zip/ZipInputStream;

    invoke-direct {v3, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 125
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5
    :try_end_67
    .catchall {:try_start_67 .. :try_end_67} :catchall_51

    const/4 v6, 0x1

    :try_start_68
    new-array v7, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v7, v6

    const/16 v3, 0x21b

    .line 126
    aget-byte v3, v2, v3

    neg-int v3, v3

    int-to-byte v3, v3

    const/16 v6, 0x20

    aget-byte v8, v2, v6

    int-to-byte v6, v8

    xor-int/lit16 v8, v6, 0x380

    and-int/lit16 v9, v6, 0x380

    or-int/2addr v8, v9

    int-to-short v8, v8

    invoke-static {v3, v6, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/Class;

    const/4 v6, 0x4

    aget-byte v9, v2, v6

    neg-int v6, v9

    int-to-byte v6, v6

    const/16 v9, 0x20

    aget-byte v11, v2, v9

    int-to-byte v9, v11

    const/4 v11, 0x6

    aget-byte v13, v2, v11

    int-to-short v11, v13

    invoke-static {v6, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_68
    .catchall {:try_start_68 .. :try_end_68} :catchall_50

    .line 127
    sget v6, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    add-int/lit8 v6, v6, 0x5b

    rem-int/lit16 v7, v6, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    const/4 v7, 0x2

    rem-int/2addr v6, v7

    if-nez v6, :cond_57

    const/16 v6, 0x66a3

    :try_start_69
    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0x60

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    const/16 v7, 0x2691

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_69
    .catchall {:try_start_69 .. :try_end_69} :catchall_35

    const/16 v6, 0x6545

    :try_start_6a
    new-array v6, v6, [B
    :try_end_6a
    .catchall {:try_start_6a .. :try_end_6a} :catchall_33

    const/4 v7, 0x1

    goto :goto_49

    :catchall_35
    move-exception v0

    move-object v3, v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    goto/16 :goto_5d

    :cond_57
    const/16 v6, 0x357

    .line 128
    :try_start_6b
    aget-byte v7, v2, v6

    int-to-byte v6, v7

    const/16 v7, 0x20

    aget-byte v2, v2, v7

    int-to-byte v2, v2

    const/16 v7, 0x1b9

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_6b
    .catchall {:try_start_6b .. :try_end_6b} :catchall_4f

    const/16 v6, 0x400

    :try_start_6c
    new-array v6, v6, [B
    :try_end_6c
    .catchall {:try_start_6c .. :try_end_6c} :catchall_51

    const/4 v7, 0x0

    :goto_49
    const/4 v8, 0x1

    :try_start_6d
    new-array v9, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v9, v8

    .line 129
    sget-object v8, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v11, 0x21b

    aget-byte v11, v8, v11

    neg-int v11, v11

    int-to-byte v11, v11

    const/16 v13, 0x20

    aget-byte v14, v8, v13

    int-to-byte v13, v14

    or-int/lit16 v14, v13, 0x380

    int-to-short v14, v14

    invoke-static {v11, v13, v14}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    const/4 v13, 0x5

    aget-byte v14, v8, v13

    int-to-byte v13, v14

    const/16 v14, 0x219

    aget-byte v14, v8, v14

    int-to-byte v14, v14

    const/16 v15, 0x2d6

    int-to-short v15, v15

    invoke-static {v13, v14, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    new-array v15, v14, [Ljava/lang/Class;

    const/4 v14, 0x0

    aput-object v1, v15, v14

    invoke-virtual {v11, v13, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    invoke-virtual {v11, v3, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9
    :try_end_6d
    .catchall {:try_start_6d .. :try_end_6d} :catchall_4e

    if-lez v9, :cond_58

    const/16 v11, 0x60

    goto :goto_4a

    :cond_58
    const/16 v11, 0x16

    :goto_4a
    const/16 v13, 0x60

    if-eq v11, v13, :cond_5a

    :cond_59
    const/4 v14, 0x3

    goto/16 :goto_4c

    :cond_5a
    int-to-long v13, v7

    .line 130
    :try_start_6e
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v40
    :try_end_6e
    .catchall {:try_start_6e .. :try_end_6e} :catchall_51

    cmp-long v11, v13, v40

    if-gez v11, :cond_59

    const/4 v11, 0x3

    :try_start_6f
    new-array v13, v11, [Ljava/lang/Object;

    .line 131
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v14, 0x2

    aput-object v11, v13, v14

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/4 v15, 0x1

    aput-object v14, v13, v15

    aput-object v6, v13, v11

    const/16 v11, 0x357

    aget-byte v14, v8, v11

    int-to-byte v11, v14

    const/16 v14, 0x20

    aget-byte v15, v8, v14

    int-to-byte v14, v15

    const/16 v15, 0x1b9

    int-to-short v15, v15

    invoke-static {v11, v14, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    const/16 v14, 0xe

    aget-byte v8, v8, v14

    int-to-byte v8, v8

    or-int/lit8 v14, v8, 0x4a

    int-to-byte v14, v14

    const/16 v15, 0x2e4

    int-to-short v15, v15

    invoke-static {v8, v14, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8
    :try_end_6f
    .catchall {:try_start_6f .. :try_end_6f} :catchall_37

    const/4 v14, 0x3

    :try_start_70
    new-array v15, v14, [Ljava/lang/Class;

    const/16 v19, 0x0

    aput-object v1, v15, v19

    sget-object v19, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v20, 0x1

    aput-object v19, v15, v20

    const/16 v18, 0x2

    aput-object v19, v15, v18

    invoke-virtual {v11, v8, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    invoke-virtual {v8, v2, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_70
    .catchall {:try_start_70 .. :try_end_70} :catchall_36

    neg-int v8, v9

    neg-int v8, v8

    and-int v9, v7, v8

    or-int/2addr v7, v8

    add-int/2addr v7, v9

    goto/16 :goto_49

    :catchall_36
    move-exception v0

    goto :goto_4b

    :catchall_37
    move-exception v0

    const/4 v14, 0x3

    :goto_4b
    move-object v2, v0

    :try_start_71
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_5b

    throw v3

    :cond_5b
    throw v2
    :try_end_71
    .catchall {:try_start_71 .. :try_end_71} :catchall_38

    :catchall_38
    move-exception v0

    move-object v3, v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    goto/16 :goto_31

    :goto_4c
    const/16 v5, 0x357

    .line 132
    :try_start_72
    aget-byte v6, v8, v5

    int-to-byte v5, v6

    const/16 v6, 0x20

    aget-byte v7, v8, v6

    int-to-byte v6, v7

    const/16 v7, 0x1b9

    int-to-short v7, v7

    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0x67

    aget-byte v6, v8, v6

    int-to-byte v6, v6

    const/16 v9, 0x199

    aget-byte v9, v8, v9

    int-to-byte v9, v9

    const/16 v11, 0x3c

    aget-byte v11, v8, v11

    and-int/lit8 v13, v11, -0x1

    const/4 v15, -0x1

    or-int/2addr v11, v15

    add-int/2addr v13, v11

    int-to-short v11, v13

    invoke-static {v6, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_72
    .catchall {:try_start_72 .. :try_end_72} :catchall_4d

    const/16 v6, 0x21b

    .line 133
    :try_start_73
    aget-byte v6, v8, v6

    neg-int v6, v6

    int-to-byte v6, v6

    const/16 v9, 0x20

    aget-byte v11, v8, v9

    int-to-byte v9, v11

    xor-int/lit16 v11, v9, 0x380

    and-int/lit16 v13, v9, 0x380

    or-int/2addr v11, v13

    int-to-short v11, v11

    invoke-static {v6, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/16 v9, 0xe

    aget-byte v11, v8, v9

    int-to-byte v9, v11

    const/16 v11, 0x151

    aget-byte v8, v8, v11

    neg-int v8, v8

    int-to-byte v8, v8

    const/16 v11, 0x112

    int-to-short v11, v11

    invoke-static {v9, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    invoke-virtual {v6, v3, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_73
    .catchall {:try_start_73 .. :try_end_73} :catchall_39

    goto :goto_4d

    :catchall_39
    move-exception v0

    move-object v3, v0

    :try_start_74
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    if-eqz v6, :cond_5c

    throw v6

    :cond_5c
    throw v3
    :try_end_74
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_74} :catch_a
    .catchall {:try_start_74 .. :try_end_74} :catchall_38

    .line 134
    :catch_a
    :goto_4d
    sget v3, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    xor-int/lit8 v6, v3, 0x31

    and-int/lit8 v3, v3, 0x31

    const/4 v8, 0x1

    shl-int/2addr v3, v8

    add-int/2addr v6, v3

    rem-int/lit16 v3, v6, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v3, 0x2

    rem-int/2addr v6, v3

    .line 135
    :try_start_75
    sget-object v3, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v6, 0x357

    aget-byte v8, v3, v6

    int-to-byte v6, v8

    const/16 v8, 0x20

    aget-byte v9, v3, v8

    int-to-byte v8, v9

    invoke-static {v6, v8, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/16 v7, 0xe

    aget-byte v8, v3, v7

    int-to-byte v7, v8

    const/16 v8, 0x151

    aget-byte v3, v3, v8

    neg-int v3, v3

    int-to-byte v3, v3

    const/16 v8, 0x112

    int-to-short v8, v8

    invoke-static {v7, v3, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v2, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_75
    .catchall {:try_start_75 .. :try_end_75} :catchall_3a

    goto :goto_4e

    :catchall_3a
    move-exception v0

    move-object v2, v0

    :try_start_76
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_5d

    throw v3

    :cond_5d
    throw v2
    :try_end_76
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_76} :catch_b
    .catchall {:try_start_76 .. :try_end_76} :catchall_38

    .line 136
    :catch_b
    :goto_4e
    :try_start_77
    const-class v2, Lcom/appsflyer/internal/AFa1ySDK;
    :try_end_77
    .catchall {:try_start_77 .. :try_end_77} :catchall_4c

    :try_start_78
    const-class v3, Ljava/lang/Class;

    sget-object v6, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/4 v7, 0x7

    aget-byte v8, v6, v7

    int-to-byte v7, v8

    const/16 v8, 0xd8

    aget-byte v9, v6, v8

    int-to-byte v8, v9

    const/16 v9, 0x180

    int-to-short v9, v9

    invoke-static {v7, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    invoke-virtual {v3, v2, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_78
    .catchall {:try_start_78 .. :try_end_78} :catchall_4b

    const/16 v3, 0x60

    .line 137
    :try_start_79
    aget-byte v3, v6, v3

    int-to-byte v3, v3

    const/16 v7, 0x1e

    aget-byte v8, v6, v7

    neg-int v7, v8

    int-to-byte v7, v7

    const/16 v8, 0x343

    int-to-short v8, v8

    invoke-static {v3, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v7, 0x2

    new-array v8, v7, [Ljava/lang/Class;

    const/4 v7, 0x4

    .line 138
    aget-byte v9, v6, v7

    neg-int v7, v9

    int-to-byte v7, v7

    const/16 v9, 0x20

    aget-byte v11, v6, v9

    int-to-byte v9, v11

    const/16 v11, 0x387

    int-to-short v11, v11

    invoke-static {v7, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v9, 0x0

    aput-object v7, v8, v9

    const/16 v7, 0xcd

    aget-byte v7, v6, v7

    int-to-byte v7, v7

    const/16 v9, 0x20

    aget-byte v13, v6, v9

    int-to-byte v9, v13

    const/16 v13, 0x235

    int-to-short v13, v13

    invoke-static {v7, v9, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const/4 v9, 0x1

    aput-object v7, v8, v9

    invoke-virtual {v3, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v7, 0x2

    new-array v8, v7, [Ljava/lang/Object;
    :try_end_79
    .catchall {:try_start_79 .. :try_end_79} :catchall_4c

    :try_start_7a
    new-array v7, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v7, v9

    const/4 v5, 0x4

    .line 139
    aget-byte v9, v6, v5

    neg-int v5, v9

    int-to-byte v5, v5

    const/16 v9, 0x20

    aget-byte v13, v6, v9

    int-to-byte v9, v13

    invoke-static {v5, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v9, 0x5

    aget-byte v11, v6, v9

    int-to-byte v11, v11

    const/16 v13, 0x18a

    aget-byte v13, v6, v13
    :try_end_7a
    .catchall {:try_start_7a .. :try_end_7a} :catchall_4a

    xor-int/lit8 v15, v13, -0x1

    const/16 v16, -0x1

    and-int/lit8 v13, v13, -0x1

    const/4 v4, 0x1

    shl-int/2addr v13, v4

    add-int/2addr v15, v13

    int-to-byte v13, v15

    const/16 v15, 0x21b

    :try_start_7b
    aget-byte v15, v6, v15

    neg-int v15, v15

    int-to-short v15, v15

    invoke-static {v11, v13, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v11

    new-array v13, v4, [Ljava/lang/Class;

    const/4 v15, 0x0

    aput-object v1, v13, v15

    invoke-virtual {v5, v11, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v11, 0x0

    invoke-virtual {v5, v11, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_7b
    .catchall {:try_start_7b .. :try_end_7b} :catchall_49

    :try_start_7c
    aput-object v5, v8, v15

    aput-object v2, v8, v4

    invoke-virtual {v3, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_7c
    .catchall {:try_start_7c .. :try_end_7c} :catchall_48

    const/16 v4, 0x133

    .line 140
    :try_start_7d
    aget-byte v4, v6, v4

    neg-int v4, v4

    int-to-byte v4, v4

    const/16 v5, 0x1e

    aget-byte v7, v6, v5

    neg-int v5, v7

    int-to-byte v5, v5

    const/16 v7, 0x2b6

    int-to-short v7, v7

    invoke-static {v4, v5, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x3b

    .line 141
    aget-byte v5, v6, v5

    int-to-byte v5, v5

    xor-int/lit8 v7, v5, 0x40

    and-int/lit8 v8, v5, 0x40

    or-int/2addr v7, v8

    int-to-byte v7, v7

    const/16 v8, 0x87

    int-to-short v8, v8

    invoke-static {v5, v7, v8}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    const/4 v5, 0x1

    .line 142
    invoke-virtual {v4, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 143
    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 144
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const/16 v8, 0x186

    .line 145
    aget-byte v11, v6, v8

    int-to-byte v13, v11

    const/16 v15, 0x6f

    aget-byte v15, v6, v15

    int-to-byte v15, v15

    int-to-short v11, v11

    invoke-static {v13, v15, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v11

    const/4 v13, 0x1

    .line 146
    invoke-virtual {v11, v13}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/16 v13, 0x14

    .line 147
    aget-byte v13, v6, v13

    int-to-byte v13, v13

    const/16 v15, 0x6f

    aget-byte v6, v6, v15

    int-to-byte v6, v6

    const/16 v15, 0x12a

    int-to-short v15, v15

    invoke-static {v13, v6, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    const/4 v7, 0x1

    .line 148
    invoke-virtual {v6, v7}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 149
    invoke-virtual {v11, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 150
    invoke-virtual {v6, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 151
    invoke-virtual {v4, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 152
    new-instance v13, Ljava/util/ArrayList;

    check-cast v7, Ljava/util/List;

    invoke-direct {v13, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 153
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 154
    invoke-virtual {v7}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v7

    .line 155
    invoke-static {v5}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v15

    .line 156
    invoke-static {v7, v15}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7
    :try_end_7d
    .catch Ljava/lang/Exception; {:try_start_7d .. :try_end_7d} :catch_f
    .catchall {:try_start_7d .. :try_end_7d} :catchall_44

    const/4 v8, 0x0

    :goto_4f
    if-ge v8, v15, :cond_5e

    .line 157
    :try_start_7e
    invoke-static {v5, v8}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v7, v8, v9}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_7e} :catch_c
    .catchall {:try_start_7e .. :try_end_7e} :catchall_3e

    or-int/lit8 v9, v8, 0x1

    const/16 v20, 0x1

    shl-int/lit8 v9, v9, 0x1

    xor-int/lit8 v8, v8, 0x1

    sub-int v8, v9, v8

    const/4 v9, 0x5

    goto :goto_4f

    :catch_c
    move-exception v0

    move-object v3, v0

    const/16 v9, 0x151

    goto/16 :goto_5b

    .line 158
    :cond_5e
    :try_start_7f
    invoke-virtual {v11, v4, v13}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 159
    invoke-virtual {v6, v4, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_7f} :catch_f
    .catchall {:try_start_7f .. :try_end_7f} :catchall_44

    .line 160
    :try_start_80
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;
    :try_end_80
    .catchall {:try_start_80 .. :try_end_80} :catchall_44

    if-nez v2, :cond_5f

    const/16 v2, 0x1f

    goto :goto_50

    :cond_5f
    const/16 v2, 0x4d

    :goto_50
    const/16 v4, 0x4d

    if-eq v2, v4, :cond_60

    .line 161
    :try_start_81
    sput-object v3, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    :cond_60
    :goto_51
    if-eqz v23, :cond_65

    .line 162
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v4, 0xcd

    aget-byte v4, v2, v4

    int-to-byte v4, v4

    const/16 v5, 0x1e

    aget-byte v5, v2, v5

    neg-int v5, v5

    int-to-byte v5, v5

    const/16 v6, 0x2d3

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x13

    .line 163
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x150

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0xe5

    aget-byte v7, v2, v7

    int-to-short v7, v7

    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v10, v7, v6

    const/16 v6, 0xcd

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v8, 0x20

    aget-byte v9, v2, v8

    int-to-byte v8, v9

    const/16 v9, 0x235

    int-to-short v9, v9

    invoke-static {v6, v8, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/4 v8, 0x1

    aput-object v6, v7, v8

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 164
    invoke-virtual {v5, v8}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v45, v7, v6

    .line 165
    const-class v6, Lcom/appsflyer/internal/AFa1ySDK;
    :try_end_81
    .catchall {:try_start_81 .. :try_end_81} :catchall_3e

    :try_start_82
    const-class v8, Ljava/lang/Class;

    const/4 v9, 0x7

    aget-byte v11, v2, v9
    :try_end_82
    .catchall {:try_start_82 .. :try_end_82} :catchall_3d

    int-to-byte v9, v11

    const/16 v11, 0xd8

    :try_start_83
    aget-byte v13, v2, v11

    int-to-byte v13, v13

    const/16 v15, 0x180

    int-to-short v15, v15

    invoke-static {v9, v13, v15}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v9

    const/4 v13, 0x0

    invoke-virtual {v8, v9, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    invoke-virtual {v8, v6, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6
    :try_end_83
    .catchall {:try_start_83 .. :try_end_83} :catchall_3c

    const/4 v8, 0x1

    :try_start_84
    aput-object v6, v7, v8

    invoke-virtual {v5, v3, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_84
    .catchall {:try_start_84 .. :try_end_84} :catchall_3e

    if-eqz v5, :cond_61

    const/4 v6, 0x0

    goto :goto_52

    :cond_61
    const/4 v6, 0x1

    :goto_52
    if-eqz v6, :cond_62

    goto :goto_53

    .line 166
    :cond_62
    sget v6, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    add-int/lit8 v6, v6, 0x78

    const/4 v7, 0x1

    sub-int/2addr v6, v7

    rem-int/lit16 v7, v6, 0x80

    sput v7, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v7, 0x2

    rem-int/2addr v6, v7

    if-eqz v6, :cond_63

    const/16 v6, 0x2d

    .line 167
    :try_start_85
    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0x5174

    aget-byte v2, v2, v7

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v7, 0x5a68

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    new-array v7, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v2, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    .line 168
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_85
    .catchall {:try_start_85 .. :try_end_85} :catchall_3e

    goto :goto_53

    :cond_63
    const/16 v6, 0xe

    .line 169
    :try_start_86
    aget-byte v7, v2, v6
    :try_end_86
    .catchall {:try_start_86 .. :try_end_86} :catchall_3b

    int-to-byte v6, v7

    const/16 v7, 0x151

    :try_start_87
    aget-byte v2, v2, v7

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v7, 0x112

    int-to-short v7, v7

    invoke-static {v6, v2, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    new-array v7, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v2, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v4, v6, [Ljava/lang/Object;

    .line 170
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :goto_53
    move-object v2, v5

    goto :goto_56

    :catchall_3b
    move-exception v0

    move-object v3, v0

    const/4 v2, 0x6

    goto :goto_55

    :catchall_3c
    move-exception v0

    goto :goto_54

    :catchall_3d
    move-exception v0

    const/16 v11, 0xd8

    :goto_54
    move-object v2, v0

    .line 171
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_64

    throw v3

    :cond_64
    throw v2
    :try_end_87
    .catchall {:try_start_87 .. :try_end_87} :catchall_3e

    :catchall_3e
    move-exception v0

    move-object v3, v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    :goto_55
    const/16 v9, 0x151

    goto/16 :goto_62

    :cond_65
    const/16 v11, 0xd8

    .line 172
    :try_start_88
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v4, 0xcd

    aget-byte v4, v2, v4

    int-to-byte v4, v4

    const/16 v5, 0x20

    aget-byte v6, v2, v5

    int-to-byte v5, v6

    const/16 v6, 0x235

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/16 v5, 0x13

    .line 173
    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x150

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v7, 0xe5

    aget-byte v2, v2, v7

    int-to-short v2, v2

    invoke-static {v5, v6, v2}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v10, v6, v7

    invoke-virtual {v4, v2, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_88
    .catchall {:try_start_88 .. :try_end_88} :catchall_44

    .line 174
    :try_start_89
    invoke-virtual {v2, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v45, v4, v7

    .line 175
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2
    :try_end_89
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_89 .. :try_end_89} :catch_d
    .catchall {:try_start_89 .. :try_end_89} :catchall_3e

    goto :goto_56

    :catch_d
    move-exception v0

    move-object v2, v0

    .line 176
    :try_start_8a
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljava/lang/Exception;

    throw v2
    :try_end_8a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8a .. :try_end_8a} :catch_e
    .catchall {:try_start_8a .. :try_end_8a} :catchall_3e

    :catch_e
    nop

    const/4 v2, 0x0

    :goto_56
    if-eqz v2, :cond_6a

    .line 177
    :try_start_8b
    move-object v8, v2

    check-cast v8, Ljava/lang/Class;

    .line 178
    sget-object v2, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v4, 0xa

    aget-byte v4, v2, v4

    int-to-byte v4, v4

    const/16 v5, 0x151

    aget-byte v6, v2, v5

    neg-int v5, v6

    int-to-byte v5, v5

    const/16 v6, 0x164

    int-to-short v6, v6

    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Class;

    .line 179
    const-class v4, Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v5, v6

    .line 180
    invoke-virtual {v8, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 181
    invoke-virtual {v4, v6}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v6, v5

    if-nez v23, :cond_66

    const/4 v3, 0x1

    goto :goto_57

    :cond_66
    const/4 v3, 0x0

    .line 182
    :goto_57
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v5, 0x1

    aput-object v3, v6, v5

    invoke-virtual {v4, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;

    const/16 v3, 0x30c0

    new-array v3, v3, [B

    .line 183
    const-class v4, Lcom/appsflyer/internal/AFa1ySDK;

    const/16 v5, 0x2b6

    aget-byte v5, v2, v5

    int-to-byte v5, v5

    const/16 v6, 0x3f

    aget-byte v6, v2, v6

    int-to-byte v6, v6

    const/16 v9, 0x202

    int-to-short v9, v9

    invoke-static {v5, v6, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    .line 184
    invoke-virtual {v4, v5}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4
    :try_end_8b
    .catchall {:try_start_8b .. :try_end_8b} :catchall_44

    .line 185
    sget v5, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    add-int/lit8 v5, v5, 0x31

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v6, 0x2

    rem-int/2addr v5, v6

    const/4 v5, 0x1

    :try_start_8c
    new-array v6, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v6, v5

    const/16 v4, 0x19c

    .line 186
    aget-byte v5, v2, v4

    int-to-byte v4, v5

    const/16 v5, 0x20

    aget-byte v9, v2, v5

    int-to-byte v5, v9

    or-int/lit16 v9, v5, 0x1a8

    int-to-short v9, v9

    invoke-static {v4, v5, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/Class;

    const/4 v5, 0x4

    aget-byte v13, v2, v5

    neg-int v5, v13

    int-to-byte v5, v5

    const/16 v13, 0x20

    aget-byte v15, v2, v13

    int-to-byte v13, v15

    const/4 v15, 0x6

    aget-byte v11, v2, v15

    int-to-short v11, v11

    invoke-static {v5, v13, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v11, 0x0

    aput-object v5, v9, v11

    invoke-virtual {v4, v9}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4
    :try_end_8c
    .catchall {:try_start_8c .. :try_end_8c} :catchall_42

    const/4 v5, 0x1

    :try_start_8d
    new-array v6, v5, [Ljava/lang/Object;

    aput-object v3, v6, v11

    const/16 v5, 0x19c

    .line 187
    aget-byte v9, v2, v5

    int-to-byte v5, v9

    const/16 v9, 0x20

    aget-byte v11, v2, v9

    int-to-byte v9, v11

    xor-int/lit16 v11, v9, 0x1a8

    and-int/lit16 v13, v9, 0x1a8

    or-int/2addr v11, v13

    int-to-short v11, v11

    invoke-static {v5, v9, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v9, 0x13

    aget-byte v9, v2, v9

    int-to-byte v9, v9

    const/16 v11, 0x219

    aget-byte v11, v2, v11

    int-to-byte v11, v11

    const/16 v13, 0x320

    int-to-short v13, v13

    invoke-static {v9, v11, v13}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v1, v13, v11

    invoke-virtual {v5, v9, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8d
    .catchall {:try_start_8d .. :try_end_8d} :catchall_41

    const/16 v5, 0x19c

    .line 188
    :try_start_8e
    aget-byte v6, v2, v5

    int-to-byte v5, v6

    const/16 v6, 0x20

    aget-byte v9, v2, v6

    int-to-byte v6, v9

    or-int/lit16 v9, v6, 0x1a8

    int-to-short v9, v9

    invoke-static {v5, v6, v9}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/16 v6, 0xe

    aget-byte v9, v2, v6
    :try_end_8e
    .catchall {:try_start_8e .. :try_end_8e} :catchall_40

    int-to-byte v6, v9

    const/16 v9, 0x151

    :try_start_8f
    aget-byte v2, v2, v9

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v11, 0x112

    int-to-short v11, v11

    invoke-static {v6, v2, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8f
    .catchall {:try_start_8f .. :try_end_8f} :catchall_3f

    .line 189
    :try_start_90
    invoke-static/range {v36 .. v36}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/16 v5, 0x3095

    move/from16 v4, v39

    move-object/from16 v15, v43

    move/from16 v6, v44

    move-object/from16 v46, v3

    move v3, v2

    move-object/from16 v2, v46

    goto/16 :goto_33

    :catchall_3f
    move-exception v0

    goto :goto_58

    :catchall_40
    move-exception v0

    const/16 v9, 0x151

    :goto_58
    move-object v2, v0

    .line 190
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_67

    throw v3

    :cond_67
    throw v2

    :catchall_41
    move-exception v0

    const/16 v9, 0x151

    move-object v2, v0

    .line 191
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_68

    throw v3

    :cond_68
    throw v2

    :catchall_42
    move-exception v0

    const/16 v9, 0x151

    move-object v2, v0

    .line 192
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_69

    throw v3

    :cond_69
    throw v2

    :cond_6a
    const/16 v9, 0x151

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Class;

    .line 193
    const-class v2, Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    move-object/from16 v8, v35

    .line 194
    invoke-virtual {v8, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 195
    invoke-virtual {v2, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v5, v4
    :try_end_90
    .catchall {:try_start_90 .. :try_end_90} :catchall_43

    if-nez v23, :cond_6b

    .line 196
    sget v3, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    xor-int/lit8 v4, v3, 0x5b

    and-int/lit8 v3, v3, 0x5b

    const/4 v6, 0x1

    shl-int/2addr v3, v6

    add-int/2addr v4, v3

    rem-int/lit16 v3, v4, 0x80

    sput v3, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    const/4 v3, 0x2

    rem-int/2addr v4, v3

    const/4 v3, 0x1

    goto :goto_59

    :cond_6b
    const/4 v3, 0x0

    .line 197
    :goto_59
    :try_start_91
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v5, v4

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sput-object v2, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;
    :try_end_91
    .catchall {:try_start_91 .. :try_end_91} :catchall_43

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x7

    const/16 v6, 0xe

    const/16 v7, 0x20

    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v13, 0x1

    const/16 v25, 0x1

    goto/16 :goto_67

    :catchall_43
    move-exception v0

    goto :goto_5a

    :catchall_44
    move-exception v0

    const/16 v9, 0x151

    :goto_5a
    move-object v3, v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    goto/16 :goto_62

    :catch_f
    move-exception v0

    const/16 v9, 0x151

    move-object v3, v0

    .line 198
    :goto_5b
    :try_start_92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B
    :try_end_92
    .catchall {:try_start_92 .. :try_end_92} :catchall_47

    const/16 v6, 0xe

    :try_start_93
    aget-byte v7, v5, v6

    int-to-byte v7, v7

    const/16 v8, 0x357

    aget-byte v11, v5, v8

    int-to-byte v8, v11

    const/16 v11, 0x239

    int-to-short v11, v11

    invoke-static {v7, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_93
    .catchall {:try_start_93 .. :try_end_93} :catchall_46

    const/4 v2, 0x6

    :try_start_94
    aget-byte v7, v5, v2

    int-to-byte v7, v7

    int-to-byte v8, v7

    xor-int/lit16 v11, v8, 0x2d3

    and-int/lit16 v13, v8, 0x2d3

    or-int/2addr v11, v13

    int-to-short v11, v11

    invoke-static {v7, v8, v11}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4
    :try_end_94
    .catchall {:try_start_94 .. :try_end_94} :catchall_57

    const/4 v7, 0x2

    :try_start_95
    new-array v8, v7, [Ljava/lang/Object;

    const/4 v7, 0x1

    aput-object v3, v8, v7

    const/4 v3, 0x0

    aput-object v4, v8, v3

    const/4 v3, 0x4

    aget-byte v4, v5, v3

    neg-int v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x20

    aget-byte v5, v5, v4

    int-to-byte v4, v5

    const/16 v5, 0x99

    int-to-short v5, v5

    invoke-static {v3, v4, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v10, v5, v4

    const-class v4, Ljava/lang/Throwable;

    const/4 v7, 0x1

    aput-object v4, v5, v7

    invoke-virtual {v3, v5}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    throw v3
    :try_end_95
    .catchall {:try_start_95 .. :try_end_95} :catchall_45

    :catchall_45
    move-exception v0

    move-object v3, v0

    :try_start_96
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_6c

    throw v4

    :cond_6c
    throw v3

    :catchall_46
    move-exception v0

    const/4 v2, 0x6

    goto/16 :goto_61

    :catchall_47
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    goto/16 :goto_61

    :catchall_48
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    goto/16 :goto_61

    :catchall_49
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    goto :goto_5c

    :catchall_4a
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/16 v16, -0x1

    :goto_5c
    move-object v3, v0

    .line 199
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_6d

    throw v4

    :cond_6d
    throw v3

    :catchall_4b
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/16 v16, -0x1

    move-object v3, v0

    .line 200
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_6e

    throw v4

    :cond_6e
    throw v3

    :catchall_4c
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    goto/16 :goto_60

    :catchall_4d
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/16 v16, -0x1

    move-object v3, v0

    .line 201
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_6f

    throw v4

    :cond_6f
    throw v3

    :catchall_4e
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 202
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_70

    throw v4

    :cond_70
    throw v3

    :catchall_4f
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 203
    :goto_5d
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_71

    throw v4

    :cond_71
    throw v3

    :catchall_50
    move-exception v0

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_72

    throw v4

    :cond_72
    throw v3

    :catchall_51
    move-exception v0

    goto/16 :goto_5f

    :catchall_52
    move-exception v0

    move/from16 v39, v4

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 204
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_73

    throw v4

    :cond_73
    throw v3

    :catchall_53
    move-exception v0

    move/from16 v39, v4

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 205
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_74

    throw v4

    :cond_74
    throw v3

    :catchall_54
    move-exception v0

    move/from16 v39, v4

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_75

    throw v4

    :cond_75
    throw v3

    :catchall_55
    move-exception v0

    move/from16 v39, v4

    goto/16 :goto_5f

    :catchall_56
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 206
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_76

    throw v4

    :cond_76
    throw v3

    :catchall_57
    move-exception v0

    goto/16 :goto_61

    :catchall_58
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    const/4 v2, 0x6

    const/16 v6, 0xe

    goto :goto_5e

    :catchall_59
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    :goto_5e
    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 207
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_77

    throw v4

    :cond_77
    throw v3

    :catchall_5a
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 208
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_78

    throw v4

    :cond_78
    throw v3

    :catchall_5b
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    const/16 v16, -0x1

    move-object v3, v0

    .line 209
    invoke-virtual {v3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_79

    throw v4

    :cond_79
    throw v3
    :try_end_96
    .catchall {:try_start_96 .. :try_end_96} :catchall_57

    :catchall_5c
    move-exception v0

    move/from16 v39, v4

    move/from16 v44, v6

    goto :goto_5f

    :catchall_5d
    move-exception v0

    move/from16 v27, v3

    move/from16 v39, v4

    move-object/from16 v28, v5

    move/from16 v44, v6

    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    :goto_5f
    const/4 v2, 0x6

    const/16 v6, 0xe

    const/16 v9, 0x151

    const/4 v14, 0x3

    :goto_60
    const/16 v16, -0x1

    :goto_61
    move-object v3, v0

    :goto_62
    add-int/lit8 v4, v44, 0x2

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    :goto_63
    const/4 v5, 0x7

    if-ge v4, v5, :cond_7c

    .line 210
    :try_start_97
    aget-boolean v7, v33, v4

    if-eqz v7, :cond_7a

    const/16 v7, 0x28

    goto :goto_64

    :cond_7a
    const/16 v7, 0x1c

    :goto_64
    const/16 v8, 0x1c

    if-eq v7, v8, :cond_7b

    const/4 v4, 0x1

    goto :goto_65

    :cond_7b
    add-int/lit8 v4, v4, 0x1

    goto :goto_63

    :cond_7c
    const/4 v4, 0x0

    :goto_65
    if-eqz v4, :cond_7d

    const/4 v3, 0x0

    .line 211
    sput-object v3, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;

    .line 212
    sput-object v3, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    const/4 v4, 0x4

    const/16 v7, 0x20

    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v13, 0x1

    goto :goto_66

    .line 213
    :cond_7d
    sget-object v1, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    const/16 v2, 0x19c

    aget-byte v2, v1, v2

    int-to-byte v2, v2

    const/16 v4, 0x357

    aget-byte v4, v1, v4

    int-to-byte v4, v4

    const/16 v5, 0x257

    int-to-short v5, v5

    invoke-static {v2, v4, v5}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v2
    :try_end_97
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_97} :catch_10

    const/4 v4, 0x2

    :try_start_98
    new-array v5, v4, [Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v3, v5, v4

    const/4 v3, 0x0

    aput-object v2, v5, v3

    const/4 v4, 0x4

    aget-byte v2, v1, v4

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v7, 0x20

    aget-byte v1, v1, v7

    int-to-byte v1, v1

    const/16 v3, 0x99

    int-to-short v3, v3

    invoke-static {v2, v1, v3}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v8, 0x2

    new-array v2, v8, [Ljava/lang/Class;

    const/4 v11, 0x0

    aput-object v10, v2, v11

    const-class v3, Ljava/lang/Throwable;

    const/4 v13, 0x1

    aput-object v3, v2, v13

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_98
    .catchall {:try_start_98 .. :try_end_98} :catchall_5e

    :catchall_5e
    move-exception v0

    move-object v1, v0

    :try_start_99
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_7e

    throw v2

    :cond_7e
    throw v1

    :cond_7f
    move/from16 v27, v3

    move/from16 v39, v4

    move-object/from16 v28, v5

    move/from16 v44, v6

    move-object/from16 v32, v7

    move-object/from16 v31, v11

    move-object/from16 v33, v13

    move-object/from16 v29, v14

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x7

    const/16 v6, 0xe

    const/16 v7, 0x20

    const/4 v8, 0x2

    const/16 v9, 0x151

    const/4 v11, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x3

    const/16 v16, -0x1

    :goto_66
    move/from16 v25, v27

    :goto_67
    add-int/lit8 v15, v44, 0x1

    move v6, v15

    move/from16 v3, v25

    move-object/from16 v5, v28

    move-object/from16 v14, v29

    move-object/from16 v11, v31

    move-object/from16 v7, v32

    move-object/from16 v13, v33

    move/from16 v4, v39

    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v15, 0x4

    goto/16 :goto_17

    :cond_80
    :goto_68
    return-void

    :catchall_5f
    move-exception v0

    move-object v1, v0

    .line 214
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_81

    throw v2

    :cond_81
    throw v1

    :catchall_60
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_82

    throw v2

    :cond_82
    throw v1

    :catchall_61
    move-exception v0

    move-object v1, v0

    .line 215
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_83

    throw v2

    :cond_83
    throw v1
    :try_end_99
    .catch Ljava/lang/Exception; {:try_start_99 .. :try_end_99} :catch_10

    :catch_10
    move-exception v0

    move-object v1, v0

    .line 216
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AFInAppEventParameterName(Ljava/lang/Object;)I
    .locals 8

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x1c

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    sub-int/2addr v1, v2

    .line 7
    rem-int/lit16 v3, v1, 0x80

    .line 8
    .line 9
    sput v3, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 10
    .line 11
    const/4 v3, 0x2

    .line 12
    rem-int/2addr v1, v3

    .line 13
    sget-object v1, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;

    .line 14
    .line 15
    and-int/lit8 v4, v0, 0x43

    .line 16
    .line 17
    or-int/lit8 v5, v0, 0x43

    .line 18
    .line 19
    add-int/2addr v4, v5

    .line 20
    rem-int/lit16 v5, v4, 0x80

    .line 21
    .line 22
    sput v5, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 23
    .line 24
    rem-int/2addr v4, v3

    .line 25
    and-int/lit8 v4, v0, 0x31

    .line 26
    .line 27
    or-int/lit8 v0, v0, 0x31

    .line 28
    .line 29
    add-int/2addr v4, v0

    .line 30
    rem-int/lit16 v0, v4, 0x80

    .line 31
    .line 32
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 33
    .line 34
    rem-int/2addr v4, v3

    .line 35
    :try_start_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 36
    .line 37
    const/4 v4, 0x0

    .line 38
    aput-object p0, v0, v4

    .line 39
    .line 40
    sget-object p0, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    .line 41
    .line 42
    const/16 v5, 0xa

    .line 43
    .line 44
    aget-byte v5, p0, v5

    .line 45
    .line 46
    int-to-byte v5, v5

    .line 47
    const/16 v6, 0x151

    .line 48
    .line 49
    aget-byte v6, p0, v6

    .line 50
    .line 51
    neg-int v6, v6

    .line 52
    int-to-byte v6, v6

    .line 53
    const/16 v7, 0x318

    .line 54
    .line 55
    int-to-short v7, v7

    .line 56
    invoke-static {v5, v6, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    sget-object v6, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    .line 61
    .line 62
    check-cast v6, Ljava/lang/ClassLoader;

    .line 63
    .line 64
    invoke-static {v5, v2, v6}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    const/16 v6, 0x23

    .line 69
    .line 70
    aget-byte v6, p0, v6

    .line 71
    .line 72
    int-to-byte v6, v6

    .line 73
    const/16 v7, 0x14

    .line 74
    .line 75
    aget-byte p0, p0, v7

    .line 76
    .line 77
    int-to-byte p0, p0

    .line 78
    sget v7, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    .line 79
    .line 80
    or-int/lit16 v7, v7, 0x204

    .line 81
    .line 82
    int-to-short v7, v7

    .line 83
    invoke-static {v6, p0, v7}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    new-array v6, v2, [Ljava/lang/Class;

    .line 88
    .line 89
    const-class v7, Ljava/lang/Object;

    .line 90
    .line 91
    aput-object v7, v6, v4

    .line 92
    .line 93
    invoke-virtual {v5, p0, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 94
    .line 95
    .line 96
    move-result-object p0

    .line 97
    invoke-virtual {p0, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object p0

    .line 101
    check-cast p0, Ljava/lang/Integer;

    .line 102
    .line 103
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 104
    .line 105
    .line 106
    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 107
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 108
    .line 109
    or-int/lit8 v1, v0, 0x3d

    .line 110
    .line 111
    shl-int/2addr v1, v2

    .line 112
    xor-int/lit8 v0, v0, 0x3d

    .line 113
    .line 114
    sub-int/2addr v1, v0

    .line 115
    rem-int/lit16 v0, v1, 0x80

    .line 116
    .line 117
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 118
    .line 119
    rem-int/2addr v1, v3

    .line 120
    if-eqz v1, :cond_0

    .line 121
    .line 122
    const/16 v0, 0x4d

    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_0
    const/4 v0, 0x2

    .line 126
    :goto_0
    if-eq v0, v3, :cond_1

    .line 127
    .line 128
    const/16 v0, 0x57

    .line 129
    .line 130
    :try_start_1
    div-int/2addr v0, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    return p0

    .line 132
    :catchall_0
    move-exception p0

    .line 133
    throw p0

    .line 134
    :cond_1
    return p0

    .line 135
    :catchall_1
    move-exception p0

    .line 136
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    if-eqz v0, :cond_2

    .line 141
    .line 142
    throw v0

    .line 143
    :cond_2
    throw p0
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public static AFKeystoreWrapper(ICI)Ljava/lang/Object;
    .locals 7

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x7

    .line 4
    .line 5
    rem-int/lit16 v2, v1, 0x80

    .line 6
    .line 7
    sput v2, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 8
    .line 9
    const/4 v2, 0x2

    .line 10
    rem-int/2addr v1, v2

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v1, 0x1

    .line 18
    :goto_0
    if-eqz v1, :cond_2

    .line 19
    .line 20
    sget-object v1, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;

    .line 21
    .line 22
    xor-int/lit8 v5, v0, 0x2f

    .line 23
    .line 24
    and-int/lit8 v0, v0, 0x2f

    .line 25
    .line 26
    shl-int/2addr v0, v4

    .line 27
    add-int/2addr v5, v0

    .line 28
    rem-int/lit16 v0, v5, 0x80

    .line 29
    .line 30
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 31
    .line 32
    rem-int/2addr v5, v2

    .line 33
    const/4 v0, 0x3

    .line 34
    :try_start_0
    new-array v5, v0, [Ljava/lang/Object;

    .line 35
    .line 36
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    aput-object p2, v5, v2

    .line 41
    .line 42
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    aput-object p1, v5, v4

    .line 47
    .line 48
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    aput-object p0, v5, v3

    .line 53
    .line 54
    sget-object p0, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    .line 55
    .line 56
    const/16 p1, 0xa

    .line 57
    .line 58
    aget-byte p1, p0, p1

    .line 59
    .line 60
    int-to-byte p1, p1

    .line 61
    const/16 p2, 0x151

    .line 62
    .line 63
    aget-byte p2, p0, p2

    .line 64
    .line 65
    neg-int p2, p2

    .line 66
    int-to-byte p2, p2

    .line 67
    const/16 v6, 0x318

    .line 68
    .line 69
    int-to-short v6, v6

    .line 70
    invoke-static {p1, p2, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    sget-object p2, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    .line 75
    .line 76
    check-cast p2, Ljava/lang/ClassLoader;

    .line 77
    .line 78
    invoke-static {p1, v4, p2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    const/16 p2, 0x10

    .line 83
    .line 84
    aget-byte p2, p0, p2

    .line 85
    .line 86
    int-to-byte p2, p2

    .line 87
    const/16 v6, 0x14

    .line 88
    .line 89
    aget-byte p0, p0, v6

    .line 90
    .line 91
    int-to-byte p0, p0

    .line 92
    const/16 v6, 0x140

    .line 93
    .line 94
    int-to-short v6, v6

    .line 95
    invoke-static {p2, p0, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object p0

    .line 99
    new-array p2, v0, [Ljava/lang/Class;

    .line 100
    .line 101
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 102
    .line 103
    aput-object v0, p2, v3

    .line 104
    .line 105
    sget-object v3, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    .line 106
    .line 107
    aput-object v3, p2, v4

    .line 108
    .line 109
    aput-object v0, p2, v2

    .line 110
    .line 111
    invoke-virtual {p1, p0, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 112
    .line 113
    .line 114
    move-result-object p0

    .line 115
    invoke-virtual {p0, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    return-object p0

    .line 120
    :catchall_0
    move-exception p0

    .line 121
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    if-eqz p1, :cond_1

    .line 126
    .line 127
    throw p1

    .line 128
    :cond_1
    throw p0

    .line 129
    :cond_2
    const/4 p0, 0x0

    .line 130
    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 131
    :catchall_1
    move-exception p0

    .line 132
    throw p0
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method static init$0()V
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 2
    .line 3
    xor-int/lit8 v1, v0, 0x1f

    .line 4
    .line 5
    and-int/lit8 v0, v0, 0x1f

    .line 6
    .line 7
    shl-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    add-int/2addr v1, v0

    .line 10
    rem-int/lit16 v0, v1, 0x80

    .line 11
    .line 12
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 13
    .line 14
    rem-int/lit8 v1, v1, 0x2

    .line 15
    .line 16
    const/16 v0, 0x3ed

    .line 17
    .line 18
    new-array v1, v0, [B

    .line 19
    .line 20
    const-string v2, "h\u008da\u00f6\u00ee\u0003\u0000\r\u00f7\u00fa \u00ec\u00f6\r\u0004\u00fd\u0010\u00eb\u00fc\u0008\u0018\u00e4\u00fd\u0000\u0003\u00f6\t\u00eb\u00153\u00c5\u00faA\u00ec\u00cd\u000f\u0000\u0001\u00f3\r\u0001\u001b\u00db\u00fe\u00fb\u0001!\u00df\u0002\r\u0004\u00f4\t\u00eb\u00153\u00c2\u000b\u00f3\u00079\u00db\u00da\u0006\u00ff\u000f\u00f8\u00ee\u0003\u0000\r\u00f7\u00fa3\u00d1\u0000\u0004\u0003\u0006\u0002\u00ed\u000b\u00fa\u0001\t\u00eb\u00153\u00c0\u0005\u00faA\u00ec\u00c9\u0005\u000f#\u00cd\u000f\u0000\u0001\u00f3\u00f3\n\u00f2\u0003\u0006\u00056\u00bf\u00fcE\u00ec\u00cd\u000c\u00fd\u0008@\u00ce\u0011\u00f3\u00ff\n\u00fa\u0001\t\u00eb\u00153\u00c5\u00faA\u00ec\u00c9\u0005\u000f$\u00cf\u0000\u0011\u00e80\u00db\u00fe\u00fb\u0001!\u00df\u0002\r\u0004\u00f4\u0003\u00f5\u00f6\r\u00fe=\u00bb\u00fa\u0006\u00ff\u000f\u00f8?\u00e5\u00db!\u00e8\u00f8\u00fe\u00fd\u00f95\u00df\u00ed5\u00d7\u000b\u00ee\u0000\'\u00dd\u000e\u00fd\u00ff\u00f3\r\u0004\u00fd\u001e\u00d1\t\u0000\u00f3\u00f4\u0002?\u00cd\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3D\u00c5\u00fb\u00fa\u000f\u00f3\u0004\r\u00f5>\u00ed\u00fb\u00e50\u00ba$\u000f\u00f9\'\u0000\u0002\u00f1.\u00dd\u00fd\u0007\u00f2/\u00db\u00f7\u0002\u00f10\u00df\u0004\u00fd!\u00db\u0007\u00ef\u0005\t\u00f5\u000f\u0002\u00f11\u00e2\u00fe\u00fb\u0003!\u00db\u00f7\r\u0004\u00fd\u0003\u00f5\u00f6\r\u00fe=\u00bb\u00fa\u0006\u00ff\u000f\u00f8?\u00ea\u00df\u00ed2\u00dd\u00fd\u0007\u00f4\u000b\u00ff\u0006\u00fc\u0002\u00fe\u00fb\u0003\u0003\u00f5\u00f6\r\u00fe=\u00bb\u00fa\u0006\u00ff\u000f\u00f8?\u00ec\u00e1\u00ee\u000e!\u00df\u00ed5\u00d7\u000b\u00ee\u0000\'\u00dd\u000e\u00fd\u00ff\u00f3\u00f4\u0002>\u00ce\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3C\u00c6\u00fb\u00fa\u000f\u00f3\u0004\r\u00f5=\u00ee\u00fb\u00e50\u00ba$\u000f\u00f9\'\u0000\u0002\u00f11\u00d4\u000b\u00ff\"\u00e2\u00fe\u00fb\u0003!\u00db\u00f7\u00fa\u000b\u000b\u00fb\u00fd\u00db-\u00d1\u0000+\u00cf\u0011\u00f7\u00fa \u00db\t\u000b\u0015\u00f9\u0017\u00f8\u00ba\u00ffO\u00ba\u0005\u00f5\u0000\n\u0001\u00fe\u00f8\u00f8S\u00b4\u0007\u00ff\u00f2K\u0015\u00fa\u0016\u00f8\u0015\u00fc\u0014\u00f8\u0015\u00f8\u0018\u00f8\t\u00eb\u00153\u00c2\u000b\u00f3\u00079\u00eb\u00d7\u000b\u00ee\u0000\'\u00dd\u000e\u00fd\u00ff\u00f3\u00f3\n\u00f2\u0003\u0006\u00056\u00cd\u00f1\u0000B\u00ed\u00d1\u0000)\u00db\u00fd\r\u0001\u00f5\u00f9\u000e\u00f1\"\u00ed\u0004\u00fd\u0015\u00e1\u0002\u00f3\u00cc\u00f4\u0002>\u00ce\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3C\u00c6\u00fb\u00fa\u000f\u00f3\u0004\r\u00f5=\u00cd5\t\u00eb\u00153\u00c5\u00faA\u00ea\u00e3\u00ed\u0013\u0018\u00db\u00fe\u00fb\u0001!\u00df\u0002\r\u0004\u00f4\u00fd\u000e\u00fd \u00df\u00ed\u00f3\n\u00f2\u0003\u0006\u00056\u00b8\r\u0004\u00eeI\u00e3\u00e6\u00ec4\u00cf\u0011\u00f7\u00fa\t\u00eb\u00153\u00c5\u00faA\u00ec\u00c9\u0005\u000f$\u00cf\u0000\u0011\u00e8*\u00da\u0001\u0004\u00fb\u0001!\u00df\u0002\r\u0004\u00f4\u0015\u00f5\u00f7\u0010\u00f2\u00f3\n\u00f2\u0003\u0006\u00056\u00bf\u00fcE\u00db\u00da\u0006\u00ff\u000f\u00f8*\u00d7\u00fd\u000c\u00f8\u0008\u0002\u00f9\u0002\u00f11\u00d7\u000b\u00ee\u0000\'\u00dd\u000e\u00fd\u00ff\u00f3\u00cb\u0003\u00ed\u00132\u00cb\u0003\u00ed\u00132\u00ff\u00f9\u0007\u00f1\u000f\u00f4\u0002>\u00ce\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3C\u00c6\u00fb\u00fa\u000f\u00f3\u0004\r\u00f5=\u00ee\u00fb\u00e50\u00b8&\u000f\u00f9\'\u00ad\u0002\u00f1.\u0002\u00fb\u00fb\u00e6\u00ec\u0006\u00ff\u0005\u00fd\r\u000e\u00e5\u0011\u00f1\u0000\u000b\u00f3\u000f\u00f9\u00ec\u0016\u00fb\u00fa\r\u00ed\u000b\u00f3\u0011\u0019\u00e3\u0007\u00f0\u0011\u00ef\u00f9)\u00ef\u00ed\u000c#\u00d9\u0007\u00f8\u0008\u00f7\u00fa\u0001\u00f7\u00fd\u00fc\u000e\u00cc\u00f4\u0002>\u00ce\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3C\u00c6\u00fb\u00fa\u000f\u00f3\u0004\r\u00f5=\u00ce4\t\u00eb\u00153\u00c5\u00faA\u00ba\u0007\u00fd\u000c\u00fb\u00f7\u0002\u00f1$\u00de\u0003\u00ff\u000b\u00f3\u00fe\u00fb\u0002\u00f13\u00df\u00ef\u0004\u0003\u00f7\u0001\u000f\u0015\u00ef\u00ed\u000c\u00f3\n\u00f2\u0003\u0006\u00056\u00cd\u00f1\u0000B\u00ed\u00de\u00ef\u000b\u00f3\r\u00f5\u00fb%\u00ec\u00f6\r\u0004\u00fd\t\u00eb\u00153\u00c5\u00faA\u00e8\u00dd\u00fd\u0007\u0016\u00da\u0001\u0004\u00fb\u0001!\u00df\u0002\r\u0004\u00f4\u0002\u00f3\u0017\u00e5\t\u00f5\u000f\t\u00eb\u00153\u00c5\u00faA\u00e5\u00fa\n\u00cd\u0015\u00fe\u00f5\u00fc\u000b\u00fa\u0001\u000f\u00ed\u000c\u001c\u00e3\u00f6\u00ff\u0002\u00f1+\u00db\u0005\u00f5\u000b\u0008\u00f5+\u00d1\u0000\u0004\u0003\u0006\u0002\u00ed\u000b\u00fa\u0001\t\u00eb\u00153\u00c5\u00faA\u00e8\u00dd\u00fd\u0007\u0015\u00fd\u0013\u00f8\u00ee\u0003\u0000\r\u00f7\u00fa \u00eb\u00fc\u0008\u0018\u00e4\u00fd\u0000\u0003\u00f6\t\u00eb\u00153\u00c5\u00faA\u00e8\u00dd\u00fd\u0007!\u00df\u00f2\u0010\u00f1\t\u00f9\u00fc\u0005\u00fd\u0005-\u00c9\u0005\u000f$\u00cf\u0000\u0011\u00e8\u00fd\u000e\u00fd!\u00d7\u000b\u00ee\u0000\u00f4\u0002?\u00cd\u00f1\u0000\u00fd\r\u00fa\u00f3\u0014\u00f3\u0005\u0011\u00f1\r\u00ed\u000b\u00f3\u0011\u0019\u00e3\u0007\u00f0\u0011\u00ef\u00f95\u00db\u00f7\r\u0002\u00ef\u0005\u00fd\t\u0004\u00f2\t\u00eb\u00153\u00c5\u00faA\u00e5\u00db\u00fe\u00fb\u0001!\u00df\u0002\r\u0004\u00f4"

    .line 21
    .line 22
    const-string v3, "ISO-8859-1"

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    const/4 v3, 0x0

    .line 29
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 30
    .line 31
    .line 32
    sput-object v1, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    .line 33
    .line 34
    const/16 v0, 0x62

    .line 35
    .line 36
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$$b:I

    .line 37
    .line 38
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 39
    .line 40
    add-int/lit8 v0, v0, 0xf

    .line 41
    .line 42
    rem-int/lit16 v1, v0, 0x80

    .line 43
    .line 44
    sput v1, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 45
    .line 46
    rem-int/lit8 v0, v0, 0x2

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public static values(I)I
    .locals 7

    .line 1
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 2
    .line 3
    and-int/lit8 v1, v0, 0x17

    .line 4
    .line 5
    or-int/lit8 v0, v0, 0x17

    .line 6
    .line 7
    add-int/2addr v1, v0

    .line 8
    rem-int/lit16 v0, v1, 0x80

    .line 9
    .line 10
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 11
    .line 12
    rem-int/lit8 v1, v1, 0x2

    .line 13
    .line 14
    sget-object v1, Lcom/appsflyer/internal/AFa1ySDK;->onResponseErrorNative:Ljava/lang/Object;

    .line 15
    .line 16
    xor-int/lit8 v2, v0, 0x23

    .line 17
    .line 18
    and-int/lit8 v0, v0, 0x23

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    shl-int/2addr v0, v3

    .line 22
    add-int/2addr v2, v0

    .line 23
    rem-int/lit16 v0, v2, 0x80

    .line 24
    .line 25
    sput v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 26
    .line 27
    rem-int/lit8 v2, v2, 0x2

    .line 28
    .line 29
    :try_start_0
    new-array v0, v3, [Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object p0

    .line 35
    const/4 v2, 0x0

    .line 36
    aput-object p0, v0, v2

    .line 37
    .line 38
    sget-object p0, Lcom/appsflyer/internal/AFa1ySDK;->$$a:[B

    .line 39
    .line 40
    const/16 v4, 0xa

    .line 41
    .line 42
    aget-byte v4, p0, v4

    .line 43
    .line 44
    int-to-byte v4, v4

    .line 45
    const/16 v5, 0x151

    .line 46
    .line 47
    aget-byte v5, p0, v5

    .line 48
    .line 49
    neg-int v5, v5

    .line 50
    int-to-byte v5, v5

    .line 51
    const/16 v6, 0x318

    .line 52
    .line 53
    int-to-short v6, v6

    .line 54
    invoke-static {v4, v5, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    sget-object v5, Lcom/appsflyer/internal/AFa1ySDK;->onResponseNative:Ljava/lang/Object;

    .line 59
    .line 60
    check-cast v5, Ljava/lang/ClassLoader;

    .line 61
    .line 62
    invoke-static {v4, v3, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    const/16 v5, 0x10

    .line 67
    .line 68
    aget-byte v5, p0, v5

    .line 69
    .line 70
    int-to-byte v5, v5

    .line 71
    const/16 v6, 0x14

    .line 72
    .line 73
    aget-byte p0, p0, v6

    .line 74
    .line 75
    int-to-byte p0, p0

    .line 76
    const/16 v6, 0x140

    .line 77
    .line 78
    int-to-short v6, v6

    .line 79
    invoke-static {v5, p0, v6}, Lcom/appsflyer/internal/AFa1ySDK;->$$c(BIS)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    new-array v5, v3, [Ljava/lang/Class;

    .line 84
    .line 85
    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 86
    .line 87
    aput-object v6, v5, v2

    .line 88
    .line 89
    invoke-virtual {v4, p0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 90
    .line 91
    .line 92
    move-result-object p0

    .line 93
    invoke-virtual {p0, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object p0

    .line 97
    check-cast p0, Ljava/lang/Integer;

    .line 98
    .line 99
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 100
    .line 101
    .line 102
    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    sget v0, Lcom/appsflyer/internal/AFa1ySDK;->$10:I

    .line 104
    .line 105
    add-int/2addr v0, v3

    .line 106
    rem-int/lit16 v1, v0, 0x80

    .line 107
    .line 108
    sput v1, Lcom/appsflyer/internal/AFa1ySDK;->$11:I

    .line 109
    .line 110
    rem-int/lit8 v0, v0, 0x2

    .line 111
    .line 112
    return p0

    .line 113
    :catchall_0
    move-exception p0

    .line 114
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    .line 120
    throw v0

    .line 121
    :cond_0
    throw p0
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

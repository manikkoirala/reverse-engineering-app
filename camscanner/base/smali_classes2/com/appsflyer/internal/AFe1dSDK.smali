.class public final Lcom/appsflyer/internal/AFe1dSDK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/appsflyer/internal/AFe1aSDK;


# static fields
.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field private static AppsFlyer2dXConversionCallback:J = 0x0L

.field private static init:I = 0x1

.field private static onAppOpenAttributionNative:[C

.field private static onInstallConversionFailureNative:I


# instance fields
.field private final AFInAppEventParameterName:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFInAppEventType:Lcom/appsflyer/internal/AFc1sSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFKeystoreWrapper:Lcom/appsflyer/internal/AFa1iSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFLogger:Lcom/appsflyer/internal/AFe1hSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFLogger$LogLevel:Lcom/appsflyer/internal/AFc1rSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final AFVersionDeclaration:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFe1gSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afInfoLog:Lcom/appsflyer/internal/AFa1hSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afRDLog:Lcom/appsflyer/internal/AFf1tSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final afWarnLog:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final getLevel:Lcom/appsflyer/internal/AFb1vSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final valueOf:Lcom/appsflyer/internal/AFf1aSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final values:Lcom/appsflyer/internal/AFg1hSDK;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/16 v0, 0x14

    .line 2
    .line 3
    new-array v0, v0, [C

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/appsflyer/internal/AFe1dSDK;->onAppOpenAttributionNative:[C

    .line 9
    .line 10
    const-wide v0, 0x6ab37cd97de1a436L    # 9.775916772951982E205

    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    sput-wide v0, Lcom/appsflyer/internal/AFe1dSDK;->AppsFlyer2dXConversionCallback:J

    .line 16
    .line 17
    return-void

    .line 18
    nop

    .line 19
    :array_0
    .array-data 2
        0x619as
        -0x5ba1s
        -0x15e6s
        0x30d1s
        0x76a0s
        -0x42bes
        -0x3cdfs
        0x90fs
        0x4fc2s
        -0x6a7bs
        -0x2790s
        0x1e16s
        0x24e5s
        0x6acas
        -0x4f6bs
        0x619es
        -0x5bbcs
        -0x15f7s
        0x30ccs
        0x76b0s
    .end array-data
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/appsflyer/internal/AFf1aSDK;Lcom/appsflyer/internal/AFc1sSDK;Lcom/appsflyer/internal/AFg1hSDK;Lcom/appsflyer/internal/AFa1iSDK;Lcom/appsflyer/internal/AFf1tSDK;Lcom/appsflyer/internal/AFc1uSDK;Lcom/appsflyer/internal/AFc1tSDK;Lcom/appsflyer/internal/AFa1hSDK;Lcom/appsflyer/internal/AFe1hSDK;Lcom/appsflyer/internal/AFb1vSDK;Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFe1gSDK;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFf1aSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/appsflyer/internal/AFc1sSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/appsflyer/internal/AFg1hSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/appsflyer/internal/AFa1iSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/appsflyer/internal/AFf1tSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/appsflyer/internal/AFc1uSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/appsflyer/internal/AFc1tSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/appsflyer/internal/AFa1hSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/appsflyer/internal/AFe1hSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/appsflyer/internal/AFb1vSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/appsflyer/internal/AFc1rSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/appsflyer/internal/AFe1gSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    .line 46
    .line 47
    iput-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->valueOf:Lcom/appsflyer/internal/AFf1aSDK;

    .line 48
    .line 49
    iput-object p3, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1sSDK;

    .line 50
    .line 51
    iput-object p4, p0, Lcom/appsflyer/internal/AFe1dSDK;->values:Lcom/appsflyer/internal/AFg1hSDK;

    .line 52
    .line 53
    iput-object p5, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFa1iSDK;

    .line 54
    .line 55
    iput-object p6, p0, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog:Lcom/appsflyer/internal/AFf1tSDK;

    .line 56
    .line 57
    iput-object p7, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 58
    .line 59
    iput-object p8, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    .line 60
    .line 61
    iput-object p9, p0, Lcom/appsflyer/internal/AFe1dSDK;->afInfoLog:Lcom/appsflyer/internal/AFa1hSDK;

    .line 62
    .line 63
    iput-object p10, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 64
    .line 65
    iput-object p11, p0, Lcom/appsflyer/internal/AFe1dSDK;->getLevel:Lcom/appsflyer/internal/AFb1vSDK;

    .line 66
    .line 67
    iput-object p12, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFc1rSDK;

    .line 68
    .line 69
    iput-object p13, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFe1gSDK;

    .line 70
    .line 71
    sget-object p1, Lcom/appsflyer/internal/AFe1dSDK$3;->values:Lcom/appsflyer/internal/AFe1dSDK$3;

    .line 72
    .line 73
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afWarnLog:Lkotlin/Lazy;

    .line 78
    .line 79
    sget-object p1, Lcom/appsflyer/internal/AFe1dSDK$4;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFe1dSDK$4;

    .line 80
    .line 81
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    iput-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFVersionDeclaration:Lkotlin/Lazy;

    .line 86
    .line 87
    return-void
.end method

.method private final AFInAppEventParameterName()Ljava/text/SimpleDateFormat;
    .locals 3

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x41

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x28

    if-nez v0, :cond_0

    const/16 v0, 0x13

    goto :goto_0

    :cond_0
    const/16 v0, 0x28

    :goto_0
    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFVersionDeclaration:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x71

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFVersionDeclaration:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private final AFInAppEventParameterName(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "versionCode"

    .line 2
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x13

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v1, 0x1

    .line 3
    :try_start_0
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 4
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v3, v0, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;I)I

    move-result v3

    .line 5
    iget v5, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-le v5, v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    :goto_0
    if-eq v3, v1, :cond_1

    .line 6
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v3, v0, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventType(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x25

    rem-int/lit16 v3, v0, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    :cond_1
    :try_start_1
    const-string v0, "app_version_code"

    .line 8
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "app_version_name"

    .line 9
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    .line 10
    iget-object v3, v3, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 11
    iget-object v3, v3, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 12
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 13
    invoke-static {v3, v5}, Lcom/appsflyer/internal/AFa1eSDK;->AFKeystoreWrapper(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 14
    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "targetSDKver"

    .line 15
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    .line 16
    iget-object v3, v3, Lcom/appsflyer/internal/AFc1tSDK;->values:Lcom/appsflyer/internal/AFc1rSDK;

    .line 17
    iget-object v3, v3, Lcom/appsflyer/internal/AFc1rSDK;->AFInAppEventType:Landroid/content/Context;

    .line 18
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 19
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    iget-wide v5, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    .line 21
    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    const-string v0, "date1"

    .line 22
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName()Ljava/text/SimpleDateFormat;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "date2"

    .line 23
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName()Ljava/text/SimpleDateFormat;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    add-int/lit8 v0, v0, -0x1

    int-to-char v0, v0

    invoke-static {v4, v4, v4}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v2

    rsub-int/lit8 v2, v2, 0xf

    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatTimeout()I

    move-result v3

    shr-int/lit8 v3, v3, 0x10

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3, v5}, Lcom/appsflyer/internal/AFe1dSDK;->a(CII[Ljava/lang/Object;)V

    aget-object v0, v5, v4

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName()Ljava/text/SimpleDateFormat;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    const-string v0, "Exception while collecting app version data "

    .line 25
    invoke-static {v0, p1, v1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    return-void
.end method

.method private static AFInAppEventParameterName(Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eq v1, v0, :cond_1

    .line 74
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x5b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, "phone"

    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p0, p0, 0x37

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p0, p0, 0x2

    return-void
.end method

.method private AFInAppEventParameterName(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/appsflyer/internal/AFc1xSDK;)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/appsflyer/internal/AFc1xSDK;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eq v0, v1, :cond_1

    .line 61
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x5d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 62
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog(Ljava/util/Map;)V

    .line 63
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger$LogLevel(Ljava/util/Map;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFVersionDeclaration(Ljava/util/Map;)V

    .line 65
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFc1rSDK;

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFb1qSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)V

    .line 66
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x67

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 67
    :cond_1
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->afWarnLog(Ljava/util/Map;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger(Ljava/util/Map;)V

    .line 69
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->afInfoLog(Ljava/util/Map;)V

    .line 70
    invoke-static {p1, p3}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/util/Map;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/appsflyer/internal/AFe1dSDK;->valueOf(Ljava/util/Map;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly(Ljava/util/Map;)V

    const/4 p2, 0x3

    if-eqz p4, :cond_2

    const/16 p3, 0x1b

    goto :goto_1

    :cond_2
    const/4 p3, 0x3

    :goto_1
    if-eq p3, p2, :cond_3

    .line 73
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p2, p2, 0x57

    rem-int/lit16 p3, p2, 0x80

    sput p3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p2, p2, 0x2

    invoke-virtual {p4, p1}, Lcom/appsflyer/internal/AFc1xSDK;->AFKeystoreWrapper(Ljava/util/Map;)V

    :cond_3
    return-void
.end method

.method private static AFInAppEventParameterName(Ljava/util/Map;Z)V
    .locals 3
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .line 75
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x1f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "af_preinstalled"

    const-string v2, ""

    if-eqz v0, :cond_1

    .line 76
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private static AFInAppEventParameterName(Ljava/io/File;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eq v2, v1, :cond_1

    goto :goto_2

    .line 78
    :cond_1
    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v2, v2, 0xf

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result p0

    const/16 v2, 0x24

    if-nez p0, :cond_2

    const/16 p0, 0x1d

    goto :goto_1

    :cond_2
    const/16 p0, 0x24

    :goto_1
    if-eq p0, v2, :cond_3

    :goto_2
    return v1

    :cond_3
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p0, p0, 0x61

    rem-int/lit16 v1, p0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p0, p0, 0x2

    const/16 v1, 0x29

    if-eqz p0, :cond_4

    const/16 p0, 0x29

    goto :goto_3

    :cond_4
    const/16 p0, 0x40

    :goto_3
    if-eq p0, v1, :cond_5

    return v0

    :cond_5
    :try_start_0
    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :cond_6
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p0

    throw p0
.end method

.method private static AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateApi"
        }
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 58
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v2, 0x21

    if-eqz v0, :cond_0

    const/16 v0, 0x21

    goto :goto_0

    :cond_0
    const/16 v0, 0x2d

    .line 59
    :goto_0
    const-class v3, Ljava/lang/String;

    const-string v4, "get"

    const-string v5, "android.os.SystemProperties"

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eq v0, v2, :cond_1

    :try_start_0
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Class;

    aput-object v3, v2, v7

    .line 60
    invoke-virtual {v0, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v7

    .line 61
    invoke-virtual {v0, v6, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_4

    goto :goto_1

    .line 62
    :cond_1
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Class;

    .line 63
    aput-object v3, v2, v1

    invoke-virtual {v0, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v7

    .line 64
    invoke-virtual {v0, v6, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 65
    :goto_1
    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x77

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x22

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    goto :goto_2

    :cond_2
    const/16 v0, 0x22

    :goto_2
    if-eq v0, v1, :cond_3

    const/16 v0, 0x32

    :try_start_1
    div-int/2addr v0, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p0

    :catchall_0
    move-exception p0

    throw p0

    :cond_3
    return-object p0

    :cond_4
    :try_start_2
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v6
.end method

.method private final AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFa1uSDK;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x59

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    .line 2
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result p1

    const/4 v0, 0x5

    if-nez p1, :cond_0

    const/16 p1, 0x60

    goto :goto_0

    :cond_0
    const/4 p1, 0x5

    :goto_0
    if-eq p1, v0, :cond_3

    .line 3
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x13

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p1, p1, 0x2

    .line 4
    :try_start_0
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->values:Lcom/appsflyer/internal/AFg1hSDK;

    invoke-interface {p1}, Lcom/appsflyer/internal/AFg1hSDK;->values()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0xd

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    const/16 p2, 0x3e

    if-nez p1, :cond_1

    const/16 p1, 0x27

    goto :goto_1

    :cond_1
    const/16 p1, 0x3e

    :goto_1
    if-ne p1, p2, :cond_2

    return-void

    :cond_2
    const/4 p1, 0x0

    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :catch_0
    move-exception p1

    const-string p2, "error while getting sensors data"

    .line 6
    invoke-static {p2, p1}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 7
    new-instance p2, Ljava/lang/StringBuilder;

    const-string v0, "Unexpected exception from AFSensorManager: "

    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private AFInAppEventType(Ljava/util/Map;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v1

    const-string v2, "deviceTrackingDisabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_8

    .line 27
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-virtual {v1, v2}, Lcom/appsflyer/internal/AFe1hSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 28
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/16 v5, 0x50

    if-nez v2, :cond_1

    const/16 v2, 0x50

    goto :goto_1

    :cond_1
    const/16 v2, 0x22

    :goto_1
    if-eq v2, v5, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_3

    const-string v2, "imei"

    .line 29
    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    :cond_3
    invoke-direct {p0, p2}, Lcom/appsflyer/internal/AFe1dSDK;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_4

    .line 31
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v2, "androidIdCached"

    invoke-interface {v1, v2, p2}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "android_id"

    .line 32
    invoke-interface {p1, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    const-string p2, "Android ID was not collected."

    .line 33
    invoke-static {p2}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 34
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p2, p2, 0x7d

    rem-int/lit16 v1, p2, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p2, p2, 0x2

    :goto_3
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {p2}, Lcom/appsflyer/internal/AFa1dSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFa1bSDK;

    move-result-object p2

    if-eqz p2, :cond_7

    .line 35
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 36
    iget-object v2, p2, Lcom/appsflyer/internal/AFa1bSDK;->values:Ljava/lang/Boolean;

    .line 37
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "isManual"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v2, p2, Lcom/appsflyer/internal/AFa1bSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 39
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "val"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object p2, p2, Lcom/appsflyer/internal/AFa1bSDK;->valueOf:Ljava/lang/Boolean;

    if-eqz p2, :cond_6

    .line 41
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x43

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v2, "isLat"

    if-nez v0, :cond_5

    .line 42
    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_5
    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 43
    throw p1

    :cond_6
    :goto_4
    const-string p2, "oaid"

    .line 44
    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    return-void

    .line 45
    :cond_8
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p2, p2, 0x27

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p2, p2, 0x2

    const-string p2, "true"

    .line 46
    invoke-interface {p1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private AFInAppEventType(Ljava/util/Map;Lkotlin/jvm/functions/Function0;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "Exception while collecting facebook\'s attribution ID. "

    .line 47
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x71

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    const-string v1, ""

    .line 48
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v1

    const-string v2, "collectFacebookAttrId"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_3

    .line 50
    :cond_1
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x69

    rem-int/lit16 v4, v1, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v1, 0x0

    .line 51
    :try_start_0
    iget-object v4, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.facebook.katana"

    invoke-virtual {v4, v5, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 52
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x13

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    move-object v1, p2

    goto :goto_1

    :catchall_0
    move-exception p2

    .line 54
    invoke-static {v0, p2}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_0
    move-exception p2

    const-string v4, "com.facebook.katana not found"

    .line 55
    invoke-static {v4, p2, v3}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 56
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_2

    const/4 p2, 0x0

    goto :goto_2

    :cond_2
    const/4 p2, 0x1

    :goto_2
    if-eq p2, v3, :cond_3

    .line 57
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p2, p2, 0x39

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p2, p2, 0x2

    const-string v0, "fb"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_3

    const/16 p1, 0x42

    :try_start_1
    div-int/2addr p1, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception p1

    throw p1

    :cond_3
    :goto_3
    return-void
.end method

.method private static AFKeystoreWrapper()J
    .locals 4

    .line 35
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v2, v2, 0x51

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v2, v2, 0x2

    return-wide v0
.end method

.method private static AFKeystoreWrapper(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_2

    .line 71
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p0, p0, 0x39

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p0, p0, 0x2

    const/16 p1, 0x16

    if-nez p0, :cond_0

    const/16 p0, 0x16

    goto :goto_0

    :cond_0
    const/16 p0, 0x46

    :goto_0
    if-eq p0, p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 p0, 0x35

    :try_start_0
    div-int/lit8 p0, p0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    return-object v0

    :catchall_0
    move-exception p0

    throw p0

    .line 72
    :cond_2
    :try_start_1
    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    .line 73
    new-instance v2, Ljava/io/InputStreamReader;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 74
    :try_start_2
    invoke-virtual {v1, v2}, Ljava/util/Properties;->load(Ljava/io/Reader;)V

    const-string v3, "Found PreInstall property!"

    .line 75
    invoke-static {v3}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 77
    :try_start_3
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    .line 78
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-object p0

    :catchall_2
    move-exception p0

    goto :goto_3

    :catchall_3
    move-exception p0

    move-object v2, v0

    .line 79
    :goto_3
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    if-eqz v2, :cond_3

    .line 80
    :try_start_5
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto :goto_5

    :catchall_4
    move-exception p0

    .line 81
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 82
    :cond_3
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p0, p0, 0x6d

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    :goto_4
    rem-int/lit8 p0, p0, 0x2

    goto :goto_5

    :catch_0
    move-object v2, v0

    .line 83
    :catch_1
    :try_start_6
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "PreInstall file wasn\'t found: "

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    if-eqz v2, :cond_4

    .line 84
    :try_start_7
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_5

    .line 85
    :cond_4
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p0, p0, 0x7d

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    goto :goto_4

    :goto_5
    return-object v0

    :catchall_5
    move-exception p0

    if-eqz v2, :cond_5

    .line 86
    :try_start_8
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    goto :goto_6

    :catchall_6
    move-exception p1

    .line 87
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    :goto_6
    throw p0
.end method

.method private final AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 70
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x17

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x2c

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/16 v0, 0x2c

    :goto_0
    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    invoke-virtual {v0, p1}, Lcom/appsflyer/internal/AFc1tSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x63

    :try_start_0
    div-int/lit8 v0, v0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    throw p1

    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    invoke-virtual {v0, p1}, Lcom/appsflyer/internal/AFc1tSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private AFKeystoreWrapper(Ljava/text/SimpleDateFormat;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/text/SimpleDateFormat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const/4 v2, 0x0

    const-string v3, "appsFlyerFirstInstall"

    invoke-interface {v1, v3, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x42

    if-nez v1, :cond_0

    const/16 v4, 0x42

    goto :goto_0

    :cond_0
    const/4 v4, 0x4

    :goto_0
    if-eq v4, v2, :cond_1

    goto :goto_2

    .line 62
    :cond_1
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x1f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    .line 63
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "AppsFlyer: first launch detected"

    .line 64
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 65
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 66
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x2b

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    move-object v1, p1

    goto :goto_1

    :cond_2
    move-object v1, v0

    .line 67
    :goto_1
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {p1, v3, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "AppsFlyer: first launch date: "

    invoke-virtual {v2, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/appsflyer/AFLogger;->afInfoLog(Ljava/lang/String;)V

    .line 69
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final AFKeystoreWrapper(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    const-string v3, "yyyy-MM-dd_HHmmssZ"

    .line 30
    new-instance v4, Ljava/text/SimpleDateFormat;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const-string v3, "installDate"

    const-string v5, "UTC"

    .line 31
    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 32
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x21

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    const/16 v0, 0x43

    if-nez p1, :cond_0

    const/16 p1, 0x43

    goto :goto_0

    :cond_0
    const/16 p1, 0x52

    :goto_0
    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    const/16 p1, 0x16

    :try_start_1
    div-int/2addr p1, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1

    :catch_0
    move-exception p1

    const-string v0, "Exception while collecting install date. "

    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static AFKeystoreWrapper(Ljava/util/Map;Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 2
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/internal/AFa1uSDK;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, "eventName"

    .line 89
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v0, Lorg/json/JSONObject;

    iget-object p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventParameterName:Ljava/util/Map;

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    :cond_0
    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "eventValue"

    .line 91
    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private AFKeystoreWrapper(Ljava/util/Map;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 47
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x53

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x55

    if-eqz v0, :cond_0

    const/16 v0, 0x22

    goto :goto_0

    :cond_0
    const/16 v0, 0x55

    :goto_0
    const/4 v2, 0x0

    const-string v3, "prev_event_name"

    const-string v4, ""

    if-ne v0, v1, :cond_2

    .line 48
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v3, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "prev_event_timestamp"

    if-eqz v0, :cond_1

    .line 50
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 51
    iget-object v4, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const-wide/16 v5, -0x1

    invoke-interface {v4, v1, v5, v6}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 52
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "prev_event"

    .line 53
    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 54
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x67

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p1, p1, 0x2

    .line 55
    :cond_1
    :try_start_2
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {p1, v3, p2}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    .line 57
    :cond_2
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    :try_start_3
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {p1, v3, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 59
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    .line 60
    throw p1

    :catch_0
    move-exception p1

    const-string p2, "Error while processing previous event."

    invoke-static {p2, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private AFLogger()Ljava/lang/String;
    .locals 4

    .line 6
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "api_store_value"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x4f

    const/4 v2, 0x1

    if-nez v0, :cond_0

    const/16 v3, 0x4f

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    :goto_0
    if-eq v3, v1, :cond_1

    goto :goto_2

    .line 7
    :cond_1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x37

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    :goto_1
    const-string v3, "AF_STORE"

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v3}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x58

    :try_start_0
    div-int/2addr v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    throw v0

    :cond_3
    invoke-direct {p0, v3}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x27

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    return-object v0

    :cond_4
    const/4 v0, 0x0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v0

    throw v0
.end method

.method private AFLogger(Ljava/util/Map;)V
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "oneLinkSlug"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v1

    const-string v2, "onelinkVersion"

    invoke-virtual {v1, v2}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x39

    if-eqz v0, :cond_0

    const/16 v3, 0x1a

    goto :goto_0

    :cond_0
    const/16 v3, 0x39

    :goto_0
    if-eq v3, v2, :cond_1

    .line 3
    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v2, v2, 0x13

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v2, v2, 0x2

    const-string v2, "onelink_id"

    .line 4
    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 5
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, "onelink_ver"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method private final AFLogger$LogLevel(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-lt v0, v1, :cond_2

    .line 8
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x5b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    .line 9
    :goto_0
    const-class v0, Landroid/app/UiModeManager;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {v1, v0}, Landroidx/appcompat/widget/oo88o8O;->〇080(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/app/UiModeManager;

    goto :goto_3

    :cond_1
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {p1, v0}, Landroidx/appcompat/widget/oo88o8O;->〇080(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/UiModeManager;

    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 10
    throw p1

    .line 11
    :cond_2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    const-string v1, "uimode"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/app/UiModeManager;

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    goto :goto_3

    .line 12
    :cond_4
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x49

    rem-int/lit16 v5, v1, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    const/4 v3, 0x1

    :goto_2
    if-ne v3, v4, :cond_7

    .line 13
    move-object v2, v0

    check-cast v2, Landroid/app/UiModeManager;

    :goto_3
    if-eqz v2, :cond_6

    .line 14
    invoke-virtual {v2}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    const-string v0, "tv"

    .line 15
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-void

    :cond_7
    check-cast v0, Landroid/app/UiModeManager;

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1
.end method

.method private AFLogger$LogLevel()Z
    .locals 4

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    const/16 v1, 0xd

    add-int/2addr v0, v1

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x51

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    goto :goto_2

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    const/16 v3, 0x55

    if-lt v0, v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0x55

    :goto_1
    if-eq v1, v3, :cond_3

    .line 3
    :goto_2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afWarnLog()Z

    move-result v0

    return v0

    .line 4
    :cond_3
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x1d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 5
    invoke-static {}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly()Z

    move-result v0

    return v0

    :cond_4
    invoke-static {}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly()Z

    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    .line 6
    throw v0
.end method

.method private final AFVersionDeclaration()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x7d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    const-string v2, "android_id"

    const-string v3, "androidIdCached"

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v0, :cond_1

    .line 2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v3, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3
    :try_start_0
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    return-object v2

    :catch_0
    move-exception v2

    goto :goto_1

    .line 4
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v3, v5}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5
    :try_start_1
    iget-object v3, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 6
    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    .line 7
    throw v0

    .line 8
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    :goto_2
    if-eq v1, v4, :cond_5

    .line 9
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x15

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    const-string v2, "use cached AndroidId: "

    if-eqz v1, :cond_4

    .line 10
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    return-object v0

    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 11
    :try_start_3
    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    .line 12
    throw v0

    :cond_5
    return-object v5
.end method

.method private final AFVersionDeclaration(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 13
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x51

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 14
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {v0}, Lcom/appsflyer/internal/AFf1xSDK;->AFInAppEventParameterName(Landroid/content/Context;)Z

    move-result v0

    const/16 v1, 0x2d

    if-eqz v0, :cond_1

    const/16 v0, 0x55

    goto :goto_1

    :cond_1
    const/16 v0, 0x2d

    :goto_1
    if-eq v0, v1, :cond_2

    const-string v0, "inst_app"

    .line 15
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x61

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    :cond_2
    return-void

    :cond_3
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {p1}, Lcom/appsflyer/internal/AFf1xSDK;->AFInAppEventParameterName(Landroid/content/Context;)Z

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 17
    throw p1
.end method

.method private final AppsFlyer2dXConversionCallback()Ljava/lang/String;
    .locals 4

    .line 12
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x6d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    const-string v2, "ro.appsflyer.preinstall.path"

    if-eqz v0, :cond_5

    .line 13
    invoke-static {v2}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 14
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "AF_PRE_INSTALL_PATH"

    .line 16
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 18
    :cond_0
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x2b

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, "/data/local/tmp/pre_install.appsflyer"

    .line 20
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 21
    :cond_1
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/io/File;)Z

    move-result v2

    const/16 v3, 0x9

    if-eqz v2, :cond_2

    const/16 v2, 0x9

    goto :goto_0

    :cond_2
    const/16 v2, 0x52

    :goto_0
    if-eq v2, v3, :cond_3

    goto :goto_1

    :cond_3
    const-string v0, "/etc/pre_install.appsflyer"

    .line 22
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 23
    :goto_1
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 24
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x57

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    return-object v1

    .line 25
    :cond_4
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 26
    :cond_5
    invoke-static {v2}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 27
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 28
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/io/File;)Z

    :try_start_0
    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    .line 29
    throw v0
.end method

.method private AppsFlyer2dXConversionCallback(Ljava/util/Map;)V
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog:Lcom/appsflyer/internal/AFc1tSDK;

    invoke-virtual {v1}, Lcom/appsflyer/internal/AFc1tSDK;->valueOf()Ljava/lang/String;

    move-result-object v1

    .line 2
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-static {v2, v1}, Lcom/appsflyer/internal/AFe1dSDK;->values(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const/4 v5, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 3
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 4
    sget v5, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v5, v5, 0x4f

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v5, v5, 0x2

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-nez v2, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    const/4 v2, 0x1

    :goto_2
    if-eq v2, v4, :cond_6

    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v2, v2, 0x55

    rem-int/lit16 v6, v2, 0x80

    sput v6, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v2, v2, 0x2

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    if-eq v2, v4, :cond_4

    goto :goto_4

    :cond_4
    add-int/2addr v6, v4

    rem-int/lit16 v2, v6, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v6, v6, 0x2

    if-nez v6, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x1

    goto :goto_5

    :cond_6
    :goto_4
    const/4 v2, 0x0

    :goto_5
    if-nez v5, :cond_7

    if-eqz v2, :cond_8

    :cond_7
    const-string v2, "af_latestchannel"

    .line 5
    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    :cond_8
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afInfoLog()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1c

    if-eqz v1, :cond_9

    const/16 v5, 0x5f

    goto :goto_6

    :cond_9
    const/16 v5, 0x1c

    :goto_6
    if-eq v5, v2, :cond_a

    .line 7
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "af_installstore"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    :cond_a
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->getLevel()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "af_preinstall_name"

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    :cond_b
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    const/4 v3, 0x1

    :cond_c
    if-eq v3, v4, :cond_d

    return-void

    .line 11
    :cond_d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "af_currentstore"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static a(CII[Ljava/lang/Object;)V
    .locals 12

    .line 1
    new-instance v0, Lcom/appsflyer/internal/AFh1zSDK;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/appsflyer/internal/AFh1zSDK;-><init>()V

    .line 4
    .line 5
    .line 6
    new-array v1, p1, [J

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    iput v2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 10
    .line 11
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->$10:I

    .line 12
    .line 13
    add-int/lit8 v3, v3, 0xf

    .line 14
    .line 15
    rem-int/lit16 v4, v3, 0x80

    .line 16
    .line 17
    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->$11:I

    .line 18
    .line 19
    rem-int/lit8 v3, v3, 0x2

    .line 20
    .line 21
    :goto_0
    iget v3, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 22
    .line 23
    if-ge v3, p1, :cond_0

    .line 24
    .line 25
    const/4 v4, 0x1

    .line 26
    goto :goto_1

    .line 27
    :cond_0
    const/4 v4, 0x0

    .line 28
    :goto_1
    if-eqz v4, :cond_1

    .line 29
    .line 30
    sget-object v4, Lcom/appsflyer/internal/AFe1dSDK;->onAppOpenAttributionNative:[C

    .line 31
    .line 32
    add-int v5, p2, v3

    .line 33
    .line 34
    aget-char v4, v4, v5

    .line 35
    .line 36
    int-to-long v4, v4

    .line 37
    const-wide v6, -0x5c1be10a21129e04L    # -8.650878505717092E-136

    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    xor-long/2addr v4, v6

    .line 43
    long-to-int v5, v4

    .line 44
    int-to-char v4, v5

    .line 45
    int-to-long v4, v4

    .line 46
    int-to-long v8, v3

    .line 47
    sget-wide v10, Lcom/appsflyer/internal/AFe1dSDK;->AppsFlyer2dXConversionCallback:J

    .line 48
    .line 49
    xor-long/2addr v6, v10

    .line 50
    mul-long v8, v8, v6

    .line 51
    .line 52
    xor-long/2addr v4, v8

    .line 53
    int-to-long v6, p0

    .line 54
    xor-long/2addr v4, v6

    .line 55
    aput-wide v4, v1, v3

    .line 56
    .line 57
    add-int/lit8 v3, v3, 0x1

    .line 58
    .line 59
    iput v3, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_1
    new-array p0, p1, [C

    .line 63
    .line 64
    iput v2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 65
    .line 66
    :goto_2
    iget p2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 67
    .line 68
    if-ge p2, p1, :cond_4

    .line 69
    .line 70
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->$10:I

    .line 71
    .line 72
    add-int/lit8 v3, v3, 0x77

    .line 73
    .line 74
    rem-int/lit16 v4, v3, 0x80

    .line 75
    .line 76
    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->$11:I

    .line 77
    .line 78
    rem-int/lit8 v3, v3, 0x2

    .line 79
    .line 80
    const/16 v4, 0x5d

    .line 81
    .line 82
    if-nez v3, :cond_2

    .line 83
    .line 84
    const/16 v3, 0x5d

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_2
    const/16 v3, 0x29

    .line 88
    .line 89
    :goto_3
    if-eq v3, v4, :cond_3

    .line 90
    .line 91
    aget-wide v3, v1, p2

    .line 92
    .line 93
    long-to-int v4, v3

    .line 94
    int-to-char v3, v4

    .line 95
    aput-char v3, p0, p2

    .line 96
    .line 97
    add-int/lit8 p2, p2, 0x1

    .line 98
    .line 99
    :goto_4
    iput p2, v0, Lcom/appsflyer/internal/AFh1zSDK;->AFInAppEventType:I

    .line 100
    .line 101
    goto :goto_2

    .line 102
    :cond_3
    aget-wide v3, v1, p2

    .line 103
    .line 104
    long-to-int v4, v3

    .line 105
    int-to-char v3, v4

    .line 106
    aput-char v3, p0, p2

    .line 107
    .line 108
    add-int/lit8 p2, p2, -0x1

    .line 109
    .line 110
    goto :goto_4

    .line 111
    :cond_4
    new-instance p1, Ljava/lang/String;

    .line 112
    .line 113
    invoke-direct {p1, p0}, Ljava/lang/String;-><init>([C)V

    .line 114
    .line 115
    .line 116
    aput-object p1, p3, v2

    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method private static afDebugLog()Ljava/lang/String;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x63

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 3
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 4
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v3

    rem-long/2addr v3, v1

    .line 5
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v5

    xor-long v0, v5, v1

    goto :goto_1

    .line 6
    :cond_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v1

    .line 7
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v3

    mul-long v3, v3, v1

    .line 8
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v5

    mul-long v0, v5, v1

    :goto_1
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    const-wide/high16 v7, 0x4034000000000000L    # 20.0

    .line 9
    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    long-to-double v2, v3

    div-double/2addr v2, v5

    double-to-long v2, v2

    long-to-double v0, v0

    div-double/2addr v0, v5

    double-to-long v0, v0

    .line 10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 11
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x1f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const/16 v2, 0x26

    if-eqz v1, :cond_2

    const/16 v1, 0x26

    goto :goto_2

    :cond_2
    const/16 v1, 0x13

    :goto_2
    if-eq v1, v2, :cond_3

    return-object v0

    :cond_3
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private static afDebugLog(Ljava/util/Map;)V
    .locals 7
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 12
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x27

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 13
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    shr-int/lit8 v0, v0, 0x16

    int-to-char v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v2}, Landroid/util/TypedValue;->complexToFraction(IFF)F

    move-result v3

    const/4 v4, 0x1

    const-string v5, "sdk"

    cmpl-float v2, v3, v2

    add-int/lit8 v2, v2, 0x5

    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3, v6}, Lcom/appsflyer/internal/AFe1dSDK;->a(CII[Ljava/lang/Object;)V

    aget-object v0, v6, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "device"

    .line 15
    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "product"

    .line 16
    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "model"

    .line 18
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "deviceType"

    .line 19
    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p0, p0, 0x5d

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p0, p0, 0x2

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-ne v1, v4, :cond_1

    return-void

    :cond_1
    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0
.end method

.method private final afErrorLog(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 2
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x29

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 3
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/appsflyer/AppsFlyerProperties;->isOtherSdkStringDisabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1sSDK;

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1sSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;

    move-result-object v0

    .line 5
    iget v0, v0, Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;->AFInAppEventParameterName:F

    const-string v1, "batteryLevel"

    .line 6
    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x19

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p1

    invoke-virtual {p1}, Lcom/appsflyer/AppsFlyerProperties;->isOtherSdkStringDisabled()Z

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 8
    throw p1
.end method

.method private afErrorLog()Z
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x19

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v1, "appsFlyerCount"

    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    move-result v0

    const/16 v1, 0x12

    if-nez v0, :cond_0

    const/16 v0, 0x12

    goto :goto_0

    :cond_0
    const/16 v0, 0x40

    :goto_0
    if-eq v0, v1, :cond_3

    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x7b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x5e

    if-eqz v0, :cond_1

    const/16 v0, 0x53

    goto :goto_1

    :cond_1
    const/16 v0, 0x5e

    :goto_1
    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    return v0

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private afErrorLogForExcManagerOnly(Ljava/util/Map;)V
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog:Lcom/appsflyer/internal/AFf1tSDK;

    .line 10
    iget-wide v0, v0, Lcom/appsflyer/internal/AFf1tSDK;->afErrorLogForExcManagerOnly:J

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    cmp-long v6, v0, v2

    if-eqz v6, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    .line 11
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "prev_session_dur"

    .line 12
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x13

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    :cond_1
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x13

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x1

    :goto_1
    if-ne v4, v5, :cond_3

    return-void

    :cond_3
    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private static afErrorLogForExcManagerOnly()Z
    .locals 5

    const/4 v0, 0x0

    .line 1
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_3

    .line 2
    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v2, v2, 0x33

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    :goto_0
    if-ne v2, v3, :cond_2

    .line 3
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/NetworkInterface;

    .line 4
    invoke-virtual {v2}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "tun0"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v2, :cond_0

    .line 5
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x29

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    return v3

    :cond_2
    :try_start_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/NetworkInterface;

    .line 6
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->isUp()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v1, 0x0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    .line 7
    throw v0

    :catch_0
    move-exception v1

    const-string v2, "Failed collecting ivc data"

    .line 8
    invoke-static {v2, v1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    return v0
.end method

.method private afInfoLog()Ljava/lang/String;
    .locals 4

    .line 10
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x33

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    const-string v2, "INSTALL_STORE"

    if-nez v0, :cond_3

    .line 11
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog()Z

    move-result v0

    const/4 v3, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/16 v0, 0xb

    :goto_0
    if-eq v0, v3, :cond_2

    goto :goto_1

    .line 14
    :cond_2
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x69

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    .line 15
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger()Ljava/lang/String;

    move-result-object v1

    .line 16
    :goto_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v2, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 17
    :cond_3
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    :try_start_0
    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    .line 18
    throw v0
.end method

.method private afInfoLog(Ljava/util/Map;)V
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog:Lcom/appsflyer/internal/AFf1tSDK;

    .line 2
    new-instance v2, Ljava/util/HashMap;

    iget-object v3, v1, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventType:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3
    iget-object v1, v1, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventType:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 4
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog:Lcom/appsflyer/internal/AFf1tSDK;

    .line 5
    iget-object v1, v1, Lcom/appsflyer/internal/AFf1tSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v3, "gcd"

    invoke-interface {v1, v3}, Lcom/appsflyer/internal/AFc1uSDK;->valueOf(Ljava/lang/String;)V

    .line 6
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eq v1, v5, :cond_1

    .line 7
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x69

    rem-int/lit16 v6, v1, 0x80

    sput v6, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x65

    rem-int/lit16 v6, v1, 0x80

    sput v6, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v4, 0x1

    :goto_2
    if-eq v4, v5, :cond_3

    .line 8
    invoke-static {p1}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x51

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p1, p1, 0x2

    :cond_3
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x49

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method private final afRDLog(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x3d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x1a

    if-nez v0, :cond_0

    const/16 v0, 0x1a

    goto :goto_0

    :cond_0
    const/16 v0, 0x27

    :goto_0
    const-string v2, "btl"

    if-eq v0, v1, :cond_2

    .line 2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1sSDK;

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1sSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;

    move-result-object v0

    .line 3
    iget v1, v0, Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;->AFInAppEventParameterName:F

    .line 4
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;->AFInAppEventType:Ljava/lang/String;

    .line 5
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x9

    if-eqz v0, :cond_1

    const/16 v2, 0x2d

    goto :goto_1

    :cond_1
    const/16 v2, 0x9

    :goto_1
    if-eq v2, v1, :cond_3

    goto :goto_2

    .line 6
    :cond_2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType:Lcom/appsflyer/internal/AFc1sSDK;

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/appsflyer/internal/AFc1sSDK;->AFInAppEventParameterName(Landroid/content/Context;)Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;

    move-result-object v0

    .line 7
    iget v1, v0, Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;->AFInAppEventParameterName:F

    .line 8
    iget-object v0, v0, Lcom/appsflyer/internal/AFc1sSDK$AFa1vSDK;->AFInAppEventType:Ljava/lang/String;

    .line 9
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x61

    .line 10
    :try_start_0
    div-int/lit8 v1, v1, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    :goto_2
    const-string v1, "btch"

    .line 11
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x21

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    throw p1
.end method

.method private afRDLog()Z
    .locals 3

    .line 12
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x23

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const-string v1, "sentSuccessfully"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x4d

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    return v0
.end method

.method private afWarnLog(Ljava/util/Map;)V
    .locals 11
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 16
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x23

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    const-string v2, ""

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const-string v6, "AppsFlyerTimePassedSincePrevLaunch"

    if-nez v0, :cond_1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const-wide/16 v7, 0x1

    invoke-interface {v0, v6, v7, v8}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    move-result-wide v7

    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 19
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v6, v9, v10}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;J)V

    cmp-long v0, v7, v3

    if-lez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eq v0, v5, :cond_4

    goto :goto_1

    .line 20
    :cond_1
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v6, v3, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;J)J

    move-result-wide v7

    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 23
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v0, v6, v9, v10}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;J)V

    cmp-long v0, v7, v3

    if-lez v0, :cond_4

    .line 24
    :goto_1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x55

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x1

    :goto_2
    if-eq v1, v5, :cond_3

    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    and-long v1, v9, v7

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    goto :goto_3

    :cond_3
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long/2addr v9, v7

    invoke-virtual {v0, v9, v10}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    goto :goto_3

    :cond_4
    const-wide/16 v0, -0x1

    :goto_3
    const-string v2, "timepassedsincelastlaunch"

    .line 26
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final afWarnLog()Z
    .locals 9
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x15
    .end annotation

    const/4 v0, 0x0

    .line 1
    :try_start_0
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 2
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v3, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    :goto_0
    const/16 v5, 0x3c

    const/16 v6, 0x5c

    if-ge v4, v3, :cond_0

    const/16 v7, 0x3c

    goto :goto_1

    :cond_0
    const/16 v7, 0x5c

    :goto_1
    if-eq v7, v6, :cond_a

    .line 3
    sget v6, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v6, v6, 0x23

    rem-int/lit16 v7, v6, 0x80

    sput v7, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_8

    .line 4
    :try_start_1
    aget-object v6, v2, v4

    .line 5
    invoke-virtual {v1, v6}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v7, 0x1

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    :cond_1
    if-eq v5, v7, :cond_2

    .line 6
    sget v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v5, v5, 0x6b

    rem-int/lit16 v8, v5, 0x80

    sput v8, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v5, v5, 0x2

    :goto_2
    const/4 v5, 0x0

    goto :goto_5

    :cond_2
    const/4 v5, 0x4

    .line 7
    :try_start_2
    invoke-virtual {v6, v5}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-ne v5, v7, :cond_3

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    const/4 v5, 0x1

    :goto_3
    if-eqz v5, :cond_4

    goto :goto_2

    .line 8
    :cond_4
    sget v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v5, v5, 0x5d

    rem-int/lit16 v8, v5, 0x80

    sput v8, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v5, v5, 0x2

    const/16 v8, 0x13

    if-nez v5, :cond_5

    const/16 v5, 0x4d

    goto :goto_4

    :cond_5
    const/16 v5, 0x13

    :goto_4
    if-eq v5, v8, :cond_6

    goto :goto_2

    :cond_6
    const/4 v5, 0x1

    :goto_5
    if-eqz v5, :cond_7

    const/16 v5, 0xf

    .line 9
    :try_start_3
    invoke-virtual {v6, v5}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v5

    if-nez v5, :cond_7

    return v7

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 10
    :cond_8
    aget-object v2, v2, v4

    .line 11
    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/4 v1, 0x0

    .line 12
    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    .line 13
    throw v0

    .line 14
    :cond_9
    :try_start_5
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "null cannot be cast to non-null type android.net.ConnectivityManager"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "Failed collecting ivc data"

    .line 15
    invoke-static {v2, v1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_a
    return v0
.end method

.method private getLevel()Ljava/lang/String;
    .locals 7

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x5d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "preInstallName"

    if-eqz v0, :cond_1

    .line 2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x2c

    .line 3
    :try_start_0
    div-int/2addr v4, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/4 v4, 0x1

    :goto_0
    if-eq v4, v1, :cond_2

    goto :goto_1

    :catchall_0
    move-exception v0

    .line 4
    throw v0

    .line 5
    :cond_1
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    return-object v0

    .line 6
    :cond_2
    iget-object v4, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v4, v3}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-eq v4, v1, :cond_a

    .line 7
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLog()Z

    move-result v4

    const/16 v5, 0x3c

    const/16 v6, 0x13

    if-eqz v4, :cond_4

    const/16 v4, 0x3c

    goto :goto_3

    :cond_4
    const/16 v4, 0x13

    :goto_3
    if-eq v4, v5, :cond_5

    goto :goto_4

    .line 8
    :cond_5
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/2addr v0, v6

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    .line 9
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AppsFlyer2dXConversionCallback()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 10
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x31

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const-string v4, "AF_PRE_INSTALL_NAME"

    if-nez v0, :cond_6

    invoke-direct {p0, v4}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x2b

    :try_start_1
    div-int/2addr v4, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    throw v0

    .line 11
    :cond_6
    invoke-direct {p0, v4}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    :goto_4
    if-eqz v0, :cond_8

    const/4 v1, 0x0

    :cond_8
    if-eqz v1, :cond_9

    goto :goto_6

    .line 12
    :cond_9
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-interface {v1, v3, v0}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x53

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    :goto_5
    rem-int/lit8 v1, v1, 0x2

    goto :goto_6

    .line 14
    :cond_a
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const/4 v1, 0x0

    invoke-interface {v0, v3, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 15
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x5

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    goto :goto_5

    :goto_6
    if-eqz v0, :cond_b

    .line 16
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Lcom/appsflyer/AppsFlyerProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x57

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    :cond_b
    return-object v0
.end method

.method private static getLevel(Ljava/util/Map;)V
    .locals 7
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {}, Lcom/appsflyer/internal/AFb1qSDK;->valueOf()Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-static {}, Lcom/appsflyer/internal/AFb1qSDK;->AFKeystoreWrapper()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eq v4, v3, :cond_1

    goto :goto_2

    .line 20
    :cond_1
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v5, v4, 0x55

    rem-int/lit16 v6, v5, 0x80

    sput v6, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v5, v5, 0x2

    if-eqz v1, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-eq v5, v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v4, v4, 0x63

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v4, v4, 0x2

    .line 21
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_6

    .line 22
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v4, v4, 0x47

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_4

    const/4 v2, 0x1

    :cond_4
    const-string v4, "originalAppsflyerId"

    const-string v5, "reinstallCounter"

    if-eq v2, v3, :cond_5

    .line 23
    invoke-interface {p0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-interface {p0, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 25
    :cond_5
    invoke-interface {p0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    invoke-interface {p0, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p0, 0x0

    :try_start_0
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :cond_6
    :goto_2
    return-void
.end method

.method private init(Ljava/util/Map;)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x29

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    const-string v1, ""

    .line 17
    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFe1gSDK;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFe1gSDK;->values()Ljava/util/Map;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afErrorLogForExcManagerOnly:Lcom/appsflyer/internal/AFe1gSDK;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/appsflyer/internal/AFe1gSDK;->values()Ljava/util/Map;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 43
    .line 44
    .line 45
    const/4 p1, 0x0

    .line 46
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :catchall_0
    move-exception p1

    .line 48
    throw p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private onAppOpenAttributionNative(Ljava/util/Map;)V
    .locals 5
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    :try_start_0
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger$LogLevel:Lcom/appsflyer/internal/AFc1rSDK;

    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-static {v1, v2}, Lcom/appsflyer/internal/AFb1uSDK;->values(Lcom/appsflyer/internal/AFc1rSDK;Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v2, 0x3

    const/16 v3, 0x5c

    if-eqz v1, :cond_0

    const/4 v4, 0x3

    goto :goto_0

    :cond_0
    const/16 v4, 0x5c

    :goto_0
    if-eq v4, v3, :cond_3

    .line 2
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v3, v3, 0x2b

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v3, v3, 0x2

    :try_start_1
    const-string v3, "uid"

    .line 3
    invoke-interface {p1, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/2addr p1, v2

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    const/16 v0, 0x1c

    if-nez p1, :cond_1

    const/16 p1, 0x1c

    goto :goto_1

    :cond_1
    const/16 p1, 0x46

    :goto_1
    if-eq p1, v0, :cond_2

    return-void

    :cond_2
    const/4 p1, 0x0

    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :catchall_1
    move-exception p1

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: could not get uid "

    .line 6
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-static {v1, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    return-void
.end method

.method private final onAppOpenAttributionNative()Z
    .locals 4

    .line 10
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x69

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v1, "collectAndroidIdForceByUser"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 11
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 12
    :goto_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "collectIMEIForceByUser"

    invoke-virtual {v0, v1, v3}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eq v0, v2, :cond_2

    goto :goto_2

    :cond_2
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x7

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_6

    invoke-static {}, Lcom/appsflyer/internal/AFa1cSDK;->AFInAppEventType()Lcom/appsflyer/internal/AFa1cSDK;

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {v0}, Lcom/appsflyer/internal/AFa1cSDK;->valueOf(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    goto :goto_4

    :cond_4
    const/4 v0, 0x1

    :goto_4
    if-eq v0, v2, :cond_5

    goto :goto_5

    :cond_5
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x13

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    return v3

    :cond_6
    :goto_5
    return v2
.end method

.method private onAttributionFailureNative(Ljava/util/Map;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x4d

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    rem-int/2addr v0, v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const-string v3, ""

    .line 13
    .line 14
    const-string v4, "is_stop_tracking_used"

    .line 15
    .line 16
    if-eqz v0, :cond_5

    .line 17
    .line 18
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 22
    .line 23
    invoke-interface {v0, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v3, 0x0

    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 33
    :goto_0
    if-eqz v0, :cond_1

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 37
    .line 38
    add-int/lit8 v0, v0, 0x11

    .line 39
    .line 40
    rem-int/lit16 v5, v0, 0x80

    .line 41
    .line 42
    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 43
    .line 44
    rem-int/2addr v0, v1

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    const/4 v0, 0x2

    .line 48
    goto :goto_1

    .line 49
    :cond_2
    const/16 v0, 0x13

    .line 50
    .line 51
    :goto_1
    const-string v5, "istu"

    .line 52
    .line 53
    if-eq v0, v1, :cond_3

    .line 54
    .line 55
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 56
    .line 57
    invoke-interface {v0, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_3
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 70
    .line 71
    invoke-interface {v0, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;)Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    .line 81
    .line 82
    const/16 p1, 0x62

    .line 83
    .line 84
    :try_start_0
    div-int/2addr p1, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85
    :goto_2
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 86
    .line 87
    add-int/lit8 p1, p1, 0x4d

    .line 88
    .line 89
    rem-int/lit16 v0, p1, 0x80

    .line 90
    .line 91
    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 92
    .line 93
    rem-int/2addr p1, v1

    .line 94
    if-eqz p1, :cond_4

    .line 95
    .line 96
    return-void

    .line 97
    :cond_4
    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    :catchall_0
    move-exception p1

    .line 99
    throw p1

    .line 100
    :catchall_1
    move-exception p1

    .line 101
    throw p1

    .line 102
    :cond_5
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    iget-object p1, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 106
    .line 107
    invoke-interface {p1, v4}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;)Z

    .line 108
    .line 109
    .line 110
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 111
    :catchall_2
    move-exception p1

    .line 112
    throw p1
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private onConversionDataSuccess(Ljava/util/Map;)V
    .locals 5
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x29

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const-string v1, "didConfigureTokenRefreshService="

    .line 12
    .line 13
    const-string v2, ""

    .line 14
    .line 15
    const/4 v3, 0x1

    .line 16
    const/4 v4, 0x0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1bSDK;->values(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    const/16 v1, 0xf

    .line 40
    .line 41
    :try_start_0
    div-int/2addr v1, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    if-nez v0, :cond_0

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/4 v4, 0x1

    .line 46
    :goto_0
    if-eq v4, v3, :cond_4

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :catchall_0
    move-exception p1

    .line 50
    throw p1

    .line 51
    :cond_1
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    .line 55
    .line 56
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1bSDK;->values(Landroid/content/Context;)Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    if-nez v0, :cond_2

    .line 72
    .line 73
    const/4 v3, 0x0

    .line 74
    :cond_2
    if-eqz v3, :cond_3

    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_3
    :goto_1
    const-string v0, "tokenRefreshConfigured"

    .line 78
    .line 79
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 80
    .line 81
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 85
    .line 86
    add-int/lit8 v0, v0, 0x39

    .line 87
    .line 88
    rem-int/lit16 v1, v0, 0x80

    .line 89
    .line 90
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 91
    .line 92
    rem-int/lit8 v0, v0, 0x2

    .line 93
    .line 94
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    .line 95
    .line 96
    invoke-static {v0}, Lcom/appsflyer/internal/AFe1bSDK;->AFKeystoreWrapper(Lcom/appsflyer/internal/AFc1uSDK;)Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "registeredUninstall"

    .line 105
    .line 106
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private onDeepLinkingNative(Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x7

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const-string v0, ""

    .line 12
    .line 13
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-static {v0}, Lcom/appsflyer/internal/AFa1dSDK;->AFKeystoreWrapper(Landroid/content/ContentResolver;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v1, 0x1

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v2, 0x0

    .line 32
    :goto_0
    if-eq v2, v1, :cond_1

    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    iget-object v1, v0, Lcom/appsflyer/internal/AFa1bSDK;->AFInAppEventParameterName:Ljava/lang/String;

    .line 36
    .line 37
    const-string v2, "amazon_aid"

    .line 38
    .line 39
    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    iget-object v0, v0, Lcom/appsflyer/internal/AFa1bSDK;->valueOf:Ljava/lang/Boolean;

    .line 43
    .line 44
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const-string v1, "amazon_aid_limit"

    .line 49
    .line 50
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 54
    .line 55
    add-int/lit8 p1, p1, 0x7

    .line 56
    .line 57
    rem-int/lit16 v0, p1, 0x80

    .line 58
    .line 59
    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 60
    .line 61
    rem-int/lit8 p1, p1, 0x2

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private static onInstallConversionDataLoadedNative(Ljava/util/Map;)V
    .locals 2
    .param p0    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x4b

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const-string v0, ""

    .line 12
    .line 13
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :try_start_0
    const-string v0, "lang"

    .line 17
    .line 18
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    const-string v1, "Exception while collecting display language name. "

    .line 32
    .line 33
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    :try_start_1
    const-string v0, "lang_code"

    .line 37
    .line 38
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 47
    .line 48
    .line 49
    goto :goto_1

    .line 50
    :catch_1
    move-exception v0

    .line 51
    const-string v1, "Exception while collecting display language code. "

    .line 52
    .line 53
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    .line 55
    .line 56
    :goto_1
    :try_start_2
    const-string v0, "country"

    .line 57
    .line 58
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 67
    .line 68
    .line 69
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 70
    .line 71
    add-int/lit8 p0, p0, 0x4f

    .line 72
    .line 73
    rem-int/lit16 v0, p0, 0x80

    .line 74
    .line 75
    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 76
    .line 77
    rem-int/lit8 p0, p0, 0x2

    .line 78
    .line 79
    return-void

    .line 80
    :catch_2
    move-exception p0

    .line 81
    const-string v0, "Exception while collecting country name. "

    .line 82
    .line 83
    invoke-static {v0, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private onInstallConversionFailureNative(Ljava/util/Map;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x23

    .line 4
    .line 5
    rem-int/lit16 v1, v0, 0x80

    .line 6
    .line 7
    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 8
    .line 9
    rem-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    const-string v1, ""

    .line 12
    .line 13
    const-string v2, "sdkExtension"

    .line 14
    .line 15
    if-eqz v0, :cond_5

    .line 16
    .line 17
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0, v2}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const/4 v1, 0x0

    .line 29
    const/4 v3, 0x1

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    const/4 v4, 0x0

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 v4, 0x1

    .line 35
    :goto_0
    if-eqz v4, :cond_1

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_1
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 39
    .line 40
    add-int/lit8 v4, v4, 0x47

    .line 41
    .line 42
    rem-int/lit16 v5, v4, 0x80

    .line 43
    .line 44
    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 45
    .line 46
    rem-int/lit8 v4, v4, 0x2

    .line 47
    .line 48
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    const/16 v5, 0x42

    .line 53
    .line 54
    if-nez v4, :cond_2

    .line 55
    .line 56
    const/16 v4, 0x42

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    const/16 v4, 0x48

    .line 60
    .line 61
    :goto_1
    if-eq v4, v5, :cond_3

    .line 62
    .line 63
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 64
    .line 65
    add-int/lit8 v3, v3, 0x4d

    .line 66
    .line 67
    rem-int/lit16 v4, v3, 0x80

    .line 68
    .line 69
    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 70
    .line 71
    rem-int/lit8 v3, v3, 0x2

    .line 72
    .line 73
    goto :goto_3

    .line 74
    :cond_3
    :goto_2
    const/4 v1, 0x1

    .line 75
    :goto_3
    if-nez v1, :cond_4

    .line 76
    .line 77
    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    :cond_4
    return-void

    .line 81
    :cond_5
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p1, v2}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    const/4 p1, 0x0

    .line 92
    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :catchall_0
    move-exception p1

    .line 94
    throw p1
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private onResponseNative(Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger:Lcom/appsflyer/internal/AFe1hSDK;

    .line 7
    .line 8
    iget-object v0, v0, Lcom/appsflyer/internal/AFe1hSDK;->AFLogger:Ljava/lang/String;

    .line 9
    .line 10
    const/16 v1, 0x1e

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const/16 v2, 0x1e

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/16 v2, 0x1a

    .line 18
    .line 19
    :goto_0
    if-eq v2, v1, :cond_1

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_2

    .line 27
    .line 28
    :goto_1
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x37

    .line 31
    .line 32
    rem-int/lit16 v2, v1, 0x80

    .line 33
    .line 34
    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 35
    .line 36
    rem-int/lit8 v1, v1, 0x2

    .line 37
    .line 38
    const/4 v1, 0x1

    .line 39
    goto :goto_2

    .line 40
    :cond_2
    const/4 v1, 0x0

    .line 41
    :goto_2
    const/4 v2, 0x7

    .line 42
    if-nez v1, :cond_3

    .line 43
    .line 44
    const/16 v1, 0x42

    .line 45
    .line 46
    goto :goto_3

    .line 47
    :cond_3
    const/4 v1, 0x7

    .line 48
    :goto_3
    if-eq v1, v2, :cond_4

    .line 49
    .line 50
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    .line 51
    .line 52
    add-int/lit8 v1, v1, 0x49

    .line 53
    .line 54
    rem-int/lit16 v2, v1, 0x80

    .line 55
    .line 56
    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    .line 57
    .line 58
    rem-int/lit8 v1, v1, 0x2

    .line 59
    .line 60
    const-string v1, "appsflyerKey"

    .line 61
    .line 62
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    :cond_4
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private valueOf()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;,
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .line 20
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x35

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFa1eSDK;->valueOf(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFa1eSDK;->valueOf(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private final valueOf(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 26
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x6b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x3a

    if-eqz v0, :cond_0

    const/16 v0, 0x5f

    goto :goto_0

    :cond_0
    const/16 v0, 0x3a

    :goto_0
    const-string v2, "collectAndroidId"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq v0, v1, :cond_2

    .line 27
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v2, v4}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_1

    const/16 v0, 0x23

    goto :goto_1

    :cond_1
    const/4 v0, 0x3

    :goto_1
    if-eq v0, v1, :cond_a

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v2, v5}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 28
    :goto_2
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v0, 0x1f

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_9

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_6

    add-int/lit8 v0, v0, 0x73

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 29
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_4

    goto :goto_4

    :cond_4
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x3b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x0

    goto :goto_5

    .line 30
    :cond_5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    :try_start_0
    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :cond_6
    :goto_4
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x4b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_a

    .line 31
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->onAppOpenAttributionNative()Z

    move-result p1

    if-eqz p1, :cond_7

    const/4 v4, 0x0

    :cond_7
    if-eqz v4, :cond_8

    goto :goto_7

    .line 32
    :cond_8
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x59

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p1, p1, 0x2

    .line 33
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFVersionDeclaration()Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 34
    :cond_9
    :try_start_1
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    throw p1

    :cond_a
    const/16 v0, 0x40

    if-eqz p1, :cond_b

    const/16 v1, 0x40

    goto :goto_6

    :cond_b
    const/16 v1, 0x43

    :goto_6
    if-eq v1, v0, :cond_c

    :goto_7
    move-object p1, v3

    :cond_c
    :goto_8
    return-object p1
.end method

.method private valueOf(Ljava/util/Map;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 35
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x3f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const-string v1, ""

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x37

    .line 36
    :try_start_0
    div-int/2addr v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_3

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 37
    throw p1

    .line 38
    :cond_0
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x18

    if-eqz p2, :cond_1

    const/16 v1, 0x18

    goto :goto_0

    :cond_1
    const/16 v1, 0x5a

    :goto_0
    if-eq v1, v0, :cond_2

    goto :goto_2

    .line 39
    :cond_2
    :goto_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    :goto_2
    const/4 v0, 0x1

    goto :goto_3

    :cond_4
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x7

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x0

    :goto_3
    const/16 v1, 0x15

    if-nez v0, :cond_5

    const/16 v0, 0xc

    goto :goto_4

    :cond_5
    const/16 v0, 0x15

    :goto_4
    const-string v4, "referrer"

    if-eq v0, v1, :cond_6

    .line 40
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x33

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    .line 41
    invoke-interface {p1, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    :cond_6
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    const/4 v0, 0x0

    const-string v1, "extraReferrers"

    invoke-interface {p2, v1, v0}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_7

    const/4 v0, 0x1

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    :goto_5
    if-eqz v0, :cond_8

    .line 43
    invoke-interface {p1, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_8
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object p2

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog:Lcom/appsflyer/internal/AFc1uSDK;

    invoke-virtual {p2, v0}, Lcom/appsflyer/AppsFlyerProperties;->getReferrer(Lcom/appsflyer/internal/AFc1uSDK;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_a

    .line 45
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_9

    goto :goto_6

    :cond_9
    const/4 v0, 0x0

    goto :goto_7

    :cond_a
    :goto_6
    const/4 v0, 0x1

    :goto_7
    const/16 v1, 0x1c

    if-nez v0, :cond_b

    const/16 v0, 0x1c

    goto :goto_8

    :cond_b
    const/16 v0, 0x16

    :goto_8
    if-eq v0, v1, :cond_c

    goto :goto_b

    .line 46
    :cond_c
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x77

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    goto :goto_9

    :cond_d
    const/4 v0, 0x1

    :goto_9
    if-eq v0, v2, :cond_e

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/16 v1, 0x23

    :try_start_1
    div-int/2addr v1, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_f

    goto :goto_a

    :catchall_1
    move-exception p1

    throw p1

    .line 47
    :cond_e
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_f

    .line 48
    :goto_a
    invoke-interface {p1, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    :goto_b
    return-void
.end method

.method private final values()Lcom/appsflyer/AppsFlyerProperties;
    .locals 2

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x21

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x1a

    if-eqz v0, :cond_0

    const/16 v0, 0x1a

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afWarnLog:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/appsflyer/AppsFlyerProperties;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->afWarnLog:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/appsflyer/AppsFlyerProperties;

    const/4 v0, 0x0

    :try_start_0
    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method private static values(Ljava/lang/String;)Ljava/io/File;
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 40
    :try_start_0
    invoke-static {p0}, Lkotlin/text/StringsKt;->O0oO008(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v2, 0x0

    if-lez v1, :cond_1

    .line 41
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0x7d

    rem-int/lit16 v3, v1, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eq v1, v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_4

    .line 42
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lkotlin/text/StringsKt;->O0oO008(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 43
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p0, p0, 0x69

    rem-int/lit16 v2, p0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p0, p0, 0x2

    const/16 v2, 0x50

    if-eqz p0, :cond_2

    const/16 p0, 0x50

    goto :goto_1

    :cond_2
    const/16 p0, 0x51

    :goto_1
    if-eq p0, v2, :cond_3

    return-object v1

    :cond_3
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :catchall_1
    move-exception p0

    .line 44
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    return-object v0
.end method

.method private static values(Lcom/appsflyer/internal/AFc1uSDK;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, "CACHED_CHANNEL"

    const/4 v1, 0x0

    .line 37
    invoke-interface {p0, v0, v1}, Lcom/appsflyer/internal/AFc1uSDK;->AFInAppEventParameterName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x13

    if-eqz v2, :cond_0

    const/16 v4, 0x13

    goto :goto_0

    :cond_0
    const/16 v4, 0x55

    :goto_0
    if-eq v4, v3, :cond_3

    .line 38
    invoke-interface {p0, v0, p1}, Lcom/appsflyer/internal/AFc1uSDK;->AFKeystoreWrapper(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p0, p0, 0x31

    rem-int/lit16 v0, p0, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    if-nez p0, :cond_2

    return-object p1

    :cond_2
    :try_start_0
    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p0

    throw p0

    :cond_3
    sget p0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p0, p0, 0x67

    rem-int/lit16 p1, p0, 0x80

    sput p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p0, p0, 0x2

    return-object v2
.end method

.method private values(Ljava/util/Map;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    iget-object v1, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/WindowManager;

    const/16 v3, 0x2d

    if-eqz v2, :cond_0

    const/16 v2, 0x2d

    goto :goto_0

    :cond_0
    const/16 v2, 0x57

    :goto_0
    const/4 v4, 0x2

    if-eq v2, v3, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    .line 5
    :cond_1
    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v2, v2, 0x19

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/2addr v2, v4

    .line 6
    check-cast v1, Landroid/view/WindowManager;

    :goto_1
    const/16 v2, 0x47

    const/4 v3, 0x3

    if-nez v1, :cond_2

    const/16 v5, 0x47

    goto :goto_2

    :cond_2
    const/4 v5, 0x3

    :goto_2
    if-eq v5, v2, :cond_7

    .line 7
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 8
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    if-eqz v1, :cond_6

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    if-eq v1, v4, :cond_4

    if-eq v1, v3, :cond_3

    goto :goto_3

    :cond_3
    const-string v0, "lr"

    goto :goto_3

    :cond_4
    const-string v0, "pr"

    goto :goto_3

    :cond_5
    const-string v0, "l"

    goto :goto_3

    :cond_6
    const-string v0, "p"

    :goto_3
    const-string v1, "sc_o"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_7
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, p1, 0x39

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/2addr v0, v4

    add-int/lit8 p1, p1, 0x59

    rem-int/lit16 v0, p1, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/2addr p1, v4

    return-void
.end method

.method private values(Ljava/util/Map;Z)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->getLevel:Lcom/appsflyer/internal/AFb1vSDK;

    invoke-virtual {v0}, Lcom/appsflyer/internal/AFb1vSDK;->AFKeystoreWrapper()Ljava/lang/String;

    move-result-object v0

    const-string v1, "platformextension"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 3
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p2, p2, 0x23

    rem-int/lit16 v0, p2, 0x80

    sput v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p2, p2, 0x2

    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->valueOf:Lcom/appsflyer/internal/AFf1aSDK;

    invoke-interface {p2}, Lcom/appsflyer/internal/AFf1aSDK;->AFInAppEventType()Ljava/util/Map;

    move-result-object p2

    const-string v0, "platform_extension_v2"

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x49

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    :cond_1
    return-void
.end method

.method private values(Ljava/util/Map;ZI)V
    .locals 5
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;ZI)V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ro.product.cpu.abi"

    .line 10
    invoke-static {v1}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cpu_abi"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ro.product.cpu.abi2"

    .line 11
    invoke-static {v1}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cpu_abi2"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "os.arch"

    .line 12
    invoke-static {v1}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "arch"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ro.build.display.id"

    .line 13
    invoke-static {v1}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "build_display_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz p2, :cond_1

    .line 14
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p2, p2, 0x29

    rem-int/lit16 v4, p2, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/2addr p2, v3

    .line 15
    invoke-direct {p0, v0}, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog(Ljava/util/Map;)V

    if-gt p3, v3, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    :goto_0
    if-eq p2, v1, :cond_1

    .line 16
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->values:Lcom/appsflyer/internal/AFg1hSDK;

    invoke-interface {p2}, Lcom/appsflyer/internal/AFg1hSDK;->AFInAppEventType()Ljava/util/Map;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 17
    :cond_1
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFa1iSDK;

    iget-object p3, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-interface {p2, p3}, Lcom/appsflyer/internal/AFa1iSDK;->AFKeystoreWrapper(Landroid/content/Context;)Ljava/util/Map;

    move-result-object p2

    const-string p3, "dim"

    .line 18
    invoke-interface {v0, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "deviceData"

    .line 19
    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x21

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/2addr p1, v3

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    const/16 p1, 0x57

    :try_start_0
    div-int/2addr p1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    throw p1

    :cond_3
    return-void
.end method


# virtual methods
.method public final AFInAppEventParameterName(Ljava/util/Map;Lcom/appsflyer/internal/AFb1aSDK;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/appsflyer/internal/AFb1aSDK;",
            ")V"
        }
    .end annotation

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "appid"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 27
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v3, v3, 0x4b

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    .line 28
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 29
    throw p1

    .line 30
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "currencyCode"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 31
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v1, v1, 0x35

    rem-int/lit16 v3, v1, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    .line 32
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v3, 0x4

    if-eq v1, v3, :cond_3

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_3

    .line 33
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "WARNING: currency code should be 3 characters!!! \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\' is not a legal value."

    .line 35
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 37
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    :cond_3
    const-string v1, "currency"

    .line 38
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    :cond_4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "IS_UPDATE"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_5

    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_6

    goto :goto_4

    .line 40
    :cond_6
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v4, v4, 0x7

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    :goto_3
    const-string v5, "isUpdate"

    if-eq v4, v3, :cond_12

    .line 41
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    :goto_4
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v4, "additionalCustomData"

    invoke-virtual {v0, v4}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v4, 0x1

    goto :goto_5

    :cond_8
    const/4 v4, 0x0

    :goto_5
    if-eq v4, v3, :cond_9

    .line 43
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x31

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    goto :goto_6

    :cond_9
    const-string v4, "customData"

    .line 44
    invoke-interface {p1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x53

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    .line 46
    :goto_6
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v4, "AppUserId"

    invoke-virtual {v0, v4}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 47
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v4, v4, 0x9

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v4, v4, 0x2

    const-string v5, "appUserId"

    if-nez v4, :cond_a

    .line 48
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x3e

    :try_start_1
    div-int/2addr v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_7

    :catchall_1
    move-exception p1

    .line 49
    throw p1

    .line 50
    :cond_a
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_b
    :goto_7
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v4, "userEmails"

    invoke-virtual {v0, v4}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x4c

    if-eqz v0, :cond_c

    const/16 v5, 0x4c

    goto :goto_8

    :cond_c
    const/16 v5, 0xe

    :goto_8
    if-eq v5, v4, :cond_d

    goto :goto_9

    .line 52
    :cond_d
    sget v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v4, v4, 0x2d

    rem-int/lit16 v5, v4, 0x80

    sput v5, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v4, v4, 0x2

    const-string v5, "user_emails"

    if-nez v4, :cond_11

    .line 53
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_9
    if-eqz p2, :cond_10

    .line 54
    iget-object p2, p2, Lcom/appsflyer/internal/AFb1aSDK;->values:[Ljava/lang/String;

    if-eqz p2, :cond_e

    goto :goto_a

    :cond_e
    const/4 v1, 0x1

    :goto_a
    if-eqz v1, :cond_f

    goto :goto_b

    :cond_f
    const-string v0, "sharing_filter"

    .line 55
    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_10
    :goto_b
    return-void

    .line 56
    :cond_11
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception p1

    .line 57
    throw p1

    .line 58
    :cond_12
    invoke-interface {p1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :catchall_3
    move-exception p1

    .line 59
    throw p1
.end method

.method public final AFInAppEventType()J
    .locals 4

    .line 8
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x45

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget v2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v2, v2, 0x43

    rem-int/lit16 v3, v2, 0x80

    sput v3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v2, v2, 0x2

    return-wide v0
.end method

.method public final AFInAppEventType(Ljava/util/Map;)V
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 9
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    const/16 v1, 0x37

    add-int/2addr v0, v1

    rem-int/lit16 v2, v0, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v2, 0x27

    if-eqz v0, :cond_0

    const/16 v0, 0x3e

    goto :goto_0

    :cond_0
    const/16 v0, 0x27

    :goto_0
    const-string v3, "network"

    const-string v4, "disableCollectNetworkData"

    const-string v5, ""

    if-eq v0, v2, :cond_1

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v4, v2}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 11
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afInfoLog:Lcom/appsflyer/internal/AFa1hSDK;

    iget-object v4, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/appsflyer/internal/AFa1hSDK;->AFInAppEventType(Landroid/content/Context;)Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;

    move-result-object v2

    .line 12
    iget-object v4, v2, Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;->values:Ljava/lang/String;

    .line 13
    invoke-interface {p1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_2

    goto :goto_1

    .line 14
    :cond_1
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Lcom/appsflyer/AppsFlyerProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 16
    iget-object v2, p0, Lcom/appsflyer/internal/AFe1dSDK;->afInfoLog:Lcom/appsflyer/internal/AFa1hSDK;

    iget-object v4, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/appsflyer/internal/AFa1hSDK;->AFInAppEventType(Landroid/content/Context;)Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;

    move-result-object v2

    .line 17
    iget-object v4, v2, Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;->values:Ljava/lang/String;

    .line 18
    invoke-interface {p1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_2

    :goto_1
    return-void

    .line 19
    :cond_2
    iget-object v0, v2, Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;->AFKeystoreWrapper:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 20
    sget v3, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v3, v3, 0x75

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v3, v3, 0x2

    const-string v3, "operator"

    .line 21
    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    :cond_3
    iget-object v0, v2, Lcom/appsflyer/internal/AFa1hSDK$AFa1zSDK;->AFInAppEventType:Ljava/lang/String;

    const/16 v2, 0x1f

    if-eqz v0, :cond_4

    const/16 v1, 0x1f

    :cond_4
    if-eq v1, v2, :cond_5

    return-void

    .line 23
    :cond_5
    sget v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v1, v1, 0xd

    rem-int/lit16 v2, v1, 0x80

    sput v2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v1, v1, 0x2

    const-string v2, "carrier"

    if-nez v1, :cond_6

    .line 24
    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_6
    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 25
    throw p1
.end method

.method public final AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;)V
    .locals 5
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, ""

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    move-result-object v1

    .line 37
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "open_referrer"

    .line 38
    iget-object v2, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType:Ljava/lang/String;

    .line 39
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFLogger:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    .line 41
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 42
    :cond_1
    :goto_0
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x51

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    :goto_2
    if-eq v0, v3, :cond_5

    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x33

    rem-int/lit16 v4, v0, 0x80

    sput v4, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    const/4 v2, 0x1

    :goto_3
    const-string v0, "af_web_referrer"

    if-ne v2, v3, :cond_4

    .line 43
    iget-object p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFLogger:Ljava/lang/String;

    .line 44
    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 45
    :cond_4
    iget-object p1, p1, Lcom/appsflyer/internal/AFa1uSDK;->AFLogger:Ljava/lang/String;

    .line 46
    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    :try_start_0
    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    throw p1

    :cond_5
    :goto_4
    return-void
.end method

.method public final AFKeystoreWrapper(Lcom/appsflyer/internal/AFa1uSDK;Ljava/lang/String;Lcom/appsflyer/internal/AFc1xSDK;)V
    .locals 4
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x3b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    move-result-object v1

    .line 4
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result v2

    const/16 v3, 0x2a

    if-eqz v2, :cond_0

    const/16 v2, 0x3d

    goto :goto_0

    :cond_0
    const/16 v2, 0x2a

    :goto_0
    if-eq v2, v3, :cond_1

    .line 5
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object v0, p1, Lcom/appsflyer/internal/AFa1uSDK;->afDebugLog:Ljava/lang/String;

    .line 7
    invoke-direct {p0, v1, v0, p2, p3}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/appsflyer/internal/AFc1xSDK;)V

    .line 8
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p2, p2, 0x67

    rem-int/lit16 p3, p2, 0x80

    sput p3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p2, p2, 0x2

    goto :goto_1

    .line 9
    :cond_1
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    iget-object p2, p1, Lcom/appsflyer/internal/AFa1uSDK;->afInfoLog:Ljava/lang/String;

    .line 11
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/util/Map;Ljava/lang/String;)V

    .line 12
    :goto_1
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->onResponseNative(Ljava/util/Map;)V

    .line 13
    invoke-static {v1}, Lcom/appsflyer/internal/AFe1dSDK;->getLevel(Ljava/util/Map;)V

    .line 14
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->init(Ljava/util/Map;)V

    .line 15
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative(Ljava/util/Map;)V

    .line 16
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->onAppOpenAttributionNative(Ljava/util/Map;)V

    .line 17
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result p2

    invoke-direct {p0, v1, p2}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/util/Map;Z)V

    .line 18
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->onConversionDataSuccess(Ljava/util/Map;)V

    .line 19
    invoke-direct {p0, v1}, Lcom/appsflyer/internal/AFe1dSDK;->onAttributionFailureNative(Ljava/util/Map;)V

    .line 20
    invoke-static {v1, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/util/Map;Lcom/appsflyer/internal/AFa1uSDK;)V

    const-string p1, "af_events_api"

    const-string p2, "1"

    .line 21
    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final AFKeystoreWrapper(Ljava/util/Map;ZLkotlin/jvm/functions/Function0;)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 22
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x2b

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 23
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper(Ljava/util/Map;)V

    .line 25
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/util/Map;)V

    .line 26
    invoke-direct {p0, p1}, Lcom/appsflyer/internal/AFe1dSDK;->AppsFlyer2dXConversionCallback(Ljava/util/Map;)V

    .line 27
    invoke-static {p1, p2}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName(Ljava/util/Map;Z)V

    .line 28
    invoke-direct {p0, p1, p3}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/util/Map;Lkotlin/jvm/functions/Function0;)V

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x71

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    return-void
.end method

.method public final valueOf(Lcom/appsflyer/internal/AFa1uSDK;ILjava/lang/String;)V
    .locals 3
    .param p1    # Lcom/appsflyer/internal/AFa1uSDK;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x2d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    const/4 v1, 0x2

    rem-int/2addr v0, v1

    const-string v0, ""

    .line 2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFInAppEventType()Ljava/util/Map;

    move-result-object v2

    .line 4
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/appsflyer/internal/AFa1uSDK;->AFKeystoreWrapper()Z

    move-result v0

    invoke-direct {p0, v2, v0, p2}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/util/Map;ZI)V

    .line 5
    invoke-direct {p0, p1, v2}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFa1uSDK;Ljava/util/Map;)V

    .line 6
    invoke-static {v2}, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog(Ljava/util/Map;)V

    .line 7
    invoke-direct {p0, v2}, Lcom/appsflyer/internal/AFe1dSDK;->values(Ljava/util/Map;)V

    .line 8
    invoke-static {v2}, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionDataLoadedNative(Ljava/util/Map;)V

    .line 9
    invoke-virtual {p0, v2}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/util/Map;)V

    .line 10
    invoke-direct {p0, v2, p3}, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventType(Ljava/util/Map;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, v2}, Lcom/appsflyer/internal/AFe1dSDK;->onDeepLinkingNative(Ljava/util/Map;)V

    .line 12
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->AFLogger$LogLevel()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string p2, "ivc"

    invoke-interface {v2, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array p1, v1, [Lkotlin/Pair;

    .line 13
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    iget p2, p2, Landroid/content/res/Configuration;->mcc:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "mcc"

    invoke-static {p3, p2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, p1, p3

    .line 14
    iget-object p2, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    iget p2, p2, Landroid/content/res/Configuration;->mnc:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "mnc"

    invoke-static {p3, p2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, p1, p3

    .line 15
    invoke-static {p1}, Lkotlin/collections/MapsKt;->〇80〇808〇O([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    const-string p2, "cell"

    .line 16
    invoke-interface {v2, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "sig"

    .line 17
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->valueOf()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    invoke-static {}, Lcom/appsflyer/internal/AFe1dSDK;->AFKeystoreWrapper()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "last_boot_time"

    invoke-interface {v2, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "disk"

    .line 19
    invoke-static {}, Lcom/appsflyer/internal/AFe1dSDK;->afDebugLog()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p1, p1, 0x2f

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/2addr p1, v1

    return-void
.end method

.method public final valueOf(Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 21
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x4d

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 22
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    const-string v1, "advertiserId"

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x63

    if-nez v0, :cond_0

    const/16 v0, 0x63

    goto :goto_0

    :cond_0
    const/16 v0, 0x5b

    :goto_0
    if-eq v0, v2, :cond_1

    goto :goto_3

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/appsflyer/internal/AFe1dSDK;->AFInAppEventParameterName:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/appsflyer/internal/AFa1dSDK;->AFKeystoreWrapper(Landroid/content/Context;Ljava/util/Map;)Lcom/appsflyer/internal/AFa1bSDK;

    .line 25
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->values()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x1f

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GAID_retry"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    return-void
.end method

.method public final values(Ljava/util/Map;II)V
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;II)V"
        }
    .end annotation

    .line 20
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 v0, v0, 0x39

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 v0, v0, 0x2

    const-string v0, ""

    .line 21
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "counter"

    .line 22
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "iaecounter"

    .line 23
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-direct {p0}, Lcom/appsflyer/internal/AFe1dSDK;->afRDLog()Z

    move-result p2

    const/16 p3, 0x8

    if-nez p2, :cond_0

    const/16 p2, 0x8

    goto :goto_0

    :cond_0
    const/16 p2, 0x3d

    :goto_0
    if-eq p2, p3, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    sget p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    add-int/lit8 p2, p2, 0x37

    rem-int/lit16 p3, p2, 0x80

    sput p3, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    rem-int/lit8 p2, p2, 0x2

    const/4 p2, 0x1

    :goto_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    const-string p3, "isFirstCall"

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final values(Ljava/util/Map;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 25
    sget v0, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 v0, v0, 0x49

    rem-int/lit16 v1, v0, 0x80

    sput v1, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 v0, v0, 0x2

    const/16 v1, 0x11

    if-nez v0, :cond_0

    const/16 v0, 0x11

    goto :goto_0

    :cond_0
    const/16 v0, 0x48

    :goto_0
    const-string v2, ""

    if-eq v0, v1, :cond_7

    .line 26
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x63

    if-eqz p2, :cond_1

    const/16 v1, 0x63

    goto :goto_1

    :cond_1
    const/16 v1, 0x46

    :goto_1
    if-eq v1, v0, :cond_2

    goto :goto_5

    :cond_2
    const-string v0, "af_deeplink"

    .line 27
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    :goto_2
    if-eq v1, v3, :cond_6

    .line 28
    sget p1, Lcom/appsflyer/internal/AFe1dSDK;->onInstallConversionFailureNative:I

    add-int/lit8 p1, p1, 0x1b

    rem-int/lit16 p2, p1, 0x80

    sput p2, Lcom/appsflyer/internal/AFe1dSDK;->init:I

    rem-int/lit8 p1, p1, 0x2

    const/16 p2, 0x24

    if-nez p1, :cond_4

    const/16 p1, 0x24

    goto :goto_3

    :cond_4
    const/16 p1, 0xb

    :goto_3
    const-string v0, "Skip \'af\' payload as deeplink was found by path"

    if-eq p1, p2, :cond_5

    .line 29
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    const/16 p1, 0x36

    :try_start_0
    div-int/2addr p1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_4
    return-void

    :catchall_0
    move-exception p1

    .line 30
    throw p1

    .line 31
    :cond_6
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p2, "isPush"

    const-string v2, "true"

    .line 32
    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 33
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    return-void

    .line 34
    :cond_7
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 35
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    .line 36
    throw p1
.end method

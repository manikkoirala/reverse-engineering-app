.class public interface abstract Lcom/appsflyer/internal/AFc1qSDK;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract AFInAppEventParameterName()Lcom/appsflyer/internal/AFd1zSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AFInAppEventType()Lcom/appsflyer/internal/AFc1tSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AFKeystoreWrapper()Ljava/util/concurrent/ExecutorService;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AFLogger()Lcom/appsflyer/internal/AFf1tSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AFLogger$LogLevel()Lcom/appsflyer/internal/AFg1nSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AFVersionDeclaration()Lcom/appsflyer/internal/AFc1rSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract AppsFlyer2dXConversionCallback()Lcom/appsflyer/internal/AFf1aSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afDebugLog()Lcom/appsflyer/internal/AFe1aSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afErrorLog()Lcom/appsflyer/internal/AFe1oSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afErrorLogForExcManagerOnly()Lcom/appsflyer/internal/AFd1fSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afInfoLog()Lcom/appsflyer/PurchaseHandler;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afRDLog()Lcom/appsflyer/internal/AFg1hSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract afWarnLog()Lcom/appsflyer/internal/AFe1hSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract getLevel()Lcom/appsflyer/internal/AFb1sSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract init()Lcom/appsflyer/internal/AFb1fSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onAppOpenAttributionNative()Lcom/appsflyer/internal/AFb1xSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onConversionDataSuccess()Lcom/appsflyer/internal/AFc1ySDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onInstallConversionDataLoadedNative()Lcom/appsflyer/internal/AFd1rSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onInstallConversionFailureNative()Lcom/appsflyer/internal/AFb1kSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onResponseErrorNative()Lcom/appsflyer/internal/AFg1wSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract onResponseNative()Lcom/appsflyer/internal/AFc1jSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract valueOf()Ljava/util/concurrent/ScheduledExecutorService;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract values()Lcom/appsflyer/internal/AFc1uSDK;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

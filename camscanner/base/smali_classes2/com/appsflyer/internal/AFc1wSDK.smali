.class public final Lcom/appsflyer/internal/AFc1wSDK;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/appsflyer/internal/AFc1wSDK$AFa1vSDK;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static $10:I = 0x0

.field private static $11:I = 0x1

.field private static AFInAppEventParameterName:[C = null

.field private static AFKeystoreWrapper:I = 0x0

.field private static afInfoLog:I = 0x1

.field private static values:J


# instance fields
.field private final AFInAppEventType:Landroid/content/Context;

.field private final valueOf:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 5
    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 9
    .line 10
    .line 11
    const-string v1, ""

    .line 12
    .line 13
    invoke-static {v1, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 14
    .line 15
    .line 16
    invoke-static {v0, v0, v0}, Landroid/view/View;->resolveSizeAndState(III)I

    .line 17
    .line 18
    .line 19
    invoke-static {}, Landroid/view/ViewConfiguration;->getJumpTapTimeout()I

    .line 20
    .line 21
    .line 22
    sget v0, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 23
    .line 24
    add-int/lit8 v0, v0, 0x77

    .line 25
    .line 26
    rem-int/lit16 v1, v0, 0x80

    .line 27
    .line 28
    sput v1, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 29
    .line 30
    rem-int/lit8 v0, v0, 0x2

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>(Ljava/util/Map;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventType:Landroid/content/Context;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventType()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-direct {p0}, Lcom/appsflyer/internal/AFc1wSDK;->valueOf()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    invoke-virtual {p0, p1, p2}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private AFInAppEventType()Ljava/lang/String;
    .locals 11
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    sget v1, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 4
    .line 5
    add-int/lit8 v1, v1, 0x5d

    .line 6
    .line 7
    rem-int/lit16 v2, v1, 0x80

    .line 8
    .line 9
    sput v2, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 10
    .line 11
    const/4 v2, 0x2

    .line 12
    rem-int/2addr v1, v2

    .line 13
    const/4 v1, 0x4

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x1

    .line 16
    :try_start_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 17
    .line 18
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v5

    .line 22
    iget-object v6, p0, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 23
    .line 24
    const-string v7, "\u9098\u07a8\ubec8\u5528\ucc4c\u6487\u1bd6\ub20b\u2935\uc077\u78b2\uefd4"

    .line 25
    .line 26
    invoke-static {v0}, Landroid/os/Process;->getGidForName(Ljava/lang/String;)I

    .line 27
    .line 28
    .line 29
    move-result v8

    .line 30
    const v9, 0x9736

    .line 31
    .line 32
    .line 33
    sub-int/2addr v9, v8

    .line 34
    new-array v8, v4, [Ljava/lang/Object;

    .line 35
    .line 36
    invoke-static {v7, v9, v8}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    aget-object v7, v8, v3

    .line 40
    .line 41
    check-cast v7, Ljava/lang/String;

    .line 42
    .line 43
    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v7

    .line 47
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v6

    .line 55
    iget-object v7, p0, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 56
    .line 57
    const-string v8, "\u909b\u2818\ue1be\ub92e\u72d1"

    .line 58
    .line 59
    invoke-static {v3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 60
    .line 61
    .line 62
    move-result v9

    .line 63
    const v10, 0xb893

    .line 64
    .line 65
    .line 66
    add-int/2addr v9, v10

    .line 67
    new-array v10, v4, [Ljava/lang/Object;

    .line 68
    .line 69
    invoke-static {v8, v9, v10}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    aget-object v8, v10, v3

    .line 73
    .line 74
    check-cast v8, Ljava/lang/String;

    .line 75
    .line 76
    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v8

    .line 80
    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v7

    .line 84
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    const/16 v8, 0x3d

    .line 89
    .line 90
    if-nez v7, :cond_0

    .line 91
    .line 92
    const/16 v9, 0xa

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_0
    const/16 v9, 0x3d

    .line 96
    .line 97
    :goto_0
    if-eq v9, v8, :cond_1

    .line 98
    .line 99
    sget v7, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 100
    .line 101
    add-int/lit8 v7, v7, 0x59

    .line 102
    .line 103
    rem-int/lit16 v8, v7, 0x80

    .line 104
    .line 105
    sput v8, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 106
    .line 107
    rem-int/2addr v7, v2

    .line 108
    :try_start_1
    const-string v7, "\u90b7\ue9c5\u6240\ufce2\u7567\ucf87\u4805\uc298"

    .line 109
    .line 110
    const/16 v8, 0x30

    .line 111
    .line 112
    invoke-static {v0, v8}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;C)I

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    rsub-int v0, v0, 0x7972

    .line 117
    .line 118
    new-array v8, v4, [Ljava/lang/Object;

    .line 119
    .line 120
    invoke-static {v7, v0, v8}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 121
    .line 122
    .line 123
    aget-object v0, v8, v3

    .line 124
    .line 125
    check-cast v0, Ljava/lang/String;

    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v7

    .line 131
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 132
    .line 133
    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    const/4 v6, 0x3

    .line 140
    new-array v6, v6, [Ljava/lang/String;

    .line 141
    .line 142
    aput-object v5, v6, v3

    .line 143
    .line 144
    aput-object v7, v6, v4

    .line 145
    .line 146
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    aput-object v0, v6, v2

    .line 151
    .line 152
    invoke-static {v6}, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper([Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 157
    .line 158
    .line 159
    move-result v5

    .line 160
    if-le v5, v1, :cond_2

    .line 161
    .line 162
    invoke-virtual {v0, v1, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 163
    .line 164
    .line 165
    sget v5, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 166
    .line 167
    add-int/2addr v5, v4

    .line 168
    rem-int/lit16 v6, v5, 0x80

    .line 169
    .line 170
    sput v6, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 171
    .line 172
    rem-int/2addr v5, v2

    .line 173
    goto :goto_3

    .line 174
    :cond_2
    :goto_1
    if-ge v5, v1, :cond_3

    .line 175
    .line 176
    const/4 v2, 0x0

    .line 177
    goto :goto_2

    .line 178
    :cond_3
    const/4 v2, 0x1

    .line 179
    :goto_2
    if-eqz v2, :cond_4

    .line 180
    .line 181
    :goto_3
    :try_start_2
    const-string v2, "\u9092\u85c3\uba21"

    .line 182
    .line 183
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    .line 184
    .line 185
    .line 186
    move-result v5

    .line 187
    shr-int/lit8 v5, v5, 0x10

    .line 188
    .line 189
    add-int/lit16 v5, v5, 0x155f

    .line 190
    .line 191
    new-array v6, v4, [Ljava/lang/Object;

    .line 192
    .line 193
    invoke-static {v2, v5, v6}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 194
    .line 195
    .line 196
    aget-object v2, v6, v3

    .line 197
    .line 198
    check-cast v2, Ljava/lang/String;

    .line 199
    .line 200
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v2

    .line 204
    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v0

    .line 211
    return-object v0

    .line 212
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 213
    .line 214
    const/16 v2, 0x31

    .line 215
    .line 216
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 217
    .line 218
    .line 219
    goto :goto_1

    .line 220
    :catch_0
    move-exception v0

    .line 221
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    .line 222
    .line 223
    .line 224
    move-result v2

    .line 225
    shr-int/lit8 v2, v2, 0x10

    .line 226
    .line 227
    rsub-int v2, v2, 0x7f7

    .line 228
    .line 229
    new-array v5, v4, [Ljava/lang/Object;

    .line 230
    .line 231
    const-string v6, "\u90d9\u9769\u9f72\u8768\u8f73\ub718\ubf78\ua75d\uaf38\ud776\udf39\uc705\ucf04\uf71e\uff1e\ue7e4\uefa9\u17f9\u1fc2\u07c2\u0fd0\u37c8\u3fa2\u27bc\u2fb8\u5788\u5f88\u47d4\u4f96\u7667\u7e6d\u6630\u6e72\u964b\u9e4e\u861c\u8e32\ub623\ube27\ua630"

    .line 232
    .line 233
    invoke-static {v6, v2, v5}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 234
    .line 235
    .line 236
    aget-object v2, v5, v3

    .line 237
    .line 238
    check-cast v2, Ljava/lang/String;

    .line 239
    .line 240
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v2

    .line 244
    invoke-static {v2, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 245
    .line 246
    .line 247
    new-instance v2, Ljava/lang/StringBuilder;

    .line 248
    .line 249
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .line 251
    .line 252
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    .line 253
    .line 254
    .line 255
    move-result v5

    .line 256
    const/4 v6, 0x0

    .line 257
    const-string v7, "\u909f\u6849\u6132\u7ae6\u73d8\u4c88\u443f\u5d29\u5614\u2fce\u28b6\u2070\u3954\u3210\u0bfe\u04a8\u1d8e\u1538\uee20\ue71f\ue0cb\uf9fc\uf164\uca5b\uc318\udcb0\ud5b4\uae9b\ua651\ubf3c\ub8a7\ub1d3\u8aa1\u826b\u9b5e\u941a\u6de9\u66a5\u7f90\u7740\u706b\u49a0"

    .line 258
    .line 259
    const v8, 0xf8d2

    .line 260
    .line 261
    .line 262
    const-string v9, "\u0000\u0000\u0000\u0000\u0000\u0000\u0001"

    .line 263
    .line 264
    cmpl-float v5, v5, v6

    .line 265
    .line 266
    sub-int/2addr v8, v5

    .line 267
    new-array v5, v4, [Ljava/lang/Object;

    .line 268
    .line 269
    invoke-static {v7, v8, v5}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 270
    .line 271
    .line 272
    aget-object v5, v5, v3

    .line 273
    .line 274
    check-cast v5, Ljava/lang/String;

    .line 275
    .line 276
    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 277
    .line 278
    .line 279
    move-result-object v5

    .line 280
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 291
    .line 292
    .line 293
    new-array v0, v1, [I

    .line 294
    .line 295
    fill-array-data v0, :array_0

    .line 296
    .line 297
    .line 298
    new-array v1, v4, [Ljava/lang/Object;

    .line 299
    .line 300
    invoke-static {v0, v9, v4, v1}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 301
    .line 302
    .line 303
    aget-object v0, v1, v3

    .line 304
    .line 305
    check-cast v0, Ljava/lang/String;

    .line 306
    .line 307
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 308
    .line 309
    .line 310
    move-result-object v0

    .line 311
    return-object v0

    .line 312
    nop

    .line 313
    :array_0
    .array-data 4
        0x0
        0x7
        0x27
        0x2
    .end array-data
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method private static varargs AFKeystoreWrapper([Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 9
    .param p0    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    array-length v1, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    aget-object v3, p0, v2

    .line 4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    .line 5
    sget v3, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    add-int/lit8 v3, v3, 0x9

    rem-int/lit16 v4, v3, 0x80

    sput v4, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    rem-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 6
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 7
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    :goto_1
    const/16 v5, 0x3d

    if-ge v4, v0, :cond_1

    const/16 v6, 0x3d

    goto :goto_2

    :cond_1
    const/16 v6, 0x5b

    :goto_2
    if-eq v6, v5, :cond_2

    return-object v2

    :cond_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v3, :cond_5

    .line 9
    sget v7, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    add-int/lit8 v7, v7, 0x37

    rem-int/lit16 v8, v7, 0x80

    sput v8, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_3

    aget-object v7, p0, v6

    .line 10
    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x15

    .line 11
    :try_start_0
    div-int/2addr v8, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_4

    goto :goto_4

    :catchall_0
    move-exception p0

    .line 12
    throw p0

    .line 13
    :cond_3
    aget-object v7, p0, v6

    .line 14
    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-nez v5, :cond_4

    .line 15
    :goto_4
    sget v5, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    add-int/lit8 v5, v5, 0x4b

    rem-int/lit16 v8, v5, 0x80

    sput v8, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    rem-int/lit8 v5, v5, 0x2

    goto :goto_5

    .line 16
    :cond_4
    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    xor-int/2addr v7, v5

    :goto_5
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 17
    :cond_5
    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    .line 18
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method static AFKeystoreWrapper()V
    .locals 2

    .line 1
    const-wide v0, -0x51b12ca8e3072539L    # -1.2396584649473202E-85

    sput-wide v0, Lcom/appsflyer/internal/AFc1wSDK;->values:J

    const/16 v0, 0x9e

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventParameterName:[C

    return-void

    nop

    :array_0
    .array-data 2
        -0x7025s
        -0x70ees
        -0x701cs
        -0x7003s
        -0x7003s
        -0x7003s
        -0x7015s
        -0x7045s
        -0x7003s
        -0x7012s
        -0x7011s
        -0x7010s
        -0x7006s
        -0x7008s
        -0x700fs
        -0x7009s
        -0x7031s
        -0x7035s
        -0x7008s
        -0x700bs
        -0x7014s
        -0x700as
        -0x7079s
        -0x7054s
        -0x7053s
        -0x7054s
        -0x704es
        -0x7051s
        -0x7052s
        -0x7005s
        -0x700fs
        -0x7008s
        -0x7027s
        -0x7027s
        -0x703as
        -0x7005s
        -0x7013s
        -0x7010s
        -0x7021s
        -0x7022s
        -0x7002s
        -0x7008s
        -0x7009s
        -0x700bs
        -0x7007s
        -0x7021s
        -0x7022s
        -0x7005s
        -0x700cs
        -0x700cs
        -0x700as
        -0x700cs
        -0x7009s
        -0x700ds
        -0x700as
        -0x7009s
        -0x7022s
        -0x7028s
        -0x700bs
        -0x7008s
        -0x7022s
        -0x702as
        -0x700as
        -0x7005s
        -0x7013s
        -0x7010s
        -0x7010s
        -0x70a8s
        -0x70cfs
        -0x70a1s
        -0x70a2s
        -0x70cbs
        -0x70c6s
        -0x70cas
        -0x70a7s
        -0x70bbs
        -0x70bbs
        -0x70b9s
        -0x70b9s
        -0x70b9s
        -0x70a4s
        -0x70ccs
        -0x70cas
        -0x70ccs
        -0x702cs
        -0x70f2s
        -0x70f8s
        -0x70fcs
        -0x70f5s
        -0x70f2s
        -0x70f7s
        -0x70c3s
        -0x70cas
        -0x70c7s
        -0x70f9s
        -0x70fas
        -0x70c2s
        -0x70fcs
        -0x70f3s
        -0x70e6s
        -0x7100s
        -0x70e0s
        -0x70das
        -0x70e0s
        -0x70d9s
        -0x70d4s
        -0x70f5s
        -0x70c3s
        -0x70a3s
        -0x70dbs
        -0x70das
        -0x70a3s
        -0x70d9s
        -0x70f9s
        -0x70fbs
        -0x70d8s
        -0x70das
        -0x70des
        -0x70d9s
        -0x70dbs
        -0x70d5s
        -0x7059s
        -0x7017s
        -0x7009s
        -0x700cs
        -0x700as
        -0x7009s
        -0x700ds
        -0x700cs
        -0x700fs
        -0x700fs
        -0x700as
        -0x705cs
        -0x700fs
        -0x700cs
        -0x7013s
        -0x7014s
        -0x7013s
        -0x703cs
        -0x70ecs
        -0x7039s
        -0x70f0s
        -0x701bs
        -0x708ds
        -0x708ds
        -0x7093s
        -0x708cs
        -0x70a3s
        -0x70a3s
        -0x708as
        -0x708ds
        -0x7087s
        -0x70bcs
        -0x70a1s
        -0x70a1s
        -0x7082s
        -0x7089s
        -0x7087s
    .end array-data
.end method

.method private static a(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 10

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    :cond_0
    check-cast p0, [C

    .line 8
    .line 9
    new-instance v0, Lcom/appsflyer/internal/AFh1xSDK;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/appsflyer/internal/AFh1xSDK;-><init>()V

    .line 12
    .line 13
    .line 14
    iput p1, v0, Lcom/appsflyer/internal/AFh1xSDK;->AFInAppEventType:I

    .line 15
    .line 16
    array-length p1, p0

    .line 17
    new-array v1, p1, [J

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    iput v2, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 21
    .line 22
    :goto_0
    iget v3, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 23
    .line 24
    array-length v4, p0

    .line 25
    const/16 v5, 0x24

    .line 26
    .line 27
    if-ge v3, v4, :cond_1

    .line 28
    .line 29
    const/16 v4, 0x24

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    const/16 v4, 0x3b

    .line 33
    .line 34
    :goto_1
    if-eq v4, v5, :cond_4

    .line 35
    .line 36
    new-array p1, p1, [C

    .line 37
    .line 38
    iput v2, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 39
    .line 40
    sget v3, Lcom/appsflyer/internal/AFc1wSDK;->$10:I

    .line 41
    .line 42
    add-int/lit8 v3, v3, 0x2d

    .line 43
    .line 44
    rem-int/lit16 v4, v3, 0x80

    .line 45
    .line 46
    sput v4, Lcom/appsflyer/internal/AFc1wSDK;->$11:I

    .line 47
    .line 48
    rem-int/lit8 v3, v3, 0x2

    .line 49
    .line 50
    :goto_2
    iget v3, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 51
    .line 52
    array-length v4, p0

    .line 53
    const/4 v5, 0x1

    .line 54
    if-ge v3, v4, :cond_2

    .line 55
    .line 56
    const/4 v4, 0x0

    .line 57
    goto :goto_3

    .line 58
    :cond_2
    const/4 v4, 0x1

    .line 59
    :goto_3
    if-eq v4, v5, :cond_3

    .line 60
    .line 61
    aget-wide v4, v1, v3

    .line 62
    .line 63
    long-to-int v5, v4

    .line 64
    int-to-char v4, v5

    .line 65
    aput-char v4, p1, v3

    .line 66
    .line 67
    add-int/lit8 v3, v3, 0x1

    .line 68
    .line 69
    iput v3, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_3
    new-instance p0, Ljava/lang/String;

    .line 73
    .line 74
    invoke-direct {p0, p1}, Ljava/lang/String;-><init>([C)V

    .line 75
    .line 76
    .line 77
    sget p1, Lcom/appsflyer/internal/AFc1wSDK;->$10:I

    .line 78
    .line 79
    add-int/lit8 p1, p1, 0x25

    .line 80
    .line 81
    rem-int/lit16 v0, p1, 0x80

    .line 82
    .line 83
    sput v0, Lcom/appsflyer/internal/AFc1wSDK;->$11:I

    .line 84
    .line 85
    rem-int/lit8 p1, p1, 0x2

    .line 86
    .line 87
    aput-object p0, p2, v2

    .line 88
    .line 89
    return-void

    .line 90
    :cond_4
    aget-char v4, p0, v3

    .line 91
    .line 92
    int-to-long v4, v4

    .line 93
    int-to-long v6, v3

    .line 94
    iget v8, v0, Lcom/appsflyer/internal/AFh1xSDK;->AFInAppEventType:I

    .line 95
    .line 96
    int-to-long v8, v8

    .line 97
    mul-long v6, v6, v8

    .line 98
    .line 99
    xor-long/2addr v4, v6

    .line 100
    sget-wide v6, Lcom/appsflyer/internal/AFc1wSDK;->values:J

    .line 101
    .line 102
    const-wide v8, -0x365aa805a7e5b5c2L    # -6.092538953542472E46

    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    xor-long/2addr v6, v8

    .line 108
    xor-long/2addr v4, v6

    .line 109
    aput-wide v4, v1, v3

    .line 110
    .line 111
    add-int/lit8 v3, v3, 0x1

    .line 112
    .line 113
    iput v3, v0, Lcom/appsflyer/internal/AFh1xSDK;->valueOf:I

    .line 114
    .line 115
    goto :goto_0
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private static b([ILjava/lang/String;Z[Ljava/lang/Object;)V
    .locals 17

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v1, "ISO-8859-1"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    check-cast v0, [B

    .line 12
    .line 13
    new-instance v1, Lcom/appsflyer/internal/AFh1ySDK;

    .line 14
    .line 15
    invoke-direct {v1}, Lcom/appsflyer/internal/AFh1ySDK;-><init>()V

    .line 16
    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    aget v3, p0, v2

    .line 20
    .line 21
    const/4 v4, 0x1

    .line 22
    aget v5, p0, v4

    .line 23
    .line 24
    const/4 v6, 0x2

    .line 25
    aget v7, p0, v6

    .line 26
    .line 27
    const/4 v8, 0x3

    .line 28
    aget v8, p0, v8

    .line 29
    .line 30
    sget-object v9, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventParameterName:[C

    .line 31
    .line 32
    if-eqz v9, :cond_1

    .line 33
    .line 34
    const/4 v10, 0x1

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v10, 0x0

    .line 37
    :goto_0
    if-eqz v10, :cond_4

    .line 38
    .line 39
    sget v10, Lcom/appsflyer/internal/AFc1wSDK;->$10:I

    .line 40
    .line 41
    add-int/lit8 v10, v10, 0x25

    .line 42
    .line 43
    rem-int/lit16 v11, v10, 0x80

    .line 44
    .line 45
    sput v11, Lcom/appsflyer/internal/AFc1wSDK;->$11:I

    .line 46
    .line 47
    rem-int/2addr v10, v6

    .line 48
    if-nez v10, :cond_2

    .line 49
    .line 50
    array-length v10, v9

    .line 51
    new-array v11, v10, [C

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_2
    array-length v10, v9

    .line 55
    new-array v11, v10, [C

    .line 56
    .line 57
    :goto_1
    const/4 v12, 0x0

    .line 58
    :goto_2
    if-ge v12, v10, :cond_3

    .line 59
    .line 60
    aget-char v13, v9, v12

    .line 61
    .line 62
    int-to-long v13, v13

    .line 63
    const-wide v15, 0x29dae175cc2f8f9dL    # 4.578278272815995E-107

    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    xor-long/2addr v13, v15

    .line 69
    long-to-int v14, v13

    .line 70
    int-to-char v13, v14

    .line 71
    aput-char v13, v11, v12

    .line 72
    .line 73
    add-int/lit8 v12, v12, 0x1

    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_3
    move-object v9, v11

    .line 77
    :cond_4
    new-array v10, v5, [C

    .line 78
    .line 79
    invoke-static {v9, v3, v10, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    .line 81
    .line 82
    if-eqz v0, :cond_9

    .line 83
    .line 84
    sget v3, Lcom/appsflyer/internal/AFc1wSDK;->$10:I

    .line 85
    .line 86
    add-int/lit8 v3, v3, 0x71

    .line 87
    .line 88
    rem-int/lit16 v9, v3, 0x80

    .line 89
    .line 90
    sput v9, Lcom/appsflyer/internal/AFc1wSDK;->$11:I

    .line 91
    .line 92
    rem-int/2addr v3, v6

    .line 93
    if-nez v3, :cond_5

    .line 94
    .line 95
    new-array v3, v5, [C

    .line 96
    .line 97
    iput v4, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_5
    new-array v3, v5, [C

    .line 101
    .line 102
    iput v2, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 103
    .line 104
    :goto_3
    const/4 v9, 0x0

    .line 105
    :goto_4
    iget v11, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 106
    .line 107
    if-ge v11, v5, :cond_8

    .line 108
    .line 109
    aget-byte v12, v0, v11

    .line 110
    .line 111
    const/16 v13, 0x3d

    .line 112
    .line 113
    if-ne v12, v4, :cond_6

    .line 114
    .line 115
    const/16 v12, 0x5f

    .line 116
    .line 117
    goto :goto_5

    .line 118
    :cond_6
    const/16 v12, 0x3d

    .line 119
    .line 120
    :goto_5
    if-eq v12, v13, :cond_7

    .line 121
    .line 122
    aget-char v12, v10, v11

    .line 123
    .line 124
    mul-int/lit8 v12, v12, 0x2

    .line 125
    .line 126
    add-int/2addr v12, v4

    .line 127
    sub-int/2addr v12, v9

    .line 128
    int-to-char v9, v12

    .line 129
    aput-char v9, v3, v11

    .line 130
    .line 131
    goto :goto_6

    .line 132
    :cond_7
    aget-char v12, v10, v11

    .line 133
    .line 134
    mul-int/lit8 v12, v12, 0x2

    .line 135
    .line 136
    sub-int/2addr v12, v9

    .line 137
    int-to-char v9, v12

    .line 138
    aput-char v9, v3, v11

    .line 139
    .line 140
    :goto_6
    aget-char v9, v3, v11

    .line 141
    .line 142
    add-int/lit8 v11, v11, 0x1

    .line 143
    .line 144
    iput v11, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 145
    .line 146
    goto :goto_4

    .line 147
    :cond_8
    move-object v10, v3

    .line 148
    :cond_9
    if-lez v8, :cond_a

    .line 149
    .line 150
    new-array v0, v5, [C

    .line 151
    .line 152
    invoke-static {v10, v2, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    .line 154
    .line 155
    sub-int v3, v5, v8

    .line 156
    .line 157
    invoke-static {v0, v2, v10, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 158
    .line 159
    .line 160
    invoke-static {v0, v8, v10, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    .line 162
    .line 163
    :cond_a
    if-eqz p2, :cond_c

    .line 164
    .line 165
    new-array v0, v5, [C

    .line 166
    .line 167
    iput v2, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 168
    .line 169
    :goto_7
    iget v3, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 170
    .line 171
    if-ge v3, v5, :cond_b

    .line 172
    .line 173
    sub-int v8, v5, v3

    .line 174
    .line 175
    sub-int/2addr v8, v4

    .line 176
    aget-char v8, v10, v8

    .line 177
    .line 178
    aput-char v8, v0, v3

    .line 179
    .line 180
    add-int/lit8 v3, v3, 0x1

    .line 181
    .line 182
    iput v3, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 183
    .line 184
    goto :goto_7

    .line 185
    :cond_b
    move-object v10, v0

    .line 186
    :cond_c
    if-lez v7, :cond_d

    .line 187
    .line 188
    sget v0, Lcom/appsflyer/internal/AFc1wSDK;->$11:I

    .line 189
    .line 190
    add-int/lit8 v0, v0, 0x69

    .line 191
    .line 192
    rem-int/lit16 v3, v0, 0x80

    .line 193
    .line 194
    sput v3, Lcom/appsflyer/internal/AFc1wSDK;->$10:I

    .line 195
    .line 196
    rem-int/2addr v0, v6

    .line 197
    iput v2, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 198
    .line 199
    :goto_8
    iget v0, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 200
    .line 201
    if-ge v0, v5, :cond_d

    .line 202
    .line 203
    aget-char v3, v10, v0

    .line 204
    .line 205
    aget v4, p0, v6

    .line 206
    .line 207
    sub-int/2addr v3, v4

    .line 208
    int-to-char v3, v3

    .line 209
    aput-char v3, v10, v0

    .line 210
    .line 211
    add-int/lit8 v0, v0, 0x1

    .line 212
    .line 213
    iput v0, v1, Lcom/appsflyer/internal/AFh1ySDK;->values:I

    .line 214
    .line 215
    goto :goto_8

    .line 216
    :cond_d
    new-instance v0, Ljava/lang/String;

    .line 217
    .line 218
    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([C)V

    .line 219
    .line 220
    .line 221
    aput-object v0, p3, v2

    .line 222
    .line 223
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method private valueOf()Ljava/lang/String;
    .locals 18

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v2, "\u909f\ub78f\udebe\ue5d0\u0cc0\u53ee\u7a53\u813f\ua824\ucf58\u167a\u3d76\u458c\u6ca6\ub3d2\udace\ue1ee\u085e\u2f0c\u7629\u9d53\ua43a\ucb75\u1389\u3abd\u41b3\u68ca\u8fb4\ud60a\ufd0b\u043f\u2b58\u7239\u996b\ua18f\uc8bf\uefa0\u36da\u5de7\u6411\u8b0e\ud238\uf905\u0004"

    .line 4
    .line 5
    const-string v3, ""

    .line 6
    .line 7
    const/16 v4, 0xf

    .line 8
    .line 9
    const/16 v5, 0x10

    .line 10
    .line 11
    const/4 v6, 0x6

    .line 12
    const/4 v7, 0x3

    .line 13
    const/4 v8, 0x4

    .line 14
    const/4 v9, 0x2

    .line 15
    const/4 v10, 0x1

    .line 16
    const/4 v11, 0x0

    .line 17
    :try_start_0
    iget-object v0, v1, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 18
    .line 19
    const-string v12, "\u9098\u07a8\ubec8\u5528\ucc4c\u6487\u1bd6\ub20b\u2935\uc077\u78b2\uefd4"

    .line 20
    .line 21
    const/16 v13, 0x30

    .line 22
    .line 23
    invoke-static {v3, v13}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;C)I

    .line 24
    .line 25
    .line 26
    move-result v13

    .line 27
    const v14, 0x9736

    .line 28
    .line 29
    .line 30
    sub-int/2addr v14, v13

    .line 31
    new-array v13, v10, [Ljava/lang/Object;

    .line 32
    .line 33
    invoke-static {v12, v14, v13}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    aget-object v12, v13, v11

    .line 37
    .line 38
    check-cast v12, Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v12

    .line 44
    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    iget-object v12, v1, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 53
    .line 54
    new-array v13, v8, [I

    .line 55
    .line 56
    const/4 v14, 0x7

    .line 57
    aput v14, v13, v11

    .line 58
    .line 59
    aput v4, v13, v10

    .line 60
    .line 61
    aput v11, v13, v9

    .line 62
    .line 63
    aput v6, v13, v7

    .line 64
    .line 65
    const-string v14, "\u0000\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0000\u0001\u0001\u0001\u0000"

    .line 66
    .line 67
    new-array v15, v10, [Ljava/lang/Object;

    .line 68
    .line 69
    invoke-static {v13, v14, v10, v15}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    aget-object v13, v15, v11

    .line 73
    .line 74
    check-cast v13, Ljava/lang/String;

    .line 75
    .line 76
    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v13

    .line 80
    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v12

    .line 84
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v12

    .line 88
    new-array v13, v8, [I

    .line 89
    .line 90
    const/16 v14, 0x16

    .line 91
    .line 92
    aput v14, v13, v11

    .line 93
    .line 94
    aput v6, v13, v10

    .line 95
    .line 96
    aput v11, v13, v9

    .line 97
    .line 98
    aput v11, v13, v7

    .line 99
    .line 100
    const-string v14, "\u0000\u0000\u0000\u0001\u0001\u0000"

    .line 101
    .line 102
    new-array v15, v10, [Ljava/lang/Object;

    .line 103
    .line 104
    invoke-static {v13, v14, v10, v15}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    aget-object v13, v15, v11

    .line 108
    .line 109
    check-cast v13, Ljava/lang/String;

    .line 110
    .line 111
    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v13

    .line 115
    const-string v14, "\u90d4\u83d6\ub620\uaac0\udda7"

    .line 116
    .line 117
    invoke-static {}, Landroid/view/KeyEvent;->getModifierMetaStateMask()I

    .line 118
    .line 119
    .line 120
    move-result v15

    .line 121
    int-to-byte v15, v15

    .line 122
    add-int/lit16 v15, v15, 0x135e

    .line 123
    .line 124
    new-array v4, v10, [Ljava/lang/Object;

    .line 125
    .line 126
    invoke-static {v14, v15, v4}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 127
    .line 128
    .line 129
    aget-object v4, v4, v11

    .line 130
    .line 131
    check-cast v4, Ljava/lang/String;

    .line 132
    .line 133
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    invoke-virtual {v13, v4, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    new-instance v13, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    invoke-static {v0}, Lcom/appsflyer/internal/AFb1wSDK;->AFInAppEventParameterName(Ljava/lang/String;)Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    new-instance v4, Ljava/lang/StringBuilder;

    .line 164
    .line 165
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v0, v11, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    new-array v4, v8, [I

    .line 185
    .line 186
    fill-array-data v4, :array_0

    .line 187
    .line 188
    .line 189
    new-array v12, v10, [Ljava/lang/Object;

    .line 190
    .line 191
    const-string v13, "\u0001\u0000\u0001\u0000\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000"

    .line 192
    .line 193
    invoke-static {v4, v13, v11, v12}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 194
    .line 195
    .line 196
    aget-object v4, v12, v11

    .line 197
    .line 198
    check-cast v4, Ljava/lang/String;

    .line 199
    .line 200
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v4

    .line 204
    invoke-static {v4, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 205
    .line 206
    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    .line 208
    .line 209
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    .line 211
    .line 212
    invoke-static {}, Landroid/view/ViewConfiguration;->getEdgeSlop()I

    .line 213
    .line 214
    .line 215
    move-result v12

    .line 216
    shr-int/2addr v12, v5

    .line 217
    rsub-int v12, v12, 0x2717

    .line 218
    .line 219
    new-array v13, v10, [Ljava/lang/Object;

    .line 220
    .line 221
    invoke-static {v2, v12, v13}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 222
    .line 223
    .line 224
    aget-object v12, v13, v11

    .line 225
    .line 226
    check-cast v12, Ljava/lang/String;

    .line 227
    .line 228
    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v12

    .line 232
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    .line 246
    .line 247
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    new-array v3, v8, [I

    .line 254
    .line 255
    fill-array-data v3, :array_1

    .line 256
    .line 257
    .line 258
    new-array v4, v10, [Ljava/lang/Object;

    .line 259
    .line 260
    const-string v12, "\u0001\u0001\u0000\u0001\u0000\u0000\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000"

    .line 261
    .line 262
    invoke-static {v3, v12, v11, v4}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 263
    .line 264
    .line 265
    aget-object v3, v4, v11

    .line 266
    .line 267
    check-cast v3, Ljava/lang/String;

    .line 268
    .line 269
    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v3

    .line 273
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    .line 275
    .line 276
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 277
    .line 278
    .line 279
    move-result-object v0

    .line 280
    :goto_0
    move-object v3, v0

    .line 281
    :try_start_1
    iget-object v0, v1, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventType:Landroid/content/Context;

    .line 282
    .line 283
    new-instance v4, Landroid/content/IntentFilter;

    .line 284
    .line 285
    new-array v12, v8, [I

    .line 286
    .line 287
    const/16 v13, 0x54

    .line 288
    .line 289
    aput v13, v12, v11

    .line 290
    .line 291
    const/16 v13, 0x25

    .line 292
    .line 293
    aput v13, v12, v10

    .line 294
    .line 295
    const/16 v13, 0x4f

    .line 296
    .line 297
    aput v13, v12, v9

    .line 298
    .line 299
    aput v11, v12, v7

    .line 300
    .line 301
    const-string v13, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0000\u0000\u0001\u0000\u0001\u0001\u0000\u0001\u0000\u0000\u0001\u0001\u0000\u0001\u0001\u0000\u0001\u0000\u0001\u0000\u0000\u0001"

    .line 302
    .line 303
    new-array v14, v10, [Ljava/lang/Object;

    .line 304
    .line 305
    invoke-static {v12, v13, v10, v14}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 306
    .line 307
    .line 308
    aget-object v12, v14, v11

    .line 309
    .line 310
    check-cast v12, Ljava/lang/String;

    .line 311
    .line 312
    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 313
    .line 314
    .line 315
    move-result-object v12

    .line 316
    invoke-direct {v4, v12}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 317
    .line 318
    .line 319
    const/4 v12, 0x0

    .line 320
    invoke-virtual {v0, v12, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 321
    .line 322
    .line 323
    move-result-object v0

    .line 324
    const/16 v4, -0xa8c

    .line 325
    .line 326
    if-eqz v0, :cond_0

    .line 327
    .line 328
    new-array v13, v8, [I

    .line 329
    .line 330
    const/16 v14, 0x79

    .line 331
    .line 332
    aput v14, v13, v11

    .line 333
    .line 334
    const/16 v14, 0xb

    .line 335
    .line 336
    aput v14, v13, v10

    .line 337
    .line 338
    aput v11, v13, v9

    .line 339
    .line 340
    const/16 v14, 0x9

    .line 341
    .line 342
    aput v14, v13, v7

    .line 343
    .line 344
    const-string v14, "\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0000\u0001\u0001\u0001"

    .line 345
    .line 346
    new-array v15, v10, [Ljava/lang/Object;

    .line 347
    .line 348
    invoke-static {v13, v14, v10, v15}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 349
    .line 350
    .line 351
    aget-object v13, v15, v11

    .line 352
    .line 353
    check-cast v13, Ljava/lang/String;

    .line 354
    .line 355
    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 356
    .line 357
    .line 358
    move-result-object v13

    .line 359
    invoke-virtual {v0, v13, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 360
    .line 361
    .line 362
    move-result v4

    .line 363
    :cond_0
    iget-object v0, v1, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventType:Landroid/content/Context;

    .line 364
    .line 365
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    .line 366
    .line 367
    .line 368
    move-result-object v0

    .line 369
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    .line 370
    .line 371
    if-eqz v0, :cond_1

    .line 372
    .line 373
    const/4 v13, 0x1

    .line 374
    goto :goto_1

    .line 375
    :cond_1
    const/4 v13, 0x0

    .line 376
    :goto_1
    const-wide/16 v14, 0x0

    .line 377
    .line 378
    if-eqz v13, :cond_4

    .line 379
    .line 380
    const-string v13, "\u9081\u51b2\u1229"

    .line 381
    .line 382
    invoke-static {v14, v15}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    .line 383
    .line 384
    .line 385
    move-result v16

    .line 386
    const v17, 0xc173

    .line 387
    .line 388
    .line 389
    add-int v12, v16, v17

    .line 390
    .line 391
    new-array v14, v10, [Ljava/lang/Object;

    .line 392
    .line 393
    invoke-static {v13, v12, v14}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 394
    .line 395
    .line 396
    aget-object v12, v14, v11

    .line 397
    .line 398
    check-cast v12, Ljava/lang/String;

    .line 399
    .line 400
    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 401
    .line 402
    .line 403
    move-result-object v12

    .line 404
    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 405
    .line 406
    .line 407
    move-result v0

    .line 408
    const/16 v12, 0x61

    .line 409
    .line 410
    if-eqz v0, :cond_2

    .line 411
    .line 412
    const/16 v0, 0x61

    .line 413
    .line 414
    goto :goto_2

    .line 415
    :cond_2
    const/4 v0, 0x6

    .line 416
    :goto_2
    if-eq v0, v12, :cond_3

    .line 417
    .line 418
    goto :goto_3

    .line 419
    :cond_3
    const/4 v0, 0x1

    .line 420
    goto :goto_4

    .line 421
    :cond_4
    :goto_3
    const/4 v0, 0x0

    .line 422
    :goto_4
    iget-object v12, v1, Lcom/appsflyer/internal/AFc1wSDK;->AFInAppEventType:Landroid/content/Context;

    .line 423
    .line 424
    new-array v13, v8, [I

    .line 425
    .line 426
    const/16 v14, 0x84

    .line 427
    .line 428
    aput v14, v13, v11

    .line 429
    .line 430
    aput v6, v13, v10

    .line 431
    .line 432
    aput v11, v13, v9

    .line 433
    .line 434
    aput v11, v13, v7

    .line 435
    .line 436
    const-string v14, "\u0001\u0000\u0001\u0001\u0000\u0001"

    .line 437
    .line 438
    new-array v15, v10, [Ljava/lang/Object;

    .line 439
    .line 440
    invoke-static {v13, v14, v11, v15}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 441
    .line 442
    .line 443
    aget-object v13, v15, v11

    .line 444
    .line 445
    check-cast v13, Ljava/lang/String;

    .line 446
    .line 447
    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 448
    .line 449
    .line 450
    move-result-object v13

    .line 451
    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 452
    .line 453
    .line 454
    move-result-object v12

    .line 455
    check-cast v12, Landroid/hardware/SensorManager;

    .line 456
    .line 457
    const/4 v13, -0x1

    .line 458
    invoke-virtual {v12, v13}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    .line 459
    .line 460
    .line 461
    move-result-object v12

    .line 462
    invoke-interface {v12}, Ljava/util/List;->size()I

    .line 463
    .line 464
    .line 465
    move-result v12

    .line 466
    new-instance v13, Ljava/lang/StringBuilder;

    .line 467
    .line 468
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 469
    .line 470
    .line 471
    const-string v14, "\u909b"

    .line 472
    .line 473
    invoke-static {}, Landroid/view/KeyEvent;->getMaxKeyCode()I

    .line 474
    .line 475
    .line 476
    move-result v15

    .line 477
    shr-int/lit8 v5, v15, 0x10

    .line 478
    .line 479
    rsub-int v5, v5, 0x61ff

    .line 480
    .line 481
    new-array v15, v10, [Ljava/lang/Object;

    .line 482
    .line 483
    invoke-static {v14, v5, v15}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 484
    .line 485
    .line 486
    aget-object v5, v15, v11

    .line 487
    .line 488
    check-cast v5, Ljava/lang/String;

    .line 489
    .line 490
    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 491
    .line 492
    .line 493
    move-result-object v5

    .line 494
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    .line 496
    .line 497
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 498
    .line 499
    .line 500
    new-array v4, v8, [I

    .line 501
    .line 502
    const/16 v5, 0x8a

    .line 503
    .line 504
    aput v5, v4, v11

    .line 505
    .line 506
    aput v9, v4, v10

    .line 507
    .line 508
    const/16 v5, 0x3a

    .line 509
    .line 510
    aput v5, v4, v9

    .line 511
    .line 512
    aput v9, v4, v7

    .line 513
    .line 514
    const-string v5, "\u0000\u0000"

    .line 515
    .line 516
    new-array v14, v10, [Ljava/lang/Object;

    .line 517
    .line 518
    invoke-static {v4, v5, v10, v14}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 519
    .line 520
    .line 521
    aget-object v4, v14, v11

    .line 522
    .line 523
    check-cast v4, Ljava/lang/String;

    .line 524
    .line 525
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 526
    .line 527
    .line 528
    move-result-object v4

    .line 529
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    .line 531
    .line 532
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 533
    .line 534
    .line 535
    new-array v0, v8, [I

    .line 536
    .line 537
    const/16 v4, 0x8c

    .line 538
    .line 539
    aput v4, v0, v11

    .line 540
    .line 541
    aput v9, v0, v10

    .line 542
    .line 543
    const/16 v4, 0x41

    .line 544
    .line 545
    aput v4, v0, v9

    .line 546
    .line 547
    aput v11, v0, v7

    .line 548
    .line 549
    const-string v4, "\u0000\u0001"

    .line 550
    .line 551
    new-array v5, v10, [Ljava/lang/Object;

    .line 552
    .line 553
    invoke-static {v0, v4, v10, v5}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 554
    .line 555
    .line 556
    aget-object v0, v5, v11

    .line 557
    .line 558
    check-cast v0, Ljava/lang/String;

    .line 559
    .line 560
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 561
    .line 562
    .line 563
    move-result-object v0

    .line 564
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    .line 566
    .line 567
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 568
    .line 569
    .line 570
    const-string v0, "\u90df\u43ca"

    .line 571
    .line 572
    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    .line 573
    .line 574
    .line 575
    move-result-wide v4

    .line 576
    const v7, 0xd344

    .line 577
    .line 578
    .line 579
    const-wide/16 v14, 0x0

    .line 580
    .line 581
    cmp-long v12, v4, v14

    .line 582
    .line 583
    sub-int/2addr v7, v12

    .line 584
    new-array v4, v10, [Ljava/lang/Object;

    .line 585
    .line 586
    invoke-static {v0, v7, v4}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 587
    .line 588
    .line 589
    aget-object v0, v4, v11

    .line 590
    .line 591
    check-cast v0, Ljava/lang/String;

    .line 592
    .line 593
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 594
    .line 595
    .line 596
    move-result-object v0

    .line 597
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    .line 599
    .line 600
    iget-object v0, v1, Lcom/appsflyer/internal/AFc1wSDK;->valueOf:Ljava/util/Map;

    .line 601
    .line 602
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 603
    .line 604
    .line 605
    move-result v0

    .line 606
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 607
    .line 608
    .line 609
    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 610
    .line 611
    .line 612
    move-result-object v0

    .line 613
    new-instance v4, Ljava/lang/StringBuilder;

    .line 614
    .line 615
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 616
    .line 617
    .line 618
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    .line 620
    .line 621
    invoke-static {v0}, Lcom/appsflyer/internal/AFc1wSDK$AFa1vSDK;->AFInAppEventType(Ljava/lang/String;)[B

    .line 622
    .line 623
    .line 624
    move-result-object v0

    .line 625
    invoke-static {v0}, Lcom/appsflyer/internal/AFc1wSDK$AFa1vSDK;->AFInAppEventType([B)[B

    .line 626
    .line 627
    .line 628
    move-result-object v0

    .line 629
    new-instance v5, Ljava/lang/StringBuilder;

    .line 630
    .line 631
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 632
    .line 633
    .line 634
    array-length v7, v0

    .line 635
    const/4 v12, 0x0

    .line 636
    :goto_5
    const/16 v13, 0x58

    .line 637
    .line 638
    if-ge v12, v7, :cond_5

    .line 639
    .line 640
    const/16 v14, 0x58

    .line 641
    .line 642
    goto :goto_6

    .line 643
    :cond_5
    const/16 v14, 0x1a

    .line 644
    .line 645
    :goto_6
    if-eq v14, v13, :cond_6

    .line 646
    .line 647
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 648
    .line 649
    .line 650
    move-result-object v0

    .line 651
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    .line 653
    .line 654
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 655
    .line 656
    .line 657
    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 658
    goto/16 :goto_9

    .line 659
    .line 660
    :cond_6
    sget v13, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 661
    .line 662
    add-int/lit8 v13, v13, 0x73

    .line 663
    .line 664
    rem-int/lit16 v14, v13, 0x80

    .line 665
    .line 666
    sput v14, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 667
    .line 668
    rem-int/2addr v13, v9

    .line 669
    :try_start_2
    aget-byte v13, v0, v12

    .line 670
    .line 671
    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 672
    .line 673
    .line 674
    move-result-object v13

    .line 675
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    .line 676
    .line 677
    .line 678
    move-result v14
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 679
    if-ne v14, v10, :cond_9

    .line 680
    .line 681
    sget v14, Lcom/appsflyer/internal/AFc1wSDK;->afInfoLog:I

    .line 682
    .line 683
    const/16 v15, 0xf

    .line 684
    .line 685
    add-int/2addr v14, v15

    .line 686
    rem-int/lit16 v15, v14, 0x80

    .line 687
    .line 688
    sput v15, Lcom/appsflyer/internal/AFc1wSDK;->AFKeystoreWrapper:I

    .line 689
    .line 690
    rem-int/2addr v14, v9

    .line 691
    if-eqz v14, :cond_7

    .line 692
    .line 693
    const/4 v14, 0x0

    .line 694
    goto :goto_7

    .line 695
    :cond_7
    const/4 v14, 0x1

    .line 696
    :goto_7
    const-string v15, "0"

    .line 697
    .line 698
    if-eqz v14, :cond_8

    .line 699
    .line 700
    :try_start_3
    invoke-virtual {v15, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 701
    .line 702
    .line 703
    move-result-object v13

    .line 704
    goto :goto_8

    .line 705
    :cond_8
    invoke-virtual {v15, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 706
    .line 707
    .line 708
    const/4 v14, 0x0

    .line 709
    :try_start_4
    throw v14
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 710
    :catchall_0
    move-exception v0

    .line 711
    move-object v2, v0

    .line 712
    throw v2

    .line 713
    :cond_9
    :goto_8
    const/4 v14, 0x0

    .line 714
    :try_start_5
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 715
    .line 716
    .line 717
    add-int/lit8 v12, v12, 0x1

    .line 718
    .line 719
    goto :goto_5

    .line 720
    :catch_1
    move-exception v0

    .line 721
    new-array v4, v8, [I

    .line 722
    .line 723
    fill-array-data v4, :array_2

    .line 724
    .line 725
    .line 726
    new-array v5, v10, [Ljava/lang/Object;

    .line 727
    .line 728
    const-string v7, "\u0000\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u0000"

    .line 729
    .line 730
    invoke-static {v4, v7, v10, v5}, Lcom/appsflyer/internal/AFc1wSDK;->b([ILjava/lang/String;Z[Ljava/lang/Object;)V

    .line 731
    .line 732
    .line 733
    aget-object v4, v5, v11

    .line 734
    .line 735
    check-cast v4, Ljava/lang/String;

    .line 736
    .line 737
    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 738
    .line 739
    .line 740
    move-result-object v4

    .line 741
    invoke-static {v4, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 742
    .line 743
    .line 744
    new-instance v4, Ljava/lang/StringBuilder;

    .line 745
    .line 746
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 747
    .line 748
    .line 749
    invoke-static {v11}, Landroid/graphics/Color;->red(I)I

    .line 750
    .line 751
    .line 752
    move-result v5

    .line 753
    add-int/lit16 v5, v5, 0x2717

    .line 754
    .line 755
    new-array v7, v10, [Ljava/lang/Object;

    .line 756
    .line 757
    invoke-static {v2, v5, v7}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 758
    .line 759
    .line 760
    aget-object v2, v7, v11

    .line 761
    .line 762
    check-cast v2, Ljava/lang/String;

    .line 763
    .line 764
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 765
    .line 766
    .line 767
    move-result-object v2

    .line 768
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    .line 770
    .line 771
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 772
    .line 773
    .line 774
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 775
    .line 776
    .line 777
    move-result-object v0

    .line 778
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afRDLog(Ljava/lang/String;)V

    .line 779
    .line 780
    .line 781
    new-instance v0, Ljava/lang/StringBuilder;

    .line 782
    .line 783
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 784
    .line 785
    .line 786
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 787
    .line 788
    .line 789
    invoke-static {v11}, Landroid/os/Process;->getThreadPriority(I)I

    .line 790
    .line 791
    .line 792
    move-result v2

    .line 793
    add-int/lit8 v2, v2, 0x14

    .line 794
    .line 795
    shr-int/2addr v2, v6

    .line 796
    const v3, 0xcc89

    .line 797
    .line 798
    .line 799
    add-int/2addr v2, v3

    .line 800
    new-array v3, v10, [Ljava/lang/Object;

    .line 801
    .line 802
    const-string v4, "\u909f\u5c40\u099a\uf552\ua2ac\u6e65\u5bbf\u0777\uf4c1\ua01a\u6d91\u5979\u06fd\uf278\ubfe6\u6c93"

    .line 803
    .line 804
    invoke-static {v4, v2, v3}, Lcom/appsflyer/internal/AFc1wSDK;->a(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 805
    .line 806
    .line 807
    aget-object v2, v3, v11

    .line 808
    .line 809
    check-cast v2, Ljava/lang/String;

    .line 810
    .line 811
    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 812
    .line 813
    .line 814
    move-result-object v2

    .line 815
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    .line 817
    .line 818
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 819
    .line 820
    .line 821
    move-result-object v0

    .line 822
    :goto_9
    return-object v0

    .line 823
    :array_0
    .array-data 4
        0x1c
        0x26
        0x0
        0x0
    .end array-data

    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    :array_1
    .array-data 4
        0x42
        0x12
        0x77
        0x8
    .end array-data

    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    :array_2
    .array-data 4
        0x8e
        0x10
        0x7e
        0x0
    .end array-data
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

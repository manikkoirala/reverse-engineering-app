.class public interface abstract Lcom/appsflyer/internal/AFd1mSDK;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract AFInAppEventParameterName(Lcom/appsflyer/internal/AFd1kSDK;)V
    .param p1    # Lcom/appsflyer/internal/AFd1kSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFd1kSDK<",
            "*>;)V"
        }
    .end annotation
.end method

.method public abstract AFInAppEventType(Lcom/appsflyer/internal/AFd1kSDK;)V
    .param p1    # Lcom/appsflyer/internal/AFd1kSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFd1kSDK<",
            "*>;)V"
        }
    .end annotation
.end method

.method public abstract AFKeystoreWrapper(Lcom/appsflyer/internal/AFd1kSDK;Lcom/appsflyer/internal/AFd1nSDK;)V
    .param p1    # Lcom/appsflyer/internal/AFd1kSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/appsflyer/internal/AFd1nSDK;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/appsflyer/internal/AFd1kSDK<",
            "*>;",
            "Lcom/appsflyer/internal/AFd1nSDK;",
            ")V"
        }
    .end annotation
.end method

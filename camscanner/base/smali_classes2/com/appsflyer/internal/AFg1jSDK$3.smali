.class final Lcom/appsflyer/internal/AFg1jSDK$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/appsflyer/internal/AFg1jSDK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;


# direct methods
.method constructor <init>(Lcom/appsflyer/internal/AFg1jSDK;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/appsflyer/internal/AFg1jSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFg1jSDK;)Ljava/util/Map;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/appsflyer/internal/AFg1jSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFg1jSDK;)Ljava/util/Map;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_0

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    check-cast v1, Lcom/appsflyer/internal/AFg1lSDK;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 40
    .line 41
    invoke-static {v2}, Lcom/appsflyer/internal/AFg1jSDK;->valueOf(Lcom/appsflyer/internal/AFg1jSDK;)Landroid/hardware/SensorManager;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 46
    .line 47
    .line 48
    iget-object v2, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 49
    .line 50
    invoke-static {v2}, Lcom/appsflyer/internal/AFg1jSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1jSDK;)Ljava/util/Map;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    const/4 v3, 0x1

    .line 55
    invoke-virtual {v1, v2, v3}, Lcom/appsflyer/internal/AFg1lSDK;->values(Ljava/util/Map;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    .line 60
    const-string v1, "error while unregistering listeners"

    .line 61
    .line 62
    invoke-static {v1, v0}, Lcom/appsflyer/AFLogger;->afErrorLogForExcManagerOnly(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    .line 64
    .line 65
    :cond_0
    iget-object v0, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 66
    .line 67
    const/4 v1, 0x0

    .line 68
    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFg1jSDK;->AFInAppEventParameterName(Lcom/appsflyer/internal/AFg1jSDK;I)I

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/appsflyer/internal/AFg1jSDK$3;->AFKeystoreWrapper:Lcom/appsflyer/internal/AFg1jSDK;

    .line 72
    .line 73
    invoke-static {v0, v1}, Lcom/appsflyer/internal/AFg1jSDK;->values(Lcom/appsflyer/internal/AFg1jSDK;Z)Z

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

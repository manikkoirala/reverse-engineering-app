.class public final Lcom/appsflyer/internal/AFd1fSDK$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/appsflyer/internal/AFd1fSDK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field private synthetic AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

.field private synthetic values:Lcom/appsflyer/internal/AFd1fSDK;


# direct methods
.method public constructor <init>(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 4
    .line 5
    monitor-enter v0

    .line 6
    :try_start_0
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1fSDK;->afRDLog:Ljava/util/Set;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 11
    .line 12
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    const-string v2, "QUEUE: tried to add already running task: "

    .line 21
    .line 22
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    monitor-exit v0

    .line 38
    return-void

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 40
    .line 41
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 44
    .line 45
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-nez v1, :cond_f

    .line 50
    .line 51
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 52
    .line 53
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 54
    .line 55
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 56
    .line 57
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    if-eqz v1, :cond_1

    .line 62
    .line 63
    goto/16 :goto_7

    .line 64
    .line 65
    :cond_1
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 66
    .line 67
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 68
    .line 69
    iget-object v3, v2, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventType:Ljava/util/Set;

    .line 70
    .line 71
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    if-eqz v4, :cond_3

    .line 80
    .line 81
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    check-cast v4, Lcom/appsflyer/internal/AFd1eSDK;

    .line 86
    .line 87
    iget-object v5, v1, Lcom/appsflyer/internal/AFd1fSDK;->afInfoLog:Ljava/util/Set;

    .line 88
    .line 89
    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    if-eqz v5, :cond_2

    .line 94
    .line 95
    iget-object v5, v2, Lcom/appsflyer/internal/AFd1kSDK;->values:Ljava/util/Set;

    .line 96
    .line 97
    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 102
    .line 103
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 104
    .line 105
    invoke-static {v1, v2}, Lcom/appsflyer/internal/AFd1fSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFd1fSDK;Lcom/appsflyer/internal/AFd1kSDK;)Z

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    if-eqz v1, :cond_4

    .line 110
    .line 111
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 112
    .line 113
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 114
    .line 115
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 116
    .line 117
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    goto :goto_1

    .line 122
    :cond_4
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 123
    .line 124
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 125
    .line 126
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 127
    .line 128
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    if-eqz v1, :cond_5

    .line 133
    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    .line 135
    .line 136
    const-string v3, "QUEUE: new task was blocked: "

    .line 137
    .line 138
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    iget-object v3, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 142
    .line 143
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v2

    .line 150
    invoke-static {v2}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 154
    .line 155
    invoke-virtual {v2}, Lcom/appsflyer/internal/AFd1kSDK;->AFKeystoreWrapper()V

    .line 156
    .line 157
    .line 158
    :cond_5
    :goto_1
    if-eqz v1, :cond_6

    .line 159
    .line 160
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 161
    .line 162
    iget-object v3, v2, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 163
    .line 164
    iget-object v2, v2, Lcom/appsflyer/internal/AFd1fSDK;->afErrorLog:Ljava/util/List;

    .line 165
    .line 166
    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 167
    .line 168
    .line 169
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 170
    .line 171
    iget-object v2, v2, Lcom/appsflyer/internal/AFd1fSDK;->afErrorLog:Ljava/util/List;

    .line 172
    .line 173
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 174
    .line 175
    .line 176
    goto :goto_2

    .line 177
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    .line 178
    .line 179
    const-string v3, "QUEUE: task not added, it\'s already in the queue: "

    .line 180
    .line 181
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    iget-object v3, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 185
    .line 186
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    .line 188
    .line 189
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    invoke-static {v2}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    :goto_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 197
    if-eqz v1, :cond_e

    .line 198
    .line 199
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 200
    .line 201
    iget-object v0, v0, Lcom/appsflyer/internal/AFd1fSDK;->afInfoLog:Ljava/util/Set;

    .line 202
    .line 203
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 204
    .line 205
    iget-object v1, v1, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventParameterName:Lcom/appsflyer/internal/AFd1eSDK;

    .line 206
    .line 207
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    .line 211
    .line 212
    const-string v1, "QUEUE: new task added: "

    .line 213
    .line 214
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 215
    .line 216
    .line 217
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 218
    .line 219
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object v0

    .line 226
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 230
    .line 231
    iget-object v0, v0, Lcom/appsflyer/internal/AFd1fSDK;->valueOf:Ljava/util/List;

    .line 232
    .line 233
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 238
    .line 239
    .line 240
    move-result v1

    .line 241
    if-eqz v1, :cond_7

    .line 242
    .line 243
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 244
    .line 245
    .line 246
    move-result-object v1

    .line 247
    check-cast v1, Lcom/appsflyer/internal/AFd1mSDK;

    .line 248
    .line 249
    goto :goto_3

    .line 250
    :cond_7
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 251
    .line 252
    iget-object v1, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFInAppEventType:Ljava/util/concurrent/ExecutorService;

    .line 253
    .line 254
    new-instance v2, Lcom/appsflyer/internal/AFd1fSDK$4;

    .line 255
    .line 256
    invoke-direct {v2, v0}, Lcom/appsflyer/internal/AFd1fSDK$4;-><init>(Lcom/appsflyer/internal/AFd1fSDK;)V

    .line 257
    .line 258
    .line 259
    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 260
    .line 261
    .line 262
    iget-object v0, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->values:Lcom/appsflyer/internal/AFd1fSDK;

    .line 263
    .line 264
    iget-object v1, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 265
    .line 266
    monitor-enter v1

    .line 267
    :try_start_1
    iget-object v2, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 268
    .line 269
    invoke-interface {v2}, Ljava/util/Set;->size()I

    .line 270
    .line 271
    .line 272
    move-result v2

    .line 273
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 274
    .line 275
    invoke-interface {v3}, Ljava/util/Set;->size()I

    .line 276
    .line 277
    .line 278
    move-result v3

    .line 279
    add-int/2addr v2, v3

    .line 280
    add-int/lit8 v2, v2, -0x28

    .line 281
    .line 282
    :goto_4
    if-lez v2, :cond_d

    .line 283
    .line 284
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 285
    .line 286
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    .line 287
    .line 288
    .line 289
    move-result v3

    .line 290
    const/4 v4, 0x1

    .line 291
    if-nez v3, :cond_8

    .line 292
    .line 293
    const/4 v3, 0x1

    .line 294
    goto :goto_5

    .line 295
    :cond_8
    const/4 v3, 0x0

    .line 296
    :goto_5
    iget-object v5, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 297
    .line 298
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    .line 299
    .line 300
    .line 301
    move-result v5

    .line 302
    xor-int/2addr v4, v5

    .line 303
    if-eqz v4, :cond_a

    .line 304
    .line 305
    if-eqz v3, :cond_a

    .line 306
    .line 307
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 308
    .line 309
    invoke-interface {v3}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    .line 310
    .line 311
    .line 312
    move-result-object v3

    .line 313
    check-cast v3, Lcom/appsflyer/internal/AFd1kSDK;

    .line 314
    .line 315
    iget-object v4, v0, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 316
    .line 317
    invoke-interface {v4}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    .line 318
    .line 319
    .line 320
    move-result-object v4

    .line 321
    check-cast v4, Lcom/appsflyer/internal/AFd1kSDK;

    .line 322
    .line 323
    invoke-virtual {v3, v4}, Lcom/appsflyer/internal/AFd1kSDK;->AFInAppEventType(Lcom/appsflyer/internal/AFd1kSDK;)I

    .line 324
    .line 325
    .line 326
    move-result v3

    .line 327
    if-lez v3, :cond_9

    .line 328
    .line 329
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 330
    .line 331
    invoke-virtual {v0, v3}, Lcom/appsflyer/internal/AFd1fSDK;->AFKeystoreWrapper(Ljava/util/NavigableSet;)V

    .line 332
    .line 333
    .line 334
    goto :goto_6

    .line 335
    :cond_9
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 336
    .line 337
    invoke-virtual {v0, v3}, Lcom/appsflyer/internal/AFd1fSDK;->AFKeystoreWrapper(Ljava/util/NavigableSet;)V

    .line 338
    .line 339
    .line 340
    goto :goto_6

    .line 341
    :cond_a
    if-eqz v4, :cond_b

    .line 342
    .line 343
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->AFLogger:Ljava/util/NavigableSet;

    .line 344
    .line 345
    invoke-virtual {v0, v3}, Lcom/appsflyer/internal/AFd1fSDK;->AFKeystoreWrapper(Ljava/util/NavigableSet;)V

    .line 346
    .line 347
    .line 348
    goto :goto_6

    .line 349
    :cond_b
    if-eqz v3, :cond_c

    .line 350
    .line 351
    iget-object v3, v0, Lcom/appsflyer/internal/AFd1fSDK;->afDebugLog:Ljava/util/NavigableSet;

    .line 352
    .line 353
    invoke-virtual {v0, v3}, Lcom/appsflyer/internal/AFd1fSDK;->AFKeystoreWrapper(Ljava/util/NavigableSet;)V

    .line 354
    .line 355
    .line 356
    :cond_c
    :goto_6
    add-int/lit8 v2, v2, -0x1

    .line 357
    .line 358
    goto :goto_4

    .line 359
    :cond_d
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    return-void

    .line 361
    :catchall_0
    move-exception v0

    .line 362
    monitor-exit v1

    .line 363
    throw v0

    .line 364
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    .line 365
    .line 366
    const-string v1, "QUEUE: tried to add already pending task: "

    .line 367
    .line 368
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 369
    .line 370
    .line 371
    iget-object v1, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 372
    .line 373
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 377
    .line 378
    .line 379
    move-result-object v0

    .line 380
    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afWarnLog(Ljava/lang/String;)V

    .line 381
    .line 382
    .line 383
    return-void

    .line 384
    :cond_f
    :goto_7
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 385
    .line 386
    const-string v2, "QUEUE: tried to add already scheduled task: "

    .line 387
    .line 388
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 389
    .line 390
    .line 391
    iget-object v2, p0, Lcom/appsflyer/internal/AFd1fSDK$3;->AFInAppEventType:Lcom/appsflyer/internal/AFd1kSDK;

    .line 392
    .line 393
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 394
    .line 395
    .line 396
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 397
    .line 398
    .line 399
    move-result-object v1

    .line 400
    invoke-static {v1}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 401
    .line 402
    .line 403
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404
    return-void

    .line 405
    :catchall_1
    move-exception v1

    .line 406
    monitor-exit v0

    .line 407
    throw v1
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

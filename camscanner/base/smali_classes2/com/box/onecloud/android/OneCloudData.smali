.class public Lcom/box/onecloud/android/OneCloudData;
.super Ljava/lang/Object;
.source "OneCloudData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/onecloud/android/OneCloudData$UploadListener;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/box/onecloud/android/OneCloudData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private o0:Lcom/box/onecloud/android/OneCloudInterface;

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/box/onecloud/android/OneCloudData$6;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/box/onecloud/android/OneCloudData$6;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/box/onecloud/android/OneCloudData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/box/onecloud/android/OneCloudData;->〇OOo8〇0:Z

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/box/onecloud/android/OneCloudData;->OO0o〇〇〇〇0(Landroid/os/Parcel;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private OO0o〇〇〇〇0(Landroid/os/Parcel;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/box/onecloud/android/OneCloudInterface$Stub;->〇00〇8(Landroid/os/IBinder;)Lcom/box/onecloud/android/OneCloudInterface;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    const/4 v0, 0x1

    .line 16
    if-ne p1, v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    iput-boolean v0, p0, Lcom/box/onecloud/android/OneCloudData;->〇OOo8〇0:Z

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇080(Lcom/box/onecloud/android/OneCloudData;)Lcom/box/onecloud/android/OneCloudInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/box/onecloud/android/OneCloudData;->〇OOo8〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {v0}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/box/onecloud/android/OneCloudData;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/box/onecloud/android/OneCloudData;->〇OOo8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public O8()J
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/box/onecloud/android/OneCloudData;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-wide v1

    .line 10
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/box/onecloud/android/OneCloudInterface;->oo〇()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    return-wide v0

    .line 17
    :catch_0
    return-wide v1
.end method

.method public Oo08()Ljava/io/InputStream;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/box/onecloud/android/OneCloudData;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance v0, Lcom/box/onecloud/android/OneCloudData$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/box/onecloud/android/OneCloudData$1;-><init>(Lcom/box/onecloud/android/OneCloudData;)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public Oooo8o0〇(Landroid/content/Context;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/box/onecloud/android/OneCloudData$7;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/box/onecloud/android/OneCloudData$7;-><init>(Lcom/box/onecloud/android/OneCloudData;Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    :try_start_0
    iget-object p1, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 7
    .line 8
    invoke-interface {p1, v0}, Lcom/box/onecloud/android/OneCloudInterface;->Oooo8o0〇(Lcom/box/onecloud/android/HandshakeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    :catch_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o〇0()Ljava/io/OutputStream;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/box/onecloud/android/OneCloudData;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance v0, Lcom/box/onecloud/android/OneCloudData$2;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/box/onecloud/android/OneCloudData$2;-><init>(Lcom/box/onecloud/android/OneCloudData;)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 2
    .line 3
    invoke-interface {p2}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 8
    .line 9
    .line 10
    iget-boolean p2, p0, Lcom/box/onecloud/android/OneCloudData;->〇OOo8〇0:Z

    .line 11
    .line 12
    int-to-byte p2, p2

    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇O00(Ljava/lang/String;Lcom/box/onecloud/android/OneCloudData$UploadListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/box/onecloud/android/OneCloudData;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 9
    .line 10
    new-instance v1, Lcom/box/onecloud/android/OneCloudData$5;

    .line 11
    .line 12
    invoke-direct {v1, p0, p2}, Lcom/box/onecloud/android/OneCloudData$5;-><init>(Lcom/box/onecloud/android/OneCloudData;Lcom/box/onecloud/android/OneCloudData$UploadListener;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {v0, p1, v1}, Lcom/box/onecloud/android/OneCloudInterface;->〇0〇O0088o(Ljava/lang/String;Lcom/box/onecloud/android/FileUploadCallbacks;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇o〇()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/box/onecloud/android/OneCloudData;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/box/onecloud/android/OneCloudData;->o0:Lcom/box/onecloud/android/OneCloudInterface;

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/box/onecloud/android/OneCloudInterface;->getFileName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    return-object v0

    .line 16
    :catch_0
    return-object v1
    .line 17
.end method

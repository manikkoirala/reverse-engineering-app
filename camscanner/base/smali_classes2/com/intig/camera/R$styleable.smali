.class public final Lcom/intig/camera/R$styleable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intig/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CameraView:[I

.field public static final CameraView_android_adjustViewBounds:I = 0x0

.field public static final CameraView_aspectRatio:I = 0x1

.field public static final CameraView_autoFocus:I = 0x2

.field public static final CameraView_cameraApi:I = 0x3

.field public static final CameraView_collectParams:I = 0x4

.field public static final CameraView_facing:I = 0x5

.field public static final CameraView_flash:I = 0x6

.field public static final CameraView_focusColor:I = 0x7

.field public static final CameraView_previewMode:I = 0x8

.field public static final CameraView_touchFocus:I = 0x9


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intig/camera/R$styleable;->CameraView:[I

    .line 9
    .line 10
    return-void

    .line 11
    :array_0
    .array-data 4
        0x101011e
        0x7f040072
        0x7f040077
        0x7f0400ea
        0x7f040149
        0x7f040281
        0x7f04029d
        0x7f0402c6
        0x7f0404de
        0x7f04067d
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

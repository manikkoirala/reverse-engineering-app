.class public Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;
.super Ljava/lang/Object;
.source "DbxUserCloudDocsRequests.java"


# instance fields
.field private final client:Lcom/dropbox/core/v2/DbxRawClientV2;


# direct methods
.method public constructor <init>(Lcom/dropbox/core/v2/DbxRawClientV2;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method getContent(Lcom/dropbox/core/v2/clouddocs/GetContentArg;Ljava/util/List;)Lcom/dropbox/core/DbxDownloader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/dropbox/core/v2/clouddocs/GetContentArg;",
            "Ljava/util/List<",
            "Lcom/dropbox/core/http/HttpRequestor$Header;",
            ">;)",
            "Lcom/dropbox/core/DbxDownloader<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getContent()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/get_content"

    const/4 v4, 0x0

    sget-object v6, Lcom/dropbox/core/v2/clouddocs/GetContentArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/GetContentArg$Serializer;

    .line 2
    invoke-static {}, Lcom/dropbox/core/stone/StoneSerializers;->void_()Lcom/dropbox/core/stone/StoneSerializer;

    move-result-object v7

    sget-object v8, Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessError$Serializer;

    move-object v3, p1

    move-object v5, p2

    .line 3
    invoke-virtual/range {v0 .. v8}, Lcom/dropbox/core/v2/DbxRawClientV2;->downloadStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLjava/util/List;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;)Lcom/dropbox/core/DbxDownloader;

    move-result-object p1
    :try_end_0
    .catch Lcom/dropbox/core/DbxWrappedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 4
    new-instance p2, Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessErrorException;

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getRequestId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getUserMessage()Lcom/dropbox/core/LocalizedText;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getErrorValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessError;

    const-string v2, "2/cloud_docs/get_content"

    invoke-direct {p2, v2, v0, v1, p1}, Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessErrorException;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/LocalizedText;Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessError;)V

    throw p2
.end method

.method public getContent(Ljava/lang/String;)Lcom/dropbox/core/DbxDownloader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/dropbox/core/DbxDownloader<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/CloudDocsAccessErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 5
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/GetContentArg;

    invoke-direct {v0, p1}, Lcom/dropbox/core/v2/clouddocs/GetContentArg;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->getContent(Lcom/dropbox/core/v2/clouddocs/GetContentArg;Ljava/util/List;)Lcom/dropbox/core/DbxDownloader;

    move-result-object p1

    return-object p1
.end method

.method public getContentBuilder(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetContentBuilder;
    .locals 1

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/GetContentBuilder;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/dropbox/core/v2/clouddocs/GetContentBuilder;-><init>(Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public getMetadata()Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/GetMetadataErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;

    invoke-direct {v0}, Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;-><init>()V

    .line 4
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->getMetadata(Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;

    move-result-object v0

    return-object v0
.end method

.method getMetadata(Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/GetMetadataErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getApi()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/get_metadata"

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/core/v2/clouddocs/GetMetadataArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/GetMetadataArg$Serializer;

    sget-object v6, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Serializer;

    sget-object v7, Lcom/dropbox/core/v2/clouddocs/GetMetadataError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/GetMetadataError$Serializer;

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/dropbox/core/v2/DbxRawClientV2;->rpcStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
    :try_end_0
    .catch Lcom/dropbox/core/DbxWrappedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 2
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/GetMetadataErrorException;

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getUserMessage()Lcom/dropbox/core/LocalizedText;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getErrorValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/GetMetadataError;

    const-string v3, "2/cloud_docs/get_metadata"

    invoke-direct {v0, v3, v1, v2, p1}, Lcom/dropbox/core/v2/clouddocs/GetMetadataErrorException;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/LocalizedText;Lcom/dropbox/core/v2/clouddocs/GetMetadataError;)V

    throw v0
.end method

.method public getMetadata(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/GetMetadataErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 5
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;

    invoke-direct {v0, p1}, Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->getMetadata(Lcom/dropbox/core/v2/clouddocs/GetMetadataArg;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value for \'fileId\' is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public lock()Lcom/dropbox/core/v2/clouddocs/LockResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/LockArg;

    invoke-direct {v0}, Lcom/dropbox/core/v2/clouddocs/LockArg;-><init>()V

    .line 4
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->lock(Lcom/dropbox/core/v2/clouddocs/LockArg;)Lcom/dropbox/core/v2/clouddocs/LockResult;

    move-result-object v0

    return-object v0
.end method

.method lock(Lcom/dropbox/core/v2/clouddocs/LockArg;)Lcom/dropbox/core/v2/clouddocs/LockResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getApi()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/lock"

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/core/v2/clouddocs/LockArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/LockArg$Serializer;

    sget-object v6, Lcom/dropbox/core/v2/clouddocs/LockResult$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/LockResult$Serializer;

    sget-object v7, Lcom/dropbox/core/v2/clouddocs/LockingError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/LockingError$Serializer;

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/dropbox/core/v2/DbxRawClientV2;->rpcStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/LockResult;
    :try_end_0
    .catch Lcom/dropbox/core/DbxWrappedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 2
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/LockingErrorException;

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getUserMessage()Lcom/dropbox/core/LocalizedText;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getErrorValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/LockingError;

    const-string v3, "2/cloud_docs/lock"

    invoke-direct {v0, v3, v1, v2, p1}, Lcom/dropbox/core/v2/clouddocs/LockingErrorException;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/LocalizedText;Lcom/dropbox/core/v2/clouddocs/LockingError;)V

    throw v0
.end method

.method public lock(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/LockResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 5
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/LockArg;

    invoke-direct {v0, p1}, Lcom/dropbox/core/v2/clouddocs/LockArg;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->lock(Lcom/dropbox/core/v2/clouddocs/LockArg;)Lcom/dropbox/core/v2/clouddocs/LockResult;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value for \'fileId\' is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public rename()Lcom/dropbox/core/v2/clouddocs/RenameResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/RenameErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/RenameArg;

    invoke-direct {v0}, Lcom/dropbox/core/v2/clouddocs/RenameArg;-><init>()V

    .line 4
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->rename(Lcom/dropbox/core/v2/clouddocs/RenameArg;)Lcom/dropbox/core/v2/clouddocs/RenameResult;

    move-result-object v0

    return-object v0
.end method

.method rename(Lcom/dropbox/core/v2/clouddocs/RenameArg;)Lcom/dropbox/core/v2/clouddocs/RenameResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/RenameErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getApi()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/rename"

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/core/v2/clouddocs/RenameArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/RenameArg$Serializer;

    sget-object v6, Lcom/dropbox/core/v2/clouddocs/RenameResult$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/RenameResult$Serializer;

    sget-object v7, Lcom/dropbox/core/v2/clouddocs/RenameError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/RenameError$Serializer;

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/dropbox/core/v2/DbxRawClientV2;->rpcStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/RenameResult;
    :try_end_0
    .catch Lcom/dropbox/core/DbxWrappedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 2
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/RenameErrorException;

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getUserMessage()Lcom/dropbox/core/LocalizedText;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getErrorValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/RenameError;

    const-string v3, "2/cloud_docs/rename"

    invoke-direct {v0, v3, v1, v2, p1}, Lcom/dropbox/core/v2/clouddocs/RenameErrorException;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/LocalizedText;Lcom/dropbox/core/v2/clouddocs/RenameError;)V

    throw v0
.end method

.method public renameBuilder()Lcom/dropbox/core/v2/clouddocs/RenameBuilder;
    .locals 2

    .line 1
    invoke-static {}, Lcom/dropbox/core/v2/clouddocs/RenameArg;->newBuilder()Lcom/dropbox/core/v2/clouddocs/RenameArg$Builder;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/dropbox/core/v2/clouddocs/RenameBuilder;

    .line 6
    .line 7
    invoke-direct {v1, p0, v0}, Lcom/dropbox/core/v2/clouddocs/RenameBuilder;-><init>(Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;Lcom/dropbox/core/v2/clouddocs/RenameArg$Builder;)V

    .line 8
    .line 9
    .line 10
    return-object v1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public unlock()Lcom/dropbox/core/v2/clouddocs/UnlockResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UnlockArg;

    invoke-direct {v0}, Lcom/dropbox/core/v2/clouddocs/UnlockArg;-><init>()V

    .line 4
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->unlock(Lcom/dropbox/core/v2/clouddocs/UnlockArg;)Lcom/dropbox/core/v2/clouddocs/UnlockResult;

    move-result-object v0

    return-object v0
.end method

.method unlock(Lcom/dropbox/core/v2/clouddocs/UnlockArg;)Lcom/dropbox/core/v2/clouddocs/UnlockResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getApi()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/unlock"

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/core/v2/clouddocs/UnlockArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/UnlockArg$Serializer;

    sget-object v6, Lcom/dropbox/core/v2/clouddocs/UnlockResult$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/UnlockResult$Serializer;

    sget-object v7, Lcom/dropbox/core/v2/clouddocs/LockingError$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/LockingError$Serializer;

    move-object v3, p1

    invoke-virtual/range {v0 .. v7}, Lcom/dropbox/core/v2/DbxRawClientV2;->rpcStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;Lcom/dropbox/core/stone/StoneSerializer;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/UnlockResult;
    :try_end_0
    .catch Lcom/dropbox/core/DbxWrappedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 2
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/LockingErrorException;

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getUserMessage()Lcom/dropbox/core/LocalizedText;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dropbox/core/DbxWrappedException;->getErrorValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/dropbox/core/v2/clouddocs/LockingError;

    const-string v3, "2/cloud_docs/unlock"

    invoke-direct {v0, v3, v1, v2, p1}, Lcom/dropbox/core/v2/clouddocs/LockingErrorException;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/LocalizedText;Lcom/dropbox/core/v2/clouddocs/LockingError;)V

    throw v0
.end method

.method public unlock(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/UnlockResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/v2/clouddocs/LockingErrorException;,
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 5
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UnlockArg;

    invoke-direct {v0, p1}, Lcom/dropbox/core/v2/clouddocs/UnlockArg;-><init>(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->unlock(Lcom/dropbox/core/v2/clouddocs/UnlockArg;)Lcom/dropbox/core/v2/clouddocs/UnlockResult;

    move-result-object p1

    return-object p1

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value for \'fileId\' is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method updateContent(Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;)Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v0}, Lcom/dropbox/core/v2/DbxRawClientV2;->getHost()Lcom/dropbox/core/DbxHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/core/DbxHost;->getContent()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2/cloud_docs/update_content"

    const/4 v4, 0x0

    sget-object v5, Lcom/dropbox/core/v2/clouddocs/UpdateContentArg$Serializer;->INSTANCE:Lcom/dropbox/core/v2/clouddocs/UpdateContentArg$Serializer;

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/core/v2/DbxRawClientV2;->uploadStyle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZLcom/dropbox/core/stone/StoneSerializer;)Lcom/dropbox/core/http/HttpRequestor$Uploader;

    move-result-object p1

    .line 2
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;

    iget-object v1, p0, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->client:Lcom/dropbox/core/v2/DbxRawClientV2;

    invoke-virtual {v1}, Lcom/dropbox/core/v2/DbxRawClientV2;->getUserId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;-><init>(Lcom/dropbox/core/http/HttpRequestor$Uploader;Ljava/lang/String;)V

    return-object v0
.end method

.method public updateContent(Ljava/lang/String;Ljava/util/List;)Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;

    invoke-direct {v0, p1, p2}, Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 4
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->updateContent(Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;)Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;

    move-result-object p1

    return-object p1
.end method

.method public updateContent(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/dropbox/core/v2/clouddocs/Content;",
            ">;)",
            "Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/core/DbxException;
        }
    .end annotation

    if-eqz p3, :cond_1

    .line 5
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/core/v2/clouddocs/Content;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "An item in list \'additionalContents\' is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 7
    :cond_1
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;

    invoke-direct {v0, p1, p2, p3}, Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 8
    invoke-virtual {p0, v0}, Lcom/dropbox/core/v2/clouddocs/DbxUserCloudDocsRequests;->updateContent(Lcom/dropbox/core/v2/clouddocs/UpdateContentArg;)Lcom/dropbox/core/v2/clouddocs/UpdateContentUploader;

    move-result-object p1

    return-object p1
.end method

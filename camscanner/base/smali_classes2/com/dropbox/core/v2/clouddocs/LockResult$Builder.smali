.class public Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;
.super Ljava/lang/Object;
.source "LockResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/clouddocs/LockResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected expiresAt:J

.field protected fileId:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "\"\""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->fileId:Ljava/lang/String;

    .line 7
    .line 8
    const-wide/16 v0, 0x0

    .line 9
    .line 10
    iput-wide v0, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->expiresAt:J

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/clouddocs/LockResult;
    .locals 4

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/LockResult;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->fileId:Ljava/lang/String;

    .line 4
    .line 5
    iget-wide v2, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->expiresAt:J

    .line 6
    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/core/v2/clouddocs/LockResult;-><init>(Ljava/lang/String;J)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public withExpiresAt(Ljava/lang/Long;)Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    iput-wide v0, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->expiresAt:J

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-wide/16 v0, 0x0

    .line 11
    .line 12
    iput-wide v0, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->expiresAt:J

    .line 13
    .line 14
    :goto_0
    return-object p0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withFileId(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->fileId:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/LockResult$Builder;->fileId:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

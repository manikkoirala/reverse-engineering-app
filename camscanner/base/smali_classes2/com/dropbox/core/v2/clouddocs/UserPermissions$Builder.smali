.class public Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;
.super Ljava/lang/Object;
.source "UserPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/clouddocs/UserPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected canComment:Z

.field protected canDownload:Z

.field protected canEdit:Z

.field protected canRename:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canEdit:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canRename:Z

    .line 8
    .line 9
    iput-boolean v0, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canComment:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canDownload:Z

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/clouddocs/UserPermissions;
    .locals 5

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UserPermissions;

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canEdit:Z

    .line 4
    .line 5
    iget-boolean v2, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canRename:Z

    .line 6
    .line 7
    iget-boolean v3, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canComment:Z

    .line 8
    .line 9
    iget-boolean v4, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canDownload:Z

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/dropbox/core/v2/clouddocs/UserPermissions;-><init>(ZZZZ)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
.end method

.method public withCanComment(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canComment:Z

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canComment:Z

    .line 12
    .line 13
    :goto_0
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withCanDownload(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canDownload:Z

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canDownload:Z

    .line 12
    .line 13
    :goto_0
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withCanEdit(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canEdit:Z

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canEdit:Z

    .line 12
    .line 13
    :goto_0
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withCanRename(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canRename:Z

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/UserPermissions$Builder;->canRename:Z

    .line 12
    .line 13
    :goto_0
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

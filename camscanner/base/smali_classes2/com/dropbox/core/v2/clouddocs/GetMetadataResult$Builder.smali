.class public Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
.super Ljava/lang/Object;
.source "GetMetadataResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected fileId:Ljava/lang/String;

.field protected isDeleted:Z

.field protected mimeType:Ljava/lang/String;

.field protected providerVersion:Ljava/lang/String;

.field protected title:Ljava/lang/String;

.field protected user:Lcom/dropbox/core/v2/clouddocs/UserInfo;

.field protected userPermissions:Lcom/dropbox/core/v2/clouddocs/UserPermissions;

.field protected version:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "\"\""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->fileId:Ljava/lang/String;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->title:Ljava/lang/String;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->mimeType:Ljava/lang/String;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->version:Ljava/lang/String;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->providerVersion:Ljava/lang/String;

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->user:Lcom/dropbox/core/v2/clouddocs/UserInfo;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    iput-boolean v1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->isDeleted:Z

    .line 21
    .line 22
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->userPermissions:Lcom/dropbox/core/v2/clouddocs/UserPermissions;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;
    .locals 10

    .line 1
    new-instance v9, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->fileId:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->title:Ljava/lang/String;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->mimeType:Ljava/lang/String;

    .line 8
    .line 9
    iget-object v4, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->version:Ljava/lang/String;

    .line 10
    .line 11
    iget-object v5, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->providerVersion:Ljava/lang/String;

    .line 12
    .line 13
    iget-object v6, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->user:Lcom/dropbox/core/v2/clouddocs/UserInfo;

    .line 14
    .line 15
    iget-boolean v7, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->isDeleted:Z

    .line 16
    .line 17
    iget-object v8, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->userPermissions:Lcom/dropbox/core/v2/clouddocs/UserPermissions;

    .line 18
    .line 19
    move-object v0, v9

    .line 20
    invoke-direct/range {v0 .. v8}, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/core/v2/clouddocs/UserInfo;ZLcom/dropbox/core/v2/clouddocs/UserPermissions;)V

    .line 21
    .line 22
    .line 23
    return-object v9
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public withFileId(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->fileId:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->fileId:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withIsDeleted(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->isDeleted:Z

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->isDeleted:Z

    .line 12
    .line 13
    :goto_0
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withMimeType(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->mimeType:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->mimeType:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withProviderVersion(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->providerVersion:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->providerVersion:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withTitle(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->title:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->title:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withUser(Lcom/dropbox/core/v2/clouddocs/UserInfo;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->user:Lcom/dropbox/core/v2/clouddocs/UserInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withUserPermissions(Lcom/dropbox/core/v2/clouddocs/UserPermissions;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->userPermissions:Lcom/dropbox/core/v2/clouddocs/UserPermissions;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withVersion(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->version:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/GetMetadataResult$Builder;->version:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

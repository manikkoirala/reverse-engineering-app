.class public Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;
.super Ljava/lang/Object;
.source "UserInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/clouddocs/UserInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected email:Ljava/lang/String;

.field protected id:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "\"\""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->id:Ljava/lang/String;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->email:Ljava/lang/String;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/clouddocs/UserInfo;
    .locals 3

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/clouddocs/UserInfo;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->id:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->email:Ljava/lang/String;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/clouddocs/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public withEmail(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->email:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->email:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public withId(Ljava/lang/String;)Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->id:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "\"\""

    .line 7
    .line 8
    iput-object p1, p0, Lcom/dropbox/core/v2/clouddocs/UserInfo$Builder;->id:Ljava/lang/String;

    .line 9
    .line 10
    :goto_0
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

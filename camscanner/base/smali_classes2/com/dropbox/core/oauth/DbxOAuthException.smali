.class public Lcom/dropbox/core/oauth/DbxOAuthException;
.super Lcom/dropbox/core/DbxException;
.source "DbxOAuthException.java"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final dbxOAuthError:Lcom/dropbox/core/oauth/DbxOAuthError;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/dropbox/core/oauth/DbxOAuthError;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/dropbox/core/oauth/DbxOAuthError;->getErrorDescription()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, p1, v0}, Lcom/dropbox/core/DbxException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iput-object p2, p0, Lcom/dropbox/core/oauth/DbxOAuthException;->dbxOAuthError:Lcom/dropbox/core/oauth/DbxOAuthError;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public getDbxOAuthError()Lcom/dropbox/core/oauth/DbxOAuthError;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/dropbox/core/oauth/DbxOAuthException;->dbxOAuthError:Lcom/dropbox/core/oauth/DbxOAuthError;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

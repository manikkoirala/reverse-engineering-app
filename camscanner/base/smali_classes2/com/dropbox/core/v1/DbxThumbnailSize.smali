.class public Lcom/dropbox/core/v1/DbxThumbnailSize;
.super Ljava/lang/Object;
.source "DbxThumbnailSize.java"


# static fields
.field public static final w1024h768:Lcom/dropbox/core/v1/DbxThumbnailSize;

.field public static final w128h128:Lcom/dropbox/core/v1/DbxThumbnailSize;

.field public static final w32h32:Lcom/dropbox/core/v1/DbxThumbnailSize;

.field public static final w640h480:Lcom/dropbox/core/v1/DbxThumbnailSize;

.field public static final w64h64:Lcom/dropbox/core/v1/DbxThumbnailSize;


# instance fields
.field public final height:I

.field public final ident:Ljava/lang/String;

.field public final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 2
    .line 3
    const-string v1, "xs"

    .line 4
    .line 5
    const/16 v2, 0x20

    .line 6
    .line 7
    invoke-direct {v0, v1, v2, v2}, Lcom/dropbox/core/v1/DbxThumbnailSize;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/dropbox/core/v1/DbxThumbnailSize;->w32h32:Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 11
    .line 12
    new-instance v0, Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 13
    .line 14
    const-string v1, "s"

    .line 15
    .line 16
    const/16 v2, 0x40

    .line 17
    .line 18
    invoke-direct {v0, v1, v2, v2}, Lcom/dropbox/core/v1/DbxThumbnailSize;-><init>(Ljava/lang/String;II)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/dropbox/core/v1/DbxThumbnailSize;->w64h64:Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 22
    .line 23
    new-instance v0, Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 24
    .line 25
    const-string v1, "m"

    .line 26
    .line 27
    const/16 v2, 0x80

    .line 28
    .line 29
    invoke-direct {v0, v1, v2, v2}, Lcom/dropbox/core/v1/DbxThumbnailSize;-><init>(Ljava/lang/String;II)V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/dropbox/core/v1/DbxThumbnailSize;->w128h128:Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 33
    .line 34
    new-instance v0, Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 35
    .line 36
    const/16 v1, 0x280

    .line 37
    .line 38
    const/16 v2, 0x1e0

    .line 39
    .line 40
    const-string v3, "l"

    .line 41
    .line 42
    invoke-direct {v0, v3, v1, v2}, Lcom/dropbox/core/v1/DbxThumbnailSize;-><init>(Ljava/lang/String;II)V

    .line 43
    .line 44
    .line 45
    sput-object v0, Lcom/dropbox/core/v1/DbxThumbnailSize;->w640h480:Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 46
    .line 47
    new-instance v0, Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 48
    .line 49
    const/16 v1, 0x400

    .line 50
    .line 51
    const/16 v2, 0x300

    .line 52
    .line 53
    const-string v3, "xl"

    .line 54
    .line 55
    invoke-direct {v0, v3, v1, v2}, Lcom/dropbox/core/v1/DbxThumbnailSize;-><init>(Ljava/lang/String;II)V

    .line 56
    .line 57
    .line 58
    sput-object v0, Lcom/dropbox/core/v1/DbxThumbnailSize;->w1024h768:Lcom/dropbox/core/v1/DbxThumbnailSize;

    .line 59
    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->ident:Ljava/lang/String;

    .line 5
    .line 6
    iput p2, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->width:I

    .line 7
    .line 8
    iput p3, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->height:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "("

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->ident:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, " "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->width:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, "x"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/dropbox/core/v1/DbxThumbnailSize;->height:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, ")"

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

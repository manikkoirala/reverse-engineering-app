.class public final Lcom/bytedance/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final app_name:I = 0x7f1304ed

.field public static final common_google_play_services_enable_button:I = 0x7f1305a7

.field public static final common_google_play_services_enable_text:I = 0x7f1305a8

.field public static final common_google_play_services_enable_title:I = 0x7f1305a9

.field public static final common_google_play_services_install_button:I = 0x7f1305aa

.field public static final common_google_play_services_install_text:I = 0x7f1305ab

.field public static final common_google_play_services_install_title:I = 0x7f1305ac

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1305ad

.field public static final common_google_play_services_notification_ticker:I = 0x7f1305ae

.field public static final common_google_play_services_unknown_issue:I = 0x7f1305af

.field public static final common_google_play_services_unsupported_text:I = 0x7f1305b0

.field public static final common_google_play_services_update_button:I = 0x7f1305b1

.field public static final common_google_play_services_update_text:I = 0x7f1305b2

.field public static final common_google_play_services_update_title:I = 0x7f1305b3

.field public static final common_google_play_services_updating_text:I = 0x7f1305b4

.field public static final common_google_play_services_wear_update_text:I = 0x7f1305b5

.field public static final common_open_on_phone:I = 0x7f1305b6

.field public static final common_signin_button_text:I = 0x7f1305b7

.field public static final common_signin_button_text_long:I = 0x7f1305b8

.field public static final offline_notification_text:I = 0x7f131e2f

.field public static final offline_notification_title:I = 0x7f131e30

.field public static final offline_opt_in_confirm:I = 0x7f131e31

.field public static final offline_opt_in_confirmation:I = 0x7f131e32

.field public static final offline_opt_in_decline:I = 0x7f131e33

.field public static final offline_opt_in_message:I = 0x7f131e34

.field public static final offline_opt_in_title:I = 0x7f131e35

.field public static final s1:I = 0x7f131e62

.field public static final s2:I = 0x7f131e63

.field public static final s3:I = 0x7f131e64

.field public static final s4:I = 0x7f131e65

.field public static final s5:I = 0x7f131e66

.field public static final s6:I = 0x7f131e67

.field public static final s7:I = 0x7f131e68

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca

.field public static final tt_00_00:I = 0x7f131eea

.field public static final tt_ad:I = 0x7f131eeb

.field public static final tt_ad_clicked_text:I = 0x7f131eec

.field public static final tt_ad_close_text:I = 0x7f131eed

.field public static final tt_ad_data_error:I = 0x7f131eee

.field public static final tt_ad_is_closed:I = 0x7f131eef

.field public static final tt_ad_logo_txt:I = 0x7f131ef0

.field public static final tt_ad_showed_text:I = 0x7f131ef1

.field public static final tt_adslot_empty:I = 0x7f131ef2

.field public static final tt_adslot_id_error:I = 0x7f131ef3

.field public static final tt_adslot_size_empty:I = 0x7f131ef4

.field public static final tt_adtype_not_match_rit:I = 0x7f131ef5

.field public static final tt_app_empty:I = 0x7f131ef6

.field public static final tt_app_name:I = 0x7f131ef7

.field public static final tt_auto_play_cancel_text:I = 0x7f131ef8

.field public static final tt_banner_ad_load_image_error:I = 0x7f131ef9

.field public static final tt_cancel:I = 0x7f131efa

.field public static final tt_choose_language:I = 0x7f131efb

.field public static final tt_click_to_replay:I = 0x7f131efc

.field public static final tt_comment_num:I = 0x7f131efd

.field public static final tt_comment_num_backup:I = 0x7f131efe

.field public static final tt_comment_score:I = 0x7f131eff

.field public static final tt_confirm_download:I = 0x7f131f00

.field public static final tt_confirm_download_have_app_name:I = 0x7f131f01

.field public static final tt_content_type:I = 0x7f131f02

.field public static final tt_count_down_view:I = 0x7f131f03

.field public static final tt_dislike_header_tv_back:I = 0x7f131f04

.field public static final tt_dislike_header_tv_title:I = 0x7f131f05

.field public static final tt_error_access_method_pass:I = 0x7f131f06

.field public static final tt_error_ad_able_false_msg:I = 0x7f131f07

.field public static final tt_error_ad_sec_false_msg:I = 0x7f131f08

.field public static final tt_error_ad_type:I = 0x7f131f09

.field public static final tt_error_adtype_differ:I = 0x7f131f0a

.field public static final tt_error_apk_sign_check_error:I = 0x7f131f0b

.field public static final tt_error_bidding_type:I = 0x7f131f0c

.field public static final tt_error_code_adcount_error:I = 0x7f131f0d

.field public static final tt_error_code_click_event_error:I = 0x7f131f0e

.field public static final tt_error_device_ip:I = 0x7f131f0f

.field public static final tt_error_empty_content:I = 0x7f131f10

.field public static final tt_error_image_size:I = 0x7f131f11

.field public static final tt_error_interstitial_version:I = 0x7f131f12

.field public static final tt_error_media_id:I = 0x7f131f13

.field public static final tt_error_media_type:I = 0x7f131f14

.field public static final tt_error_new_register_limit:I = 0x7f131f15

.field public static final tt_error_origin_ad_error:I = 0x7f131f16

.field public static final tt_error_package_name:I = 0x7f131f17

.field public static final tt_error_redirect:I = 0x7f131f18

.field public static final tt_error_request_invalid:I = 0x7f131f19

.field public static final tt_error_slot_id_app_id_differ:I = 0x7f131f1a

.field public static final tt_error_splash_ad_type:I = 0x7f131f1b

.field public static final tt_error_union_os_error:I = 0x7f131f1c

.field public static final tt_error_union_sdk_too_old:I = 0x7f131f1d

.field public static final tt_error_unknow:I = 0x7f131f1e

.field public static final tt_error_update_version:I = 0x7f131f1f

.field public static final tt_error_verify_reward:I = 0x7f131f20

.field public static final tt_feedback_experience_text:I = 0x7f131f21

.field public static final tt_feedback_submit_text:I = 0x7f131f22

.field public static final tt_feedback_thank_text:I = 0x7f131f23

.field public static final tt_frequent_call_erroe:I = 0x7f131f24

.field public static final tt_full_screen_skip_tx:I = 0x7f131f25

.field public static final tt_get_reward:I = 0x7f131f26

.field public static final tt_init_setting_config_not_complete:I = 0x7f131f27

.field public static final tt_insert_ad_load_image_error:I = 0x7f131f28

.field public static final tt_label_cancel:I = 0x7f131f29

.field public static final tt_label_ok:I = 0x7f131f2a

.field public static final tt_lack_android_manifest_configuration:I = 0x7f131f2b

.field public static final tt_load_creative_icon_error:I = 0x7f131f2c

.field public static final tt_load_creative_icon_response_error:I = 0x7f131f2d

.field public static final tt_load_failed_text:I = 0x7f131f2e

.field public static final tt_load_success_text:I = 0x7f131f2f

.field public static final tt_loading_language:I = 0x7f131f30

.field public static final tt_logo_cn:I = 0x7f131f31

.field public static final tt_logo_en:I = 0x7f131f32

.field public static final tt_msgPlayable:I = 0x7f131f33

.field public static final tt_negtiveBtnBtnText:I = 0x7f131f34

.field public static final tt_negtive_txt:I = 0x7f131f35

.field public static final tt_net_error:I = 0x7f131f36

.field public static final tt_no_ad:I = 0x7f131f37

.field public static final tt_no_ad_parse:I = 0x7f131f38

.field public static final tt_no_network:I = 0x7f131f39

.field public static final tt_no_thank_you:I = 0x7f131f3a

.field public static final tt_parse_fail:I = 0x7f131f3b

.field public static final tt_permission_denied:I = 0x7f131f3c

.field public static final tt_playable_btn_play:I = 0x7f131f3d

.field public static final tt_postiveBtnText:I = 0x7f131f3e

.field public static final tt_postiveBtnTextPlayable:I = 0x7f131f3f

.field public static final tt_postive_txt:I = 0x7f131f40

.field public static final tt_privacy_title:I = 0x7f131f41

.field public static final tt_reder_ad_load_timeout:I = 0x7f131f42

.field public static final tt_render_diff_template_invalid:I = 0x7f131f43

.field public static final tt_render_fail_meta_invalid:I = 0x7f131f44

.field public static final tt_render_fail_template_parse_error:I = 0x7f131f45

.field public static final tt_render_fail_timeout:I = 0x7f131f46

.field public static final tt_render_fail_unknown:I = 0x7f131f47

.field public static final tt_render_main_template_invalid:I = 0x7f131f48

.field public static final tt_render_render_parse_error:I = 0x7f131f49

.field public static final tt_request_body_error:I = 0x7f131f4a

.field public static final tt_request_pb_error:I = 0x7f131f4b

.field public static final tt_reward_feedback:I = 0x7f131f4f

.field public static final tt_reward_full_skip:I = 0x7f131f50

.field public static final tt_reward_msg:I = 0x7f131f51

.field public static final tt_reward_screen_skip_tx:I = 0x7f131f52

.field public static final tt_reward_video_show_error:I = 0x7f131f53

.field public static final tt_ror_code_show_event_error:I = 0x7f131f54

.field public static final tt_skip_ad_time_text:I = 0x7f131f55

.field public static final tt_slide_up_3d:I = 0x7f131f56

.field public static final tt_splash_ad_load_image_error:I = 0x7f131f57

.field public static final tt_splash_brush_mask_hint:I = 0x7f131f58

.field public static final tt_splash_brush_mask_title:I = 0x7f131f59

.field public static final tt_splash_cache_expired_error:I = 0x7f131f5a

.field public static final tt_splash_cache_parse_error:I = 0x7f131f5b

.field public static final tt_splash_default_click_shake:I = 0x7f131f5c

.field public static final tt_splash_not_have_cache_error:I = 0x7f131f5d

.field public static final tt_splash_rock_text:I = 0x7f131f5e

.field public static final tt_splash_rock_top_text:I = 0x7f131f5f

.field public static final tt_splash_skip_tv:I = 0x7f131f60

.field public static final tt_splash_wriggle_text:I = 0x7f131f61

.field public static final tt_splash_wriggle_top_text:I = 0x7f131f62

.field public static final tt_splash_wriggle_top_text_style_17:I = 0x7f131f63

.field public static final tt_sys_error:I = 0x7f131f64

.field public static final tt_template_load_fail:I = 0x7f131f65

.field public static final tt_text_privacy_app_version:I = 0x7f131f66

.field public static final tt_text_privacy_development:I = 0x7f131f67

.field public static final tt_tip:I = 0x7f131f68

.field public static final tt_toast_ad_on_rewarded:I = 0x7f131f69

.field public static final tt_toast_later_download:I = 0x7f131f6a

.field public static final tt_toast_no_ad:I = 0x7f131f6b

.field public static final tt_toast_start_loading:I = 0x7f131f6c

.field public static final tt_toast_tiktok_ad_failed:I = 0x7f131f6d

.field public static final tt_try_now:I = 0x7f131f6e

.field public static final tt_txt_skip:I = 0x7f131f6f

.field public static final tt_unlike:I = 0x7f131f70

.field public static final tt_video_bytesize:I = 0x7f131f71

.field public static final tt_video_bytesize_M:I = 0x7f131f72

.field public static final tt_video_bytesize_MB:I = 0x7f131f73

.field public static final tt_video_continue_play:I = 0x7f131f74

.field public static final tt_video_dial_phone:I = 0x7f131f75

.field public static final tt_video_download_apk:I = 0x7f131f76

.field public static final tt_video_mobile_go_detail:I = 0x7f131f77

.field public static final tt_video_retry_des:I = 0x7f131f78

.field public static final tt_video_retry_des_txt:I = 0x7f131f79

.field public static final tt_video_without_wifi_tips:I = 0x7f131f7a

.field public static final tt_wap_empty:I = 0x7f131f7b

.field public static final tt_web_title_default:I = 0x7f131f7c

.field public static final tt_will_play:I = 0x7f131f7d

.field public static final tt_yes_i_agree:I = 0x7f131f7e


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/bytedance/sdk/openadsdk/o/e;
.super Ljava/lang/Object;
.source "PlayableJsBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/o/e$z;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/o/g;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/o/e$z;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/hardware/SensorEventListener;

.field private e:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/o/g;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    .line 10
    .line 11
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/e$k;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/e$k;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->d:Landroid/hardware/SensorEventListener;

    .line 17
    .line 18
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/e$r;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/e$r;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->e:Landroid/hardware/SensorEventListener;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g;->f()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->a:Landroid/content/Context;

    .line 30
    .line 31
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 32
    .line 33
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->b:Ljava/lang/ref/WeakReference;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/e;->d()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/o/e;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/e;->c()Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p0

    return-object p0
.end method

.method private b()Lcom/bytedance/sdk/openadsdk/o/a;
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/e;->c()Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b()Lcom/bytedance/sdk/openadsdk/o/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/o/e;)Lcom/bytedance/sdk/openadsdk/o/a;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/e;->b()Lcom/bytedance/sdk/openadsdk/o/a;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/o/e;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->a:Landroid/content/Context;

    return-object p0
.end method

.method private c()Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 3
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/o/g;

    return-object v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/o/e;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->d:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method private d()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$s;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$s;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "adInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$t;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$t;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "appInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$u;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$u;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "playableSDKInfo"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$v;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$v;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "subscribe_app_ad"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$w;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$w;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "download_app_ad"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$x;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$x;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "isViewable"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$y;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$y;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "getVolume"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$a;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$a;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "getScreenSize"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$b;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$b;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "start_accelerometer_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$c;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$c;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "close_accelerometer_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$d;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$d;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "start_gyro_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$e;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$e;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "close_gyro_observer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$f;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$f;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "device_shake"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$g;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$g;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "playable_style"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$h;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$h;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "sendReward"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$i;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$i;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "webview_time_track"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$j;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$j;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "playable_event"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$l;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$l;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "reportAd"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$m;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$m;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "close"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$n;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$n;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "openAdLandPageLinks"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$o;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$o;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "get_viewport"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$p;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$p;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "jssdk_load_finish"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/o/e$q;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/o/e$q;-><init>(Lcom/bytedance/sdk/openadsdk/o/e;)V

    const-string v2, "material_render_result"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/o/e;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->e:Landroid/hardware/SensorEventListener;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2

    const/4 v0, 0x0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/e;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/o/e$z;

    if-nez p1, :cond_0

    return-object v0

    .line 4
    :cond_0
    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/e$z;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    const-string p2, "PlayableJsBridge"

    const-string v1, "invoke error"

    .line 5
    invoke-static {p2, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public e()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/e;->d:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/i;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/e;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/e;->e:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/i;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    return-void
.end method

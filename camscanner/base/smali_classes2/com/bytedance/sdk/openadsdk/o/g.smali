.class public Lcom/bytedance/sdk/openadsdk/o/g;
.super Ljava/lang/Object;
.source "PlayablePlugin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/o/g$h;
    }
.end annotation


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/o/a;

.field private B:Lcom/bytedance/sdk/openadsdk/o/c;

.field private C:I

.field private D:I

.field private E:Lorg/json/JSONObject;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lorg/json/JSONObject;

.field private K:Ljava/lang/String;

.field private L:Lorg/json/JSONObject;

.field private M:F

.field private N:I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field private X:Ljava/lang/String;

.field private Y:Z

.field private Z:Z

.field private final a:Landroid/os/Handler;

.field private a0:Z

.field private b:Ljava/lang/Runnable;

.field private b0:Z

.field private c:Ljava/lang/Runnable;

.field private c0:Ljava/lang/String;

.field private d:Ljava/util/Timer;

.field private d0:Ljava/lang/String;

.field private e:Ljava/util/TimerTask;

.field private e0:Z

.field private f:Lcom/bytedance/sdk/openadsdk/o/b;

.field private f0:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:J

.field private t:I

.field private u:I

.field private v:Lcom/bytedance/sdk/openadsdk/o/g$h;

.field private w:Landroid/content/Context;

.field private x:Landroid/webkit/WebView;

.field private y:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcom/bytedance/sdk/openadsdk/o/e;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/os/Handler;

    .line 5
    .line 6
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    .line 14
    .line 15
    new-instance v0, Ljava/util/Timer;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/Timer;

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Z

    .line 24
    .line 25
    new-instance v1, Ljava/util/HashSet;

    .line 26
    .line 27
    const-string v2, "subscribe_app_ad"

    .line 28
    .line 29
    const-string v3, "download_app_ad"

    .line 30
    .line 31
    const-string v4, "adInfo"

    .line 32
    .line 33
    const-string v5, "appInfo"

    .line 34
    .line 35
    filled-new-array {v4, v5, v2, v3}, [Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 44
    .line 45
    .line 46
    const/4 v1, 0x0

    .line 47
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    .line 48
    .line 49
    const-string v1, "embeded_ad"

    .line 50
    .line 51
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Ljava/lang/String;

    .line 52
    .line 53
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Z

    .line 54
    .line 55
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    .line 56
    .line 57
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->l:Z

    .line 58
    .line 59
    const-wide/16 v0, 0xa

    .line 60
    .line 61
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:J

    .line 62
    .line 63
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->n:J

    .line 64
    .line 65
    const-wide/16 v0, 0x0

    .line 66
    .line 67
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    .line 68
    .line 69
    const-wide/16 v0, -0x1

    .line 70
    .line 71
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    .line 72
    .line 73
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:J

    .line 74
    .line 75
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->r:J

    .line 76
    .line 77
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->s:J

    .line 78
    .line 79
    const/4 v0, 0x0

    .line 80
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->t:I

    .line 81
    .line 82
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->u:I

    .line 83
    .line 84
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->C:I

    .line 85
    .line 86
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->D:I

    .line 87
    .line 88
    new-instance v1, Lorg/json/JSONObject;

    .line 89
    .line 90
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 91
    .line 92
    .line 93
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->E:Lorg/json/JSONObject;

    .line 94
    .line 95
    new-instance v1, Ljava/util/HashMap;

    .line 96
    .line 97
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 98
    .line 99
    .line 100
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->I:Ljava/util/Map;

    .line 101
    .line 102
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->e0:Z

    .line 103
    .line 104
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$a;

    .line 105
    .line 106
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$a;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 107
    .line 108
    .line 109
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f0:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 110
    .line 111
    sget-object v0, Lcom/bytedance/sdk/openadsdk/o/g$h;->a:Lcom/bytedance/sdk/openadsdk/o/g$h;

    .line 112
    .line 113
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    .line 114
    .line 115
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->x:Landroid/webkit/WebView;

    .line 116
    .line 117
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/o/h;->b(Landroid/webkit/WebView;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Landroid/view/View;)V

    .line 121
    .line 122
    .line 123
    invoke-direct {p0, p1, p3, p4}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)V

    .line 124
    .line 125
    .line 126
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->q()V

    .line 127
    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method public static a(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/o/g;-><init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)V

    return-object v0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/o/g;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->y:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method private a(ILjava/lang/String;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/a;->a(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)V
    .locals 1

    .line 4
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    .line 5
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->w:Landroid/content/Context;

    .line 6
    new-instance p1, Lcom/bytedance/sdk/openadsdk/o/e;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/o/e;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->z:Lcom/bytedance/sdk/openadsdk/o/e;

    .line 7
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    .line 8
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->B:Lcom/bytedance/sdk/openadsdk/o/c;

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 9
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->C:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->D:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_1

    return-void

    .line 10
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->C:I

    .line 11
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->D:I

    .line 12
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "width"

    .line 13
    :try_start_1
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->C:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v0, "height"

    .line 14
    :try_start_2
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->D:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "resize"

    .line 15
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 16
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->E:Lorg/json/JSONObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "resetViewDataJsonByView error"

    .line 17
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/o/g;Landroid/view/View;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/o/g;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    return p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "rubeex://playable-lynx?accessKey=%1s&groupId=%2s&cardId=main"

    .line 18
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 6

    const-string v0, "log_extra"

    const-string v1, "PlayablePlugin"

    .line 19
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    if-nez p2, :cond_1

    .line 20
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :cond_1
    :try_start_0
    const-string v2, "playable_event"

    .line 21
    invoke-virtual {p2, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string p1, "playable_ts"

    .line 22
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p1, "playable_viewable"

    .line 23
    :try_start_2
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    invoke-virtual {p2, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string p1, "playable_session_id"

    .line 24
    :try_start_3
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->h:Ljava/lang/String;

    invoke-virtual {p2, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/o/g$h;->a:Lcom/bytedance/sdk/openadsdk/o/g$h;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v3, "playable_url"

    if-ne p1, v2, :cond_2

    .line 26
    :try_start_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->K:Ljava/lang/String;

    invoke-virtual {p2, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 27
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c0:Ljava/lang/String;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d0:Ljava/lang/String;

    invoke-direct {p0, p1, v4}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_0
    const-string p1, "playable_is_prerender"

    .line 28
    :try_start_5
    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b0:Z

    invoke-virtual {p2, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string p1, "playable_render_type"

    .line 29
    :try_start_6
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    invoke-virtual {p2, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "playable_sdk_version"

    const-string v3, "5.2.2"

    .line 30
    invoke-virtual {p2, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 31
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "ad_extra_data"

    .line 32
    invoke-virtual {p1, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v3, "tag"

    .line 33
    :try_start_7
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->i:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "nt"

    const/4 v4, 0x4

    .line 34
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "category"

    const-string v4, "umeng"

    .line 35
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "is_ad_event"

    const-string v4, "1"

    .line 36
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "refer"

    const-string v4, "playable"

    .line 37
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v3, "value"

    .line 38
    :try_start_8
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->J:Lorg/json/JSONObject;

    const-string v5, "cid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 39
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->J:Lorg/json/JSONObject;

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    if-eqz v0, :cond_5

    .line 41
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v3, "playable_track"

    const-string v4, "reportEvent by ActionProxy"

    if-ne v0, v2, :cond_3

    :try_start_9
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    invoke-static {v1, v4}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, v3, p1}, Lcom/bytedance/sdk/openadsdk/o/a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 44
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/a;->b(Lorg/json/JSONObject;)V

    goto :goto_1

    .line 45
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    if-eq v0, v2, :cond_4

    .line 46
    invoke-static {v1, v4}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, v3, p1}, Lcom/bytedance/sdk/openadsdk/o/a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 48
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/a;->b(Lorg/json/JSONObject;)V

    goto :goto_1

    :cond_4
    const-string p1, "reportEvent error no not playable url"

    .line 49
    invoke-static {v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string p1, "reportEvent error no impl"

    .line 50
    invoke-static {v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    const-string p2, "reportEvent error"

    .line 51
    invoke-static {v1, p2, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/o/g;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    return p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/o/g;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/o/g;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/o/g;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/o/g;)Landroid/webkit/WebView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->x:Landroid/webkit/WebView;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/o/g;)Lcom/bytedance/sdk/openadsdk/o/b;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/o/g;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->t:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->t:I

    return v0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/o/g;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->u:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->u:I

    return v0
.end method

.method private q()V
    .locals 1

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/b;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/b;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;

    .line 7
    .line 8
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$b;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$b;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    .line 14
    .line 15
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$c;

    .line 16
    .line 17
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$c;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    .line 21
    .line 22
    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$d;

    .line 23
    .line 24
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$d;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->e:Ljava/util/TimerTask;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private r()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->K:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const-string v1, "/union-fe/playable/"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->K:Ljava/lang/String;

    .line 14
    .line 15
    const-string v1, "/union-fe-sg/playable/"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->K:Ljava/lang/String;

    .line 24
    .line 25
    const-string v1, "/union-fe-i18n/playable/"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    :cond_0
    const/4 v0, 0x1

    .line 34
    return v0

    .line 35
    :cond_1
    const/4 v0, 0x0

    .line 36
    return v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->I:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Z)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 2

    .line 19
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Y:Z

    .line 20
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "endcard_mute"

    .line 21
    :try_start_1
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Y:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "volumeChange"

    .line 22
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setIsMute error"

    .line 23
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 7

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 79
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/f;->a()Z

    move-result v2

    const-string v3, ""

    const-string v4, "PlayablePlugin"

    if-eqz v2, :cond_1

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PlayablePlugin JSB-REQ ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "] "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object v5, v3

    :goto_0
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->z:Lcom/bytedance/sdk/openadsdk/o/e;

    invoke-virtual {v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/e;->a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    .line 82
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/f;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PlayablePlugin JSB-RSP ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "] time:"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object p2
.end method

.method public a()V
    .locals 7

    .line 33
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->e0:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->e0:Z

    .line 35
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->x()V

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->y:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f0:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :catchall_0
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->z:Lcom/bytedance/sdk/openadsdk/o/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/e;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 39
    :catchall_1
    :try_start_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 40
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 41
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;

    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/b;->a()V

    const/4 v0, 0x0

    .line 43
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v0

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "crash -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Playable_CrashMonitor"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_3
    :goto_0
    :try_start_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const-string v1, "playable_all_times"

    .line 46
    :try_start_4
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->t:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    const-string v1, "playable_hit_times"

    .line 47
    :try_start_5
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->u:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "PL_sdk_preload_times"

    .line 48
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 49
    :catchall_3
    :try_start_6
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-eqz v4, :cond_4

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    sub-long/2addr v0, v4

    const-string v4, "PlayablePlugin"

    .line 51
    :try_start_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "playable show time +"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    .line 53
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    .line 54
    :cond_4
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    const-string v1, "playable_user_play_duration"

    .line 55
    :try_start_8
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "PL_sdk_user_play_duration"

    .line 56
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :catchall_4
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 57
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "errorCode"

    .line 58
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "description"

    .line 59
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "failingUrl"

    .line 60
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string p2, "PlayablePlugin"

    const-string p3, "onWebReceivedError error"

    .line 61
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    const-string p1, "PL_sdk_html_load_error"

    .line 62
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 63
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 64
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    .line 65
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 66
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    const-string p2, "ContainerLoadFail"

    .line 67
    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 32
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$f;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$f;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "success"

    const/4 v1, 0x1

    .line 24
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-nez p1, :cond_0

    .line 25
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 26
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    .line 27
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 28
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 p1, 0x4

    const-string v0, "CaseRenderFail"

    .line 29
    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(ZLjava/lang/String;I)V
    .locals 1

    if-eqz p1, :cond_0

    .line 68
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "errorCode"

    .line 69
    invoke-virtual {p1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p3, "failingUrl"

    .line 70
    invoke-virtual {p1, p3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p2

    const-string p3, "PlayablePlugin"

    const-string v0, "onWebReceivedHttpError error"

    .line 71
    invoke-static {p3, v0, p2}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    const-string p2, "PL_sdk_html_load_error"

    .line 72
    invoke-direct {p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 73
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 74
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->k:Z

    .line 75
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 76
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    const-string p2, "ContainerLoadFail"

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/o/a;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    return-object v0
.end method

.method public b(Z)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 2

    .line 6
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a0:Z

    .line 7
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "send_click"

    .line 8
    :try_start_1
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a0:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "change_playable_click"

    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setPlayableClick error"

    .line 10
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method protected b(ILjava/lang/String;)V
    .locals 2

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ILjava/lang/String;)V

    .line 13
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "playable_code"

    .line 14
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "playable_msg"

    .line 15
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string p2, "PlayablePlugin"

    const-string v1, "reportRenderFatal error"

    .line 16
    invoke-static {p2, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    const-string p1, "PL_sdk_global_faild"

    .line 17
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 2
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->y:Ljava/lang/ref/WeakReference;

    .line 3
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Landroid/view/View;)V

    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f0:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setViewForScreenSize error"

    .line 5
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 52
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/o/g$g;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/o/g$g;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected b(Lorg/json/JSONObject;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/a;->d(Lorg/json/JSONObject;)V

    return-void
.end method

.method public c(Z)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 7

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    if-ne v0, p1, :cond_0

    return-object p0

    .line 3
    :cond_0
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    if-eqz p1, :cond_1

    const-string p1, "PL_sdk_viewable_true"

    goto :goto_0

    :cond_1
    const-string p1, "PL_sdk_viewable_false"

    :goto_0
    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 5
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:J

    const-wide/16 v3, -0x1

    cmp-long p1, v1, v3

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    if-eqz p1, :cond_2

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:J

    const-string p1, "PL_sdk_page_show"

    .line 7
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 8
    :cond_2
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    const-string v0, "PlayablePlugin"

    if-eqz p1, :cond_3

    .line 9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    goto :goto_1

    .line 10
    :cond_3
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    cmp-long p1, v1, v3

    if-eqz p1, :cond_4

    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    sub-long/2addr v1, v5

    .line 12
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playable show time +"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    add-long/2addr v5, v1

    iput-wide v5, p0, Lcom/bytedance/sdk/openadsdk/o/g;->o:J

    .line 14
    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->p:J

    .line 15
    :cond_4
    :goto_1
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "viewStatus"

    .line 16
    :try_start_1
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "viewableChange"

    .line 17
    invoke-virtual {p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    const-string v1, "setViewable error"

    .line 18
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-object p0
.end method

.method public c()Lorg/json/JSONObject;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->J:Lorg/json/JSONObject;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 9

    const-string v0, "PlayablePlugin"

    .line 20
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 21
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->s:J

    const-string v2, "playable_full_url"

    .line 22
    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 23
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/o/g;->r:J

    const-wide/16 v4, -0x1

    cmp-long p1, v2, v4

    if-eqz p1, :cond_0

    .line 24
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->s:J

    sub-long/2addr v4, v2

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x0

    :goto_0
    const-string p1, "playable_html_load_start_duration"

    .line 25
    invoke-virtual {v1, p1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    const-string v2, "reportUrlLoadFinish error"

    .line 26
    invoke-static {v0, v2, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    const-string p1, "PL_sdk_html_load_finish"

    .line 27
    invoke-direct {p0, p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 28
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 29
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Z

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 30
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->g:Z

    .line 31
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->x:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/o/g;->t()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/o/g$e;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/o/g$e;-><init>(Lcom/bytedance/sdk/openadsdk/o/g;)V

    invoke-virtual {p1, v2, v3}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 32
    :cond_1
    :try_start_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/o/g$h;->a:Lcom/bytedance/sdk/openadsdk/o/g$h;

    if-ne p1, v2, :cond_2

    .line 33
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->l:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;

    if-eqz p1, :cond_2

    .line 34
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->l:Z

    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/b;->a(J)V

    .line 36
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/Timer;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/o/g;->e:Ljava/util/TimerTask;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x5dc

    invoke-virtual/range {v3 .. v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 37
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->f:Lcom/bytedance/sdk/openadsdk/o/b;

    const/16 v1, 0x3e8

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/b;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    const-string v1, "crashMonitor error"

    .line 38
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_2
    return-void
.end method

.method protected c(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .line 39
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 40
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method protected c(Lorg/json/JSONObject;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/a;->e(Lorg/json/JSONObject;)V

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->F:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 5

    .line 7
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 8
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->r:J

    const-string v1, "playable_full_url"

    .line 9
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 10
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->q:J

    const-wide/16 v3, -0x1

    cmp-long p1, v1, v3

    if-eqz p1, :cond_0

    .line 11
    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/o/g;->r:J

    sub-long/2addr v3, v1

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0x0

    :goto_0
    const-string p1, "playable_page_show_duration"

    .line 12
    invoke-virtual {v0, p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    const-string v1, "PlayablePlugin"

    const-string v2, "reportUrlLoadStart error"

    .line 13
    invoke-static {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    const-string p1, "PL_sdk_html_load_start"

    .line 14
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 15
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->v:Lcom/bytedance/sdk/openadsdk/o/g$h;

    sget-object v0, Lcom/bytedance/sdk/openadsdk/o/g$h;->a:Lcom/bytedance/sdk/openadsdk/o/g$h;

    if-ne p1, v0, :cond_1

    .line 16
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->m:J

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 17
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->n:J

    mul-long v1, v1, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 p1, 0x0

    .line 18
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->j:Z

    :cond_1
    return-void
.end method

.method public d(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/o/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CALL JS ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PlayablePlugin"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->B:Lcom/bytedance/sdk/openadsdk/o/c;

    if-eqz v0, :cond_2

    .line 6
    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/o/c;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_2
    return-void
.end method

.method protected d(Lorg/json/JSONObject;)V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/a;->f(Lorg/json/JSONObject;)V

    return-void
.end method

.method public e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->F:Ljava/lang/String;

    return-object p0
.end method

.method public e(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->J:Lorg/json/JSONObject;

    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->H:Ljava/lang/String;

    return-object v0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->w:Landroid/content/Context;

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->H:Ljava/lang/String;

    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->X:Ljava/lang/String;

    return-object p0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->X:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->G:Ljava/lang/String;

    return-object p0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->G:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 2

    .line 2
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "playable_style"

    .line 3
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->L:Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setPlayableStyle error"

    .line 5
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public i()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Y:Z

    return v0
.end method

.method public j(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;
    .locals 2

    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "?"

    .line 2
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    .line 3
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    :catchall_0
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->K:Ljava/lang/String;

    return-object p0
.end method

.method public j()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->z:Lcom/bytedance/sdk/openadsdk/o/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/e;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->I:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public l()Lcom/bytedance/sdk/openadsdk/o/d;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/a;->a()Lcom/bytedance/sdk/openadsdk/o/d;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public m()Lorg/json/JSONObject;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->L:Lorg/json/JSONObject;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public n()Lorg/json/JSONObject;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->E:Lorg/json/JSONObject;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Z:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public p()Lorg/json/JSONObject;
    .locals 8

    .line 1
    const-string v0, "y"

    .line 2
    .line 3
    const-string v1, "x"

    .line 4
    .line 5
    const-string v2, "height"

    .line 6
    .line 7
    const-string v3, "width"

    .line 8
    .line 9
    new-instance v4, Lorg/json/JSONObject;

    .line 10
    .line 11
    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v5, "devicePixelRatio"

    .line 15
    .line 16
    :try_start_0
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->M:F

    .line 17
    .line 18
    float-to-double v6, v6

    .line 19
    invoke-virtual {v4, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 20
    .line 21
    .line 22
    new-instance v5, Lorg/json/JSONObject;

    .line 23
    .line 24
    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 25
    .line 26
    .line 27
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->N:I

    .line 28
    .line 29
    invoke-virtual {v5, v3, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 30
    .line 31
    .line 32
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->O:I

    .line 33
    .line 34
    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 35
    .line 36
    .line 37
    const-string v6, "screen"

    .line 38
    .line 39
    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    .line 41
    .line 42
    new-instance v5, Lorg/json/JSONObject;

    .line 43
    .line 44
    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 45
    .line 46
    .line 47
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Q:I

    .line 48
    .line 49
    invoke-virtual {v5, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 50
    .line 51
    .line 52
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->P:I

    .line 53
    .line 54
    invoke-virtual {v5, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 55
    .line 56
    .line 57
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->R:I

    .line 58
    .line 59
    invoke-virtual {v5, v3, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 60
    .line 61
    .line 62
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->S:I

    .line 63
    .line 64
    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 65
    .line 66
    .line 67
    const-string v6, "webview"

    .line 68
    .line 69
    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    .line 71
    .line 72
    new-instance v5, Lorg/json/JSONObject;

    .line 73
    .line 74
    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 75
    .line 76
    .line 77
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/o/g;->U:I

    .line 78
    .line 79
    invoke-virtual {v5, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 80
    .line 81
    .line 82
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->T:I

    .line 83
    .line 84
    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 85
    .line 86
    .line 87
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->V:I

    .line 88
    .line 89
    invoke-virtual {v5, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 90
    .line 91
    .line 92
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->W:I

    .line 93
    .line 94
    invoke-virtual {v5, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 95
    .line 96
    .line 97
    const-string v0, "visible"

    .line 98
    .line 99
    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .line 101
    .line 102
    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    .line 104
    const-string v1, "PlayablePlugin"

    .line 105
    .line 106
    const-string v2, "getViewport error"

    .line 107
    .line 108
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 109
    .line 110
    .line 111
    :goto_0
    return-object v4
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public s()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->b:Ljava/lang/Runnable;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->a:Landroid/os/Handler;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/o/g;->c:Ljava/lang/Runnable;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "function playable_callJS(){return \"Android call the JS method is callJS\";}"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public u()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public v()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public w()V
    .locals 2

    .line 1
    const-string v0, "Playable_CrashMonitor"

    .line 2
    .line 3
    const-string v1, "-- Detected that the page is stuck for more than 2s and needs to be reported"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "PL_sdk_page_stuck"

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->d:Ljava/util/Timer;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public x()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->M:F

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->N:I

    .line 6
    .line 7
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->O:I

    .line 8
    .line 9
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->P:I

    .line 10
    .line 11
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->Q:I

    .line 12
    .line 13
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->R:I

    .line 14
    .line 15
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->S:I

    .line 16
    .line 17
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->T:I

    .line 18
    .line 19
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->U:I

    .line 20
    .line 21
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->V:I

    .line 22
    .line 23
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->W:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public y()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/o/g;->A:Lcom/bytedance/sdk/openadsdk/o/a;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/a;->b()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

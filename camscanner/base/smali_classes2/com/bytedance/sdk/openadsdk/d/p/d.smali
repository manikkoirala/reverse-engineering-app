.class public Lcom/bytedance/sdk/openadsdk/d/p/d;
.super Ljava/lang/Object;
.source "AdLogSwitchUtils.java"


# static fields
.field public static a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final b:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/bytedance/sdk/openadsdk/d/p/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 8
    .line 9
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 10
    .line 11
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/bytedance/sdk/openadsdk/d/p/d;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15
    .line 16
    return-void
    .line 17
.end method

.method public static a()Lcom/bytedance/sdk/openadsdk/l/e/a;
    .locals 1

    .line 22
    sget-object v0, Lcom/bytedance/sdk/openadsdk/d/p/l;->a:Lcom/bytedance/sdk/openadsdk/d/p/l;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    .line 1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/d/p/d;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    new-instance v0, Lb/b/a/a/i/a/a$b;

    invoke-direct {v0}, Lb/b/a/a/i/a/a$b;-><init>()V

    new-instance v1, Lcom/bytedance/sdk/openadsdk/d/p/j;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/d/p/j;-><init>()V

    .line 3
    invoke-virtual {v0, v1}, Lb/b/a/a/i/a/a$b;->O8(Lb/b/a/a/i/a/k/c;)Lb/b/a/a/i/a/a$b;

    move-result-object v0

    .line 4
    invoke-static {}, Lb/b/a/a/i/a/m/d/a;->〇080()Lb/b/a/a/i/a/m/d/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/b/a/a/i/a/a$b;->OO0o〇〇〇〇0(Lb/b/a/a/i/a/m/d/a;)Lb/b/a/a/i/a/a$b;

    move-result-object v0

    .line 5
    invoke-static {}, Lb/b/a/a/i/a/m/d/a;->〇o〇()Lb/b/a/a/i/a/m/d/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/b/a/a/i/a/a$b;->Oo08(Lb/b/a/a/i/a/m/d/a;)Lb/b/a/a/i/a/a$b;

    move-result-object v0

    .line 6
    invoke-static {}, Lb/b/a/a/i/a/m/d/a;->〇o00〇〇Oo()Lb/b/a/a/i/a/m/d/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/b/a/a/i/a/a$b;->〇80〇808〇O(Lb/b/a/a/i/a/m/d/a;)Lb/b/a/a/i/a/a$b;

    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lb/b/a/a/i/a/a$b;->o〇0(Z)Lb/b/a/a/i/a/a$b;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/p/k;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/p/k;-><init>()V

    .line 8
    invoke-virtual {p1, v0}, Lb/b/a/a/i/a/a$b;->〇o00〇〇Oo(Lb/b/a/a/i/a/f;)Lb/b/a/a/i/a/a$b;

    move-result-object p1

    sget-object v0, Lcom/bytedance/sdk/openadsdk/d/p/h;->b:Lcom/bytedance/sdk/openadsdk/d/p/h;

    .line 9
    invoke-virtual {p1, v0}, Lb/b/a/a/i/a/a$b;->〇o〇(Lb/b/a/a/i/a/j/e;)Lb/b/a/a/i/a/a$b;

    move-result-object p1

    .line 10
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->l()I

    move-result v0

    invoke-virtual {p1, v0}, Lb/b/a/a/i/a/a$b;->〇080(I)Lb/b/a/a/i/a/a$b;

    move-result-object p1

    .line 11
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->T()I

    move-result v0

    invoke-virtual {p1, v0}, Lb/b/a/a/i/a/a$b;->oO80(I)Lb/b/a/a/i/a/a$b;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lb/b/a/a/i/a/a$b;->〇〇888()Lb/b/a/a/i/a/a;

    move-result-object p1

    .line 13
    invoke-static {p1, p0}, Lb/b/a/a/i/a/b;->〇o00〇〇Oo(Lb/b/a/a/i/a/a;Landroid/content/Context;)V

    .line 14
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/d/p/d;->b()V

    :cond_0
    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/d/a;)V
    .locals 2

    .line 15
    new-instance v0, Lb/b/a/a/i/a/m/c/a;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/d/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lb/b/a/a/i/a/m/c/a;-><init>(Ljava/lang/String;Lb/b/a/a/i/a/m/c/b;)V

    .line 16
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/d/a;->g()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x2

    .line 17
    :goto_0
    invoke-virtual {v0, p0}, Lb/b/a/a/i/a/m/c/a;->〇o00〇〇Oo(B)V

    const/4 p0, 0x0

    .line 18
    invoke-virtual {v0, p0}, Lb/b/a/a/i/a/m/c/a;->〇080(B)V

    .line 19
    invoke-static {}, Lb/b/a/a/i/a/b;->o〇0()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 20
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/m/b;->b()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/bytedance/sdk/openadsdk/d/p/d;->a(Landroid/content/Context;Z)V

    .line 21
    :cond_1
    invoke-static {v0}, Lb/b/a/a/i/a/b;->〇o〇(Lb/b/a/a/i/a/m/a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .line 24
    invoke-static {}, Lb/b/a/a/i/a/b;->o〇0()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/m/b;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/p/d;->a(Landroid/content/Context;Z)V

    .line 26
    :cond_0
    invoke-static {p0}, Lb/b/a/a/i/a/b;->O8(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    .line 23
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/p/d$a;

    const-string v1, "track"

    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/d/p/d$a;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static b()V
    .locals 0

    .line 1
    invoke-static {}, Lb/b/a/a/i/a/b;->〇〇888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static c()V
    .locals 2

    .line 1
    const-string v0, "AdLogSwitchUtils"

    .line 2
    .line 3
    :try_start_0
    invoke-static {}, Lb/b/a/a/i/a/b;->oO80()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catchall_0
    move-exception v1

    .line 8
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :goto_0
    :try_start_1
    invoke-static {}, Lb/b/a/a/i/a/b;->〇80〇808〇O()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 16
    .line 17
    .line 18
    goto :goto_1

    .line 19
    :catchall_1
    move-exception v1

    .line 20
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :goto_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

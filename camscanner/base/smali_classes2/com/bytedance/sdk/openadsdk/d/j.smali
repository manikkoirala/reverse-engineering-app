.class public Lcom/bytedance/sdk/openadsdk/d/j;
.super Ljava/lang/Object;
.source "LandingPageLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/d/j$b;
    }
.end annotation


# static fields
.field private static final G:[I


# instance fields
.field private volatile A:J

.field private final B:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final C:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final D:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile E:I

.field private F:I

.field private a:I

.field private b:J

.field private c:I

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:I

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private final l:Landroid/content/Context;

.field private final m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field private n:Ljava/lang/String;

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:J

.field private t:Z

.field private final u:Ljava/util/concurrent/atomic/AtomicInteger;

.field private v:Landroid/webkit/WebView;

.field private w:Z

.field private x:Lcom/bytedance/sdk/openadsdk/d/o;

.field private y:Z

.field private z:Lcom/bytedance/sdk/openadsdk/d/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/bytedance/sdk/openadsdk/d/j;->G:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0xa
        0x1e
        0x32
        0x4b
        0x64
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/webkit/WebView;)V
    .locals 7

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->a:I

    const-wide/16 v1, -0x1

    .line 6
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->b:J

    const/4 v3, 0x1

    .line 7
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 8
    new-instance v3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 9
    new-instance v3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 10
    new-instance v3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    new-instance v3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, -0x1

    .line 12
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->h:I

    const-string v4, "landingpage"

    .line 13
    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-wide/16 v4, 0x0

    .line 14
    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->o:J

    .line 15
    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->p:J

    .line 16
    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->q:J

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->r:J

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->s:J

    .line 17
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->t:Z

    .line 18
    new-instance v6, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v6, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 19
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->w:Z

    .line 20
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->y:Z

    .line 21
    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    .line 22
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 23
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->C:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 24
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v4, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 25
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->E:I

    .line 26
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I

    .line 27
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->l:Landroid/content/Context;

    .line 28
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 29
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    if-nez p2, :cond_0

    return-void

    .line 30
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/j$b;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/bytedance/sdk/openadsdk/d/j$b;-><init>(Lcom/bytedance/sdk/openadsdk/d/j;Lcom/bytedance/sdk/openadsdk/d/j$a;)V

    const-string v3, "JS_LANDING_PAGE_LOG_OBJ"

    invoke-virtual {p2, v0, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 31
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->O()Lorg/json/JSONObject;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 32
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->O()Lorg/json/JSONObject;

    move-result-object p1

    const-string p2, "page_id"

    invoke-virtual {p1, p2, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->b:J

    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/d/i;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/d/j;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/webkit/WebView;)V

    .line 2
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->z:Lcom/bytedance/sdk/openadsdk/d/i;

    .line 3
    iput p4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/d/j;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->b:J

    return-wide v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/d/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "url"

    .line 31
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "type"

    .line 32
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    const-string p1, "load_finish_progress"

    .line 33
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    const-wide/16 v0, -0x1

    .line 122
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;J)V
    .locals 3

    .line 123
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->w:Z

    if-nez v0, :cond_0

    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    if-eqz p2, :cond_2

    const-string v0, "is_playable"

    .line 125
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "usecache"

    .line 126
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->b()Lcom/bytedance/sdk/openadsdk/core/j0/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    :catch_0
    :try_start_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    const-string v1, "ad_extra_data"

    .line 128
    :try_start_3
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-wide/16 v1, 0x0

    cmp-long p2, p3, v1

    if-lez p2, :cond_3

    const-string p2, "duration"

    .line 129
    invoke-virtual {v0, p2, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_1
    :cond_2
    const/4 v0, 0x0

    .line 130
    :catch_2
    :cond_3
    :goto_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    invoke-static {p2, p3, p1, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_4
    :goto_1
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/d/j;)Landroid/webkit/WebView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/d/j;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method


# virtual methods
.method public a(Z)Lcom/bytedance/sdk/openadsdk/d/j;
    .locals 0

    .line 6
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->w:Z

    return-object p0
.end method

.method public a()Lcom/bytedance/sdk/openadsdk/d/o;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->x:Lcom/bytedance/sdk/openadsdk/d/o;

    return-object v0
.end method

.method public a(I)V
    .locals 6

    .line 131
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 132
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->C:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 133
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result p1

    if-nez p1, :cond_1

    .line 134
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "url"

    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->H0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :catch_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    sub-long/2addr v0, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-string v2, "click_time"

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(J)V
    .locals 0

    .line 3
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->p:J

    return-void
.end method

.method public a(Landroid/webkit/WebView;I)V
    .locals 10

    if-nez p1, :cond_0

    return-void

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onWebProgress: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LandingPageLog"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    .line 10
    :cond_1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->q:J

    const/16 v4, 0x64

    cmp-long v5, v0, v2

    if-nez v5, :cond_2

    if-lez p2, :cond_2

    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->q:J

    goto :goto_0

    .line 12
    :cond_2
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->r:J

    cmp-long v5, v0, v2

    if-nez v5, :cond_3

    if-ne p2, v4, :cond_3

    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->r:J

    .line 14
    :cond_3
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->a:I

    sget-object v1, Lcom/bytedance/sdk/openadsdk/d/j;->G:[I

    array-length v1, v1

    if-eq v0, v1, :cond_7

    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_endcard"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_split_screen"

    .line 16
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_direct"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 17
    :cond_4
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->a:I

    :goto_1
    sget-object v1, Lcom/bytedance/sdk/openadsdk/d/j;->G:[I

    array-length v2, v1

    if-ge v0, v2, :cond_7

    .line 18
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->a:I

    aget v2, v1, v2

    if-ge p2, v2, :cond_5

    goto :goto_2

    :cond_5
    add-int/lit8 v2, v0, 0x1

    .line 19
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->a:I

    .line 20
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "url"

    .line 21
    :try_start_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 22
    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/d/j;->b:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v7, -0x1

    cmp-long v9, v5, v7

    if-eqz v9, :cond_6

    const-string v7, "page_id"

    .line 23
    :try_start_1
    invoke-virtual {v3, v7, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    :cond_6
    const-string v5, "render_type"

    const-string v6, "h5"

    .line 24
    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v5, "render_type_2"

    const/4 v6, 0x0

    .line 25
    :try_start_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v5, "pct"

    .line 26
    :try_start_3
    aget v0, v1, v0

    invoke-virtual {v3, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    const-string v0, "progress_load_finish"

    .line 27
    invoke-direct {p0, v0, v3}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    move v0, v2

    goto :goto_1

    :cond_7
    :goto_2
    if-ne p2, v4, :cond_8

    .line 28
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object p1

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->r:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->q:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    const-string p2, "progress"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Ljava/lang/String;J)V

    :cond_8
    return-void
.end method

.method public a(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 102
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->x:Lcom/bytedance/sdk/openadsdk/d/o;

    if-eqz p1, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/d/o;->m()V

    :cond_0
    if-eqz p5, :cond_1

    const-string p1, "image"

    .line 104
    invoke-virtual {p5, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_2

    .line 105
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    const/4 p5, 0x2

    if-eq p1, p5, :cond_2

    const/4 p1, 0x3

    .line 106
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 107
    :cond_2
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->h:I

    .line 108
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->j:Ljava/lang/String;

    .line 109
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->k:Ljava/lang/String;

    .line 110
    iput-boolean p6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->i:Z

    return-void
.end method

.method public a(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)V
    .locals 0
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x15
    .end annotation

    if-eqz p2, :cond_0

    .line 121
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .line 34
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    if-eqz p1, :cond_1

    .line 35
    invoke-virtual {p1}, Landroid/webkit/WebView;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 36
    invoke-virtual {p1}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result p2

    iget p3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->E:I

    if-le p2, p3, :cond_0

    .line 37
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 38
    :cond_0
    invoke-virtual {p1}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->E:I

    .line 39
    :cond_1
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    const-wide/16 v0, 0x0

    cmp-long p3, p1, v0

    if-nez p3, :cond_2

    .line 40
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->A:J

    .line 41
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->x:Lcom/bytedance/sdk/openadsdk/d/o;

    if-eqz p1, :cond_3

    .line 42
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/d/o;->n()V

    .line 43
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    const/4 p3, 0x0

    invoke-virtual {p1, p3, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 44
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string p2, "render_type"

    const-string v0, "h5"

    .line 45
    invoke-virtual {p1, p2, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string p2, "render_type_2"

    .line 46
    :try_start_1
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    iget p2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ltz p2, :cond_4

    const-string p3, "preload_status"

    .line 48
    :try_start_2
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_4
    const-string p2, "load_start"

    .line 49
    invoke-direct {p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_5
    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 50
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->x:Lcom/bytedance/sdk/openadsdk/d/o;

    if-eqz v2, :cond_0

    .line 51
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/d/o;->o()V

    :cond_0
    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 52
    iget-boolean v3, v0, Lcom/bytedance/sdk/openadsdk/d/j;->t:Z

    if-nez v3, :cond_1

    iget-boolean v3, v0, Lcom/bytedance/sdk/openadsdk/d/j;->w:Z

    if-eqz v3, :cond_1

    .line 53
    iput-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->t:Z

    const-string v3, "javascript:\nfunction sendScroll(){\n   var totalH = document.body.scrollHeight || document.documentElement.scrollHeight;\n   var clientH = window.innerHeight || document.documentElement.clientHeight;\n   var scrollH = document.body.scrollTop || document.documentElement.scrollTop;\n   var validH = scrollH + clientH;\n   var result = (validH/totalH*100).toFixed(2);\n   console.log(\'LandingPageLogscroll status: (\' + scrollH + \'+\' + clientH + \')/\' + totalH + \'=\' + result);\n   window.JS_LANDING_PAGE_LOG_OBJ.readPercent(result);\n}\nsendScroll();\nwindow.addEventListener(\'scroll\', function(e){\n    sendScroll();\n});"

    .line 54
    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/l;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 55
    :cond_1
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/d/j;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-nez v1, :cond_2

    return-void

    .line 56
    :cond_2
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-eq v1, v4, :cond_3

    .line 57
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 58
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/bytedance/sdk/openadsdk/d/j;->o:J

    .line 59
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    if-ne v1, v5, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    const-string v4, "url"

    const-string v5, "h5"

    const-string v6, "preload_status"

    const-string v7, "error_url"

    const-string v8, "error_msg"

    const-string v9, "error_code"

    const-string v10, "render_type_2"

    const-string v11, "render_type"

    if-eqz v1, :cond_9

    .line 60
    iget-wide v12, v0, Lcom/bytedance/sdk/openadsdk/d/j;->r:J

    iget-wide v14, v0, Lcom/bytedance/sdk/openadsdk/d/j;->q:J

    sub-long/2addr v12, v14

    .line 61
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 62
    :try_start_0
    iget v14, v0, Lcom/bytedance/sdk/openadsdk/d/j;->h:I

    invoke-virtual {v1, v9, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 63
    iget-object v9, v0, Lcom/bytedance/sdk/openadsdk/d/j;->j:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    iget-object v8, v0, Lcom/bytedance/sdk/openadsdk/d/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 65
    iget v7, v0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I

    if-ltz v7, :cond_5

    .line 66
    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 67
    :cond_5
    invoke-virtual {v1, v11, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v10, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->H0()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :catch_0
    :try_start_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->x()Lcom/bytedance/sdk/openadsdk/core/settings/e;

    move-result-object v4

    if-eqz p3, :cond_8

    .line 71
    iget-object v5, v4, Lcom/bytedance/sdk/openadsdk/core/settings/e;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-boolean v5, v4, Lcom/bytedance/sdk/openadsdk/core/settings/e;->b:Z

    if-eqz v5, :cond_8

    .line 72
    iget-object v5, v0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    if-eqz v5, :cond_7

    .line 73
    invoke-virtual {v5}, Landroid/webkit/WebView;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 74
    invoke-virtual {v5}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result v5

    if-nez v5, :cond_6

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    .line 75
    :cond_7
    :goto_1
    iget-object v3, v4, Lcom/bytedance/sdk/openadsdk/core/settings/e;->a:Ljava/lang/String;

    .line 76
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/n/c;->b()Lcom/bytedance/sdk/openadsdk/n/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/n/c;->d()Lb/b/a/a/j/a;

    move-result-object v4

    invoke-virtual {v4}, Lb/b/a/a/j/a;->Oo08()Lb/b/a/a/j/d/b;

    move-result-object v4

    .line 77
    invoke-virtual {v4, v3}, Lb/b/a/a/j/d/c;->o〇0(Ljava/lang/String;)V

    .line 78
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v5, "content-type"

    const-string v6, "application/json; charset=utf-8"

    .line 79
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-virtual {v4, v3}, Lb/b/a/a/j/d/c;->Oo08(Ljava/util/Map;)V

    .line 81
    new-instance v3, Lcom/bytedance/sdk/openadsdk/d/j$a;

    invoke-direct {v3, v0, v2}, Lcom/bytedance/sdk/openadsdk/d/j$a;-><init>(Lcom/bytedance/sdk/openadsdk/d/j;I)V

    invoke-virtual {v4, v3}, Lb/b/a/a/j/d/b;->〇〇888(Lb/b/a/a/j/c/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    nop

    :cond_8
    :goto_2
    const-wide/32 v3, 0x927c0

    .line 82
    invoke-static {v12, v13, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    const-string v5, "load_finish"

    .line 83
    invoke-direct {v0, v5, v1, v3, v4}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    move-object/from16 v1, p2

    .line 84
    invoke-direct {v0, v1, v5, v3, v4}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 85
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/d/j;->z:Lcom/bytedance/sdk/openadsdk/d/i;

    if-eqz v1, :cond_b

    .line 86
    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/d/i;->a(I)V

    goto :goto_4

    .line 87
    :cond_9
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 88
    :try_start_2
    iget v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->h:I

    invoke-virtual {v1, v9, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 89
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->j:Ljava/lang/String;

    invoke-virtual {v1, v8, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 90
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->k:Ljava/lang/String;

    invoke-virtual {v1, v7, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 91
    iget v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I

    if-ltz v2, :cond_a

    .line 92
    invoke-virtual {v1, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 93
    :cond_a
    invoke-virtual {v1, v11, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->H0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    nop

    :goto_3
    const-string v2, "load_fail"

    .line 96
    invoke-direct {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 97
    iget-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/d/j;->i:Z

    if-eqz v2, :cond_b

    .line 98
    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 99
    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "load_fail_main"

    .line 100
    invoke-direct {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_b
    :goto_4
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/widget/SSWebView;)V
    .locals 8

    .line 111
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_endcard"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_split_screen"

    .line 112
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    const-string v1, "landingpage_direct"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 113
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->i()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 114
    :cond_1
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v1, v0, :cond_2

    return-void

    :cond_2
    if-eqz p1, :cond_4

    .line 115
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 116
    :cond_3
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Lcom/bytedance/sdk/component/widget/SSWebView;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 117
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v1, :cond_4

    .line 118
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getUrl()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->b:J

    const-string v3, "landing_page_blank"

    .line 120
    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;J)V

    :cond_4
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/d/o;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->x:Lcom/bytedance/sdk/openadsdk/d/o;

    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    return-void
.end method

.method public c()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->y:Z

    return v0
.end method

.method public d()V
    .locals 6

    .line 1
    const-string v0, "LandingPageLog"

    .line 2
    .line 3
    const-string v1, "onDestroy"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const-string v1, "JS_LANDING_PAGE_LOG_OBJ"

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->v:Landroid/webkit/WebView;

    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    const/4 v2, 0x0

    .line 24
    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->m:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 31
    .line 32
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 35
    .line 36
    .line 37
    move-result-wide v2

    .line 38
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->s:J

    .line 39
    .line 40
    sub-long/2addr v2, v4

    .line 41
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->F:I

    .line 42
    .line 43
    invoke-static {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;JI)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 48
    .line 49
    const/4 v1, 0x2

    .line 50
    if-ne v0, v1, :cond_2

    .line 51
    .line 52
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-nez v0, :cond_2

    .line 59
    .line 60
    new-instance v0, Lorg/json/JSONObject;

    .line 61
    .line 62
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v1, "load_status"

    .line 66
    .line 67
    :try_start_0
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 68
    .line 69
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .line 71
    .line 72
    const-string v1, "max_scroll_percent"

    .line 73
    .line 74
    :try_start_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 75
    .line 76
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 81
    .line 82
    .line 83
    const-string v1, "jump_times"

    .line 84
    .line 85
    :try_start_2
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 86
    .line 87
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    .line 93
    .line 94
    const-string v1, "click_times"

    .line 95
    .line 96
    :try_start_3
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->C:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 97
    .line 98
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    .line 99
    .line 100
    .line 101
    move-result v3

    .line 102
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 103
    .line 104
    .line 105
    const-string v1, "render_type"

    .line 106
    .line 107
    const-string v3, "h5"

    .line 108
    .line 109
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 110
    .line 111
    .line 112
    const-string v1, "render_type_2"

    .line 113
    .line 114
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 115
    .line 116
    .line 117
    move-result-object v2

    .line 118
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 119
    .line 120
    .line 121
    :catch_0
    const-string v1, "stay_page"

    .line 122
    .line 123
    const-wide/16 v2, 0x0

    .line 124
    .line 125
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    .line 126
    .line 127
    .line 128
    :cond_2
    :goto_0
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public e()V
    .locals 5

    .line 1
    const-string v0, "LandingPageLog"

    .line 2
    .line 3
    const-string v1, "onResume"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->s:J

    .line 9
    .line 10
    const-wide/16 v2, 0x0

    .line 11
    .line 12
    cmp-long v4, v0, v2

    .line 13
    .line 14
    if-nez v4, :cond_0

    .line 15
    .line 16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->s:J

    .line 21
    .line 22
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 23
    .line 24
    .line 25
    move-result-wide v0

    .line 26
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->o:J

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public f()V
    .locals 8

    .line 1
    const-string v0, "LandingPageLog"

    .line 2
    .line 3
    const-string v1, "onStop"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 9
    .line 10
    const-string v1, "landingpage"

    .line 11
    .line 12
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "landingpage_endcard"

    .line 21
    .line 22
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 29
    .line 30
    const-string v1, "landingpage_split_screen"

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    .line 38
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->n:Ljava/lang/String;

    .line 39
    .line 40
    const-string v1, "landingpage_direct"

    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    .line 48
    return-void

    .line 49
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 50
    .line 51
    const/4 v1, 0x2

    .line 52
    const/4 v2, 0x1

    .line 53
    const/4 v3, 0x0

    .line 54
    if-ne v0, v1, :cond_1

    .line 55
    .line 56
    const/4 v0, 0x1

    .line 57
    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    .line 59
    :goto_0
    if-eqz v0, :cond_3

    .line 60
    .line 61
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/d/j;->p:J

    .line 62
    .line 63
    const-wide/16 v4, 0x0

    .line 64
    .line 65
    cmp-long v6, v0, v4

    .line 66
    .line 67
    if-gtz v6, :cond_2

    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/d/j;->c()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-eqz v0, :cond_2

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 77
    .line 78
    .line 79
    move-result-wide v0

    .line 80
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/d/j;->o:J

    .line 81
    .line 82
    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->p:J

    .line 83
    .line 84
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    .line 85
    .line 86
    .line 87
    move-result-wide v4

    .line 88
    sub-long/2addr v0, v4

    .line 89
    new-instance v4, Lorg/json/JSONObject;

    .line 90
    .line 91
    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string v5, "load_status"

    .line 95
    .line 96
    :try_start_0
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->c:I

    .line 97
    .line 98
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .line 100
    .line 101
    const-string v5, "max_scroll_percent"

    .line 102
    .line 103
    :try_start_1
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 104
    .line 105
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 106
    .line 107
    .line 108
    move-result v6

    .line 109
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 110
    .line 111
    .line 112
    const-string v5, "jump_times"

    .line 113
    .line 114
    :try_start_2
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 115
    .line 116
    invoke-virtual {v6, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    .line 117
    .line 118
    .line 119
    move-result v6

    .line 120
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 121
    .line 122
    .line 123
    const-string v5, "click_times"

    .line 124
    .line 125
    :try_start_3
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/d/j;->C:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 126
    .line 127
    invoke-virtual {v6, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    .line 128
    .line 129
    .line 130
    move-result v6

    .line 131
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 132
    .line 133
    .line 134
    const-string v5, "render_type"

    .line 135
    .line 136
    const-string v6, "h5"

    .line 137
    .line 138
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 139
    .line 140
    .line 141
    const-string v5, "render_type_2"

    .line 142
    .line 143
    :try_start_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 144
    .line 145
    .line 146
    move-result-object v3

    .line 147
    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 148
    .line 149
    .line 150
    :catch_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/d/j;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 151
    .line 152
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 153
    .line 154
    .line 155
    const-wide/32 v2, 0x927c0

    .line 156
    .line 157
    .line 158
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    .line 159
    .line 160
    .line 161
    move-result-wide v0

    .line 162
    const-string v2, "stay_page"

    .line 163
    .line 164
    invoke-direct {p0, v2, v4, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    .line 165
    .line 166
    .line 167
    :cond_3
    :goto_1
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.class public Lcom/bytedance/sdk/openadsdk/d/c$o;
.super Ljava/lang/Object;
.source "AdEventManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/d/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "o"
.end annotation


# direct methods
.method public static a(IIIILcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;I)V
    .locals 10

    .line 4
    new-instance v9, Lcom/bytedance/sdk/openadsdk/d/c$o$d;

    const-string v1, "resource_detail"

    move-object v0, v9

    move-object v2, p5

    move-object v3, p4

    move v4, p0

    move/from16 v5, p6

    move v6, p1

    move v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/d/c$o$d;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;IIIII)V

    invoke-static {v9}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static a(IILcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    .line 2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/c$o$b;

    const-string v1, "res_hit"

    invoke-direct {v0, v1, p2, p1, p0}, Lcom/bytedance/sdk/openadsdk/d/c$o$b;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;II)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static a(JLcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;Ljava/lang/String;)V
    .locals 9

    .line 3
    new-instance v8, Lcom/bytedance/sdk/openadsdk/d/c$o$c;

    const-string v1, "page_init"

    move-object v0, v8

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p2

    move-wide v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/d/c$o$c;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;J)V

    invoke-static {v8}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    .line 5
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/c$o$e;

    const-string v1, "download_gecko_start"

    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/d/c$o$e;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lorg/json/JSONObject;)V
    .locals 2

    .line 6
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/c$o$f;

    const-string v1, "download_gecko_end"

    invoke-direct {v0, v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/d/c$o$f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lorg/json/JSONObject;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/c$o$a;

    const-string v1, "res_hit"

    invoke-direct {v0, v1, p2, p1, p0}, Lcom/bytedance/sdk/openadsdk/d/c$o$a;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lorg/json/JSONObject;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

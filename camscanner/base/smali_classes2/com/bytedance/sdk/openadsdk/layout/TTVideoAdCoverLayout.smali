.class public Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;
.super Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;
.source "TTVideoAdCoverLayout.java"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 12

    .line 1
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->D0:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/view/View;->setId(I)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0x8

    .line 7
    .line 8
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    const-string v1, "#7f000000"

    .line 12
    .line 13
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 18
    .line 19
    .line 20
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;-><init>(Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 30
    .line 31
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->F0:I

    .line 32
    .line 33
    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 34
    .line 35
    .line 36
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 37
    .line 38
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 39
    .line 40
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 41
    .line 42
    .line 43
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 44
    .line 45
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 48
    .line 49
    .line 50
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 51
    .line 52
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 61
    .line 62
    .line 63
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 64
    .line 65
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 70
    .line 71
    .line 72
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 73
    .line 74
    const/4 v2, -0x1

    .line 75
    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 76
    .line 77
    .line 78
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 79
    .line 80
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    .line 82
    .line 83
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->a:Landroid/widget/ImageView;

    .line 84
    .line 85
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 86
    .line 87
    .line 88
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;

    .line 89
    .line 90
    invoke-direct {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 91
    .line 92
    .line 93
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->E0:I

    .line 94
    .line 95
    invoke-virtual {v1, v3}, Landroid/view/View;->setId(I)V

    .line 96
    .line 97
    .line 98
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 99
    .line 100
    const/4 v4, -0x2

    .line 101
    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 102
    .line 103
    .line 104
    const/16 v5, 0xd

    .line 105
    .line 106
    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 113
    .line 114
    .line 115
    const/high16 v3, 0x42300000    # 44.0f

    .line 116
    .line 117
    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

    .line 122
    .line 123
    invoke-direct {v5, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;-><init>(Landroid/content/Context;)V

    .line 124
    .line 125
    .line 126
    iput-object v5, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->b:Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

    .line 127
    .line 128
    sget v6, Lcom/bytedance/sdk/openadsdk/utils/h;->K:I

    .line 129
    .line 130
    invoke-virtual {v5, v6}, Landroid/view/View;->setId(I)V

    .line 131
    .line 132
    .line 133
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 134
    .line 135
    invoke-direct {v5, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 136
    .line 137
    .line 138
    const/16 v7, 0xe

    .line 139
    .line 140
    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 141
    .line 142
    .line 143
    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->b:Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

    .line 144
    .line 145
    invoke-virtual {v8, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    .line 147
    .line 148
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->b:Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

    .line 149
    .line 150
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    .line 151
    .line 152
    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 153
    .line 154
    .line 155
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->b:Lcom/bytedance/sdk/openadsdk/core/widget/CornerIV;

    .line 156
    .line 157
    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 158
    .line 159
    .line 160
    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 161
    .line 162
    invoke-direct {v5, p1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 163
    .line 164
    .line 165
    iput-object v5, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 166
    .line 167
    sget v8, Lcom/bytedance/sdk/openadsdk/utils/h;->L:I

    .line 168
    .line 169
    invoke-virtual {v5, v8}, Landroid/view/View;->setId(I)V

    .line 170
    .line 171
    .line 172
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 173
    .line 174
    invoke-direct {v5, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v5, v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 178
    .line 179
    .line 180
    const/16 v3, 0x13

    .line 181
    .line 182
    invoke-virtual {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 183
    .line 184
    .line 185
    const/4 v3, 0x5

    .line 186
    invoke-virtual {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 187
    .line 188
    .line 189
    const/4 v3, 0x7

    .line 190
    invoke-virtual {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 191
    .line 192
    .line 193
    const/16 v3, 0x12

    .line 194
    .line 195
    invoke-virtual {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 196
    .line 197
    .line 198
    const/4 v3, 0x6

    .line 199
    invoke-virtual {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 203
    .line 204
    .line 205
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 206
    .line 207
    invoke-virtual {v3, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    .line 209
    .line 210
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 211
    .line 212
    const-string v5, "tt_circle_solid_mian"

    .line 213
    .line 214
    invoke-static {p1, v5}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 215
    .line 216
    .line 217
    move-result v5

    .line 218
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 219
    .line 220
    .line 221
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 222
    .line 223
    const/16 v5, 0x11

    .line 224
    .line 225
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 226
    .line 227
    .line 228
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 229
    .line 230
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 231
    .line 232
    .line 233
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 234
    .line 235
    const/high16 v8, 0x41980000    # 19.0f

    .line 236
    .line 237
    const/4 v9, 0x2

    .line 238
    invoke-virtual {v3, v9, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 239
    .line 240
    .line 241
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 242
    .line 243
    const/4 v8, 0x1

    .line 244
    invoke-static {v8}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    .line 245
    .line 246
    .line 247
    move-result-object v10

    .line 248
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 249
    .line 250
    .line 251
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 252
    .line 253
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 254
    .line 255
    .line 256
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->c:Landroid/widget/TextView;

    .line 257
    .line 258
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 259
    .line 260
    .line 261
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 262
    .line 263
    invoke-direct {v3, p1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 264
    .line 265
    .line 266
    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 267
    .line 268
    sget v10, Lcom/bytedance/sdk/openadsdk/utils/h;->M:I

    .line 269
    .line 270
    invoke-virtual {v3, v10}, Landroid/view/View;->setId(I)V

    .line 271
    .line 272
    .line 273
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 274
    .line 275
    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 276
    .line 277
    .line 278
    const/4 v4, 0x3

    .line 279
    invoke-virtual {v3, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 280
    .line 281
    .line 282
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 283
    .line 284
    .line 285
    const/high16 v6, 0x40c00000    # 6.0f

    .line 286
    .line 287
    invoke-static {p1, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 288
    .line 289
    .line 290
    move-result v6

    .line 291
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 292
    .line 293
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 294
    .line 295
    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    .line 297
    .line 298
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 299
    .line 300
    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 301
    .line 302
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 303
    .line 304
    .line 305
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 306
    .line 307
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 308
    .line 309
    .line 310
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 311
    .line 312
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 313
    .line 314
    .line 315
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 316
    .line 317
    const/high16 v6, 0x41400000    # 12.0f

    .line 318
    .line 319
    invoke-virtual {v3, v9, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 320
    .line 321
    .line 322
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->d:Landroid/widget/TextView;

    .line 323
    .line 324
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 325
    .line 326
    .line 327
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 328
    .line 329
    invoke-direct {v3, p1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 330
    .line 331
    .line 332
    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 333
    .line 334
    sget v6, Lcom/bytedance/sdk/openadsdk/utils/h;->N:I

    .line 335
    .line 336
    invoke-virtual {v3, v6}, Landroid/view/View;->setId(I)V

    .line 337
    .line 338
    .line 339
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 340
    .line 341
    const/high16 v6, 0x42c80000    # 100.0f

    .line 342
    .line 343
    invoke-static {p1, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 344
    .line 345
    .line 346
    move-result v6

    .line 347
    const/high16 v11, 0x41e00000    # 28.0f

    .line 348
    .line 349
    invoke-static {p1, v11}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 350
    .line 351
    .line 352
    move-result v11

    .line 353
    invoke-direct {v3, v6, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 354
    .line 355
    .line 356
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 357
    .line 358
    .line 359
    invoke-virtual {v3, v4, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 360
    .line 361
    .line 362
    const/high16 v4, 0x41a00000    # 20.0f

    .line 363
    .line 364
    invoke-static {p1, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 365
    .line 366
    .line 367
    move-result v4

    .line 368
    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 369
    .line 370
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 371
    .line 372
    invoke-virtual {v4, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 373
    .line 374
    .line 375
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 376
    .line 377
    const/high16 v4, 0x42900000    # 72.0f

    .line 378
    .line 379
    invoke-static {p1, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 380
    .line 381
    .line 382
    move-result v4

    .line 383
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 384
    .line 385
    .line 386
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 387
    .line 388
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 389
    .line 390
    .line 391
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 392
    .line 393
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 394
    .line 395
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 396
    .line 397
    .line 398
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 399
    .line 400
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 401
    .line 402
    .line 403
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 404
    .line 405
    const/high16 v3, 0x41600000    # 14.0f

    .line 406
    .line 407
    invoke-virtual {v2, v9, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 408
    .line 409
    .line 410
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 411
    .line 412
    const-string v3, "tt_ad_cover_btn_begin_bg"

    .line 413
    .line 414
    invoke-static {p1, v3}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 415
    .line 416
    .line 417
    move-result v3

    .line 418
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 419
    .line 420
    .line 421
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 422
    .line 423
    invoke-virtual {v2, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;->setGravity(I)V

    .line 424
    .line 425
    .line 426
    const/high16 v2, 0x41200000    # 10.0f

    .line 427
    .line 428
    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 429
    .line 430
    .line 431
    move-result v2

    .line 432
    const/high16 v3, 0x40000000    # 2.0f

    .line 433
    .line 434
    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 435
    .line 436
    .line 437
    move-result p1

    .line 438
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 439
    .line 440
    invoke-virtual {v3, v2, p1, v2, p1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;->setPadding(IIII)V

    .line 441
    .line 442
    .line 443
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 444
    .line 445
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 446
    .line 447
    .line 448
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/layout/TTVideoAdCoverLayout;->e:Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 449
    .line 450
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 451
    .line 452
    .line 453
    return-void
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

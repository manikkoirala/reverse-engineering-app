.class public Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;
.super Landroid/view/GestureDetector;
.source "ViewGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/c0/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 3
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;

    .line 4
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/c0/f;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/core/c0/f;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    const/4 p1, 0x0

    .line 5
    invoke-virtual {p0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/View;Landroid/view/View;)Lcom/bytedance/sdk/openadsdk/core/f0/i;
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    if-nez v0, :cond_0

    .line 3
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;-><init>()V

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a()Lcom/bytedance/sdk/openadsdk/core/f0/i;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;-><init>()V

    iget v0, v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->a:F

    .line 5
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->b:F

    .line 6
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 7
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->e(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 8
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->f(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget-wide v1, v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->e:J

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(J)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget-wide v1, v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->f:J

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(J)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    .line 11
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    .line 12
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object v0

    .line 13
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Landroid/view/View;)[I

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 14
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Landroid/view/View;)[I

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget p3, p3, Lcom/bytedance/sdk/openadsdk/core/c0/f;->g:I

    .line 15
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->f(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget p3, p3, Lcom/bytedance/sdk/openadsdk/core/c0/f;->h:I

    .line 16
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget p3, p3, Lcom/bytedance/sdk/openadsdk/core/c0/f;->i:I

    .line 17
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->e(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    iget-object p3, p3, Lcom/bytedance/sdk/openadsdk/core/c0/f;->j:Landroid/util/SparseArray;

    .line 18
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(Landroid/util/SparseArray;)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/h;->s()Z

    move-result p3

    if-eqz p3, :cond_1

    const/4 p3, 0x1

    goto :goto_0

    :cond_1
    const/4 p3, 0x2

    :goto_0
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    const-string p3, "vessel"

    .line 20
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 21
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;)F

    move-result p3

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 22
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;)I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 23
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->f(Landroid/content/Context;)F

    move-result p1

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a()Lcom/bytedance/sdk/openadsdk/core/f0/i;

    move-result-object p1

    return-object p1
.end method

.method public a()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;->a()Z

    move-result v0

    return v0
.end method

.method b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o$a;->b()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/o;->b:Lcom/bytedance/sdk/openadsdk/core/c0/f;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/c0/f;->a(Landroid/view/MotionEvent;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.class public interface abstract Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.super Ljava/lang/Object;
.source "ISettingsDataRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/settings/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "c"
.end annotation


# virtual methods
.method public abstract commit()V
.end method

.method public abstract putBoolean(Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

.method public abstract putFloat(Ljava/lang/String;F)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

.method public abstract putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

.method public abstract putLong(Ljava/lang/String;J)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

.method public abstract putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

.method public abstract remove(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
.end method

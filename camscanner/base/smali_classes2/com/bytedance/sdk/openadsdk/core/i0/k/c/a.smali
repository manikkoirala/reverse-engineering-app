.class public Lcom/bytedance/sdk/openadsdk/core/i0/k/c/a;
.super Ljava/lang/Object;
.source "CompanionAdsParser.java"


# direct methods
.method public static a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Lcom/bytedance/sdk/openadsdk/core/i0/c;
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 1
    invoke-static/range {p1 .. p1}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    return-object v2

    .line 2
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 3
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 4
    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 5
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v3

    float-to-int v4, v4

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v3, v5

    const/4 v5, 0x1

    move-object v12, v2

    .line 6
    :goto_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    const/4 v13, 0x3

    if-ne v6, v13, :cond_2

    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "CompanionAds"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_1

    :cond_1
    return-object v12

    .line 7
    :cond_2
    :goto_1
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 8
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    const/4 v14, 0x2

    if-eq v6, v14, :cond_3

    goto/16 :goto_15

    .line 9
    :cond_3
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v15, "Companion"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 10
    sget-object v6, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->g:Ljava/lang/String;

    const-string v7, "width"

    invoke-interface {v1, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->d(Ljava/lang/String;)I

    move-result v11

    const-string v7, "height"

    .line 11
    invoke-interface {v1, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->d(Ljava/lang/String;)I

    move-result v10

    const/16 v6, 0x12c

    if-lt v11, v6, :cond_1b

    const/16 v6, 0xfa

    if-ge v10, v6, :cond_4

    goto/16 :goto_14

    .line 12
    :cond_4
    new-instance v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;

    invoke-direct {v9}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;-><init>()V

    .line 13
    :goto_2
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    if-ne v6, v13, :cond_6

    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    goto :goto_3

    .line 14
    :cond_5
    iget-object v6, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1c

    iget v6, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    cmpl-float v6, v6, v5

    if-ltz v6, :cond_1c

    .line 15
    new-instance v12, Lcom/bytedance/sdk/openadsdk/core/i0/c;

    iget-object v5, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->b:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    iget-object v6, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->c:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;

    iget-object v7, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a:Ljava/lang/String;

    iget-object v8, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->e:Ljava/util/List;

    iget-object v13, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->f:Ljava/util/List;

    iget-object v14, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->d:Ljava/lang/String;

    move-object/from16 v16, v12

    move/from16 v17, v11

    move/from16 v18, v10

    move-object/from16 v19, v5

    move-object/from16 v20, v6

    move-object/from16 v21, v7

    move-object/from16 v22, v8

    move-object/from16 v23, v13

    move-object/from16 v24, v14

    invoke-direct/range {v16 .. v24}, Lcom/bytedance/sdk/openadsdk/core/i0/c;-><init>(IILcom/bytedance/sdk/openadsdk/core/i0/m/a$c;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 16
    iget v5, v9, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    goto/16 :goto_0

    .line 17
    :cond_6
    :goto_3
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 18
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    if-eq v6, v14, :cond_7

    goto :goto_2

    .line 19
    :cond_7
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    const-string v8, "HTMLResource"

    const-string v2, "CompanionClickTracking"

    const-string v14, "StaticResource"

    const-string v13, "TrackingEvents"

    move-object/from16 v19, v9

    const-string v9, "CompanionClickThrough"

    move-object/from16 v20, v12

    const-string v12, "IFrameResource"

    const/16 v21, -0x1

    sparse-switch v7, :sswitch_data_0

    goto :goto_4

    :sswitch_0
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    goto :goto_4

    :cond_8
    const/4 v6, 0x5

    const/16 v21, 0x5

    goto :goto_4

    :sswitch_1
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_4

    :cond_9
    const/4 v6, 0x4

    const/16 v21, 0x4

    goto :goto_4

    :sswitch_2
    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    goto :goto_4

    :cond_a
    const/16 v21, 0x3

    goto :goto_4

    :sswitch_3
    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_b

    goto :goto_4

    :cond_b
    const/16 v21, 0x2

    goto :goto_4

    :sswitch_4
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    goto :goto_4

    :cond_c
    const/4 v6, 0x1

    const/16 v21, 0x1

    goto :goto_4

    :sswitch_5
    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    goto :goto_4

    :cond_d
    const/4 v6, 0x0

    const/16 v21, 0x0

    :goto_4
    packed-switch v21, :pswitch_data_0

    .line 20
    invoke-static/range {p1 .. p1}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    move-object/from16 v9, v19

    move-object/from16 v12, v20

    goto/16 :goto_8

    .line 21
    :pswitch_0
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;->a:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;

    invoke-static {v0, v11, v10, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/m/a;->a(Landroid/content/Context;IILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)Landroid/graphics/Point;

    move-result-object v6

    .line 22
    iget v9, v6, Landroid/graphics/Point;->x:I

    iget v12, v6, Landroid/graphics/Point;->y:I

    sget-object v13, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;->a:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    move v6, v4

    move v7, v3

    move-object v14, v8

    move v8, v9

    move/from16 v21, v3

    move-object/from16 v3, v19

    move v9, v12

    move v12, v10

    move-object v10, v2

    move/from16 v19, v4

    move v4, v11

    move-object v11, v13

    invoke-static/range {v6 .. v11}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->a(IIIILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;)F

    move-result v6

    .line 23
    invoke-static {v1, v14}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 24
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_f

    iget v8, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    cmpg-float v8, v6, v8

    if-lez v8, :cond_f

    cmpg-float v8, v6, v5

    if-gtz v8, :cond_e

    goto :goto_5

    .line 25
    :cond_e
    iput v6, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    .line 26
    invoke-virtual {v3, v7, v13, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)V

    goto :goto_6

    :cond_f
    :goto_5
    const/4 v2, 0x3

    .line 27
    invoke-static {v1, v15, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)V

    :goto_6
    move-object v9, v3

    move v11, v4

    move v10, v12

    goto :goto_7

    :pswitch_1
    move/from16 v21, v3

    move v12, v10

    move-object/from16 v3, v19

    move/from16 v19, v4

    move v4, v11

    .line 28
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a(Ljava/lang/String;)V

    move-object v9, v3

    :goto_7
    move/from16 v4, v19

    move-object/from16 v12, v20

    move/from16 v3, v21

    :goto_8
    const/4 v2, 0x0

    const/4 v13, 0x3

    const/4 v14, 0x2

    goto/16 :goto_2

    :pswitch_2
    move/from16 v21, v3

    move v12, v10

    move-object/from16 v3, v19

    move/from16 v19, v4

    move v4, v11

    .line 29
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;->a:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    .line 30
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->g:Ljava/lang/String;

    const-string v6, "creativeType"

    invoke-interface {v1, v2, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 31
    sget-object v13, Lcom/bytedance/sdk/openadsdk/core/i0/m/a;->a:Ljava/util/Set;

    invoke-interface {v13, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 32
    sget-object v6, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;->b:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    goto :goto_9

    .line 33
    :cond_10
    sget-object v6, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;->c:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    :goto_9
    move-object v10, v6

    .line 34
    sget-object v11, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;->b:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;

    invoke-static {v0, v4, v12, v11}, Lcom/bytedance/sdk/openadsdk/core/i0/m/a;->a(Landroid/content/Context;IILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)Landroid/graphics/Point;

    move-result-object v6

    .line 35
    iget v8, v6, Landroid/graphics/Point;->x:I

    iget v9, v6, Landroid/graphics/Point;->y:I

    move/from16 v6, v19

    move/from16 v7, v21

    move-object/from16 v22, v10

    move-object v10, v11

    move/from16 v23, v12

    move-object v12, v11

    move-object/from16 v11, v22

    invoke-static/range {v6 .. v11}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->a(IIIILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;)F

    move-result v6

    .line 36
    invoke-interface {v13, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_12

    sget-object v7, Lcom/bytedance/sdk/openadsdk/core/i0/m/a;->b:Ljava/util/Set;

    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    goto :goto_a

    :cond_11
    const/4 v2, 0x0

    goto :goto_b

    .line 37
    :cond_12
    :goto_a
    invoke-static {v1, v14}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    :goto_b
    iget v7, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    cmpg-float v7, v6, v7

    if-ltz v7, :cond_14

    cmpg-float v7, v6, v5

    if-lez v7, :cond_14

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_13

    goto :goto_c

    .line 39
    :cond_13
    iput v6, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    move-object/from16 v6, v22

    .line 40
    invoke-virtual {v3, v2, v6, v12}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)V

    goto :goto_e

    :cond_14
    :goto_c
    const/4 v2, 0x3

    .line 41
    invoke-static {v1, v15, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)V

    goto :goto_e

    :pswitch_3
    move/from16 v21, v3

    move/from16 v23, v10

    move-object/from16 v3, v19

    const/4 v2, 0x3

    move/from16 v19, v4

    move v4, v11

    .line 42
    :goto_d
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    if-ne v6, v2, :cond_16

    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    goto :goto_f

    :cond_15
    :goto_e
    move-object v9, v3

    move v11, v4

    move/from16 v4, v19

    move-object/from16 v12, v20

    move/from16 v3, v21

    move/from16 v10, v23

    goto/16 :goto_8

    .line 43
    :cond_16
    :goto_f
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    const/4 v14, 0x2

    if-eq v2, v14, :cond_17

    goto :goto_10

    .line 44
    :cond_17
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v6, "Tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 45
    invoke-static {v1, v6}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->b(Ljava/lang/String;)V

    :cond_18
    :goto_10
    const/4 v2, 0x3

    goto :goto_d

    :pswitch_4
    move/from16 v21, v3

    move/from16 v23, v10

    move-object/from16 v3, v19

    const/4 v14, 0x2

    move/from16 v19, v4

    move v4, v11

    .line 46
    invoke-static {v1, v9}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->d:Ljava/lang/String;

    move-object v9, v3

    :goto_11
    move/from16 v4, v19

    move-object/from16 v12, v20

    move/from16 v3, v21

    const/4 v2, 0x0

    const/4 v13, 0x3

    goto/16 :goto_2

    :pswitch_5
    move/from16 v21, v3

    move v2, v10

    move-object/from16 v3, v19

    const/4 v14, 0x2

    move/from16 v19, v4

    move v4, v11

    .line 47
    sget-object v6, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;->a:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;

    invoke-static {v0, v4, v2, v6}, Lcom/bytedance/sdk/openadsdk/core/i0/m/a;->a(Landroid/content/Context;IILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)Landroid/graphics/Point;

    move-result-object v6

    .line 48
    iget v8, v6, Landroid/graphics/Point;->x:I

    iget v9, v6, Landroid/graphics/Point;->y:I

    sget-object v13, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;->c:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;

    sget-object v11, Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;->a:Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;

    move/from16 v6, v19

    move/from16 v7, v21

    move-object v10, v13

    move-object/from16 v17, v11

    invoke-static/range {v6 .. v11}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->a(IIIILcom/bytedance/sdk/openadsdk/core/i0/m/a$d;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;)F

    move-result v6

    .line 49
    invoke-static {v1, v12}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 50
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1a

    iget v8, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    cmpg-float v8, v6, v8

    if-lez v8, :cond_1a

    cmpg-float v8, v6, v5

    if-gtz v8, :cond_19

    goto :goto_12

    .line 51
    :cond_19
    iput v6, v3, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->g:F

    move-object/from16 v6, v17

    .line 52
    invoke-virtual {v3, v7, v6, v13}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e$a;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$c;Lcom/bytedance/sdk/openadsdk/core/i0/m/a$d;)V

    goto :goto_13

    :cond_1a
    :goto_12
    const/4 v6, 0x3

    .line 53
    invoke-static {v1, v15, v6}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)V

    :goto_13
    move v10, v2

    move-object v9, v3

    move v11, v4

    goto :goto_11

    :cond_1b
    :goto_14
    move/from16 v21, v3

    move/from16 v19, v4

    move-object/from16 v20, v12

    .line 54
    invoke-static/range {p1 .. p1}, Lcom/bytedance/sdk/openadsdk/core/i0/k/c/e;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_16

    :cond_1c
    :goto_15
    move/from16 v21, v3

    move/from16 v19, v4

    move-object/from16 v20, v12

    :goto_16
    move/from16 v4, v19

    move-object/from16 v12, v20

    move/from16 v3, v21

    const/4 v2, 0x0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x165f3d2e -> :sswitch_5
        -0x14c116d7 -> :sswitch_4
        0x247392d0 -> :sswitch_3
        0x285474bc -> :sswitch_2
        0x6fec8cd3 -> :sswitch_1
        0x72ef4cd9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

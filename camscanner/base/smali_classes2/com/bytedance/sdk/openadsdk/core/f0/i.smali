.class public Lcom/bytedance/sdk/openadsdk/core/f0/i;
.super Ljava/lang/Object;
.source "ClickEventModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/f0/i$b;
    }
.end annotation


# instance fields
.field private final a:[I

.field private final b:[I

.field private final c:[I

.field private final d:[I

.field private final e:F

.field private final f:F

.field private final g:F

.field private final h:F

.field private final i:J

.field private final j:J

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private final p:I

.field private final q:Lorg/json/JSONObject;

.field private final r:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/f0/i$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->a:[I

    .line 4
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->b:[I

    .line 5
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->k(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->d:[I

    .line 6
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->l(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->c:[I

    .line 7
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->m(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->e:F

    .line 8
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->n(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->f:F

    .line 9
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->o(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->g:F

    .line 10
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->p(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->h:F

    .line 11
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->q(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->i:J

    .line 12
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->r(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->j:J

    .line 13
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->k:I

    .line 14
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->l:I

    .line 15
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->e(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->m:I

    .line 16
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->f(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->n:I

    .line 17
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->g(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->o:Landroid/util/SparseArray;

    .line 18
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->h(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->r:Ljava/lang/String;

    .line 19
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->i(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->p:I

    .line 20
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->j(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)Lorg/json/JSONObject;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->q:Lorg/json/JSONObject;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;Lcom/bytedance/sdk/openadsdk/core/f0/i$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/i$b;)V

    return-void
.end method

.method public static a(Landroid/util/SparseArray;I)Lorg/json/JSONObject;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;I)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 29
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 30
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    if-eqz p0, :cond_1

    const/4 v2, 0x0

    .line 31
    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 32
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;

    if-eqz v3, :cond_0

    .line 33
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v5, "force"

    .line 34
    :try_start_1
    iget-wide v6, v3, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;->c:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v6, "mr"

    :try_start_2
    iget-wide v7, v3, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;->b:D

    .line 35
    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v5
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    const-string v6, "phase"

    :try_start_3
    iget v7, v3, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;->a:I

    .line 36
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v5
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    const-string v6, "ts"

    :try_start_4
    iget-wide v7, v3, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;->d:J

    .line 37
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    const-string v3, "ftc"

    .line 39
    :try_start_5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "info"

    .line 40
    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public a()Lorg/json/JSONObject;
    .locals 7

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->a:[I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_0

    array-length v5, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v4, :cond_0

    const-string v5, "ad_x"

    .line 3
    :try_start_1
    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v5, "ad_y"

    :try_start_2
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->a:[I

    aget v6, v6, v2

    .line 4
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->b:[I

    if-eqz v1, :cond_1

    array-length v5, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-ne v5, v4, :cond_1

    const-string v5, "width"

    .line 6
    :try_start_3
    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v5, "height"

    :try_start_4
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->b:[I

    aget v6, v6, v2

    .line 7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 8
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->c:[I

    if-eqz v1, :cond_2

    array-length v5, v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    if-ne v5, v4, :cond_2

    const-string v5, "button_x"

    .line 9
    :try_start_5
    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const-string v5, "button_y"

    :try_start_6
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->c:[I

    aget v6, v6, v2

    .line 10
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 11
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->d:[I

    if-eqz v1, :cond_3

    array-length v5, v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    if-ne v5, v4, :cond_3

    const-string v4, "button_width"

    .line 12
    :try_start_7
    aget v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const-string v3, "button_height"

    :try_start_8
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->d:[I

    aget v2, v4, v2

    .line 13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_3
    const-string v1, "down_x"

    .line 14
    :try_start_9
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->e:F

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    const-string v2, "down_y"

    :try_start_a
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->f:F

    .line 15
    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    const-string v2, "up_x"

    :try_start_b
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->g:F

    .line 16
    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    const-string v2, "up_y"

    :try_start_c
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->h:F

    .line 17
    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    const-string v2, "down_time"

    :try_start_d
    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->i:J

    .line 18
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    const-string v2, "up_time"

    :try_start_e
    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->j:J

    .line 19
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    const-string v2, "toolType"

    :try_start_f
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->k:I

    .line 20
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    const-string v2, "deviceId"

    :try_start_10
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->l:I

    .line 21
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    const-string v2, "source"

    :try_start_11
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->m:I

    .line 22
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    const-string v2, "ft"

    :try_start_12
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->o:Landroid/util/SparseArray;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->n:I

    .line 23
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/f0/i;->a(Landroid/util/SparseArray;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    const-string v2, "click_area_type"

    :try_start_13
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->r:Ljava/lang/String;

    .line 24
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->p:I
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0

    if-lez v1, :cond_4

    const-string v2, "areaType"

    .line 26
    :try_start_14
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 27
    :cond_4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/i;->q:Lorg/json/JSONObject;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_0

    if-eqz v1, :cond_5

    const-string v2, "rectInfo"

    .line 28
    :try_start_15
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_0

    :catch_0
    :cond_5
    return-object v0
.end method

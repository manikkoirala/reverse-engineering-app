.class Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;
.super Ljava/lang/Object;
.source "PAGBannerAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->b()Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private a(Z)Landroid/view/View;
    .locals 8

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 4
    .line 5
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 6
    .line 7
    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 11
    .line 12
    const/4 v2, -0x1

    .line 13
    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 20
    .line 21
    .line 22
    new-instance v1, Landroid/view/View;

    .line 23
    .line 24
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 25
    .line 26
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 27
    .line 28
    invoke-direct {v1, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 29
    .line 30
    .line 31
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 32
    .line 33
    invoke-direct {v3, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 34
    .line 35
    .line 36
    const v4, 0x3e99999a    # 0.3f

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 40
    .line 41
    .line 42
    const-string v4, "#F3F7F8"

    .line 43
    .line 44
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    .line 53
    .line 54
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    .line 55
    .line 56
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 57
    .line 58
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 59
    .line 60
    invoke-direct {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    .line 62
    .line 63
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 64
    .line 65
    invoke-direct {v3, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 66
    .line 67
    .line 68
    if-eqz p1, :cond_0

    .line 69
    .line 70
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 71
    .line 72
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 73
    .line 74
    const-string v4, "tt_ad_closed_background_300_250"

    .line 75
    .line 76
    invoke-static {v2, v4}, Lcom/bytedance/sdk/component/utils/t;->f(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 85
    .line 86
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 87
    .line 88
    const-string v4, "tt_ad_closed_background_320_50"

    .line 89
    .line 90
    invoke-static {v2, v4}, Lcom/bytedance/sdk/component/utils/t;->f(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 95
    .line 96
    .line 97
    :goto_0
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    .line 99
    .line 100
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;

    .line 101
    .line 102
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 103
    .line 104
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 105
    .line 106
    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;-><init>(Landroid/content/Context;)V

    .line 107
    .line 108
    .line 109
    const v3, 0x1f00002b

    .line 110
    .line 111
    .line 112
    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 113
    .line 114
    .line 115
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 116
    .line 117
    const/4 v4, -0x2

    .line 118
    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 119
    .line 120
    .line 121
    if-eqz p1, :cond_1

    .line 122
    .line 123
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 124
    .line 125
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 126
    .line 127
    const/high16 v6, 0x41800000    # 16.0f

    .line 128
    .line 129
    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 130
    .line 131
    .line 132
    move-result v5

    .line 133
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 134
    .line 135
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 136
    .line 137
    const/high16 v7, 0x429a0000    # 77.0f

    .line 138
    .line 139
    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    iput v6, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 144
    .line 145
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 146
    .line 147
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 148
    .line 149
    const/high16 v7, 0x41600000    # 14.0f

    .line 150
    .line 151
    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 152
    .line 153
    .line 154
    move-result v6

    .line 155
    iput v6, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 156
    .line 157
    iput v5, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 158
    .line 159
    iput v5, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_1
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 163
    .line 164
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 165
    .line 166
    const/high16 v6, 0x41000000    # 8.0f

    .line 167
    .line 168
    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 169
    .line 170
    .line 171
    move-result v5

    .line 172
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 173
    .line 174
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 175
    .line 176
    const/high16 v7, 0x42340000    # 45.0f

    .line 177
    .line 178
    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 179
    .line 180
    .line 181
    move-result v6

    .line 182
    iput v6, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 183
    .line 184
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 185
    .line 186
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 187
    .line 188
    const v7, 0x4102e148    # 8.18f

    .line 189
    .line 190
    .line 191
    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 192
    .line 193
    .line 194
    move-result v6

    .line 195
    iput v6, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 196
    .line 197
    iput v5, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 198
    .line 199
    iput v5, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 200
    .line 201
    :goto_1
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 202
    .line 203
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 204
    .line 205
    const-string v6, "tt_ad_closed_logo_red"

    .line 206
    .line 207
    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 208
    .line 209
    .line 210
    move-result v5

    .line 211
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 212
    .line 213
    .line 214
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    .line 216
    .line 217
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 218
    .line 219
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 220
    .line 221
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 222
    .line 223
    invoke-direct {v3, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 224
    .line 225
    .line 226
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 227
    .line 228
    invoke-direct {v5, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 229
    .line 230
    .line 231
    const/16 v4, 0x11

    .line 232
    .line 233
    iput v4, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 234
    .line 235
    const/high16 v4, 0x3f000000    # 0.5f

    .line 236
    .line 237
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 238
    .line 239
    .line 240
    const/4 v4, 0x1

    .line 241
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setLines(I)V

    .line 242
    .line 243
    .line 244
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 245
    .line 246
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c:Landroid/content/Context;

    .line 247
    .line 248
    const-string v6, "tt_ad_is_closed"

    .line 249
    .line 250
    invoke-static {v4, v6}, Lcom/bytedance/sdk/component/utils/t;->k(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v4

    .line 254
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    .line 256
    .line 257
    if-eqz p1, :cond_2

    .line 258
    .line 259
    const/high16 p1, 0x41900000    # 18.0f

    .line 260
    .line 261
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 262
    .line 263
    .line 264
    goto :goto_2

    .line 265
    :cond_2
    const/high16 p1, 0x41400000    # 12.0f

    .line 266
    .line 267
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 268
    .line 269
    .line 270
    :goto_2
    invoke-virtual {v1, v3, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    .line 272
    .line 273
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f$a;

    .line 274
    .line 275
    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f$a;-><init>(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;)V

    .line 276
    .line 277
    .line 278
    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    .line 280
    .line 281
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    .line 283
    .line 284
    return-object v0
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method


# virtual methods
.method public onItemClickClosed()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 12
    .line 13
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    int-to-double v2, v1

    .line 22
    int-to-double v4, v0

    .line 23
    const-wide v6, 0x407c200000000000L    # 450.0

    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    mul-double v4, v4, v6

    .line 29
    .line 30
    const-wide v6, 0x4082c00000000000L    # 600.0

    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    div-double/2addr v4, v6

    .line 36
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    .line 37
    .line 38
    .line 39
    move-result-wide v4

    .line 40
    const/4 v6, 0x1

    .line 41
    cmpl-double v7, v2, v4

    .line 42
    .line 43
    if-ltz v7, :cond_0

    .line 44
    .line 45
    const/4 v2, 0x1

    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/4 v2, 0x0

    .line 48
    :goto_0
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a(Z)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 53
    .line 54
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->A()V

    .line 59
    .line 60
    .line 61
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 62
    .line 63
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Z

    .line 64
    .line 65
    .line 66
    move-result v3

    .line 67
    const/4 v4, 0x0

    .line 68
    if-nez v3, :cond_1

    .line 69
    .line 70
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 71
    .line 72
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    invoke-static {v3, v5}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->a(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;Landroid/view/ViewGroup;)Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    .line 77
    .line 78
    .line 79
    move-result-object v3

    .line 80
    goto :goto_1

    .line 81
    :cond_1
    move-object v3, v4

    .line 82
    :goto_1
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 83
    .line 84
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 85
    .line 86
    .line 87
    move-result-object v5

    .line 88
    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 89
    .line 90
    .line 91
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 92
    .line 93
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    new-instance v7, Landroid/view/ViewGroup$LayoutParams;

    .line 98
    .line 99
    invoke-direct {v7, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v5, v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 106
    .line 107
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setClickCreativeListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;)V

    .line 112
    .line 113
    .line 114
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 115
    .line 116
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->setClickListener(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;)V

    .line 121
    .line 122
    .line 123
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->o()I

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    if-ne v0, v6, :cond_2

    .line 132
    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 134
    .line 135
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->d(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)V

    .line 136
    .line 137
    .line 138
    goto :goto_2

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 140
    .line 141
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->e(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)I

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    if-eqz v0, :cond_3

    .line 146
    .line 147
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 148
    .line 149
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->c(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Z

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    if-nez v0, :cond_3

    .line 154
    .line 155
    if-eqz v3, :cond_3

    .line 156
    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 158
    .line 159
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->k(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 164
    .line 165
    .line 166
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 167
    .line 168
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->i(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/api/banner/PAGBannerAdWrapperListener;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    if-eqz v0, :cond_4

    .line 173
    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a$f;->a:Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;

    .line 175
    .line 176
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;->i(Lcom/bytedance/sdk/openadsdk/core/bannerexpress/a;)Lcom/bytedance/sdk/openadsdk/api/banner/PAGBannerAdWrapperListener;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/api/banner/PAGBannerAdWrapperListener;->onAdDismissed()V

    .line 181
    .line 182
    .line 183
    :cond_4
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

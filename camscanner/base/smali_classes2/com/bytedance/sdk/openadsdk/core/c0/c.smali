.class public abstract Lcom/bytedance/sdk/openadsdk/core/c0/c;
.super Ljava/lang/Object;
.source "InteractionListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/c0/c$a;
    }
.end annotation


# static fields
.field protected static n:I = 0x8

.field private static o:F

.field private static p:F

.field private static q:F

.field private static r:F

.field private static s:J


# instance fields
.field protected a:F

.field protected b:F

.field protected c:F

.field protected d:F

.field protected e:J

.field protected f:J

.field protected g:I

.field protected h:I

.field protected i:I

.field protected j:Z

.field public k:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->e()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 12
    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->o:F

    .line 15
    .line 16
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->p:F

    .line 17
    .line 18
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->q:F

    .line 19
    .line 20
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->r:F

    .line 21
    .line 22
    const-wide/16 v0, 0x0

    .line 23
    .line 24
    sput-wide v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->s:J

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, -0x40800000    # -1.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a:F

    .line 7
    .line 8
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->b:F

    .line 9
    .line 10
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 11
    .line 12
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 13
    .line 14
    const-wide/16 v0, -0x1

    .line 15
    .line 16
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->e:J

    .line 17
    .line 18
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->f:J

    .line 19
    .line 20
    const/4 v0, -0x1

    .line 21
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    .line 22
    .line 23
    const/16 v1, -0x400

    .line 24
    .line 25
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->h:I

    .line 26
    .line 27
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->i:I

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    .line 31
    .line 32
    new-instance v0, Landroid/util/SparseArray;

    .line 33
    .line 34
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->k:Landroid/util/SparseArray;

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->l:I

    .line 41
    .line 42
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->m:I

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private a(Landroid/view/View;Landroid/graphics/Point;)Z
    .locals 6

    .line 1
    instance-of v0, p1, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 2
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    .line 3
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 4
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->c(Landroid/view/View;)Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 6
    invoke-virtual {v3, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p2, Landroid/graphics/Point;->x:I

    aget v2, v0, v1

    if-lt p1, v2, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    if-gt p1, v2, :cond_0

    iget p1, p2, Landroid/graphics/Point;->y:I

    aget p2, v0, v5

    if-lt p1, p2, :cond_0

    .line 8
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    if-gt p1, p2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 9
    :cond_1
    invoke-direct {p0, v3, p2}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a(Landroid/view/View;Landroid/graphics/Point;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method


# virtual methods
.method protected abstract a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "FFFF",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public a()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/j;->a()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a:F

    .line 9
    .line 10
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->b:F

    .line 11
    .line 12
    iget v5, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 13
    .line 14
    iget v6, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 15
    .line 16
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->k:Landroid/util/SparseArray;

    .line 17
    .line 18
    iget-boolean v8, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    .line 19
    .line 20
    move-object v1, p0

    .line 21
    move-object v2, p1

    .line 22
    invoke-virtual/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13

    .line 1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDeviceId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->h:I

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    .line 13
    .line 14
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getSource()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->i:I

    .line 19
    .line 20
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const/4 v2, 0x1

    .line 25
    if-eqz v1, :cond_a

    .line 26
    .line 27
    const/4 v3, 0x3

    .line 28
    if-eq v1, v2, :cond_6

    .line 29
    .line 30
    const/4 p1, 0x2

    .line 31
    if-eq v1, p1, :cond_1

    .line 32
    .line 33
    if-eq v1, v3, :cond_0

    .line 34
    .line 35
    const/4 v3, -0x1

    .line 36
    const/4 v6, -0x1

    .line 37
    goto/16 :goto_1

    .line 38
    .line 39
    :cond_0
    const/4 v3, 0x4

    .line 40
    const/4 v6, 0x4

    .line 41
    goto/16 :goto_1

    .line 42
    .line 43
    :cond_1
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->q:F

    .line 44
    .line 45
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/c;->o:F

    .line 50
    .line 51
    sub-float/2addr v3, v4

    .line 52
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    add-float/2addr v1, v3

    .line 57
    sput v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->q:F

    .line 58
    .line 59
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->r:F

    .line 60
    .line 61
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/c;->p:F

    .line 66
    .line 67
    sub-float/2addr v3, v4

    .line 68
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    add-float/2addr v1, v3

    .line 73
    sput v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->r:F

    .line 74
    .line 75
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    sput v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->o:F

    .line 80
    .line 81
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    sput v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->p:F

    .line 86
    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 88
    .line 89
    .line 90
    move-result-wide v3

    .line 91
    sget-wide v5, Lcom/bytedance/sdk/openadsdk/core/c0/c;->s:J

    .line 92
    .line 93
    sub-long/2addr v3, v5

    .line 94
    const-wide/16 v5, 0xc8

    .line 95
    .line 96
    cmp-long v1, v3, v5

    .line 97
    .line 98
    if-lez v1, :cond_2

    .line 99
    .line 100
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->q:F

    .line 101
    .line 102
    sget v3, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 103
    .line 104
    int-to-float v3, v3

    .line 105
    cmpl-float v1, v1, v3

    .line 106
    .line 107
    if-gtz v1, :cond_3

    .line 108
    .line 109
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->r:F

    .line 110
    .line 111
    cmpl-float v1, v1, v3

    .line 112
    .line 113
    if-lez v1, :cond_2

    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_2
    const/4 v2, 0x2

    .line 117
    :cond_3
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 122
    .line 123
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 128
    .line 129
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 130
    .line 131
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->l:I

    .line 132
    .line 133
    int-to-float v1, v1

    .line 134
    sub-float/2addr p1, v1

    .line 135
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 140
    .line 141
    int-to-float v1, v1

    .line 142
    cmpl-float p1, p1, v1

    .line 143
    .line 144
    if-gez p1, :cond_4

    .line 145
    .line 146
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 147
    .line 148
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->m:I

    .line 149
    .line 150
    int-to-float v1, v1

    .line 151
    sub-float/2addr p1, v1

    .line 152
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 157
    .line 158
    int-to-float v1, v1

    .line 159
    cmpl-float p1, p1, v1

    .line 160
    .line 161
    if-ltz p1, :cond_5

    .line 162
    .line 163
    :cond_4
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    .line 164
    .line 165
    :cond_5
    move v6, v2

    .line 166
    goto/16 :goto_1

    .line 167
    .line 168
    :cond_6
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    .line 169
    .line 170
    .line 171
    move-result v1

    .line 172
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 173
    .line 174
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    .line 175
    .line 176
    .line 177
    move-result v1

    .line 178
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 179
    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 181
    .line 182
    .line 183
    move-result-wide v4

    .line 184
    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->f:J

    .line 185
    .line 186
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 187
    .line 188
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->l:I

    .line 189
    .line 190
    int-to-float v4, v4

    .line 191
    sub-float/2addr v1, v4

    .line 192
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 193
    .line 194
    .line 195
    move-result v1

    .line 196
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 197
    .line 198
    int-to-float v4, v4

    .line 199
    cmpl-float v1, v1, v4

    .line 200
    .line 201
    if-gez v1, :cond_7

    .line 202
    .line 203
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 204
    .line 205
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->m:I

    .line 206
    .line 207
    int-to-float v4, v4

    .line 208
    sub-float/2addr v1, v4

    .line 209
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 210
    .line 211
    .line 212
    move-result v1

    .line 213
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/c;->n:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    cmpl-float v1, v1, v4

    .line 217
    .line 218
    if-ltz v1, :cond_8

    .line 219
    .line 220
    :cond_7
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    .line 221
    .line 222
    :cond_8
    new-instance v1, Landroid/graphics/Point;

    .line 223
    .line 224
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->c:F

    .line 225
    .line 226
    float-to-int v4, v4

    .line 227
    iget v5, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->d:F

    .line 228
    .line 229
    float-to-int v5, v5

    .line 230
    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 231
    .line 232
    .line 233
    if-eqz p1, :cond_9

    .line 234
    .line 235
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->c(Landroid/view/View;)Z

    .line 236
    .line 237
    .line 238
    move-result v4

    .line 239
    if-nez v4, :cond_9

    .line 240
    .line 241
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    check-cast p1, Landroid/view/View;

    .line 246
    .line 247
    invoke-direct {p0, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a(Landroid/view/View;Landroid/graphics/Point;)Z

    .line 248
    .line 249
    .line 250
    move-result p1

    .line 251
    if-eqz p1, :cond_9

    .line 252
    .line 253
    return v2

    .line 254
    :cond_9
    const/4 v6, 0x3

    .line 255
    goto :goto_1

    .line 256
    :cond_a
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    .line 257
    .line 258
    .line 259
    move-result p1

    .line 260
    float-to-int p1, p1

    .line 261
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->l:I

    .line 262
    .line 263
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    .line 264
    .line 265
    .line 266
    move-result p1

    .line 267
    float-to-int p1, p1

    .line 268
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->m:I

    .line 269
    .line 270
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    .line 271
    .line 272
    .line 273
    move-result p1

    .line 274
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a:F

    .line 275
    .line 276
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    .line 277
    .line 278
    .line 279
    move-result p1

    .line 280
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->b:F

    .line 281
    .line 282
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 283
    .line 284
    .line 285
    move-result-wide v3

    .line 286
    iput-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->e:J

    .line 287
    .line 288
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    .line 289
    .line 290
    .line 291
    move-result p1

    .line 292
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    .line 293
    .line 294
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDeviceId()I

    .line 295
    .line 296
    .line 297
    move-result p1

    .line 298
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->h:I

    .line 299
    .line 300
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getSource()I

    .line 301
    .line 302
    .line 303
    move-result p1

    .line 304
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->i:I

    .line 305
    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 307
    .line 308
    .line 309
    move-result-wide v3

    .line 310
    sput-wide v3, Lcom/bytedance/sdk/openadsdk/core/c0/c;->s:J

    .line 311
    .line 312
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->j:Z

    .line 313
    .line 314
    const/4 v6, 0x0

    .line 315
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->k:Landroid/util/SparseArray;

    .line 316
    .line 317
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 318
    .line 319
    .line 320
    move-result v1

    .line 321
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;

    .line 322
    .line 323
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getSize()F

    .line 324
    .line 325
    .line 326
    move-result v3

    .line 327
    float-to-double v7, v3

    .line 328
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPressure()F

    .line 329
    .line 330
    .line 331
    move-result p2

    .line 332
    float-to-double v9, p2

    .line 333
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 334
    .line 335
    .line 336
    move-result-wide v11

    .line 337
    move-object v5, v2

    .line 338
    invoke-direct/range {v5 .. v12}, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;-><init>(IDDJ)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {p1, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 342
    .line 343
    .line 344
    return v0
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.class public Lcom/bytedance/sdk/openadsdk/core/f0/m;
.super Ljava/lang/Object;
.source "DynamicClickInfo.java"

# interfaces
.implements Lb/b/a/a/d/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/f0/m$b;
    }
.end annotation


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:J

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:Ljava/lang/String;

.field public l:I

.field public m:Lorg/json/JSONObject;

.field public n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Z

.field public p:I


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/f0/m$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->a(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->a:F

    .line 4
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->b(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->b:F

    .line 5
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->h(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->c:F

    .line 6
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->i(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->d:F

    .line 7
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->j(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->e:J

    .line 8
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->k(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->f:J

    .line 9
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->l(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->g:I

    .line 10
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->m(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->h:I

    .line 11
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->n(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->i:I

    .line 12
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->o(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->j:I

    .line 13
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->c(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->k:Ljava/lang/String;

    .line 14
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->o:Landroid/util/SparseArray;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->n:Landroid/util/SparseArray;

    .line 15
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->d(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->o:Z

    .line 16
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->e(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->l:I

    .line 17
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->f(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->m:Lorg/json/JSONObject;

    .line 18
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m$b;->g(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/m;->p:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;Lcom/bytedance/sdk/openadsdk/core/f0/m$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/f0/m;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/m$b;)V

    return-void
.end method

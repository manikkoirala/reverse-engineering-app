.class public abstract Lcom/bytedance/sdk/openadsdk/core/c0/g;
.super Lcom/bytedance/sdk/openadsdk/core/c0/c;
.source "VastClickListenerWrapper.java"


# instance fields
.field private final t:Ljava/lang/String;

.field private final u:Lcom/bytedance/sdk/openadsdk/core/i0/a;

.field private v:Lcom/bytedance/sdk/openadsdk/core/c0/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/c0/g;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/a;Lcom/bytedance/sdk/openadsdk/core/c0/c;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/a;Lcom/bytedance/sdk/openadsdk/core/c0/c;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/c0/c;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->t:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->u:Lcom/bytedance/sdk/openadsdk/core/i0/a;

    .line 5
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->v:Lcom/bytedance/sdk/openadsdk/core/c0/c;

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "FFFF",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;Z)V"
        }
    .end annotation

    move-object v0, p0

    move-object v2, p1

    .line 2
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->u:Lcom/bytedance/sdk/openadsdk/core/i0/a;

    if-eqz v1, :cond_0

    .line 3
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->t:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->b(Ljava/lang/String;)V

    :cond_0
    if-eqz v2, :cond_3

    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->c:I

    const v4, 0x22000001

    if-ne v1, v3, :cond_1

    const-string v1, "VAST_TITLE"

    .line 5
    invoke-virtual {p1, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 6
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->g:I

    if-ne v1, v3, :cond_2

    const-string v1, "VAST_DESCRIPTION"

    .line 7
    invoke-virtual {p1, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 8
    :cond_2
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->t:Ljava/lang/String;

    invoke-virtual {p1, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 9
    :cond_3
    :goto_0
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->v:Lcom/bytedance/sdk/openadsdk/core/c0/c;

    if-eqz v1, :cond_4

    .line 10
    iget-wide v3, v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->e:J

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->e:J

    .line 11
    iget-wide v3, v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->f:J

    iput-wide v3, v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->f:J

    .line 12
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    iput v3, v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    .line 13
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/c0/c;->g:I

    iput v3, v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->h:I

    .line 14
    iput v3, v1, Lcom/bytedance/sdk/openadsdk/core/c0/c;->i:I

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move/from16 v8, p7

    .line 15
    invoke-virtual/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V

    .line 16
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/c0/g;->b()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/c0/c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/g;->v:Lcom/bytedance/sdk/openadsdk/core/c0/c;

    return-void
.end method

.method public abstract b()V
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

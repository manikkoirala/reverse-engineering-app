.class public Lcom/bytedance/sdk/openadsdk/core/j0/d/a;
.super Ljava/lang/Object;
.source "VideoPreloadFactory.java"


# static fields
.field public static final a:Lb/a/a/a/a/a/a/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lb/a/a/a/a/a/b/b/c/a;

    .line 2
    .line 3
    invoke-direct {v0}, Lb/a/a/a/a/a/b/b/c/a;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a:Lb/a/a/a/a/a/a/h/a;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static a(Lb/a/a/a/a/a/a/f/c;Lb/a/a/a/a/a/a/h/a$a;)V
    .locals 12

    .line 4
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->j()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->u()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->i()I

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_8

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto/16 :goto_4

    :cond_1
    const/16 v0, 0x1770

    .line 5
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->a(I)V

    .line 6
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->c(I)V

    .line 7
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->g(I)V

    const-string v0, "material_meta"

    .line 8
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    const-string v4, "ad_slot"

    .line 9
    invoke-virtual {p0, v4}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, v4}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz v5, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    .line 10
    invoke-virtual {p0, v0}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 11
    invoke-virtual {p0, v4}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 12
    invoke-static {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->c(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    move-object v8, v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    move-object v8, v0

    .line 13
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    .line 14
    new-instance v11, Lcom/bytedance/sdk/openadsdk/core/j0/d/a$a;

    move-object v1, v11

    move-object v2, p1

    move-object v3, v0

    move-object v4, v8

    move-wide v5, v9

    move-object v7, p0

    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a$a;-><init>(Lb/a/a/a/a/a/a/h/a$a;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;JLb/a/a/a/a/a/a/f/c;)V

    .line 15
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz p1, :cond_5

    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x194

    invoke-interface {p1, p0, v2, v1}, Lb/a/a/a/a/a/a/h/a$a;->a(Lb/a/a/a/a/a/a/f/c;ILjava/lang/String;)V

    .line 17
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sub-long v4, v1, v9

    const/4 v6, -0x1

    const-string v7, "video url is invalid"

    move-object v1, p0

    move-object v2, v0

    move-object v3, v8

    .line 18
    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;JILjava/lang/String;)V

    return-void

    .line 19
    :cond_6
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x17

    if-lt p1, v0, :cond_7

    .line 20
    :try_start_0
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a:Lb/a/a/a/a/a/a/h/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0, p0, v11}, Lb/a/a/a/a/a/a/h/a;->〇080(Landroid/content/Context;Lb/a/a/a/a/a/a/f/c;Lb/a/a/a/a/a/a/h/a$a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p0

    .line 21
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string p1, "VideoPreloadUtils"

    invoke-static {p1, p0}, Lcom/bytedance/sdk/component/utils/m;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 22
    :cond_7
    invoke-static {}, Lb/a/a/a/a/a/b/c/q/a;->〇o〇()Lb/a/a/a/a/a/b/c/q/a;

    move-result-object p1

    invoke-virtual {p1, p0}, Lb/a/a/a/a/a/b/c/q/a;->〇o00〇〇Oo(Lb/a/a/a/a/a/a/f/c;)Z

    :goto_3
    return-void

    :cond_8
    :goto_4
    if-eqz p1, :cond_9

    const/16 v0, 0x64

    .line 23
    invoke-interface {p1, p0, v0}, Lb/a/a/a/a/a/a/h/a$a;->a(Lb/a/a/a/a/a/a/f/c;I)V

    :cond_9
    return-void
.end method

.method static synthetic a(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 3
    invoke-static {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method static synthetic a(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;J)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;J)V

    return-void
.end method

.method static synthetic a(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;JILjava/lang/String;)V
    .locals 0

    .line 2
    invoke-static/range {p0 .. p6}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;JILjava/lang/String;)V

    return-void
.end method

.method private static a(Lb/a/a/a/a/a/a/f/c;)Z
    .locals 2

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->i()I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 8

    .line 25
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, "ws:"

    const/4 v6, 0x0

    const/4 v7, 0x3

    move-object v2, p0

    .line 26
    invoke-virtual/range {v2 .. v7}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, "wss:"

    const/4 v6, 0x0

    const/4 v7, 0x4

    move-object v2, p0

    .line 28
    invoke-virtual/range {v2 .. v7}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 30
    :cond_2
    :goto_0
    invoke-static {p0}, Lb/b/a/a/f/a/g;->〇O8o08O(Ljava/lang/String;)Lb/b/a/a/f/a/g;

    move-result-object p0

    if-eqz p0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private static b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 5

    .line 25
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 26
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/a0;->d(I)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 27
    invoke-static {p1, v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILb/a/a/a/a/a/a/f/c;)Lorg/json/JSONObject;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/bytedance/sdk/openadsdk/d/r/b/i;

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->j()I

    move-result p0

    int-to-long v3, p0

    invoke-direct {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/d/r/b/i;-><init>(Ljava/lang/String;J)V

    .line 29
    new-instance p0, Lcom/bytedance/sdk/openadsdk/d/r/b/a;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/a;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/d/r/b/c;)V

    .line 30
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/d/r/b/a;)V

    return-void
.end method

.method private static b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;J)V
    .locals 4

    .line 13
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 14
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/a0;->d(I)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 15
    invoke-static {p1, v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILb/a/a/a/a/a/a/f/c;)Lorg/json/JSONObject;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/bytedance/sdk/openadsdk/d/r/b/l;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;-><init>()V

    .line 17
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;->a(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->j()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;->c(J)V

    .line 19
    invoke-virtual {v1, p3, p4}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;->a(J)V

    .line 20
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->l()I

    move-result p0

    const/4 p3, 0x1

    if-ne p0, p3, :cond_1

    const-wide/16 p3, 0x1

    .line 21
    invoke-virtual {v1, p3, p4}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;->b(J)V

    goto :goto_0

    :cond_1
    const-wide/16 p3, 0x0

    .line 22
    invoke-virtual {v1, p3, p4}, Lcom/bytedance/sdk/openadsdk/d/r/b/l;->b(J)V

    .line 23
    :goto_0
    new-instance p0, Lcom/bytedance/sdk/openadsdk/d/r/b/a;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/a;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/d/r/b/c;)V

    .line 24
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->d(Lcom/bytedance/sdk/openadsdk/d/r/b/a;)V

    return-void
.end method

.method private static b(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;JILjava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/a0;->d(I)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 3
    invoke-static {p1, v0, v1, p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILb/a/a/a/a/a/a/f/c;)Lorg/json/JSONObject;

    move-result-object v0

    .line 4
    new-instance v1, Lcom/bytedance/sdk/openadsdk/d/r/b/j;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;-><init>()V

    .line 5
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->c(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->j()I

    move-result p0

    int-to-long v2, p0

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->b(J)V

    .line 7
    invoke-virtual {v1, p3, p4}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->a(J)V

    .line 8
    invoke-virtual {v1, p5}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->a(I)V

    .line 9
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    const-string p3, ""

    if-eqz p0, :cond_1

    move-object p6, p3

    :cond_1
    invoke-virtual {v1, p6}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->a(Ljava/lang/String;)V

    .line 10
    invoke-virtual {v1, p3}, Lcom/bytedance/sdk/openadsdk/d/r/b/j;->b(Ljava/lang/String;)V

    .line 11
    new-instance p0, Lcom/bytedance/sdk/openadsdk/d/r/b/a;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/a;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/d/r/b/c;)V

    .line 12
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->b(Lcom/bytedance/sdk/openadsdk/d/r/b/a;)V

    return-void
.end method

.method private static c(Lb/a/a/a/a/a/a/f/c;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->u()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->p()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->j()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    int-to-long v0, v0

    .line 24
    :goto_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    .line 25
    .line 26
    .line 27
    move-result p2

    .line 28
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/a0;->d(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    const/4 v2, 0x0

    .line 33
    const/4 v3, -0x1

    .line 34
    invoke-static {p1, v2, v3, p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILb/a/a/a/a/a/a/f/c;)Lorg/json/JSONObject;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    new-instance v3, Lcom/bytedance/sdk/openadsdk/d/r/b/k;

    .line 39
    .line 40
    invoke-virtual {p0}, Lb/a/a/a/a/a/a/f/c;->m()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-direct {v3, p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/k;-><init>(Ljava/lang/String;J)V

    .line 45
    .line 46
    .line 47
    new-instance p0, Lcom/bytedance/sdk/openadsdk/d/r/b/a;

    .line 48
    .line 49
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/r/b/a;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/d/r/b/c;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->c(Lcom/bytedance/sdk/openadsdk/d/r/b/a;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

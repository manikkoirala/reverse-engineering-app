.class public interface abstract Lcom/bytedance/sdk/openadsdk/core/settings/d;
.super Ljava/lang/Object;
.source "ITTSdkSettings.java"


# virtual methods
.method public abstract A()Z
.end method

.method public abstract A(Ljava/lang/String;)Z
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract B()V
.end method

.method public abstract C()I
.end method

.method public abstract D()V
.end method

.method public abstract E()Z
.end method

.method public abstract F()V
.end method

.method public abstract G()Z
.end method

.method public abstract H()Z
.end method

.method public abstract I()I
.end method

.method public abstract J()I
.end method

.method public abstract K()[Ljava/lang/String;
.end method

.method public abstract L()Ljava/lang/String;
.end method

.method public abstract M()I
.end method

.method public abstract N()I
.end method

.method public abstract O()Z
.end method

.method public abstract P()Z
.end method

.method public abstract Q()Ljava/lang/String;
.end method

.method public abstract R()Z
.end method

.method public abstract S()Lorg/json/JSONObject;
.end method

.method public abstract T()I
.end method

.method public abstract U()Z
.end method

.method public abstract V()I
.end method

.method public abstract W()Ljava/lang/String;
.end method

.method public abstract X()I
.end method

.method public abstract Y()Z
.end method

.method public abstract Z()J
.end method

.method public abstract a(I)I
.end method

.method public abstract a(Ljava/lang/String;)I
.end method

.method public abstract a(Ljava/lang/String;Z)I
.end method

.method public abstract a()J
.end method

.method public abstract a(IZ)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Landroid/content/Context;)V
.end method

.method public abstract a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/settings/c$c;)V
.end method

.method public abstract a0()I
.end method

.method public abstract b()I
.end method

.method public abstract b(I)I
.end method

.method public abstract b(Ljava/lang/String;)I
.end method

.method public abstract b(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/settings/c$c;)V
.end method

.method public abstract b0()Z
.end method

.method public abstract c()I
.end method

.method public abstract c(I)I
.end method

.method public abstract c(Ljava/lang/String;)Z
.end method

.method public abstract c0()Ljava/lang/String;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d(I)V
.end method

.method public abstract d(Ljava/lang/String;)Z
.end method

.method public abstract d0()Ljava/lang/String;
.end method

.method public abstract e()I
.end method

.method public abstract e(Ljava/lang/String;)Z
.end method

.method public abstract e0()Z
.end method

.method public abstract f()Z
.end method

.method public abstract f(Ljava/lang/String;)Z
.end method

.method public abstract g()Lorg/json/JSONObject;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract g(Ljava/lang/String;)Z
.end method

.method public abstract h(Ljava/lang/String;)I
.end method

.method public abstract h()Z
.end method

.method public abstract i()I
.end method

.method public abstract i(Ljava/lang/String;)Z
.end method

.method public abstract j(Ljava/lang/String;)I
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract k(Ljava/lang/String;)Z
.end method

.method public abstract l()I
.end method

.method public abstract l(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract m()I
.end method

.method public abstract m(Ljava/lang/String;)Z
.end method

.method public abstract n()Z
.end method

.method public abstract n(Ljava/lang/String;)Z
.end method

.method public abstract o()I
.end method

.method public abstract o(Ljava/lang/String;)I
.end method

.method public abstract p(Ljava/lang/String;)I
.end method

.method public abstract p()Z
.end method

.method public abstract q()Lcom/bytedance/sdk/openadsdk/core/settings/g;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract q(Ljava/lang/String;)Z
.end method

.method public abstract r(Ljava/lang/String;)I
.end method

.method public abstract r()Ljava/lang/String;
.end method

.method public abstract s(Ljava/lang/String;)I
.end method

.method public abstract s()V
.end method

.method public abstract t()Ljava/lang/String;
.end method

.method public abstract t(Ljava/lang/String;)Z
.end method

.method public abstract u(Ljava/lang/String;)I
.end method

.method public abstract u()V
.end method

.method public abstract v(Ljava/lang/String;)I
.end method

.method public abstract v()Z
.end method

.method public abstract w(Ljava/lang/String;)I
.end method

.method public abstract w()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract x()Lcom/bytedance/sdk/openadsdk/core/settings/e;
.end method

.method public abstract x(Ljava/lang/String;)Z
.end method

.method public abstract y(Ljava/lang/String;)I
.end method

.method public abstract y()Z
.end method

.method public abstract z()Ljava/lang/String;
.end method

.method public abstract z(Ljava/lang/String;)Z
.end method

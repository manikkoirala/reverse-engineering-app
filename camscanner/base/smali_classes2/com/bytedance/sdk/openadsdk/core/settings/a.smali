.class public Lcom/bytedance/sdk/openadsdk/core/settings/a;
.super Ljava/lang/Object;
.source "AdSlotSetter.java"


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:I

.field public E:I

.field public F:Z

.field public G:I

.field public H:I

.field public I:I

.field public J:J

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/settings/f;",
            ">;"
        }
    .end annotation
.end field

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/settings/f;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:Z

.field public u:I

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 5

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 106
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->b:I

    .line 107
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    const/4 v1, 0x2

    .line 108
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->d:I

    .line 109
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->e:I

    const/16 v2, 0x64

    .line 110
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->f:I

    const/4 v2, 0x0

    .line 111
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->g:I

    .line 112
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->h:I

    .line 113
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->i:I

    const/4 v3, 0x3

    .line 114
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->j:I

    const/16 v3, 0x1e

    .line 115
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->k:I

    .line 116
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->l:I

    .line 117
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->m:I

    .line 118
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->n:I

    const/16 v3, 0x5dc

    .line 119
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->o:I

    .line 120
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->p:I

    const/4 v3, -0x1

    .line 121
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->q:I

    .line 122
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->r:I

    const/4 v4, 0x5

    .line 123
    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->s:I

    .line 124
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->t:Z

    .line 125
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->u:I

    .line 126
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->w:I

    .line 127
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->x:I

    .line 128
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->y:I

    .line 129
    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->z:I

    .line 130
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->A:Z

    .line 131
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->B:Z

    .line 132
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->C:Z

    .line 133
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->D:I

    .line 134
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->E:I

    .line 135
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->F:Z

    const v2, 0xea60

    .line 136
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->G:I

    .line 137
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->H:I

    .line 138
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->I:I

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->M:Ljava/util/List;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->N:Ljava/util/List;

    .line 141
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->a:Ljava/lang/String;

    .line 142
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x1

    .line 2
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->b:I

    .line 3
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    const/4 v3, 0x2

    .line 4
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->d:I

    .line 5
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->e:I

    const/16 v4, 0x64

    .line 6
    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->f:I

    const/4 v5, 0x0

    .line 7
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->g:I

    .line 8
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->h:I

    .line 9
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->i:I

    const/4 v6, 0x3

    .line 10
    iput v6, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->j:I

    const/16 v7, 0x1e

    .line 11
    iput v7, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->k:I

    .line 12
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->l:I

    .line 13
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->m:I

    .line 14
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->n:I

    const/16 v8, 0x5dc

    .line 15
    iput v8, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->o:I

    .line 16
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->p:I

    const/4 v9, -0x1

    .line 17
    iput v9, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->q:I

    .line 18
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->r:I

    const/4 v10, 0x5

    .line 19
    iput v10, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->s:I

    .line 20
    iput-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->t:Z

    .line 21
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->u:I

    .line 22
    iput v9, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->w:I

    .line 23
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->x:I

    .line 24
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->y:I

    .line 25
    iput v10, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->z:I

    .line 26
    iput-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->A:Z

    .line 27
    iput-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->B:Z

    .line 28
    iput-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->C:Z

    .line 29
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->D:I

    .line 30
    iput v9, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->E:I

    .line 31
    iput-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->F:Z

    const v11, 0xea60

    .line 32
    iput v11, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->G:I

    .line 33
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->H:I

    .line 34
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->I:I

    .line 35
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->M:Ljava/util/List;

    .line 36
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->N:Ljava/util/List;

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v12, "code_id"

    .line 37
    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->a:Ljava/lang/String;

    const-string v12, "auto_play"

    .line 38
    invoke-virtual {v1, v12, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v12

    iput v12, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->b:I

    const-string v12, "endcard_close_time"

    .line 39
    invoke-virtual {v1, v12, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v12

    iput v12, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->E:I

    const-string v12, "voice_control"

    .line 40
    invoke-virtual {v1, v12, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    iput v13, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    const-string v13, "rv_preload"

    .line 41
    invoke-virtual {v1, v13, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    iput v13, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->d:I

    const-string v13, "nv_preload"

    .line 42
    invoke-virtual {v1, v13, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    iput v13, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->e:I

    const-string v13, "proportion_watching"

    .line 43
    invoke-virtual {v1, v13, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->f:I

    const-string v4, "skip_time_displayed"

    .line 44
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->g:I

    const-string v4, "video_skip_result"

    .line 45
    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->h:I

    const-string v4, "reg_creative_control"

    .line 46
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->i:I

    const-string v4, "play_bar_show_time"

    .line 47
    invoke-virtual {v1, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->j:I

    const-string v4, "rv_skip_time"

    .line 48
    invoke-virtual {v1, v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->k:I

    if-gez v4, :cond_1

    .line 49
    iput v7, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->k:I

    .line 50
    :cond_1
    invoke-virtual {v1, v12, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->l:I

    const-string v4, "if_show_win"

    .line 51
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->m:I

    const-string v4, "sp_preload"

    .line 52
    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->n:I

    const-string v4, "stop_time"

    .line 53
    invoke-virtual {v1, v4, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->o:I

    const-string v4, "native_playable_delay"

    .line 54
    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->p:I

    const-string v4, "time_out_control"

    .line 55
    invoke-virtual {v1, v4, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->q:I

    const-string v4, "playable_close_time"

    .line 56
    invoke-virtual {v1, v4, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->w:I

    const-string v4, "playable_reward_type"

    .line 57
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->r:I

    const-string v4, "reward_is_callback"

    .line 58
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->u:I

    const-string v4, "iv_skip_time"

    .line 59
    invoke-virtual {v1, v4, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->s:I

    const-string v4, "parent_tpl_ids"

    .line 60
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/a;->a(Lorg/json/JSONArray;)V

    const-string v4, "slot_type"

    .line 61
    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    const-string v4, "close_on_click"

    .line 62
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->t:Z

    const-string v4, "allow_system_back"

    .line 63
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->x:I

    const-string v4, "splash_skip_time"

    .line 64
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->y:I

    const-string v4, "splash_image_count_down_time"

    .line 65
    invoke-virtual {v1, v4, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->z:I

    const-string v4, "splash_count_down_time_off"

    .line 66
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->B:Z

    const-string v4, "splash_close_on_click"

    .line 67
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->C:Z

    const-string v4, "splash_load_strategy"

    .line 68
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->D:I

    if-ltz v4, :cond_2

    if-le v4, v2, :cond_3

    .line 69
    :cond_2
    iput v5, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->D:I

    :cond_3
    const-string v4, "allow_mediaview_click"

    .line 70
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->A:Z

    const-string v4, "total_time_out"

    .line 71
    invoke-virtual {v1, v4, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->G:I

    if-lez v4, :cond_4

    const v6, 0x1b7740

    if-le v4, v6, :cond_5

    .line 72
    :cond_4
    iput v11, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->G:I

    :cond_5
    const-string v4, "req_parallel_num"

    .line 73
    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->H:I

    if-lt v4, v2, :cond_6

    const/4 v6, 0x4

    if-le v4, v6, :cond_7

    .line 74
    :cond_6
    iput v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->H:I

    :cond_7
    const-string v4, "bidding_token_tmax"

    const/16 v6, 0x3e8

    .line 75
    invoke-virtual {v1, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    const-string v4, "ad_load_type"

    .line 76
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->I:I

    if-lt v4, v2, :cond_8

    if-le v4, v3, :cond_9

    .line 77
    :cond_8
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->I:I

    :cond_9
    const-string v4, "is_mediation"

    .line 78
    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->F:Z

    if-eqz v4, :cond_11

    .line 79
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->F()V

    .line 80
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 81
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const-string v7, "mediation_config"

    .line 82
    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 83
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v5, v8, :cond_10

    .line 84
    invoke-virtual {v7, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "adn_name"

    .line 85
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v9, "adn_slot_id"

    .line 86
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v9, "ad_expired_time"

    const v10, 0x36ee80

    .line 87
    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    const-string v9, "req_bidding_type"

    .line 88
    invoke-virtual {v8, v9, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    const-string v10, "rit_cpm"

    .line 89
    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v10, "show_sort"

    .line 90
    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v18

    const-string v10, "layer_time_out"

    const/16 v12, 0x7d0

    .line 91
    invoke-virtual {v8, v10, v12}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    if-lez v10, :cond_b

    if-le v10, v11, :cond_a

    goto :goto_1

    :cond_a
    move/from16 v19, v10

    goto :goto_2

    :cond_b
    :goto_1
    const/16 v19, 0x7d0

    :goto_2
    const-string v10, "server_params"

    .line 92
    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    const-string v10, ""

    if-ne v9, v3, :cond_d

    .line 93
    new-instance v12, Lcom/bytedance/sdk/openadsdk/core/settings/f;

    if-nez v8, :cond_c

    move-object/from16 v20, v10

    goto :goto_3

    :cond_c
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v20, v8

    :goto_3
    move-object v8, v12

    move-object v12, v8

    move/from16 v16, v9

    invoke-direct/range {v12 .. v20}, Lcom/bytedance/sdk/openadsdk/core/settings/f;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_d
    if-nez v9, :cond_f

    .line 94
    new-instance v12, Lcom/bytedance/sdk/openadsdk/core/settings/f;

    if-nez v8, :cond_e

    move-object/from16 v20, v10

    goto :goto_4

    :cond_e
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v20, v8

    :goto_4
    move-object v8, v12

    move-object v12, v8

    move/from16 v16, v9

    invoke-direct/range {v12 .. v20}, Lcom/bytedance/sdk/openadsdk/core/settings/f;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_f
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 95
    :cond_10
    iput-object v4, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->M:Ljava/util/List;

    .line 96
    iput-object v6, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->N:Ljava/util/List;

    .line 97
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 98
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->N:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 99
    :cond_11
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/settings/a;->a(I)Z

    move-result v3

    if-nez v3, :cond_12

    .line 100
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->c:I

    .line 101
    :cond_12
    iget v3, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->l:I

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/settings/a;->a(I)Z

    move-result v3

    if-nez v3, :cond_13

    .line 102
    iput v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->l:I

    :cond_13
    const-string v2, "waterfall_id"

    .line 103
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->J:J

    const-string v2, "waterfall_version"

    .line 104
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->K:Ljava/lang/String;

    return-void
.end method

.method private static a(I)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public a(Lorg/json/JSONArray;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 2
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 3
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->v:Ljava/util/List;

    const/4 v0, 0x0

    .line 4
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/settings/a;->v:Ljava/util/List;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    :goto_1
    return-void
.end method

.class public Lcom/bytedance/sdk/openadsdk/core/i;
.super Ljava/lang/Object;
.source "H5AdInteractionManager.java"


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

.field private b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;

.field private c:Landroid/view/View;


# virtual methods
.method public a(ILcom/bytedance/sdk/openadsdk/core/f0/m;)V
    .locals 9

    .line 1
    const/4 v0, -0x1

    .line 2
    if-eq p1, v0, :cond_3

    .line 3
    .line 4
    if-nez p2, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    iget v3, p2, Lcom/bytedance/sdk/openadsdk/core/f0/m;->a:F

    .line 8
    .line 9
    iget v4, p2, Lcom/bytedance/sdk/openadsdk/core/f0/m;->b:F

    .line 10
    .line 11
    iget v5, p2, Lcom/bytedance/sdk/openadsdk/core/f0/m;->c:F

    .line 12
    .line 13
    iget v6, p2, Lcom/bytedance/sdk/openadsdk/core/f0/m;->d:F

    .line 14
    .line 15
    iget-object v7, p2, Lcom/bytedance/sdk/openadsdk/core/f0/m;->n:Landroid/util/SparseArray;

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    if-eq p1, v0, :cond_2

    .line 19
    .line 20
    const/4 v0, 0x2

    .line 21
    if-eq p1, v0, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;

    .line 25
    .line 26
    if-eqz p1, :cond_3

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;->a(Lcom/bytedance/sdk/openadsdk/core/f0/m;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->b:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;

    .line 32
    .line 33
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->c:Landroid/view/View;

    .line 34
    .line 35
    const/4 v8, 0x1

    .line 36
    invoke-virtual/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/c0/a;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    .line 41
    .line 42
    if-eqz p1, :cond_3

    .line 43
    .line 44
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->a(Lcom/bytedance/sdk/openadsdk/core/f0/m;)V

    .line 45
    .line 46
    .line 47
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/i;->a:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    .line 48
    .line 49
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/i;->c:Landroid/view/View;

    .line 50
    .line 51
    const/4 v8, 0x1

    .line 52
    invoke-virtual/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;Z)V

    .line 53
    .line 54
    .line 55
    :cond_3
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
.end method

.class public Lcom/bytedance/sdk/openadsdk/core/settings/k;
.super Lcom/bytedance/sdk/openadsdk/core/settings/m;
.source "SettingsDefaultRepository.java"


# static fields
.field static final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public j:Z

.field private k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 20

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    const-string v1, "ja"

    .line 4
    .line 5
    const-string v2, "en"

    .line 6
    .line 7
    const-string v3, "ko"

    .line 8
    .line 9
    const-string v4, "zh"

    .line 10
    .line 11
    const-string v5, "th"

    .line 12
    .line 13
    const-string v6, "vi"

    .line 14
    .line 15
    const-string v7, "id"

    .line 16
    .line 17
    const-string v8, "ru"

    .line 18
    .line 19
    const-string v9, "ar"

    .line 20
    .line 21
    const-string v10, "fr"

    .line 22
    .line 23
    const-string v11, "de"

    .line 24
    .line 25
    const-string v12, "it"

    .line 26
    .line 27
    const-string v13, "es"

    .line 28
    .line 29
    const-string v14, "hi"

    .line 30
    .line 31
    const-string v15, "pt"

    .line 32
    .line 33
    const-string v16, "zh-Hant"

    .line 34
    .line 35
    const-string v17, "ms"

    .line 36
    .line 37
    const-string v18, "pl"

    .line 38
    .line 39
    const-string v19, "tr"

    .line 40
    .line 41
    filled-new-array/range {v1 .. v19}, [Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 50
    .line 51
    .line 52
    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->l:Ljava/util/ArrayList;

    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    const-string v0, "tt_sdk_settings.prop"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/settings/m;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/HashSet;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->k:Ljava/util/Set;

    .line 16
    .line 17
    return-void
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 195
    :try_start_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0

    .line 196
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 197
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 198
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 199
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_2
    return-object v0

    .line 200
    :catchall_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 11

    const-string v0, "SettingsDefaultRepository"

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/settings/m;->c()Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    move-result-object v1

    .line 2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->g()Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "digest"

    .line 3
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 4
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->j:Z

    if-eqz v4, :cond_1

    .line 5
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    goto :goto_1

    .line 6
    :cond_1
    invoke-interface {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->remove(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :goto_1
    const-string v2, "data_time"

    .line 7
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "data_time"

    invoke-interface {v1, v4, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putLong(Ljava/lang/String;J)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    const-string v2, "req_inter_min"

    .line 8
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-wide/16 v3, 0xa

    .line 9
    invoke-virtual {p1, v2, v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-wide/16 v7, 0x3c

    mul-long v3, v3, v7

    const-wide/16 v7, 0x3e8

    mul-long v3, v3, v7

    const-wide/16 v7, 0x0

    cmp-long v9, v3, v7

    if-ltz v9, :cond_2

    const-wide/32 v7, 0x5265c00

    cmp-long v9, v3, v7

    if-lez v9, :cond_3

    :cond_2
    const-wide/32 v3, 0x927c0

    .line 10
    :cond_3
    invoke-interface {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putLong(Ljava/lang/String;J)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_4
    const-string v2, "lp_new_style"

    .line 11
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    const v3, 0x7fffffff

    if-eqz v2, :cond_5

    const-string v2, "lp_new_style"

    .line 12
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "landingpage_new_style"

    .line 13
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_5
    const-string v2, "blank_detect_rate"

    .line 14
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x1e

    .line 15
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    if-ltz v4, :cond_6

    const/16 v7, 0x64

    if-le v4, v7, :cond_7

    :cond_6
    const/16 v4, 0x1e

    .line 16
    :cond_7
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_8
    const-string v2, "feq_policy"

    .line 17
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v4, "duration"

    .line 18
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 19
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long v7, v7, v9

    .line 20
    invoke-interface {v1, v4, v7, v8}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putLong(Ljava/lang/String;J)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_9
    const-string v4, "max"

    .line 21
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 22
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 23
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_a
    const-string v2, "vbtt"

    .line 24
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x5

    .line 25
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 26
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_b
    const-string v2, "abtest"

    .line 27
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_d

    const-string v4, "version"

    .line 28
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "version"

    .line 29
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "ab_test_version"

    .line 30
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_c
    const-string v4, "param"

    .line 31
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "param"

    .line 32
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "ab_test_param"

    .line 33
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    goto :goto_2

    .line 34
    :cond_d
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->s()V

    :cond_e
    :goto_2
    const-string v2, "log_rate_conf"

    .line 35
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_f

    const-string v4, "global_rate"

    .line 36
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 37
    invoke-virtual {v2, v4, v7, v8}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v7

    double-to-float v2, v7

    .line 38
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putFloat(Ljava/lang/String;F)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_f
    const-string v2, "pyload_h5"

    .line 39
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 40
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 41
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_10
    const-string v2, "pure_pyload_h5"

    .line 42
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v2, "pure_pyload_h5"

    .line 43
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "playableLoadH5Url"

    .line 44
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_11
    const-string v2, "ads_url"

    .line 45
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 46
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 47
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_12
    const-string v2, "app_log_url"

    .line 48
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 49
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_13
    const-string v2, "apm_url"

    .line 51
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 52
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 53
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_14
    const-string v2, "coppa"

    .line 54
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    const/16 v4, -0x63

    .line 55
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 56
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/bytedance/sdk/openadsdk/core/h;->c(I)V

    .line 57
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_15
    const-string v2, "privacy_url"

    .line 58
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v2, "privacy_url"

    .line 59
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "policy_url"

    .line 60
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_16
    const-string v2, "consent_url"

    .line 61
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 62
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_17
    const-string v2, "ivrv_downward"

    .line 64
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 65
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 66
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_18
    const-string v2, "dc"

    .line 67
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 68
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 69
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    .line 70
    :cond_19
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/settings/c$c;)V

    .line 71
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->b(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/settings/c$c;)V

    const-string v2, "if_both_open"

    .line 72
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 73
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 74
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1a
    const-string v2, "support_tnc"

    .line 75
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 76
    invoke-virtual {p1, v2, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 77
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1b
    const-string v2, "insert_js_config"

    .line 78
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1c

    const-string v4, ""

    .line 79
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1c
    const-string v2, "max_tpl_cnts"

    .line 81
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const-string v2, "max_tpl_cnts"

    const/16 v4, 0x64

    .line 82
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "max_tpl_cnts"

    .line 83
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1d
    const-string v2, "app_common_config"

    .line 84
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_28

    const-string v4, "force_language"

    .line 85
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1e

    const-string v4, "force_language"

    .line 86
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1e

    sget-object v7, Lcom/bytedance/sdk/openadsdk/core/settings/k;->l:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1e

    const-string v7, "force_language"

    .line 88
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1e
    const-string v4, "fetch_tpl_timeout_ctrl"

    .line 89
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1f

    const-string v4, "fetch_tpl_timeout_ctrl"

    const/16 v7, 0xbb8

    .line 90
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "fetch_tpl_timeout_ctrl"

    .line 91
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_1f
    const-string v4, "fetch_tpl_second"

    .line 92
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_20

    const-string v4, "fetch_tpl_second"

    .line 93
    invoke-virtual {v2, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "fetch_tpl_second"

    .line 94
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_20
    const-string v4, "disable_rotate_banner_on_dislike"

    .line 95
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    const-string v4, "disable_rotate_banner_on_dislike"

    .line 96
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "disable_rotate_banner_on_dislike"

    .line 97
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_21
    const-string v4, "webview_cache_count"

    .line 98
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_22

    const-string v4, "webview_cache_count"

    const/16 v7, 0x14

    .line 99
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "webview_cache_count"

    .line 100
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_22
    const-string v4, "support_gzip"

    .line 101
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_23

    const-string v4, "support_gzip"

    .line 102
    invoke-virtual {v2, v4, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v7, "support_gzip"

    .line 103
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putBoolean(Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_23
    const-string v4, "aes_key"

    .line 104
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    const-string v4, "aes_key"

    .line 105
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "aes_key"

    .line 106
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_24
    const-string v4, "support_rtl"

    .line 107
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_25

    const-string v4, "support_rtl"

    .line 108
    invoke-virtual {v2, v4, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v7, "support_rtl"

    .line 109
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putBoolean(Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_25
    const-string v4, "ad_revenue_enable"

    .line 110
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_26

    const-string v4, "ad_revenue_enable"

    .line 111
    invoke-virtual {v2, v4, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v7, "ad_revenue_enable"

    .line 112
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putBoolean(Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_26
    const-string v4, "gecko_hosts"

    .line 113
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 114
    :try_start_0
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->k:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    const-string v4, "gecko_hosts"

    .line 115
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-eqz v2, :cond_27

    .line 116
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-eqz v4, :cond_27

    const/4 v4, 0x0

    .line 117
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v4, v7, :cond_27

    .line 118
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->k:Ljava/util/Set;

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 119
    :cond_27
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->k:Ljava/util/Set;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/settings/k;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/settings/k;->k:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "gecko_hosts"

    .line 120
    :try_start_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v2

    .line 121
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GeckoLog: settings json error "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;)V

    :cond_28
    :goto_4
    const-string v2, "read_video_from_cache"

    .line 122
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    const-string v2, "read_video_from_cache"

    .line 123
    invoke-virtual {p1, v2, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "read_video_from_cache"

    .line 124
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_29
    const-string v2, "use_prop"

    .line 125
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2a

    const-string v2, "use_prop"

    .line 126
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 127
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 128
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v7, "use_prop"

    .line 129
    invoke-interface {v4, v7, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2a
    const-string v2, "ad_slot_conf_list"

    .line 131
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 132
    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/settings/b;->b(Lorg/json/JSONArray;)V

    const-string v2, "privacy"

    .line 133
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_31

    const-string v4, "ad_enable"

    .line 134
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2b

    const-string v4, "ad_enable"

    .line 135
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "privacy_ad_enable"

    .line 136
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_2b
    const-string v4, "personalized_ad"

    .line 137
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2c

    const-string v4, "personalized_ad"

    .line 138
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "privacy_personalized_ad"

    .line 139
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_2c
    const-string v4, "sladar_enable"

    .line 140
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2d

    const-string v4, "sladar_enable"

    .line 141
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "privacy_sladar_enable"

    .line 142
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_2d
    const-string v4, "app_log_enable"

    .line 143
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2e

    const-string v4, "app_log_enable"

    .line 144
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "privacy_app_log_enable"

    .line 145
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_2e
    const-string v4, "debug_unlock"

    .line 146
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2f

    const-string v4, "debug_unlock"

    .line 147
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "privacy_debug_unlock"

    .line 148
    invoke-interface {v1, v7, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_2f
    const-string v4, "fields_allowed"

    .line 149
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_31

    const-string v4, "fields_allowed"

    const-string v7, ""

    .line 150
    invoke-virtual {v2, v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_30

    const-string v4, "privacy_fields_allowed"

    .line 152
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    goto :goto_5

    :cond_30
    const-string v2, "privacy_fields_allowed"

    .line 153
    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->remove(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_31
    :goto_5
    const-string v2, "video_cache_config"

    .line 154
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_32

    const-string v2, "video_cache_config"

    .line 155
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "video_cache_config"

    .line 156
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_32
    const-string v2, "loaded_recall_time"

    .line 157
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_34

    const-string v2, "loaded_recall_time"

    .line 158
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_33

    if-eq v2, v5, :cond_33

    const/4 v2, 0x0

    :cond_33
    const-string v4, "loadedCallbackOpportunity"

    .line 159
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_34
    const-string v2, "load_strategy"

    .line 160
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_36

    const-string v2, "load_strategy"

    .line 161
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_35

    if-eq v2, v5, :cond_35

    const/4 v2, 0x0

    :cond_35
    const-string v4, "load_callback_strategy"

    .line 162
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_36
    const-string v2, "support_mem_dynamic"

    .line 163
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    const-string v2, "support_mem_dynamic"

    .line 164
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_37

    if-eq v2, v5, :cond_37

    const/4 v2, 0x0

    :cond_37
    const-string v4, "support_mem_dynamic"

    .line 165
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_38
    const-string v2, "allow_blind_mode_request_ad"

    .line 166
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_39

    const-string v2, "allow_blind_mode_request_ad"

    .line 167
    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string v4, "allow_blind_mode_request_ad"

    .line 168
    invoke-interface {v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putBoolean(Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_39
    const-string v2, "bus_con"

    .line 169
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_3b

    const-string v4, "bus_con_send_log_type"

    .line 170
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3a

    const-string v4, "bus_con_send_log_type"

    .line 171
    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "bus_con_send_log_type"

    .line 172
    invoke-interface {v1, v5, v4}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_3a
    const-string v4, "bus_con_sec_type"

    .line 173
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3b

    const-string v4, "bus_con_sec_type"

    .line 174
    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "bus_con_sec_type"

    .line 175
    invoke-interface {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;

    :cond_3b
    const-string v2, "perf_con"

    .line 176
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3e

    :try_start_2
    const-string v2, "perf_con"

    .line 177
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception p1

    .line 178
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    :goto_6
    if-eqz p1, :cond_3c

    :try_start_3
    const-string v2, "perf_con_stats_rate"

    .line 179
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3c

    const-string v2, "perf_con_stats_rate"

    .line 180
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3c

    const-string v3, "perf_con_stats_rate"

    .line 182
    invoke-interface {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_7

    :catchall_2
    move-exception v2

    .line 183
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3c
    :goto_7
    if-eqz p1, :cond_3d

    :try_start_4
    const-string v2, "perf_con_applog_send"

    .line 184
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3d

    const-string v2, "perf_con_applog_send"

    .line 185
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3d

    const-string v3, "perf_con_applog_send"

    .line 187
    invoke-interface {v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_8

    :catchall_3
    move-exception v2

    .line 188
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3d
    :goto_8
    if-eqz p1, :cond_3e

    :try_start_5
    const-string v2, "perf_con_apm_native"

    .line 189
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3e

    const-string v2, "perf_con_apm_native"

    .line 190
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    const-string v2, "perf_con_apm_native"

    .line 191
    invoke-interface {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->putInt(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/core/settings/c$c;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto :goto_9

    :catchall_4
    move-exception p1

    .line 192
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_3e
    :goto_9
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/settings/c$c;->commit()V

    .line 194
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->D()V

    return-void
.end method

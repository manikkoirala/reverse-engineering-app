.class public abstract Lcom/bytedance/sdk/openadsdk/core/j0/a/a;
.super Ljava/lang/Object;
.source "BaseController.java"

# interfaces
.implements Lb/a/a/a/a/a/a/g/c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/a;
.implements Lcom/bytedance/sdk/component/utils/y$a;


# instance fields
.field protected A:Lb/a/a/a/a/a/a/g/c$a;

.field protected B:Lcom/bytedance/sdk/openadsdk/core/i0/f;

.field protected C:J

.field protected D:Ljava/lang/Runnable;

.field private E:Z

.field private F:I

.field protected a:Ljava/lang/String;

.field protected final b:Lcom/bytedance/sdk/component/utils/y;

.field protected c:Landroid/view/SurfaceHolder;

.field protected d:Landroid/graphics/SurfaceTexture;

.field protected e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

.field protected f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

.field protected final g:Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field protected h:J

.field protected i:J

.field protected final j:Landroid/content/Context;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field protected final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field protected p:Z

.field protected q:Z

.field protected r:Z

.field private s:J

.field protected t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected u:Z

.field protected v:Z

.field protected w:Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

.field protected x:Z

.field protected final y:Landroid/view/ViewGroup;

.field protected z:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lb/a/a/a/a/a/a/g/c$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/view/ViewGroup;)V
    .locals 3
    .param p2    # Lcom/bytedance/sdk/openadsdk/core/f0/q;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "TTAD.VideoController"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Lcom/bytedance/sdk/component/utils/y;

    .line 9
    .line 10
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, v1, p0}, Lcom/bytedance/sdk/component/utils/y;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/y$a;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->b:Lcom/bytedance/sdk/component/utils/y;

    .line 18
    .line 19
    const-wide/16 v0, 0x0

    .line 20
    .line 21
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->h:J

    .line 22
    .line 23
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    .line 24
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->k:Ljava/util/List;

    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    .line 34
    .line 35
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->m:Z

    .line 36
    .line 37
    const/4 v1, 0x1

    .line 38
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->n:Z

    .line 39
    .line 40
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->p:Z

    .line 41
    .line 42
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->q:Z

    .line 43
    .line 44
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    .line 45
    .line 46
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 47
    .line 48
    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 49
    .line 50
    .line 51
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    .line 53
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->v:Z

    .line 54
    .line 55
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$a;

    .line 56
    .line 57
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$a;-><init>(Lcom/bytedance/sdk/openadsdk/core/j0/a/a;)V

    .line 58
    .line 59
    .line 60
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->D:Ljava/lang/Runnable;

    .line 61
    .line 62
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->E:Z

    .line 63
    .line 64
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->F:I

    .line 65
    .line 66
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 67
    .line 68
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->j:Landroid/content/Context;

    .line 69
    .line 70
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->y:Landroid/view/ViewGroup;

    .line 71
    .line 72
    new-instance p1, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    .line 78
    .line 79
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
.end method

.method private A()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->f(I)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(ZZ)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->c(Z)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->v()V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->w()V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private a(JZ)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 41
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->A()V

    .line 42
    :cond_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    invoke-virtual {p3, p1, p2}, Lb/a/a/a/a/a/b/e/d;->b(J)V

    return-void
.end method

.method private a(I)Z
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->b(I)Z

    move-result p1

    return p1
.end method

.method private x()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->k()Lcom/bykv/vk/openvk/component/video/api/renderview/b;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    instance-of v0, v0, Lcom/bykv/vk/openvk/component/video/api/renderview/SSRenderTextureView;

    .line 10
    .line 11
    return v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method protected final B()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->q0()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/q/a;->a(Ljava/util/List;ZLcom/bytedance/sdk/openadsdk/core/f0/q;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/p/d;->a(Ljava/util/List;)V

    .line 15
    .line 16
    .line 17
    return-void
.end method

.method protected final C()V
    .locals 5

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(J)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a()J

    .line 14
    .line 15
    .line 16
    move-result-wide v1

    .line 17
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-long v3, v3

    .line 22
    div-long/2addr v1, v3

    .line 23
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l()J

    .line 27
    .line 28
    .line 29
    move-result-wide v1

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(J)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->c(Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method protected final D()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->w:Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    .line 6
    .line 7
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lb/a/a/a/a/a/a/e/a;Lb/a/a/a/a/a/a/f/c;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected final E()V
    .locals 5

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(J)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a()J

    .line 14
    .line 15
    .line 16
    move-result-wide v1

    .line 17
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    int-to-long v3, v3

    .line 22
    div-long/2addr v1, v3

    .line 23
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l()J

    .line 27
    .line 28
    .line 29
    move-result-wide v1

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(J)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->b(Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public final a()J
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/d;->r()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public a(J)V
    .locals 0

    .line 18
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->C:J

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 0

    .line 1
    return-void
.end method

.method protected final a(Lb/a/a/a/a/a/a/f/a;)V
    .locals 5

    .line 50
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 51
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(J)V

    .line 52
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t()I

    move-result v3

    int-to-long v3, v3

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 53
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(J)V

    .line 54
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(Lb/a/a/a/a/a/a/f/a;)V

    .line 55
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->e(Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;)V

    return-void
.end method

.method public final a(Lb/a/a/a/a/a/a/g/b;I)V
    .locals 0

    .line 38
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->u()V

    :cond_0
    return-void
.end method

.method public final a(Lb/a/a/a/a/a/a/g/b;IZ)V
    .locals 4

    .line 31
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->j:Landroid/content/Context;

    if-nez p1, :cond_0

    return-void

    :cond_0
    int-to-long p2, p2

    .line 32
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->C:J

    mul-long p2, p2, v0

    long-to-float p2, p2

    const/high16 p3, 0x3f800000    # 1.0f

    mul-float p2, p2, p3

    const-string p3, "tt_video_progress_max"

    invoke-static {p1, p3}, Lcom/bytedance/sdk/component/utils/t;->i(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    float-to-long p1, p2

    .line 33
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->C:J

    const-wide/16 v2, 0x0

    cmp-long p3, v0, v2

    if-lez p3, :cond_1

    long-to-int p2, p1

    int-to-long p1, p2

    .line 34
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->s:J

    goto :goto_0

    .line 35
    :cond_1
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->s:J

    .line 36
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_2

    .line 37
    iget-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->s:J

    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(J)V

    :cond_2
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/b;Landroid/graphics/SurfaceTexture;)V
    .locals 1

    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    .line 13
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    const-string v0, "surfaceTextureDestroyed: "

    invoke-static {p2, v0}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz p2, :cond_0

    .line 15
    invoke-virtual {p2, p1}, Lb/a/a/a/a/a/b/e/d;->b(Z)V

    :cond_0
    const/4 p1, 0x0

    .line 16
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d:Landroid/graphics/SurfaceTexture;

    .line 17
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->q()V

    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/b;Landroid/view/SurfaceHolder;)V
    .locals 0

    const/4 p1, 0x0

    .line 8
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    const/4 p2, 0x0

    .line 9
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c:Landroid/view/SurfaceHolder;

    .line 10
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz p2, :cond_0

    .line 11
    invoke-virtual {p2, p1}, Lb/a/a/a/a/a/b/e/d;->b(Z)V

    :cond_0
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/b;Landroid/view/SurfaceHolder;III)V
    .locals 0

    .line 2
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
    .locals 0

    .line 3
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;Z)V
    .locals 0

    .line 4
    return-void
.end method

.method public final a(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;ZZ)V
    .locals 1

    .line 22
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->n:Z

    if-eqz p1, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d()V

    :cond_0
    if-eqz p3, :cond_1

    .line 24
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->n:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->y()Z

    move-result p1

    if-nez p1, :cond_1

    .line 25
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->z()Z

    move-result p2

    const/4 p3, 0x1

    xor-int/2addr p2, p3

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->b(ZZ)V

    .line 26
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1, p4, p3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(ZZZ)V

    .line 27
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lb/a/a/a/a/a/b/e/d;->h()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 28
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->u()V

    .line 29
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->v()V

    goto :goto_0

    .line 30
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->u()V

    :goto_0
    return-void
.end method

.method public final a(Lb/a/a/a/a/a/a/g/c$a;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->A:Lb/a/a/a/a/a/a/g/c$a;

    return-void
.end method

.method public final a(Lb/a/a/a/a/a/a/g/c$b;)V
    .locals 1

    .line 21
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->z:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/c$d;)V
    .locals 0

    .line 5
    return-void
.end method

.method public final a(Lcom/bytedance/sdk/openadsdk/core/widget/e$b;Ljava/lang/String;)V
    .locals 1

    .line 57
    sget-object p2, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$d;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 58
    :cond_0
    invoke-interface {p0}, Lb/a/a/a/a/a/a/g/c;->f()V

    const/4 p1, 0x0

    .line 59
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->p:Z

    .line 60
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->q:Z

    goto :goto_0

    .line 61
    :cond_1
    invoke-interface {p0}, Lb/a/a/a/a/a/a/g/c;->e()V

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d()V

    :goto_0
    return-void
.end method

.method protected final a(Lcom/bytedance/sdk/openadsdk/d/g;)V
    .locals 5

    .line 44
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 45
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(J)V

    .line 46
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t()I

    move-result v3

    int-to-long v3, v3

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 47
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(J)V

    .line 48
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(I)V

    .line 49
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-static {v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->b(Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;Lcom/bytedance/sdk/openadsdk/d/g;)V

    return-void
.end method

.method protected a(Ljava/lang/Runnable;)V
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 6
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .line 20
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->m:Z

    return-void
.end method

.method public abstract synthetic a(ZI)V
.end method

.method public abstract synthetic a(Lb/a/a/a/a/a/a/f/c;)Z
.end method

.method public final b(I)V
    .locals 0

    .line 1
    return-void
.end method

.method public b(J)V
    .locals 0

    .line 2
    return-void
.end method

.method public b(Lb/a/a/a/a/a/a/f/c;)V
    .locals 1

    .line 20
    move-object v0, p1

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->w:Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    .line 21
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/c;->v()Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o:Z

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lb/a/a/a/a/a/a/f/c;->d(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lb/a/a/a/a/a/a/g/b;I)V
    .locals 2

    .line 45
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-nez p1, :cond_0

    return-void

    .line 46
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->s:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a(I)Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a(JZ)V

    return-void
.end method

.method public b(Lb/a/a/a/a/a/a/g/b;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    const/4 p1, 0x1

    .line 12
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    .line 13
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d:Landroid/graphics/SurfaceTexture;

    .line 14
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz p1, :cond_0

    .line 15
    invoke-virtual {p1, p2}, Lb/a/a/a/a/a/b/e/d;->a(Landroid/graphics/SurfaceTexture;)V

    .line 16
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    invoke-virtual {p1, p2}, Lb/a/a/a/a/a/b/e/d;->b(Z)V

    .line 17
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    const-string p2, "surfaceTextureCreated: "

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->q()V

    return-void
.end method

.method public b(Lb/a/a/a/a/a/a/g/b;Landroid/view/SurfaceHolder;)V
    .locals 0

    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    .line 7
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c:Landroid/view/SurfaceHolder;

    .line 8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-nez p1, :cond_0

    return-void

    .line 9
    :cond_0
    invoke-virtual {p1, p2}, Lb/a/a/a/a/a/b/e/d;->a(Landroid/view/SurfaceHolder;)V

    .line 10
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    const-string p2, "surfaceCreated: "

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->q()V

    return-void
.end method

.method public final b(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
    .locals 1

    .line 39
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 40
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f(Z)V

    .line 41
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_0

    .line 42
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->y:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->b(Landroid/view/ViewGroup;)V

    .line 43
    :cond_0
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c(I)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x3

    .line 44
    invoke-interface {p0, p2, p1}, Lb/a/a/a/a/a/a/g/c;->a(ZI)V

    :goto_0
    return-void
.end method

.method public final b(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;ZZ)V
    .locals 0

    .line 24
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    const/4 p2, 0x1

    xor-int/2addr p1, p2

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f(Z)V

    .line 25
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->j:Landroid/content/Context;

    if-nez p1, :cond_0

    return-void

    .line 26
    :cond_0
    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_1

    .line 27
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    const-string p2, "context is not activity, not support this function."

    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 28
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    const/4 p4, 0x0

    if-eqz p1, :cond_3

    if-eqz p3, :cond_2

    const/16 p1, 0x8

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 29
    :goto_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c(I)V

    .line 30
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_4

    .line 31
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->y:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(Landroid/view/ViewGroup;)V

    .line 32
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->c(Z)V

    goto :goto_1

    .line 33
    :cond_3
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c(I)V

    .line 34
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_4

    .line 35
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->y:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->b(Landroid/view/ViewGroup;)V

    .line 36
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->c(Z)V

    .line 37
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->z:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb/a/a/a/a/a/a/g/c$b;

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_6

    .line 38
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    invoke-interface {p1, p2}, Lb/a/a/a/a/a/a/g/c$b;->a(Z)V

    :cond_6
    return-void
.end method

.method protected final b(Lcom/bytedance/sdk/openadsdk/d/g;)V
    .locals 3

    .line 47
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 48
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 49
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->w()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 51
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-static {v1, v2, v0, p1}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Landroid/content/Context;Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;Lcom/bytedance/sdk/openadsdk/d/g;)V

    return-void
.end method

.method protected b(Ljava/lang/Runnable;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->E:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->x:Z

    return v0
.end method

.method public abstract synthetic c()V
.end method

.method public final c(I)V
    .locals 3

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->j:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    const/16 v1, 0x8

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 5
    :goto_1
    instance-of v2, v0, Landroid/app/Activity;

    if-nez v2, :cond_3

    return-void

    .line 6
    :cond_3
    check-cast v0, Landroid/app/Activity;

    .line 7
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    nop

    :goto_2
    const/16 p1, 0x400

    if-nez v1, :cond_4

    .line 8
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1, p1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_3

    .line 9
    :cond_4
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->clearFlags(I)V

    :goto_3
    return-void
.end method

.method public c(J)V
    .locals 2

    .line 1
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->h:J

    .line 2
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    return-void
.end method

.method public abstract synthetic c(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
.end method

.method public c(Z)V
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->v:Z

    return-void
.end method

.method public final d()V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lb/a/a/a/a/a/b/e/d;->w()V

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->u:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->C()V

    :cond_1
    return-void
.end method

.method public d(I)V
    .locals 0

    .line 5
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->F:I

    return-void
.end method

.method public final d(J)V
    .locals 3

    .line 10
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->h:J

    .line 11
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    .line 12
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_0

    .line 13
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->c()V

    .line 14
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz p1, :cond_1

    .line 15
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->h:J

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o:Z

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0, v1, p2}, Lb/a/a/a/a/a/b/e/d;->a(ZJZ)V

    :cond_1
    return-void
.end method

.method public final d(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;Z)V

    return-void
.end method

.method public d(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->n:Z

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->b(Z)V

    :cond_0
    return-void
.end method

.method public abstract synthetic e()V
.end method

.method public final e(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->b(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;ZZ)V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o:Z

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0, p1}, Lb/a/a/a/a/a/b/e/d;->a(Z)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->B:Lcom/bytedance/sdk/openadsdk/core/i0/f;

    if-eqz v0, :cond_2

    .line 5
    invoke-static {}, Lb/a/a/a/a/a/b/d/a;->〇O888o0o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->B:Lcom/bytedance/sdk/openadsdk/core/i0/f;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/i0/f;->a(Z)V

    goto :goto_0

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->b:Lcom/bytedance/sdk/component/utils/y;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$c;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$c;-><init>(Lcom/bytedance/sdk/openadsdk/core/j0/a/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public abstract synthetic f()V
.end method

.method public final f(Lb/a/a/a/a/a/a/g/b;Landroid/view/View;)V
    .locals 0

    .line 2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->t()V

    :cond_0
    const/4 p1, 0x1

    const/4 p2, 0x3

    .line 4
    invoke-interface {p0, p1, p2}, Lb/a/a/a/a/a/a/g/c;->a(ZI)V

    return-void
.end method

.method protected f(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->r:Z

    return-void
.end method

.method public bridge synthetic g()Lb/a/a/a/a/a/a/g/b;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public h()Lb/a/a/a/a/a/a/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isUseTextureView()Z
    .locals 3

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->u0()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-ne v1, v2, :cond_0

    .line 13
    .line 14
    const/16 v1, 0x17

    .line 15
    .line 16
    if-ge v0, v1, :cond_0

    .line 17
    .line 18
    return v2

    .line 19
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/r;->w()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_1

    .line 24
    .line 25
    const/16 v1, 0x1e

    .line 26
    .line 27
    if-lt v0, v1, :cond_1

    .line 28
    .line 29
    return v2

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->g:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/t;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    return v2

    .line 39
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->t()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    return v2

    .line 46
    :cond_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 47
    .line 48
    const-string v1, "Pixel 4"

    .line 49
    .line 50
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-eqz v0, :cond_4

    .line 55
    .line 56
    return v2

    .line 57
    :cond_4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->u()Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    return v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public final j()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lb/a/a/a/a/a/b/e/d;->m()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final k()J
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->o()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->l()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    add-long/2addr v0, v2

    .line 10
    return-wide v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final l()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-wide/16 v0, 0x0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lb/a/a/a/a/a/b/e/d;->q()J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    :goto_0
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final m()I
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->i:J

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->C:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a/a/b/f/a;->〇080(JJ)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public n()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->m:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->h:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public p()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->p:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "execPendingActions: before "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->k:Ljava/util/List;

    .line 9
    .line 10
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->a:Ljava/lang/String;

    .line 18
    .line 19
    const-string v1, "execPendingActions:  exec"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance v0, Ljava/util/ArrayList;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->k:Ljava/util/List;

    .line 27
    .line 28
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    check-cast v1, Ljava/lang/Runnable;

    .line 46
    .line 47
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->k:Ljava/util/List;

    .line 52
    .line 53
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method protected r()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->b:Lcom/bytedance/sdk/component/utils/y;

    .line 2
    .line 3
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$b;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a$b;-><init>(Lcom/bytedance/sdk/openadsdk/core/j0/a/a;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public s()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->n:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public t()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->F:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final u()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->f:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected v()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->x()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d:Landroid/graphics/SurfaceTexture;

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 17
    .line 18
    invoke-virtual {v1}, Lb/a/a/a/a/a/b/e/d;->p()Landroid/graphics/SurfaceTexture;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eq v0, v1, :cond_2

    .line 23
    .line 24
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d:Landroid/graphics/SurfaceTexture;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/b/e/d;->a(Landroid/graphics/SurfaceTexture;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c:Landroid/view/SurfaceHolder;

    .line 33
    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 37
    .line 38
    invoke-virtual {v1}, Lb/a/a/a/a/a/b/e/d;->o()Landroid/view/SurfaceHolder;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    if-eq v0, v1, :cond_2

    .line 43
    .line 44
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->c:Landroid/view/SurfaceHolder;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/b/e/d;->a(Landroid/view/SurfaceHolder;)V

    .line 49
    .line 50
    .line 51
    :cond_2
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public w()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->E:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final y()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lb/a/a/a/a/a/b/e/d;->d()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    return v0
    .line 16
    .line 17
.end method

.method public final z()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->e:Lcom/bytedance/sdk/openadsdk/core/j0/c/d;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lb/a/a/a/a/a/b/e/d;->h()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

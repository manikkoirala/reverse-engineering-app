.class public Lcom/bytedance/sdk/openadsdk/core/c0/f;
.super Ljava/lang/Object;
.source "TouchInfoHelper.java"


# static fields
.field public static m:I = 0x8

.field private static n:F

.field private static o:F

.field private static p:F

.field private static q:F

.field private static r:J


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F

.field public e:J

.field public f:J

.field public g:I

.field public h:I

.field public i:I

.field public j:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->e()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->m:I

    .line 12
    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->n:F

    .line 15
    .line 16
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->o:F

    .line 17
    .line 18
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->p:F

    .line 19
    .line 20
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->q:F

    .line 21
    .line 22
    const-wide/16 v0, 0x0

    .line 23
    .line 24
    sput-wide v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->r:J

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, -0x40800000    # -1.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->a:F

    .line 7
    .line 8
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->b:F

    .line 9
    .line 10
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 11
    .line 12
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 13
    .line 14
    const-wide/16 v0, -0x1

    .line 15
    .line 16
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->e:J

    .line 17
    .line 18
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->f:J

    .line 19
    .line 20
    const/4 v0, -0x1

    .line 21
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->g:I

    .line 22
    .line 23
    const/16 v1, -0x400

    .line 24
    .line 25
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->h:I

    .line 26
    .line 27
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->i:I

    .line 28
    .line 29
    new-instance v0, Landroid/util/SparseArray;

    .line 30
    .line 31
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->j:Landroid/util/SparseArray;

    .line 35
    .line 36
    const/4 v0, 0x0

    .line 37
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->k:I

    .line 38
    .line 39
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->l:I

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->h:I

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->g:I

    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->i:I

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_7

    .line 25
    .line 26
    const/4 v0, 0x3

    .line 27
    const/4 v2, 0x1

    .line 28
    if-eq v1, v2, :cond_5

    .line 29
    .line 30
    const/4 v3, 0x2

    .line 31
    if-eq v1, v3, :cond_1

    .line 32
    .line 33
    if-eq v1, v0, :cond_0

    .line 34
    .line 35
    const/4 v0, -0x1

    .line 36
    const/4 v4, -0x1

    .line 37
    goto/16 :goto_1

    .line 38
    .line 39
    :cond_0
    const/4 v0, 0x4

    .line 40
    const/4 v4, 0x4

    .line 41
    goto/16 :goto_1

    .line 42
    .line 43
    :cond_1
    sget v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->p:F

    .line 44
    .line 45
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/f;->n:F

    .line 50
    .line 51
    sub-float/2addr v1, v4

    .line 52
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    add-float/2addr v0, v1

    .line 57
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->p:F

    .line 58
    .line 59
    sget v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->q:F

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    sget v4, Lcom/bytedance/sdk/openadsdk/core/c0/f;->o:F

    .line 66
    .line 67
    sub-float/2addr v1, v4

    .line 68
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    add-float/2addr v0, v1

    .line 73
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->q:F

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->n:F

    .line 80
    .line 81
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    sput v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->o:F

    .line 86
    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 88
    .line 89
    .line 90
    move-result-wide v0

    .line 91
    sget-wide v4, Lcom/bytedance/sdk/openadsdk/core/c0/f;->r:J

    .line 92
    .line 93
    sub-long/2addr v0, v4

    .line 94
    const-wide/16 v4, 0xc8

    .line 95
    .line 96
    cmp-long v6, v0, v4

    .line 97
    .line 98
    if-lez v6, :cond_3

    .line 99
    .line 100
    sget v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->p:F

    .line 101
    .line 102
    sget v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->m:I

    .line 103
    .line 104
    int-to-float v1, v1

    .line 105
    cmpl-float v0, v0, v1

    .line 106
    .line 107
    if-gtz v0, :cond_2

    .line 108
    .line 109
    sget v0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->q:F

    .line 110
    .line 111
    cmpl-float v0, v0, v1

    .line 112
    .line 113
    if-lez v0, :cond_3

    .line 114
    .line 115
    :cond_2
    const/4 v0, 0x1

    .line 116
    goto :goto_0

    .line 117
    :cond_3
    const/4 v0, 0x2

    .line 118
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 129
    .line 130
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 131
    .line 132
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->k:I

    .line 133
    .line 134
    int-to-float v2, v2

    .line 135
    sub-float/2addr v1, v2

    .line 136
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    sget v2, Lcom/bytedance/sdk/openadsdk/core/c0/f;->m:I

    .line 141
    .line 142
    int-to-float v2, v2

    .line 143
    cmpl-float v1, v1, v2

    .line 144
    .line 145
    if-gez v1, :cond_4

    .line 146
    .line 147
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 148
    .line 149
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->l:I

    .line 150
    .line 151
    int-to-float v2, v2

    .line 152
    sub-float/2addr v1, v2

    .line 153
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 154
    .line 155
    .line 156
    :cond_4
    move v4, v0

    .line 157
    goto :goto_1

    .line 158
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 159
    .line 160
    .line 161
    move-result v1

    .line 162
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 163
    .line 164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 165
    .line 166
    .line 167
    move-result v1

    .line 168
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 169
    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 171
    .line 172
    .line 173
    move-result-wide v1

    .line 174
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->f:J

    .line 175
    .line 176
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->c:F

    .line 177
    .line 178
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->k:I

    .line 179
    .line 180
    int-to-float v2, v2

    .line 181
    sub-float/2addr v1, v2

    .line 182
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    sget v2, Lcom/bytedance/sdk/openadsdk/core/c0/f;->m:I

    .line 187
    .line 188
    int-to-float v2, v2

    .line 189
    cmpl-float v1, v1, v2

    .line 190
    .line 191
    if-gez v1, :cond_6

    .line 192
    .line 193
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->d:F

    .line 194
    .line 195
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->l:I

    .line 196
    .line 197
    int-to-float v2, v2

    .line 198
    sub-float/2addr v1, v2

    .line 199
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 200
    .line 201
    .line 202
    :cond_6
    const/4 v4, 0x3

    .line 203
    goto :goto_1

    .line 204
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 205
    .line 206
    .line 207
    move-result v1

    .line 208
    float-to-int v1, v1

    .line 209
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->k:I

    .line 210
    .line 211
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 212
    .line 213
    .line 214
    move-result v1

    .line 215
    float-to-int v1, v1

    .line 216
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->l:I

    .line 217
    .line 218
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->a:F

    .line 223
    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 225
    .line 226
    .line 227
    move-result v1

    .line 228
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->b:F

    .line 229
    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 231
    .line 232
    .line 233
    move-result-wide v1

    .line 234
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->e:J

    .line 235
    .line 236
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    .line 237
    .line 238
    .line 239
    move-result v1

    .line 240
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->g:I

    .line 241
    .line 242
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    .line 243
    .line 244
    .line 245
    move-result v1

    .line 246
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->h:I

    .line 247
    .line 248
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    .line 249
    .line 250
    .line 251
    move-result v1

    .line 252
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->i:I

    .line 253
    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 255
    .line 256
    .line 257
    move-result-wide v1

    .line 258
    sput-wide v1, Lcom/bytedance/sdk/openadsdk/core/c0/f;->r:J

    .line 259
    .line 260
    const/4 v4, 0x0

    .line 261
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/c0/f;->j:Landroid/util/SparseArray;

    .line 262
    .line 263
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 264
    .line 265
    .line 266
    move-result v1

    .line 267
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;

    .line 268
    .line 269
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    .line 270
    .line 271
    .line 272
    move-result v3

    .line 273
    float-to-double v5, v3

    .line 274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    .line 275
    .line 276
    .line 277
    move-result p1

    .line 278
    float-to-double v7, p1

    .line 279
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 280
    .line 281
    .line 282
    move-result-wide v9

    .line 283
    move-object v3, v2

    .line 284
    invoke-direct/range {v3 .. v10}, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;-><init>(IDDJ)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 288
    .line 289
    .line 290
    return-void
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.class public Lcom/bytedance/sdk/openadsdk/core/f0/o;
.super Ljava/lang/Object;
.source "LandingPageModel.java"


# instance fields
.field private A:Z

.field B:Landroid/animation/ObjectAnimator;

.field C:Landroid/animation/ValueAnimator;

.field D:Landroid/animation/ObjectAnimator;

.field E:Landroid/animation/ObjectAnimator;

.field F:Lb/a/a/a/a/a/a/g/c$a;

.field G:Lcom/bytedance/sdk/openadsdk/core/c0/a;

.field H:Lcom/bytedance/sdk/openadsdk/core/c0/b;

.field private final I:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private J:Z

.field private K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

.field private L:Ljava/lang/String;

.field private M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

.field private volatile N:I

.field private volatile O:I

.field private volatile P:I

.field private Q:I

.field private R:I

.field a:Landroid/widget/ImageView;

.field b:Landroid/widget/FrameLayout;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/FrameLayout;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/RelativeLayout;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/bytedance/sdk/openadsdk/core/x;

.field private m:Lcom/bytedance/sdk/component/widget/SSWebView;

.field private n:Landroid/widget/FrameLayout;

.field private o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/ImageView;

.field private r:Landroid/view/View;

.field private s:J

.field private final t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final u:Landroid/app/Activity;

.field final v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field w:Landroid/widget/FrameLayout;

.field private x:Ljava/lang/String;

.field private y:Lb/c/a/a/a/a/c;

.field private z:Lcom/bytedance/sdk/openadsdk/d/j;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Landroid/widget/FrameLayout;)V
    .locals 15

    .line 1
    move-object v8, p0

    .line 2
    move-object/from16 v0, p1

    .line 3
    .line 4
    move-object/from16 v9, p2

    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 10
    .line 11
    const/4 v10, 0x0

    .line 12
    invoke-direct {v1, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 13
    .line 14
    .line 15
    iput-object v1, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 16
    .line 17
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 18
    .line 19
    invoke-direct {v1, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 20
    .line 21
    .line 22
    iput-object v1, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 23
    .line 24
    iput v10, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->N:I

    .line 25
    .line 26
    iput v10, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->O:I

    .line 27
    .line 28
    iput v10, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->P:I

    .line 29
    .line 30
    iput-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    .line 31
    .line 32
    iput-object v9, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 33
    .line 34
    move-object/from16 v1, p3

    .line 35
    .line 36
    iput-object v1, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 37
    .line 38
    if-eqz v9, :cond_0

    .line 39
    .line 40
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->X()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    iput-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    .line 45
    .line 46
    :cond_0
    iget-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    .line 47
    .line 48
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    const/4 v11, 0x2

    .line 53
    if-nez v2, :cond_2

    .line 54
    .line 55
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/b;->c()Lcom/bytedance/sdk/openadsdk/f/b;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/f/b;->a()Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    iput-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    .line 64
    .line 65
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/b;->c()Lcom/bytedance/sdk/openadsdk/f/b;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    iget-object v3, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    .line 70
    .line 71
    iget-object v4, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    .line 72
    .line 73
    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/f/b;->a(Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;Ljava/lang/String;)I

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    iput v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->Q:I

    .line 78
    .line 79
    if-lez v2, :cond_1

    .line 80
    .line 81
    const/4 v2, 0x2

    .line 82
    goto :goto_0

    .line 83
    :cond_1
    const/4 v2, 0x0

    .line 84
    :goto_0
    iput v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->R:I

    .line 85
    .line 86
    :cond_2
    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 87
    .line 88
    .line 89
    move-result v12

    .line 90
    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    if-eqz v2, :cond_3

    .line 95
    .line 96
    const-string v2, "landingpage_split_screen"

    .line 97
    .line 98
    iput-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_3
    if-eqz v12, :cond_4

    .line 102
    .line 103
    const-string v2, "landingpage_direct"

    .line 104
    .line 105
    iput-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 106
    .line 107
    :cond_4
    :goto_1
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/c0/a;

    .line 108
    .line 109
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 110
    .line 111
    .line 112
    move-result-object v3

    .line 113
    iget-object v4, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 114
    .line 115
    invoke-static/range {p3 .. p3}, Lcom/bytedance/sdk/openadsdk/utils/a0;->f(Ljava/lang/String;)I

    .line 116
    .line 117
    .line 118
    move-result v5

    .line 119
    invoke-direct {v2, v3, v9, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/c0/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;I)V

    .line 120
    .line 121
    .line 122
    iput-object v2, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->G:Lcom/bytedance/sdk/openadsdk/core/c0/a;

    .line 123
    .line 124
    const v2, 0x1020002

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 128
    .line 129
    .line 130
    move-result-object v13

    .line 131
    iget-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->G:Lcom/bytedance/sdk/openadsdk/core/c0/a;

    .line 132
    .line 133
    invoke-virtual {v0, v13}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->b(Landroid/view/View;)V

    .line 134
    .line 135
    .line 136
    new-instance v14, Lcom/bytedance/sdk/openadsdk/core/f0/o$g;

    .line 137
    .line 138
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    iget-object v4, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 143
    .line 144
    invoke-static/range {p3 .. p3}, Lcom/bytedance/sdk/openadsdk/utils/a0;->f(Ljava/lang/String;)I

    .line 145
    .line 146
    .line 147
    move-result v5

    .line 148
    const/4 v6, 0x1

    .line 149
    move-object v0, v14

    .line 150
    move-object v1, p0

    .line 151
    move-object/from16 v3, p2

    .line 152
    .line 153
    move-object/from16 v7, p2

    .line 154
    .line 155
    invoke-direct/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/core/f0/o$g;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;IZLcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 156
    .line 157
    .line 158
    iput-object v14, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->H:Lcom/bytedance/sdk/openadsdk/core/c0/b;

    .line 159
    .line 160
    invoke-virtual {v14, v13}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->b(Landroid/view/View;)V

    .line 161
    .line 162
    .line 163
    move-object/from16 v0, p4

    .line 164
    .line 165
    iput-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->w:Landroid/widget/FrameLayout;

    .line 166
    .line 167
    if-eqz v12, :cond_5

    .line 168
    .line 169
    :try_start_0
    new-array v0, v11, [I

    .line 170
    .line 171
    aput v10, v0, v10

    .line 172
    .line 173
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/p;->c()J

    .line 178
    .line 179
    .line 180
    move-result-wide v1

    .line 181
    const-wide/16 v3, 0x3e8

    .line 182
    .line 183
    mul-long v1, v1, v3

    .line 184
    .line 185
    long-to-int v2, v1

    .line 186
    const/4 v1, 0x1

    .line 187
    aput v2, v0, v1

    .line 188
    .line 189
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    iput-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    .line 194
    .line 195
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    .line 196
    .line 197
    .line 198
    move-result-object v1

    .line 199
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/p;->c()J

    .line 200
    .line 201
    .line 202
    move-result-wide v1

    .line 203
    long-to-float v1, v1

    .line 204
    sget v2, Lcom/bytedance/sdk/openadsdk/core/l;->f:F

    .line 205
    .line 206
    div-float/2addr v1, v2

    .line 207
    const/high16 v2, 0x447a0000    # 1000.0f

    .line 208
    .line 209
    mul-float v1, v1, v2

    .line 210
    .line 211
    float-to-long v1, v1

    .line 212
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 213
    .line 214
    .line 215
    iget-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    .line 216
    .line 217
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    .line 218
    .line 219
    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 220
    .line 221
    .line 222
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 223
    .line 224
    .line 225
    iget-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    .line 226
    .line 227
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/o$h;

    .line 228
    .line 229
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$h;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 233
    .line 234
    .line 235
    iget-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    .line 236
    .line 237
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/o$i;

    .line 238
    .line 239
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$i;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    .line 240
    .line 241
    .line 242
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 243
    .line 244
    .line 245
    iget-object v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    .line 246
    .line 247
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    .line 249
    .line 250
    :catch_0
    :cond_5
    return-void
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/f0/o;J)J
    .locals 0

    .line 3
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->s:J

    return-wide p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->i()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 7
    fill-array-data v0, :array_0

    const-string v1, "timeVisible"

    invoke-static {p0, v1, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->E:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x64

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->E:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/o$b;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$b;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->E:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_1
    :goto_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/f0/o;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->J:Z

    return p1
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 2

    if-eqz p0, :cond_1

    .line 12
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->v()I

    move-result v0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    .line 13
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->v()I

    move-result p0

    const/16 v0, 0x14

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private b()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;->a()V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;->f()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->J:Z

    return p0
.end method

.method public static b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 5
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 6
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e0()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->v()I

    move-result v1

    const/4 v3, 0x5

    if-ne v1, v3, :cond_3

    .line 8
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 9
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    move-result v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    move-result p0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float p0, p0, v1

    if-nez p0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l()V

    return-void
.end method

.method public static c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->v()I

    move-result p0

    const/16 v0, 0x13

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    return-object p0
.end method

.method public static d(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 1

    .line 2
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    return-object p0
.end method

.method private e()V
    .locals 11

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->b(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Landroid/webkit/WebView;)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/j;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v4

    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/f0/o$k;

    invoke-direct {v5, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$k;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->R:I

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/d/j;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/d/i;I)V

    .line 6
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Z)Lcom/bytedance/sdk/openadsdk/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 7
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/d/j;->b(Ljava/lang/String;)V

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g()V

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLandingPage(Z)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setTag(Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->L()Lcom/bytedance/sdk/component/widget/b/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setMaterialMeta(Lcom/bytedance/sdk/component/widget/b/a;)V

    .line 12
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/f0/o$l;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 13
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    const/4 v10, 0x1

    move-object v4, v0

    move-object v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/bytedance/sdk/openadsdk/core/f0/o$l;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/x;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/d/j;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    .line 14
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v3, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 16
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->e(Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/f0/o$m;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    invoke-direct {v3, p0, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/f0/o$m;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;Lcom/bytedance/sdk/openadsdk/core/x;Lcom/bytedance/sdk/openadsdk/d/j;)V

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->y:Lb/c/a/a/a/a/c;

    if-nez v0, :cond_1

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lb/c/a/a/a/a/d;->〇080(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)Lb/c/a/a/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->y:Lb/c/a/a/a/a/c;

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/f0/o$n;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$n;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 21
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v3

    const/16 v4, 0x1523

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/k;->a(Landroid/webkit/WebView;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setUserAgentString(Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setMixedContentMode(I)V

    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/o$o;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$o;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 24
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->H:Lcom/bytedance/sdk/openadsdk/core/c0/b;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->R:I

    invoke-static {v0, v1, v3}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;I)V

    .line 26
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->H0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/n;->a(Lcom/bytedance/sdk/component/widget/SSWebView;Ljava/lang/String;)V

    .line 27
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->A:Z

    .line 28
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_3

    .line 29
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    if-eqz v0, :cond_3

    .line 30
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;->o()V

    :cond_3
    return-void
.end method

.method public static e(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 2

    if-eqz p0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->f0()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 32
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    return-object p0
.end method

.method private f()V
    .locals 5

    .line 2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->i()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->q:Landroid/widget/ImageView;

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    const-string v4, "translationY"

    invoke-static {v0, v4, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v3, 0x1f4

    .line 5
    invoke-virtual {v0, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->B:Landroid/animation/ObjectAnimator;

    .line 6
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->B:Landroid/animation/ObjectAnimator;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->B:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/f0/o$c;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$c;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->H:Lcom/bytedance/sdk/openadsdk/core/c0/b;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->w:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 16
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a:Landroid/widget/ImageView;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/f0/o$d;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$d;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 18
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/f0/n;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/n/c;->b()Lcom/bytedance/sdk/openadsdk/n/c;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/f0/n;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/n/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/n;Landroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 20
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/f0/n;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v0

    .line 21
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/d;->a()Lb/b/a/a/h/n;

    move-result-object v2

    invoke-interface {v2, v0}, Lb/b/a/a/h/n;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 22
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/f0/n;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->e()I

    move-result v3

    invoke-interface {v2, v3}, Lb/b/a/a/h/j;->b(I)Lb/b/a/a/h/j;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 23
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->d0()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/f0/n;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->b()I

    move-result v1

    invoke-interface {v2, v1}, Lb/b/a/a/h/j;->a(I)Lb/b/a/a/h/j;

    move-result-object v1

    .line 24
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v2}, Lb/b/a/a/h/j;->d(I)Lb/b/a/a/h/j;

    move-result-object v1

    .line 25
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v2}, Lb/b/a/a/h/j;->c(I)Lb/b/a/a/h/j;

    move-result-object v1

    sget-object v2, Lb/b/a/a/h/u;->b:Lb/b/a/a/h/u;

    .line 26
    invoke-interface {v1, v2}, Lb/b/a/a/h/j;->oO80(Lb/b/a/a/h/u;)Lb/b/a/a/h/j;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/f0/o$f;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$f;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-interface {v1, v2}, Lb/b/a/a/h/j;->〇o00〇〇Oo(Lb/b/a/a/h/h;)Lb/b/a/a/h/j;

    move-result-object v1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/h/b;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/f0/o$e;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$e;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    invoke-direct {v2, v3, v0, v4}, Lcom/bytedance/sdk/openadsdk/h/b;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lb/b/a/a/h/o;)V

    .line 27
    invoke-interface {v1, v2}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :array_0
    .array-data 4
        0x41800000    # 16.0f
        0x0
    .end array-data
.end method

.method public static f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e0()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 29
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->v()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 30
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 31
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->j0()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 32
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    move-result v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    move-result p0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float p0, p0, v1

    if-nez p0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    return-object p0
.end method

.method private g()V
    .locals 2

    .line 2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 5
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->j(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 6
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    const/4 v1, -0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(I)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 8
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->L0()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->c(I)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 9
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 10
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->D()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 11
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/o$a;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$a;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    .line 12
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/widget/b;)Lcom/bytedance/sdk/openadsdk/core/x;

    return-void
.end method

.method public static g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 13
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->Y()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lb/c/a/a/a/a/c;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->y:Lb/c/a/a/a/a/c;

    return-object p0
.end method

.method public static h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 44
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 45
    :cond_0
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lcom/bytedance/sdk/component/widget/SSWebView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Lcom/bytedance/sdk/openadsdk/d/j;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    return-object p0
.end method

.method private j()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    return v0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Landroid/widget/FrameLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->n:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    return-object p0
.end method

.method private l()V
    .locals 7

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->s:J

    sub-long/2addr v3, v5

    .line 6
    invoke-static {v0, v2, v3, v4, v1}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;JZ)V

    .line 7
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->j()Z

    move-result p0

    return p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/core/f0/o;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/core/f0/o;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->s:J

    return-wide v0
.end method

.method private p()V
    .locals 8

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b()V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    instance-of v2, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    if-eqz v2, :cond_1

    .line 6
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;->g()V

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;->n()V

    .line 9
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xd

    .line 11
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v3, 0xa

    .line 12
    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 13
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 15
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/n/c;->b()Lcom/bytedance/sdk/openadsdk/n/c;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->e()I

    move-result v4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->b()I

    move-result v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->j:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/n/c;->a(Ljava/lang/String;IILandroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 16
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->F0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 19
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c()V

    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 21
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->G:Lcom/bytedance/sdk/openadsdk/core/c0/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->G:Lcom/bytedance/sdk/openadsdk/core/c0/a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_4
    return-void
.end method

.method static synthetic q(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic r(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->Q:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic s(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->P:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic t(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->P:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x1

    .line 4
    .line 5
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->P:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic u(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic v(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->O:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x1

    .line 4
    .line 5
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->O:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic w(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->N:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic x(Lcom/bytedance/sdk/openadsdk/core/f0/o;)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->N:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x1

    .line 4
    .line 5
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->N:I

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public a(F)V
    .locals 0

    .line 11
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/j0/c/c;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/c$a;)V
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->F:Lb/a/a/a/a/a/a/g/c$a;

    return-void
.end method

.method protected c()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->n:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 11

    .line 2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->t:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/16 v2, 0x8

    if-eqz v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->l()V

    goto :goto_0

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 7
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->s:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->n:Landroid/widget/FrameLayout;

    .line 8
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->x:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    .line 9
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->u:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->p:Landroid/view/View;

    .line 10
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->v:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->q:Landroid/widget/ImageView;

    .line 11
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->E:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->r:Landroid/view/View;

    .line 12
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->q:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b:Landroid/widget/FrameLayout;

    .line 13
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->r:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a:Landroid/widget/ImageView;

    .line 14
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->w:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f:Landroid/widget/RelativeLayout;

    .line 15
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->s1:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c:Landroid/widget/TextView;

    .line 16
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->j:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->d:Landroid/widget/FrameLayout;

    .line 17
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->y:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g:Landroid/view/View;

    if-nez v1, :cond_1

    .line 18
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->G:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->g:Landroid/view/View;

    .line 19
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->A:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->h:Landroid/widget/TextView;

    .line 20
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->B:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->i:Landroid/widget/TextView;

    .line 21
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->z:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->j:Lcom/bytedance/sdk/openadsdk/core/widget/TTRoundRectImageView;

    .line 22
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->C:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->k:Landroid/widget/TextView;

    .line 23
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 24
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/p;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->u:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->D:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->e:Landroid/widget/TextView;

    .line 26
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 27
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 28
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    :cond_4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->c()Landroid/os/Handler;

    move-result-object v1

    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/f0/o$j;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o$j;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/o;)V

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 30
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/p;->b()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v4, v4, v6

    .line 31
    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 32
    :cond_5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->e()V

    .line 33
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 34
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f()V

    .line 35
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->i()Z

    move-result v1

    if-nez v1, :cond_6

    .line 36
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const v3, 0x40151eb8    # 2.33f

    .line 37
    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 38
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    :cond_6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->r:Landroid/view/View;

    if-eqz v1, :cond_7

    .line 40
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 41
    :cond_7
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    if-eqz v1, :cond_8

    .line 42
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)V

    .line 43
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v5, v1, v3

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->x:Ljava/lang/String;

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    invoke-static/range {v5 .. v10}, Lcom/bytedance/sdk/openadsdk/d/c$o;->a(JLcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;Ljava/lang/String;)V

    return-void
.end method

.method public i()Z
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c0()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c0()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public k()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->D:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    .line 9
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->D:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 11
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->E:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_3

    .line 12
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->E:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 14
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->o:Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;

    if-eqz v0, :cond_4

    .line 15
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/common/LandingPageLoadingLayout;->n()V

    .line 16
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->B:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_5

    .line 17
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 18
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_6

    .line 19
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/a0;->a(Landroid/webkit/WebView;)V

    :cond_6
    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->m:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 21
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_7

    .line 22
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->o()V

    .line 23
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    if-eqz v0, :cond_8

    .line 24
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->d()V

    .line 25
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->L:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->A:Z

    if-eqz v0, :cond_9

    .line 26
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->O:I

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->N:I

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->v:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/c$o;->a(IILcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 27
    :cond_9
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/b;->c()Lcom/bytedance/sdk/openadsdk/f/b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->M:Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/b;->a(Lcom/bykv/vk/openvk/preload/falconx/loader/ILoader;)V

    return-void
.end method

.method public m()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->p()V

    :cond_0
    return-void
.end method

.method public n()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->l:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->q()V

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->e()V

    :cond_1
    return-void
.end method

.method public o()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/f0/o;->z:Lcom/bytedance/sdk/openadsdk/d/j;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->f()V

    :cond_0
    return-void
.end method

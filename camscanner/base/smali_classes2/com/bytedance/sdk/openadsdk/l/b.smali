.class public Lcom/bytedance/sdk/openadsdk/l/b;
.super Ljava/lang/Object;
.source "StatsLogManager.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/l/b;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static a(Ljava/io/File;)J
    .locals 6

    .line 17
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0

    .line 19
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    .line 20
    array-length v0, p0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    aget-object v4, p0, v3

    .line 21
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method public static a()Lcom/bytedance/sdk/openadsdk/l/b;
    .locals 2

    .line 2
    sget-object v0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/b;

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lcom/bytedance/sdk/openadsdk/l/b;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/b;

    if-nez v1, :cond_0

    .line 5
    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/b;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/l/b;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/b;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 7
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/b;

    return-object v0
.end method

.method public static a(JJLjava/lang/String;I)V
    .locals 11

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    return-void

    .line 27
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v3, v0, p0

    sub-long v5, v0, p2

    sub-long v7, p2, p0

    .line 28
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$f;

    move-object v2, v0

    move-object v9, p4

    move/from16 v10, p5

    invoke-direct/range {v2 .. v10}, Lcom/bytedance/sdk/openadsdk/l/b$f;-><init>(JJJLjava/lang/String;I)V

    const-string v1, "ad_show_cost_time"

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/l/c/c;)V
    .locals 2

    .line 29
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$g;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/l/b$g;-><init>(Lcom/bytedance/sdk/openadsdk/l/c/c;)V

    const-string p0, "request_monitor"

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V
    .locals 8

    .line 22
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->o(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "type="

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v4, 0x2

    const-string v5, "  "

    aput-object v5, v1, v4

    .line 23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x3

    aput-object v4, v1, v5

    const-string v4, "StatsLogManager"

    invoke-static {v4, v1}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_4

    if-eqz v0, :cond_4

    if-nez p2, :cond_0

    goto :goto_2

    :cond_0
    const/16 p0, 0x64

    if-ne v0, p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    if-nez p0, :cond_2

    .line 25
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v4, v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    double-to-int p0, v4

    if-gt p0, v0, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    move v3, p0

    :cond_3
    :goto_1
    if-eqz v3, :cond_4

    .line 26
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->b()Lcom/bytedance/sdk/openadsdk/l/e/a;

    move-result-object p0

    invoke-interface {p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/l/e/a;->a(Lcom/bytedance/sdk/openadsdk/l/a;Z)V

    :cond_4
    :goto_2
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/l/c/b;)Z
    .locals 0

    .line 1
    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static b()V
    .locals 3

    .line 5
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$d;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/l/b$d;-><init>()V

    const-string v1, "disk_log"

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 6
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/c/b;->o()Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object v0

    .line 8
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object p0

    .line 9
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/c/b;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object p0

    .line 10
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->b()Lcom/bytedance/sdk/openadsdk/l/e/a;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$e;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/l/b$e;-><init>(Lcom/bytedance/sdk/openadsdk/l/c/b;)V

    const/4 p0, 0x0

    invoke-interface {p1, v0, p0}, Lcom/bytedance/sdk/openadsdk/l/e/a;->a(Lcom/bytedance/sdk/openadsdk/l/a;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception p0

    .line 11
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string p1, "StatsLogManager"

    invoke-static {p1, p0}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public a(JJ)V
    .locals 9

    sub-long v6, p3, p1

    .line 10
    new-instance v8, Lcom/bytedance/sdk/openadsdk/l/b$n;

    move-object v0, v8

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/l/b$n;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;JJJ)V

    const-string p1, "general_label"

    const/4 p2, 0x0

    invoke-static {p1, p2, v8}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 8
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$k;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$k;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Ljava/lang/String;)V

    const-string p1, "click_playable_test_tool"

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .line 9
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$m;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/l/b$m;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Ljava/lang/String;ILjava/lang/String;)V

    const-string p1, "use_playable_test_tool_error"

    const/4 p2, 0x0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 12
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 13
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/l/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$o;

    invoke-direct {v0, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/l/b$o;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Ljava/lang/String;Lorg/json/JSONObject;)V

    const/4 p2, 0x0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "device_ad_mediation_platform"

    .line 14
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 15
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 16
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$c;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$c;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Lorg/json/JSONObject;)V

    const-string p1, "ad_revenue"

    const/4 v1, 0x1

    invoke-static {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/l/c/b;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Lcom/bytedance/sdk/openadsdk/l/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "show_backup_endcard"

    .line 2
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->b()Lcom/bytedance/sdk/openadsdk/l/e/a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/b$j;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$j;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/c/b;)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/e/a;->a(Lcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .line 4
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$l;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$l;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Ljava/lang/String;)V

    const-string p1, "close_playable_test_tool"

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public c()V
    .locals 3

    .line 4
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b$b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/l/b$b;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;)V

    const-string v1, "blind_mode_status"

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/l/c/b;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Lcom/bytedance/sdk/openadsdk/l/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "express_ad_render"

    .line 2
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->b()Lcom/bytedance/sdk/openadsdk/l/e/a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/b$h;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$h;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/c/b;)V

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/e/a;->a(Lcom/bytedance/sdk/openadsdk/l/a;)V

    return-void
.end method

.method public d(Lcom/bytedance/sdk/openadsdk/l/c/b;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Lcom/bytedance/sdk/openadsdk/l/c/b;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const-string v0, "tpl_update_fail"

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->b()Lcom/bytedance/sdk/openadsdk/l/e/a;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    new-instance v1, Lcom/bytedance/sdk/openadsdk/l/b$i;

    .line 18
    .line 19
    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/l/b$i;-><init>(Lcom/bytedance/sdk/openadsdk/l/b;Lcom/bytedance/sdk/openadsdk/l/c/b;)V

    .line 20
    .line 21
    .line 22
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/e/a;->a(Lcom/bytedance/sdk/openadsdk/l/a;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
.end method

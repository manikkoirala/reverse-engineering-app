.class public Lcom/bytedance/sdk/openadsdk/n/d/a;
.super Ljava/lang/Object;
.source "GifLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/n/d/a$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private a(Lb/b/a/a/h/k;)I
    .locals 1

    .line 26
    invoke-interface {p1}, Lb/b/a/a/h/k;->b()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "image_size"

    .line 27
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 28
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 29
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method protected a()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    throw v0
.end method

.method protected a(ILjava/lang/String;Ljava/lang/Throwable;Lcom/bytedance/sdk/openadsdk/n/d/a$c;)V
    .locals 0

    if-eqz p4, :cond_0

    .line 30
    invoke-interface {p4, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/n/d/a$c;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method protected a(Lb/b/a/a/h/k;Lcom/bytedance/sdk/openadsdk/n/d/a$c;)V
    .locals 4

    if-eqz p2, :cond_3

    .line 16
    invoke-interface {p1}, Lb/b/a/a/h/k;->c()Ljava/lang/Object;

    move-result-object v0

    .line 17
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/n/d/a;->a(Lb/b/a/a/h/k;)I

    move-result v1

    .line 18
    instance-of v2, v0, [B

    if-eqz v2, :cond_0

    .line 19
    invoke-interface {p1}, Lb/b/a/a/h/k;->a()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/n/d/b;

    check-cast v0, [B

    invoke-direct {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/n/d/b;-><init>([BI)V

    invoke-interface {p2, p1, v2}, Lcom/bytedance/sdk/openadsdk/n/d/a$c;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/n/d/b;)V

    goto :goto_0

    .line 20
    :cond_0
    instance-of v2, v0, Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    .line 21
    invoke-interface {p1}, Lb/b/a/a/h/k;->e()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 22
    invoke-interface {p1}, Lb/b/a/a/h/k;->e()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/graphics/Bitmap;

    .line 23
    :cond_1
    invoke-interface {p1}, Lb/b/a/a/h/k;->a()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/n/d/b;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0, v3, v1}, Lcom/bytedance/sdk/openadsdk/n/d/b;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)V

    invoke-interface {p2, p1, v2}, Lcom/bytedance/sdk/openadsdk/n/d/a$c;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/n/d/b;)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    const-string v0, "not bitmap or gif result!"

    .line 24
    invoke-interface {p2, p1, v0, v3}, Lcom/bytedance/sdk/openadsdk/n/d/a$c;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    :goto_0
    if-eqz p2, :cond_4

    .line 25
    invoke-interface {p2}, Lcom/bytedance/sdk/openadsdk/n/d/a$c;->a()V

    :cond_4
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/n/a;Lcom/bytedance/sdk/openadsdk/n/d/a$c;IILandroid/widget/ImageView$ScaleType;Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    .line 2
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/n/a;->a:Ljava/lang/String;

    .line 3
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/h/d;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v0

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/n/a;->b:Ljava/lang/String;

    .line 4
    invoke-interface {v0, v1}, Lb/b/a/a/h/j;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v0

    .line 5
    invoke-interface {v0, p3}, Lb/b/a/a/h/j;->b(I)Lb/b/a/a/h/j;

    move-result-object p3

    .line 6
    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->a(I)Lb/b/a/a/h/j;

    move-result-object p3

    .line 7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p4

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result p4

    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->d(I)Lb/b/a/a/h/j;

    move-result-object p3

    .line 8
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p4

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result p4

    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->c(I)Lb/b/a/a/h/j;

    move-result-object p3

    .line 9
    invoke-interface {p3, p6}, Lb/b/a/a/h/j;->b(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object p3

    sget-object p4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 10
    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->〇〇888(Landroid/graphics/Bitmap$Config;)Lb/b/a/a/h/j;

    move-result-object p3

    .line 11
    invoke-interface {p3, p5}, Lb/b/a/a/h/j;->o〇0(Landroid/widget/ImageView$ScaleType;)Lb/b/a/a/h/j;

    move-result-object p3

    .line 12
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    xor-int/lit8 p4, p4, 0x1

    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->a(Z)Lb/b/a/a/h/j;

    move-result-object p3

    new-instance p4, Lcom/bytedance/sdk/openadsdk/n/d/a$b;

    invoke-direct {p4, p0, p7}, Lcom/bytedance/sdk/openadsdk/n/d/a$b;-><init>(Lcom/bytedance/sdk/openadsdk/n/d/a;I)V

    .line 13
    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->〇o00〇〇Oo(Lb/b/a/a/h/h;)Lb/b/a/a/h/j;

    move-result-object p3

    new-instance p4, Lcom/bytedance/sdk/openadsdk/h/b;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/n/a;->a:Ljava/lang/String;

    new-instance p5, Lcom/bytedance/sdk/openadsdk/n/d/a$a;

    invoke-direct {p5, p0, p2}, Lcom/bytedance/sdk/openadsdk/n/d/a$a;-><init>(Lcom/bytedance/sdk/openadsdk/n/d/a;Lcom/bytedance/sdk/openadsdk/n/d/a$c;)V

    invoke-direct {p4, p8, p1, p5}, Lcom/bytedance/sdk/openadsdk/h/b;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lb/b/a/a/h/o;)V

    .line 14
    invoke-interface {p3, p4}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;

    .line 15
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/n/d/a;->a()V

    return-void
.end method

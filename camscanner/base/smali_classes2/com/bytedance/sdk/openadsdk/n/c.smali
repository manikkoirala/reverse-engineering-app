.class public Lcom/bytedance/sdk/openadsdk/n/c;
.super Ljava/lang/Object;
.source "TTNetClient.java"


# static fields
.field private static volatile d:Lcom/bytedance/sdk/openadsdk/n/c;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lb/b/a/a/j/a;

.field private c:Lcom/bytedance/sdk/openadsdk/n/d/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    :goto_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/n/c;->a:Landroid/content/Context;

    .line 16
    .line 17
    new-instance p1, Lb/b/a/a/j/a$b;

    .line 18
    .line 19
    invoke-direct {p1}, Lb/b/a/a/j/a$b;-><init>()V

    .line 20
    .line 21
    .line 22
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 23
    .line 24
    const-wide/16 v1, 0x2710

    .line 25
    .line 26
    invoke-virtual {p1, v1, v2, v0}, Lb/b/a/a/j/a$b;->〇o00〇〇Oo(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/j/a$b;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1, v1, v2, v0}, Lb/b/a/a/j/a$b;->Oo08(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/j/a$b;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-virtual {p1, v1, v2, v0}, Lb/b/a/a/j/a$b;->o〇0(JLjava/util/concurrent/TimeUnit;)Lb/b/a/a/j/a$b;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    const/4 v0, 0x1

    .line 39
    invoke-virtual {p1, v0}, Lb/b/a/a/j/a$b;->〇o〇(Z)Lb/b/a/a/j/a$b;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lb/b/a/a/j/a$b;->O8()Lb/b/a/a/j/a;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/n/c;->b:Lb/b/a/a/j/a;

    .line 48
    .line 49
    invoke-virtual {p1}, Lb/b/a/a/j/a;->o〇0()Lb/b/a/a/f/a/j;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lb/b/a/a/f/a/j;->〇o00〇〇Oo()Lb/b/a/a/f/a/d;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    if-eqz p1, :cond_1

    .line 58
    .line 59
    const/16 v0, 0x20

    .line 60
    .line 61
    invoke-virtual {p1, v0}, Lb/b/a/a/f/a/d;->〇o00〇〇Oo(I)V

    .line 62
    .line 63
    .line 64
    :cond_1
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private a()V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/n/c;->c:Lcom/bytedance/sdk/openadsdk/n/d/c;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/bytedance/sdk/openadsdk/n/d/c;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/n/d/c;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/n/c;->c:Lcom/bytedance/sdk/openadsdk/n/d/c;

    :cond_0
    return-void
.end method

.method public static b()Lcom/bytedance/sdk/openadsdk/n/c;
    .locals 3

    .line 1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/n/c;->d:Lcom/bytedance/sdk/openadsdk/n/c;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    const-class v0, Lcom/bytedance/sdk/openadsdk/n/c;

    .line 6
    .line 7
    monitor-enter v0

    .line 8
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/n/c;->d:Lcom/bytedance/sdk/openadsdk/n/c;

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    new-instance v1, Lcom/bytedance/sdk/openadsdk/n/c;

    .line 13
    .line 14
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/n/c;-><init>(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    sput-object v1, Lcom/bytedance/sdk/openadsdk/n/c;->d:Lcom/bytedance/sdk/openadsdk/n/c;

    .line 22
    .line 23
    :cond_0
    monitor-exit v0

    .line 24
    goto :goto_0

    .line 25
    :catchall_0
    move-exception v1

    .line 26
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    throw v1

    .line 28
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/n/c;->d:Lcom/bytedance/sdk/openadsdk/n/c;

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a(ILandroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 1

    .line 5
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/h/d;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v0

    invoke-interface {v0, p1}, Lb/b/a/a/h/j;->b(I)Lb/b/a/a/h/j;

    move-result-object v0

    invoke-interface {v0, p1}, Lb/b/a/a/h/j;->a(I)Lb/b/a/a/h/j;

    move-result-object p1

    .line 6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result v0

    invoke-interface {p1, v0}, Lb/b/a/a/h/j;->d(I)Lb/b/a/a/h/j;

    move-result-object p1

    .line 7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result v0

    invoke-interface {p1, v0}, Lb/b/a/a/h/j;->c(I)Lb/b/a/a/h/j;

    move-result-object p1

    sget-object v0, Lb/b/a/a/h/u;->b:Lb/b/a/a/h/u;

    .line 8
    invoke-interface {p1, v0}, Lb/b/a/a/h/j;->oO80(Lb/b/a/a/h/u;)Lb/b/a/a/h/j;

    move-result-object p1

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->E()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0, p2}, Lcom/bytedance/sdk/openadsdk/h/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Landroid/widget/ImageView;)Lb/b/a/a/h/o;

    move-result-object v0

    invoke-interface {p1, v0}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;

    if-eqz p2, :cond_0

    .line 9
    new-instance p1, Lcom/bytedance/sdk/openadsdk/n/c$a;

    invoke-direct {p1, p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/n/c$a;-><init>(Lcom/bytedance/sdk/openadsdk/n/c;Landroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/f0/n;Landroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 10
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 11
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/h/d;->a(Lcom/bytedance/sdk/openadsdk/core/f0/n;)Lb/b/a/a/h/j;

    move-result-object v0

    sget-object v1, Lb/b/a/a/h/u;->b:Lb/b/a/a/h/u;

    invoke-interface {v0, v1}, Lb/b/a/a/h/j;->oO80(Lb/b/a/a/h/u;)Lb/b/a/a/h/j;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/h/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Landroid/widget/ImageView;)Lb/b/a/a/h/o;

    move-result-object p1

    invoke-interface {v0, p1}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;IILandroid/widget/ImageView;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/h/d;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v0

    invoke-interface {v0, p2}, Lb/b/a/a/h/j;->b(I)Lb/b/a/a/h/j;

    move-result-object p2

    invoke-interface {p2, p3}, Lb/b/a/a/h/j;->a(I)Lb/b/a/a/h/j;

    move-result-object p2

    .line 2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result p3

    invoke-interface {p2, p3}, Lb/b/a/a/h/j;->d(I)Lb/b/a/a/h/j;

    move-result-object p2

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result p3

    invoke-interface {p2, p3}, Lb/b/a/a/h/j;->c(I)Lb/b/a/a/h/j;

    move-result-object p2

    sget-object p3, Lb/b/a/a/h/u;->b:Lb/b/a/a/h/u;

    .line 4
    invoke-interface {p2, p3}, Lb/b/a/a/h/j;->oO80(Lb/b/a/a/h/u;)Lb/b/a/a/h/j;

    move-result-object p2

    invoke-static {p5, p1, p4}, Lcom/bytedance/sdk/openadsdk/h/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Landroid/widget/ImageView;)Lb/b/a/a/h/o;

    move-result-object p1

    invoke-interface {p2, p1}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;

    return-void
.end method

.method public c()Lcom/bytedance/sdk/openadsdk/n/d/c;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/n/c;->a()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/n/c;->c:Lcom/bytedance/sdk/openadsdk/n/d/c;

    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public d()Lb/b/a/a/j/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/n/c;->b:Lb/b/a/a/j/a;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

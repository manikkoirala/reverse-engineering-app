.class public Lcom/bytedance/sdk/openadsdk/b/m/q/a;
.super Ljava/lang/Object;
.source "BaseRewardFullAdType.java"


# instance fields
.field protected a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field protected b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field protected c:I

.field protected d:I

.field protected final e:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

.field protected final f:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

.field protected final g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

.field protected h:Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

.field protected i:Lcom/bytedance/sdk/component/utils/y;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 9
    .line 10
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->l:I

    .line 11
    .line 12
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->c:I

    .line 13
    .line 14
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->m:I

    .line 15
    .line 16
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->d:I

    .line 17
    .line 18
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 21
    .line 22
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 23
    .line 24
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->f:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->m()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 30
    .line 31
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/component/utils/y;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->h:Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

    .line 2
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    return-void
.end method

.method public a(Z)V
    .locals 5

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->X0()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Z)V

    .line 7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->c(I)V

    .line 8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    return-void

    :cond_1
    if-nez p1, :cond_2

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Z)V

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b(I)V

    goto :goto_1

    .line 13
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->g1()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Z)V

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 16
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->c()Z

    move-result v0

    if-nez v0, :cond_6

    instance-of v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/g;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    .line 17
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d()V

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b(I)V

    goto :goto_1

    .line 19
    :cond_6
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->g:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    :goto_1
    if-eqz p1, :cond_9

    .line 20
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    if-nez v0, :cond_8

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->k:F

    sget v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;->c0:F

    cmpl-float p1, p1, v0

    if-nez p1, :cond_7

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->c()Z

    move-result p1

    if-nez p1, :cond_8

    .line 21
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->c(I)V

    .line 22
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    goto :goto_2

    .line 23
    :cond_8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->c(I)V

    .line 24
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    goto :goto_2

    .line 25
    :cond_9
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->c(I)V

    .line 26
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    :goto_2
    return-void
.end method

.method public a()Z
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e()Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v2, 0x4

    .line 29
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return v1

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method protected b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils;->d()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    iput-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 23
    .line 24
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 25
    .line 26
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public c()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->k1()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 12
    .line 13
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c0()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/16 v1, 0xf

    .line 20
    .line 21
    if-eq v0, v1, :cond_1

    .line 22
    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 24
    .line 25
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c0()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v1, 0x5

    .line 32
    if-eq v0, v1, :cond_1

    .line 33
    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 35
    .line 36
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c0()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    const/16 v1, 0x32

    .line 43
    .line 44
    if-ne v0, v1, :cond_0

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 48
    goto :goto_1

    .line 49
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 50
    :goto_1
    return v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

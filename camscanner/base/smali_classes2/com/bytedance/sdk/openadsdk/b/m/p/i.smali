.class public Lcom/bytedance/sdk/openadsdk/b/m/p/i;
.super Ljava/lang/Object;
.source "RewardFullScreenManager.java"


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private b:Z

.field protected c:I


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b:Z

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->c:I

    .line 9
    .line 10
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->d()V

    .line 13
    .line 14
    .line 15
    :try_start_0
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 16
    .line 17
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b()F

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->c:I

    .line 26
    .line 27
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 28
    .line 29
    invoke-virtual {v1, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 30
    .line 31
    .line 32
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 33
    .line 34
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const v1, 0x1000080

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 42
    .line 43
    .line 44
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    .line 45
    .line 46
    const/4 v1, 0x2

    .line 47
    if-eq v0, v1, :cond_0

    .line 48
    .line 49
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 50
    .line 51
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/app/Activity;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-nez v0, :cond_1

    .line 56
    .line 57
    :cond_0
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    const/16 v0, 0x400

    .line 64
    .line 65
    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .line 67
    .line 68
    :catchall_0
    :cond_1
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private a()F
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Landroid/content/Context;)I

    move-result v0

    .line 53
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/i;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    return-object p0
.end method

.method private b()F
    .locals 2

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->d(Landroid/content/Context;)I

    move-result v0

    .line 6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private b(I)[F
    .locals 7

    .line 2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a()F

    move-result v0

    .line 3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b()F

    move-result v1

    .line 4
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    cmpl-float v6, v0, v1

    if-lez v6, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    if-eq v5, v6, :cond_2

    add-float/2addr v0, v1

    sub-float v1, v0, v1

    sub-float/2addr v0, v1

    :cond_2
    int-to-float p1, p1

    if-ne v2, v4, :cond_3

    sub-float/2addr v0, p1

    goto :goto_2

    :cond_3
    sub-float/2addr v1, p1

    :goto_2
    const/4 p1, 0x2

    new-array p1, p1, [F

    aput v1, p1, v3

    aput v0, p1, v4

    return-object p1
.end method

.method private d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->k:F

    .line 10
    .line 11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 12
    .line 13
    const/16 v1, 0x1a

    .line 14
    .line 15
    if-ne v1, v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 18
    .line 19
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    if-ne v0, v1, :cond_0

    .line 33
    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 35
    .line 36
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 40
    .line 41
    const/4 v1, 0x2

    .line 42
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->j0()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    .line 54
    .line 55
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    .line 4
    .line 5
    const/4 v2, 0x2

    .line 6
    if-ne v1, v2, :cond_1

    .line 7
    .line 8
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->n:Z

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 13
    .line 14
    const/16 v1, 0x8

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/component/utils/y;)V
    .locals 4

    .line 31
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a0()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    .line 33
    :cond_2
    new-instance v3, Lcom/bytedance/sdk/openadsdk/b/m/p/i$b;

    invoke-direct {v3, p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/i;ZZ)V

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 34
    :cond_3
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Z)V
    .locals 9

    .line 2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_1

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_0

    .line 3
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    goto :goto_0

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->e()V

    .line 5
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b()F

    move-result v0

    .line 6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a()F

    move-result v1

    .line 7
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1

    .line 10
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 12
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 13
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b()F

    move-result v4

    invoke-static {v1, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    move-result v4

    .line 14
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    if-eq v5, v3, :cond_3

    .line 15
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_4

    int-to-float v4, v4

    sub-float/2addr v0, v4

    goto :goto_2

    .line 16
    :cond_3
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_4

    int-to-float v4, v4

    sub-float/2addr v2, v4

    :cond_4
    :goto_2
    if-eqz p1, :cond_5

    .line 17
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    float-to-int v1, v2

    iput v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->l:I

    float-to-int v0, v0

    .line 18
    iput v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->m:I

    return-void

    .line 19
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v4, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x42c80000    # 100.0f

    const/16 v7, 0x14

    const/4 v8, 0x0

    if-eq v4, v3, :cond_6

    .line 20
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->k:F

    cmpl-float v3, p1, v8

    if-eqz v3, :cond_7

    cmpl-float v3, p1, v6

    if-eqz v3, :cond_7

    int-to-float v3, v7

    sub-float v4, v2, v3

    sub-float/2addr v4, v3

    div-float/2addr v4, p1

    sub-float p1, v0, v4

    div-float/2addr p1, v5

    .line 21
    invoke-static {p1, v8}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    move v4, p1

    move v7, v4

    const/16 p1, 0x14

    const/16 v3, 0x14

    goto :goto_3

    .line 22
    :cond_6
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->k:F

    cmpl-float v3, p1, v8

    if-eqz v3, :cond_7

    cmpl-float v3, p1, v6

    if-eqz v3, :cond_7

    int-to-float v3, v7

    sub-float v4, v0, v3

    sub-float/2addr v4, v3

    mul-float v4, v4, p1

    sub-float p1, v2, v4

    div-float/2addr p1, v5

    .line 23
    invoke-static {p1, v8}, Ljava/lang/Math;->max(FF)F

    move-result p1

    float-to-int p1, p1

    move v3, p1

    const/16 v4, 0x14

    goto :goto_3

    :cond_7
    const/4 v7, 0x0

    const/4 p1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 24
    :goto_3
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    int-to-float p1, p1

    sub-float/2addr v2, p1

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->l:I

    int-to-float v2, v7

    sub-float/2addr v0, v2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    float-to-int v0, v0

    .line 25
    iput v0, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->m:I

    .line 26
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v0

    .line 27
    invoke-static {v1, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v2

    .line 28
    invoke-static {v1, p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result p1

    .line 29
    invoke-static {v1, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v3

    .line 30
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, v0, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public a(I)[F
    .locals 8

    const/4 v0, 0x2

    new-array v1, v0, [F

    .line 35
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 36
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 37
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/4 v5, 0x0

    aput v4, v1, v5

    .line 38
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v4, v3

    int-to-float v3, v4

    const/4 v4, 0x1

    aput v3, v1, v4

    aget v3, v1, v5

    .line 39
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    aput v3, v1, v5

    aget v3, v1, v4

    .line 40
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    aput v3, v1, v4

    aget v6, v1, v5

    const/high16 v7, 0x41200000    # 10.0f

    cmpg-float v6, v6, v7

    if-ltz v6, :cond_0

    cmpg-float v3, v3, v7

    if-gez v3, :cond_1

    :cond_0
    const-string v1, "TTAD.RFSM"

    const-string v3, "get root view size error, so run backup"

    .line 41
    invoke-static {v1, v3}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->c:I

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->b(I)[F

    move-result-object v1

    .line 43
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x1a

    if-eq v3, v6, :cond_5

    const/16 v6, 0x1b

    if-ne v3, v6, :cond_2

    goto :goto_1

    .line 44
    :cond_2
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 45
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_3

    const/4 v2, 0x2

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    :goto_0
    if-eq v2, p1, :cond_5

    if-ne p1, v0, :cond_4

    .line 46
    aget p1, v1, v5

    aget v0, v1, v4

    cmpg-float v2, p1, v0

    if-gez v2, :cond_5

    .line 47
    aput p1, v1, v4

    .line 48
    aput v0, v1, v5

    goto :goto_1

    .line 49
    :cond_4
    aget p1, v1, v5

    aget v0, v1, v4

    cmpl-float v2, p1, v0

    if-lez v2, :cond_5

    .line 50
    aput p1, v1, v4

    .line 51
    aput v0, v1, v5

    :cond_5
    :goto_1
    return-object v1
.end method

.method public b(Lcom/bytedance/sdk/component/utils/y;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/i$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/i;)V

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/app/Activity;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/i$c;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/i;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

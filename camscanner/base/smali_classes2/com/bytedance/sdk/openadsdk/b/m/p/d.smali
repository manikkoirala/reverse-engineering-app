.class public Lcom/bytedance/sdk/openadsdk/b/m/p/d;
.super Ljava/lang/Object;
.source "RewardFullDownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field private final b:Ljava/lang/String;

.field c:Lb/c/a/a/a/a/c;

.field private d:Z

.field private final e:Lcom/bytedance/sdk/openadsdk/b/m/p/a;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 9
    .line 10
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->b:Ljava/lang/String;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private c()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/m/b;->b()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e0()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, 0x4

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 17
    .line 18
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->W:Landroid/content/Context;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 21
    .line 22
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->b:Ljava/lang/String;

    .line 23
    .line 24
    invoke-static {v0, v1, v2}, Lb/c/a/a/a/a/d;->〇080(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)Lb/c/a/a/a/a/c;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    .line 29
    .line 30
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    .line 31
    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 35
    .line 36
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 37
    .line 38
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 39
    .line 40
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->b:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v0, v1, v2}, Lb/c/a/a/a/a/d;->〇080(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)Lb/c/a/a/a/a/c;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    .line 47
    .line 48
    :cond_1
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a()Lb/c/a/a/a/a/c;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    return-object v0
.end method

.method public a(Landroid/view/View;FFFFLandroid/util/SparseArray;IIILcom/bytedance/sdk/openadsdk/b/m/p/d$a;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "FFFF",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;III",
            "Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p10

    .line 2
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    if-eqz v2, :cond_3

    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 4
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->e:I

    const/4 v4, 0x0

    if-ne v2, v3, :cond_0

    const-string v2, "click_play_star_level"

    .line 5
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 6
    :cond_0
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->d:I

    if-ne v2, v3, :cond_1

    const-string v2, "click_play_star_nums"

    .line 7
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 8
    :cond_1
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->c:I

    if-ne v2, v3, :cond_2

    const-string v2, "click_play_source"

    .line 9
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 10
    :cond_2
    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->b:I

    if-ne v2, v3, :cond_4

    const-string v2, "click_play_logo"

    .line 11
    invoke-interface {v1, v2, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_0

    :cond_3
    move-object/from16 v1, p10

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    .line 12
    invoke-interface/range {v1 .. v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/d$a;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;III)V

    :cond_4
    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->d:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->d:Z

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->c:Lb/c/a/a/a/a/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/c/a/a/a/a/c;->b()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.class public Lcom/bytedance/sdk/openadsdk/b/m/p/g;
.super Ljava/lang/Object;
.source "RewardFullPlayableManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/k/g;
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final w:Lcom/bytedance/sdk/openadsdk/o/f$a;


# instance fields
.field protected final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile b:Z

.field private final c:Landroid/app/Activity;

.field private final d:Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private final f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private final g:Landroid/os/Handler;

.field h:Z

.field i:Z

.field j:J

.field k:I

.field l:I

.field m:I

.field private n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

.field private o:I

.field private p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

.field private q:Z

.field private r:Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

.field private s:Lcom/bytedance/sdk/openadsdk/o/g;

.field private t:Z

.field private u:Z

.field private final v:Lcom/bytedance/sdk/openadsdk/k/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/g$a;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$a;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->w:Lcom/bytedance/sdk/openadsdk/o/f$a;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    new-instance v0, Landroid/os/Handler;

    .line 13
    .line 14
    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 18
    .line 19
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->h:Z

    .line 20
    .line 21
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->i:Z

    .line 22
    .line 23
    const-wide/16 v2, 0x0

    .line 24
    .line 25
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->j:J

    .line 26
    .line 27
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k:I

    .line 28
    .line 29
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->l:I

    .line 30
    .line 31
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->m:I

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->o:I

    .line 35
    .line 36
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->q:Z

    .line 37
    .line 38
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/g$h;

    .line 39
    .line 40
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$h;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->v:Lcom/bytedance/sdk/openadsdk/k/d;

    .line 44
    .line 45
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c:Landroid/app/Activity;

    .line 50
    .line 51
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 52
    .line 53
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->e:Ljava/lang/String;

    .line 54
    .line 55
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 56
    .line 57
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->r:Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;->a(Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver$a;)V

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->r:Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/g;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    return-object p0
.end method

.method private c()Ljava/lang/String;
    .locals 14

    .line 2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->L()Ljava/lang/String;

    move-result-object v0

    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPlayableLoadH5Url->loadH5Url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Playable"

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 6
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->f()D

    move-result-wide v3

    .line 7
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->c()I

    move-result v5

    .line 8
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v6

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 9
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b0()Lcom/bytedance/sdk/openadsdk/core/f0/n;

    move-result-object v6

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/f0/n;->d()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    const-string v6, ""

    .line 10
    :goto_0
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v7

    .line 11
    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v8

    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->e()Ljava/lang/String;

    move-result-object v8

    .line 12
    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v9

    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->d()Ljava/lang/String;

    move-result-object v9

    .line 13
    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v10}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h()Lcom/bytedance/sdk/openadsdk/core/f0/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/bytedance/sdk/openadsdk/core/f0/c;->a()Ljava/lang/String;

    move-result-object v10

    .line 14
    iget-object v11, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v11}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->K0()Ljava/lang/String;

    move-result-object v11

    .line 15
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "appname="

    .line 16
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&stars="

    .line 17
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v12, v3, v4}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    const-string v1, "&comments="

    .line 18
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v12, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "&icon="

    .line 19
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&downloading="

    .line 20
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v3, "&id="

    .line 21
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "&packageName="

    .line 22
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "&downloadUrl="

    .line 23
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v9}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "&name="

    .line 24
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "&orientation="

    .line 25
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->o:I

    if-ne v3, v1, :cond_2

    const-string v1, "portrait"

    goto :goto_1

    :cond_2
    const-string v1, "landscape"

    :goto_1
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&apptitle="

    .line 26
    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {v11}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Playable-loadH5Url="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    return-object v0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static d(I)Landroid/os/Message;
    .locals 2

    .line 2
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x320

    .line 3
    iput v1, v0, Landroid/os/Message;->what:I

    .line 4
    iput p0, v0, Landroid/os/Message;->arg1:I

    return-object v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->q:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    return-object p0
.end method

.method private g()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h1()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->p()V

    .line 26
    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    return v0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->n()V

    .line 33
    .line 34
    .line 35
    return v1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c:Landroid/app/Activity;

    .line 2
    .line 3
    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->h1:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->l:I

    return v0
.end method

.method public a(I)V
    .locals 7

    .line 60
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->m(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->m(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/h;->b()Z

    move-result v0

    const-string v1, "TTAD.RFPM"

    const-string v2, " mLastVolume="

    const-string v3, " mVolume="

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_3

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onVolumeChanged by SDK mIsMute="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/k/h;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_2

    .line 64
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 65
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Z)V

    goto :goto_0

    .line 66
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 67
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Z)V

    goto :goto_0

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    const/4 v6, -0x1

    invoke-virtual {v0, v6}, Lcom/bytedance/sdk/openadsdk/k/h;->a(I)V

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onVolumeChanged by User mIsMute="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/k/h;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->e:Z

    if-eqz v1, :cond_5

    if-nez p1, :cond_4

    .line 71
    iput-boolean v4, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 72
    iget-object p1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 73
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Z)V

    goto :goto_0

    .line 74
    :cond_4
    iput-boolean v5, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 75
    iget-object p1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 76
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1, v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Z)V

    :cond_5
    :goto_0
    return-void
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/core/f0/q;Z)V
    .locals 0

    if-nez p2, :cond_0

    return-void

    .line 58
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->r0()I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->l:I

    .line 59
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(Ljava/lang/String;Z)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->m:I

    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 2

    .line 85
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x384

    .line 86
    iput v1, v0, Landroid/os/Message;->what:I

    .line 87
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 88
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public a(Landroid/webkit/DownloadListener;)V
    .locals 10

    .line 38
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 39
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c()Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 41
    :cond_1
    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/m/p/g$f;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e()Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v6

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, v2

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$f;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/x;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/d/j;Z)V

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startWebViewLoading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTAD.RFPM"

    invoke-static {v3, v2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->c(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 44
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDisplayZoomControls(Z)V

    .line 45
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e()Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f()Lcom/bytedance/sdk/openadsdk/d/j;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/c;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lcom/bytedance/sdk/openadsdk/d/j;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 46
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 54
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->getPlayView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/k/e;Z)V
    .locals 6

    const-string v0, "PlayablePlugin_init"

    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    sget-object v1, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->w:Lcom/bytedance/sdk/openadsdk/o/f$a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/o/f;->a(Lcom/bytedance/sdk/openadsdk/o/f$a;)V

    .line 6
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/g$b;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;Lcom/bytedance/sdk/openadsdk/k/e;)V

    .line 7
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/p/g$c;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    .line 8
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "cid"

    const/4 v4, 0x0

    .line 9
    :try_start_0
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "log_extra"

    .line 10
    :try_start_1
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 11
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v5

    invoke-static {v3, v5, p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/o/c;Lcom/bytedance/sdk/openadsdk/o/a;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 12
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->j(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 13
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/common/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 14
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/common/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 15
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->e(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "sdkEdition"

    .line 16
    :try_start_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/common/a;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 17
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/common/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 18
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/common/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 19
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/openadsdk/o/g;->b(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    .line 20
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez p1, :cond_3

    .line 21
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    invoke-static {v0, v4, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 22
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-nez p2, :cond_2

    .line 23
    new-instance p2, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    invoke-static {v0, v4, p2}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    :cond_2
    throw p1

    :catch_0
    nop

    .line 24
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-nez p1, :cond_3

    .line 25
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    invoke-static {v0, v4, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ZLcom/bytedance/sdk/openadsdk/l/a;)V

    .line 26
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->d(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 27
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->d(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/o/g;->i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/o/g;

    .line 28
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz p1, :cond_7

    .line 29
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/o/g;->j()Ljava/util/Set;

    move-result-object p1

    .line 30
    new-instance p2, Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    invoke-direct {p2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 31
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "subscribe_app_ad"

    .line 32
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "adInfo"

    .line 33
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "webview_time_track"

    .line 34
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "download_app_ad"

    .line 35
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_1

    .line 36
    :cond_6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d()Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b()Lb/b/a/a/e/r;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 37
    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/m/p/g$e;

    invoke-direct {v2, p0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$e;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v0, v2}, Lb/b/a/a/e/r;->〇o〇(Ljava/lang/String;Lb/b/a/a/e/e;)Lb/b/a/a/e/r;

    goto :goto_1

    :cond_7
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 4

    if-eqz p1, :cond_0

    .line 49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->j:J

    sub-long/2addr v0, v2

    :try_start_0
    const-string v2, "duration"

    .line 50
    invoke-virtual {p1, v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "TTAD.RFPM"

    const-string v1, "endShow json error"

    .line 51
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->a(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    :cond_0
    return-void
.end method

.method public a(ZLjava/lang/String;I)V
    .locals 1

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/o/g;->a(ZLjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/k/d;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->v:Lcom/bytedance/sdk/openadsdk/k/d;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    add-int/lit8 p1, p1, -0x1

    .line 2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/o/g;->c(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    :cond_0
    return-void
.end method

.method public c(I)I
    .locals 2

    .line 37
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->m:I

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->l:I

    sub-int/2addr v1, p1

    sub-int/2addr v0, v1

    return v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 5

    .line 29
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->i:Z

    if-nez v0, :cond_0

    return-void

    .line 30
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->j:J

    sub-long/2addr v1, v3

    :try_start_0
    const-string v3, "duration"

    .line 32
    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "TTAD.RFPM"

    const-string v3, "sendPlayableEvent error"

    .line 33
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->e:Ljava/lang/String;

    invoke-static {v1, v2, p1, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    const-string v0, "return_foreground"

    .line 35
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 36
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->i:Z

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    .line 38
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 39
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 40
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h1()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 41
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->j(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    const/16 v1, 0x320

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    if-eqz p1, :cond_2

    .line 43
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->q()V

    .line 44
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c(Z)V

    .line 45
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Z)V

    .line 46
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->e:Ljava/lang/String;

    const-string v1, "py_loading_success"

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/c;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_2
    return-void
.end method

.method public d()I
    .locals 1

    .line 6
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k:I

    return v0
.end method

.method public d(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDomStorageEnabled(Z)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->n()V

    :cond_0
    return-void
.end method

.method public e(I)V
    .locals 0

    .line 8
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k:I

    return-void
.end method

.method public e(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g()I

    move-result v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    nop

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 4
    :try_start_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/b;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    :cond_1
    return-void
.end method

.method public f()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->t:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->t:Z

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 4
    iget v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->o:I

    .line 5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->h()V

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils$AudioInfoReceiver;->a(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 8
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->j(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/16 v3, 0x320

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_2
    return-void
.end method

.method public f(I)V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->p:Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    if-eqz v0, :cond_0

    .line 12
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 1

    .line 13
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b:Z

    if-nez p1, :cond_0

    .line 14
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    const/16 v0, 0x384

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 8

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/16 v2, 0x384

    .line 5
    .line 6
    if-ne v0, v2, :cond_6

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b:Z

    .line 9
    .line 10
    if-eqz v0, :cond_5

    .line 11
    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 13
    .line 14
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    goto/16 :goto_2

    .line 23
    .line 24
    :cond_0
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    if-lez p1, :cond_3

    .line 28
    .line 29
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 30
    .line 31
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 32
    .line 33
    invoke-virtual {v3, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 34
    .line 35
    .line 36
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 37
    .line 38
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 39
    .line 40
    invoke-virtual {v3, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c(I)I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-ne v3, p1, :cond_1

    .line 45
    .line 46
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 47
    .line 48
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 49
    .line 50
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    const/4 v4, 0x0

    .line 55
    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    if-lez v3, :cond_2

    .line 60
    .line 61
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 62
    .line 63
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 64
    .line 65
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 70
    .line 71
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 72
    .line 73
    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    const-string v7, "tt_skip_ad_time_text"

    .line 78
    .line 79
    invoke-static {v6, v7}, Lcom/bytedance/sdk/component/utils/t;->k(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    new-array v7, v1, [Ljava/lang/Object;

    .line 84
    .line 85
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    aput-object v3, v7, v0

    .line 90
    .line 91
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v4, v5, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 100
    .line 101
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 102
    .line 103
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 108
    .line 109
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 110
    .line 111
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 112
    .line 113
    .line 114
    move-result-object v4

    .line 115
    const-string v5, "tt_txt_skip"

    .line 116
    .line 117
    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/t;->k(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v4

    .line 121
    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 125
    .line 126
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d(Z)V

    .line 129
    .line 130
    .line 131
    :goto_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    iput v2, v0, Landroid/os/Message;->what:I

    .line 136
    .line 137
    add-int/lit8 v2, p1, -0x1

    .line 138
    .line 139
    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 140
    .line 141
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 142
    .line 143
    const-wide/16 v3, 0x3e8

    .line 144
    .line 145
    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 146
    .line 147
    .line 148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 149
    .line 150
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 151
    .line 152
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b(I)V

    .line 153
    .line 154
    .line 155
    goto :goto_1

    .line 156
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 157
    .line 158
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 159
    .line 160
    .line 161
    move-result p1

    .line 162
    if-eqz p1, :cond_4

    .line 163
    .line 164
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 165
    .line 166
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->f()V

    .line 169
    .line 170
    .line 171
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 172
    .line 173
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 174
    .line 175
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d(Z)V

    .line 176
    .line 177
    .line 178
    goto :goto_1

    .line 179
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 180
    .line 181
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 182
    .line 183
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 184
    .line 185
    .line 186
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 187
    .line 188
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 189
    .line 190
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 191
    .line 192
    .line 193
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 194
    .line 195
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Y:Lcom/bytedance/sdk/openadsdk/b/m/q/b;

    .line 196
    .line 197
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    .line 198
    .line 199
    .line 200
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 201
    .line 202
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 203
    .line 204
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->u()V

    .line 205
    .line 206
    .line 207
    goto :goto_4

    .line 208
    :cond_5
    :goto_2
    return v1

    .line 209
    :cond_6
    const/16 v2, 0x320

    .line 210
    .line 211
    if-ne v0, v2, :cond_8

    .line 212
    .line 213
    new-instance v0, Lorg/json/JSONObject;

    .line 214
    .line 215
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 216
    .line 217
    .line 218
    const-string v3, "remove_loading_page_type"

    .line 219
    .line 220
    :try_start_0
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 221
    .line 222
    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 223
    .line 224
    .line 225
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 226
    .line 227
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 228
    .line 229
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    .line 230
    .line 231
    .line 232
    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    if-eqz p1, :cond_7

    .line 234
    .line 235
    const-string p1, "playable_url"

    .line 236
    .line 237
    :try_start_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 238
    .line 239
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 240
    .line 241
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    .line 242
    .line 243
    .line 244
    move-result-object v3

    .line 245
    invoke-virtual {v3}, Lb/a/a/a/a/a/a/f/b;->〇O8o08O()Ljava/lang/String;

    .line 246
    .line 247
    .line 248
    move-result-object v3

    .line 249
    invoke-virtual {v0, p1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 250
    .line 251
    .line 252
    goto :goto_3

    .line 253
    :catch_0
    move-exception p1

    .line 254
    const-string v3, "TTAD.RFPM"

    .line 255
    .line 256
    const-string v4, "handleMessage json error"

    .line 257
    .line 258
    invoke-static {v3, v4, p1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 259
    .line 260
    .line 261
    :cond_7
    :goto_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 262
    .line 263
    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 264
    .line 265
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 266
    .line 267
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->a:Ljava/lang/String;

    .line 268
    .line 269
    const-string v4, "remove_loading_page"

    .line 270
    .line 271
    invoke-static {v3, p1, v4, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 272
    .line 273
    .line 274
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 275
    .line 276
    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 277
    .line 278
    .line 279
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 280
    .line 281
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 282
    .line 283
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->e()V

    .line 284
    .line 285
    .line 286
    :cond_8
    :goto_4
    return v1
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->h:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public j()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public k()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->u:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->u:Z

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f(Z)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c:Landroid/app/Activity;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/g;->a()V

    .line 27
    .line 28
    .line 29
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils$AudioInfoReceiver;->b(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public l()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/g;->u()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->c(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 15
    .line 16
    const/16 v1, 0x384

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/o/g;->v()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->e(Landroid/view/View;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->s:Lcom/bytedance/sdk/openadsdk/o/g;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/o/g;->c(Z)Lcom/bytedance/sdk/openadsdk/o/g;

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public n()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0x384

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 9
    .line 10
    const/16 v1, 0x258

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
.end method

.method public o()V
    .locals 3

    .line 1
    :try_start_0
    new-instance v0, Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->r:Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

    .line 7
    .line 8
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/g$g;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g$g;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/g;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;->a(Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver$a;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Landroid/content/IntentFilter;

    .line 17
    .line 18
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    .line 19
    .line 20
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->c:Landroid/app/Activity;

    .line 24
    .line 25
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->r:Lcom/bytedance/sdk/component/utils/HomeWatcherReceiver;

    .line 30
    .line 31
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    .line 33
    .line 34
    :catchall_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public p()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->h:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->I()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 17
    .line 18
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->n:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->F()V

    .line 27
    .line 28
    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public r()V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->j:J

    .line 6
    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->g:Landroid/os/Handler;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/16 v2, 0x384

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    invoke-virtual {v0, v2, v1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f(Z)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

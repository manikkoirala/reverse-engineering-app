.class public Lcom/bytedance/sdk/openadsdk/b/m/p/m;
.super Ljava/lang/Object;
.source "RewardFullWebViewManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/k/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/b/m/p/m$o;,
        Lcom/bytedance/sdk/openadsdk/b/m/p/m$q;,
        Lcom/bytedance/sdk/openadsdk/b/m/p/m$p;,
        Lcom/bytedance/sdk/openadsdk/b/m/p/m$r;
    }
.end annotation


# instance fields
.field private A:J

.field private B:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field private D:F

.field private E:F

.field private F:Lcom/bytedance/sdk/openadsdk/common/f;

.field private G:Z

.field private final H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private I:Z

.field private J:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

.field private K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:J

.field private Q:J

.field private R:Ljava/lang/String;

.field private S:I

.field private final a:Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private c:I

.field private final d:Z

.field private e:I

.field private f:I

.field private g:Lcom/bytedance/sdk/component/widget/SSWebView;

.field private h:Lcom/bytedance/sdk/component/widget/SSWebView;

.field i:Lcom/bytedance/sdk/openadsdk/core/x;

.field j:Lcom/bytedance/sdk/openadsdk/core/x;

.field protected k:Z

.field protected l:Ljava/lang/String;

.field m:Lcom/bytedance/sdk/openadsdk/d/j;

.field private n:Z

.field private final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private p:Z

.field private q:Z

.field r:I

.field s:Ljava/lang/String;

.field protected t:Lcom/bytedance/sdk/openadsdk/d/o;

.field u:Z

.field private v:Z

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->k:Z

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n:Z

    .line 9
    .line 10
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 13
    .line 14
    .line 15
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 16
    .line 17
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->r:I

    .line 18
    .line 19
    const-string v2, ""

    .line 20
    .line 21
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s:Ljava/lang/String;

    .line 22
    .line 23
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u:Z

    .line 24
    .line 25
    new-instance v2, Landroid/util/SparseArray;

    .line 26
    .line 27
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->B:Landroid/util/SparseArray;

    .line 31
    .line 32
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->C:Z

    .line 33
    .line 34
    const/high16 v0, -0x40800000    # -1.0f

    .line 35
    .line 36
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->D:F

    .line 37
    .line 38
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E:F

    .line 39
    .line 40
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->I:Z

    .line 41
    .line 42
    const-wide/16 v0, -0x1

    .line 43
    .line 44
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 45
    .line 46
    const/4 v0, -0x1

    .line 47
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->S:I

    .line 48
    .line 49
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 50
    .line 51
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 52
    .line 53
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 54
    .line 55
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 56
    .line 57
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b:Ljava/lang/String;

    .line 58
    .line 59
    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 60
    .line 61
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d:Z

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private J()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 2
    .line 3
    const-string v1, "showPlayableEndCardOverlay"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 10
    .line 11
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    .line 12
    .line 13
    const/16 v1, 0x258

    .line 14
    .line 15
    const-wide/16 v2, 0x3e8

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F
    .locals 0

    .line 6
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->y:F

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;I)I
    .locals 0

    .line 4
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->S:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;J)J
    .locals 0

    .line 7
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->A:J

    return-wide p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/util/SparseArray;)Landroid/util/SparseArray;
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->B:Landroid/util/SparseArray;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .line 8
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->x:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/component/widget/SSWebView;
    .locals 0

    .line 2
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    return-object p0
.end method

.method private static a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;III)Ljava/lang/String;
    .locals 4

    .line 68
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->o()F

    move-result v0

    .line 69
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    const-string v2, "&"

    const-string v3, "?"

    if-ne p2, v1, :cond_1

    .line 70
    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 71
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 72
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 73
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "orientation=portrait"

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 74
    :cond_1
    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 75
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 76
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 77
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "height="

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, "&width="

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, "&aspect_ratio="

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 78
    :cond_3
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 79
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :cond_4
    return-object p0
.end method

.method private a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/b/m/p/m$r;)V
    .locals 11

    .line 82
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 83
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/j;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/j;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/webkit/WebView;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/j;->a(Z)Lcom/bytedance/sdk/openadsdk/d/j;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 84
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p1, "landingpage_endcard"

    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/d/j;->b(Ljava/lang/String;)V

    .line 85
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/p/m$c;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 86
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    move-result v0

    const/4 v10, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v8, 0x1

    :goto_1
    move-object v2, p1

    move-object v3, p0

    move-object v9, p2

    invoke-direct/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/x;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/d/j;ZLcom/bytedance/sdk/openadsdk/b/m/p/m$r;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 88
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 89
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d:Z

    if-eqz v0, :cond_3

    const-string v0, "rewarded_video"

    goto :goto_2

    :cond_3
    const-string v0, "fullscreen_interstitial_ad"

    :goto_2
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->e(Ljava/lang/String;)V

    .line 90
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 91
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object p1

    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    invoke-direct {v0, p0, v2, v3, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$e;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Lcom/bytedance/sdk/openadsdk/core/x;Lcom/bytedance/sdk/openadsdk/d/j;Lcom/bytedance/sdk/openadsdk/b/m/p/m$r;)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 93
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)V

    .line 94
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x18

    if-lt p1, p2, :cond_5

    .line 95
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 p2, 0x0

    invoke-virtual {p1, v1, p2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 96
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 p2, -0x1

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setBackgroundColor(I)V

    .line 97
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p1, v10}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDisplayZoomControls(Z)V

    .line 98
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Ljava/lang/String;)Z
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .line 99
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ".mp4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F
    .locals 0

    .line 3
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->z:F

    return p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->C:Z

    return p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F
    .locals 0

    .line 2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->D:F

    return p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->R:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->I:Z

    return p1
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F
    .locals 0

    .line 2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E:F

    return p1
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n:Z

    return p1
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->C:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->L:Z

    return p1
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/util/SparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->B:Landroid/util/SparseArray;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->O:Z

    return p1
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->y:F

    return p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q:Z

    return p1
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->z:F

    return p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->A:J

    return-wide v0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->D:F

    return p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->I:Z

    return p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E:F

    return p0
.end method

.method private m()Lcom/bytedance/sdk/openadsdk/d/o;
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 3
    :goto_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/d/o;

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d:Z

    if-eqz v2, :cond_1

    const-string v2, "rewarded_video"

    goto :goto_1

    :cond_1
    const-string v2, "fullscreen_interstitial_ad"

    :goto_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-direct {v1, v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/o;-><init>(ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    return-object v1
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->G:Z

    return p0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/common/f;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->F:Lcom/bytedance/sdk/openadsdk/common/f;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n:Z

    return p0
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->x:Landroid/view/View;

    return-object p0
.end method

.method static synthetic q(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->w:Landroid/view/View;

    return-object p0
.end method

.method static synthetic r(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d:Z

    return p0
.end method

.method static synthetic s(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    return-object p0
.end method

.method static synthetic t(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    return-object p0
.end method


# virtual methods
.method public A()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->h()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->f()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public B()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->o()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->o()V

    .line 13
    .line 14
    .line 15
    :cond_1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 16
    .line 17
    const-wide/16 v2, 0x0

    .line 18
    .line 19
    cmp-long v4, v0, v2

    .line 20
    .line 21
    if-lez v4, :cond_2

    .line 22
    .line 23
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->Q:J

    .line 24
    .line 25
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 26
    .line 27
    .line 28
    move-result-wide v4

    .line 29
    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 30
    .line 31
    sub-long/2addr v4, v6

    .line 32
    add-long/2addr v0, v4

    .line 33
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->Q:J

    .line 34
    .line 35
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 36
    .line 37
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 38
    .line 39
    const/4 v1, 0x1

    .line 40
    const/4 v2, 0x0

    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->p()V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 52
    .line 53
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 57
    .line 58
    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 59
    .line 60
    .line 61
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 62
    .line 63
    if-eqz v0, :cond_4

    .line 64
    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 66
    .line 67
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    if-eqz v0, :cond_4

    .line 72
    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->p()V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 84
    .line 85
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 89
    .line 90
    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 91
    .line 92
    .line 93
    :cond_4
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public C()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->p()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->p()V

    .line 13
    .line 14
    .line 15
    :cond_1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 16
    .line 17
    const-wide/16 v2, 0x0

    .line 18
    .line 19
    cmp-long v4, v0, v2

    .line 20
    .line 21
    if-nez v4, :cond_2

    .line 22
    .line 23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 28
    .line 29
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    const/4 v2, 0x0

    .line 33
    if-eqz v0, :cond_4

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->q()V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 39
    .line 40
    if-eqz v0, :cond_4

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-nez v0, :cond_3

    .line 47
    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 54
    .line 55
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 59
    .line 60
    invoke-virtual {p0, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 70
    .line 71
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 75
    .line 76
    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 77
    .line 78
    .line 79
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 80
    .line 81
    if-eqz v0, :cond_6

    .line 82
    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 84
    .line 85
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_6

    .line 90
    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->q()V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 97
    .line 98
    if-eqz v0, :cond_6

    .line 99
    .line 100
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-nez v0, :cond_5

    .line 105
    .line 106
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 112
    .line 113
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 117
    .line 118
    invoke-virtual {p0, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 119
    .line 120
    .line 121
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->L:Z

    .line 122
    .line 123
    if-nez v0, :cond_6

    .line 124
    .line 125
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 126
    .line 127
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 128
    .line 129
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->Q0()Z

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    if-eqz v0, :cond_6

    .line 134
    .line 135
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J()V

    .line 136
    .line 137
    .line 138
    goto :goto_1

    .line 139
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 140
    .line 141
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 142
    .line 143
    .line 144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 145
    .line 146
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 150
    .line 151
    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 152
    .line 153
    .line 154
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 155
    .line 156
    if-eqz v0, :cond_7

    .line 157
    .line 158
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->e()V

    .line 159
    .line 160
    .line 161
    :cond_7
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public D()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->d()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->f()V

    .line 13
    .line 14
    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
.end method

.method public E()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "play.google.com/store"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 20
    .line 21
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->d(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    :cond_1
    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u:Z

    .line 29
    .line 30
    return-void

    .line 31
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 32
    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->k:Z

    .line 36
    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 40
    .line 41
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_3

    .line 46
    .line 47
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 48
    .line 49
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_3

    .line 54
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v1, "&is_pre_render=1"

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-string v2, "preLoadEndCard: "

    .line 80
    .line 81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    const-string v2, "TTAD.RFWVM"

    .line 92
    .line 93
    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 97
    .line 98
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/n;->a(Lcom/bytedance/sdk/component/widget/SSWebView;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    :cond_3
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public F()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/b;->〇O8o08O()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->R:Ljava/lang/String;

    .line 15
    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->R:Ljava/lang/String;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 26
    .line 27
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c:I

    .line 28
    .line 29
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f:I

    .line 30
    .line 31
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e:I

    .line 32
    .line 33
    invoke-static {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;III)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->R:Ljava/lang/String;

    .line 38
    .line 39
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$a;

    .line 40
    .line 41
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 46
    .line 47
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v5

    .line 53
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 54
    .line 55
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    const/4 v8, 0x1

    .line 62
    if-nez v1, :cond_3

    .line 63
    .line 64
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 65
    .line 66
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    if-eqz v1, :cond_2

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_2
    const/4 v1, 0x0

    .line 74
    const/4 v7, 0x0

    .line 75
    goto :goto_1

    .line 76
    :cond_3
    :goto_0
    const/4 v7, 0x1

    .line 77
    :goto_1
    move-object v1, v0

    .line 78
    move-object v2, p0

    .line 79
    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/x;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/d/j;Z)V

    .line 80
    .line 81
    .line 82
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->K:Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    .line 83
    .line 84
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 85
    .line 86
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 90
    .line 91
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/m$b;

    .line 92
    .line 93
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 94
    .line 95
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 96
    .line 97
    invoke-direct {v1, p0, v2, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Lcom/bytedance/sdk/openadsdk/core/x;Lcom/bytedance/sdk/openadsdk/d/j;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 101
    .line 102
    .line 103
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 104
    .line 105
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->R:Ljava/lang/String;

    .line 106
    .line 107
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/n;->a(Lcom/bytedance/sdk/component/widget/SSWebView;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    iput-boolean v8, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q:Z

    .line 111
    .line 112
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public G()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/j;->a(J)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public H()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q:Z

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q:Z

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->J()V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 25
    .line 26
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k()V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    invoke-virtual {p0, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 46
    .line 47
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 51
    .line 52
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->g()V

    .line 58
    .line 59
    .line 60
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q:Z

    .line 61
    .line 62
    if-eqz v0, :cond_1

    .line 63
    .line 64
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 70
    .line 71
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 72
    .line 73
    iget-boolean v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 74
    .line 75
    invoke-virtual {p0, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 79
    .line 80
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 84
    .line 85
    invoke-virtual {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 89
    .line 90
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    .line 91
    .line 92
    const/16 v1, 0x258

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 95
    .line 96
    .line 97
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 98
    .line 99
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 100
    .line 101
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Y:Lcom/bytedance/sdk/openadsdk/b/m/q/b;

    .line 102
    .line 103
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)Z

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    if-nez v0, :cond_2

    .line 108
    .line 109
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 110
    .line 111
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Y:Lcom/bytedance/sdk/openadsdk/b/m/q/b;

    .line 112
    .line 113
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->C()V

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 118
    .line 119
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 120
    .line 121
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->g()V

    .line 122
    .line 123
    .line 124
    :cond_2
    :goto_0
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public I()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 8
    .line 9
    const/16 v1, 0x8

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
.end method

.method public a()V
    .locals 0

    .line 1
    return-void
.end method

.method public a(F)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;F)V

    return-void
.end method

.method public a(I)V
    .locals 2

    .line 119
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->S:I

    const-string v1, "TTAD.RFWVM"

    if-gtz v0, :cond_0

    if-lez p1, :cond_0

    const-string v0, "onVolumeChanged >>>> become unmuted, notify h5"

    .line 120
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 121
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Z)V

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    if-nez p1, :cond_1

    const-string v0, "onVolumeChanged >>>> become mute notification h5"

    .line 122
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 123
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Z)V

    .line 124
    :cond_1
    :goto_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->S:I

    return-void
.end method

.method public a(II)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "width"

    .line 65
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "height"

    .line 66
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 67
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    const-string p2, "resize"

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method public a(Landroid/webkit/DownloadListener;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/component/widget/SSWebView;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->b(Z)Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/b;->a(Landroid/webkit/WebView;)V

    .line 104
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    const/16 v2, 0x1523

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/k;->a(Landroid/webkit/WebView;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->setUserAgentString(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setMixedContentMode(I)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/common/f;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->F:Lcom/bytedance/sdk/openadsdk/common/f;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V
    .locals 2

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b(Z)V

    .line 108
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "viewStatus"

    .line 109
    :try_start_1
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p2, "viewableChange"

    .line 110
    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V
    .locals 2

    .line 112
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "endcard_mute"

    .line 113
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string p2, "endcard_show"

    .line 114
    invoke-virtual {v0, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string p2, "endcard_control_event"

    .line 115
    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Ljava/lang/Boolean;Lcom/bytedance/sdk/openadsdk/k/e;Ljava/lang/String;)V
    .locals 9

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 17
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    const/4 v2, 0x2

    .line 18
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "click_scence"

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    .line 19
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 20
    :cond_0
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m()Lcom/bytedance/sdk/openadsdk/d/o;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 22
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-direct {v1, v4}, Lcom/bytedance/sdk/openadsdk/core/x;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 23
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->D()Ljava/lang/String;

    move-result-object v1

    .line 24
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 25
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 26
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 27
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->j(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    .line 28
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/4 v6, 0x7

    const/4 v7, 0x5

    if-eqz v5, :cond_1

    const/4 v5, 0x7

    goto :goto_1

    :cond_1
    const/4 v5, 0x5

    :goto_1
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->b(I)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    new-instance v5, Lcom/bytedance/sdk/openadsdk/b/m/p/m$p;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {v5, v8}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$p;-><init>(Landroid/view/View;)V

    .line 29
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/a;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    .line 30
    invoke-virtual {v4, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 31
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    .line 32
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n()Z

    move-result v5

    const-string v8, "landingpage_endcard"

    if-eqz v5, :cond_2

    move-object v5, v8

    goto :goto_2

    :cond_2
    move-object v5, p3

    :goto_2
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v4

    .line 33
    invoke-virtual {v4, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/util/Map;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 34
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/d/o;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v0

    new-instance v4, Lcom/bytedance/sdk/openadsdk/b/m/p/m$j;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$j;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    .line 35
    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/widget/b;)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 37
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 38
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    :cond_3
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/x;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 40
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 41
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 42
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 43
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->j(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object v2

    .line 44
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v6, 0x5

    :goto_3
    invoke-virtual {v2, v6}, Lcom/bytedance/sdk/openadsdk/core/x;->b(I)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/m/p/m$p;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$p;-><init>(Landroid/view/View;)V

    .line 45
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/a;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 46
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Lcom/bytedance/sdk/component/widget/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    .line 47
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    .line 48
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    move-object p3, v8

    :cond_5
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    .line 49
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/util/Map;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 50
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/d/o;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    new-instance p3, Lcom/bytedance/sdk/openadsdk/b/m/p/m$l;

    invoke-direct {p3, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$l;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    .line 51
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/widget/b;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    new-instance p3, Lcom/bytedance/sdk/openadsdk/b/m/p/m$k;

    invoke-direct {p3, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$k;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    .line 52
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/x$j;)V

    .line 53
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    new-instance p3, Lcom/bytedance/sdk/openadsdk/b/m/p/m$q;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v1, 0x0

    invoke-direct {p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$q;-><init>(Lcom/bytedance/sdk/component/widget/SSWebView;Lcom/bytedance/sdk/openadsdk/b/m/p/m$f;)V

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/i;)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 54
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    new-instance p3, Lcom/bytedance/sdk/openadsdk/b/m/p/m$q;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$q;-><init>(Lcom/bytedance/sdk/component/widget/SSWebView;Lcom/bytedance/sdk/openadsdk/b/m/p/m$f;)V

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/i;)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 55
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b()Landroid/view/View;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Landroid/view/View;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean p3, p3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->q:Z

    .line 56
    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    .line 57
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/e;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 58
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->b()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/d;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    new-instance p2, Lcom/bytedance/sdk/openadsdk/b/m/p/m$m;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$m;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    .line 59
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/b;)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 60
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->M:Z

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->e(Z)V

    .line 61
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Landroid/view/View;)Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    new-instance p2, Lcom/bytedance/sdk/openadsdk/b/m/p/m$n;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$n;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    .line 62
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/b;)Lcom/bytedance/sdk/openadsdk/core/x;

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V
    .locals 1

    .line 10
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$g;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$g;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/b/m/p/m$r;)V

    .line 11
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 12
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/component/widget/SSWebView;)V

    .line 13
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$h;

    invoke-direct {v0, p0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$h;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Landroid/webkit/DownloadListener;)V

    .line 14
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->q:Z

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d(Z)V

    .line 15
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/p/m$i;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$i;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Landroid/webkit/DownloadListener;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    return-void
.end method

.method public a(ZILjava/lang/String;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 117
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->l()V

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {v0, p2, p3}, Lcom/bytedance/sdk/openadsdk/d/o;->b(ILjava/lang/String;)V

    :goto_0
    return-void
.end method

.method public a(ZZ)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    invoke-virtual {p0, v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/core/x;ZZ)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_0

    .line 6
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S0()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLandingPage(Z)V

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "landingpage_endcard"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setTag(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->L()Lcom/bytedance/sdk/component/widget/b/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setMaterialMeta(Lcom/bytedance/sdk/component/widget/b/a;)V

    :cond_3
    if-nez p1, :cond_4

    .line 11
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 12
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->F()V

    :cond_4
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/x;Z)V
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Z)Lcom/bytedance/sdk/openadsdk/core/x;

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Z)V

    .line 17
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "endcard_mute"

    .line 18
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 19
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    const-string v1, "volumeChange"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method public c()Lcom/bytedance/sdk/component/widget/SSWebView;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/core/x;Z)V

    return-void
.end method

.method public d()Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    return-object v0
.end method

.method public d(Z)V
    .locals 0

    .line 4
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->k:Z

    return-void
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    return-object v0
.end method

.method public f()Lcom/bytedance/sdk/openadsdk/d/j;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    return-object v0
.end method

.method public g()I
    .locals 1

    .line 3
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->r:I

    return v0
.end method

.method public h()Lcom/bytedance/sdk/component/widget/SSWebView;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s:Ljava/lang/String;

    return-object v0
.end method

.method public j()V
    .locals 2

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->v:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->v:Z

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->j:I

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c:I

    .line 5
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->l:I

    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e:I

    .line 6
    iget v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->m:I

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f:I

    .line 7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l()V

    return-void
.end method

.method public k()V
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c:I

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f:I

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    .line 4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    const-string v1, "use_second_endcard=1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->M:Z

    :cond_0
    return-void
.end method

.method l()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->w:Landroid/view/View;

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->G:Z

    const/16 v2, 0x8

    if-eqz v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->F:Lcom/bytedance/sdk/openadsdk/common/f;

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/common/f;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->n:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/widget/SSWebView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->l()V

    goto :goto_0

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 10
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->o:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/component/widget/SSWebView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_2

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->l()V

    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDisplayZoomControls(Z)V

    goto :goto_1

    .line 14
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 15
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_3

    .line 16
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/m$f;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m$f;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 18
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLandingPage(Z)V

    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v1, "landingpage_endcard"

    :goto_2
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setTag(Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView$r0;

    invoke-direct {v1}, Lcom/bytedance/sdk/component/widget/SSWebView$r0;-><init>()V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->L()Lcom/bytedance/sdk/component/widget/b/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setMaterialMeta(Lcom/bytedance/sdk/component/widget/b/a;)V

    :cond_5
    return-void
.end method

.method public n()Z
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    :try_start_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    const-string v2, "show_landingpage"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    return v1
.end method

.method public o()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u:Z

    return v0
.end method

.method public p()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->N:Z

    return v0
.end method

.method public q()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 3
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->m()Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->O:Z

    return v0
.end method

.method public s()Z
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public t()Z
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->N:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public u()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public v()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b:Ljava/lang/String;

    .line 4
    .line 5
    const-string v2, "use_second_endcard"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->N:Z

    .line 13
    .line 14
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 19
    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 21
    .line 22
    const-string v1, "click_endcard_close"

    .line 23
    .line 24
    invoke-virtual {v0, v1, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    .line 27
    :catch_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public w()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/a0;->a(Landroid/webkit/WebView;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/a0;->a(Landroid/webkit/WebView;)V

    .line 21
    .line 22
    .line 23
    :cond_1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->Q:J

    .line 24
    .line 25
    const-wide/16 v2, 0x0

    .line 26
    .line 27
    cmp-long v4, v0, v2

    .line 28
    .line 29
    if-lez v4, :cond_3

    .line 30
    .line 31
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 32
    .line 33
    cmp-long v6, v4, v2

    .line 34
    .line 35
    if-lez v6, :cond_2

    .line 36
    .line 37
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 38
    .line 39
    .line 40
    move-result-wide v2

    .line 41
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->P:J

    .line 42
    .line 43
    sub-long/2addr v2, v4

    .line 44
    add-long/2addr v0, v2

    .line 45
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->Q:J

    .line 46
    .line 47
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 48
    .line 49
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 50
    .line 51
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b:Ljava/lang/String;

    .line 52
    .line 53
    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->Q:J

    .line 54
    .line 55
    const-string v3, "second_endcard_duration"

    .line 56
    .line 57
    const/4 v4, 0x0

    .line 58
    invoke-static/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;J)V

    .line 59
    .line 60
    .line 61
    :cond_3
    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 65
    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 69
    .line 70
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-nez v0, :cond_4

    .line 75
    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 77
    .line 78
    const/4 v1, 0x1

    .line 79
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/d/o;->a(Z)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->q()V

    .line 85
    .line 86
    .line 87
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 88
    .line 89
    if-eqz v0, :cond_5

    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->o()V

    .line 92
    .line 93
    .line 94
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 95
    .line 96
    if-eqz v0, :cond_6

    .line 97
    .line 98
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/x;->o()V

    .line 99
    .line 100
    .line 101
    :cond_6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    .line 102
    .line 103
    if-eqz v0, :cond_7

    .line 104
    .line 105
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/j;->d()V

    .line 106
    .line 107
    .line 108
    :cond_7
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils$AudioInfoReceiver;->b(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public x()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->b()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public y()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->c()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public z()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t:Lcom/bytedance/sdk/openadsdk/d/o;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/d/o;->e()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

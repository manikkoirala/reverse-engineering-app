.class public Lcom/bytedance/sdk/openadsdk/b/m/p/e;
.super Ljava/lang/Object;
.source "RewardFullEndCardManager.java"


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private final b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 7
    .line 8
    invoke-direct {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private a()I
    .locals 6

    .line 82
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->K()I

    move-result v0

    const/4 v1, -0x1

    const/16 v2, 0x1388

    if-gt v0, v2, :cond_4

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v3, 0x3e8

    if-ge v0, v3, :cond_1

    add-int/lit16 v0, v0, 0x3e8

    .line 83
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(I)I

    move-result v4

    if-gt v4, v2, :cond_4

    if-gez v4, :cond_2

    goto :goto_0

    :cond_2
    if-ge v4, v3, :cond_3

    add-int/lit16 v4, v4, 0x3e8

    .line 84
    :cond_3
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method private b()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->m(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 12
    .line 13
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 14
    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e(Z)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 24
    .line 25
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/h;->a(Z)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V

    return-void
.end method

.method public a(ZZZLcom/bytedance/sdk/openadsdk/b/m/q/b;)V
    .locals 4

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showEndCard() called with: isSkip = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "], force = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "], isFromLandingPage = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "], mAdType = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTAD.RFEndM"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1a

    if-nez p4, :cond_0

    goto/16 :goto_4

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->q()V

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    .line 5
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->z:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 6
    :cond_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    const/4 v2, 0x0

    if-nez p2, :cond_5

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-nez p2, :cond_5

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 7
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->z:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->B:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    if-nez p2, :cond_5

    .line 8
    :cond_2
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    return-void

    .line 9
    :cond_3
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_5

    :cond_4
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    if-eqz p2, :cond_5

    .line 10
    invoke-virtual {p2, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 11
    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    return-void

    .line 12
    :cond_5
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p2}, Landroid/app/Activity;->isDestroyed()Z

    move-result p2

    if-eqz p2, :cond_6

    return-void

    .line 13
    :cond_6
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p2}, Landroid/app/Activity;->isFinishing()Z

    move-result p2

    if-eqz p2, :cond_7

    return-void

    .line 14
    :cond_7
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->X0()Z

    move-result p2

    if-eqz p2, :cond_8

    .line 15
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->finish()V

    return-void

    .line 16
    :cond_8
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->D()V

    .line 17
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->y()V

    .line 18
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 19
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 20
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->N:Lcom/bytedance/sdk/openadsdk/b/m/p/c;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/c;->b()V

    .line 21
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    iget-boolean v3, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    if-nez v3, :cond_9

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->g1()Z

    move-result p2

    if-eqz p2, :cond_9

    const/4 p2, 0x1

    goto :goto_0

    :cond_9
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p3, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Z)V

    .line 22
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result p2

    if-eqz p2, :cond_a

    return-void

    .line 23
    :cond_a
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    move-result p2

    if-eqz p2, :cond_b

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_b

    if-eqz p1, :cond_b

    .line 24
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 25
    :cond_b
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b()V

    .line 26
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_c

    return-void

    .line 27
    :cond_c
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 28
    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->e()V

    .line 29
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->k1()Z

    move-result p2

    if-eqz p2, :cond_d

    .line 30
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p2, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    goto :goto_1

    .line 31
    :cond_d
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    invoke-virtual {p3, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 32
    :goto_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    invoke-virtual {p3, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 33
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    move-result p2

    if-eqz p2, :cond_e

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p2

    if-eqz p2, :cond_e

    if-eqz p1, :cond_e

    .line 34
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->b(Z)V

    .line 35
    :cond_e
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->G()V

    .line 36
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->s:Lcom/bytedance/sdk/openadsdk/core/f0/o;

    if-eqz p1, :cond_f

    .line 37
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->d()V

    .line 38
    :cond_f
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->p()V

    .line 39
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-nez p1, :cond_19

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 40
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s()Z

    move-result p1

    if-nez p1, :cond_19

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p2, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 41
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u()Z

    move-result p1

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->i()Z

    move-result p3

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o()Z

    move-result v3

    invoke-static {p2, p1, p3, v3}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;ZZZ)Z

    move-result p1

    if-nez p1, :cond_10

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-nez p1, :cond_10

    goto/16 :goto_3

    .line 42
    :cond_10
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_11

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-nez p1, :cond_11

    const-string p1, "TimeTrackLog report Success from Android"

    .line 43
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1, v0, v2, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(ZILjava/lang/String;)V

    .line 45
    :cond_11
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(F)V

    .line 46
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(F)V

    .line 47
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(I)V

    .line 48
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    const/16 p3, 0x8

    if-eqz p1, :cond_12

    .line 49
    iget-object p1, p4, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m:Landroid/widget/LinearLayout;

    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 50
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b(I)V

    .line 51
    :cond_12
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_17

    .line 52
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->s0()I

    move-result p1

    .line 53
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 54
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->r0()I

    move-result p1

    add-int/2addr p1, v0

    mul-int/lit16 p1, p1, 0x3e8

    .line 55
    :cond_13
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showEndCard1: cT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x1

    if-ne p1, v1, :cond_15

    .line 56
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_14

    .line 57
    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->C()V

    goto :goto_2

    .line 58
    :cond_14
    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    goto :goto_2

    :cond_15
    if-ltz p1, :cond_18

    .line 59
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p4

    if-eqz p4, :cond_16

    .line 60
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    int-to-long v1, p1

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 61
    :cond_16
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    int-to-long v1, p1

    const/16 p1, 0x258

    invoke-virtual {p4, p1, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 62
    :cond_17
    invoke-virtual {p0, p4}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)Z

    .line 63
    :cond_18
    :goto_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    const/16 p4, 0x1f4

    const-wide/16 v1, 0x64

    invoke-virtual {p1, p4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 64
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p4, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    invoke-virtual {p4, p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(ZZ)V

    .line 65
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Z)V

    .line 66
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    .line 67
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->r()V

    .line 68
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c(Z)V

    .line 69
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d()Lcom/bytedance/sdk/openadsdk/core/x;

    move-result-object p1

    const-string p3, "prerender_page_show"

    invoke-virtual {p1, p3, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void

    .line 70
    :cond_19
    :goto_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->g()V

    :cond_1a
    :goto_4
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)Z
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a()I

    move-result v1

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showEndCard: cT2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTAD.RFEndM"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->Z0()Z

    move-result v0

    if-nez v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 75
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->C()V

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    goto :goto_0

    :cond_1
    if-ltz v1, :cond_2

    .line 78
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    const/16 v0, 0x2bc

    .line 79
    iput v0, p1, Landroid/os/Message;->what:I

    .line 80
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    iput v1, p1, Landroid/os/Message;->arg1:I

    .line 81
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    :goto_0
    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->b()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public e()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->c()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public f()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->d()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public g()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "TTAD.RFEndM"

    .line 13
    .line 14
    const-string v2, "TimeTrackLog report 408 from backup page"

    .line 15
    .line 16
    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 20
    .line 21
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 22
    .line 23
    const/16 v2, 0x198

    .line 24
    .line 25
    const-string v3, "end_card_timeout"

    .line 26
    .line 27
    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(ZILjava/lang/String;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 31
    .line 32
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->A()V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 38
    .line 39
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 40
    .line 41
    const/16 v2, 0x8

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 47
    .line 48
    iget-boolean v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    .line 49
    .line 50
    if-eqz v3, :cond_1

    .line 51
    .line 52
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Y:Lcom/bytedance/sdk/openadsdk/b/m/q/b;

    .line 53
    .line 54
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m:Landroid/widget/LinearLayout;

    .line 55
    .line 56
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 60
    .line 61
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b(I)V

    .line 64
    .line 65
    .line 66
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 67
    .line 68
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 69
    .line 70
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e(I)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 74
    .line 75
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->k1()Z

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-eqz v0, :cond_2

    .line 82
    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 84
    .line 85
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 86
    .line 87
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 88
    .line 89
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/l;)Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-nez v0, :cond_3

    .line 94
    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 96
    .line 97
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->finish()V

    .line 100
    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->b:Lcom/bytedance/sdk/openadsdk/component/reward/view/b;

    .line 104
    .line 105
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/b;->e()V

    .line 106
    .line 107
    .line 108
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 109
    .line 110
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 111
    .line 112
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->r()V

    .line 113
    .line 114
    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 116
    .line 117
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Y:Lcom/bytedance/sdk/openadsdk/b/m/q/b;

    .line 118
    .line 119
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)Z

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 123
    .line 124
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 125
    .line 126
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 127
    .line 128
    .line 129
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 130
    .line 131
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->L:Lcom/bytedance/sdk/openadsdk/b/m/p/h;

    .line 132
    .line 133
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 134
    .line 135
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->m()Z

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c(Z)V

    .line 140
    .line 141
    .line 142
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

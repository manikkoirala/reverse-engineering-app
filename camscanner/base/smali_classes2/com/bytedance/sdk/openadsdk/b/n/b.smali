.class public Lcom/bytedance/sdk/openadsdk/b/n/b;
.super Ljava/lang/Object;
.source "TTAppOpenAdTopLayoutHelper.java"


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/b/p/a;

.field private b:Lcom/bytedance/sdk/openadsdk/b/n/a;

.field private c:I

.field private d:F

.field private e:I

.field private f:Landroid/animation/ValueAnimator;

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/p/a;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->c:I

    .line 6
    .line 7
    const/high16 v1, 0x40a00000    # 5.0f

    .line 8
    .line 9
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->d:F

    .line 10
    .line 11
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->e:I

    .line 12
    .line 13
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->a:Lcom/bytedance/sdk/openadsdk/b/p/a;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/n/b;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->c:I

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/n/b;)Lcom/bytedance/sdk/openadsdk/b/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->a:Lcom/bytedance/sdk/openadsdk/b/p/a;

    return-object p0
.end method


# virtual methods
.method public a()Landroid/animation/ValueAnimator;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->f:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public a(F)V
    .locals 1

    .line 10
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->d:F

    const/4 v0, 0x0

    cmpg-float p1, p1, v0

    if-gtz p1, :cond_0

    const/high16 p1, 0x40a00000    # 5.0f

    .line 11
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->d:F

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/n/b;->c()V

    return-void
.end method

.method public a(I)V
    .locals 5

    .line 2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->c:I

    int-to-float v0, p1

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v0, v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 3
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->d:F

    sub-float/2addr v1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-gtz v1, :cond_1

    .line 4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->b:Lcom/bytedance/sdk/openadsdk/b/n/a;

    if-eqz v1, :cond_0

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->g:Z

    if-nez v4, :cond_0

    .line 5
    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/b/n/a;->a()V

    .line 6
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->g:Z

    :cond_0
    const/4 v1, 0x0

    .line 7
    :cond_1
    iget v4, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->e:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 8
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->b:Lcom/bytedance/sdk/openadsdk/b/n/a;

    if-eqz v0, :cond_3

    .line 9
    invoke-interface {v0, v1, p1, v2}, Lcom/bytedance/sdk/openadsdk/b/n/a;->a(IIZ)V

    :cond_3
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/b/n/a;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->b:Lcom/bytedance/sdk/openadsdk/b/n/a;

    return-void
.end method

.method public b()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->e:I

    return v0
.end method

.method public b(I)V
    .locals 0

    .line 3
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->e:I

    return-void
.end method

.method public c()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->d:F

    .line 2
    .line 3
    const/high16 v1, 0x447a0000    # 1000.0f

    .line 4
    .line 5
    mul-float v0, v0, v1

    .line 6
    .line 7
    float-to-int v0, v0

    .line 8
    const/4 v1, 0x2

    .line 9
    new-array v1, v1, [I

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    aput v2, v1, v2

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    aput v0, v1, v2

    .line 16
    .line 17
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->f:Landroid/animation/ValueAnimator;

    .line 22
    .line 23
    int-to-long v2, v0

    .line 24
    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->f:Landroid/animation/ValueAnimator;

    .line 28
    .line 29
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    .line 30
    .line 31
    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/n/b;->f:Landroid/animation/ValueAnimator;

    .line 38
    .line 39
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/n/b$a;

    .line 40
    .line 41
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/n/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/n/b;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.class public Lcom/bytedance/sdk/openadsdk/b/g;
.super Ljava/lang/Object;
.source "TTAppOpenAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/component/utils/y$a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/openadsdk/core/p<",
            "Lcom/bytedance/sdk/openadsdk/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/bytedance/sdk/openadsdk/b/f;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:I

.field private f:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private g:Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

.field private h:I

.field private volatile i:I

.field private final j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    .line 13
    .line 14
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->i:I

    .line 15
    .line 16
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/f0/v;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/v;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->a:Landroid/content/Context;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->a:Landroid/content/Context;

    .line 37
    .line 38
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->c()Lcom/bytedance/sdk/openadsdk/core/p;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->b:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 43
    .line 44
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->a:Landroid/content/Context;

    .line 45
    .line 46
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/f;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->i:I

    return p1
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/g;
    .locals 1

    .line 7
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/g;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/g;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;)Lcom/bytedance/sdk/openadsdk/core/f0/v;
    .locals 0

    .line 3
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    return-object p0
.end method

.method private a()V
    .locals 2

    .line 22
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/g$b;

    const-string v1, "tryGetAppOpenAdFromCache"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/b/g$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/g;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/y;->c(Lb/b/a/a/k/g;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/b/l/b;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/l/b;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/g;Z)V
    .locals 0

    .line 6
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Z)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/b/l/b;)V
    .locals 10

    .line 28
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->e()I

    move-result v0

    .line 29
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->c()I

    move-result v1

    .line 30
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/g;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    const/16 v3, 0x64

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    if-ne v0, v4, :cond_0

    if-ne v1, v3, :cond_0

    .line 31
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/l/a;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/l/a;-><init>(ILcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 33
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Lcom/bytedance/sdk/openadsdk/b/l/a;)V

    .line 34
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->k:Z

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    invoke-static {p1, v4, v0}, Lcom/bytedance/sdk/openadsdk/b/k/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;ILcom/bytedance/sdk/openadsdk/core/f0/v;)V

    :cond_0
    return-void

    :cond_1
    if-ne v0, v4, :cond_6

    .line 36
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->g:Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    const/4 v2, 0x0

    const/16 v5, 0x65

    if-eqz v0, :cond_3

    .line 37
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/d;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/g;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v7

    if-ne v1, v5, :cond_2

    const/4 v8, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    :goto_0
    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/bytedance/sdk/openadsdk/b/d;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;ZLcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 38
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/g;->g:Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    invoke-interface {v6, v0}, Lcom/bytedance/sdk/openadsdk/api/PAGLoadListener;->onAdLoaded(Ljava/lang/Object;)V

    .line 39
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-ne v1, v5, :cond_4

    .line 40
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/v;->h()Lcom/bytedance/sdk/openadsdk/utils/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/utils/z;->d()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/k/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;J)V

    goto :goto_1

    :cond_4
    if-ne v1, v3, :cond_9

    .line 41
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/k/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;ILcom/bytedance/sdk/openadsdk/core/f0/v;)V

    .line 42
    iput-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/b/g;->k:Z

    .line 43
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/core/f0/v;->i:Z

    if-nez v0, :cond_9

    .line 44
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->p(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 45
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/f;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    goto :goto_1

    .line 46
    :cond_5
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/l/a;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->d()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/b/l/a;-><init>(ILcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 47
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Lcom/bytedance/sdk/openadsdk/b/l/a;)V

    goto :goto_1

    :cond_6
    const/4 v1, 0x2

    const/4 v2, 0x3

    if-eq v0, v1, :cond_7

    if-ne v0, v2, :cond_9

    .line 48
    :cond_7
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->g:Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    if-eqz v1, :cond_8

    .line 49
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->a()I

    move-result v3

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/l/b;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v3, p1}, Lcom/bytedance/sdk/openadsdk/api/PAGLoadListener;->onError(ILjava/lang/String;)V

    .line 50
    :cond_8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-ne v0, v2, :cond_9

    .line 51
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->i:I

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->h:I

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/k/a;->a(II)V

    :cond_9
    :goto_1
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 3
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/f0/q;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 27
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/g$d;

    invoke-direct {v2, p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/core/f0/v;Lcom/bytedance/sdk/openadsdk/b/f$j;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 3
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/f0/q;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 26
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/g$c;

    invoke-direct {v2, p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/f;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/f0/v;Lcom/bytedance/sdk/openadsdk/b/f$k;)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 23
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/f;->c(I)V

    .line 24
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->p(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 25
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/g;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    return p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 4
    .param p1    # Lcom/bytedance/sdk/openadsdk/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->i:I

    .line 3
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/f0/s;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/s;-><init>()V

    .line 4
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    iput-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/f0/s;->h:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    .line 5
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/f0/s;->d:I

    const/4 v0, 0x2

    .line 6
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/f0/s;->f:I

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->b:Lcom/bytedance/sdk/openadsdk/core/p;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/b/g$a;

    invoke-direct {v2, p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    const/4 v3, 0x3

    invoke-interface {v0, p1, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/f0/s;ILcom/bytedance/sdk/openadsdk/core/p$a;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/g;)Lcom/bytedance/sdk/openadsdk/b/f;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->c:Lcom/bytedance/sdk/openadsdk/b/f;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/b/g;)Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;)I
    .locals 0
    .param p1    # Lcom/bytedance/sdk/openadsdk/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 52
    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(Landroid/os/Message;)V
    .locals 4

    .line 53
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 54
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 55
    :cond_0
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/l/b;

    const/16 v0, 0x2712

    .line 56
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/16 v3, 0x66

    invoke-direct {p1, v2, v3, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/l/b;-><init>(IIILjava/lang/String;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/l/b;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/common/b;I)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-nez p2, :cond_0

    return-void

    :cond_0
    if-gtz p3, :cond_1

    const-string p3, "TTAppOpenAdLoadManager"

    const-string v0, "Since the timeout value passed by loadAppOpenAd is <=0, now it is set to the default value of 3500ms"

    .line 8
    invoke-static {p3, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p3, 0xdac

    .line 9
    :cond_1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getBidAdm()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    iput-boolean p1, v0, Lcom/bytedance/sdk/openadsdk/core/f0/v;->i:Z

    .line 11
    instance-of p1, p2, Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    if-eqz p1, :cond_2

    .line 12
    check-cast p2, Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/g;->g:Lcom/bytedance/sdk/openadsdk/api/open/PAGAppOpenAdLoadListener;

    .line 13
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->e:I

    .line 14
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/b/g;->h:I

    .line 15
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/z;->c()Lcom/bytedance/sdk/openadsdk/utils/z;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/f0/v;->b(Lcom/bytedance/sdk/openadsdk/utils/z;)V

    .line 16
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/core/f0/v;->i:Z

    if-nez p1, :cond_3

    .line 17
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/settings/n;->j0()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->p(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_4

    .line 18
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->f:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/g;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 19
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g;->j:Lcom/bytedance/sdk/openadsdk/core/f0/v;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/core/f0/v;->i:Z

    if-nez p1, :cond_5

    .line 20
    new-instance p1, Lcom/bytedance/sdk/component/utils/y;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->a()Landroid/os/Handler;

    move-result-object p2

    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2, p0}, Lcom/bytedance/sdk/component/utils/y;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/component/utils/y$a;)V

    int-to-long p2, p3

    invoke-virtual {p1, v1, p2, p3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 21
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/g;->a()V

    :cond_5
    return-void
.end method

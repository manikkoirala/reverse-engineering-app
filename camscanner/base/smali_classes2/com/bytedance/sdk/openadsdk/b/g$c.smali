.class Lcom/bytedance/sdk/openadsdk/b/g$c;
.super Ljava/lang/Object;
.source "TTAppOpenAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/b/f$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/b/g;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->b:Lcom/bytedance/sdk/openadsdk/b/g;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->b:Lcom/bytedance/sdk/openadsdk/b/g;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/g;I)I

    .line 2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/l/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    const/16 v2, 0x64

    const/4 v3, 0x1

    invoke-direct {v0, v3, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/l/b;-><init>(IILcom/bytedance/sdk/openadsdk/core/f0/q;)V

    .line 3
    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/b/l/b;->a(Z)V

    .line 4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->b:Lcom/bytedance/sdk/openadsdk/b/g;

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/b/l/b;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 4

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "preLoadFail() called with: code = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "], message = ["

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTAppOpenAdLoadManager"

    invoke-static {p2, p1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->b:Lcom/bytedance/sdk/openadsdk/b/g;

    const/4 p2, 0x5

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/g;I)I

    .line 7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/g$c;->b:Lcom/bytedance/sdk/openadsdk/b/g;

    new-instance p2, Lcom/bytedance/sdk/openadsdk/b/l/b;

    const/16 v0, 0x2713

    .line 8
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/g;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/16 v3, 0x64

    invoke-direct {p2, v2, v3, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/l/b;-><init>(IIILjava/lang/String;)V

    .line 9
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/b/g;->a(Lcom/bytedance/sdk/openadsdk/b/g;Lcom/bytedance/sdk/openadsdk/b/l/b;)V

    return-void
.end method

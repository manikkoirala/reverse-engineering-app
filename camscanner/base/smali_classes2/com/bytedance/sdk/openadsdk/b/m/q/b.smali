.class public abstract Lcom/bytedance/sdk/openadsdk/b/m/q/b;
.super Lcom/bytedance/sdk/openadsdk/b/m/q/a;
.source "RewardFullAdType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/b/m/q/b$c;
    }
.end annotation


# instance fields
.field protected j:Ljava/lang/String;

.field protected k:Lcom/bytedance/sdk/openadsdk/core/c0/e;

.field public l:Lcom/bytedance/sdk/openadsdk/core/widget/a;

.field public m:Landroid/widget/LinearLayout;

.field private n:Lcom/bytedance/sdk/openadsdk/k/e;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method protected static a(Landroid/content/Context;)Landroid/widget/FrameLayout;
    .locals 2

    .line 60
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 61
    sget p0, Lcom/bytedance/sdk/openadsdk/utils/h;->k:I

    invoke-virtual {v0, p0}, Landroid/view/View;->setId(I)V

    const/high16 p0, -0x1000000

    .line 62
    invoke-virtual {v0, p0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 63
    new-instance p0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {p0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x11

    .line 64
    iput v1, p0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 65
    invoke-virtual {v0, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method static b(Landroid/widget/FrameLayout;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    .line 6
    .line 7
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->j:I

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 13
    .line 14
    .line 15
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 16
    .line 17
    const/4 v3, -0x1

    .line 18
    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(Landroid/content/Context;)Landroid/widget/FrameLayout;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 29
    .line 30
    .line 31
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;

    .line 32
    .line 33
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;-><init>(Landroid/content/Context;)V

    .line 34
    .line 35
    .line 36
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 39
    .line 40
    .line 41
    const/high16 v2, 0x41700000    # 15.0f

    .line 42
    .line 43
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-virtual {v1, v2, v4, v4, v4}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;->setPadding(IIII)V

    .line 49
    .line 50
    .line 51
    const-string v2, "tt_download_bar_background_new"

    .line 52
    .line 53
    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/t;->c(Landroid/content/Context;Ljava/lang/String;)I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 58
    .line 59
    .line 60
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 61
    .line 62
    const/high16 v4, 0x42b40000    # 90.0f

    .line 63
    .line 64
    invoke-static {v0, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 69
    .line 70
    .line 71
    const/16 v4, 0x50

    .line 72
    .line 73
    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 74
    .line 75
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    .line 77
    .line 78
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;

    .line 79
    .line 80
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;-><init>(Landroid/content/Context;)V

    .line 81
    .line 82
    .line 83
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->m:I

    .line 84
    .line 85
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 86
    .line 87
    .line 88
    const/4 v2, 0x1

    .line 89
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 90
    .line 91
    .line 92
    const/16 v4, 0x8

    .line 93
    .line 94
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95
    .line 96
    .line 97
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 98
    .line 99
    invoke-direct {v5, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p0, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    .line 104
    .line 105
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 106
    .line 107
    invoke-direct {v1, v0, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 108
    .line 109
    .line 110
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->n:I

    .line 111
    .line 112
    invoke-virtual {v1, v5}, Landroid/view/View;->setId(I)V

    .line 113
    .line 114
    .line 115
    const/4 v5, 0x2

    .line 116
    const/4 v6, 0x0

    .line 117
    invoke-virtual {v1, v5, v6}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 118
    .line 119
    .line 120
    const/4 v5, 0x4

    .line 121
    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 122
    .line 123
    .line 124
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 125
    .line 126
    invoke-direct {v5, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {p0, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    .line 131
    .line 132
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 133
    .line 134
    invoke-direct {v1, v0, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 135
    .line 136
    .line 137
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->o:I

    .line 138
    .line 139
    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 143
    .line 144
    .line 145
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 146
    .line 147
    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    .line 152
    .line 153
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private c(Landroid/widget/FrameLayout;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(Landroid/content/Context;)Landroid/widget/FrameLayout;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 10
    .line 11
    .line 12
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;

    .line 13
    .line 14
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    const-string v2, "tt_reward_full_new_bar_bg"

    .line 18
    .line 19
    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 24
    .line 25
    .line 26
    const/high16 v2, 0x41700000    # 15.0f

    .line 27
    .line 28
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    const/4 v3, 0x0

    .line 33
    invoke-virtual {v1, v2, v3, v3, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;->setPadding(IIII)V

    .line 34
    .line 35
    .line 36
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 39
    .line 40
    .line 41
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 42
    .line 43
    const/high16 v3, 0x42980000    # 76.0f

    .line 44
    .line 45
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    const/4 v4, -0x1

    .line 50
    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 51
    .line 52
    .line 53
    const/16 v3, 0x50

    .line 54
    .line 55
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 56
    .line 57
    const/high16 v3, 0x41200000    # 10.0f

    .line 58
    .line 59
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 64
    .line 65
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 70
    .line 71
    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 72
    .line 73
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 74
    .line 75
    .line 76
    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 77
    .line 78
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 79
    .line 80
    .line 81
    const/high16 v3, 0x41600000    # 14.0f

    .line 82
    .line 83
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 88
    .line 89
    invoke-virtual {p1, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    .line 91
    .line 92
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;

    .line 93
    .line 94
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;-><init>(Landroid/content/Context;)V

    .line 95
    .line 96
    .line 97
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->m:I

    .line 98
    .line 99
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 100
    .line 101
    .line 102
    const/16 v2, 0x8

    .line 103
    .line 104
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 105
    .line 106
    .line 107
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 108
    .line 109
    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p1, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    .line 114
    .line 115
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 116
    .line 117
    const/4 v3, 0x1

    .line 118
    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 119
    .line 120
    .line 121
    const/4 v5, 0x2

    .line 122
    const/4 v6, 0x0

    .line 123
    invoke-virtual {v1, v5, v6}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 124
    .line 125
    .line 126
    const/4 v5, 0x4

    .line 127
    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 128
    .line 129
    .line 130
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->n:I

    .line 131
    .line 132
    invoke-virtual {v1, v5}, Landroid/view/View;->setId(I)V

    .line 133
    .line 134
    .line 135
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 136
    .line 137
    invoke-direct {v5, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p1, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    .line 142
    .line 143
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 144
    .line 145
    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 149
    .line 150
    .line 151
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->o:I

    .line 152
    .line 153
    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    .line 154
    .line 155
    .line 156
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 157
    .line 158
    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private d(Landroid/widget/FrameLayout;)V
    .locals 7

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 8
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(Landroid/content/Context;)Landroid/widget/FrameLayout;

    move-result-object v1

    .line 9
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 10
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;-><init>(Landroid/content/Context;)V

    const-string v2, "tt_reward_full_new_bar_bg"

    .line 11
    invoke-static {v0, v2}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    const/high16 v2, 0x41700000    # 15.0f

    .line 12
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v3, v3}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;->setPadding(IIII)V

    .line 13
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 14
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/high16 v3, 0x42980000    # 76.0f

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v3

    const/4 v4, -0x1

    invoke-direct {v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0x50

    .line 15
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/high16 v3, 0x41200000    # 10.0f

    .line 16
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v5

    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 17
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 18
    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 19
    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    const/high16 v3, 0x41600000    # 14.0f

    .line 20
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 21
    invoke-virtual {p1, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGLinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->m:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    const/16 v2, 0x8

    .line 24
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 25
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 26
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v3, 0x1

    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    .line 27
    invoke-virtual {v1, v5, v6}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLayerType(ILandroid/graphics/Paint;)V

    const/4 v5, 0x4

    .line 28
    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 29
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->n:I

    invoke-virtual {v1, v5}, Landroid/view/View;->setId(I)V

    .line 30
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    new-instance v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 32
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 33
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->o:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    .line 34
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private e(Landroid/widget/FrameLayout;)V
    .locals 13

    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 4
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;-><init>(Landroid/content/Context;)V

    const/high16 v2, -0x1000000

    .line 5
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 6
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 7
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    invoke-direct {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 8
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->k:I

    invoke-virtual {p1, v2}, Landroid/view/View;->setId(I)V

    .line 9
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v5, 0x43530000    # 211.0f

    invoke-static {v0, v5}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v5

    invoke-direct {v4, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xd

    .line 10
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 11
    invoke-virtual {v1, p1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 12
    new-instance p1, Landroid/view/View;

    invoke-direct {p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v4, 0x8

    .line 13
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 14
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->p:I

    invoke-virtual {p1, v5}, Landroid/view/View;->setId(I)V

    .line 15
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v7, 0x2

    .line 16
    invoke-virtual {v5, v7, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 17
    invoke-virtual {v1, p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 18
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;

    invoke-direct {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFDownloadBarLayout;-><init>(Landroid/content/Context;)V

    .line 19
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    invoke-virtual {p1, v5}, Landroid/view/View;->setId(I)V

    .line 20
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x438c0000    # 280.0f

    invoke-static {v0, v8}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v8

    const/high16 v9, 0x428c0000    # 70.0f

    invoke-static {v0, v9}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v10

    invoke-direct {v5, v8, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 21
    invoke-virtual {v5, v7, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v8, 0xe

    .line 22
    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/high16 v10, 0x42040000    # 33.0f

    .line 23
    invoke-static {v0, v10}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v10

    iput v10, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 24
    invoke-virtual {v1, p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    new-instance p1, Landroid/view/View;

    invoke-direct {p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 26
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->q0:I

    invoke-virtual {p1, v5}, Landroid/view/View;->setId(I)V

    .line 27
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 28
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v6, 0x3

    .line 29
    invoke-virtual {v5, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 30
    invoke-virtual {v1, p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    invoke-direct {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 32
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const/4 v5, 0x1

    .line 33
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setLines(I)V

    const/16 v10, 0x11

    .line 34
    invoke-virtual {p1, v10}, Landroid/widget/TextView;->setGravity(I)V

    const-string v10, "tt_video_download_apk"

    .line 35
    invoke-static {v0, v10}, Lcom/bytedance/sdk/component/utils/t;->j(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {p1, v10}, Landroid/widget/TextView;->setText(I)V

    .line 36
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v10, 0x41700000    # 15.0f

    .line 37
    invoke-virtual {p1, v7, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    const-string v10, "tt_reward_video_download_btn_bg"

    .line 38
    invoke-static {v0, v10}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {p1, v10}, Landroid/view/View;->setBackgroundResource(I)V

    const v10, 0x1f000009

    .line 39
    invoke-virtual {p1, v10}, Landroid/view/View;->setId(I)V

    .line 40
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v11, 0x43820000    # 260.0f

    invoke-static {v0, v11}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v11

    const/high16 v12, 0x42100000    # 36.0f

    invoke-static {v0, v12}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v12

    invoke-direct {v10, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 41
    invoke-virtual {v10, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 42
    invoke-virtual {v10, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 43
    invoke-static {v0, v9}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    move-result v2

    iput v2, v10, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 44
    invoke-virtual {v1, p1, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    new-instance p1, Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {p1, v0, v5}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    const/4 v2, 0x0

    .line 46
    invoke-virtual {p1, v7, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLayerType(ILandroid/graphics/Paint;)V

    const/4 v2, 0x4

    .line 47
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 48
    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->n:I

    invoke-virtual {p1, v2}, Landroid/view/View;->setId(I)V

    .line 49
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    new-instance p1, Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-direct {p1, v0, v5}, Lcom/bytedance/sdk/component/widget/SSWebView;-><init>(Landroid/content/Context;Z)V

    .line 51
    invoke-virtual {p1, v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 52
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->o:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public B()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public C()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->f()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
.end method

.method public a(Landroid/os/Message;)V
    .locals 5

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleMsg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTAD.RFAdType"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    const/16 v2, 0x12c

    if-eq v0, v2, :cond_9

    const/16 v2, 0x190

    const/4 v3, 0x0

    if-eq v0, v2, :cond_8

    const/16 v1, 0x1f4

    if-eq v0, v1, :cond_4

    const/16 v1, 0x258

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2bc

    if-eq v0, v1, :cond_0

    goto/16 :goto_0

    .line 27
    :cond_0
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 28
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->F:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_b

    if-lez p1, :cond_1

    .line 29
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->e()V

    .line 30
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit16 v4, p1, 0x3e8

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "s"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->d(Z)V

    .line 32
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 33
    iput v1, v0, Landroid/os/Message;->what:I

    add-int/lit16 p1, p1, -0x3e8

    .line 34
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 35
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    add-int/lit16 v1, v1, -0x3e8

    iput v1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    .line 36
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 37
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 38
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->g(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->u()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 39
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->C()V

    goto/16 :goto_0

    .line 40
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    goto/16 :goto_0

    .line 41
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->A()V

    goto/16 :goto_0

    .line 42
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 43
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    invoke-virtual {p1, v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 44
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 45
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 46
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->p()V

    .line 47
    invoke-virtual {p1}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/webkit/WebView;->resumeTimers()V

    .line 48
    :cond_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 49
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(F)V

    .line 50
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(F)V

    .line 51
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->m()Z

    move-result p1

    if-nez p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->w()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 52
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->D()V

    goto :goto_0

    .line 53
    :cond_8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->D()V

    .line 54
    invoke-virtual {p0, v3, v1, v3}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(ZZZ)V

    goto :goto_0

    .line 55
    :cond_9
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->y()V

    .line 56
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->q()Z

    move-result v0

    xor-int/2addr v0, v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->q()Z

    move-result v2

    xor-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a(II)V

    .line 57
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->m()Lcom/bytedance/sdk/openadsdk/core/i0/d;

    move-result-object p1

    if-eqz p1, :cond_b

    .line 58
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->m()Lcom/bytedance/sdk/openadsdk/core/i0/d;

    move-result-object p1

    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/i0/k/a;->f:Lcom/bytedance/sdk/openadsdk/core/i0/k/a;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/i0/d;->a(Lcom/bytedance/sdk/openadsdk/core/i0/k/a;)V

    goto :goto_0

    .line 59
    :cond_a
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->C()V

    :cond_b
    :goto_0
    return-void
.end method

.method public abstract a(Landroid/widget/FrameLayout;)V
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/component/reward/view/RewardFullBaseLayout;)V
    .locals 2

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->p0()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 7
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->e(Landroid/widget/FrameLayout;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 9
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->c(Landroid/widget/FrameLayout;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 10
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->d(Landroid/widget/FrameLayout;)V

    goto :goto_0

    .line 11
    :cond_2
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->b(Landroid/widget/FrameLayout;)V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/component/utils/y;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/component/utils/y;)V

    .line 2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d(Z)V

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->o()V

    :cond_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/k/e;)V
    .locals 3

    .line 13
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->n:Lcom/bytedance/sdk/openadsdk/k/e;

    .line 14
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b()V

    .line 15
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->k1()Z

    move-result p1

    if-nez p1, :cond_0

    .line 16
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m()V

    .line 18
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->f()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 19
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->d()V

    .line 20
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 21
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    const/16 v0, 0x1f4

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 22
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->k:F

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float p1, p1, v1

    if-nez p1, :cond_3

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(Z)V

    .line 23
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->l()V

    .line 24
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->o()V

    return-void
.end method

.method public a(ZZZ)V
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a(ZZZLcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    return-void
.end method

.method public d()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->q()Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/g;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/h;

    if-eqz v0, :cond_1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->L:Lcom/bytedance/sdk/openadsdk/b/m/p/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->g()V

    return-void

    .line 3
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->h:Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->y()V

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->q()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a(II)V

    :cond_2
    return-void
.end method

.method public e()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->l:Lcom/bytedance/sdk/openadsdk/core/widget/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->l:Lcom/bytedance/sdk/openadsdk/core/widget/a;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected f()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public g()Lcom/bytedance/sdk/openadsdk/component/reward/view/RFEndCardBackUpLayout;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFEndCardBackUpLayout;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 4
    .line 5
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 6
    .line 7
    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/RFEndCardBackUpLayout;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public h()Landroid/view/View;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->p0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x5

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 13
    .line 14
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 15
    .line 16
    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 17
    .line 18
    .line 19
    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->f1:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v2, "getLoadingFrameView"

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    const-string v2, "TTAD.RFAdType"

    .line 44
    .line 45
    invoke-static {v2, v1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method protected i()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->p0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/high16 v2, 0x42c80000    # 100.0f

    .line 9
    .line 10
    if-eq v0, v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/high16 v2, 0x41a00000    # 20.0f

    .line 23
    .line 24
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 25
    .line 26
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->W:Landroid/content/Context;

    .line 27
    .line 28
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    return v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public j()Lcom/bytedance/sdk/openadsdk/b/m/q/b$c;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public k()Landroid/view/View;
    .locals 9

    .line 1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 4
    .line 5
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 6
    .line 7
    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 13
    .line 14
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 15
    .line 16
    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 17
    .line 18
    .line 19
    const v2, 0x1f00000c

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 23
    .line 24
    .line 25
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 26
    .line 27
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 28
    .line 29
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 30
    .line 31
    const/high16 v4, 0x41e00000    # 28.0f

    .line 32
    .line 33
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 38
    .line 39
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 40
    .line 41
    invoke-static {v5, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 46
    .line 47
    .line 48
    const v3, 0x800035

    .line 49
    .line 50
    .line 51
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 52
    .line 53
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 54
    .line 55
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 56
    .line 57
    const/high16 v4, 0x41a00000    # 20.0f

    .line 58
    .line 59
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 64
    .line 65
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 66
    .line 67
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 68
    .line 69
    const/high16 v4, 0x41c00000    # 24.0f

    .line 70
    .line 71
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 76
    .line 77
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    .line 79
    .line 80
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 81
    .line 82
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 83
    .line 84
    const-string v3, "tt_mute_btn_bg"

    .line 85
    .line 86
    invoke-static {v2, v3}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 91
    .line 92
    .line 93
    const/16 v2, 0x11

    .line 94
    .line 95
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGRelativeLayout;->setGravity(I)V

    .line 96
    .line 97
    .line 98
    const/16 v3, 0x8

    .line 99
    .line 100
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    .line 102
    .line 103
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 104
    .line 105
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 106
    .line 107
    const-string v4, "tt_ad_close_text"

    .line 108
    .line 109
    invoke-static {v3, v4}, Lcom/bytedance/sdk/component/utils/t;->k(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v3

    .line 113
    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    .line 115
    .line 116
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;

    .line 117
    .line 118
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 119
    .line 120
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 121
    .line 122
    invoke-direct {v3, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;-><init>(Landroid/content/Context;)V

    .line 123
    .line 124
    .line 125
    const v5, 0x1f00000a

    .line 126
    .line 127
    .line 128
    invoke-virtual {v3, v5}, Landroid/view/View;->setId(I)V

    .line 129
    .line 130
    .line 131
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 132
    .line 133
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 134
    .line 135
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 136
    .line 137
    const/high16 v7, 0x41400000    # 12.0f

    .line 138
    .line 139
    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 144
    .line 145
    iget-object v8, v8, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 146
    .line 147
    invoke-static {v8, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 152
    .line 153
    .line 154
    invoke-virtual {v3, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    .line 156
    .line 157
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 158
    .line 159
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 160
    .line 161
    const-string v6, "tt_video_close_drawable"

    .line 162
    .line 163
    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 164
    .line 165
    .line 166
    move-result v5

    .line 167
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    .line 169
    .line 170
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 171
    .line 172
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 173
    .line 174
    invoke-static {v5, v4}, Lcom/bytedance/sdk/component/utils/t;->k(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v4

    .line 178
    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 182
    .line 183
    .line 184
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;

    .line 185
    .line 186
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 187
    .line 188
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 189
    .line 190
    invoke-direct {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;-><init>(Landroid/content/Context;)V

    .line 191
    .line 192
    .line 193
    const v4, 0x1f00003d

    .line 194
    .line 195
    .line 196
    invoke-virtual {v3, v4}, Landroid/view/View;->setId(I)V

    .line 197
    .line 198
    .line 199
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    .line 200
    .line 201
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 202
    .line 203
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 204
    .line 205
    const/high16 v6, 0x42000000    # 32.0f

    .line 206
    .line 207
    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 208
    .line 209
    .line 210
    move-result v5

    .line 211
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 212
    .line 213
    iget-object v7, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 214
    .line 215
    const/high16 v8, 0x41600000    # 14.0f

    .line 216
    .line 217
    invoke-static {v7, v8}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 218
    .line 219
    .line 220
    move-result v7

    .line 221
    invoke-direct {v4, v5, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 222
    .line 223
    .line 224
    const v5, 0x800053

    .line 225
    .line 226
    .line 227
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 228
    .line 229
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 230
    .line 231
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 232
    .line 233
    const/high16 v7, 0x41800000    # 16.0f

    .line 234
    .line 235
    invoke-static {v5, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 236
    .line 237
    .line 238
    move-result v5

    .line 239
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 240
    .line 241
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 242
    .line 243
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 244
    .line 245
    const/high16 v7, 0x42c80000    # 100.0f

    .line 246
    .line 247
    invoke-static {v5, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 248
    .line 249
    .line 250
    move-result v5

    .line 251
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 252
    .line 253
    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    .line 255
    .line 256
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 257
    .line 258
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 259
    .line 260
    const-string v5, "tt_ad_logo_new"

    .line 261
    .line 262
    invoke-static {v4, v5}, Lcom/bytedance/sdk/component/utils/t;->e(Landroid/content/Context;Ljava/lang/String;)I

    .line 263
    .line 264
    .line 265
    move-result v4

    .line 266
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 267
    .line 268
    .line 269
    invoke-virtual {v3, v2}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGTextView;->setGravity(I)V

    .line 270
    .line 271
    .line 272
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;

    .line 273
    .line 274
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 275
    .line 276
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 277
    .line 278
    invoke-direct {v2, v4}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;-><init>(Landroid/content/Context;)V

    .line 279
    .line 280
    .line 281
    sget v4, Lcom/bytedance/sdk/openadsdk/utils/h;->g1:I

    .line 282
    .line 283
    invoke-virtual {v2, v4}, Landroid/view/View;->setId(I)V

    .line 284
    .line 285
    .line 286
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    .line 287
    .line 288
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 289
    .line 290
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 291
    .line 292
    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 293
    .line 294
    .line 295
    move-result v5

    .line 296
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 297
    .line 298
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 299
    .line 300
    invoke-static {v6, v8}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 301
    .line 302
    .line 303
    move-result v6

    .line 304
    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 305
    .line 306
    .line 307
    const v5, 0x800055

    .line 308
    .line 309
    .line 310
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 311
    .line 312
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 313
    .line 314
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 315
    .line 316
    const/high16 v6, 0x40e00000    # 7.0f

    .line 317
    .line 318
    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 319
    .line 320
    .line 321
    move-result v5

    .line 322
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 323
    .line 324
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 325
    .line 326
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 327
    .line 328
    invoke-static {v5, v7}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 329
    .line 330
    .line 331
    move-result v5

    .line 332
    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 333
    .line 334
    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    .line 336
    .line 337
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 338
    .line 339
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 340
    .line 341
    const/high16 v5, 0x41100000    # 9.0f

    .line 342
    .line 343
    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 344
    .line 345
    .line 346
    move-result v4

    .line 347
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 348
    .line 349
    iget-object v6, v6, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 350
    .line 351
    invoke-static {v6, v5}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 352
    .line 353
    .line 354
    move-result v5

    .line 355
    const/4 v6, 0x0

    .line 356
    invoke-virtual {v2, v4, v6, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGImageView;->setPadding(IIII)V

    .line 357
    .line 358
    .line 359
    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    .line 360
    .line 361
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 362
    .line 363
    .line 364
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;

    .line 365
    .line 366
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 367
    .line 368
    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 369
    .line 370
    invoke-direct {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/widget/PlayableLoadingView;-><init>(Landroid/content/Context;)V

    .line 371
    .line 372
    .line 373
    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->h1:I

    .line 374
    .line 375
    invoke-virtual {v4, v5}, Landroid/view/View;->setId(I)V

    .line 376
    .line 377
    .line 378
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 379
    .line 380
    const/4 v6, -0x1

    .line 381
    invoke-direct {v5, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 382
    .line 383
    .line 384
    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/core/customview/PAGFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 385
    .line 386
    .line 387
    const/4 v5, 0x1

    .line 388
    invoke-virtual {v4, v5}, Landroid/view/View;->setClickable(Z)V

    .line 389
    .line 390
    .line 391
    invoke-virtual {v4, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 392
    .line 393
    .line 394
    new-instance v5, Lcom/bytedance/sdk/openadsdk/component/reward/top/TopProxyLayout;

    .line 395
    .line 396
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 397
    .line 398
    iget-object v7, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 399
    .line 400
    invoke-direct {v5, v7}, Lcom/bytedance/sdk/openadsdk/component/reward/top/TopProxyLayout;-><init>(Landroid/content/Context;)V

    .line 401
    .line 402
    .line 403
    sget v7, Lcom/bytedance/sdk/openadsdk/utils/h;->i1:I

    .line 404
    .line 405
    invoke-virtual {v5, v7}, Landroid/view/View;->setId(I)V

    .line 406
    .line 407
    .line 408
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 409
    .line 410
    const/4 v8, -0x2

    .line 411
    invoke-direct {v7, v6, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 412
    .line 413
    .line 414
    invoke-virtual {v5, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 415
    .line 416
    .line 417
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 418
    .line 419
    .line 420
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 421
    .line 422
    .line 423
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 424
    .line 425
    .line 426
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 427
    .line 428
    .line 429
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 430
    .line 431
    .line 432
    return-object v0
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method public l()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->L:Lcom/bytedance/sdk/openadsdk/b/m/p/h;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->e()Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->k:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected final m()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->k()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 9
    .line 10
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v1, "reward_endcard"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v1, "fullscreen_endcard"

    .line 18
    .line 19
    :goto_0
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 20
    .line 21
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->m()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->n:Lcom/bytedance/sdk/openadsdk/k/e;

    .line 32
    .line 33
    invoke-virtual {v2, v0, v3, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/Boolean;Lcom/bytedance/sdk/openadsdk/k/e;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 37
    .line 38
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 39
    .line 40
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 41
    .line 42
    invoke-virtual {v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 48
    .line 49
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->n:Lcom/bytedance/sdk/openadsdk/k/e;

    .line 50
    .line 51
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 52
    .line 53
    invoke-virtual {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Lcom/bytedance/sdk/openadsdk/k/e;Z)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 57
    .line 58
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public n()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 4
    .line 5
    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->m:I

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/LinearLayout;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m:Landroid/widget/LinearLayout;

    .line 14
    .line 15
    const/16 v1, 0x8

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 21
    .line 22
    new-instance v1, Lcom/bytedance/sdk/openadsdk/common/f;

    .line 23
    .line 24
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 25
    .line 26
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 27
    .line 28
    const-string v4, "landingpage_endcard"

    .line 29
    .line 30
    invoke-direct {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/common/f;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->P:Lcom/bytedance/sdk/openadsdk/common/f;

    .line 34
    .line 35
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 36
    .line 37
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->P:Lcom/bytedance/sdk/openadsdk/common/f;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/common/f;->a()Landroid/widget/ImageView;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/q/b$a;

    .line 44
    .line 45
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m:Landroid/widget/LinearLayout;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 54
    .line 55
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->P:Lcom/bytedance/sdk/openadsdk/common/f;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/common/f;->b()Landroid/view/View;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 62
    .line 63
    const/4 v3, -0x1

    .line 64
    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 71
    .line 72
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 73
    .line 74
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->P:Lcom/bytedance/sdk/openadsdk/common/f;

    .line 75
    .line 76
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/common/f;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public abstract o()V
.end method

.method public p()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->K:Lcom/bytedance/sdk/openadsdk/b/m/p/i;

    .line 4
    .line 5
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/i;->a(Z)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 11
    .line 12
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->f()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 18
    .line 19
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->i()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->d(I)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 29
    .line 30
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->c()V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 36
    .line 37
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->k1()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    .line 48
    .line 49
    if-eqz v0, :cond_0

    .line 50
    .line 51
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->n()V

    .line 52
    .line 53
    .line 54
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 55
    .line 56
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 57
    .line 58
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j()V

    .line 59
    .line 60
    .line 61
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 62
    .line 63
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->f()V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 69
    .line 70
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c()V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 76
    .line 77
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 78
    .line 79
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->i(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    const/4 v1, 0x1

    .line 84
    const/4 v2, 0x0

    .line 85
    if-eqz v0, :cond_2

    .line 86
    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 88
    .line 89
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    const/high16 v3, -0x1000000

    .line 96
    .line 97
    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setBackgroundColor(I)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 101
    .line 102
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/component/widget/SSWebView;->setBackgroundColor(I)V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 112
    .line 113
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->c(Z)V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 119
    .line 120
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 121
    .line 122
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-eqz v0, :cond_2

    .line 127
    .line 128
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 129
    .line 130
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 131
    .line 132
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->s()V

    .line 133
    .line 134
    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 136
    .line 137
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 138
    .line 139
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    const/4 v3, 0x4

    .line 144
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 145
    .line 146
    .line 147
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 148
    .line 149
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h()Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;I)V

    .line 156
    .line 157
    .line 158
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 159
    .line 160
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 161
    .line 162
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    if-nez v0, :cond_5

    .line 167
    .line 168
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 169
    .line 170
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 171
    .line 172
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->f(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 173
    .line 174
    .line 175
    move-result v0

    .line 176
    if-eqz v0, :cond_3

    .line 177
    .line 178
    goto :goto_0

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 180
    .line 181
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 182
    .line 183
    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->W:Landroid/content/Context;

    .line 184
    .line 185
    iget v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->l:I

    .line 186
    .line 187
    int-to-float v0, v0

    .line 188
    invoke-static {v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 189
    .line 190
    .line 191
    move-result v0

    .line 192
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 193
    .line 194
    iget-object v5, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->W:Landroid/content/Context;

    .line 195
    .line 196
    iget v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->m:I

    .line 197
    .line 198
    int-to-float v4, v4

    .line 199
    invoke-static {v5, v4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;F)I

    .line 200
    .line 201
    .line 202
    move-result v4

    .line 203
    invoke-virtual {v3, v0, v4}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(II)V

    .line 204
    .line 205
    .line 206
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 207
    .line 208
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/d;

    .line 209
    .line 210
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->b()V

    .line 211
    .line 212
    .line 213
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 214
    .line 215
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 216
    .line 217
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 218
    .line 219
    .line 220
    move-result v0

    .line 221
    if-eqz v0, :cond_4

    .line 222
    .line 223
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 224
    .line 225
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 226
    .line 227
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d(Z)V

    .line 228
    .line 229
    .line 230
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 231
    .line 232
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 233
    .line 234
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->E()V

    .line 235
    .line 236
    .line 237
    invoke-virtual {p0, v2, v2, v2}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(ZZZ)V

    .line 238
    .line 239
    .line 240
    return-void

    .line 241
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 242
    .line 243
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 244
    .line 245
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->r()Z

    .line 246
    .line 247
    .line 248
    move-result v0

    .line 249
    if-eqz v0, :cond_5

    .line 250
    .line 251
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 252
    .line 253
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 254
    .line 255
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(I)V

    .line 256
    .line 257
    .line 258
    :cond_5
    :goto_0
    return-void
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method public abstract q()Z
.end method

.method public abstract r()Z
.end method

.method public s()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 6
    .line 7
    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    .line 8
    .line 9
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->u(Ljava/lang/String;)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x1

    .line 18
    if-ne v0, v1, :cond_7

    .line 19
    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 21
    .line 22
    iget-boolean v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 23
    .line 24
    if-eqz v2, :cond_1

    .line 25
    .line 26
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 27
    .line 28
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 39
    .line 40
    iget v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    .line 41
    .line 42
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-interface {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(Ljava/lang/String;Z)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 56
    .line 57
    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    .line 58
    .line 59
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(Ljava/lang/String;)I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 69
    .line 70
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    .line 76
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 81
    .line 82
    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    .line 83
    .line 84
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    const/4 v2, 0x0

    .line 89
    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->a(Ljava/lang/String;Z)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    goto :goto_0

    .line 94
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 99
    .line 100
    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    .line 101
    .line 102
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->r(Ljava/lang/String;)I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 111
    .line 112
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 113
    .line 114
    if-eqz v1, :cond_3

    .line 115
    .line 116
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->c()Z

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    if-eqz v1, :cond_3

    .line 121
    .line 122
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 123
    .line 124
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 125
    .line 126
    if-eqz v0, :cond_7

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->b()Landroid/view/View;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 137
    .line 138
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 139
    .line 140
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    if-eqz v1, :cond_4

    .line 145
    .line 146
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 147
    .line 148
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 149
    .line 150
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    if-eqz v1, :cond_7

    .line 155
    .line 156
    :cond_4
    const/4 v1, -0x1

    .line 157
    if-eq v0, v1, :cond_7

    .line 158
    .line 159
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 160
    .line 161
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 162
    .line 163
    if-eqz v1, :cond_5

    .line 164
    .line 165
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->n()J

    .line 166
    .line 167
    .line 168
    move-result-wide v1

    .line 169
    mul-int/lit16 v3, v0, 0x3e8

    .line 170
    .line 171
    int-to-long v3, v3

    .line 172
    cmp-long v5, v1, v3

    .line 173
    .line 174
    if-gez v5, :cond_6

    .line 175
    .line 176
    :cond_5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 177
    .line 178
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 179
    .line 180
    if-eqz v1, :cond_7

    .line 181
    .line 182
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a()I

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 187
    .line 188
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 189
    .line 190
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->d()I

    .line 191
    .line 192
    .line 193
    move-result v2

    .line 194
    sub-int/2addr v1, v2

    .line 195
    if-lt v1, v0, :cond_7

    .line 196
    .line 197
    :cond_6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 198
    .line 199
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    .line 200
    .line 201
    if-eqz v0, :cond_7

    .line 202
    .line 203
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a()V

    .line 204
    .line 205
    .line 206
    :cond_7
    :goto_1
    return-void
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
.end method

.method public t()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->b()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 11
    .line 12
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->j()V

    .line 17
    .line 18
    .line 19
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 20
    .line 21
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 22
    .line 23
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a(Z)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->r()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-nez v0, :cond_2

    .line 33
    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 35
    .line 36
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 37
    .line 38
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-nez v0, :cond_2

    .line 43
    .line 44
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 45
    .line 46
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a()V

    .line 49
    .line 50
    .line 51
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 52
    .line 53
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->w()V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 59
    .line 60
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->k()V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 66
    .line 67
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->q()V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 73
    .line 74
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->e()V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public u()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->k()V

    .line 11
    .line 12
    .line 13
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    iput-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->e:Z

    .line 17
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v1, "onPause mIsActivityShow="

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 29
    .line 30
    iget-boolean v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->e:Z

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v1, " mIsMute="

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 41
    .line 42
    iget-boolean v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const-string v1, "TTAD.RFAdType"

    .line 52
    .line 53
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 57
    .line 58
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 59
    .line 60
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-nez v0, :cond_2

    .line 65
    .line 66
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 67
    .line 68
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->x()V

    .line 71
    .line 72
    .line 73
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->z()V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 77
    .line 78
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 79
    .line 80
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->B()V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 84
    .line 85
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->l()V

    .line 88
    .line 89
    .line 90
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 91
    .line 92
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 93
    .line 94
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-eqz v0, :cond_3

    .line 99
    .line 100
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 101
    .line 102
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->F:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 103
    .line 104
    const/4 v1, 0x1

    .line 105
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 106
    .line 107
    .line 108
    :cond_3
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public v()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public w()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 12
    .line 13
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->s:Z

    .line 14
    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->F:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 27
    .line 28
    iget v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    .line 29
    .line 30
    if-gez v0, :cond_0

    .line 31
    .line 32
    const/4 v1, -0x1

    .line 33
    if-ne v0, v1, :cond_1

    .line 34
    .line 35
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const/16 v1, 0x2bc

    .line 40
    .line 41
    iput v1, v0, Landroid/os/Message;->what:I

    .line 42
    .line 43
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 44
    .line 45
    iget v2, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->t:I

    .line 46
    .line 47
    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 48
    .line 49
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    .line 50
    .line 51
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 52
    .line 53
    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public x()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->m()V

    .line 11
    .line 12
    .line 13
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v1, "onStop mIsMute="

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 24
    .line 25
    iget-boolean v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, " mLast="

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 36
    .line 37
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->O:Lcom/bytedance/sdk/openadsdk/k/h;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/h;->a()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, " mVolume="

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils;->d()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    const-string v1, "TTAD.RFAdType"

    .line 63
    .line 64
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 68
    .line 69
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 70
    .line 71
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->D()V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 75
    .line 76
    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    .line 77
    .line 78
    if-eqz v1, :cond_2

    .line 79
    .line 80
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 81
    .line 82
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/q/b$b;

    .line 83
    .line 84
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 88
    .line 89
    .line 90
    :cond_2
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->e:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->D()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-virtual {p0, v0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(ZZZ)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 12
    .line 13
    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->h:Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

    .line 18
    .line 19
    const/16 v1, 0x2710

    .line 20
    .line 21
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;->b(I)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public z()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->i:Lcom/bytedance/sdk/component/utils/y;

    .line 2
    .line 3
    const/16 v1, 0x12c

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.class public Lcom/bytedance/sdk/openadsdk/b/m/e;
.super Ljava/lang/Object;
.source "FullScreenVideoLoadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/b/m/e$h;,
        Lcom/bytedance/sdk/openadsdk/b/m/e$i;
    }
.end annotation


# static fields
.field private static volatile g:Lcom/bytedance/sdk/openadsdk/b/m/e;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/p;

.field private c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/b/m/e$i;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lb/b/a/a/k/g;

.field private final f:Lcom/bytedance/sdk/component/utils/w$b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    new-instance v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->d:Ljava/util/List;

    .line 22
    .line 23
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/e$g;

    .line 24
    .line 25
    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/b/m/e$g;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;)V

    .line 26
    .line 27
    .line 28
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->f:Lcom/bytedance/sdk/component/utils/w$b;

    .line 29
    .line 30
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->c()Lcom/bytedance/sdk/openadsdk/core/p;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->b:Lcom/bytedance/sdk/openadsdk/core/p;

    .line 35
    .line 36
    if-nez p1, :cond_0

    .line 37
    .line 38
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    :goto_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/e;->c()V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/e;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/e;Lb/b/a/a/k/g;)Lb/b/a/a/k/g;
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->e:Lb/b/a/a/k/g;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/e;
    .locals 2

    .line 4
    sget-object v0, Lcom/bytedance/sdk/openadsdk/b/m/e;->g:Lcom/bytedance/sdk/openadsdk/b/m/e;

    if-nez v0, :cond_1

    .line 5
    const-class v0, Lcom/bytedance/sdk/openadsdk/b/m/e;

    monitor-enter v0

    .line 6
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/b/m/e;->g:Lcom/bytedance/sdk/openadsdk/b/m/e;

    if-nez v1, :cond_0

    .line 7
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/e;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/e;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/b/m/e;->g:Lcom/bytedance/sdk/openadsdk/b/m/e;

    .line 8
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 9
    :cond_1
    :goto_0
    sget-object p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->g:Lcom/bytedance/sdk/openadsdk/b/m/e;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/common/b;)V
    .locals 11

    .line 15
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/z;->c()Lcom/bytedance/sdk/openadsdk/utils/z;

    move-result-object v7

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 16
    invoke-direct {p0, p1, p2, v7, p3}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/common/b;)V

    goto/16 :goto_2

    .line 17
    :cond_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object p2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/f0/a;

    move-result-object p2

    const/4 v8, 0x0

    if-eqz p2, :cond_5

    .line 18
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 19
    new-instance v9, Lcom/bytedance/sdk/openadsdk/b/m/n;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-direct {v9, v0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/n;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/a;)V

    .line 20
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->a()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->a()Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-virtual {v9, v0}, Lcom/bytedance/sdk/openadsdk/b/m/n;->a(Ljava/lang/String;)V

    :cond_1
    if-eqz p3, :cond_3

    .line 24
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 25
    instance-of v0, p3, Lcom/bytedance/sdk/openadsdk/api/interstitial/PAGInterstitialAdLoadListener;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->C()I

    move-result v0

    if-nez v0, :cond_2

    .line 26
    move-object v0, p3

    check-cast v0, Lcom/bytedance/sdk/openadsdk/api/interstitial/PAGInterstitialAdLoadListener;

    invoke-virtual {v9}, Lcom/bytedance/sdk/openadsdk/b/m/n;->a()Lcom/bytedance/sdk/openadsdk/b/m/f;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/api/PAGLoadListener;->onAdLoaded(Ljava/lang/Object;)V

    .line 27
    :cond_2
    new-instance v10, Lcom/bytedance/sdk/openadsdk/b/m/e$h;

    const/4 v0, 0x0

    invoke-direct {v10, p3, p2, v0}, Lcom/bytedance/sdk/openadsdk/b/m/e$h;-><init>(Lcom/bytedance/sdk/openadsdk/common/b;Lcom/bytedance/sdk/openadsdk/core/f0/a;Lcom/bytedance/sdk/openadsdk/b/m/e$a;)V

    const/4 p3, 0x0

    .line 28
    :goto_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_3

    .line 29
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 30
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->h()Z

    move-result v6

    move-object v0, p0

    move-object v2, p1

    move-object v3, v10

    move-object v4, v7

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/b/m/e$h;Lcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/n;Z)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 31
    :cond_3
    :goto_1
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->b()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-ge v8, p3, :cond_4

    .line 32
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/f0/a;->b()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 33
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->b()Lcom/bytedance/sdk/openadsdk/core/j0/b/a;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/e$a;

    invoke-direct {v1, p0, p3, p1, v7}, Lcom/bytedance/sdk/openadsdk/b/m/e$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/utils/z;)V

    invoke-virtual {v0, p3, v1}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/core/j0/b/a$d;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    const-string p1, "FullScreenVideoLoadManager"

    const-string p2, "get cache data success"

    .line 34
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "bidding"

    const-string p2, "full video get cache data success"

    .line 35
    invoke-static {p1, p2}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 36
    :cond_5
    invoke-direct {p0, p1, v8, v7, p3}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/common/b;)V

    :goto_2
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/common/b;)V
    .locals 11

    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 48
    new-instance v8, Lcom/bytedance/sdk/openadsdk/core/f0/s;

    invoke-direct {v8}, Lcom/bytedance/sdk/openadsdk/core/f0/s;-><init>()V

    const/4 v0, 0x2

    if-eqz p2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 49
    :goto_0
    iput v1, v8, Lcom/bytedance/sdk/openadsdk/core/f0/s;->c:I

    .line 50
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->isExpressAd()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 52
    :cond_1
    iput v0, v8, Lcom/bytedance/sdk/openadsdk/core/f0/s;->f:I

    .line 53
    :cond_2
    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->b:Lcom/bytedance/sdk/openadsdk/core/p;

    new-instance v10, Lcom/bytedance/sdk/openadsdk/b/m/e$c;

    move-object v0, v10

    move-object v1, p0

    move v2, p2

    move-object v3, p4

    move-object v4, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/b/m/e$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;ZLcom/bytedance/sdk/openadsdk/common/b;Lcom/bytedance/sdk/openadsdk/AdSlot;JLcom/bytedance/sdk/openadsdk/utils/z;)V

    const/16 p2, 0x8

    invoke-interface {v9, p1, v8, p2, v10}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/f0/s;ILcom/bytedance/sdk/openadsdk/core/p$a;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/b/m/e$i;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    .line 74
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/e;Lcom/bytedance/sdk/openadsdk/core/f0/a;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/b/m/n;Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;Z)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p8}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/core/f0/a;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/b/m/n;Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;Z)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/f0/a;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/b/m/n;Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;Z)V
    .locals 16

    move-object/from16 v11, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v6, p4

    move-object/from16 v12, p7

    .line 54
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->b()Lcom/bytedance/sdk/openadsdk/core/j0/b/a;

    move-result-object v7

    new-instance v8, Lcom/bytedance/sdk/openadsdk/b/m/e$d;

    move-object v0, v8

    move-object/from16 v1, p0

    move/from16 v2, p5

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/b/m/e$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;ZLcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/utils/z;)V

    invoke-virtual {v7, v10, v8}, Lcom/bytedance/sdk/openadsdk/core/j0/b/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/core/j0/b/a$d;)V

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    .line 55
    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v1

    invoke-virtual/range {p4 .. p4}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->l(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/settings/a;

    move-result-object v1

    .line 57
    iget v1, v1, Lcom/bytedance/sdk/openadsdk/core/settings/a;->d:I

    if-ne v1, v0, :cond_0

    .line 58
    iget-object v1, v11, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/component/utils/p;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/e$i;

    invoke-direct {v0, v10, v6, v9}, Lcom/bytedance/sdk/openadsdk/b/m/e$i;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/f0/a;)V

    invoke-direct {v11, v0}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/b/m/e$i;)V

    return-void

    :cond_0
    if-eqz v12, :cond_1

    if-nez p8, :cond_2

    .line 60
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->C()I

    move-result v1

    if-ne v1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 61
    :cond_2
    :goto_0
    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 62
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_3

    .line 63
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 64
    invoke-virtual/range {p2 .. p2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->u0()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/CacheDirFactory;->getICacheDir(I)Lb/a/a/a/a/a/a/d/b;

    move-result-object v0

    invoke-interface {v0}, Lb/a/a/a/a/a/a/d/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    move-result-object v15

    const-string v0, "material_meta"

    .line 65
    invoke-virtual {v15, v0, v10}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "ad_slot"

    .line 66
    invoke-virtual {v15, v0, v6}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "FullScreenVideoLoadManager"

    const-string v1, "FullScreenLog: preload video "

    .line 67
    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v8, Lcom/bytedance/sdk/openadsdk/b/m/e$e;

    move-object v0, v8

    move-object/from16 v1, p0

    move/from16 v2, p5

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move/from16 v7, p8

    move-object v10, v8

    move-object/from16 v8, p3

    move-object/from16 v9, p1

    move-object v13, v10

    move-object v10, v14

    invoke-direct/range {v0 .. v10}, Lcom/bytedance/sdk/openadsdk/b/m/e$e;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;ZLcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;ZLcom/bytedance/sdk/openadsdk/b/m/n;Lcom/bytedance/sdk/openadsdk/core/f0/a;Lb/a/a/a/a/a/a/f/b;)V

    invoke-static {v15, v13}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;Lb/a/a/a/a/a/a/h/a$a;)V

    goto :goto_1

    .line 69
    :cond_3
    iget-object v0, v11, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v13

    new-instance v14, Lcom/bytedance/sdk/openadsdk/b/m/e$f;

    move-object v0, v14

    move-object/from16 v1, p0

    move/from16 v2, p5

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/bytedance/sdk/openadsdk/b/m/e$f;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;ZLcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/b/m/n;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;ZLcom/bytedance/sdk/openadsdk/core/f0/a;)V

    invoke-virtual {v13, v10, v14}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/b/m/d$d;)V

    :goto_1
    const/4 v13, 0x0

    goto :goto_2

    .line 70
    :cond_4
    iget-object v1, v11, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v1

    invoke-virtual {v1, v6, v9}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/f0/a;)V

    :cond_5
    move v13, v0

    :goto_2
    if-eqz v13, :cond_6

    .line 71
    invoke-virtual/range {p3 .. p3}, Lcom/bytedance/sdk/openadsdk/b/m/n;->a()Lcom/bytedance/sdk/openadsdk/b/m/f;

    move-result-object v0

    invoke-interface {v12, v0}, Lcom/bytedance/sdk/openadsdk/api/PAGLoadListener;->onAdLoaded(Ljava/lang/Object;)V

    :cond_6
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/b/m/e$h;Lcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/n;Z)V
    .locals 13

    move-object v2, p1

    move-object/from16 v9, p3

    const/4 v10, 0x0

    if-eqz v9, :cond_0

    const/4 v0, 0x1

    if-nez p6, :cond_1

    .line 37
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->C()I

    move-result v1

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 38
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 39
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v1, v3, :cond_2

    .line 40
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object v8

    .line 41
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->u0()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/CacheDirFactory;->getICacheDir(I)Lb/a/a/a/a/a/a/d/b;

    move-result-object v0

    invoke-interface {v0}, Lb/a/a/a/a/a/a/d/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    move-result-object v11

    const-string v0, "material_meta"

    .line 42
    invoke-virtual {v11, v0, p1}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "ad_slot"

    move-object v3, p2

    .line 43
    invoke-virtual {v11, v0, p2}, Lb/a/a/a/a/a/a/f/c;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    new-instance v12, Lcom/bytedance/sdk/openadsdk/b/m/e$b;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    move/from16 v6, p6

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/b/m/e$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/e;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/utils/z;Lcom/bytedance/sdk/openadsdk/b/m/e$h;ZLcom/bytedance/sdk/openadsdk/b/m/n;Lb/a/a/a/a/a/a/f/b;)V

    invoke-static {v11, v12}, Lcom/bytedance/sdk/openadsdk/core/j0/d/a;->a(Lb/a/a/a/a/a/a/f/c;Lb/a/a/a/a/a/a/h/a$a;)V

    goto :goto_1

    :cond_2
    move-object v3, p2

    .line 45
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/a0;->d(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v3, p4

    invoke-static {p1, v1, v3}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/utils/z;)V

    :cond_3
    move v10, v0

    :goto_1
    if-eqz v10, :cond_4

    .line 46
    invoke-virtual/range {p5 .. p5}, Lcom/bytedance/sdk/openadsdk/b/m/n;->a()Lcom/bytedance/sdk/openadsdk/b/m/f;

    move-result-object v0

    invoke-interface {v9, v0}, Lcom/bytedance/sdk/openadsdk/api/PAGLoadListener;->onAdLoaded(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/e;)Lb/b/a/a/k/g;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->e:Lb/b/a/a/k/g;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/e;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->d:Ljava/util/List;

    return-object p0
.end method

.method private c()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->f:Lcom/bytedance/sdk/component/utils/w$b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/component/utils/w;->a(Lcom/bytedance/sdk/component/utils/w$b;Landroid/content/Context;)V

    return-void
.end method

.method private d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 14
    .line 15
    .line 16
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->f:Lcom/bytedance/sdk/component/utils/w$b;

    .line 17
    .line 18
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/w;->a(Lcom/bytedance/sdk/component/utils/w$b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    .line 20
    .line 21
    :catch_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 10
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 13
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getBidAdm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/common/b;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/common/b;)V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;ZLcom/bytedance/sdk/openadsdk/common/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object p1

    return-object p1
.end method

.method public b()V
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->b()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/b/m/d;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/f0/a;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/b/m/e;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/b/m/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/d;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->e:Lb/b/a/a/k/g;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/component/utils/i;->a()Landroid/os/Handler;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->e:Lb/b/a/a/k/g;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    .line 16
    .line 17
    :catch_0
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/e;->e:Lb/b/a/a/k/g;

    .line 19
    .line 20
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/e;->d()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

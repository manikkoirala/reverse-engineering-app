.class public Lcom/bytedance/sdk/openadsdk/b/m/p/f;
.super Ljava/lang/Object;
.source "RewardFullExpressManager.java"


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/f;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    return-object p0
.end method


# virtual methods
.method public a([FLcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V
    .locals 16

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initExpressView() called with: size = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTAD.RFExpressM"

    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S()I

    move-result v0

    .line 4
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    .line 5
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v0

    const/4 v10, 0x0

    aget v1, p1, v10

    const/4 v2, 0x1

    .line 6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 7
    aget v2, p1, v2

    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v0

    .line 9
    iget-object v1, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 10
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->R:Lcom/bytedance/sdk/openadsdk/b/m/p/j;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    if-eqz v0, :cond_0

    .line 11
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/j;->a(Lb/b/a/a/d/d/d;)V

    .line 12
    :cond_0
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/f$a;

    invoke-direct {v1, v7, v8, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/f$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/f;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/l;)V

    .line 13
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/f$b;

    invoke-direct {v1, v7, v8, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/f$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/f;Lcom/bytedance/sdk/openadsdk/core/j0/c/b;Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/api/banner/PAGBannerAdWrapperListener;)V

    .line 14
    new-instance v12, Lcom/bytedance/sdk/openadsdk/b/m/p/f$c;

    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/utils/a0;->f(Ljava/lang/String;)I

    move-result v5

    move-object v0, v12

    move-object/from16 v1, p0

    move-object/from16 v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/b/m/p/f$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/f;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 16
    iget-object v1, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    const/4 v13, 0x3

    const-string v14, "click_scence"

    if-eqz v1, :cond_1

    .line 17
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 18
    :cond_1
    invoke-interface {v0, v14, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    :goto_0
    invoke-virtual {v12, v0}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->a(Ljava/util/Map;)V

    .line 20
    new-instance v15, Lcom/bytedance/sdk/openadsdk/b/m/p/f$d;

    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/utils/a0;->f(Ljava/lang/String;)I

    move-result v5

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/b/m/p/f$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/f;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 22
    iget-object v1, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 23
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 24
    :cond_2
    invoke-interface {v0, v14, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    :goto_1
    invoke-virtual {v15, v0}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->a(Ljava/util/Map;)V

    .line 26
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0, v12, v15}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;Lcom/bytedance/sdk/openadsdk/core/nativeexpress/f;)V

    .line 27
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->X0()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 28
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    goto :goto_2

    .line 29
    :cond_3
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    :goto_2
    const/16 v1, 0x11

    .line 30
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 31
    iget-object v1, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->e()Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g()Z

    move-result v0

    if-nez v0, :cond_4

    .line 33
    invoke-virtual {v9, v10}, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a(Z)V

    .line 34
    :cond_4
    iget-object v0, v7, Lcom/bytedance/sdk/openadsdk/b/m/p/f;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->l()V

    return-void
.end method

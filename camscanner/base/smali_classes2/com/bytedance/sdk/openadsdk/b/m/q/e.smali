.class public Lcom/bytedance/sdk/openadsdk/b/m/q/e;
.super Lcom/bytedance/sdk/openadsdk/b/m/q/b;
.source "RewardFullTypeDoubleAds.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/k/f;


# instance fields
.field private final o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 5
    .line 6
    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 7
    .line 8
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V
    .locals 2

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "req_id"

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->U()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/b;

    move-result-object p0

    const-string v1, "choose_ad_start_show"

    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/l/b;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public a(IJ)V
    .locals 3

    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->p:Z

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/r;->K(I)V

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    invoke-static {v0, v2, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;IJ)V

    .line 12
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/r;->p1()Lcom/bytedance/sdk/openadsdk/core/f0/a;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/b/m/a;->a(Lcom/bytedance/sdk/openadsdk/core/f0/a;I)V

    .line 13
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/q/e$a;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/e$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/q/e;)V

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/y;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Landroid/widget/FrameLayout;)V
    .locals 0

    .line 1
    return-void
.end method

.method public a(Lb/b/a/a/d/f/n;)V
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;->c(Lb/b/a/a/d/f/n;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/component/reward/view/RewardFullBaseLayout;)V
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/b/m/q/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->o:Lcom/bytedance/sdk/openadsdk/core/f0/r;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Landroid/widget/FrameLayout;)V

    return-void

    .line 7
    :cond_0
    invoke-super {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->a(Lcom/bytedance/sdk/openadsdk/component/reward/view/RewardFullBaseLayout;)V

    return-void
.end method

.method public a()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->p:Z

    return v0
.end method

.method public d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/e;->p:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 17
    .line 18
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->d()Lcom/bytedance/sdk/openadsdk/component/reward/view/FullRewardExpressView;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/NativeExpressView;->getJsObject()Lcom/bytedance/sdk/openadsdk/core/x;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/k/f;)Lcom/bytedance/sdk/openadsdk/core/x;

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->d()V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 36
    .line 37
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->m()V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/q/a;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 49
    .line 50
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->d()V

    .line 53
    .line 54
    .line 55
    :cond_2
    :goto_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method protected f()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public q()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public r()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

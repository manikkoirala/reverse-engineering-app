.class public Lcom/bytedance/sdk/openadsdk/b/m/p/n;
.super Ljava/lang/Object;
.source "VastEndCardManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/k/g;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field private c:Lcom/bytedance/sdk/component/widget/SSWebView;

.field private d:Landroid/widget/ImageView;

.field private e:Z

.field private f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

.field private g:Z

.field private volatile h:Z

.field private final i:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private j:I


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->i:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 9
    .line 10
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a:Landroid/app/Activity;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/n;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    return-object p0
.end method

.method private a(IILcom/bytedance/sdk/openadsdk/core/i0/a;)V
    .locals 2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 40
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    if-eqz p3, :cond_2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a:Landroid/app/Activity;

    if-nez p3, :cond_0

    goto :goto_1

    .line 41
    :cond_0
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result p3

    .line 42
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result v0

    int-to-float p1, p1

    int-to-float p2, p2

    div-float/2addr p1, p2

    int-to-float p2, p3

    int-to-float v1, v0

    div-float v1, p2, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_1

    mul-float p2, p2, p1

    float-to-double p1, p2

    .line 43
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p3, p1

    goto :goto_0

    :cond_1
    div-float/2addr p2, p1

    float-to-double p1, p2

    .line 44
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int v0, p1

    .line 45
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    .line 46
    iput p3, p1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 47
    iput v0, p1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 48
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 7

    .line 31
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 32
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->h:Z

    .line 33
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    move-object v6, v0

    const v0, 0x7fffffff

    if-ne p1, v0, :cond_2

    .line 35
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    const-string p2, "load_vast_endcard_success"

    const/4 v0, 0x0

    invoke-static {p1, v6, p2, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void

    .line 36
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/n$c;

    const-string v3, "load_vast_endcard_fail"

    move-object v1, v0

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/b/m/p/n$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/n;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lb/b/a/a/k/g;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/n;ILjava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/n;Ljava/lang/String;)Z
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/n;Z)Z
    .locals 0

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->g:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    if-eqz p1, :cond_1

    .line 37
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    if-nez v0, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->e(Ljava/lang/String;)V

    .line 39
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->onClick(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method private b()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->l()V

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDisplayZoomControls(Z)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/n$d;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/n$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/n;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/n$e;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/n$e;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/n;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/b/m/p/n;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->e()V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/b/m/p/n;)Lcom/bytedance/sdk/openadsdk/core/f0/q;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    return-object p0
.end method

.method private e()V
    .locals 2

    .line 1
    const v0, 0x7fffffff

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a(ILjava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public a()V
    .locals 9

    .line 5
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils$AudioInfoReceiver;->a(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils;->d()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->j:I

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 8
    new-instance v0, Lcom/bytedance/sdk/openadsdk/b/m/p/n$a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v1

    const-string v2, "VAST_END_CARD"

    invoke-direct {v0, p0, v2, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/n$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/n;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/i0/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->c()Lcom/bytedance/sdk/openadsdk/core/i0/c;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 10
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->d()Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_0

    .line 12
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->e:Z

    .line 13
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a:Landroid/app/Activity;

    sget v3, Lcom/bytedance/sdk/openadsdk/utils/h;->h:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    .line 14
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->f()I

    move-result v2

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->b()I

    move-result v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a(IILcom/bytedance/sdk/openadsdk/core/i0/a;)V

    .line 15
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/h/d;->a(Ljava/lang/String;)Lb/b/a/a/h/j;

    move-result-object v2

    .line 16
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->f()I

    move-result v3

    invoke-interface {v2, v3}, Lb/b/a/a/h/j;->b(I)Lb/b/a/a/h/j;

    move-result-object v2

    .line 17
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->b()I

    move-result v0

    invoke-interface {v2, v0}, Lb/b/a/a/h/j;->a(I)Lb/b/a/a/h/j;

    move-result-object v0

    .line 18
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->g(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v2}, Lb/b/a/a/h/j;->d(I)Lb/b/a/a/h/j;

    move-result-object v0

    .line 19
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/b0;->i(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v2}, Lb/b/a/a/h/j;->c(I)Lb/b/a/a/h/j;

    move-result-object v0

    sget-object v2, Lb/b/a/a/h/u;->b:Lb/b/a/a/h/u;

    .line 20
    invoke-interface {v0, v2}, Lb/b/a/a/h/j;->oO80(Lb/b/a/a/h/u;)Lb/b/a/a/h/j;

    move-result-object v0

    new-instance v2, Lcom/bytedance/sdk/openadsdk/h/b;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    new-instance v4, Lcom/bytedance/sdk/openadsdk/b/m/p/n$b;

    invoke-direct {v4, p0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/n$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/n;Ljava/lang/String;)V

    invoke-direct {v2, v3, v1, v4}, Lcom/bytedance/sdk/openadsdk/h/b;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lb/b/a/a/h/o;)V

    invoke-interface {v0, v2}, Lb/b/a/a/h/j;->O8(Lb/b/a/a/h/o;)Lb/b/a/a/h/i;

    goto :goto_1

    .line 21
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->a:Landroid/app/Activity;

    sget v2, Lcom/bytedance/sdk/openadsdk/utils/h;->i:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/component/widget/SSWebView;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 22
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b()V

    .line 23
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 24
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->e:Z

    const-string v1, "http"

    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 26
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 27
    :cond_1
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v5, v0

    goto :goto_0

    :cond_2
    move-object v5, v1

    .line 29
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    const-string v1, "UTF -8"

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 30
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    const/4 v4, 0x0

    const-string v6, "text/html"

    const-string v7, "UTF-8"

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/bytedance/sdk/component/widget/SSWebView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public a(I)V
    .locals 3

    .line 64
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->j:I

    if-nez v0, :cond_0

    if-lez p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->m()Lcom/bytedance/sdk/openadsdk/core/i0/d;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->i:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/d;->i(J)V

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    if-nez p1, :cond_1

    .line 66
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->m()Lcom/bytedance/sdk/openadsdk/core/i0/d;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->i:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/d;->e(J)V

    .line 67
    :cond_1
    :goto_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->j:I

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/c0/g;->a(Lcom/bytedance/sdk/openadsdk/core/c0/c;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/b/m/p/l;)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 51
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->e:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->g:Z

    if-eqz v2, :cond_1

    .line 53
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_2

    .line 55
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/component/widget/SSWebView;->setVisibility(I)V

    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->M0()Lcom/bytedance/sdk/openadsdk/core/i0/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i0/a;->c()Lcom/bytedance/sdk/openadsdk/core/i0/c;

    move-result-object v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide v1

    goto :goto_1

    :cond_3
    const-wide/16 v1, -0x1

    .line 61
    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i0/c;->b(J)V

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public c()V
    .locals 1

    .line 2
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/utils/DeviceUtils$AudioInfoReceiver;->b(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {v0}, Lcom/bytedance/sdk/component/widget/SSWebView;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/a0;->a(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->e:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->d:Landroid/widget/ImageView;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 13
    .line 14
    .line 15
    return v2

    .line 16
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->c:Lcom/bytedance/sdk/component/widget/SSWebView;

    .line 17
    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/n;->f:Lcom/bytedance/sdk/openadsdk/core/c0/g;

    .line 21
    .line 22
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/c0/c;->onClick(Landroid/view/View;)V

    .line 23
    .line 24
    .line 25
    return v2

    .line 26
    :cond_2
    return v1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.class public Lcom/bytedance/sdk/openadsdk/b/m/p/h;
.super Ljava/lang/Object;
.source "RewardFullReportManager.java"


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Lcom/bytedance/sdk/openadsdk/core/c0/e;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 5
    .line 6
    new-instance v6, Lcom/bytedance/sdk/openadsdk/b/m/p/h$a;

    .line 7
    .line 8
    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 9
    .line 10
    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 11
    .line 12
    iget-object v4, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 13
    .line 14
    iget-boolean v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 15
    .line 16
    const/4 v7, 0x7

    .line 17
    const/4 v8, 0x5

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    const/4 v5, 0x7

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v5, 0x5

    .line 23
    :goto_0
    move-object v0, v6

    .line 24
    move-object v1, p0

    .line 25
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/h$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/h;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;I)V

    .line 26
    .line 27
    .line 28
    iput-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 29
    .line 30
    new-instance v6, Lcom/bytedance/sdk/openadsdk/b/m/p/h$b;

    .line 31
    .line 32
    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 33
    .line 34
    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 35
    .line 36
    iget-object v4, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 37
    .line 38
    iget-boolean v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    .line 39
    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    const/4 v5, 0x7

    .line 43
    goto :goto_1

    .line 44
    :cond_1
    const/4 v5, 0x5

    .line 45
    :goto_1
    move-object v0, v6

    .line 46
    move-object v1, p0

    .line 47
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/b/m/p/h$b;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/h;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;I)V

    .line 48
    .line 49
    .line 50
    iput-object v6, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->b:Landroid/view/View$OnClickListener;

    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/h;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    return-object p0
.end method

.method private a()Lorg/json/JSONObject;
    .locals 5

    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l()J

    move-result-wide v0

    .line 39
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->j()I

    move-result v2

    .line 40
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v4, "duration"

    .line 41
    invoke-virtual {v3, v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "percent"

    .line 42
    invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-object v3

    :catchall_0
    const/4 v3, 0x0

    :catchall_1
    return-object v3
.end method

.method private a(Landroid/view/View;FFFFLandroid/util/SparseArray;III)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "FFFF",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;III)V"
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 9
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    .line 10
    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->e:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->d:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->c:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->b:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->q0:I

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->p:I

    if-eq p1, v0, :cond_1

    const v0, 0x1f000009

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->k:I

    if-eq p1, v0, :cond_1

    const v0, 0x1f00000b

    if-eq p1, v0, :cond_1

    sget v0, Lcom/bytedance/sdk/openadsdk/utils/h;->g:I

    if-ne p1, v0, :cond_3

    .line 11
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->b(Landroid/content/Context;)I

    move-result p1

    .line 12
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/content/Context;)F

    move-result v0

    .line 13
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/b0;->f(Landroid/content/Context;)F

    move-result v1

    .line 14
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;-><init>()V

    .line 15
    invoke-virtual {v2, p2}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 16
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 17
    invoke-virtual {p2, p4}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->e(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 18
    invoke-virtual {p2, p5}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->f(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 19
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(J)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    const-wide/16 p3, 0x0

    .line 20
    invoke-virtual {p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(J)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 21
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->d()Landroid/view/View;

    move-result-object p3

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;)[I

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    const/4 p3, 0x0

    .line 22
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->a(Landroid/view/View;)[I

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p4, p4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 23
    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->d()Landroid/view/View;

    move-result-object p4

    invoke-static {p4}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Landroid/view/View;)[I

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 24
    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/b0;->c(Landroid/view/View;)[I

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b([I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 25
    invoke-virtual {p2, p8}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->f(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 26
    invoke-virtual {p2, p9}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->c(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 27
    invoke-virtual {p2, p7}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->e(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 28
    invoke-virtual {p2, p6}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(Landroid/util/SparseArray;)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 29
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/h;->s()Z

    move-result p3

    if-eqz p3, :cond_2

    const/4 p3, 0x1

    goto :goto_0

    :cond_2
    const/4 p3, 0x2

    :goto_0
    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p2

    .line 30
    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->b(I)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p1

    .line 31
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p1

    .line 32
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->d(F)Lcom/bytedance/sdk/openadsdk/core/f0/i$b;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/i$b;->a()Lcom/bytedance/sdk/openadsdk/core/f0/i;

    move-result-object p4

    .line 34
    new-instance p7, Ljava/util/HashMap;

    invoke-direct {p7}, Ljava/util/HashMap;-><init>()V

    .line 35
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "duration"

    invoke-virtual {p7, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p3, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object p5, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    const-string p2, "click_other"

    const/4 p6, 0x1

    const/4 p8, -0x1

    invoke-static/range {p2 .. p8}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/core/f0/i;Ljava/lang/String;ZLjava/util/Map;I)V

    :cond_3
    :goto_1
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/h;Landroid/view/View;FFFFLandroid/util/SparseArray;III)V
    .locals 0

    .line 3
    invoke-direct/range {p0 .. p9}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->b(Landroid/view/View;FFFFLandroid/util/SparseArray;III)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/b/m/p/h;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    iget-boolean v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->f:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {v1, v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method private b(Landroid/view/View;FFFFLandroid/util/SparseArray;III)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "FFFF",
            "Landroid/util/SparseArray<",
            "Lcom/bytedance/sdk/openadsdk/core/c0/c$a;",
            ">;III)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 10
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->e:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    const-string v0, "click_play_star_level"

    .line 11
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_2

    .line 12
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->d:I

    if-eq v0, v1, :cond_a

    .line 13
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->g:I

    if-ne v0, v1, :cond_2

    goto/16 :goto_1

    .line 14
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->c:I

    if-ne v0, v1, :cond_3

    const-string v0, "click_play_source"

    .line 15
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto/16 :goto_2

    .line 16
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->b:I

    if-ne v0, v1, :cond_4

    const-string v0, "click_play_logo"

    .line 17
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    .line 18
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->l:I

    if-eq v0, v1, :cond_9

    .line 19
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->q0:I

    if-eq v0, v1, :cond_9

    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->p:I

    if-ne v0, v1, :cond_5

    goto :goto_0

    .line 21
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1f000009

    if-ne v0, v1, :cond_6

    .line 22
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_start_play"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    .line 23
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->k:I

    if-ne v0, v1, :cond_7

    .line 24
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_video"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    .line 25
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1f00000b

    if-eq v0, v1, :cond_8

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/bytedance/sdk/openadsdk/utils/h;->h:I

    if-ne v0, v1, :cond_b

    .line 27
    :cond_8
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "fallback_endcard_click"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    .line 28
    :cond_9
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "click_start_play_bar"

    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_2

    :cond_a
    :goto_1
    const-string v0, "click_play_star_nums"

    .line 29
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 30
    :cond_b
    :goto_2
    invoke-direct/range {p0 .. p9}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Landroid/view/View;FFFFLandroid/util/SparseArray;III)V

    return-void
.end method

.method private d()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->C()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v2, 0x1

    .line 14
    if-ne v0, v2, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_1
    return v1
.end method


# virtual methods
.method public a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 7
    :cond_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/h$c;

    invoke-direct {v1, p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h$c;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/h;Ljava/util/Map;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Z)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 44
    iget-object p1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->Q0()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->R0()Z

    move-result p1

    if-nez p1, :cond_1

    .line 45
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->b(Z)V

    .line 46
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->t()Lcom/bytedance/sdk/openadsdk/utils/z;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/utils/z;)V

    :cond_1
    return-void
.end method

.method public b()Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public b(Z)V
    .locals 8

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 3
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-wide v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->r:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-wide v6, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->r:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    iget-object v5, v5, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->G:Lcom/bytedance/sdk/openadsdk/b/m/p/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b()Lcom/bytedance/sdk/openadsdk/d/g;

    move-result-object v1

    invoke-static {v0, v4, v5, v1}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/d/g;)V

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iput-wide v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->r:J

    goto :goto_0

    .line 7
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->r:J

    .line 8
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-eqz p1, :cond_2

    const/4 p1, 0x4

    goto :goto_1

    :cond_2
    const/16 p1, 0x8

    :goto_1
    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/q/c/e;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;I)V

    return-void
.end method

.method public c()Lcom/bytedance/sdk/openadsdk/core/c0/e;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    return-object v0
.end method

.method public c(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->S()I

    move-result v0

    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->U()Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/c/b;->o()Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object v2

    if-eqz p1, :cond_1

    const/4 p1, 0x7

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    .line 5
    :goto_0
    invoke-virtual {v2, p1}, Lcom/bytedance/sdk/openadsdk/l/c/b;->a(I)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object p1

    .line 6
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/l/c/b;->e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object p1

    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->b(I)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 8
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/c/b;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/l/c/b;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/c/b;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/l/c/b;

    .line 10
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/l/b;->b(Lcom/bytedance/sdk/openadsdk/l/c/b;)V

    return-void
.end method

.method public e()Lcom/bytedance/sdk/openadsdk/core/c0/e;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 4
    .line 5
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 6
    .line 7
    const v2, 0x1020002

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->b(Landroid/view/View;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 20
    .line 21
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 22
    .line 23
    const v2, 0x1f000011

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->a(Landroid/view/View;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 34
    .line 35
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/d;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a()Lb/c/a/a/a/a/c;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    .line 43
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 44
    .line 45
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->H:Lcom/bytedance/sdk/openadsdk/b/m/p/d;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/d;->a()Lb/c/a/a/a/a/c;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/c0/b;->a(Lb/c/a/a/a/a/c;)V

    .line 54
    .line 55
    .line 56
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 57
    .line 58
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 59
    .line 60
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 66
    .line 67
    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/h$d;

    .line 68
    .line 69
    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/h$d;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/h;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/c0/a;->a(Lcom/bytedance/sdk/openadsdk/core/c0/a$a;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 76
    .line 77
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->T:Lcom/bytedance/sdk/openadsdk/component/reward/view/e;

    .line 78
    .line 79
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 80
    .line 81
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->b:Landroid/view/View$OnClickListener;

    .line 82
    .line 83
    invoke-virtual {v0, v1, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/reward/view/e;->a(Lcom/bytedance/sdk/openadsdk/core/c0/c;Landroid/view/View$OnTouchListener;Landroid/view/View$OnClickListener;)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 87
    .line 88
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->S:Lcom/bytedance/sdk/openadsdk/b/m/p/e;

    .line 89
    .line 90
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 91
    .line 92
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/e;->a(Lcom/bytedance/sdk/openadsdk/core/c0/e;)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->c:Lcom/bytedance/sdk/openadsdk/core/c0/e;

    .line 96
    .line 97
    return-object v0
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public f()V
    .locals 6

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 9
    .line 10
    instance-of v2, v1, Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 11
    .line 12
    const/4 v3, 0x1

    .line 13
    if-eqz v2, :cond_0

    .line 14
    .line 15
    move-object v2, v1

    .line 16
    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/r;->t1()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    :try_start_0
    const-string v2, "choose_one_ad_real_show"

    .line 25
    .line 26
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :catch_0
    move-exception v2

    .line 31
    const-string v4, "TTAD.RFReportManager"

    .line 32
    .line 33
    const-string v5, "reportShow json error"

    .line 34
    .line 35
    invoke-static {v4, v5, v2}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 39
    .line 40
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    if-eqz v2, :cond_1

    .line 47
    .line 48
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    if-eqz v2, :cond_1

    .line 53
    .line 54
    return-void

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 56
    .line 57
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 58
    .line 59
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 60
    .line 61
    .line 62
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 63
    .line 64
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 65
    .line 66
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 70
    .line 71
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 72
    .line 73
    instance-of v2, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

    .line 74
    .line 75
    if-eqz v2, :cond_2

    .line 76
    .line 77
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;->b()V

    .line 78
    .line 79
    .line 80
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 81
    .line 82
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 83
    .line 84
    const v2, 0x1020002

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    const/4 v2, -0x1

    .line 92
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/q/c/e;->a(Landroid/view/View;Lcom/bytedance/sdk/openadsdk/core/f0/q;I)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public g()V
    .locals 10

    .line 1
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/4 v2, 0x1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 18
    .line 19
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    const-string v3, "dynamic_show_type"

    .line 24
    .line 25
    :try_start_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 30
    .line 31
    .line 32
    const/4 v1, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 v1, 0x0

    .line 35
    :goto_0
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 36
    .line 37
    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 38
    .line 39
    instance-of v4, v3, Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 40
    .line 41
    if-eqz v4, :cond_1

    .line 42
    .line 43
    move-object v4, v3

    .line 44
    check-cast v4, Lcom/bytedance/sdk/openadsdk/core/f0/r;

    .line 45
    .line 46
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/f0/r;->t1()Z

    .line 47
    .line 48
    .line 49
    move-result v4

    .line 50
    if-eqz v4, :cond_1

    .line 51
    .line 52
    const-string v4, "choose_one_ad_real_show"

    .line 53
    .line 54
    invoke-virtual {v0, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 55
    .line 56
    .line 57
    :cond_1
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 58
    .line 59
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 60
    .line 61
    const v5, 0x1020002

    .line 62
    .line 63
    .line 64
    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    new-instance v6, Lorg/json/JSONObject;

    .line 69
    .line 70
    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 71
    .line 72
    .line 73
    const-string v7, "width"

    .line 74
    .line 75
    :try_start_2
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    .line 76
    .line 77
    .line 78
    move-result v8

    .line 79
    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 80
    .line 81
    .line 82
    const-string v7, "height"

    .line 83
    .line 84
    :try_start_3
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 85
    .line 86
    .line 87
    move-result v8

    .line 88
    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89
    .line 90
    .line 91
    const-string v7, "alpha"

    .line 92
    .line 93
    :try_start_4
    invoke-virtual {v4}, Landroid/view/View;->getAlpha()F

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    float-to-double v8, v4

    .line 98
    invoke-virtual {v6, v7, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 99
    .line 100
    .line 101
    :catchall_0
    const-string v4, "root_view"

    .line 102
    .line 103
    :try_start_5
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v6

    .line 107
    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 108
    .line 109
    .line 110
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 111
    .line 112
    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 113
    .line 114
    invoke-virtual {v4, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 115
    .line 116
    .line 117
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 118
    .line 119
    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 120
    .line 121
    invoke-static {v3, v2, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 125
    .line 126
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 127
    .line 128
    instance-of v2, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;

    .line 129
    .line 130
    if-eqz v2, :cond_2

    .line 131
    .line 132
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;->b()V

    .line 133
    .line 134
    .line 135
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 136
    .line 137
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 138
    .line 139
    invoke-virtual {v0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    if-eqz v1, :cond_3

    .line 144
    .line 145
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 146
    .line 147
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    .line 148
    .line 149
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->c()I

    .line 150
    .line 151
    .line 152
    move-result v1

    .line 153
    goto :goto_1

    .line 154
    :cond_3
    const/4 v1, -0x1

    .line 155
    :goto_1
    invoke-static {v0, v3, v1}, Lcom/bytedance/sdk/openadsdk/q/c/e;->a(Landroid/view/View;Lcom/bytedance/sdk/openadsdk/core/f0/q;I)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    .line 156
    .line 157
    .line 158
    goto :goto_2

    .line 159
    :catch_0
    move-exception v0

    .line 160
    const-string v1, "TTAD.RFReportManager"

    .line 161
    .line 162
    const-string v2, "reportShowWhenBindVideoAd error"

    .line 163
    .line 164
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    .line 166
    .line 167
    :goto_2
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public h()V
    .locals 4

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 9
    .line 10
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 17
    .line 18
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->I:Lcom/bytedance/sdk/openadsdk/b/m/p/g;

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/b/m/p/g;->a(Lorg/json/JSONObject;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 24
    .line 25
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_3

    .line 32
    .line 33
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 34
    .line 35
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->r()Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_1

    .line 42
    .line 43
    const/4 v1, 0x1

    .line 44
    goto :goto_0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 46
    .line 47
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->Q:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    const/4 v1, 0x2

    .line 56
    goto :goto_0

    .line 57
    :cond_2
    const/4 v1, 0x0

    .line 58
    :goto_0
    :try_start_0
    const-string v2, "endcard_content"

    .line 59
    .line 60
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .line 62
    .line 63
    :catch_0
    :cond_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 64
    .line 65
    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 66
    .line 67
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 68
    .line 69
    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->a:Ljava/lang/String;

    .line 70
    .line 71
    const-string v3, "click_close"

    .line 72
    .line 73
    invoke-static {v2, v1, v3, v0}, Lcom/bytedance/sdk/openadsdk/d/c;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

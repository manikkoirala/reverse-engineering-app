.class Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;
.super Ljava/lang/Object;
.source "RewardFullWebViewManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/b/m/p/m$r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final a:I

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->e()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->a:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 18

    move-object/from16 v0, p0

    .line 1
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m:Lcom/bytedance/sdk/openadsdk/d/j;

    if-eqz v1, :cond_0

    .line 2
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/d/j;->a(I)V

    :cond_0
    const/4 v1, 0x0

    .line 3
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const-wide/16 v3, -0x1

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-eqz v2, :cond_a

    const/4 v9, 0x3

    if-eq v2, v8, :cond_9

    if-eq v2, v7, :cond_2

    if-eq v2, v9, :cond_1

    const/4 v9, -0x1

    const/4 v11, -0x1

    goto/16 :goto_1

    :cond_1
    const/4 v9, 0x4

    const/4 v11, 0x4

    goto/16 :goto_1

    .line 4
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    .line 5
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    .line 6
    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v10

    sub-float/2addr v2, v10

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v10, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->a:I

    int-to-float v10, v10

    cmpl-float v2, v2, v10

    if-gez v2, :cond_3

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v2

    sub-float v2, v9, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v10, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->a:I

    int-to-float v10, v10

    cmpl-float v2, v2, v10

    if-ltz v2, :cond_4

    .line 7
    :cond_3
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z

    .line 8
    :cond_4
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v10

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    iget-object v12, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v12}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v12

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    invoke-static {v2, v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    .line 9
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v10

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    iget-object v12, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v12}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v12

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    invoke-static {v2, v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/16 v12, 0xc8

    const/high16 v2, 0x41000000    # 8.0f

    cmp-long v14, v10, v12

    if-lez v14, :cond_6

    .line 11
    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->j(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v10

    cmpl-float v10, v10, v2

    if-gtz v10, :cond_5

    iget-object v10, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->l(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v10

    cmpl-float v10, v10, v2

    if-lez v10, :cond_6

    :cond_5
    const/4 v10, 0x1

    goto :goto_0

    :cond_6
    const/4 v10, 0x2

    .line 12
    :goto_0
    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v11}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 13
    iget-object v11, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v11}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v11

    sub-float v11, v9, v11

    cmpl-float v2, v11, v2

    if-lez v2, :cond_7

    .line 14
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/common/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/common/f;->f()V

    .line 15
    :cond_7
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v2

    sub-float/2addr v9, v2

    const/high16 v2, -0x3f000000    # -8.0f

    cmpg-float v2, v9, v2

    if-gez v2, :cond_8

    .line 16
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->n(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/common/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/common/f;->d()V

    :cond_8
    move v11, v10

    goto :goto_1

    :cond_9
    const/4 v11, 0x3

    goto :goto_1

    .line 17
    :cond_a
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2, v8}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z

    .line 18
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    invoke-static {v2, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/util/SparseArray;)Landroid/util/SparseArray;

    .line 19
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v9

    invoke-static {v2, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    .line 20
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    invoke-static {v2, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    .line 21
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v2, v9, v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :try_start_1
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/component/widget/SSWebView;->getLandingPageClickBegin()J

    move-result-wide v9

    cmp-long v2, v9, v5

    if-lez v2, :cond_b

    .line 23
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)J

    move-result-wide v11

    cmp-long v2, v9, v11

    if-gez v2, :cond_b

    .line 24
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2, v9, v10}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;J)J

    .line 25
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLandingPageClickBegin(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 26
    :catch_0
    :cond_b
    :try_start_2
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    const/high16 v9, -0x40800000    # -1.0f

    invoke-static {v2, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->c(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    .line 27
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2, v9}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;F)F

    const/4 v11, 0x0

    .line 28
    :goto_1
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    new-instance v14, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getSize()F

    move-result v10

    float-to-double v12, v10

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPressure()F

    move-result v10

    float-to-double v3, v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-object v10, v14

    move-object v1, v14

    move-wide v14, v3

    invoke-direct/range {v10 .. v17}, Lcom/bytedance/sdk/openadsdk/core/c0/c$a;-><init>(IDDJ)V

    invoke-virtual {v2, v9, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 29
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v8, :cond_17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getAlpha()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    if-ne v1, v8, :cond_17

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    .line 30
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_c
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 31
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v2, "down_x"

    .line 32
    :try_start_3
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->g(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v2, "down_y"

    .line 33
    :try_start_4
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->h(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v2, "down_time"

    .line 34
    :try_start_5
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->i(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v2, "up_x"

    .line 35
    :try_start_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v2, "up_y"

    .line 36
    :try_start_7
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 38
    :try_start_8
    iget-object v4, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->getLandingPageClickEnd()J

    move-result-wide v9
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v4, v9, v5

    if-lez v4, :cond_d

    cmp-long v4, v9, v2

    if-gez v4, :cond_d

    .line 39
    :try_start_9
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/component/widget/SSWebView;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/component/widget/SSWebView;->setLandingPageClickEnd(J)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move-wide v2, v9

    :catch_2
    :cond_d
    :try_start_a
    const-string v4, "up_time"

    .line 40
    invoke-virtual {v1, v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    new-array v2, v7, [I

    .line 41
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->m(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 42
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    move-result-object v4

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    sget v5, Lcom/bytedance/sdk/openadsdk/utils/h;->f0:I

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/view/View;)Landroid/view/View;

    goto :goto_2

    .line 43
    :cond_e
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->b(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    move-result-object v4

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    const v5, 0x1f000011

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->a(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Landroid/view/View;)Landroid/view/View;

    .line 44
    :goto_2
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 45
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const-string v3, "button_x"

    const/4 v4, 0x0

    :try_start_b
    aget v5, v2, v4

    .line 46
    invoke-virtual {v1, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const-string v3, "button_y"

    :try_start_c
    aget v2, v2, v8

    .line 47
    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    const-string v2, "button_width"

    .line 48
    :try_start_d
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-string v2, "button_height"

    .line 49
    :try_start_e
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->p(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 50
    :cond_f
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_10

    new-array v2, v7, [I

    .line 51
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    const-string v3, "ad_x"

    const/4 v4, 0x0

    :try_start_f
    aget v5, v2, v4

    .line 52
    invoke-virtual {v1, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    const-string v3, "ad_y"

    :try_start_10
    aget v2, v2, v8

    .line 53
    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    const-string v2, "width"

    .line 54
    :try_start_11
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    const-string v2, "height"

    .line 55
    :try_start_12
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->q(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_10
    const-string v2, "toolType"

    move-object/from16 v3, p2

    const/4 v4, 0x0

    .line 56
    :try_start_13
    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v5

    invoke-virtual {v1, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    const-string v2, "deviceId"

    .line 57
    :try_start_14
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    const-string v2, "source"

    .line 58
    :try_start_15
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    const-string v2, "ft"

    .line 59
    :try_start_16
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->f(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h;->a()Lcom/bytedance/sdk/openadsdk/core/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/h;->s()Z

    move-result v4

    if-eqz v4, :cond_11

    const/4 v4, 0x1

    goto :goto_3

    :cond_11
    const/4 v4, 0x2

    :goto_3
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/f0/i;->a(Landroid/util/SparseArray;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    const-string v2, "user_behavior_type"

    .line 60
    :try_start_17
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->e(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v3, 0x1

    goto :goto_4

    :cond_12
    const/4 v3, 0x2

    :goto_4
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "click_scence"

    .line 61
    invoke-virtual {v1, v2, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 62
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 63
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->t(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->a(Lorg/json/JSONObject;)V

    .line 64
    :cond_13
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->o(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v2

    if-nez v2, :cond_16

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->e(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v2

    if-eqz v2, :cond_14

    goto :goto_6

    .line 65
    :cond_14
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->r(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Z

    move-result v2
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    const-string v3, "click"

    if-eqz v2, :cond_15

    .line 66
    :try_start_18
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v2

    const-string v4, "rewarded_video"

    invoke-static {v2, v4, v3, v1}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_5

    .line 67
    :cond_15
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->s(Lcom/bytedance/sdk/openadsdk/b/m/p/m;)Lcom/bytedance/sdk/openadsdk/core/f0/q;

    move-result-object v2

    const-string v4, "fullscreen_interstitial_ad"

    invoke-static {v2, v4, v3, v1}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 68
    :goto_5
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/m$d;->b:Lcom/bytedance/sdk/openadsdk/b/m/p/m;

    invoke-static {v1, v8}, Lcom/bytedance/sdk/openadsdk/b/m/p/m;->d(Lcom/bytedance/sdk/openadsdk/b/m/p/m;Z)Z
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto :goto_7

    :cond_16
    :goto_6
    const/4 v1, 0x0

    return v1

    :catchall_0
    :cond_17
    :goto_7
    const/4 v1, 0x0

    return v1
.end method

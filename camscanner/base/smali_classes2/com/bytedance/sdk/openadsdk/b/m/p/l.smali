.class public Lcom/bytedance/sdk/openadsdk/b/m/p/l;
.super Ljava/lang/Object;
.source "RewardFullVideoPlayerManager.java"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

.field private c:Landroid/widget/FrameLayout;

.field private final d:Ljava/lang/String;

.field private e:J

.field protected f:Z

.field g:Ljava/lang/String;

.field protected h:Z

.field i:Lb/a/a/a/a/a/a/g/c;

.field private j:J

.field private k:Z

.field private final l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

.field private m:Z

.field private n:Lcom/bytedance/sdk/openadsdk/d/g;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/b/m/p/a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->f:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h:Z

    .line 8
    .line 9
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    .line 10
    .line 11
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a:Landroid/app/Activity;

    .line 14
    .line 15
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 16
    .line 17
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 18
    .line 19
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->g:Ljava/lang/String;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->d:Ljava/lang/String;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private a(JZ)Z
    .locals 3

    const-string v0, "playVideo start"

    const-string v1, "TTAD.RFVideoPlayerMag"

    .line 56
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->u0()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/CacheDirFactory;->getICacheDir(I)Lb/a/a/a/a/a/a/d/b;

    move-result-object v0

    invoke-interface {v0}, Lb/a/a/a/a/a/a/d/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 59
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object v2

    invoke-virtual {v2}, Lb/a/a/a/a/a/a/f/b;->oo88o8O()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/f0/q;)Lcom/bytedance/sdk/openadsdk/core/j0/a/b;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/a/f/c;->b(Ljava/lang/String;)V

    .line 63
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/a/f/c;->f(I)V

    .line 64
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/a/f/c;->b(I)V

    .line 65
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb/a/a/a/a/a/a/f/c;->e(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0, p1, p2}, Lb/a/a/a/a/a/a/f/c;->a(J)V

    .line 67
    invoke-virtual {v0, p3}, Lb/a/a/a/a/a/a/f/c;->a(Z)V

    .line 68
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->V:Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/activity/TTBaseVideoActivity;->l()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->g()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    .line 69
    iput p1, v0, Lb/a/a/a/a/a/a/f/c;->q:I

    .line 70
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    invoke-interface {p1, v0}, Lb/a/a/a/a/a/a/g/c;->a(Lb/a/a/a/a/a/a/f/c;)Z

    move-result p1

    return p1

    :cond_3
    :goto_0
    const-string p1, "playVideo controller is Empty"

    .line 71
    invoke-static {v1, p1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method private p()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 13
    .line 14
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->o()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e:J

    .line 19
    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 21
    .line 22
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->f()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_1

    .line 31
    .line 32
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 33
    .line 34
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->d()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-nez v0, :cond_2

    .line 43
    .line 44
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 45
    .line 46
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->d()V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 50
    .line 51
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->e()V

    .line 52
    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->f:Z

    .line 56
    .line 57
    :cond_2
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
.end method


# virtual methods
.method public A()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->w()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h:Z

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->B()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception v0

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "onPause throw Exception :"

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "TTAD.RFVideoPlayerMag"

    .line 37
    .line 38
    invoke-static {v1, v0}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public B()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->d()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public C()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->e()V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public D()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->c()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public E()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->f()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public a(II)V
    .locals 3

    .line 10
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;-><init>()V

    .line 12
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->d()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->a(J)V

    .line 13
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->k()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(J)V

    .line 14
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->b(J)V

    .line 15
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->d(I)V

    .line 16
    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;->c(I)V

    .line 17
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    invoke-interface {p1}, Lb/a/a/a/a/a/a/g/c;->g()Lb/a/a/a/a/a/a/g/b;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/d/r/a/a;->a(Lb/a/a/a/a/a/a/e/a;Lcom/bytedance/sdk/openadsdk/d/r/b/o$a;)V

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 0

    .line 9
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e:J

    return-void
.end method

.method public a(Landroid/widget/FrameLayout;Lcom/bytedance/sdk/openadsdk/d/g;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->k:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->k:Z

    .line 3
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->c:Landroid/widget/FrameLayout;

    .line 4
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->n:Lcom/bytedance/sdk/openadsdk/d/g;

    .line 5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 6
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->c:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-direct {p1, v0, v1, v2, p2}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/f0/q;Lcom/bytedance/sdk/openadsdk/d/g;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 7
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->m:Z

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->d(Z)V

    goto :goto_0

    .line 8
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/openadsdk/b/m/c;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/b/m/c;-><init>(Lcom/bytedance/sdk/openadsdk/core/f0/q;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    :goto_0
    return-void
.end method

.method public a(Lb/a/a/a/a/a/a/g/c$a;)V
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 21
    invoke-interface {v0, p1}, Lb/a/a/a/a/a/a/g/c;->a(Lb/a/a/a/a/a/a/g/c$a;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->u:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean v1, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->e:Z

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/t;->h(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 49
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->c(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/o;->d()Lcom/bytedance/sdk/openadsdk/core/settings/d;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->o:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/settings/d;->j(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->J:Lcom/bytedance/sdk/openadsdk/component/reward/view/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/reward/view/c;->h()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 50
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/b/m/q/b;->q()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    .line 52
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    const/16 v0, 0x12c

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 53
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object p1

    .line 54
    iput v0, p1, Landroid/os/Message;->what:I

    .line 55
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->X:Lcom/bytedance/sdk/component/utils/y;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V
    .locals 2

    const/4 v0, 0x0

    .line 74
    :try_start_0
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h:Z

    .line 75
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->p()V

    .line 77
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    return-void

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->v()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->E()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onContinue throw Exception :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTAD.RFVideoPlayerMag"

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 12

    .line 22
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 23
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 24
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->l()J

    move-result-wide v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 25
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    move-result-object v0

    .line 26
    invoke-static {v1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/utils/a0;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;JLb/a/a/a/a/a/a/a;)Lorg/json/JSONObject;

    move-result-object v10

    .line 27
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->d:Ljava/lang/String;

    .line 28
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e()J

    move-result-wide v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->j()I

    move-result v9

    iget-object v11, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->n:Lcom/bytedance/sdk/openadsdk/d/g;

    move-object v6, p1

    .line 29
    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;JILorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/d/g;)V

    .line 30
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "event tag:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", TotalPlayDuration="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->k()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ",mBasevideoController.getPct()="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->j()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTAD.RFVideoPlayerMag"

    invoke-static {v0, p1}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->z()V

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 19
    invoke-interface {v0, p1}, Lb/a/a/a/a/a/a/g/c;->a(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->C()V

    .line 73
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/l;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/b/m/p/l$a;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l$a;-><init>(Lcom/bytedance/sdk/openadsdk/b/m/p/l;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(ZLcom/bytedance/sdk/openadsdk/core/j0/c/b;Z)V
    .locals 0

    if-eqz p3, :cond_2

    if-nez p1, :cond_2

    .line 81
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h:Z

    if-eqz p1, :cond_0

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->v()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->E()V

    goto :goto_0

    .line 84
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->p()V

    .line 85
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public a()Z
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 33
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 34
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->i()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    if-eqz v1, :cond_1

    .line 37
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;->Q()V

    :cond_1
    return v2

    .line 38
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 39
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->c(Z)V

    .line 40
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    if-eqz v1, :cond_3

    .line 41
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;->Q()V

    :cond_3
    return v2

    :cond_4
    return v1
.end method

.method public a(JZLjava/util/Map;Lcom/bytedance/sdk/openadsdk/b/m/q/b;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/bytedance/sdk/openadsdk/b/m/q/b;",
            ")Z"
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->s()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->a:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    if-eqz p3, :cond_2

    .line 44
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->t()Z

    move-result v0

    if-nez v0, :cond_3

    .line 45
    :cond_2
    invoke-virtual {p0, p5}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a(Lcom/bytedance/sdk/openadsdk/b/m/q/b;)V

    .line 46
    :cond_3
    :try_start_0
    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-boolean p5, p5, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->d:Z

    invoke-direct {p0, p1, p2, p5}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a(JZ)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-eqz v1, :cond_4

    if-nez p3, :cond_4

    .line 47
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->l:Lcom/bytedance/sdk/openadsdk/b/m/p/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/b/m/p/a;->L:Lcom/bytedance/sdk/openadsdk/b/m/p/h;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/b/m/p/h;->a(Ljava/util/Map;)V

    :cond_4
    return v1
.end method

.method public b()Lcom/bytedance/sdk/openadsdk/d/g;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->n:Lcom/bytedance/sdk/openadsdk/d/g;

    return-object v0
.end method

.method public b(J)V
    .locals 0

    .line 2
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->j:J

    return-void
.end method

.method protected b(Lcom/bytedance/sdk/openadsdk/core/j0/c/b;)V
    .locals 3

    .line 6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->h()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-interface {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/j0/c/b;->a(JZ)Z

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->g:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0, p1}, Lb/a/a/a/a/a/a/g/c;->e(Z)V

    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 3
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->j()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->f:Z

    return-void
.end method

.method public d()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->l()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public d(Z)V
    .locals 1

    .line 3
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->m:Z

    .line 4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object p1

    invoke-virtual {p1}, Lb/a/a/a/a/a/a/f/b;->o〇〇0〇()I

    move-result p1

    .line 6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d(I)V

    goto :goto_0

    .line 7
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lb/a/a/a/a/a/a/f/b;->〇oo〇(I)V

    .line 8
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/j0/a/a;->d(I)V

    :goto_0
    return-void
.end method

.method public e()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->o()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    return-wide v0

    .line 10
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e:J

    .line 11
    .line 12
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public f()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->a()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    return-wide v0

    .line 10
    :cond_0
    const-wide/16 v0, 0x0

    .line 11
    .line 12
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public g()Landroid/view/View;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;->H()Lcom/bykv/vk/openvk/component/video/api/renderview/b;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Landroid/view/View;

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    return-object v0
.end method

.method public h()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->e:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public i()Lb/a/a/a/a/a/a/e/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->g()Lb/a/a/a/a/a/a/g/b;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public j()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->m()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public k()J
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->a()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 10
    .line 11
    invoke-interface {v2}, Lb/a/a/a/a/a/a/g/c;->l()J

    .line 12
    .line 13
    .line 14
    move-result-wide v2

    .line 15
    add-long/2addr v0, v2

    .line 16
    return-wide v0

    .line 17
    :cond_0
    const-wide/16 v0, 0x0

    .line 18
    .line 19
    return-wide v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public l()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->a()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    return-wide v0

    .line 10
    :cond_0
    const-wide/16 v0, 0x0

    .line 11
    .line 12
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->g:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public n()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->j:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o()D
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/o;->b(Lcom/bytedance/sdk/openadsdk/core/f0/q;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->h0()Lcom/bytedance/sdk/openadsdk/core/f0/p;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/p;->c()J

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    long-to-double v0, v0

    .line 28
    return-wide v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->b:Lcom/bytedance/sdk/openadsdk/core/f0/q;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/f0/q;->N0()Lb/a/a/a/a/a/a/f/b;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/b;->o0ooO()D

    .line 38
    .line 39
    .line 40
    move-result-wide v1

    .line 41
    invoke-virtual {v0}, Lb/a/a/a/a/a/a/f/b;->o〇〇0〇()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    int-to-double v3, v0

    .line 46
    mul-double v1, v1, v3

    .line 47
    .line 48
    return-wide v1

    .line 49
    :cond_1
    const-wide/16 v0, 0x0

    .line 50
    .line 51
    return-wide v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public q()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->e()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public r()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->f:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public s()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public t()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public u()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->b()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public v()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->i()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public w()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->h()Lb/a/a/a/a/a/a/a;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lb/a/a/a/a/a/a/a;->h()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public x()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->w()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 8
    .line 9
    invoke-interface {v0}, Lb/a/a/a/a/a/a/g/c;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :catchall_0
    move-exception v0

    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v2, "RewardFullVideoPlayerManager onPause throw Exception :"

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;->M()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public z()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/b/m/p/l;->i:Lb/a/a/a/a/a/a/g/c;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/j0/c/a;->N()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

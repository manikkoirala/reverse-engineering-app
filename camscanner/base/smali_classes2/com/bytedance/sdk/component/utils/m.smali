.class public Lcom/bytedance/sdk/component/utils/m;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field private static a:Z = false

.field private static b:I = 0x4

.field private static c:Lb/b/a/a/a; = null

.field private static d:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static varargs a([Ljava/lang/Object;)Ljava/lang/String;
    .locals 4

    if-eqz p0, :cond_3

    .line 17
    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_2

    .line 18
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    if-eqz v3, :cond_1

    .line 20
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v3, " null "

    .line 21
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v3, " "

    .line 22
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    :goto_2
    const-string p0, ""

    return-object p0
.end method

.method public static a()V
    .locals 1

    const/4 v0, 0x0

    .line 2
    sput-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    const/4 v0, 0x7

    .line 3
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->a(I)V

    return-void
.end method

.method public static a(I)V
    .locals 0

    .line 1
    sput p0, Lcom/bytedance/sdk/component/utils/m;->b:I

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .line 4
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Logger"

    .line 5
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/utils/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 6
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 7
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v0, 0x3

    if-gt p1, v0, :cond_2

    .line 8
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 15
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    .line 16
    :cond_0
    invoke-static {p0, p1}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2, p3}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 9
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 10
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 p2, 0x3

    if-gt p1, p2, :cond_2

    .line 11
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 12
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 13
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 14
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .line 2
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Logger"

    .line 3
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/utils/m;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 4
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 5
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v0, 0x6

    if-gt p1, v0, :cond_2

    .line 6
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 7
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 8
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 p2, 0x6

    if-gt p1, p2, :cond_2

    .line 9
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 10
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 11
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_2

    .line 12
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static b()Z
    .locals 2

    .line 1
    sget v0, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 8
    sget-object v0, Lcom/bytedance/sdk/component/utils/m;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/bytedance/sdk/component/utils/m;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]-["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "]"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 10
    sget-object v0, Lcom/bytedance/sdk/component/utils/m;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]-["

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v0, p1

    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 5
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    return-void

    .line 6
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 p2, 0x5

    if-gt p1, p2, :cond_2

    .line 7
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 2
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 3
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_2

    .line 4
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static c()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    return v0
.end method

.method public static d()V
    .locals 1

    const/4 v0, 0x1

    .line 2
    sput-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    const/4 v0, 0x3

    .line 3
    invoke-static {v0}, Lcom/bytedance/sdk/component/utils/m;->a(I)V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/bytedance/sdk/component/utils/m;->d:Ljava/lang/String;

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 4
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 5
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v0, 0x4

    if-gt p1, v0, :cond_2

    .line 6
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 7
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 8
    :cond_1
    sget v0, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    .line 9
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/component/utils/m;->a([Ljava/lang/Object;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    .line 4
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Logger"

    .line 5
    invoke-static {v0, p0}, Lcom/bytedance/sdk/component/utils/m;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 2
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    const/4 v0, 0x2

    if-gt p1, v0, :cond_2

    .line 3
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-boolean v0, Lcom/bytedance/sdk/component/utils/m;->a:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-nez p1, :cond_1

    .line 7
    .line 8
    return-void

    .line 9
    :cond_1
    sget p1, Lcom/bytedance/sdk/component/utils/m;->b:I

    .line 10
    .line 11
    const/4 v0, 0x5

    .line 12
    if-gt p1, v0, :cond_2

    .line 13
    .line 14
    invoke-static {p0}, Lcom/bytedance/sdk/component/utils/m;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    :cond_2
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

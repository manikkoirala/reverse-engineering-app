.class Lcom/bytedance/sdk/component/adexpress/widget/ShakeAnimationView$d;
.super Ljava/lang/Object;
.source "ShakeAnimationView.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/component/adexpress/widget/ShakeAnimationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/component/adexpress/widget/ShakeAnimationView$a;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/bytedance/sdk/component/adexpress/widget/ShakeAnimationView$d;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 2

    .line 1
    const/high16 v0, 0x3e800000    # 0.25f

    .line 2
    .line 3
    const/high16 v1, 0x3f000000    # 0.5f

    .line 4
    .line 5
    cmpg-float v0, p1, v0

    .line 6
    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    const/high16 v0, -0x40000000    # -2.0f

    .line 10
    .line 11
    mul-float p1, p1, v0

    .line 12
    .line 13
    add-float/2addr p1, v1

    .line 14
    return p1

    .line 15
    :cond_0
    cmpg-float v0, p1, v1

    .line 16
    .line 17
    if-gtz v0, :cond_1

    .line 18
    .line 19
    const/high16 v0, 0x40800000    # 4.0f

    .line 20
    .line 21
    mul-float p1, p1, v0

    .line 22
    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    .line 24
    .line 25
    sub-float/2addr p1, v0

    .line 26
    return p1

    .line 27
    :cond_1
    const/high16 v0, 0x3f400000    # 0.75f

    .line 28
    .line 29
    cmpg-float v0, p1, v0

    .line 30
    .line 31
    if-gtz v0, :cond_2

    .line 32
    .line 33
    const/high16 v0, -0x3f800000    # -4.0f

    .line 34
    .line 35
    mul-float p1, p1, v0

    .line 36
    .line 37
    const/high16 v0, 0x40400000    # 3.0f

    .line 38
    .line 39
    add-float/2addr p1, v0

    .line 40
    return p1

    .line 41
    :cond_2
    const/high16 v0, 0x40000000    # 2.0f

    .line 42
    .line 43
    mul-float p1, p1, v0

    .line 44
    .line 45
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 46
    .line 47
    sub-float/2addr p1, v0

    .line 48
    return p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

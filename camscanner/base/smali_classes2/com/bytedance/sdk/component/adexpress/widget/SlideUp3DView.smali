.class public Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;
.super Landroid/widget/FrameLayout;
.source "SlideUp3DView.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/graphics/drawable/AnimationDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/bytedance/sdk/component/utils/t;->f(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {v0, p2, p1}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    return-void
.end method

.method private b()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    .line 7
    .line 8
    const-string v0, "tt_slide_up_1"

    .line 9
    .line 10
    const/16 v1, 0x64

    .line 11
    .line 12
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const-string v0, "tt_slide_up_2"

    .line 16
    .line 17
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "tt_slide_up_3"

    .line 21
    .line 22
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const-string v0, "tt_slide_up_4"

    .line 26
    .line 27
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const-string v0, "tt_slide_up_5"

    .line 31
    .line 32
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    const-string v0, "tt_slide_up_6"

    .line 36
    .line 37
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const-string v0, "tt_slide_up_7"

    .line 41
    .line 42
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const-string v0, "tt_slide_up_8"

    .line 46
    .line 47
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 48
    .line 49
    .line 50
    const-string v0, "tt_slide_up_9"

    .line 51
    .line 52
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 53
    .line 54
    .line 55
    const-string v0, "tt_slide_up_10"

    .line 56
    .line 57
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const-string v0, "tt_slide_up_11"

    .line 61
    .line 62
    const/16 v1, 0x78

    .line 63
    .line 64
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 65
    .line 66
    .line 67
    const-string v0, "tt_slide_up_12"

    .line 68
    .line 69
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 70
    .line 71
    .line 72
    const-string v0, "tt_slide_up_13"

    .line 73
    .line 74
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const-string v0, "tt_slide_up_14"

    .line 78
    .line 79
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 80
    .line 81
    .line 82
    const-string v0, "tt_slide_up_15"

    .line 83
    .line 84
    invoke-direct {p0, v1, v0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a(ILjava/lang/String;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    .line 88
    .line 89
    const/4 v1, 0x0

    .line 90
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 91
    .line 92
    .line 93
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    :cond_0
    return-void
.end method

.method public c()V
    .locals 10

    .line 1
    new-instance v0, Landroid/widget/ImageView;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    iput-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->b:Landroid/widget/ImageView;

    .line 9
    .line 10
    new-instance v0, Landroid/widget/TextView;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 13
    .line 14
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 18
    .line 19
    new-instance v0, Landroid/widget/TextView;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 22
    .line 23
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 27
    .line 28
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 31
    .line 32
    const/high16 v2, 0x43480000    # 200.0f

    .line 33
    .line 34
    invoke-static {v1, v2}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    float-to-int v1, v1

    .line 39
    iget-object v3, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 40
    .line 41
    invoke-static {v3, v2}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    float-to-int v2, v2

    .line 46
    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 47
    .line 48
    .line 49
    const/16 v1, 0x31

    .line 50
    .line 51
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 52
    .line 53
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 54
    .line 55
    const/high16 v2, 0x428c0000    # 70.0f

    .line 56
    .line 57
    invoke-static {v1, v2}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    float-to-int v1, v1

    .line 62
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 63
    .line 64
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 65
    .line 66
    const/4 v2, -0x2

    .line 67
    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 68
    .line 69
    .line 70
    const/16 v3, 0x51

    .line 71
    .line 72
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 73
    .line 74
    iget-object v4, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 75
    .line 76
    const/high16 v5, 0x41c80000    # 25.0f

    .line 77
    .line 78
    invoke-static {v4, v5}, Lb/b/a/a/d/g/d;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    float-to-int v4, v4

    .line 83
    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 84
    .line 85
    iget-object v4, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 86
    .line 87
    iget-object v5, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->a:Landroid/content/Context;

    .line 88
    .line 89
    const-string v6, "tt_slide_up_3d"

    .line 90
    .line 91
    invoke-static {v5, v6}, Lcom/bytedance/sdk/component/utils/t;->j(Landroid/content/Context;Ljava/lang/String;)I

    .line 92
    .line 93
    .line 94
    move-result v5

    .line 95
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 96
    .line 97
    .line 98
    iget-object v4, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 99
    .line 100
    const/4 v5, -0x1

    .line 101
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    .line 103
    .line 104
    iget-object v4, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 105
    .line 106
    const/high16 v6, 0x41c00000    # 24.0f

    .line 107
    .line 108
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 109
    .line 110
    .line 111
    iget-object v4, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 112
    .line 113
    const-string v6, "#59000000"

    .line 114
    .line 115
    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 116
    .line 117
    .line 118
    move-result v7

    .line 119
    const/high16 v8, 0x40800000    # 4.0f

    .line 120
    .line 121
    const/high16 v9, 0x40400000    # 3.0f

    .line 122
    .line 123
    invoke-virtual {v4, v8, v9, v9, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 124
    .line 125
    .line 126
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    .line 127
    .line 128
    invoke-direct {v4, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 129
    .line 130
    .line 131
    iput v3, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 132
    .line 133
    iget-object v2, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 134
    .line 135
    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    invoke-virtual {v2, v8, v9, v9, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 140
    .line 141
    .line 142
    iget-object v2, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 143
    .line 144
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    .line 146
    .line 147
    iget-object v2, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 148
    .line 149
    const/high16 v3, 0x41600000    # 14.0f

    .line 150
    .line 151
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 152
    .line 153
    .line 154
    iget-object v2, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->b:Landroid/widget/ImageView;

    .line 155
    .line 156
    invoke-virtual {p0, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    .line 158
    .line 159
    invoke-static {}, Lb/b/a/a/d/c;->〇o〇()Z

    .line 160
    .line 161
    .line 162
    move-result v0

    .line 163
    if-nez v0, :cond_0

    .line 164
    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->d:Landroid/widget/TextView;

    .line 166
    .line 167
    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    .line 169
    .line 170
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 171
    .line 172
    invoke-virtual {p0, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    .line 174
    .line 175
    return-void
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->b()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->b:Landroid/widget/ImageView;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->e:Landroid/graphics/drawable/AnimationDrawable;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public setGuideText(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/sdk/component/adexpress/widget/SlideUp3DView;->c:Landroid/widget/TextView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

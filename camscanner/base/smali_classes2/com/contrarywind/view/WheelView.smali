.class public Lcom/contrarywind/view/WheelView;
.super Landroid/view/View;
.source "WheelView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/contrarywind/view/WheelView$DividerType;,
        Lcom/contrarywind/view/WheelView$ACTION;
    }
.end annotation


# static fields
.field private static final 〇800OO〇0O:[Ljava/lang/String;


# instance fields
.field private O0O:I

.field private O88O:Landroid/graphics/Typeface;

.field private O8o08O8O:Z

.field private OO:Landroid/os/Handler;

.field private OO〇00〇8oO:Landroid/graphics/Paint;

.field private OO〇OOo:Z

.field private Oo0O0o8:F

.field private Oo0〇Ooo:I

.field private Oo80:F

.field private Ooo08:I

.field private O〇08oOOO0:F

.field private O〇o88o08〇:F

.field private o0:Lcom/contrarywind/view/WheelView$DividerType;

.field private o0OoOOo0:J

.field private o8o:I

.field private o8oOOo:I

.field private o8〇OO:I

.field private o8〇OO0〇0o:Landroid/graphics/Paint;

.field private oO00〇o:I

.field private oOO0880O:I

.field private oOO〇〇:I

.field private oOo0:Landroid/graphics/Paint;

.field private oOoo80oO:I

.field private oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture<",
            "*>;"
        }
    .end annotation
.end field

.field private oO〇8O8oOo:I

.field private oo8ooo8O:I

.field private ooO:I

.field private ooo0〇〇O:Ljava/lang/String;

.field private o〇00O:Lcom/contrarywind/listener/OnItemSelectedListener;

.field private o〇oO:I

.field private o〇o〇Oo88:I

.field private 〇00O0:F

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Landroid/view/GestureDetector;

.field private 〇08〇o0O:F

.field private 〇0O:Ljava/util/concurrent/ScheduledExecutorService;

.field private 〇0O〇O00O:F

.field private 〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

.field private 〇OO8ooO8〇:I

.field private 〇OOo8〇0:Landroid/content/Context;

.field private 〇OO〇00〇0O:I

.field private 〇O〇〇O8:I

.field private 〇o0O:F

.field private 〇〇08O:I

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 1
    const-string v0, "00"

    .line 2
    .line 3
    const-string v1, "01"

    .line 4
    .line 5
    const-string v2, "02"

    .line 6
    .line 7
    const-string v3, "03"

    .line 8
    .line 9
    const-string v4, "04"

    .line 10
    .line 11
    const-string v5, "05"

    .line 12
    .line 13
    const-string v6, "06"

    .line 14
    .line 15
    const-string v7, "07"

    .line 16
    .line 17
    const-string v8, "08"

    .line 18
    .line 19
    const-string v9, "09"

    .line 20
    .line 21
    filled-new-array/range {v0 .. v9}, [Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    sput-object v0, Lcom/contrarywind/view/WheelView;->〇800OO〇0O:[Ljava/lang/String;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/contrarywind/view/WheelView;->O8o08O8O:Z

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    iput-boolean v1, p0, Lcom/contrarywind/view/WheelView;->〇080OO8〇0:Z

    .line 9
    .line 10
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iput-object v1, p0, Lcom/contrarywind/view/WheelView;->〇0O:Ljava/util/concurrent/ScheduledExecutorService;

    .line 15
    .line 16
    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    .line 17
    .line 18
    iput-object v1, p0, Lcom/contrarywind/view/WheelView;->O88O:Landroid/graphics/Typeface;

    .line 19
    .line 20
    const v1, 0x3fcccccd    # 1.6f

    .line 21
    .line 22
    .line 23
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 24
    .line 25
    const/16 v1, 0xb

    .line 26
    .line 27
    iput v1, p0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 28
    .line 29
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇0O〇O00O:F

    .line 33
    .line 34
    const-wide/16 v1, 0x0

    .line 35
    .line 36
    iput-wide v1, p0, Lcom/contrarywind/view/WheelView;->o0OoOOo0:J

    .line 37
    .line 38
    const/16 v1, 0x11

    .line 39
    .line 40
    iput v1, p0, Lcom/contrarywind/view/WheelView;->oO00〇o:I

    .line 41
    .line 42
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 43
    .line 44
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 45
    .line 46
    iput-boolean v0, p0, Lcom/contrarywind/view/WheelView;->OO〇OOo:Z

    .line 47
    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    sget v3, Lcom/contrarywind/view/R$dimen;->pickerview_textsize:I

    .line 53
    .line 54
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    iput v2, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 69
    .line 70
    const/high16 v3, 0x3f800000    # 1.0f

    .line 71
    .line 72
    cmpg-float v4, v2, v3

    .line 73
    .line 74
    if-gez v4, :cond_0

    .line 75
    .line 76
    const v2, 0x4019999a    # 2.4f

    .line 77
    .line 78
    .line 79
    iput v2, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_0
    const/high16 v4, 0x40000000    # 2.0f

    .line 83
    .line 84
    cmpg-float v3, v3, v2

    .line 85
    .line 86
    if-gtz v3, :cond_1

    .line 87
    .line 88
    cmpg-float v3, v2, v4

    .line 89
    .line 90
    if-gez v3, :cond_1

    .line 91
    .line 92
    const/high16 v2, 0x40800000    # 4.0f

    .line 93
    .line 94
    iput v2, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_1
    const/high16 v3, 0x40400000    # 3.0f

    .line 98
    .line 99
    cmpg-float v4, v4, v2

    .line 100
    .line 101
    if-gtz v4, :cond_2

    .line 102
    .line 103
    cmpg-float v4, v2, v3

    .line 104
    .line 105
    if-gez v4, :cond_2

    .line 106
    .line 107
    const/high16 v2, 0x40c00000    # 6.0f

    .line 108
    .line 109
    iput v2, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_2
    cmpl-float v3, v2, v3

    .line 113
    .line 114
    if-ltz v3, :cond_3

    .line 115
    .line 116
    const/high16 v3, 0x40200000    # 2.5f

    .line 117
    .line 118
    mul-float v2, v2, v3

    .line 119
    .line 120
    iput v2, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 121
    .line 122
    :cond_3
    :goto_0
    if-eqz p2, :cond_4

    .line 123
    .line 124
    sget-object v2, Lcom/contrarywind/view/R$styleable;->pickerview:[I

    .line 125
    .line 126
    invoke-virtual {p1, p2, v2, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    .line 127
    .line 128
    .line 129
    move-result-object p2

    .line 130
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_gravity:I

    .line 131
    .line 132
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oO00〇o:I

    .line 137
    .line 138
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_textColorOut:I

    .line 139
    .line 140
    const v1, -0x575758

    .line 141
    .line 142
    .line 143
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oOO〇〇:I

    .line 148
    .line 149
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_textColorCenter:I

    .line 150
    .line 151
    const v1, -0xd5d5d6

    .line 152
    .line 153
    .line 154
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 155
    .line 156
    .line 157
    move-result v0

    .line 158
    iput v0, p0, Lcom/contrarywind/view/WheelView;->o8o:I

    .line 159
    .line 160
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_dividerColor:I

    .line 161
    .line 162
    const v1, -0x2a2a2b

    .line 163
    .line 164
    .line 165
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 166
    .line 167
    .line 168
    move-result v0

    .line 169
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oo8ooo8O:I

    .line 170
    .line 171
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_dividerWidth:I

    .line 172
    .line 173
    const/4 v1, 0x2

    .line 174
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    iput v0, p0, Lcom/contrarywind/view/WheelView;->o〇oO:I

    .line 179
    .line 180
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_textSize:I

    .line 181
    .line 182
    iget v1, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 183
    .line 184
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 185
    .line 186
    .line 187
    move-result v0

    .line 188
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 189
    .line 190
    sget v0, Lcom/contrarywind/view/R$styleable;->pickerview_wheelview_lineSpacingMultiplier:I

    .line 191
    .line 192
    iget v1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 193
    .line 194
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 195
    .line 196
    .line 197
    move-result v0

    .line 198
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 199
    .line 200
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 201
    .line 202
    .line 203
    :cond_4
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->OO0o〇〇〇〇0()V

    .line 204
    .line 205
    .line 206
    invoke-direct {p0, p1}, Lcom/contrarywind/view/WheelView;->〇〇888(Landroid/content/Context;)V

    .line 207
    .line 208
    .line 209
    return-void
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method

.method private O8(I)Ljava/lang/String;
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    const/16 v0, 0xa

    .line 4
    .line 5
    if-ge p1, v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/contrarywind/view/WheelView;->〇800OO〇0O:[Ljava/lang/String;

    .line 8
    .line 9
    aget-object p1, v0, p1

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    :goto_0
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private OO0o〇〇(Ljava/lang/String;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-virtual {v1, p1, v3, v2, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    iget p1, p0, Lcom/contrarywind/view/WheelView;->oO00〇o:I

    .line 17
    .line 18
    const/4 v1, 0x3

    .line 19
    if-eq p1, v1, :cond_4

    .line 20
    .line 21
    const/4 v1, 0x5

    .line 22
    if-eq p1, v1, :cond_3

    .line 23
    .line 24
    const/16 v1, 0x11

    .line 25
    .line 26
    if-eq p1, v1, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    iget-boolean p1, p0, Lcom/contrarywind/view/WheelView;->O8o08O8O:Z

    .line 30
    .line 31
    if-nez p1, :cond_2

    .line 32
    .line 33
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    const-string v1, ""

    .line 38
    .line 39
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-nez p1, :cond_2

    .line 44
    .line 45
    iget-boolean p1, p0, Lcom/contrarywind/view/WheelView;->〇080OO8〇0:Z

    .line 46
    .line 47
    if-nez p1, :cond_1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    sub-int/2addr p1, v0

    .line 57
    int-to-double v0, p1

    .line 58
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    .line 59
    .line 60
    mul-double v0, v0, v2

    .line 61
    .line 62
    double-to-int p1, v0

    .line 63
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_2
    :goto_0
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    sub-int/2addr p1, v0

    .line 73
    int-to-double v0, p1

    .line 74
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 75
    .line 76
    mul-double v0, v0, v2

    .line 77
    .line 78
    double-to-int p1, v0

    .line 79
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_3
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 83
    .line 84
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    sub-int/2addr p1, v0

    .line 89
    iget v0, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 90
    .line 91
    float-to-int v0, v0

    .line 92
    sub-int/2addr p1, v0

    .line 93
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_4
    iput v3, p0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 97
    .line 98
    :goto_1
    return-void
    .line 99
    .line 100
.end method

.method private OO0o〇〇〇〇0()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    cmpg-float v2, v0, v1

    .line 6
    .line 7
    if-gez v2, :cond_0

    .line 8
    .line 9
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/high16 v1, 0x40800000    # 4.0f

    .line 13
    .line 14
    cmpl-float v0, v0, v1

    .line 15
    .line 16
    if-lez v0, :cond_1

    .line 17
    .line 18
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 19
    .line 20
    :cond_1
    :goto_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method private Oo08(I)I
    .locals 1

    .line 1
    if-gez p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    add-int/2addr p1, v0

    .line 10
    invoke-direct {p0, p1}, Lcom/contrarywind/view/WheelView;->Oo08(I)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    add-int/lit8 v0, v0, -0x1

    .line 22
    .line 23
    if-le p1, v0, :cond_1

    .line 24
    .line 25
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 26
    .line 27
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    sub-int/2addr p1, v0

    .line 32
    invoke-direct {p0, p1}, Lcom/contrarywind/view/WheelView;->Oo08(I)I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    :cond_1
    :goto_0
    return p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private oO80()V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 7
    .line 8
    iget v1, p0, Lcom/contrarywind/view/WheelView;->oOO〇〇:I

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->O88O:Landroid/graphics/Typeface;

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 27
    .line 28
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 32
    .line 33
    .line 34
    new-instance v0, Landroid/graphics/Paint;

    .line 35
    .line 36
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 40
    .line 41
    iget v2, p0, Lcom/contrarywind/view/WheelView;->o8o:I

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 52
    .line 53
    const v2, 0x3f8ccccd    # 1.1f

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextScaleX(F)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 60
    .line 61
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->O88O:Landroid/graphics/Typeface;

    .line 62
    .line 63
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 67
    .line 68
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 69
    .line 70
    int-to-float v2, v2

    .line 71
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 72
    .line 73
    .line 74
    new-instance v0, Landroid/graphics/Paint;

    .line 75
    .line 76
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 77
    .line 78
    .line 79
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 80
    .line 81
    iget v2, p0, Lcom/contrarywind/view/WheelView;->oo8ooo8O:I

    .line 82
    .line 83
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 89
    .line 90
    .line 91
    const/4 v0, 0x0

    .line 92
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method static synthetic 〇080(Lcom/contrarywind/view/WheelView;)Lcom/contrarywind/listener/OnItemSelectedListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/contrarywind/view/WheelView;->o〇00O:Lcom/contrarywind/listener/OnItemSelectedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private 〇8o8o〇()V
    .locals 6

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    iget-object v3, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 9
    .line 10
    invoke-interface {v3}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 11
    .line 12
    .line 13
    move-result v3

    .line 14
    if-ge v2, v3, :cond_1

    .line 15
    .line 16
    iget-object v3, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 17
    .line 18
    invoke-interface {v3, v2}, Lcom/contrarywind/adapter/WheelAdapter;->getItem(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-direct {p0, v3}, Lcom/contrarywind/view/WheelView;->〇o〇(Ljava/lang/Object;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    iget-object v4, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 27
    .line 28
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 29
    .line 30
    .line 31
    move-result v5

    .line 32
    invoke-virtual {v4, v3, v1, v5, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    iget v4, p0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 40
    .line 41
    if-le v3, v4, :cond_0

    .line 42
    .line 43
    iput v3, p0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 44
    .line 45
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 49
    .line 50
    const-string v3, "\u661f\u671f"

    .line 51
    .line 52
    const/4 v4, 0x2

    .line 53
    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    add-int/2addr v0, v4

    .line 61
    iput v0, p0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 62
    .line 63
    iget v1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 64
    .line 65
    int-to-float v0, v0

    .line 66
    mul-float v1, v1, v0

    .line 67
    .line 68
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private 〇O8o08O(Ljava/lang/String;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-virtual {v1, p1, v3, v2, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    iget p1, p0, Lcom/contrarywind/view/WheelView;->oO00〇o:I

    .line 17
    .line 18
    const/4 v1, 0x3

    .line 19
    if-eq p1, v1, :cond_4

    .line 20
    .line 21
    const/4 v1, 0x5

    .line 22
    if-eq p1, v1, :cond_3

    .line 23
    .line 24
    const/16 v1, 0x11

    .line 25
    .line 26
    if-eq p1, v1, :cond_0

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    iget-boolean p1, p0, Lcom/contrarywind/view/WheelView;->O8o08O8O:Z

    .line 30
    .line 31
    if-nez p1, :cond_2

    .line 32
    .line 33
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    const-string v1, ""

    .line 38
    .line 39
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-nez p1, :cond_2

    .line 44
    .line 45
    iget-boolean p1, p0, Lcom/contrarywind/view/WheelView;->〇080OO8〇0:Z

    .line 46
    .line 47
    if-nez p1, :cond_1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    sub-int/2addr p1, v0

    .line 57
    int-to-double v0, p1

    .line 58
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    .line 59
    .line 60
    mul-double v0, v0, v2

    .line 61
    .line 62
    double-to-int p1, v0

    .line 63
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_2
    :goto_0
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    sub-int/2addr p1, v0

    .line 73
    int-to-double v0, p1

    .line 74
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 75
    .line 76
    mul-double v0, v0, v2

    .line 77
    .line 78
    double-to-int p1, v0

    .line 79
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_3
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 83
    .line 84
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    sub-int/2addr p1, v0

    .line 89
    iget v0, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 90
    .line 91
    float-to-int v0, v0

    .line 92
    sub-int/2addr p1, v0

    .line 93
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_4
    iput v3, p0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 97
    .line 98
    :goto_1
    return-void
    .line 99
    .line 100
.end method

.method private 〇O〇(Ljava/lang/String;)V
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-virtual {v1, p1, v3, v2, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 21
    .line 22
    :goto_0
    iget v4, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 23
    .line 24
    if-le v1, v4, :cond_0

    .line 25
    .line 26
    add-int/lit8 v2, v2, -0x1

    .line 27
    .line 28
    iget-object v1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 29
    .line 30
    int-to-float v4, v2

    .line 31
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    invoke-virtual {v1, p1, v3, v4, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    goto :goto_0

    .line 48
    :cond_0
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 49
    .line 50
    int-to-float v0, v2

    .line 51
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private 〇o〇(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-string p1, ""

    .line 4
    .line 5
    return-object p1

    .line 6
    :cond_0
    instance-of v0, p1, Lcom/contrarywind/interfaces/IPickerViewData;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    check-cast p1, Lcom/contrarywind/interfaces/IPickerViewData;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/contrarywind/interfaces/IPickerViewData;->〇080()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    return-object p1

    .line 17
    :cond_1
    instance-of v0, p1, Ljava/lang/Integer;

    .line 18
    .line 19
    if-eqz v0, :cond_2

    .line 20
    .line 21
    check-cast p1, Ljava/lang/Integer;

    .line 22
    .line 23
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-direct {p0, p1}, Lcom/contrarywind/view/WheelView;->O8(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    return-object p1

    .line 32
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    return-object p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private 〇〇808〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->〇8o8o〇()V

    .line 7
    .line 8
    .line 9
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 10
    .line 11
    iget v1, p0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 12
    .line 13
    add-int/lit8 v1, v1, -0x1

    .line 14
    .line 15
    int-to-float v1, v1

    .line 16
    mul-float v0, v0, v1

    .line 17
    .line 18
    float-to-int v0, v0

    .line 19
    mul-int/lit8 v1, v0, 0x2

    .line 20
    .line 21
    int-to-double v1, v1

    .line 22
    const-wide v3, 0x400921fb54442d18L    # Math.PI

    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    div-double/2addr v1, v3

    .line 28
    double-to-int v1, v1

    .line 29
    iput v1, p0, Lcom/contrarywind/view/WheelView;->〇OO〇00〇0O:I

    .line 30
    .line 31
    int-to-double v0, v0

    .line 32
    div-double/2addr v0, v3

    .line 33
    double-to-int v0, v0

    .line 34
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 35
    .line 36
    iget v0, p0, Lcom/contrarywind/view/WheelView;->o〇o〇Oo88:I

    .line 37
    .line 38
    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    iput v0, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 43
    .line 44
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇OO〇00〇0O:I

    .line 45
    .line 46
    int-to-float v1, v0

    .line 47
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 48
    .line 49
    sub-float/2addr v1, v2

    .line 50
    const/high16 v3, 0x40000000    # 2.0f

    .line 51
    .line 52
    div-float/2addr v1, v3

    .line 53
    iput v1, p0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 54
    .line 55
    int-to-float v0, v0

    .line 56
    add-float/2addr v0, v2

    .line 57
    div-float/2addr v0, v3

    .line 58
    iput v0, p0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 59
    .line 60
    iget v1, p0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 61
    .line 62
    int-to-float v1, v1

    .line 63
    sub-float/2addr v2, v1

    .line 64
    div-float/2addr v2, v3

    .line 65
    sub-float/2addr v0, v2

    .line 66
    iget v1, p0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 67
    .line 68
    sub-float/2addr v0, v1

    .line 69
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇00O0:F

    .line 70
    .line 71
    iget v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 72
    .line 73
    const/4 v1, -0x1

    .line 74
    if-ne v0, v1, :cond_2

    .line 75
    .line 76
    iget-boolean v0, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 77
    .line 78
    if-eqz v0, :cond_1

    .line 79
    .line 80
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 81
    .line 82
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    add-int/lit8 v0, v0, 0x1

    .line 87
    .line 88
    div-int/lit8 v0, v0, 0x2

    .line 89
    .line 90
    iput v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    const/4 v0, 0x0

    .line 94
    iput v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 95
    .line 96
    :cond_2
    :goto_0
    iget v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 97
    .line 98
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method private 〇〇888(Landroid/content/Context;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    new-instance v0, Lcom/contrarywind/timer/MessageHandler;

    .line 4
    .line 5
    invoke-direct {v0, p0}, Lcom/contrarywind/timer/MessageHandler;-><init>(Lcom/contrarywind/view/WheelView;)V

    .line 6
    .line 7
    .line 8
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->OO:Landroid/os/Handler;

    .line 9
    .line 10
    new-instance v0, Landroid/view/GestureDetector;

    .line 11
    .line 12
    new-instance v1, Lcom/contrarywind/listener/LoopViewGestureListener;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/contrarywind/listener/LoopViewGestureListener;-><init>(Lcom/contrarywind/view/WheelView;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->〇08O〇00〇o:Landroid/view/GestureDetector;

    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 24
    .line 25
    .line 26
    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    iput p1, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 31
    .line 32
    const/4 p1, -0x1

    .line 33
    iput p1, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->oO80()V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method private 〇〇8O0〇8(FF)V
    .locals 5

    .line 1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇O〇〇O8:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    const/4 v2, 0x1

    .line 5
    if-lez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    if-gez v0, :cond_1

    .line 10
    .line 11
    const/4 v0, -0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_1
    const/4 v0, 0x0

    .line 14
    :goto_0
    iget-object v3, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    cmpl-float v4, p2, v4

    .line 18
    .line 19
    if-lez v4, :cond_2

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_2
    const/4 v1, 0x1

    .line 23
    :goto_1
    mul-int v0, v0, v1

    .line 24
    .line 25
    int-to-float v0, v0

    .line 26
    const/high16 v1, 0x3f000000    # 0.5f

    .line 27
    .line 28
    mul-float v0, v0, v1

    .line 29
    .line 30
    mul-float v0, v0, p1

    .line 31
    .line 32
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 33
    .line 34
    .line 35
    iget-boolean p1, p0, Lcom/contrarywind/view/WheelView;->OO〇OOo:Z

    .line 36
    .line 37
    if-eqz p1, :cond_3

    .line 38
    .line 39
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    const/high16 p2, 0x42b40000    # 90.0f

    .line 44
    .line 45
    sub-float p1, p2, p1

    .line 46
    .line 47
    div-float/2addr p1, p2

    .line 48
    const/high16 p2, 0x437f0000    # 255.0f

    .line 49
    .line 50
    mul-float p1, p1, p2

    .line 51
    .line 52
    float-to-int p1, p1

    .line 53
    goto :goto_2

    .line 54
    :cond_3
    const/16 p1, 0xff

    .line 55
    .line 56
    :goto_2
    iget-object p2, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 57
    .line 58
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
.end method


# virtual methods
.method public final Oooo8o0〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->o〇00O:Lcom/contrarywind/listener/OnItemSelectedListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/contrarywind/view/WheelView$1;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/contrarywind/view/WheelView$1;-><init>(Lcom/contrarywind/view/WheelView;)V

    .line 8
    .line 9
    .line 10
    const-wide/16 v1, 0xc8

    .line 11
    .line 12
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
.end method

.method public final getAdapter()Lcom/contrarywind/adapter/WheelAdapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final getCurrentItem()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-boolean v2, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 8
    .line 9
    if-eqz v2, :cond_2

    .line 10
    .line 11
    iget v2, p0, Lcom/contrarywind/view/WheelView;->Ooo08:I

    .line 12
    .line 13
    if-ltz v2, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-lt v2, v0, :cond_2

    .line 20
    .line 21
    :cond_1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->Ooo08:I

    .line 22
    .line 23
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 28
    .line 29
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    sub-int/2addr v0, v2

    .line 34
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 39
    .line 40
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    add-int/lit8 v2, v2, -0x1

    .line 45
    .line 46
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    return v0

    .line 55
    :cond_2
    iget v0, p0, Lcom/contrarywind/view/WheelView;->Ooo08:I

    .line 56
    .line 57
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 58
    .line 59
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    add-int/lit8 v2, v2, -0x1

    .line 64
    .line 65
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    return v0
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO:Landroid/os/Handler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getInitPosition()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getItemHeight()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getItemsCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getTotalScrollY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget v1, v0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 11
    .line 12
    const/4 v8, 0x0

    .line 13
    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 18
    .line 19
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    add-int/lit8 v2, v2, -0x1

    .line 24
    .line 25
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iput v1, v0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 30
    .line 31
    iget v2, v0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 32
    .line 33
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 34
    .line 35
    div-float/2addr v2, v3

    .line 36
    float-to-int v2, v2

    .line 37
    :try_start_0
    iget-object v3, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 38
    .line 39
    invoke-interface {v3}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    rem-int/2addr v2, v3

    .line 44
    add-int/2addr v1, v2

    .line 45
    iput v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :catch_0
    nop

    .line 49
    :goto_0
    iget-boolean v1, v0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 50
    .line 51
    if-nez v1, :cond_2

    .line 52
    .line 53
    iget v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 54
    .line 55
    if-gez v1, :cond_1

    .line 56
    .line 57
    iput v8, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 58
    .line 59
    :cond_1
    iget v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 60
    .line 61
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 62
    .line 63
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    add-int/lit8 v2, v2, -0x1

    .line 68
    .line 69
    if-le v1, v2, :cond_4

    .line 70
    .line 71
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 72
    .line 73
    invoke-interface {v1}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    add-int/lit8 v1, v1, -0x1

    .line 78
    .line 79
    iput v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    iget v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 83
    .line 84
    if-gez v1, :cond_3

    .line 85
    .line 86
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 87
    .line 88
    invoke-interface {v1}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    iget v2, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 93
    .line 94
    add-int/2addr v1, v2

    .line 95
    iput v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 96
    .line 97
    :cond_3
    iget v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 98
    .line 99
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 100
    .line 101
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    add-int/lit8 v2, v2, -0x1

    .line 106
    .line 107
    if-le v1, v2, :cond_4

    .line 108
    .line 109
    iget v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 110
    .line 111
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 112
    .line 113
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 114
    .line 115
    .line 116
    move-result v2

    .line 117
    sub-int/2addr v1, v2

    .line 118
    iput v1, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 119
    .line 120
    :cond_4
    :goto_1
    iget v1, v0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 121
    .line 122
    iget v2, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 123
    .line 124
    rem-float v9, v1, v2

    .line 125
    .line 126
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->o0:Lcom/contrarywind/view/WheelView$DividerType;

    .line 127
    .line 128
    sget-object v2, Lcom/contrarywind/view/WheelView$DividerType;->WRAP:Lcom/contrarywind/view/WheelView$DividerType;

    .line 129
    .line 130
    const/high16 v3, 0x41200000    # 10.0f

    .line 131
    .line 132
    const/4 v10, 0x0

    .line 133
    if-ne v1, v2, :cond_7

    .line 134
    .line 135
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 136
    .line 137
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    if-eqz v1, :cond_5

    .line 142
    .line 143
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 144
    .line 145
    iget v2, v0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 146
    .line 147
    sub-int/2addr v1, v2

    .line 148
    div-int/lit8 v1, v1, 0x2

    .line 149
    .line 150
    goto :goto_2

    .line 151
    :cond_5
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 152
    .line 153
    iget v2, v0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 154
    .line 155
    sub-int/2addr v1, v2

    .line 156
    div-int/lit8 v1, v1, 0x4

    .line 157
    .line 158
    :goto_2
    add-int/lit8 v1, v1, -0xc

    .line 159
    .line 160
    int-to-float v1, v1

    .line 161
    cmpg-float v2, v1, v10

    .line 162
    .line 163
    if-gtz v2, :cond_6

    .line 164
    .line 165
    const/high16 v11, 0x41200000    # 10.0f

    .line 166
    .line 167
    goto :goto_3

    .line 168
    :cond_6
    move v11, v1

    .line 169
    :goto_3
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 170
    .line 171
    int-to-float v1, v1

    .line 172
    sub-float v12, v1, v11

    .line 173
    .line 174
    iget v5, v0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 175
    .line 176
    iget-object v6, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 177
    .line 178
    move-object/from16 v1, p1

    .line 179
    .line 180
    move v2, v11

    .line 181
    move v3, v5

    .line 182
    move v4, v12

    .line 183
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 184
    .line 185
    .line 186
    iget v5, v0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 187
    .line 188
    iget-object v6, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 189
    .line 190
    move v3, v5

    .line 191
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 192
    .line 193
    .line 194
    goto/16 :goto_6

    .line 195
    .line 196
    :cond_7
    sget-object v2, Lcom/contrarywind/view/WheelView$DividerType;->CIRCLE:Lcom/contrarywind/view/WheelView$DividerType;

    .line 197
    .line 198
    if-ne v1, v2, :cond_a

    .line 199
    .line 200
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 201
    .line 202
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 203
    .line 204
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 205
    .line 206
    .line 207
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 208
    .line 209
    iget v2, v0, Lcom/contrarywind/view/WheelView;->o〇oO:I

    .line 210
    .line 211
    int-to-float v2, v2

    .line 212
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 213
    .line 214
    .line 215
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 216
    .line 217
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 218
    .line 219
    .line 220
    move-result v1

    .line 221
    const/high16 v2, 0x41400000    # 12.0f

    .line 222
    .line 223
    const/high16 v4, 0x40000000    # 2.0f

    .line 224
    .line 225
    if-eqz v1, :cond_8

    .line 226
    .line 227
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 228
    .line 229
    iget v5, v0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 230
    .line 231
    sub-int/2addr v1, v5

    .line 232
    int-to-float v1, v1

    .line 233
    div-float/2addr v1, v4

    .line 234
    goto :goto_4

    .line 235
    :cond_8
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 236
    .line 237
    iget v5, v0, Lcom/contrarywind/view/WheelView;->O0O:I

    .line 238
    .line 239
    sub-int/2addr v1, v5

    .line 240
    int-to-float v1, v1

    .line 241
    const/high16 v5, 0x40800000    # 4.0f

    .line 242
    .line 243
    div-float/2addr v1, v5

    .line 244
    :goto_4
    sub-float/2addr v1, v2

    .line 245
    cmpg-float v2, v1, v10

    .line 246
    .line 247
    if-gtz v2, :cond_9

    .line 248
    .line 249
    goto :goto_5

    .line 250
    :cond_9
    move v3, v1

    .line 251
    :goto_5
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 252
    .line 253
    int-to-float v1, v1

    .line 254
    sub-float/2addr v1, v3

    .line 255
    sub-float/2addr v1, v3

    .line 256
    iget v2, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 257
    .line 258
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    .line 259
    .line 260
    .line 261
    move-result v1

    .line 262
    const v2, 0x3fe66666    # 1.8f

    .line 263
    .line 264
    .line 265
    div-float/2addr v1, v2

    .line 266
    iget v2, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 267
    .line 268
    int-to-float v2, v2

    .line 269
    div-float/2addr v2, v4

    .line 270
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇OO〇00〇0O:I

    .line 271
    .line 272
    int-to-float v3, v3

    .line 273
    div-float/2addr v3, v4

    .line 274
    iget-object v4, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 275
    .line 276
    invoke-virtual {v7, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 277
    .line 278
    .line 279
    goto :goto_6

    .line 280
    :cond_a
    const/4 v2, 0x0

    .line 281
    iget v5, v0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 282
    .line 283
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 284
    .line 285
    int-to-float v4, v1

    .line 286
    iget-object v6, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 287
    .line 288
    move-object/from16 v1, p1

    .line 289
    .line 290
    move v3, v5

    .line 291
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 292
    .line 293
    .line 294
    iget v5, v0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 295
    .line 296
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 297
    .line 298
    int-to-float v4, v1

    .line 299
    iget-object v6, v0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 300
    .line 301
    move-object/from16 v1, p1

    .line 302
    .line 303
    move v3, v5

    .line 304
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 305
    .line 306
    .line 307
    :goto_6
    iget-object v1, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 308
    .line 309
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 310
    .line 311
    .line 312
    move-result v1

    .line 313
    if-nez v1, :cond_b

    .line 314
    .line 315
    iget-boolean v1, v0, Lcom/contrarywind/view/WheelView;->〇080OO8〇0:Z

    .line 316
    .line 317
    if-eqz v1, :cond_b

    .line 318
    .line 319
    iget v1, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 320
    .line 321
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 322
    .line 323
    iget-object v3, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 324
    .line 325
    invoke-virtual {v0, v2, v3}, Lcom/contrarywind/view/WheelView;->o〇0(Landroid/graphics/Paint;Ljava/lang/String;)I

    .line 326
    .line 327
    .line 328
    move-result v2

    .line 329
    sub-int/2addr v1, v2

    .line 330
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 331
    .line 332
    int-to-float v1, v1

    .line 333
    iget v3, v0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 334
    .line 335
    sub-float/2addr v1, v3

    .line 336
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇00O0:F

    .line 337
    .line 338
    iget-object v4, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 339
    .line 340
    invoke-virtual {v7, v2, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 341
    .line 342
    .line 343
    :cond_b
    const/4 v1, 0x0

    .line 344
    :goto_7
    iget v2, v0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 345
    .line 346
    if-ge v1, v2, :cond_15

    .line 347
    .line 348
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 349
    .line 350
    div-int/lit8 v2, v2, 0x2

    .line 351
    .line 352
    sub-int/2addr v2, v1

    .line 353
    sub-int/2addr v3, v2

    .line 354
    iget-boolean v2, v0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 355
    .line 356
    if-eqz v2, :cond_c

    .line 357
    .line 358
    invoke-direct {v0, v3}, Lcom/contrarywind/view/WheelView;->Oo08(I)I

    .line 359
    .line 360
    .line 361
    move-result v2

    .line 362
    iget-object v3, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 363
    .line 364
    invoke-interface {v3, v2}, Lcom/contrarywind/adapter/WheelAdapter;->getItem(I)Ljava/lang/Object;

    .line 365
    .line 366
    .line 367
    move-result-object v2

    .line 368
    goto :goto_8

    .line 369
    :cond_c
    const-string v2, ""

    .line 370
    .line 371
    if-gez v3, :cond_d

    .line 372
    .line 373
    goto :goto_8

    .line 374
    :cond_d
    iget-object v4, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 375
    .line 376
    invoke-interface {v4}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 377
    .line 378
    .line 379
    move-result v4

    .line 380
    add-int/lit8 v4, v4, -0x1

    .line 381
    .line 382
    if-le v3, v4, :cond_e

    .line 383
    .line 384
    goto :goto_8

    .line 385
    :cond_e
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 386
    .line 387
    invoke-interface {v2, v3}, Lcom/contrarywind/adapter/WheelAdapter;->getItem(I)Ljava/lang/Object;

    .line 388
    .line 389
    .line 390
    move-result-object v2

    .line 391
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 392
    .line 393
    .line 394
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 395
    .line 396
    int-to-float v4, v1

    .line 397
    mul-float v3, v3, v4

    .line 398
    .line 399
    sub-float/2addr v3, v9

    .line 400
    iget v4, v0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 401
    .line 402
    int-to-float v4, v4

    .line 403
    div-float/2addr v3, v4

    .line 404
    float-to-double v3, v3

    .line 405
    const-wide v5, 0x400921fb54442d18L    # Math.PI

    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    div-double v5, v3, v5

    .line 411
    .line 412
    const-wide v11, 0x4066800000000000L    # 180.0

    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    mul-double v5, v5, v11

    .line 418
    .line 419
    const-wide v11, 0x4056800000000000L    # 90.0

    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    sub-double/2addr v11, v5

    .line 425
    double-to-float v5, v11

    .line 426
    const/high16 v6, 0x42b40000    # 90.0f

    .line 427
    .line 428
    cmpl-float v11, v5, v6

    .line 429
    .line 430
    if-gtz v11, :cond_14

    .line 431
    .line 432
    const/high16 v11, -0x3d4c0000    # -90.0f

    .line 433
    .line 434
    cmpg-float v11, v5, v11

    .line 435
    .line 436
    if-gez v11, :cond_f

    .line 437
    .line 438
    goto/16 :goto_c

    .line 439
    .line 440
    :cond_f
    iget-boolean v11, v0, Lcom/contrarywind/view/WheelView;->〇080OO8〇0:Z

    .line 441
    .line 442
    if-nez v11, :cond_10

    .line 443
    .line 444
    iget-object v11, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 445
    .line 446
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 447
    .line 448
    .line 449
    move-result v11

    .line 450
    if-nez v11, :cond_10

    .line 451
    .line 452
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->〇o〇(Ljava/lang/Object;)Ljava/lang/String;

    .line 453
    .line 454
    .line 455
    move-result-object v11

    .line 456
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 457
    .line 458
    .line 459
    move-result v11

    .line 460
    if-nez v11, :cond_10

    .line 461
    .line 462
    new-instance v11, Ljava/lang/StringBuilder;

    .line 463
    .line 464
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    .line 466
    .line 467
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->〇o〇(Ljava/lang/Object;)Ljava/lang/String;

    .line 468
    .line 469
    .line 470
    move-result-object v2

    .line 471
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    .line 473
    .line 474
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 475
    .line 476
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    .line 478
    .line 479
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 480
    .line 481
    .line 482
    move-result-object v2

    .line 483
    goto :goto_9

    .line 484
    :cond_10
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->〇o〇(Ljava/lang/Object;)Ljava/lang/String;

    .line 485
    .line 486
    .line 487
    move-result-object v2

    .line 488
    :goto_9
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 489
    .line 490
    .line 491
    move-result v11

    .line 492
    div-float/2addr v11, v6

    .line 493
    float-to-double v11, v11

    .line 494
    const-wide v13, 0x400199999999999aL    # 2.2

    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    .line 500
    .line 501
    .line 502
    move-result-wide v11

    .line 503
    double-to-float v6, v11

    .line 504
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->〇O〇(Ljava/lang/String;)V

    .line 505
    .line 506
    .line 507
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->〇O8o08O(Ljava/lang/String;)V

    .line 508
    .line 509
    .line 510
    invoke-direct {v0, v2}, Lcom/contrarywind/view/WheelView;->OO0o〇〇(Ljava/lang/String;)V

    .line 511
    .line 512
    .line 513
    iget v11, v0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 514
    .line 515
    int-to-double v11, v11

    .line 516
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    .line 517
    .line 518
    .line 519
    move-result-wide v13

    .line 520
    iget v15, v0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 521
    .line 522
    move/from16 v16, v9

    .line 523
    .line 524
    int-to-double v8, v15

    .line 525
    mul-double v13, v13, v8

    .line 526
    .line 527
    sub-double/2addr v11, v13

    .line 528
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 529
    .line 530
    .line 531
    move-result-wide v8

    .line 532
    iget v13, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 533
    .line 534
    int-to-double v13, v13

    .line 535
    mul-double v8, v8, v13

    .line 536
    .line 537
    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    .line 538
    .line 539
    div-double/2addr v8, v13

    .line 540
    sub-double/2addr v11, v8

    .line 541
    double-to-float v8, v11

    .line 542
    invoke-virtual {v7, v10, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 543
    .line 544
    .line 545
    iget v9, v0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 546
    .line 547
    const v11, 0x3f4ccccd    # 0.8f

    .line 548
    .line 549
    .line 550
    const/high16 v12, 0x3f800000    # 1.0f

    .line 551
    .line 552
    cmpg-float v13, v8, v9

    .line 553
    .line 554
    if-gtz v13, :cond_11

    .line 555
    .line 556
    iget v13, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 557
    .line 558
    int-to-float v13, v13

    .line 559
    add-float/2addr v13, v8

    .line 560
    cmpl-float v13, v13, v9

    .line 561
    .line 562
    if-ltz v13, :cond_11

    .line 563
    .line 564
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 565
    .line 566
    .line 567
    iget v9, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 568
    .line 569
    int-to-float v9, v9

    .line 570
    iget v13, v0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 571
    .line 572
    sub-float/2addr v13, v8

    .line 573
    invoke-virtual {v7, v10, v10, v9, v13}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 574
    .line 575
    .line 576
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 577
    .line 578
    .line 579
    move-result-wide v13

    .line 580
    double-to-float v9, v13

    .line 581
    mul-float v9, v9, v11

    .line 582
    .line 583
    invoke-virtual {v7, v12, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 584
    .line 585
    .line 586
    invoke-direct {v0, v6, v5}, Lcom/contrarywind/view/WheelView;->〇〇8O0〇8(FF)V

    .line 587
    .line 588
    .line 589
    iget v5, v0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 590
    .line 591
    int-to-float v5, v5

    .line 592
    iget v6, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 593
    .line 594
    int-to-float v6, v6

    .line 595
    iget-object v9, v0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 596
    .line 597
    invoke-virtual {v7, v2, v5, v6, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 598
    .line 599
    .line 600
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 601
    .line 602
    .line 603
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 604
    .line 605
    .line 606
    iget v5, v0, Lcom/contrarywind/view/WheelView;->Oo80:F

    .line 607
    .line 608
    sub-float/2addr v5, v8

    .line 609
    iget v6, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 610
    .line 611
    int-to-float v6, v6

    .line 612
    iget v8, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 613
    .line 614
    float-to-int v8, v8

    .line 615
    int-to-float v8, v8

    .line 616
    invoke-virtual {v7, v10, v5, v6, v8}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 617
    .line 618
    .line 619
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 620
    .line 621
    .line 622
    move-result-wide v3

    .line 623
    double-to-float v3, v3

    .line 624
    mul-float v3, v3, v12

    .line 625
    .line 626
    invoke-virtual {v7, v12, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 627
    .line 628
    .line 629
    iget v3, v0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 630
    .line 631
    int-to-float v3, v3

    .line 632
    iget v4, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 633
    .line 634
    int-to-float v4, v4

    .line 635
    iget v5, v0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 636
    .line 637
    sub-float/2addr v4, v5

    .line 638
    iget-object v5, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 639
    .line 640
    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 641
    .line 642
    .line 643
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 644
    .line 645
    .line 646
    :goto_a
    const/4 v13, 0x0

    .line 647
    goto/16 :goto_b

    .line 648
    .line 649
    :cond_11
    iget v13, v0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 650
    .line 651
    cmpg-float v14, v8, v13

    .line 652
    .line 653
    if-gtz v14, :cond_12

    .line 654
    .line 655
    iget v14, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 656
    .line 657
    int-to-float v14, v14

    .line 658
    add-float/2addr v14, v8

    .line 659
    cmpl-float v14, v14, v13

    .line 660
    .line 661
    if-ltz v14, :cond_12

    .line 662
    .line 663
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 664
    .line 665
    .line 666
    iget v9, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 667
    .line 668
    int-to-float v9, v9

    .line 669
    iget v13, v0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 670
    .line 671
    sub-float/2addr v13, v8

    .line 672
    invoke-virtual {v7, v10, v10, v9, v13}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 673
    .line 674
    .line 675
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 676
    .line 677
    .line 678
    move-result-wide v13

    .line 679
    double-to-float v9, v13

    .line 680
    mul-float v9, v9, v12

    .line 681
    .line 682
    invoke-virtual {v7, v12, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 683
    .line 684
    .line 685
    iget v9, v0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 686
    .line 687
    int-to-float v9, v9

    .line 688
    iget v13, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 689
    .line 690
    int-to-float v13, v13

    .line 691
    iget v14, v0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 692
    .line 693
    sub-float/2addr v13, v14

    .line 694
    iget-object v14, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 695
    .line 696
    invoke-virtual {v7, v2, v9, v13, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 697
    .line 698
    .line 699
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 700
    .line 701
    .line 702
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 703
    .line 704
    .line 705
    iget v9, v0, Lcom/contrarywind/view/WheelView;->O〇o88o08〇:F

    .line 706
    .line 707
    sub-float/2addr v9, v8

    .line 708
    iget v8, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 709
    .line 710
    int-to-float v8, v8

    .line 711
    iget v13, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 712
    .line 713
    float-to-int v13, v13

    .line 714
    int-to-float v13, v13

    .line 715
    invoke-virtual {v7, v10, v9, v8, v13}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 716
    .line 717
    .line 718
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 719
    .line 720
    .line 721
    move-result-wide v3

    .line 722
    double-to-float v3, v3

    .line 723
    mul-float v3, v3, v11

    .line 724
    .line 725
    invoke-virtual {v7, v12, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 726
    .line 727
    .line 728
    invoke-direct {v0, v6, v5}, Lcom/contrarywind/view/WheelView;->〇〇8O0〇8(FF)V

    .line 729
    .line 730
    .line 731
    iget v3, v0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 732
    .line 733
    int-to-float v3, v3

    .line 734
    iget v4, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 735
    .line 736
    int-to-float v4, v4

    .line 737
    iget-object v5, v0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 738
    .line 739
    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 740
    .line 741
    .line 742
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 743
    .line 744
    .line 745
    goto :goto_a

    .line 746
    :cond_12
    cmpl-float v9, v8, v9

    .line 747
    .line 748
    if-ltz v9, :cond_13

    .line 749
    .line 750
    iget v9, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 751
    .line 752
    int-to-float v14, v9

    .line 753
    add-float/2addr v14, v8

    .line 754
    cmpg-float v8, v14, v13

    .line 755
    .line 756
    if-gtz v8, :cond_13

    .line 757
    .line 758
    int-to-float v3, v9

    .line 759
    iget v4, v0, Lcom/contrarywind/view/WheelView;->Oo0O0o8:F

    .line 760
    .line 761
    sub-float/2addr v3, v4

    .line 762
    iget v4, v0, Lcom/contrarywind/view/WheelView;->oOO0880O:I

    .line 763
    .line 764
    int-to-float v4, v4

    .line 765
    iget-object v5, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 766
    .line 767
    invoke-virtual {v7, v2, v4, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 768
    .line 769
    .line 770
    iget v2, v0, Lcom/contrarywind/view/WheelView;->〇OO8ooO8〇:I

    .line 771
    .line 772
    iget v3, v0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 773
    .line 774
    div-int/lit8 v3, v3, 0x2

    .line 775
    .line 776
    sub-int/2addr v3, v1

    .line 777
    sub-int/2addr v2, v3

    .line 778
    iput v2, v0, Lcom/contrarywind/view/WheelView;->Ooo08:I

    .line 779
    .line 780
    goto/16 :goto_a

    .line 781
    .line 782
    :cond_13
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 783
    .line 784
    .line 785
    iget v8, v0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 786
    .line 787
    iget v9, v0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 788
    .line 789
    float-to-int v9, v9

    .line 790
    const/4 v13, 0x0

    .line 791
    invoke-virtual {v7, v13, v13, v8, v9}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 792
    .line 793
    .line 794
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    .line 795
    .line 796
    .line 797
    move-result-wide v3

    .line 798
    double-to-float v3, v3

    .line 799
    mul-float v3, v3, v11

    .line 800
    .line 801
    invoke-virtual {v7, v12, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 802
    .line 803
    .line 804
    invoke-direct {v0, v6, v5}, Lcom/contrarywind/view/WheelView;->〇〇8O0〇8(FF)V

    .line 805
    .line 806
    .line 807
    iget v3, v0, Lcom/contrarywind/view/WheelView;->oOoo80oO:I

    .line 808
    .line 809
    int-to-float v3, v3

    .line 810
    iget v4, v0, Lcom/contrarywind/view/WheelView;->〇O〇〇O8:I

    .line 811
    .line 812
    int-to-float v4, v4

    .line 813
    mul-float v4, v4, v6

    .line 814
    .line 815
    add-float/2addr v3, v4

    .line 816
    iget v4, v0, Lcom/contrarywind/view/WheelView;->o8oOOo:I

    .line 817
    .line 818
    int-to-float v4, v4

    .line 819
    iget-object v5, v0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 820
    .line 821
    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 822
    .line 823
    .line 824
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 825
    .line 826
    .line 827
    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 828
    .line 829
    .line 830
    iget-object v2, v0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 831
    .line 832
    iget v3, v0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 833
    .line 834
    int-to-float v3, v3

    .line 835
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 836
    .line 837
    .line 838
    goto :goto_d

    .line 839
    :cond_14
    :goto_c
    move/from16 v16, v9

    .line 840
    .line 841
    const/4 v13, 0x0

    .line 842
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 843
    .line 844
    .line 845
    :goto_d
    add-int/lit8 v1, v1, 0x1

    .line 846
    .line 847
    move/from16 v9, v16

    .line 848
    .line 849
    const/4 v8, 0x0

    .line 850
    goto/16 :goto_7

    .line 851
    .line 852
    :cond_15
    return-void
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->o〇o〇Oo88:I

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->〇〇808〇()V

    .line 4
    .line 5
    .line 6
    iget p1, p0, Lcom/contrarywind/view/WheelView;->Oo0〇Ooo:I

    .line 7
    .line 8
    iget p2, p0, Lcom/contrarywind/view/WheelView;->〇OO〇00〇0O:I

    .line 9
    .line 10
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇08O〇00〇o:Landroid/view/GestureDetector;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 8
    .line 9
    neg-int v1, v1

    .line 10
    int-to-float v1, v1

    .line 11
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 12
    .line 13
    mul-float v1, v1, v2

    .line 14
    .line 15
    iget-object v2, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 16
    .line 17
    invoke-interface {v2}, Lcom/contrarywind/adapter/WheelAdapter;->〇o00〇〇Oo()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    const/4 v3, 0x1

    .line 22
    sub-int/2addr v2, v3

    .line 23
    iget v4, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 24
    .line 25
    sub-int/2addr v2, v4

    .line 26
    int-to-float v2, v2

    .line 27
    iget v4, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 28
    .line 29
    mul-float v2, v2, v4

    .line 30
    .line 31
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    const/4 v5, 0x0

    .line 36
    if-eqz v4, :cond_4

    .line 37
    .line 38
    const/4 v6, 0x2

    .line 39
    if-eq v4, v6, :cond_1

    .line 40
    .line 41
    if-nez v0, :cond_5

    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iget v1, p0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 48
    .line 49
    int-to-float v2, v1

    .line 50
    sub-float/2addr v2, v0

    .line 51
    int-to-float v0, v1

    .line 52
    div-float/2addr v2, v0

    .line 53
    float-to-double v0, v2

    .line 54
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    .line 55
    .line 56
    .line 57
    move-result-wide v0

    .line 58
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇〇〇0o〇〇0:I

    .line 59
    .line 60
    int-to-double v7, v2

    .line 61
    mul-double v0, v0, v7

    .line 62
    .line 63
    iget v2, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 64
    .line 65
    const/high16 v4, 0x40000000    # 2.0f

    .line 66
    .line 67
    div-float v4, v2, v4

    .line 68
    .line 69
    float-to-double v7, v4

    .line 70
    add-double/2addr v0, v7

    .line 71
    float-to-double v7, v2

    .line 72
    div-double/2addr v0, v7

    .line 73
    double-to-int v0, v0

    .line 74
    iget v1, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 75
    .line 76
    rem-float/2addr v1, v2

    .line 77
    add-float/2addr v1, v2

    .line 78
    rem-float/2addr v1, v2

    .line 79
    iget v4, p0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 80
    .line 81
    div-int/2addr v4, v6

    .line 82
    sub-int/2addr v0, v4

    .line 83
    int-to-float v0, v0

    .line 84
    mul-float v0, v0, v2

    .line 85
    .line 86
    sub-float/2addr v0, v1

    .line 87
    float-to-int v0, v0

    .line 88
    iput v0, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 89
    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 91
    .line 92
    .line 93
    move-result-wide v0

    .line 94
    iget-wide v6, p0, Lcom/contrarywind/view/WheelView;->o0OoOOo0:J

    .line 95
    .line 96
    sub-long/2addr v0, v6

    .line 97
    const-wide/16 v6, 0x78

    .line 98
    .line 99
    cmp-long v2, v0, v6

    .line 100
    .line 101
    if-lez v2, :cond_0

    .line 102
    .line 103
    sget-object v0, Lcom/contrarywind/view/WheelView$ACTION;->DAGGLE:Lcom/contrarywind/view/WheelView$ACTION;

    .line 104
    .line 105
    invoke-virtual {p0, v0}, Lcom/contrarywind/view/WheelView;->〇0〇O0088o(Lcom/contrarywind/view/WheelView$ACTION;)V

    .line 106
    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_0
    sget-object v0, Lcom/contrarywind/view/WheelView$ACTION;->CLICK:Lcom/contrarywind/view/WheelView$ACTION;

    .line 110
    .line 111
    invoke-virtual {p0, v0}, Lcom/contrarywind/view/WheelView;->〇0〇O0088o(Lcom/contrarywind/view/WheelView$ACTION;)V

    .line 112
    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_1
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇0O〇O00O:F

    .line 116
    .line 117
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    sub-float/2addr v0, v4

    .line 122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 123
    .line 124
    .line 125
    move-result v4

    .line 126
    iput v4, p0, Lcom/contrarywind/view/WheelView;->〇0O〇O00O:F

    .line 127
    .line 128
    iget v4, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 129
    .line 130
    add-float/2addr v4, v0

    .line 131
    iput v4, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 132
    .line 133
    iget-boolean v6, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 134
    .line 135
    if-nez v6, :cond_5

    .line 136
    .line 137
    iget v6, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 138
    .line 139
    const/high16 v7, 0x3e800000    # 0.25f

    .line 140
    .line 141
    mul-float v8, v6, v7

    .line 142
    .line 143
    sub-float v8, v4, v8

    .line 144
    .line 145
    const/4 v9, 0x0

    .line 146
    cmpg-float v1, v8, v1

    .line 147
    .line 148
    if-gez v1, :cond_2

    .line 149
    .line 150
    cmpg-float v1, v0, v9

    .line 151
    .line 152
    if-ltz v1, :cond_3

    .line 153
    .line 154
    :cond_2
    mul-float v6, v6, v7

    .line 155
    .line 156
    add-float/2addr v6, v4

    .line 157
    cmpl-float v1, v6, v2

    .line 158
    .line 159
    if-lez v1, :cond_5

    .line 160
    .line 161
    cmpl-float v1, v0, v9

    .line 162
    .line 163
    if-lez v1, :cond_5

    .line 164
    .line 165
    :cond_3
    sub-float/2addr v4, v0

    .line 166
    iput v4, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 167
    .line 168
    const/4 v5, 0x1

    .line 169
    goto :goto_0

    .line 170
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 171
    .line 172
    .line 173
    move-result-wide v0

    .line 174
    iput-wide v0, p0, Lcom/contrarywind/view/WheelView;->o0OoOOo0:J

    .line 175
    .line 176
    invoke-virtual {p0}, Lcom/contrarywind/view/WheelView;->〇o00〇〇Oo()V

    .line 177
    .line 178
    .line 179
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 180
    .line 181
    .line 182
    move-result v0

    .line 183
    iput v0, p0, Lcom/contrarywind/view/WheelView;->〇0O〇O00O:F

    .line 184
    .line 185
    :cond_5
    :goto_0
    if-nez v5, :cond_6

    .line 186
    .line 187
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    if-eqz p1, :cond_6

    .line 192
    .line 193
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 194
    .line 195
    .line 196
    :cond_6
    return v3
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public o〇0(Landroid/graphics/Paint;Ljava/lang/String;)I
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p2, :cond_1

    .line 3
    .line 4
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-lez v1, :cond_1

    .line 9
    .line 10
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    new-array v2, v1, [F

    .line 15
    .line 16
    invoke-virtual {p1, p2, v2}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    :goto_0
    if-ge v0, v1, :cond_0

    .line 21
    .line 22
    aget p2, v2, v0

    .line 23
    .line 24
    float-to-double v3, p2

    .line 25
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    double-to-int p2, v3

    .line 30
    add-int/2addr p1, p2

    .line 31
    add-int/lit8 v0, v0, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    move v0, p1

    .line 35
    :cond_1
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final setAdapter(Lcom/contrarywind/adapter/WheelAdapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->〇8〇oO〇〇8o:Lcom/contrarywind/adapter/WheelAdapter;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->〇〇808〇()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setAlphaGradient(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/contrarywind/view/WheelView;->OO〇OOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setCurrentItem(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->Ooo08:I

    .line 2
    .line 3
    iput p1, p0, Lcom/contrarywind/view/WheelView;->o8〇OO:I

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    iput p1, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setCyclic(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setDividerColor(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oo8ooo8O:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setDividerType(Lcom/contrarywind/view/WheelView$DividerType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->o0:Lcom/contrarywind/view/WheelView$DividerType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setDividerWidth(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->o〇oO:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->o8〇OO0〇0o:Landroid/graphics/Paint;

    .line 4
    .line 5
    int-to-float p1, p1

    .line 6
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setGravity(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oO00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setIsOptions(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/contrarywind/view/WheelView;->O8o08O8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setItemsVisibleCount(I)V
    .locals 1

    .line 1
    rem-int/lit8 v0, p1, 0x2

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    :cond_0
    add-int/lit8 p1, p1, 0x2

    .line 8
    .line 9
    iput p1, p0, Lcom/contrarywind/view/WheelView;->ooO:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLineSpacingMultiplier(F)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    cmpl-float v0, p1, v0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iput p1, p0, Lcom/contrarywind/view/WheelView;->〇08〇o0O:F

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/contrarywind/view/WheelView;->OO0o〇〇〇〇0()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setOnItemSelectedListener(Lcom/contrarywind/listener/OnItemSelectedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->o〇00O:Lcom/contrarywind/listener/OnItemSelectedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setTextColorCenter(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->o8o:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setTextColorOut(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oOO〇〇:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setTextSize(F)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    cmpl-float v0, p1, v0

    .line 3
    .line 4
    if-lez v0, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇OOo8〇0:Landroid/content/Context;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 17
    .line 18
    mul-float v0, v0, p1

    .line 19
    .line 20
    float-to-int p1, v0

    .line 21
    iput p1, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 22
    .line 23
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 24
    .line 25
    int-to-float p1, p1

    .line 26
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 30
    .line 31
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇〇08O:I

    .line 32
    .line 33
    int-to-float v0, v0

    .line 34
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 35
    .line 36
    .line 37
    :cond_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public setTextXOffset(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->〇O〇〇O8:I

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 6
    .line 7
    const/high16 v0, 0x3f800000    # 1.0f

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextScaleX(F)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setTotalScrollY(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public final setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->O88O:Landroid/graphics/Typeface;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo0:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/contrarywind/view/WheelView;->OO〇00〇8oO:Landroid/graphics/Paint;

    .line 9
    .line 10
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->O88O:Landroid/graphics/Typeface;

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public 〇0〇O0088o(Lcom/contrarywind/view/WheelView$ACTION;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/contrarywind/view/WheelView;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/contrarywind/view/WheelView$ACTION;->FLING:Lcom/contrarywind/view/WheelView$ACTION;

    .line 5
    .line 6
    if-eq p1, v0, :cond_0

    .line 7
    .line 8
    sget-object v0, Lcom/contrarywind/view/WheelView$ACTION;->DAGGLE:Lcom/contrarywind/view/WheelView$ACTION;

    .line 9
    .line 10
    if-ne p1, v0, :cond_2

    .line 11
    .line 12
    :cond_0
    iget p1, p0, Lcom/contrarywind/view/WheelView;->O〇08oOOO0:F

    .line 13
    .line 14
    iget v0, p0, Lcom/contrarywind/view/WheelView;->〇o0O:F

    .line 15
    .line 16
    rem-float/2addr p1, v0

    .line 17
    add-float/2addr p1, v0

    .line 18
    rem-float/2addr p1, v0

    .line 19
    float-to-int p1, p1

    .line 20
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 21
    .line 22
    int-to-float v1, p1

    .line 23
    const/high16 v2, 0x40000000    # 2.0f

    .line 24
    .line 25
    div-float v2, v0, v2

    .line 26
    .line 27
    cmpl-float v1, v1, v2

    .line 28
    .line 29
    if-lez v1, :cond_1

    .line 30
    .line 31
    int-to-float p1, p1

    .line 32
    sub-float/2addr v0, p1

    .line 33
    float-to-int p1, v0

    .line 34
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    neg-int p1, p1

    .line 38
    iput p1, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 39
    .line 40
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇0O:Ljava/util/concurrent/ScheduledExecutorService;

    .line 41
    .line 42
    new-instance v1, Lcom/contrarywind/timer/SmoothScrollTimerTask;

    .line 43
    .line 44
    iget p1, p0, Lcom/contrarywind/view/WheelView;->oO〇8O8oOo:I

    .line 45
    .line 46
    invoke-direct {v1, p0, p1}, Lcom/contrarywind/timer/SmoothScrollTimerTask;-><init>(Lcom/contrarywind/view/WheelView;I)V

    .line 47
    .line 48
    .line 49
    const-wide/16 v2, 0x0

    .line 50
    .line 51
    const-wide/16 v4, 0xa

    .line 52
    .line 53
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 54
    .line 55
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;

    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
.end method

.method public 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/contrarywind/view/WheelView;->〇〇o〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final 〇O00(F)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/contrarywind/view/WheelView;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->〇0O:Ljava/util/concurrent/ScheduledExecutorService;

    .line 5
    .line 6
    new-instance v1, Lcom/contrarywind/timer/InertiaTimerTask;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/contrarywind/timer/InertiaTimerTask;-><init>(Lcom/contrarywind/view/WheelView;F)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v2, 0x0

    .line 12
    .line 13
    const-wide/16 v4, 0x5

    .line 14
    .line 15
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 16
    .line 17
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iput-object p1, p0, Lcom/contrarywind/view/WheelView;->oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/contrarywind/view/WheelView;->oOo〇8o008:Ljava/util/concurrent/ScheduledFuture;

    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.class public Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
.super Ljava/lang/Object;
.source "GenAuthThemeConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

.field private J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

.field private K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

.field private L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

.field private M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:I

.field private Q:I

.field private R:Z

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field public a:Z

.field private aa:Ljava/lang/String;

.field private ab:I

.field private ac:Z

.field private ad:I

.field private ae:I

.field private af:Z

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:Z

.field private al:I

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:I

.field private ar:I

.field private as:I

.field private at:I

.field private au:I

.field private av:I

.field private aw:I

.field private ax:Z

.field private ay:Z

.field private az:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:Landroid/view/View;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Landroid/widget/ImageView$ScaleType;

.field private o:I

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Z

.field private w:I

.field private x:Z

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->b:I

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->c:Z

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->d:Landroid/view/View;

    .line 11
    .line 12
    const/4 v2, -0x1

    .line 13
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->e:I

    .line 14
    .line 15
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->f:I

    .line 16
    .line 17
    const/16 v3, 0x11

    .line 18
    .line 19
    iput v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->h:I

    .line 20
    .line 21
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->i:I

    .line 22
    .line 23
    const v3, -0xff7930

    .line 24
    .line 25
    .line 26
    iput v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->j:I

    .line 27
    .line 28
    const-string v4, "return_bg"

    .line 29
    .line 30
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->k:Ljava/lang/String;

    .line 31
    .line 32
    const/4 v4, -0x2

    .line 33
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->l:I

    .line 34
    .line 35
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->m:I

    .line 36
    .line 37
    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    .line 38
    .line 39
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->n:Landroid/widget/ImageView$ScaleType;

    .line 40
    .line 41
    const/16 v4, 0x12

    .line 42
    .line 43
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o:I

    .line 44
    .line 45
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->p:Z

    .line 46
    .line 47
    iput v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->q:I

    .line 48
    .line 49
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->r:I

    .line 50
    .line 51
    const/16 v3, 0xb8

    .line 52
    .line 53
    iput v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->s:I

    .line 54
    .line 55
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->t:I

    .line 56
    .line 57
    const-string v3, "\u672c\u673a\u53f7\u7801\u4e00\u952e\u767b\u5f55"

    .line 58
    .line 59
    iput-object v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->u:Ljava/lang/String;

    .line 60
    .line 61
    const/4 v3, 0x1

    .line 62
    iput-boolean v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->v:Z

    .line 63
    .line 64
    const/16 v4, 0xf

    .line 65
    .line 66
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->w:I

    .line 67
    .line 68
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->x:Z

    .line 69
    .line 70
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->y:I

    .line 71
    .line 72
    const-string v4, "umcsdk_login_btn_bg"

    .line 73
    .line 74
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->z:Ljava/lang/String;

    .line 75
    .line 76
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->A:I

    .line 77
    .line 78
    const/16 v4, 0x24

    .line 79
    .line 80
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->B:I

    .line 81
    .line 82
    const/16 v4, 0x2e

    .line 83
    .line 84
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->C:I

    .line 85
    .line 86
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->D:I

    .line 87
    .line 88
    const/16 v4, 0xfe

    .line 89
    .line 90
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->E:I

    .line 91
    .line 92
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->F:I

    .line 93
    .line 94
    const-string v4, "umcsdk_check_image"

    .line 95
    .line 96
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->N:Ljava/lang/String;

    .line 97
    .line 98
    const-string v4, "umcsdk_uncheck_image"

    .line 99
    .line 100
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O:Ljava/lang/String;

    .line 101
    .line 102
    const/16 v4, 0x9

    .line 103
    .line 104
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->P:I

    .line 105
    .line 106
    iput v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Q:I

    .line 107
    .line 108
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->R:Z

    .line 109
    .line 110
    const-string v4, "\u767b\u5f55\u5373\u540c\u610f$$\u8fd0\u8425\u5546\u6761\u6b3e$$\u5e76\u4f7f\u7528\u672c\u673a\u53f7\u7801\u767b\u5f55"

    .line 111
    .line 112
    iput-object v4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->S:Ljava/lang/String;

    .line 113
    .line 114
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->T:Ljava/lang/String;

    .line 115
    .line 116
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->U:Ljava/lang/String;

    .line 117
    .line 118
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->V:Ljava/lang/String;

    .line 119
    .line 120
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->W:Ljava/lang/String;

    .line 121
    .line 122
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->X:Ljava/lang/String;

    .line 123
    .line 124
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Y:Ljava/lang/String;

    .line 125
    .line 126
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Z:Ljava/lang/String;

    .line 127
    .line 128
    iput-object v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aa:Ljava/lang/String;

    .line 129
    .line 130
    const/16 v1, 0xa

    .line 131
    .line 132
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ab:I

    .line 133
    .line 134
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ac:Z

    .line 135
    .line 136
    const v1, -0x99999a

    .line 137
    .line 138
    .line 139
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ad:I

    .line 140
    .line 141
    const v1, -0xf441fa

    .line 142
    .line 143
    .line 144
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ae:I

    .line 145
    .line 146
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->af:Z

    .line 147
    .line 148
    const/16 v1, 0x34

    .line 149
    .line 150
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ag:I

    .line 151
    .line 152
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ah:I

    .line 153
    .line 154
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ai:I

    .line 155
    .line 156
    const/16 v1, 0x1e

    .line 157
    .line 158
    iput v1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aj:I

    .line 159
    .line 160
    iput-boolean v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ak:Z

    .line 161
    .line 162
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->au:I

    .line 163
    .line 164
    iput v2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->av:I

    .line 165
    .line 166
    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aw:I

    .line 167
    .line 168
    iput-boolean v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ax:Z

    .line 169
    .line 170
    iput-boolean v3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ay:Z

    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
.end method

.method static bridge synthetic O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->t:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O000(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->c:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O08000(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->au:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O0O8OO088(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O0o〇〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->i:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->D:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O8O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Landroid/widget/ImageView$ScaleType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->n:Landroid/widget/ImageView$ScaleType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ab:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O8〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ah:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OO0o〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenAuthLoginListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenLoginClickListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OO8oO0o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->j:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->g:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OOO8o〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->w:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OOO〇O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->af:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Oo08(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->E:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Oo8Oo00oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->an:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic OoO8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->T:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Ooo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->e:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Ooo8〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->z:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->N:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic Oo〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->r:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aa:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic O〇O〇oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aw:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o0O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->k:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o0ooO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ak:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->am:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o800o8O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->U:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o88〇OO08〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->m:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o8O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->s:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o8oO〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ax:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->as:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic oO00OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->b:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic oO80(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->H:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic oo88o8O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->W:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic ooOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->u:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic ooo〇8oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->h:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic oo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ag:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->F:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇0OOo〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ap:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->al:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇8oOO88(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ay:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->az:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇O8〇〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Y:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic o〇〇0〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ae:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->l:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇00(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Z:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇0000OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ad:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇00O0O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->y:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇00〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ai:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇080(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->A:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇08O8o〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ar:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇0O〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->x:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->S:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->at:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇80(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->d:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenBackPressedListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenCheckBoxListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇8〇0〇o〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->av:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->v:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O00(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Q:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O888o0o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->V:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O8o08O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->P:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇O〇80o08O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->f:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aj:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->B:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇o0O0O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->p:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ac:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇oo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->X:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->C:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇0o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aq:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇808〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇888(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->G:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->R:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇o8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->q:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method static bridge synthetic 〇〇〇0〇〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ao:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method


# virtual methods
.method public build()Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;
    .locals 2

    .line 1
    new-instance v0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;-><init>(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;LOooo8o0〇/〇080;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public setAppLanguageType(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aw:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setAuthContentView(Landroid/view/View;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->d:Landroid/view/View;

    .line 2
    .line 3
    const/4 p1, -0x1

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->e:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setAuthLayoutResID(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->e:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->d:Landroid/view/View;

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setAuthPageActIn(Ljava/lang/String;Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->am:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->an:Ljava/lang/String;

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setAuthPageActOut(Ljava/lang/String;Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ao:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ap:Ljava/lang/String;

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setAuthPageWindowMode(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aq:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ar:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setAuthPageWindowOffset(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->as:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->at:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setBackButton(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ay:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setCheckBoxImgPath(Ljava/lang/String;Ljava/lang/String;II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->N:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O:Ljava/lang/String;

    .line 4
    .line 5
    iput p3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->P:I

    .line 6
    .line 7
    iput p4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Q:I

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
.end method

.method public setCheckBoxLocation(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->al:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setCheckTipText(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/16 v1, 0x64

    .line 12
    .line 13
    if-le v0, v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->H:Z

    .line 20
    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    const-string p1, "\u8bf7\u52fe\u9009\u540c\u610f\u670d\u52a1\u6761\u6b3e"

    .line 24
    .line 25
    :cond_2
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->G:Ljava/lang/String;

    .line 26
    .line 27
    return-object p0
    .line 28
.end method

.method public setCheckedImgPath(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->N:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setClauseColor(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ad:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ae:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setClauseLayoutResID(ILjava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->f:I

    .line 2
    .line 3
    iput-object p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->g:Ljava/lang/String;

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setFitsSystemWindows(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ax:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setGenAuthLoginListener(Lcom/cmic/gen/sdk/view/GenAuthLoginListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setGenBackPressedListener(Lcom/cmic/gen/sdk/view/GenBackPressedListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setGenCheckBoxListener(Lcom/cmic/gen/sdk/view/GenCheckBoxListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setGenCheckedChangeListener(Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLogBtn(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->A:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->B:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setLogBtnClickListener(Lcom/cmic/gen/sdk/view/GenLoginClickListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLogBtnImgPath(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->z:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLogBtnMargin(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->C:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->D:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setLogBtnOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->E:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->F:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLogBtnOffsetY_B(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->F:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->E:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setLogBtnText(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "^\\s*\\n*$"

    .line 2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->u:Ljava/lang/String;

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->v:Z

    :cond_0
    return-object p0
.end method

.method public setLogBtnText(Ljava/lang/String;IIZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 1

    .line 7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "^\\s*\\n*$"

    .line 8
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 11
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->u:Ljava/lang/String;

    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->v:Z

    .line 13
    :cond_0
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->y:I

    .line 14
    iput p3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->w:I

    .line 15
    iput-boolean p4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->x:Z

    return-object p0
.end method

.method public setLogBtnTextColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->y:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNavColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->j:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNavTextColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->i:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNavTextSize(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->h:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNumFieldOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->s:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->t:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNumFieldOffsetY_B(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->t:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->s:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNumberColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->q:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNumberOffsetX(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->r:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setNumberSize(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    if-le p1, v0, :cond_0

    .line 4
    .line 5
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o:I

    .line 6
    .line 7
    iput-boolean p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->p:Z

    .line 8
    .line 9
    :cond_0
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setPrivacyAlignment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 1

    .line 1
    const-string v0, "$$\u8fd0\u8425\u5546\u6761\u6b3e$$"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->S:Ljava/lang/String;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->T:Ljava/lang/String;

    .line 12
    .line 13
    iput-object p3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->U:Ljava/lang/String;

    .line 14
    .line 15
    iput-object p4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->V:Ljava/lang/String;

    .line 16
    .line 17
    iput-object p5, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->W:Ljava/lang/String;

    .line 18
    .line 19
    iput-object p6, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->X:Ljava/lang/String;

    .line 20
    .line 21
    iput-object p7, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Y:Ljava/lang/String;

    .line 22
    .line 23
    iput-object p8, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Z:Ljava/lang/String;

    .line 24
    .line 25
    iput-object p9, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aa:Ljava/lang/String;

    .line 26
    .line 27
    :cond_0
    return-object p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
.end method

.method public setPrivacyAnimation(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->az:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setPrivacyBookSymbol(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ak:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setPrivacyMargin(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ag:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ah:I

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setPrivacyOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ai:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aj:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setPrivacyOffsetY_B(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->aj:I

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ai:I

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setPrivacyState(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->R:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setPrivacyText(IIIZZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ab:I

    .line 2
    .line 3
    iput p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ad:I

    .line 4
    .line 5
    iput p3, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ae:I

    .line 6
    .line 7
    iput-boolean p4, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->af:Z

    .line 8
    .line 9
    iput-boolean p5, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ac:Z

    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
.end method

.method public setStatusBar(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->b:I

    .line 2
    .line 3
    iput-boolean p2, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->c:Z

    .line 4
    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public setThemeId(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->av:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setUncheckedImgPath(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setWebDomStorage(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->a:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method public setWindowBottom(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->au:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

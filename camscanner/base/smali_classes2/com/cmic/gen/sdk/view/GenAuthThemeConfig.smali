.class public Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;
.super Ljava/lang/Object;
.source "GenAuthThemeConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;
    }
.end annotation


# static fields
.field public static final PLACEHOLDER:Ljava/lang/String; = "$$\u8fd0\u8425\u5546\u6761\u6b3e$$"


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

.field private J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

.field private K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

.field private L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

.field private M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:I

.field private Q:I

.field private R:Z

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private final a:Z

.field private aa:Ljava/lang/String;

.field private ab:I

.field private ac:Z

.field private ad:I

.field private ae:I

.field private af:Z

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:I

.field private al:Z

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:I

.field private ar:I

.field private as:I

.field private at:I

.field private au:I

.field private av:I

.field private aw:I

.field private ax:Z

.field private ay:Z

.field private az:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:Landroid/view/View;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Landroid/widget/ImageView$ScaleType;

.field private o:I

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Z

.field private w:I

.field private x:Z

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->v:Z

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->X:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Y:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Z:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aa:Ljava/lang/String;

    .line 8
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->oO00OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->b:I

    .line 9
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O000(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->c:Z

    .line 10
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇80(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->d:Landroid/view/View;

    .line 11
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Ooo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->e:I

    .line 12
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O〇80o08O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->f:I

    .line 13
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->g:Ljava/lang/String;

    .line 14
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ooo〇8oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->h:I

    .line 15
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O0o〇〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->i:I

    .line 16
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OO8oO0o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->j:I

    .line 17
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o0O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->k:Ljava/lang/String;

    .line 18
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->l:I

    .line 19
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o88〇OO08〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->m:I

    .line 20
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O8O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->n:Landroid/widget/ImageView$ScaleType;

    .line 21
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O0O8OO088(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->o:I

    .line 22
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇o0O0O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->p:Z

    .line 23
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇o8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->q:I

    .line 24
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Oo〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->r:I

    .line 25
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o8O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->s:I

    .line 26
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->t:I

    .line 27
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->ooOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->u:Ljava/lang/String;

    .line 28
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->v:Z

    .line 29
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OOO8o〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->w:I

    .line 30
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇0O〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->x:Z

    .line 31
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇00O0O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->y:I

    .line 32
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Ooo8〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->z:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇080(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->A:I

    .line 34
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇o00〇〇Oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->B:I

    .line 35
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->C:I

    .line 36
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->D:I

    .line 37
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Oo08(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->E:I

    .line 38
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->F:I

    .line 39
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇888(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->G:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->oO80(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->H:Z

    .line 41
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇80〇808〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenBackPressedListener;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

    .line 42
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OO0o〇〇〇〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenLoginClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

    .line 43
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇8o8o〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 44
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O8o08O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

    .line 45
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Oooo8o0〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->N:Ljava/lang/String;

    .line 46
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇808〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->O:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->P:I

    .line 48
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O00(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Q:I

    .line 49
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇8O0〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->R:Z

    .line 50
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇0〇O0088o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->S:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OoO8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->T:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o800o8O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->U:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇O888o0o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->V:Ljava/lang/String;

    .line 54
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->oo88o8O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->W:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇oo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->X:Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇O8〇〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Y:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇00(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Z:Ljava/lang/String;

    .line 58
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O〇8O8〇008(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aa:Ljava/lang/String;

    .line 59
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O8ooOoo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ab:I

    .line 60
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇oOO8O8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ac:Z

    .line 61
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇0000OOO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ad:I

    .line 62
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇〇0〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ae:I

    .line 63
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OOO〇O0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->af:Z

    .line 64
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->oo〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ah:I

    .line 65
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O8〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ai:I

    .line 66
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇00〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aj:I

    .line 67
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ak:I

    .line 68
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o0ooO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->al:Z

    .line 69
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ag:I

    .line 70
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->am:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->Oo8Oo00oo(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->an:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇〇0〇〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ao:Ljava/lang/String;

    .line 73
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇0OOo〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ap:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇〇0o(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aq:I

    .line 75
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇08O8o〇0(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ar:I

    .line 76
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->as:I

    .line 77
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇8(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->at:I

    .line 78
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O08000(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->au:I

    .line 79
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->〇8〇0〇o〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->av:I

    .line 80
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->O〇O〇oO(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aw:I

    .line 81
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o8oO〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ax:Z

    .line 82
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇8oOO88(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ay:Z

    .line 83
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->o〇O(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->az:Ljava/lang/String;

    .line 84
    iget-boolean v0, p1, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->a:Z

    iput-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->a:Z

    .line 85
    invoke-static {p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->OO0o〇〇(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    move-result-object p1

    iput-object p1, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;LOooo8o0〇/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;-><init>(Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;)V

    return-void
.end method


# virtual methods
.method public getActivityIn()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ap:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getActivityOut()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->an:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getAppLanguageType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aw:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getAuthPageActIn()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->am:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getAuthPageActOut()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ao:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCheckBoxLocation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ag:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCheckTipText()Ljava/lang/String;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->H:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aw:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const-string v0, "\u8acb\u52fe\u9078\u540c\u610f\u670d\u52d9\u689d\u6b3e"

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x2

    .line 14
    if-ne v0, v1, :cond_1

    .line 15
    .line 16
    const-string v0, "Please check to agree to the terms of service"

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const-string v0, "\u8bf7\u52fe\u9009\u540c\u610f\u670d\u52a1\u6761\u6b3e"

    .line 20
    .line 21
    :goto_0
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->G:Ljava/lang/String;

    .line 22
    .line 23
    :cond_2
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->G:Ljava/lang/String;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public getCheckedImgHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Q:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCheckedImgPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->N:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCheckedImgWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->P:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseBaseColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ad:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ae:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseLayoutResID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->f:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseLayoutReturnID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->g:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->T:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseName2()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->V:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseName3()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->X:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseName4()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Z:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseUrl()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->U:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseUrl2()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->W:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseUrl3()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->Y:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getClauseUrl4()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aa:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->d:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getGenAuthLoginListener()Lcom/cmic/gen/sdk/view/GenAuthLoginListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->M:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getGenBackPressedListener()Lcom/cmic/gen/sdk/view/GenBackPressedListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->I:Lcom/cmic/gen/sdk/view/GenBackPressedListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getGenCheckBoxListener()Lcom/cmic/gen/sdk/view/GenCheckBoxListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->K:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getGenCheckedChangeListener()Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->L:Lcom/cmic/gen/sdk/view/GenCheckedChangeListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getGenLoginClickListener()Lcom/cmic/gen/sdk/view/GenLoginClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->J:Lcom/cmic/gen/sdk/view/GenLoginClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLayoutResID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->e:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnBackgroundPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->z:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->B:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnMarginLeft()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->C:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnMarginRight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->D:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnOffsetY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->E:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnOffsetY_B()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->F:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnText()Ljava/lang/String;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->v:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aw:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    const-string v0, "\u672c\u6a5f\u865f\u78bc\u767b\u9304"

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x2

    .line 14
    if-ne v0, v1, :cond_1

    .line 15
    .line 16
    const-string v0, "Login"

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->u:Ljava/lang/String;

    .line 20
    .line 21
    :goto_0
    iput-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->u:Ljava/lang/String;

    .line 22
    .line 23
    :cond_2
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->u:Ljava/lang/String;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
.end method

.method public getLogBtnTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->y:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnTextSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->w:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getLogBtnWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->A:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->j:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavReturnImgHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->m:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavReturnImgPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->k:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavReturnImgScaleType()Landroid/widget/ImageView$ScaleType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->n:Landroid/widget/ImageView$ScaleType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavReturnImgWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->l:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->i:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNavTextSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->h:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNumFieldOffsetY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->s:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNumFieldOffsetY_B()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->t:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNumberColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->q:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNumberOffsetX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->r:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getNumberSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacy()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->S:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyAnimation()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->az:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyMarginLeft()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ah:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyMarginRight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ai:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyOffsetY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aj:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyOffsetY_B()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ak:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getPrivacyTextSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ab:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getStatusBarColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->b:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getThemeId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->av:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getUncheckedImgPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWebStorage()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->a:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWindowBottom()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->au:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWindowHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ar:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWindowWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->aq:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWindowX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->as:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getWindowY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->at:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isBackButton()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ay:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isFitsSystemWindows()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ax:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isLightColor()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->c:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isLogBtnTextBold()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->x:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isNumberBold()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->p:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isPrivacyBookSymbol()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->al:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isPrivacyState()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->R:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isPrivacyTextBold()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->ac:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public isPrivacyTextGravityCenter()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;->af:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

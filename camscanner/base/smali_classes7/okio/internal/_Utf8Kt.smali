.class public final Lokio/internal/_Utf8Kt;
.super Ljava/lang/Object;
.source "-Utf8.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public static final commonAsUtf8ToByteArray(Ljava/lang/String;)[B
    .locals 12
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "$this$commonAsUtf8ToByteArray"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    mul-int/lit8 v0, v0, 0x4

    .line 11
    .line 12
    new-array v0, v0, [B

    .line 13
    .line 14
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    const/4 v2, 0x0

    .line 19
    :goto_0
    const-string v3, "java.util.Arrays.copyOf(this, newSize)"

    .line 20
    .line 21
    if-ge v2, v1, :cond_9

    .line 22
    .line 23
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    const/16 v5, 0x80

    .line 28
    .line 29
    if-lt v4, v5, :cond_8

    .line 30
    .line 31
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    move v4, v2

    .line 36
    :goto_1
    if-ge v2, v1, :cond_7

    .line 37
    .line 38
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    .line 39
    .line 40
    .line 41
    move-result v6

    .line 42
    if-ge v6, v5, :cond_1

    .line 43
    .line 44
    int-to-byte v6, v6

    .line 45
    add-int/lit8 v7, v4, 0x1

    .line 46
    .line 47
    aput-byte v6, v0, v4

    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    :goto_2
    if-ge v2, v1, :cond_0

    .line 52
    .line 53
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-ge v4, v5, :cond_0

    .line 58
    .line 59
    add-int/lit8 v4, v2, 0x1

    .line 60
    .line 61
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    int-to-byte v2, v2

    .line 66
    add-int/lit8 v6, v7, 0x1

    .line 67
    .line 68
    aput-byte v2, v0, v7

    .line 69
    .line 70
    move v2, v4

    .line 71
    move v7, v6

    .line 72
    goto :goto_2

    .line 73
    :cond_0
    move v4, v7

    .line 74
    goto :goto_1

    .line 75
    :cond_1
    const/16 v7, 0x800

    .line 76
    .line 77
    if-ge v6, v7, :cond_2

    .line 78
    .line 79
    shr-int/lit8 v7, v6, 0x6

    .line 80
    .line 81
    or-int/lit16 v7, v7, 0xc0

    .line 82
    .line 83
    int-to-byte v7, v7

    .line 84
    add-int/lit8 v8, v4, 0x1

    .line 85
    .line 86
    aput-byte v7, v0, v4

    .line 87
    .line 88
    and-int/lit8 v4, v6, 0x3f

    .line 89
    .line 90
    or-int/2addr v4, v5

    .line 91
    int-to-byte v4, v4

    .line 92
    add-int/lit8 v6, v8, 0x1

    .line 93
    .line 94
    aput-byte v4, v0, v8

    .line 95
    .line 96
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 97
    .line 98
    :goto_4
    move v4, v6

    .line 99
    goto :goto_1

    .line 100
    :cond_2
    const v7, 0xd800

    .line 101
    .line 102
    .line 103
    const/16 v8, 0x3f

    .line 104
    .line 105
    if-gt v7, v6, :cond_6

    .line 106
    .line 107
    const v7, 0xdfff

    .line 108
    .line 109
    .line 110
    if-ge v7, v6, :cond_3

    .line 111
    .line 112
    goto :goto_6

    .line 113
    :cond_3
    const v9, 0xdbff

    .line 114
    .line 115
    .line 116
    if-gt v6, v9, :cond_5

    .line 117
    .line 118
    add-int/lit8 v9, v2, 0x1

    .line 119
    .line 120
    if-le v1, v9, :cond_5

    .line 121
    .line 122
    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    .line 123
    .line 124
    .line 125
    move-result v10

    .line 126
    const v11, 0xdc00

    .line 127
    .line 128
    .line 129
    if-gt v11, v10, :cond_5

    .line 130
    .line 131
    if-ge v7, v10, :cond_4

    .line 132
    .line 133
    goto :goto_5

    .line 134
    :cond_4
    shl-int/lit8 v6, v6, 0xa

    .line 135
    .line 136
    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    .line 137
    .line 138
    .line 139
    move-result v7

    .line 140
    add-int/2addr v6, v7

    .line 141
    const v7, -0x35fdc00

    .line 142
    .line 143
    .line 144
    add-int/2addr v6, v7

    .line 145
    shr-int/lit8 v7, v6, 0x12

    .line 146
    .line 147
    or-int/lit16 v7, v7, 0xf0

    .line 148
    .line 149
    int-to-byte v7, v7

    .line 150
    add-int/lit8 v9, v4, 0x1

    .line 151
    .line 152
    aput-byte v7, v0, v4

    .line 153
    .line 154
    shr-int/lit8 v4, v6, 0xc

    .line 155
    .line 156
    and-int/2addr v4, v8

    .line 157
    or-int/2addr v4, v5

    .line 158
    int-to-byte v4, v4

    .line 159
    add-int/lit8 v7, v9, 0x1

    .line 160
    .line 161
    aput-byte v4, v0, v9

    .line 162
    .line 163
    shr-int/lit8 v4, v6, 0x6

    .line 164
    .line 165
    and-int/2addr v4, v8

    .line 166
    or-int/2addr v4, v5

    .line 167
    int-to-byte v4, v4

    .line 168
    add-int/lit8 v9, v7, 0x1

    .line 169
    .line 170
    aput-byte v4, v0, v7

    .line 171
    .line 172
    and-int/lit8 v4, v6, 0x3f

    .line 173
    .line 174
    or-int/2addr v4, v5

    .line 175
    int-to-byte v4, v4

    .line 176
    add-int/lit8 v6, v9, 0x1

    .line 177
    .line 178
    aput-byte v4, v0, v9

    .line 179
    .line 180
    add-int/lit8 v2, v2, 0x2

    .line 181
    .line 182
    goto :goto_4

    .line 183
    :cond_5
    :goto_5
    add-int/lit8 v6, v4, 0x1

    .line 184
    .line 185
    aput-byte v8, v0, v4

    .line 186
    .line 187
    goto :goto_3

    .line 188
    :cond_6
    :goto_6
    shr-int/lit8 v7, v6, 0xc

    .line 189
    .line 190
    or-int/lit16 v7, v7, 0xe0

    .line 191
    .line 192
    int-to-byte v7, v7

    .line 193
    add-int/lit8 v9, v4, 0x1

    .line 194
    .line 195
    aput-byte v7, v0, v4

    .line 196
    .line 197
    shr-int/lit8 v4, v6, 0x6

    .line 198
    .line 199
    and-int/2addr v4, v8

    .line 200
    or-int/2addr v4, v5

    .line 201
    int-to-byte v4, v4

    .line 202
    add-int/lit8 v7, v9, 0x1

    .line 203
    .line 204
    aput-byte v4, v0, v9

    .line 205
    .line 206
    and-int/lit8 v4, v6, 0x3f

    .line 207
    .line 208
    or-int/2addr v4, v5

    .line 209
    int-to-byte v4, v4

    .line 210
    add-int/lit8 v6, v7, 0x1

    .line 211
    .line 212
    aput-byte v4, v0, v7

    .line 213
    .line 214
    goto :goto_3

    .line 215
    :cond_7
    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    .line 216
    .line 217
    .line 218
    move-result-object p0

    .line 219
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->O8(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    .line 221
    .line 222
    return-object p0

    .line 223
    :cond_8
    int-to-byte v3, v4

    .line 224
    aput-byte v3, v0, v2

    .line 225
    .line 226
    add-int/lit8 v2, v2, 0x1

    .line 227
    .line 228
    goto/16 :goto_0

    .line 229
    .line 230
    :cond_9
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 231
    .line 232
    .line 233
    move-result p0

    .line 234
    invoke-static {v0, p0}, Ljava/util/Arrays;->copyOf([BI)[B

    .line 235
    .line 236
    .line 237
    move-result-object p0

    .line 238
    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->O8(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    return-object p0
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public static final commonToUtf8String([BII)Ljava/lang/String;
    .locals 16
    .param p0    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p1

    .line 4
    .line 5
    move/from16 v2, p2

    .line 6
    .line 7
    const-string v3, "$this$commonToUtf8String"

    .line 8
    .line 9
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    if-ltz v1, :cond_27

    .line 13
    .line 14
    array-length v3, v0

    .line 15
    if-gt v2, v3, :cond_27

    .line 16
    .line 17
    if-gt v1, v2, :cond_27

    .line 18
    .line 19
    sub-int v3, v2, v1

    .line 20
    .line 21
    new-array v3, v3, [C

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    const/4 v5, 0x0

    .line 25
    :goto_0
    if-ge v1, v2, :cond_26

    .line 26
    .line 27
    aget-byte v6, v0, v1

    .line 28
    .line 29
    if-ltz v6, :cond_1

    .line 30
    .line 31
    int-to-char v6, v6

    .line 32
    add-int/lit8 v7, v5, 0x1

    .line 33
    .line 34
    aput-char v6, v3, v5

    .line 35
    .line 36
    add-int/lit8 v1, v1, 0x1

    .line 37
    .line 38
    :goto_1
    if-ge v1, v2, :cond_0

    .line 39
    .line 40
    aget-byte v5, v0, v1

    .line 41
    .line 42
    if-ltz v5, :cond_0

    .line 43
    .line 44
    add-int/lit8 v1, v1, 0x1

    .line 45
    .line 46
    int-to-char v5, v5

    .line 47
    add-int/lit8 v6, v7, 0x1

    .line 48
    .line 49
    aput-char v5, v3, v7

    .line 50
    .line 51
    move v7, v6

    .line 52
    goto :goto_1

    .line 53
    :cond_0
    :goto_2
    move v5, v7

    .line 54
    goto :goto_0

    .line 55
    :cond_1
    shr-int/lit8 v7, v6, 0x5

    .line 56
    .line 57
    const/4 v8, -0x2

    .line 58
    const/16 v10, 0x80

    .line 59
    .line 60
    const v11, 0xfffd

    .line 61
    .line 62
    .line 63
    if-ne v7, v8, :cond_8

    .line 64
    .line 65
    add-int/lit8 v7, v1, 0x1

    .line 66
    .line 67
    if-gt v2, v7, :cond_3

    .line 68
    .line 69
    int-to-char v6, v11

    .line 70
    add-int/lit8 v7, v5, 0x1

    .line 71
    .line 72
    aput-char v6, v3, v5

    .line 73
    .line 74
    :cond_2
    :goto_3
    const/4 v9, 0x1

    .line 75
    goto :goto_6

    .line 76
    :cond_3
    aget-byte v7, v0, v7

    .line 77
    .line 78
    and-int/lit16 v8, v7, 0xc0

    .line 79
    .line 80
    if-ne v8, v10, :cond_4

    .line 81
    .line 82
    const/4 v8, 0x1

    .line 83
    goto :goto_4

    .line 84
    :cond_4
    const/4 v8, 0x0

    .line 85
    :goto_4
    if-nez v8, :cond_5

    .line 86
    .line 87
    int-to-char v6, v11

    .line 88
    add-int/lit8 v7, v5, 0x1

    .line 89
    .line 90
    aput-char v6, v3, v5

    .line 91
    .line 92
    goto :goto_3

    .line 93
    :cond_5
    xor-int/lit16 v7, v7, 0xf80

    .line 94
    .line 95
    shl-int/lit8 v6, v6, 0x6

    .line 96
    .line 97
    xor-int/2addr v6, v7

    .line 98
    if-ge v6, v10, :cond_6

    .line 99
    .line 100
    int-to-char v6, v11

    .line 101
    add-int/lit8 v7, v5, 0x1

    .line 102
    .line 103
    aput-char v6, v3, v5

    .line 104
    .line 105
    goto :goto_5

    .line 106
    :cond_6
    int-to-char v6, v6

    .line 107
    add-int/lit8 v7, v5, 0x1

    .line 108
    .line 109
    aput-char v6, v3, v5

    .line 110
    .line 111
    :cond_7
    :goto_5
    const/4 v9, 0x2

    .line 112
    :goto_6
    add-int/2addr v1, v9

    .line 113
    goto :goto_2

    .line 114
    :cond_8
    shr-int/lit8 v7, v6, 0x4

    .line 115
    .line 116
    const v13, 0xd800

    .line 117
    .line 118
    .line 119
    const v14, 0xdfff

    .line 120
    .line 121
    .line 122
    const/4 v15, 0x3

    .line 123
    if-ne v7, v8, :cond_12

    .line 124
    .line 125
    add-int/lit8 v7, v1, 0x2

    .line 126
    .line 127
    if-gt v2, v7, :cond_a

    .line 128
    .line 129
    int-to-char v6, v11

    .line 130
    add-int/lit8 v7, v5, 0x1

    .line 131
    .line 132
    aput-char v6, v3, v5

    .line 133
    .line 134
    add-int/lit8 v5, v1, 0x1

    .line 135
    .line 136
    if-le v2, v5, :cond_2

    .line 137
    .line 138
    aget-byte v5, v0, v5

    .line 139
    .line 140
    and-int/lit16 v5, v5, 0xc0

    .line 141
    .line 142
    if-ne v5, v10, :cond_9

    .line 143
    .line 144
    const/4 v5, 0x1

    .line 145
    goto :goto_7

    .line 146
    :cond_9
    const/4 v5, 0x0

    .line 147
    :goto_7
    if-nez v5, :cond_7

    .line 148
    .line 149
    goto :goto_3

    .line 150
    :cond_a
    add-int/lit8 v8, v1, 0x1

    .line 151
    .line 152
    aget-byte v8, v0, v8

    .line 153
    .line 154
    and-int/lit16 v9, v8, 0xc0

    .line 155
    .line 156
    if-ne v9, v10, :cond_b

    .line 157
    .line 158
    const/4 v9, 0x1

    .line 159
    goto :goto_8

    .line 160
    :cond_b
    const/4 v9, 0x0

    .line 161
    :goto_8
    if-nez v9, :cond_c

    .line 162
    .line 163
    int-to-char v6, v11

    .line 164
    add-int/lit8 v7, v5, 0x1

    .line 165
    .line 166
    aput-char v6, v3, v5

    .line 167
    .line 168
    goto :goto_3

    .line 169
    :cond_c
    aget-byte v7, v0, v7

    .line 170
    .line 171
    and-int/lit16 v9, v7, 0xc0

    .line 172
    .line 173
    if-ne v9, v10, :cond_d

    .line 174
    .line 175
    const/4 v12, 0x1

    .line 176
    goto :goto_9

    .line 177
    :cond_d
    const/4 v12, 0x0

    .line 178
    :goto_9
    if-nez v12, :cond_e

    .line 179
    .line 180
    int-to-char v6, v11

    .line 181
    add-int/lit8 v7, v5, 0x1

    .line 182
    .line 183
    aput-char v6, v3, v5

    .line 184
    .line 185
    goto :goto_5

    .line 186
    :cond_e
    const v9, -0x1e080

    .line 187
    .line 188
    .line 189
    xor-int/2addr v7, v9

    .line 190
    shl-int/lit8 v8, v8, 0x6

    .line 191
    .line 192
    xor-int/2addr v7, v8

    .line 193
    shl-int/lit8 v6, v6, 0xc

    .line 194
    .line 195
    xor-int/2addr v6, v7

    .line 196
    const/16 v7, 0x800

    .line 197
    .line 198
    if-ge v6, v7, :cond_f

    .line 199
    .line 200
    int-to-char v6, v11

    .line 201
    add-int/lit8 v7, v5, 0x1

    .line 202
    .line 203
    aput-char v6, v3, v5

    .line 204
    .line 205
    goto :goto_b

    .line 206
    :cond_f
    if-le v13, v6, :cond_10

    .line 207
    .line 208
    goto :goto_a

    .line 209
    :cond_10
    if-lt v14, v6, :cond_11

    .line 210
    .line 211
    int-to-char v6, v11

    .line 212
    add-int/lit8 v7, v5, 0x1

    .line 213
    .line 214
    aput-char v6, v3, v5

    .line 215
    .line 216
    goto :goto_b

    .line 217
    :cond_11
    :goto_a
    int-to-char v6, v6

    .line 218
    add-int/lit8 v7, v5, 0x1

    .line 219
    .line 220
    aput-char v6, v3, v5

    .line 221
    .line 222
    :goto_b
    const/4 v9, 0x3

    .line 223
    goto :goto_6

    .line 224
    :cond_12
    shr-int/lit8 v7, v6, 0x3

    .line 225
    .line 226
    if-ne v7, v8, :cond_25

    .line 227
    .line 228
    add-int/lit8 v7, v1, 0x3

    .line 229
    .line 230
    if-gt v2, v7, :cond_19

    .line 231
    .line 232
    add-int/lit8 v6, v5, 0x1

    .line 233
    .line 234
    aput-char v11, v3, v5

    .line 235
    .line 236
    add-int/lit8 v5, v1, 0x1

    .line 237
    .line 238
    if-le v2, v5, :cond_18

    .line 239
    .line 240
    aget-byte v5, v0, v5

    .line 241
    .line 242
    and-int/lit16 v5, v5, 0xc0

    .line 243
    .line 244
    if-ne v5, v10, :cond_13

    .line 245
    .line 246
    const/4 v5, 0x1

    .line 247
    goto :goto_c

    .line 248
    :cond_13
    const/4 v5, 0x0

    .line 249
    :goto_c
    if-nez v5, :cond_14

    .line 250
    .line 251
    goto :goto_10

    .line 252
    :cond_14
    add-int/lit8 v5, v1, 0x2

    .line 253
    .line 254
    if-le v2, v5, :cond_17

    .line 255
    .line 256
    aget-byte v5, v0, v5

    .line 257
    .line 258
    and-int/lit16 v5, v5, 0xc0

    .line 259
    .line 260
    if-ne v5, v10, :cond_15

    .line 261
    .line 262
    const/4 v12, 0x1

    .line 263
    goto :goto_d

    .line 264
    :cond_15
    const/4 v12, 0x0

    .line 265
    :goto_d
    if-nez v12, :cond_16

    .line 266
    .line 267
    goto :goto_f

    .line 268
    :cond_16
    :goto_e
    const/4 v9, 0x3

    .line 269
    goto/16 :goto_16

    .line 270
    .line 271
    :cond_17
    :goto_f
    const/4 v9, 0x2

    .line 272
    goto/16 :goto_16

    .line 273
    .line 274
    :cond_18
    :goto_10
    const/4 v9, 0x1

    .line 275
    goto/16 :goto_16

    .line 276
    .line 277
    :cond_19
    add-int/lit8 v8, v1, 0x1

    .line 278
    .line 279
    aget-byte v8, v0, v8

    .line 280
    .line 281
    and-int/lit16 v9, v8, 0xc0

    .line 282
    .line 283
    if-ne v9, v10, :cond_1a

    .line 284
    .line 285
    const/4 v9, 0x1

    .line 286
    goto :goto_11

    .line 287
    :cond_1a
    const/4 v9, 0x0

    .line 288
    :goto_11
    if-nez v9, :cond_1b

    .line 289
    .line 290
    add-int/lit8 v6, v5, 0x1

    .line 291
    .line 292
    aput-char v11, v3, v5

    .line 293
    .line 294
    goto :goto_10

    .line 295
    :cond_1b
    add-int/lit8 v9, v1, 0x2

    .line 296
    .line 297
    aget-byte v9, v0, v9

    .line 298
    .line 299
    and-int/lit16 v12, v9, 0xc0

    .line 300
    .line 301
    if-ne v12, v10, :cond_1c

    .line 302
    .line 303
    const/4 v12, 0x1

    .line 304
    goto :goto_12

    .line 305
    :cond_1c
    const/4 v12, 0x0

    .line 306
    :goto_12
    if-nez v12, :cond_1d

    .line 307
    .line 308
    add-int/lit8 v6, v5, 0x1

    .line 309
    .line 310
    aput-char v11, v3, v5

    .line 311
    .line 312
    goto :goto_f

    .line 313
    :cond_1d
    aget-byte v7, v0, v7

    .line 314
    .line 315
    and-int/lit16 v12, v7, 0xc0

    .line 316
    .line 317
    if-ne v12, v10, :cond_1e

    .line 318
    .line 319
    const/4 v12, 0x1

    .line 320
    goto :goto_13

    .line 321
    :cond_1e
    const/4 v12, 0x0

    .line 322
    :goto_13
    if-nez v12, :cond_1f

    .line 323
    .line 324
    add-int/lit8 v6, v5, 0x1

    .line 325
    .line 326
    aput-char v11, v3, v5

    .line 327
    .line 328
    goto :goto_e

    .line 329
    :cond_1f
    const v10, 0x381f80

    .line 330
    .line 331
    .line 332
    xor-int/2addr v7, v10

    .line 333
    shl-int/lit8 v9, v9, 0x6

    .line 334
    .line 335
    xor-int/2addr v7, v9

    .line 336
    shl-int/lit8 v8, v8, 0xc

    .line 337
    .line 338
    xor-int/2addr v7, v8

    .line 339
    shl-int/lit8 v6, v6, 0x12

    .line 340
    .line 341
    xor-int/2addr v6, v7

    .line 342
    const v7, 0x10ffff

    .line 343
    .line 344
    .line 345
    if-le v6, v7, :cond_20

    .line 346
    .line 347
    add-int/lit8 v6, v5, 0x1

    .line 348
    .line 349
    aput-char v11, v3, v5

    .line 350
    .line 351
    goto :goto_15

    .line 352
    :cond_20
    if-le v13, v6, :cond_21

    .line 353
    .line 354
    goto :goto_14

    .line 355
    :cond_21
    if-lt v14, v6, :cond_22

    .line 356
    .line 357
    add-int/lit8 v6, v5, 0x1

    .line 358
    .line 359
    aput-char v11, v3, v5

    .line 360
    .line 361
    goto :goto_15

    .line 362
    :cond_22
    :goto_14
    const/high16 v7, 0x10000

    .line 363
    .line 364
    if-ge v6, v7, :cond_23

    .line 365
    .line 366
    add-int/lit8 v6, v5, 0x1

    .line 367
    .line 368
    aput-char v11, v3, v5

    .line 369
    .line 370
    goto :goto_15

    .line 371
    :cond_23
    if-eq v6, v11, :cond_24

    .line 372
    .line 373
    ushr-int/lit8 v7, v6, 0xa

    .line 374
    .line 375
    const v8, 0xd7c0

    .line 376
    .line 377
    .line 378
    add-int/2addr v7, v8

    .line 379
    int-to-char v7, v7

    .line 380
    add-int/lit8 v8, v5, 0x1

    .line 381
    .line 382
    aput-char v7, v3, v5

    .line 383
    .line 384
    and-int/lit16 v5, v6, 0x3ff

    .line 385
    .line 386
    const v6, 0xdc00

    .line 387
    .line 388
    .line 389
    add-int/2addr v5, v6

    .line 390
    int-to-char v5, v5

    .line 391
    add-int/lit8 v6, v8, 0x1

    .line 392
    .line 393
    aput-char v5, v3, v8

    .line 394
    .line 395
    goto :goto_15

    .line 396
    :cond_24
    add-int/lit8 v6, v5, 0x1

    .line 397
    .line 398
    aput-char v11, v3, v5

    .line 399
    .line 400
    :goto_15
    const/4 v9, 0x4

    .line 401
    :goto_16
    add-int/2addr v1, v9

    .line 402
    goto :goto_17

    .line 403
    :cond_25
    add-int/lit8 v6, v5, 0x1

    .line 404
    .line 405
    aput-char v11, v3, v5

    .line 406
    .line 407
    add-int/lit8 v1, v1, 0x1

    .line 408
    .line 409
    :goto_17
    move v5, v6

    .line 410
    goto/16 :goto_0

    .line 411
    .line 412
    :cond_26
    new-instance v0, Ljava/lang/String;

    .line 413
    .line 414
    invoke-direct {v0, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    .line 415
    .line 416
    .line 417
    return-object v0

    .line 418
    :cond_27
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 419
    .line 420
    new-instance v4, Ljava/lang/StringBuilder;

    .line 421
    .line 422
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 423
    .line 424
    .line 425
    const-string v5, "size="

    .line 426
    .line 427
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    .line 429
    .line 430
    array-length v0, v0

    .line 431
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 432
    .line 433
    .line 434
    const-string v0, " beginIndex="

    .line 435
    .line 436
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    .line 438
    .line 439
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 440
    .line 441
    .line 442
    const-string v0, " endIndex="

    .line 443
    .line 444
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 448
    .line 449
    .line 450
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v0

    .line 454
    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    throw v3
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
.end method

.method public static synthetic commonToUtf8String$default([BIIILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 1
    and-int/lit8 p4, p3, 0x1

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    and-int/lit8 p3, p3, 0x2

    .line 7
    .line 8
    if-eqz p3, :cond_1

    .line 9
    .line 10
    array-length p2, p0

    .line 11
    :cond_1
    invoke-static {p0, p1, p2}, Lokio/internal/_Utf8Kt;->commonToUtf8String([BII)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    return-object p0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

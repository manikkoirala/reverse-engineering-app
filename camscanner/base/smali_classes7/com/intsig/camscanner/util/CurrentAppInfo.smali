.class public Lcom/intsig/camscanner/util/CurrentAppInfo;
.super Ljava/lang/Object;
.source "CurrentAppInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;
    }
.end annotation


# static fields
.field private static O8:Lcom/intsig/camscanner/util/CurrentAppInfo;

.field private static 〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

.field public static 〇o00〇〇Oo:Ljava/lang/String;

.field public static 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->NORMAL_LAUNCH:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 2
    .line 3
    sput-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    sput-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    sput v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o〇:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static 〇080(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    const v0, 0x7f1304ee

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object p0

    .line 8
    const-string v0, "."

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-lez v0, :cond_0

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    :cond_0
    return-object p0
    .line 22
    .line 23
    .line 24
.end method

.method public static 〇80〇808〇O(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "key_first_install_version"

    .line 6
    .line 7
    invoke-virtual {v0, v1, p0}, Lcom/intsig/utils/PreferenceUtil;->oo88o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static 〇o00〇〇Oo()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "key_first_install_version"

    .line 6
    .line 7
    const-string v2, ""

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static 〇o〇()Lcom/intsig/camscanner/util/CurrentAppInfo;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->O8:Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/util/CurrentAppInfo;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->O8:Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 11
    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->O8:Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8oo8888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(Landroid/content/Context;)V
    .locals 4

    .line 1
    const v0, 0x7f1304ee

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇O00〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    sput-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v2, "csVersion : "

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v2, " lastCsVersion : "

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    sget-object v2, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const-string v2, "CurrentAppInfo"

    .line 42
    .line 43
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 47
    .line 48
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    const/4 v2, 0x1

    .line 53
    if-eqz v1, :cond_1

    .line 54
    .line 55
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->NEWLY_INSTALL:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 56
    .line 57
    sput-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 58
    .line 59
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    const/4 v3, 0x6

    .line 64
    if-le v1, v3, :cond_0

    .line 65
    .line 66
    const-string v1, "."

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-lez v1, :cond_0

    .line 73
    .line 74
    const/4 v3, 0x0

    .line 75
    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    goto :goto_0

    .line 80
    :cond_0
    const-string v1, ""

    .line 81
    .line 82
    :goto_0
    invoke-static {v1}, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇80〇808〇O(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 87
    .line 88
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-eqz v1, :cond_2

    .line 93
    .line 94
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->NORMAL_LAUNCH:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 95
    .line 96
    sput-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->UPGRADE:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 100
    .line 101
    sput-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 102
    .line 103
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇o8Oo08〇(Z)V

    .line 104
    .line 105
    .line 106
    :goto_1
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇〇〇00(Landroid/content/Context;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇000O0()I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    add-int/2addr p1, v2

    .line 114
    sput p1, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o〇:I

    .line 115
    .line 116
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇OO(I)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public oO80()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->UPGRADE:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->NEWLY_INSTALL:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇080:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;->NORMAL_LAUNCH:Lcom/intsig/camscanner/util/CurrentAppInfo$APPLaunchState;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

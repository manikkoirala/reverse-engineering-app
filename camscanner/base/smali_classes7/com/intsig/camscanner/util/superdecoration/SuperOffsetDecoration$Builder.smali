.class public Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
.super Ljava/lang/Object;
.source "SuperOffsetDecoration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:F

.field private Oo08:F

.field private o〇0:Z

.field private 〇080:Landroid/util/DisplayMetrics;

.field private 〇o00〇〇Oo:F

.field private 〇o〇:F

.field private 〇〇888:Landroidx/recyclerview/widget/LinearLayoutManager;


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/LinearLayoutManager;Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o00〇〇Oo:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o〇:F

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->O8:F

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->Oo08:F

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->o〇0:Z

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇〇888:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 17
    .line 18
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇080:Landroid/util/DisplayMetrics;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->O8:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o〇:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->Oo08:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->o〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)Landroidx/recyclerview/widget/LinearLayoutManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇〇888:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public OO0o〇〇〇〇0(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iget-object v1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇080:Landroid/util/DisplayMetrics;

    .line 3
    .line 4
    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o〇:F

    .line 9
    .line 10
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public oO80(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iget-object v1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇080:Landroid/util/DisplayMetrics;

    .line 3
    .line 4
    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇o00〇〇Oo:F

    .line 9
    .line 10
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iget-object v1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇080:Landroid/util/DisplayMetrics;

    .line 3
    .line 4
    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->O8:F

    .line 9
    .line 10
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iget-object v1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇080:Landroid/util/DisplayMetrics;

    .line 3
    .line 4
    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->Oo08:F

    .line 9
    .line 10
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O8o08O(Z)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->o〇0:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888()Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration;-><init>(Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;Lo00o0O〇〇o/〇080;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

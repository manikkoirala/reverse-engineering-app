.class public Lcom/intsig/camscanner/util/CsDensityUtil;
.super Ljava/lang/Object;
.source "CsDensityUtil.java"


# direct methods
.method public static 〇080()V
    .locals 10

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 14
    .line 15
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 16
    .line 17
    int-to-float v2, v2

    .line 18
    div-float/2addr v2, v1

    .line 19
    float-to-int v2, v2

    .line 20
    iget v3, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 21
    .line 22
    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 23
    .line 24
    new-instance v5, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    iget v6, v0, Landroid/util/DisplayMetrics;->xdpi:F

    .line 30
    .line 31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v6, "x"

    .line 35
    .line 36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget v7, v0, Landroid/util/DisplayMetrics;->ydpi:F

    .line 40
    .line 41
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    new-instance v7, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    iget v8, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 54
    .line 55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 62
    .line 63
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 71
    .line 72
    .line 73
    move-result-object v6

    .line 74
    const-string v7, "density"

    .line 75
    .line 76
    float-to-double v8, v1

    .line 77
    invoke-virtual {v6, v7, v8, v9}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;D)Lcom/intsig/logagent/JsonBuilder;

    .line 78
    .line 79
    .line 80
    const-string v1, "scaled_density"

    .line 81
    .line 82
    float-to-double v7, v3

    .line 83
    invoke-virtual {v6, v1, v7, v8}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;D)Lcom/intsig/logagent/JsonBuilder;

    .line 84
    .line 85
    .line 86
    const-string v1, "smallest_width"

    .line 87
    .line 88
    invoke-virtual {v6, v1, v2}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;I)Lcom/intsig/logagent/JsonBuilder;

    .line 89
    .line 90
    .line 91
    const-string v1, "density_dpi"

    .line 92
    .line 93
    invoke-virtual {v6, v1, v4}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;I)Lcom/intsig/logagent/JsonBuilder;

    .line 94
    .line 95
    .line 96
    const-string v1, "xy_dpi"

    .line 97
    .line 98
    invoke-virtual {v6, v1, v5}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 99
    .line 100
    .line 101
    const-string v1, "hw_pixels"

    .line 102
    .line 103
    invoke-virtual {v6, v1, v0}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 104
    .line 105
    .line 106
    const-string v0, "screen_size_type"

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/utils/DeviceUtil;->o800o8O()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    invoke-virtual {v6, v0, v1}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v6}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    .line 120
    .line 121
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .line 123
    .line 124
    const-string v2, "logDensity = "

    .line 125
    .line 126
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    const-string v2, "CsDensityUtil"

    .line 141
    .line 142
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    const-string v1, "CSDensityUtil"

    .line 146
    .line 147
    const-string v2, "csdensity"

    .line 148
    .line 149
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 150
    .line 151
    .line 152
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method
